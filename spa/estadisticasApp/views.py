import itertools
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import permission_required, login_required
from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.http import HttpResponseRedirect
from django.template import RequestContext
from spaapp import models as mod
# from . import cryptoaux as ca
# from . import form as forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from dateutil.parser import parse
from django.template.loader import render_to_string
from django.core import serializers
from django.http import HttpResponse
from django.http import JsonResponse
import json
from spaapp import models as mod
from datetime import datetime, date
import calendar
import psycopg2
import pytz
from spa import settings
from django.db.models import Count, Avg, Sum
from django.db.models import Count

from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.viewsets import ModelViewSet


#################################################################################
############## Import para API de Google Analytics #######################
from django.views.generic import TemplateView
from oauth2client.client import OAuth2WebServerFlow
import httplib2
from apiclient.discovery import build
#############################################################################

# from . import serializers as ser
# from django.views.decorators.cache import never_cache
# Create your views here.
# Permite a proveedor visualizar las estadisticas de sus ventas
# Permiso: Debe ser Proveedor
#          debe ser dueño de sus platos vendidos
@permission_required("spaapp.esProveedor", raise_exception=True)
def estadisticas(request):

    usuario_actual = request.user
    # --------------Obtener los platos mas vendidos------------------
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)
    proveedor = mod.Proveedor.objects.get(id_perfil=perfil.id_perfil)
    userSucursal = mod.UserSurcursal.objects.get(id_user=usuario_actual)


    # menu = mod.Menu.objects.get(id_proveedor=proveedor.id_proveedor)
    # platos = mod.Plato.objects.filter(id_menu=menu.id_menu)
    sucursal = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)
    pedidos = mod.Pedido.objects.filter(id_sucursal=sucursal,id_estado=5)


    nombre_sucursal = sucursal.nombre_sucursal
    foto_url = perfil.ruta_foto
    productos_vendidos = []
    lista_id_platos = []
    #lista = []

    # Obtener todos los platos vendidos con sus detalles (id_pedido, id_platp, cantidad)
    for pedido in pedidos:
        #total_ventas += pedido.valor
        detalles_producto = mod.DetallePedido.objects.filter(id_pedido=pedido.id_pedido)
        #print("detalles")
        #print(detalles_producto)
        productos_vendidos.append(detalles_producto)

    #recorrer lista de prodcutos vendidos
    for producto in productos_vendidos:
        #print("==== pedido id:" + str(producto[0].id_pedido.id_pedido) + "=========")
        for p in producto:
            #print("id plato: " + str(p.id_plato.id_plato) + " cantidad:" + str(p.cantidad_solicitada))
            lista_id_platos.append({
                "id_pato":p.id_plato.id_plato,
                 "cantidad": p.cantidad_solicitada
                })

    # valores
    valores_id_patos = []
    for l in lista_id_platos:
        #print("----- valores --------")
        for k, v in l.items():
            print(k, v)
            if k == 'id_pato':
               #print("guardar id")
                valores_id_patos.append(v)

    #agrupar id_platos
    cont = 1
    valores_id_patos.sort()
    #print("------------")
    #print("ordenados", valores_id_patos)
    previous = next_ = None
    l = len(valores_id_patos)
    for index, obj in enumerate(valores_id_patos):
        actual = valores_id_patos[index]
        #print("actual", actual)
        if index > 0:
            previous = valores_id_patos[index - 1]
            #print("previous",previous)
        if index < (l - 1):
            next_ = valores_id_patos[index + 1]
            #print("next",next_)
            if actual == next_:
                print("son iguales")
                cont +=1
            else:
                cont = 1
            print("contador",cont)
        #print("------------")

    copy_list = valores_id_patos
    print(valores_id_patos)
    set_list = list(set(valores_id_patos + copy_list))
    print(set_list)

    #obtener la candidad de cada plato vendido
    platos_vendidos = []
    for id in set_list:
        detalles_platos_vendidos = mod.DetallePedido.objects.filter(id_plato=id, id_pedido__id_estado=5)
        total = 0
        plato = mod.Plato.objects.get(id_plato=id)
        for detalle_plato in detalles_platos_vendidos:
            #print("cantidad %s " %detalle_plato.cantidad_solicitada )
            total += detalle_plato.cantidad_solicitada
        #print("total %s - id %s - nombre: %s " %(total, id,plato.nombre_producto ))
        row = []
        row = json.dumps({
            "nombre": plato.nombre_producto,
            "cantidad": total
        }, sort_keys=True)
        platos_vendidos.append(row)



    # -------------Ventas diarias-------------
    num_ventas = 0
    total_ventas = 0

    today = datetime.now()
    dia_actual = today.day
    mes_actual = today.month
    ultimo_dia_mes = calendar.monthrange(today.year, mes_actual)[1]


    return JsonResponse({'platos_vendidos': platos_vendidos}, safe=False)

def ventasDiarias(request):
    usuario_actual = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)
    proveedor = mod.Proveedor.objects.get(id_perfil=perfil.id_perfil)

    userSucursal = mod.UserSurcursal.objects.get(id_user=usuario_actual)
    #sucursal = get_object_or_404(mod.Sucursal, id_proveedor=proveedor.id_proveedor)
    sucursal = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)
    # ventas= mod.Pedido.objects.raw('select count (*), id_sucursal_id, extract(day from fecha), sum(valor)' +
    #                                ' from spaapp.spaapp_pedido where id_sucursal_id= %s group by id_sucursal_id,extract(day from fecha)',
    #                                [sucursal.id_sucursal])

    db = settings.DATABASES['default']
    try:
        conn = psycopg2.connect("dbname='%s' user='%s' host='%s' port='%s' password='%s'" %
                                (db['NAME'], db['USER'], db['HOST'], db['PORT'], db['PASSWORD']))
    except:
        print("I am unable to connect to the database")
        return HttpResponse(status = 500)
    suc_id=sucursal.id_sucursal
    cur = conn.cursor()
    cur.execute('select count (*), id_sucursal_id, extract(day from fecha) as dia, sum(valor)' +
        ' from spaapp.spaapp_pedido where id_sucursal_id='+str(suc_id)+' and id_estado_id=5 group by id_sucursal_id, dia order by dia;')

    rows = cur.fetchall()

    datos = []
    for cuenta_ventas, id_suc, dia_del_mes, total_ventas in rows:
        dia=int(dia_del_mes)
        fila = json.dumps({
            #"id_sucursal": int(id_suc),
            "dia_del_mes": str(dia),
            "cuenta_ventas": int(cuenta_ventas),
            "total_ventas": total_ventas
        }, sort_keys=True)
        datos.append(fila)

    return JsonResponse({'datos':datos}, safe=False)




def estadisticasHigh(request):
    usuario_actual = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)
    proveedor = mod.Proveedor.objects.get(id_perfil=perfil.id_perfil)
    sucursal = get_object_or_404(mod.Sucursal, id_proveedor=proveedor.id_proveedor)

    #pedido_datetimes = mod.Pedido.objects.filter(id_sucursal=sucursal.id_sucursal, fecha__contains=date(año_actual,mes_actual)).values('fecha')
    #pedido_num = mod.Pedido.objects.filter(id_sucursal=sucursal.id_sucursal).aggregate(Sum('valor'))

   # numero=[]

    today = datetime.now()
    dia_actual = today.day
    mes_actual = today.month
    año_actual = today.year
    pedidos_mes_actual=[]
    dias = []
    dias2 = []

    tz = pytz.timezone('America/Guayaquil')

    pedidos_mes = mod.Pedido.objects.filter(id_sucursal=sucursal.id_sucursal, id_estado=5, fecha__range=(datetime(año_actual, mes_actual, 1, tzinfo=tz),
                                                                         datetime(año_actual, mes_actual, dia_actual,
                                                                                  hour=23, minute=59, second=59,
                                                                                  tzinfo=tz)))

    for dia in range(1, dia_actual+1):
        count = 0
        sum = 0
        pedido_dia_actual = pedidos_mes.filter(fecha__contains=date(año_actual, mes_actual, dia))
        for pedido in pedido_dia_actual:
            count = count + 1
            sum = sum + pedido.valor
        if count != 0:
            dias2.append((dia, count, '%.2f' % sum))

    print(dias2)
    return render(request, 'estadisticas/estadisticasHigh.html', {'dias2': dias2, 'fecha_inicial': datetime(año_actual, mes_actual, dia_actual, tzinfo=tz)})


#######################################################################################################################

class Analytics(TemplateView):
    template_name = 'estadisticas/analytics.html'

    def get(self, *args, **kwargs):
        ID = '294208939926-s20gt9er0ci83tnochpu25j0v7bp8tss.apps.googleusercontent.com'
        secret = 'o_A8Kv5Ug0n4ynQQ7BCQFkiW'
        scope = 'https://www.googleapis.com/auth/analytics.readonly'
        redirect = 'http://localhost:8000/analitica'
        flow = OAuth2WebServerFlow(ID, secret, scope, redirect)
        if not 'code' in self.request.GET:
            print('1')
            authorize_url = flow.step1_get_authorize_url()
            return HttpResponseRedirect(authorize_url)

        code = self.request.GET['code']
        credentials = flow.step2_exchange(code)
        http_r = httplib2.Http()
        http_r = credentials.authorize(http_r)
        service = build('analytics', 'v3', http=http_r)
        id_analytics = 'ga:130452776'
        entrada = '7daysAgo'
        salida = 'today'
        api_query = service.data().ga().get(ids=id_analytics,
                                            start_date=entrada,
                                            end_date=salida,
                                            metrics='ga:users, ga:pageviews',
                                            dimensions = 'ga:source, ga:keyword').execute()
        print(api_query)
        self.coste = float(api_query['totalsForAllResults']['ga:users'])
        self.page = integer(api_query['ga:pageviews'])
        self.source = api_query
        print(self.coste)
        return super(Analytics, self).get(*args, **kwargs)


    def get_context_data(self, **kwargs):
        ctx = super(Analytics, self).get_context_data(**kwargs)
        try:
            ctx['coste'] = self.coste
            ctx['source'] = self.source
            ctx['page'] = self.page
            print(self.coste)
        except Exception:
            ctx['coste'] = None
            ctx['source'] = None
            ctx['page'] = self.page
        return ctx

