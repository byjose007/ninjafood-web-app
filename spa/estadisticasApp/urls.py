from django.conf.urls import include, url, patterns
from django.contrib.auth.views import login, logout_then_login
from . import views
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from rest_framework.routers import SimpleRouter

urlpatterns = [
    url(r'^estadisticas$', views.estadisticas, name='estadisticas'),
    url(r'^ventasDiarias$', views.ventasDiarias, name='ventasDiarias'),
    url(r'^estadisticasHigh', views.estadisticasHigh, name='estadisticasHigh'),
    url(r'^analitica$', views.Analytics.as_view(), name='analitica'),


]
