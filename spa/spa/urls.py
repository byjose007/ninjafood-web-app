"""spa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import password_reset_confirm, password_reset_complete, password_reset_done
from spaapp.views import ResetPasswordRequestView
#from spa.spaapp import urls as surls

urlpatterns = [
    url(r'^spadmin/', include(admin.site.urls)),
    url(r'', include('spaapp.urls')),
    url(r'', include('administracionAPP.urls')),
    url(r'', include('ubicacionApp.urls')),
    url(r'', include('proveedorApp.urls')),
    url(r'', include('estadisticasApp.urls')),
    url(r'', include('valoracionApp.urls')),


    url(r'^account/reset_password', ResetPasswordRequestView.as_view(), name="password_reset"),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm,
           {'template_name': 'reset_password/password_reset_confirm.html'},
           name='password_reset_confirm'
       ),
    url(r'^reset/password_reset_done', password_reset_done,
           {'template_name': 'reset_password/password_reset_done.html'},
           name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm,
           {'template_name': 'reset_password/password_reset_confirm.html'},
           name='password_reset_confirm'
           ),
    url(r'^reset/done', password_reset_complete, {'template_name': 'reset_password/password_reset_complete.html'},
           name='password_reset_complete'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)