"""
Django settings for spa project.

Generated by 'django-admin startproject' using Django 1.8.2.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from django.core.urlresolvers import reverse_lazy

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'b%eo_gpbdq&a7h1(qp89p5fopq-ve5(=2^4hcfk379l5k(fu(v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1']

REST_FRAMEWORK = {
    #'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser','rest_framework.permissions.IsAuthenticated','rest_framework.permissions.AllowAny',),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.AllowAny',),
    #'PAGE_SIZE': 10
}


CORS_ORIGIN_ALLOW_ALL = True

# File Sizes
KIB = 1024
MIB = KIB ** 2

# Max Photo Size we will accept (1 MiB)
# Hard limit as enforced by server is 10 MiB (but only in production)
MAX_PHOTO_SIZE = MIB #50 * KIB

# Image Content Types:
# Escribir como un expression regular de re
IMAGE_CONTENT_TYPES = ["^image/.*$"]


### cuenta de twilio para enviar sms ### INICIO
import bcrypt
#account_sid = "ACb3c4ebdffeee35856c8fde9ff89c4b2c"
#account_sid = "ACe10e71262f39df13fdcbe096447f429b"
#auth_token = "9308e802a3cd15b042ab226662eeb227"
#auth_token = "cfa530a72dac2bbc8546479d8eb4c74a"
#numeroFrom = "+13342394704"
#numeroFrom = "+12015524622"
salKey = bcrypt.gensalt()
#expresion regular para el numer de celular, solo de 10 digitos
#cambiar el cuatro por el 5 si se desea un digito mas en el futuro
ERNT="^(09\d)[ -]?(\d{3})[ -]?(\d{4})$"
### cuenta de twilio para enviar sms ### FIN

### configuracion para INFOBIP ###
import http.client
conn = http.client.HTTPSConnection("api.infobip.com")
headers = {
        'authorization': "Basic Q2lkc2VjdXJlOlNNU2NkczQ1Ng==",
        'content-type': "application/json",
        'accept': "application/json"
    }

####################################


# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'spaapp',
    'ubicacionApp',
    'proveedorApp',
    'estadisticasApp',
    'administracionAPP',
    'rest_framework',
    'corsheaders',
    #'ws4redis',
    'social.apps.django_app.default',
    'valoracionApp',
    # 'crispy_forms',
    # 'fm',

)

# CRISPY_TEMPLATE_PACK = 'bootstrap3'
################################

WEBSOCKET_URL = '/ws/'
'''
WS4REDIS_CONNECTION = {
    'host' : '127.0.0.1',
    'port' : 16379,
    'db' : 17,
    'password' : 'verysecret',
}
'''

WS4REDIS_EXPIRE = 7200

WS4REDIS_PREFIX = 'ws'

#################################

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'spa.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
            ],
        },
    },
]

###WSGI_APPLICATION = 'spa.wsgi.application'
#WSGI_APPLICATION = 'ws4redis.django_runserver.application'
###########################################################

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'spadb',
        'USER': 'spauser',
        'PASSWORD': 'sp@2ol6p@22',
        #'HOST': '190.63.178.243',
        #'HOST': '172.17.113.28',
        'HOST': '127.0.0.1',
        #'PORT': '63333',
        'PORT': '5432',
    }
}


# Autenticacion Facebook

AUTHENTICATION_BACKENDS = (
        'social.backends.facebook.FacebookAppOAuth2',
        'social.backends.facebook.FacebookOAuth2',
        'social.backends.twitter.TwitterOAuth',
        'social.backends.google.GoogleOAuth2',
        'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_LOGIN_REDIRECT_URL = 'https://www.goodappetit.com/'
SOCIAL_AUTH_SANITIZE_REDIRECTS = False
SOCIAL_AUTH_REDIRECT_IS_HTTPS = True

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '759260593041-b3sh5aj31hhj4lfl7oao0e76v2agilce.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'Xrn9Q6jsAp9CUB03on-WLwak'

SOCIAL_AUTH_TWITTER_KEY = 'lBPpb7FthpLMPAdgAsLhxC4B0'
SOCIAL_AUTH_TWITTER_SECRET = 'vXGKQIdaeaVwVjdZKgqIz0J3Cz4NJXx1WElkIYgXAItRvmVXN9'

SOCIAL_AUTH_FACEBOOK_KEY = '630583763776264'
SOCIAL_AUTH_FACEBOOK_SECRET = '478a051557c8bf2cc521c80205eebd3b'

# SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '759260593041-b3sh5aj31hhj4lfl7oao0e76v2agilce.apps.googleusercontent.com'
# SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'Xrn9Q6jsAp9CUB03on-WLwak'
#
# SOCIAL_AUTH_TWITTER_KEY = 'lBPpb7FthpLMPAdgAsLhxC4B0'
# SOCIAL_AUTH_TWITTER_SECRET = 'vXGKQIdaeaVwVjdZKgqIz0J3Cz4NJXx1WElkIYgXAItRvmVXN9'
#
# SOCIAL_AUTH_FACEBOOK_KEY = '630583763776264'
# SOCIAL_AUTH_FACEBOOK_SECRET = '478a051557c8bf2cc521c80205eebd3b'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
   'fields': 'id, name, email, age_range'
 }

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.social_auth.associate_by_email',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'spaapp.pipelines.detalles_usuario',
    'spaapp.pipelines.get_avatar',
)

###WSGI_APPLICATION = 'spa.wsgi.application'
#WSGI_APPLICATION = 'ws4redis.django_runserver.application'

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Guayaquil'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

# MEDIA_ROOT = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media') #Para servidor
# MEDIA_URL = 'http://localhost:8000/media/' #Para produccion
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'spaapp/static')
STATIC_URL = '/static/'
#LOGIN_REDIRECT_URL = reverse_lazy()

STATICFILES_DIRS = (
    #os.path.join(BASE_DIR, 'static'),
    #BASE_DIR, 'static'

)

correosEnviarPedidosRealizados = [
    "procesos@electritelecom.com",
    "presidencia@electritelecom.com",
    "rlmacas@cidsecure.com",
    "yaguachipereira7@gmail.com"
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
#EMAIL_HOST_USER = 'b.appetit.2016@gmail.com'
EMAIL_HOST_USER = 'info@goodappetit.com'
EMAIL_HOST_PASSWORD = 'ajamarHUB2016'
#EMAIL_HOST_PASSWORD = 'goodappetit@'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
#DEFAULT_FROM_EMAIL = 'b.appetit.2016@gmail.com'
DEFAULT_FROM_EMAIL = 'info@goodappetit.com'