from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import permission_required, login_required
from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.http import HttpResponseRedirect
from django.template import RequestContext
from spaapp import models as mod
from spaapp import form as forms
from django.db.models.query_utils import Q

# from . import cryptoaux as ca
# from . import form as forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from dateutil.parser import parse
from django.template.loader import render_to_string
from django.core import serializers
from django.http import Http404, HttpResponse
from django.http import JsonResponse
from spaapp import constantes
from io import StringIO
import json
import base64
from pytz import timezone as pytimezone
import pytz

from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.viewsets import ModelViewSet
# from . import serializers as ser
# from django.views.decorators.cache import never_cache
from datetime import datetime
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.contrib.auth.models import User
from django.utils.timezone import localtime
from django.contrib import messages
import calendar
import requests

# import datetime

# Muestra los pedidos en el panel principal del proveedor
# Permiso: Debe ser Proveedor
#          debe ser dueño de sus pedidos
# @permission_required("spaapp.esProveedor", raise_exception=True)
def soyProveedor(request):

    if request.user.is_authenticated():
        usuario_actual = request.user
        perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)

        userSucursal = mod.UserSurcursal.objects.get(id_user=usuario_actual)
        sucursal = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)

        mod.Sucursal.objects.filter(id_sucursal=sucursal.id_sucursal)


        # -------------Fechas-------------
        today = datetime.now()
        dia_actual = today.day
        mes_actual = today.month
        ultimo_dia_mes = calendar.monthrange(today.year, mes_actual)[1]

        rango1 = str(today.year) + '-' + str(mes_actual) + "-" + str(dia_actual) + ' 00:00:00'
        rango2 = str(today.year) + '-' + str(mes_actual) + '-' + str(dia_actual) + ' 23:59:59'

        pedidos = mod.Pedido.objects.filter(id_sucursal=sucursal.id_sucursal,fecha__range=[rango1, rango2])

        nombre_sucursal = sucursal.nombre_sucursal
        foto_url = perfil.ruta_foto
        total_ventas = 0
        total_ventas_perdidas = 0
        pedidos_perdidos = 0
        pedidos_entregados = 0
        productos_vendidos = []

        for pedido in pedidos:
            estado_pedido = pedido.id_estado.estado

            if (estado_pedido != 'Problema de Proveedor') and (estado_pedido != 'Cancelado por Cliente') \
                    and (estado_pedido != 'Cancelado por Proveedor'):
                total_ventas += pedido.valor
                pedidos_entregados += 1
            else:
                #total_ventas += pedido.valor
                total_ventas_perdidas += pedido.valor
                pedidos_perdidos += 1

            producto = mod.DetallePedido.objects.filter(id_pedido=pedido.id_pedido)
            # nombre del plato, contar numero de platos vendidos
            productos_vendidos.append(producto)



        rango1 = str(today.year) + '-' + str(mes_actual) + '-1'
        rango2 = str(today.year) + '-' + str(mes_actual) + '-' + str(ultimo_dia_mes)

        # --------------Estadísticas ventas diarias-----------------
        ventas_mensuales = mod.Pedido.objects.filter(id_sucursal=sucursal.id_sucursal, fecha__range=[rango1, rango2])
        return render(request, 'proveedor/soyProveedor.html', {'pedidos': pedidos,
                                                               'sucursal': sucursal,
                                                               'total_ventas': total_ventas,
                                                               'perfil': perfil,
                                                               'total_ventas_perdidas': total_ventas_perdidas,
                                                               'pedidos_perdidos': pedidos_perdidos,
                                                               'pedidos_entregados':pedidos_entregados

                                                               })
    else:
        # return render(request, "home/login.html")
        return render(request, "home/login.html")

# Permisos: Solo Proveedores tienen acceso
#TODO Debe ser involucrado en el pedido para poderlo ver
@permission_required("spaapp.esProveedor", raise_exception=True)
def obtenerPedidos(request):
    usuario_actual = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)
    #proveedor = mod.Proveedor.objects.get(id_perfil=perfil.id_perfil)
    #sucursal = get_object_or_404(mod.Sucursal, id_proveedor=proveedor.id_proveedor)
    sucursal = get_object_or_404(mod.UserSurcursal, id_user=usuario_actual.pk)


    #solo pedidos de la fecha actual
    today = datetime.now()
    dia_actual = today.day
    mes_actual = today.month
    ultimo_dia_mes = calendar.monthrange(today.year, mes_actual)[1]
    rango1 = str(today.year) + '-' + str(mes_actual) + "-" + str(dia_actual) + ' 00:00:00'
    rango2 = str(today.year) + '-' + str(mes_actual) + '-' + str(dia_actual) + ' 23:59:59'
    fechaActual = str(today.year) + '-' + str(mes_actual) +'-' +str(dia_actual)

    pedidos = mod.Pedido.objects.filter(id_sucursal=sucursal.id_sucursal, fecha__range=[rango1, rango2]).order_by('-fecha')


    lista = []
    #tz = pytimezone('America/Guayaquil')


    for pedido in pedidos:
        usuario = mod.User.objects.get(pk=pedido.usuario.pk)
        direccion = mod.Direccion.objects.get(id_direccion=pedido.id_direccion_id)
        tipo_pago = mod.TipoPago.objects.get(pk=pedido.id_tipo_pago_id)
        tipo_pedido = mod.TipoPedido.objects.get(id_tipo_pedido=pedido.id_tipo_pedido.id_tipo_pedido)
        estado = mod.CatalogoEstado.objects.get(id_estado=pedido.id_estado.id_estado)
        row = []

        result = localtime(pedido.fecha)
        fecha = ( "%s/%s/%s-%s:%s" % (result.day, result.month, result.year, result.hour, result.minute))

        row = json.dumps(
            {"pk": pedido.pk,
             "cliente": usuario.first_name +' '+usuario.last_name,
             "fecha": str(fecha),
             "tipo_pago": tipo_pago.tipo_pago,
             "tipo_pedido": tipo_pedido.tipo_pedido,
             "valor": pedido.valor,
             "estado": estado.estado,
             "direccion": direccion.calle_principal + " - " + direccion.calle_secundaria},
            sort_keys=True)
        lista.append(row)

    # data = serializers.serialize('json', pedidos)
    return JsonResponse({'lista': lista}, safe=False)\

##############                                                 ##############
#######VISTA PARA OBTENER LOS PEDIDOS RECIENTES CON ESTADO "EN ESPERA"#######
##############                                                 ##############

@permission_required("spaapp.esProveedor", raise_exception=True)
def obtenerPedidosNotificar(request):
    usuario_actual = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)
    proveedor = mod.Proveedor.objects.get(id_perfil=perfil.id_perfil)
    #sucursal = get_object_or_404(mod.Sucursal, id_proveedor=proveedor.id_proveedor)
    sucursal = get_object_or_404(mod.UserSurcursal, id_user=usuario_actual.pk)


    #solo pedidos de la fecha actual
    today = datetime.now()
    dia_actual = today.day
    mes_actual = today.month
    ultimo_dia_mes = calendar.monthrange(today.year, mes_actual)[1]
    rango1 = str(today.year) + '-' + str(mes_actual) + "-" + str(dia_actual) + ' 00:00:00'
    rango2 = str(today.year) + '-' + str(mes_actual) + '-' + str(dia_actual) + ' 23:59:59'
    fechaActual = str(today.year) + '-' + str(mes_actual) +'-' +str(dia_actual)

    pedidos = mod.Pedido.objects.filter(Q(id_sucursal=sucursal.id_sucursal, fecha__range=[rango1, rango2], id_estado_id=1, notificado=False) |
                                        Q(id_sucursal=sucursal.id_sucursal, fecha__range=[rango1, rango2], id_estado_id=10, notificado=True)).order_by('-fecha')
    print(pedidos)
    if(len(pedidos)>=1):
        res=True
        pedido = [pedidos[0]]

        dataPedido = serializers.serialize('json', pedido)

        pedidos[0].notificado = 1
        pedidos[0].save()


        if (pedidos[0].id_estado_id == 10):
            pedidos[0].notificado = 0
            pedidos[0].save()

        return HttpResponse(dataPedido, content_type="application/json")
    elif(len(pedidos)==0):
        res=False
        return JsonResponse(res, safe=False)
    else:
        res=True
        # data = serializers.serialize('json', pedidos)
        return JsonResponse(res, safe=False)

        ################################### TODOS LOS PEDIDOS ########################################

#"@login_required()
@permission_required("spaapp.esProveedor", raise_exception=True)
def todos_mis_pedidos(request):

    page = request.GET['page']
    count = request.GET['count']
    usuario_actual = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)
    #proveedor = mod.Proveedor.objects.get(id_perfil=perfil.id_perfil)

    userSucursal = mod.UserSurcursal.objects.get(id_user=usuario_actual)
    #sucursal = get_object_or_404(mod.Sucursal, id_proveedor=proveedor.id_proveedor)
    sucursal = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)
    # solo pedidos de la fecha actual
    today = datetime.now()
    dia_actual = today.day
    mes_actual = today.month
    ultimo_dia_mes = calendar.monthrange(today.year, mes_actual)[1]
    rango1 = str(today.year) + '-' + str(mes_actual) + "-" + str(dia_actual) + ' 00:00:00'
    rango2 = str(today.year) + '-' + str(mes_actual) + '-' + str(dia_actual) + ' 23:59:59'
    fechaActual = str(today.year) + '-' + str(mes_actual) + '-' + str(dia_actual)

    datos = mod.Pedido.objects.filter(id_sucursal=sucursal.id_sucursal).order_by('-fecha')

    paginador = Paginator(datos, count)
    pedidos = paginador.page(page)

    lista = []
    # tz = pytimezone('America/Guayaquil')


    for pedido in pedidos:
        usuario = mod.User.objects.get(pk=pedido.usuario.pk)
        try:
            direccion = mod.Direccion.objects.get(id_direccion=pedido.id_direccion_id)
        except mod.Direccion.DoesNoExist:
            direccion = None
        tipo_pago = mod.TipoPago.objects.get(pk=pedido.id_tipo_pago_id)
        tipo_pedido = mod.TipoPedido.objects.get(id_tipo_pedido=pedido.id_tipo_pedido.id_tipo_pedido)
        estado = mod.CatalogoEstado.objects.get(id_estado=pedido.id_estado.id_estado)
        row = []

        result = localtime(pedido.fecha)
        fecha = ("%s/%s/%s-%s:%s" % (result.day, result.month, result.year, result.hour, result.minute))

        row = json.dumps(
            {"pk": pedido.pk,
             "cliente": usuario.first_name + ' ' + usuario.last_name,
             "fecha": str(fecha),
             "tipo_pago": tipo_pago.tipo_pago,
             "tipo_pedido": tipo_pedido.tipo_pedido,
             "valor": pedido.valor,
             "estado": estado.estado,
             "direccion": direccion.calle_principal + " - " + direccion.calle_secundaria,
             "total": datos.count()
             },
            sort_keys=True)
        lista.append(row)

    # data = serializers.serialize('json', pedidos)
    return JsonResponse({'lista': lista}, safe=False) \
 \
 \
 \
    ##############################################################################################



# Proveedor puede crear sus platos para su menu
# Permiso: Debe ser Proveedor
#          debe ser dueño de su menu
@permission_required("spaapp.esProveedor", raise_exception=True)
def miMenu(request):
    usuario_actual = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)

    userSucursal = mod.UserSurcursal.objects.get(id_user=usuario_actual)
    sucursal = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)

    #proveedor = mod.Proveedor.objects.get(id_perfil=perfil.id_perfil)
    proveedor = mod.Proveedor.objects.get(pk=sucursal.id_proveedor.id_proveedor)

    #menu = mod.Menu.objects.get(id_proveedor=proveedor.id_proveedor)
    menu = mod.Menu.objects.get(id_sucursal=userSucursal.id_sucursal)
    print(menu.pk)

    platos = mod.Plato.objects.filter(id_menu=menu.id_menu)
    #sucursal = get_object_or_404(mod.Sucursal, id_proveedor=proveedor.id_proveedor)
    userSucursal = mod.UserSurcursal.objects.get(id_user=usuario_actual)
    sucursal = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)

    nombre_sucursal = sucursal.nombre_sucursal
    foto_url = perfil.ruta_foto
    context = {'platos': platos, 'perfil': perfil, 'nombre_sucursal': nombre_sucursal, 'proveedor': proveedor, 'menu': menu}

    return render(request, 'proveedor/miMenu.html', context)

@permission_required("spaapp.esProveedor", raise_exception=True)
def datosProveedor(request):
    usuario_actual = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)
    #proveedor = mod.Proveedor.objects.get(id_perfil=perfil.id_perfil)
    #sucursal = get_object_or_404(mod.Sucursal, id_proveedor=proveedor.id_proveedor)
    userSucursal = mod.UserSurcursal.objects.get(id_user=usuario_actual)
    sucursal = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)



    context = {'perfil': perfil, 'sucursal':sucursal}

    return render(request, 'proveedor/pedidosEntregados.html', context)

# Proveedor puede editar sus datos de perfil
# Permiso: Debe ser Proveedor
#          debe ser dueño de sus datos de perfil
@permission_required("spaapp.esProveedor", raise_exception=True)
def misDatos(request):
    ctx = {}
    usuario = request.user
    perfilid = mod.Perfil.objects.get(id_usuario=usuario)
    direccionid = mod.Direccion.objects.get(id_perfil=perfilid)

    #sucursalid = mod.Sucursal.objects.get(id_proveedor__id_perfil__pk=perfilid.pk)
    userSucursal = mod.UserSurcursal.objects.get(id_user=usuario)
    sucursalid = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)

    foto_url = perfilid.ruta_foto

    formulario=''
    formulario2 = ''
    formulario3 = ''
    if request.method == 'POST':
        perfilform = forms.ProveedorPerfilForm(request.POST, request.FILES, instance=perfilid, prefix='perfil')
        formset = forms.ProveedorDireccionForm(request.POST, request.FILES, instance=direccionid, prefix='direccion')
        sucursalform= forms.ProveedorSucursalForm(request.POST, request.FILES, instance=sucursalid, prefix='sucursal')

        #print(perfilform.is_valid())
        pdv1 = perfilform.is_valid()
        fsv2 = formset.is_valid()
        sfv3 = sucursalform.is_valid()
        if pdv1 and fsv2 and sfv3:
            creado_sucursal= sucursalform.save(commit=False)
            creado_sucursal.id_direccion=formset.save(commit=False)
            creado_sucursal.id_direccion.id_perfil= perfilform.save()
            creado_sucursal.id_direccion.save()
            creado_sucursal.save()
            messages.success(request, 'Sus datos han sido actualizados correctamente!')
            respuesta = redirect(misDatos)
            respuesta.set_cookie('recargame', 'True', 1000)
            return respuesta

        formulario = perfilform
        formulario2 = formset
        formulario3 = sucursalform

            #if perfilform.is_valid() and formset.is_valid() :
            #    creado_direccion = formset.save(commit=False)
            #    creado_direccion.id_perfil = perfilform.save()
            #    creado_direccion.save()
            #    return redirect(misDatos)

    else:
        formulario = forms.ProveedorPerfilForm(instance=perfilid, prefix='perfil')
        formulario2 = forms.ProveedorDireccionForm(instance=direccionid, prefix='direccion')
        formulario3 = forms.ProveedorSucursalForm(instance=sucursalid, prefix='sucursal')

    ctx['formulario'] = formulario
    ctx['formulario2'] = formulario2
    ctx['formulario3'] = formulario3
    ctx['perfil'] = perfilid
    ctx['recargame'] = 'False'

    recargame = request.COOKIES.get('recargame', 'False')

    if recargame == 'True':
        ctx['recargame'] = 'True'

    respuesta = render_to_response('proveedor/misDatos.html', ctx, context_instance=RequestContext(request))

    if recargame == 'True':
        respuesta.set_cookie('recargame', 'False', -1000)

    return respuesta


# @permission_required("spaapp.esProveedor", raise_exception=True)
# def misDatos(request):
#     ctx = {}
#     usuario = request.user
#     perfilid = mod.Perfil.objects.get(id_usuario=usuario)
#     direccionid = mod.Direccion.objects.get(id_perfil=perfilid)
#     sucursalid = mod.Sucursal.objects.get(id_proveedor__id_perfil__pk=perfilid.pk)
#     foto_url = perfilid.ruta_foto
#
#     formulario=''
#     formulario2 = ''
#     formulario3 = ''
#     if request.method == 'POST':
#         perfilform = forms.PerfilProveedorForm2(request.POST, files_req=request.FILES, prefix='perfil')
#         formset = forms.DireccionProveedorForm2(request.POST, prefix='direccion')
#         sucursalform= forms.SucursalProveedorForm2(request.POST, prefix='sucursal')
#
#         pf_1=perfilform.is_valid()
#         df_2=formset.is_valid()
#         sf_3=sucursalform.is_valid()
#
#         if pf_1 and df_2 and sf_3:
#             creado_sucursal= sucursalform.save(commit=False)
#             creado_sucursal.id_direccion=formset.save(commit=False)
#             creado_sucursal.id_direccion.id_perfil= perfilform.save()
#             creado_sucursal.id_direccion.save()
#             creado_sucursal.save()
#             respuesta = redirect(misDatos)
#             respuesta.set_cookie('recargame', 'True', 1000)
#             return respuesta
#
#             #if perfilform.is_valid() and formset.is_valid() :
#             #    creado_direccion = formset.save(commit=False)
#             #    creado_direccion.id_perfil = perfilform.save()
#             #    creado_direccion.save()
#             #    return redirect(misDatos)
#
#     else:
#         formulario = forms.PerfilProveedorForm2(initial={'id_perfil':perfilid.id_perfil, 'ruta_foto':perfilid.ruta_foto})
#         formulario2 = forms.DireccionProveedorForm2(initial={'calle_principal':direccionid.calle_principal, 'calle_secundaria': direccionid.calle_secundaria,
#                                                             'numero_casa':direccionid.numero_casa, 'referencia': direccionid.referencia,
#                                                             'latitud': direccionid.latitud, 'longitud': direccionid.longitud})
#         formulario3 = forms.SucursalProveedorForm2(initial={'telefono':sucursalid.telefono, 'dias_atencion': sucursalid.dias_atencion,
#                                                            'hora_atencion': sucursalid.hora_atencion,'descripcion': sucursalid.descripcion,
#                                                            'precio_envio': sucursalid.precio_envio, 'hora_pico': sucursalid.hora_pico})
#
#     ctx['formulario'] = formulario
#     ctx['formulario2'] = formulario2
#     ctx['formulario3'] = formulario3
#     ctx['perfil'] = perfilid
#     ctx['recargame'] = 'False'
#
#     recargame = request.COOKIES.get('recargame', 'False')
#
#     if recargame == 'True':
#         ctx['recargame'] = 'True'
#
#     respuesta = render_to_response('proveedor/misDatos.html', ctx, context_instance=RequestContext(request))
#
#     if recargame == 'True':
#         respuesta.set_cookie('recargame', 'False', -1000)
#
#     return respuesta


@permission_required("spaapp.esProveedor", raise_exception=True)
def contraseniaUpdate(request):
    ctx = {}
    usuario = request.user
    perfilid = mod.Perfil.objects.get(id_usuario=usuario)

    if request.method == 'POST':
        formulario2 = forms.UpdatePassProveedorForm(request.POST)

        if formulario2.is_valid():
            formulario2.save()
            messages.success(request, 'Su contraseña se cambió correctamente!')
            return redirect('/login')
    else:

        formulario2 = forms.UpdatePassProveedorForm(initial={'username':request.user.username})

    ctx['formulario2'] = formulario2
    ctx['perfil'] = perfilid

    return render_to_response('proveedor/clave.html', ctx, context_instance=RequestContext(request))





# Muestra detalles de un pedido
# Permiso: Debe ser Proveedor
#          debe ser dueño de sus pedidos
@permission_required("spaapp.esProveedor", raise_exception=True)
def obtenerDetalle(request):
    if request.is_ajax():
        id_pedido = request.GET['id_pedido']
        pedido = mod.Pedido.objects.get(id_pedido=id_pedido)
        sucursal = pedido.id_sucursal
        #userSucursal = mod.UserSurcursal.objects.get(id_sucursal=sucursal)
        #usuario = userSucursal.is_user
        #proveedor = sucursal.id_proveedor
        #perfil = proveedor.id_perfil
        usuario = request.user

        if request.user.has_perm("esAdmin") or usuario == request.user:

            detalles = mod.DetallePedido.objects.filter(id_pedido=pedido)
            lista = []

            for detalle in detalles:
                lista_req = []
                plato = mod.Plato.objects.get(id_plato=detalle.id_plato.id_plato)
                #tamanioPlato = mod.Tamanio.objects.get(id_plato=plato.id_plato)
                print(detalle.tamanio.id_tamanio)

                ingredientes_quitar = mod.Modificardetallepedido.objects.filter(id_detallepedido=detalle.id_detalle)
                for ingrediente_quitar in ingredientes_quitar:
                    lista_req.append(ingrediente_quitar.id_ingrediente_quitar.nombre)

                sin_ingred = ""
                primero = True
                for si in lista_req:
                    if not primero:
                        sin_ingred += ", "
                    else:
                        primero = False
                    sin_ingred += si

                row = []
                row = json.dumps(
                    {"cantidad_solicitada": detalle.cantidad_solicitada,
                     "valor": detalle.valor,
                     "nombre_producto": plato.nombre_producto,
                     "observaciones": pedido.observaciones,
                     "tamaño": str(detalle.tamanio.id_tamanio),
                     "sinIngredientes": sin_ingred

                     }, sort_keys=True)
                lista.append(row)

            return JsonResponse({'lista': lista}, safe=False)
        else:
            raise PermissionDenied('Permiso denegado')
    else:
        raise Http404


# Proveedor puede agregar ingredientes a su plato
# Permiso: Debe ser Proveedor
#          debe ser dueño de platos para poder añadir sus ingredientes
@permission_required("spaapp.esProveedor", raise_exception=True)
def obtenerIngredientes(request):
    usuario = request.user
    perfil = mod.Perfil.objects.filter(id_usuario=usuario)
    proveedor = mod.Proveedor.objects.filter(id_perfil=perfil)
    menu = mod.Menu.objects.filter(id_proveedor=proveedor)

    ingredientes = mod.Ingrediente.objects.filter(id_proveedor=proveedor)
    lista = []
    for ingrediente in ingredientes:
        row = json.dumps(
            {"nombre": ingrediente.nombre,
             "pk": ingrediente.pk
             }, sort_keys=True)
        lista.append(row)
    return JsonResponse({'lista': lista}, safe=False)


# Proveedor puede editar el estado del pedido
# Permiso: Debe ser Proveedor
#          debe ser dueño de sus pedidos para modificar el estado del mismo
@permission_required("spaapp.esProveedor", raise_exception=True)
def guardarEstado(request):
    if request.is_ajax():
        id_pedido = request.GET['id_pedido']
        par_estado = request.GET['estado']
        estado = mod.CatalogoEstado.objects.get(estado=par_estado)
        #estado22 = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_PROCESO)
        pedido = mod.Pedido.objects.get(id_pedido=id_pedido)
        sucursal = pedido.id_sucursal
        proveedor = sucursal.id_proveedor
        perfil = proveedor.id_perfil
        #usuario = perfil.id_usuario
        if request.user == request.user:
            if request.GET['tiempo']!='0':
                tiempo = request.GET['tiempo']

                # #verificar si es para recoger o entregar, si es para recoger poner en otro estado# INICIO
                # if(pedido.id_tipo_pedido_id==2 and pedido.id_estado==estado22):
                #     mod.Pedido.objects.filter(id_pedido=id_pedido).update(id_estado=estado22, tiempo=tiempo, notificado=False)
                # else:
                mod.Pedido.objects.filter(id_pedido=id_pedido).update(id_estado=estado, tiempo=tiempo, notificado=False)
                #verificar si es para recoger o entregar, si es para recoger poner en otro estado# FIN
                if par_estado == 'En Proceso':
                    notificationsDriver(sucursal.nombre_sucursal)

            else:

                # # verificar si es para recoger o entregar, si es para recoger poner en otro estado# INICIO
                # if (pedido.id_tipo_pedido_id == 2):
                #     mod.Pedido.objects.filter(id_pedido=id_pedido).update(id_estado=estado22, notificado=False)
                # else:
                mod.Pedido.objects.filter(id_pedido=id_pedido).update(id_estado=estado, notificado=False)
                # verificar si es para recoger o entregar, si es para recoger poner en otro estado# FIN


        else:
            raise PermissionDenied("No tienes permiso")

        return redirect(soyProveedor)
    else:
        raise Http404

def notificationsDriver(brachName):
    header = {"Content-Type": "application/json; charset=utf-8",
              "Authorization": "Basic MTIyZDcyYmYtNGQxMC00MGNlLWI0MjUtNGIwZjBmMWMwMTc3"}

    payload = {"app_id": "9cdcc31a-1aec-4e00-b2fb-2a5d80aae9dc",
               "included_segments": ["All"],
               "contents": {"en": "Tienes un nuevo pedido de "+brachName, "es": "Tienes un nuevo pedido de "+brachName},
               "android_group": "driverNewOrder",
               "priority": 10}

    req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

    print(req.status_code, req.reason)

# Proveedor puede cambiar si el restaurante esta cerrado o abierto
# Permiso: Debe ser Proveedor
#          debe ser dueño de su restaurante
@permission_required("spaapp.esProveedor", raise_exception=True)
def openCloseRestaurante(request):
    if request.is_ajax():
        usuarioActual = request.user
        perfil = mod.Perfil.objects.get(id_usuario=usuarioActual)

        userSucursal = mod.UserSurcursal.objects.get(id_user=usuarioActual)
        #sucursal = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)

        #proveedor = mod.Proveedor.objects.get(id_perfil=perfil)
        estado = request.GET['estado']
        if (estado == 'Cerrado'):
            onpen_close = 1
        else:
            onpen_close = 0

        mod.Sucursal.objects.filter(pk=userSucursal.id_sucursal.id_sucursal).update(estado=onpen_close)
        #sucursal.update(estado=onpen_close)
        print("HAIILLL HL")
        return HttpResponse('true', content_type="application/json")

        ##return redirect(soyProveedor)
    else:
        raise Http404

@permission_required("spaapp.esProveedor", raise_exception=True)
def openCloseRestauranteStatus(request):
    if request.is_ajax():
        usuarioActual = request.user

        perfil = mod.Perfil.objects.get(id_usuario=usuarioActual)
        userSucursal = mod.UserSurcursal.objects.get(id_user=usuarioActual)

        res = get_object_or_404(mod.Sucursal, pk=userSucursal.id_sucursal.id_sucursal)

        #proveedor = mod.Proveedor.objects.get(id_perfil=perfil)
        #estado = request.GET['estado']
        #res=mod.Sucursal.objects.filter(id_proveedor=proveedor)
        print('holaaaa')
        print(res.estado)
        if (res.estado == 0):
            #onpen_close = 0
            #return JsonResponse('false', safe=False)
            return HttpResponse('false', content_type="application/json")
        else:
            #onpen_close = 1
            #return JsonResponse('true', safe=False)
            return HttpResponse('true', content_type="application/json")

    else:
        raise Http404


# Proveedor puede guardar los ingredientes de sus platos
# Permiso: Debe ser Proveedor
#          debe ser dueño de su restaurante
@permission_required("spaapp.esProveedor", raise_exception=True)
def guardarPlato(request):
    nombre_producto = request.POST["nombre_producto"]
    ruta_foto = request.FILES.getlist('file')
    descripcion = request.POST['descripcion']
    cantidad_diaria = request.POST['cantidad_diaria']
    venta_aproximada = request.POST['venta_aproximada']
    hora_mayor_venta = request.POST['hora_mayor_venta']
    cantidad_mayor_venta = request.POST['cantidad_mayor_venta']
    existencia_actual = request.POST['existencia_actual']

    id_menu = request.POST['id_menu']
    menu = mod.Menu.objects.get(id_menu=int(id_menu))

    proveedor = menu.id_proveedor
    #perfil = proveedor.id_perfil

    usuario = mod.UserSurcursal.objects.get(id_user=request.user)

    if usuario.id_user == request.user:
        lista = []
        platocategoria = request.POST['platocategoria']
        ingredientes_seleccionados = request.POST['ingredientes']
        ingredientes = json.loads(ingredientes_seleccionados)

        tamanioPlatosSelecionados = request.POST['tamanioPlatosSelecionados']
        tamanio_platos = json.loads(tamanioPlatosSelecionados)
        if nombre_producto != '' or ruta_foto or descripcion != '' or platocategoria != 'null' or len(
                ingredientes_seleccionados) > 2 or len(tamanioPlatosSelecionados) > 2:
            hora_mayorVenta = datetime.strptime(hora_mayor_venta, '%H:%M:%S').time()
            fecha = timezone.now()

            platoCat = mod.PlatoCategoria.objects.get(pk=int(platocategoria))
            # menu = mod.Menu.objects.get(pk=int(id_menu))

            plato = mod.Plato.objects.create(nombre_producto=nombre_producto,
                                             ruta_foto=ruta_foto[0],
                                             descripcion=descripcion,
                                             cantidad_diaria=int(cantidad_diaria),
                                             venta_aproximada=int(venta_aproximada),
                                             hora_mayor_venta=hora_mayorVenta,
                                             cantidad_mayor_venta=int(cantidad_mayor_venta),
                                             existencia_actual=int(existencia_actual),
                                             id_platocategoria=platoCat)
            plato.id_menu.add(menu)

            for ingrediente in ingredientes:
                try:
                    ing = mod.Ingrediente.objects.get(nombre=ingrediente['nombre'], id_proveedor=proveedor)
                    print(ing)
                except mod.Ingrediente.DoesNotExist:
                    ing = None
                    ing = mod.Ingrediente(nombre=ingrediente['nombre'], id_proveedor=proveedor)
                    ing.save()

                mod.PlatoIngrediente.objects.create(id_plato=plato, id_ingrediente=ing,
                                                    requerido=ingrediente["obligatorio"],
                                                    horafecha=fecha)
            for tam in tamanio_platos:
                tamanio = mod.Tamanio.objects.get(tamanio=tam['tamanio'])
                existeTamanioPlato=mod.TamanioPlato.objects.filter(id_plato=plato, id_tamanio=tamanio)
                if existeTamanioPlato.count()==0:
                    valor_oferta = tam['valor_oferta']
                    if valor_oferta == '':
                        valor_oferta = None
                    mod.TamanioPlato.objects.create(id_plato=plato, id_tamanio=tamanio, valor=tam['precio'], activo="True", valor_oferta=valor_oferta)

            row = json.dumps(
                {"nombre": 'OK',
                 "pk": 1
                 }, sort_keys=True)

        else:
            row = json.dumps(
                {"nombre": 'ERROR',
                 "pk": 1
                 }, sort_keys=True)

        lista.append(row)

        return JsonResponse({'response': lista}, safe=False)
    else:
        raise PermissionDenied('No tienes acceso')


# Proveedor puede obtener categoria del plato
# Permiso: Debe ser Proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def obtenerPlatoCategoria(request):
    platoCategorias = mod.PlatoCategoria.objects.all()
    lista = []
    for platoCategoria in platoCategorias:
        row = json.dumps(
            {"categoria": platoCategoria.categoria,
             "pk": platoCategoria.pk
             }, sort_keys=True)
        lista.append(row)
    return JsonResponse({'lista': lista}, safe=False)


# Proveedor puede obtener tamanio del plato
# Permiso: Debe ser Proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def obtenerTamanioPlato(request):
    tamanios = mod.Tamanio.objects.all()
    lista = []
    for tam in tamanios:
        row = json.dumps(
            {"tamanio": tam.tamanio,
             "pk": tam.pk
             }, sort_keys=True)
        lista.append(row)
    return JsonResponse({'lista': lista}, safe=False)


# Proveedor puede listar platos de su restarurante
# Permiso: Debe ser Proveedor
#          debe ser dueño de su restaurante
@permission_required("spaapp.esProveedor", raise_exception=True)
def listarPlatos(request):
    count = request.GET['count']
    page = request.GET['page']

    usuario = request.user
    perfil = mod.Perfil.objects.filter(id_usuario=usuario)
    proveedor = mod.Proveedor.objects.filter(id_perfil=perfil)

    usuarioSucursal = mod.UserSurcursal.objects.get(id_user=usuario)
    sucursal = usuarioSucursal.id_sucursal

    menu = mod.Menu.objects.filter(id_sucursal=sucursal)

    datos = mod.Plato.objects.filter(id_menu=menu)
    paginador = Paginator(datos, count)
    platos = paginador.page(page)
    lista = []
    pk = 0

    for plato in platos:
        pk += 1
        platoCategoria = mod.PlatoCategoria.objects.get(pk=plato.id_platocategoria.id_platocategoria)
        row = []
        row = json.dumps(
            {"pk": pk,
             "id_plato": plato.pk,
             "nombre_producto": plato.nombre_producto,
             "ruta_foto": str(plato.ruta_foto),
             "descripcion": plato.descripcion,
             "categoria": platoCategoria.categoria,
             "activo": plato.activo,
             "total": datos.count()
             },
            sort_keys=True)
        lista.append(row)

    return JsonResponse({'lista': lista}, safe=False)


@permission_required("spaapp.esProveedor", raise_exception=True)
def eliminarPlato(request):
    pk = request.GET['index']
    instance = get_object_or_404(mod.Plato, id_plato=pk)
    instance.delete()
    return JsonResponse({'msj': 'ok'}, safe=False)


@permission_required("spaapp.esProveedor", raise_exception=True)
def iniciarActualizarPlato(request):
    pk = request.GET['pk']
    plato = mod.Plato.objects.get(id_plato=int(pk))
    lista = []
    listaPlatoIngrediente = []
    listaTamanioPlato = []

    platoIngrediente = mod.PlatoIngrediente.objects.filter(id_plato=int(pk))

    for ping in platoIngrediente:
        row = {
            "pk": ping.id_ingrediente.pk,
            "obligatorio": ping.requerido,
            "nombre": ping.id_ingrediente.nombre,
            "seleccionado": True,
            "plato_ingrediente_id": ping.id_plato_ingrediente
        }
        listaPlatoIngrediente.append(row)
    tamanioPlato = mod.TamanioPlato.objects.filter(id_plato=int(pk))

    for tamPlato in tamanioPlato:
        row = {
            "id": tamPlato.id_tamanio.pk,
            "id_tamanioplato": tamPlato.id_tamanioplato,
            "precio": tamPlato.valor,
            "tamanio": tamPlato.id_tamanio.tamanio,
            "activo": tamPlato.activo,
            "valor_oferta": tamPlato.valor_oferta
        }
        listaTamanioPlato.append(row)

    listaPlatoMenu = []
    for platoMenu in plato.id_menu.all():
        row = {
            "id_menu": platoMenu.pk,
            "id_proveedor": platoMenu.id_proveedor.id_proveedor
        }
        listaPlatoMenu.append(row)

    row = json.dumps(
        {"id_plato": plato.pk,
         "nombre_producto": plato.nombre_producto,
         "ruta_foto": str(plato.ruta_foto),
         "descripcion": plato.descripcion,
         "cantidad_diaria": plato.cantidad_diaria,
         "venta_aproximada": plato.venta_aproximada,
         "hora_mayor_venta": str(plato.hora_mayor_venta),
         "cantidad_mayor_venta": plato.cantidad_mayor_venta,
         "existencia_actual": plato.existencia_actual,
         "id_menu": listaPlatoMenu,
         "id_platocategoria": plato.id_platocategoria.id_platocategoria,
         "ingredientes": listaPlatoIngrediente,
         "tamanios": listaTamanioPlato,
         "activo": plato.activo
         },
        sort_keys=True)
    lista.append(row)
    return JsonResponse({'lista': lista}, safe=False)


# ACTUALIZACION DE PLATO
# DEBE SER PROVEEDOR
# AUN SE DEBE CORREGIR LA ACTUALIZACION DE FILE(IMAGEN)
@permission_required("spaapp.esProveedor", raise_exception=True)
def actualizarPlato(request):
    id_plato = request.POST["id_plato"]
    nombre_producto = request.POST["nombre_producto"]
    ruta_foto = request.FILES.getlist('file')
    descripcion = request.POST['descripcion']
    cantidad_diaria = request.POST['cantidad_diaria']
    venta_aproximada = request.POST['venta_aproximada']
    hora_mayor_venta = request.POST['hora_mayor_venta']
    cantidad_mayor_venta = request.POST['cantidad_mayor_venta']
    existencia_actual = request.POST['existencia_actual']
    id_menu = request.POST['id_menu']
    platocategoria = request.POST['platocategoria']
    ingredientes_seleccionados = request.POST['ingredientes']
    tamanioPlatosSelecionados = request.POST['tamanioPlatosSelecionados']
    platoActivo = request.POST['platoActivo']

    #ML, Actualizar foto
    try:
        print("Vale", ruta_foto[0])
        if (ruta_foto[0]):
            plato = mod.Plato.objects.get(pk=int(id_plato))
            plato.ruta_foto = request.FILES['file']
            plato.save()
    except IndexError:
         print("ERROR: No cambio de foto")


    usuario_actual = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)

    activo = True
    if platoActivo=='false':
        activo = False
    else:
        activo = True

    lista = []
    if nombre_producto != '' or descripcion != '' or len(ingredientes_seleccionados) > 2 or len(
            tamanioPlatosSelecionados) > 2:
        ingredientes = json.loads(ingredientes_seleccionados)
        tamanio_platos = json.loads(tamanioPlatosSelecionados)
        platocategoria = json.loads(platocategoria)
        hora_mayorVenta = datetime.strptime(hora_mayor_venta, '%H:%M:%S').time()
        fecha = timezone.now()
        platoCat = mod.PlatoCategoria.objects.get(pk=int(platocategoria['pk']))

        userSucursal = mod.UserSurcursal.objects.get(id_user=usuario_actual)
        menu = mod.Menu.objects.get(id_sucursal=userSucursal.id_sucursal)
        id_menu = menu.pk
        platoIngrediente = mod.PlatoIngrediente.objects.filter(id_plato=int(id_plato))

        # if (len(ruta_foto) == 0):
        # print(" - id_plato: " + id_plato, " -nombre_producto: " + nombre_producto, " - descripcion: " + descripcion,
        #       " - cantidad_diaria: " + cantidad_diaria, " - venta_aproximada: " + venta_aproximada,
        #       " - hora_mayor_venta: " + hora_mayor_venta, " - cantidad_mayor_venta: " + cantidad_mayor_venta,
        #       " - existencia_actual: " + existencia_actual, " - id_menu: " + str(menu.pk),
        #       " - platocategoria: " + str(platocategoria),
        #       " - ingredientes_seleccionados: " + ingredientes_seleccionados,
        #       " - tamanioPlatosSelecionados: " + tamanioPlatosSelecionados)
        plato = mod.Plato.objects.filter(pk=int(id_plato)).update(nombre_producto=nombre_producto,
                                                                  descripcion=descripcion,
                                                                  cantidad_diaria=int(cantidad_diaria),
                                                                  venta_aproximada=int(venta_aproximada),
                                                                  hora_mayor_venta=hora_mayorVenta,
                                                                  cantidad_mayor_venta=int(cantidad_mayor_venta),
                                                                  existencia_actual=int(existencia_actual),
                                                                  id_platocategoria=platoCat,
                                                                  activo=activo)

        plato = mod.Plato.objects.get(pk=int(id_plato))
        plato.id_menu.clear()
        plato.id_menu.add(menu)

        mod.PlatoIngrediente.objects.filter(id_plato=int(id_plato)).delete()
        #proveedor = mod.Proveedor.objects.get(pk=int(id_menu))
        proveedor = menu.id_proveedor
        for ingrediente in ingredientes:
            try:
                ing = mod.Ingrediente.objects.get(nombre=ingrediente['nombre'], id_proveedor=proveedor)
            except mod.Ingrediente.DoesNotExist or mod.Ingrediente.MultipleObjectsReturned:
                ing = None
                ing = mod.Ingrediente(nombre=ingrediente['nombre'], id_proveedor=proveedor)
                ing.save()
            print(ing)
            mod.PlatoIngrediente.objects.create(id_plato=plato,
                                                id_ingrediente=ing,
                                                requerido=ingrediente["obligatorio"],
                                                horafecha=fecha)

        mod.TamanioPlato.objects.filter(id_plato=int(id_plato)).delete()
        for tam in tamanio_platos:
            tamanio = mod.Tamanio.objects.get(tamanio=tam['tamanio'])
            existeTamanioPlato = mod.TamanioPlato.objects.filter(id_plato=plato, id_tamanio=tamanio)
            if existeTamanioPlato.count() == 0:
                valor_oferta = tam['valor_oferta']
                if valor_oferta == '':
                    valor_oferta = None
                mod.TamanioPlato.objects.create(id_plato=plato, id_tamanio=tamanio,
                                                valor=tam['precio'], activo=tam['activo'],
                                                valor_oferta=valor_oferta)

        row = json.dumps(
            {"msj": 'OK',
             "pk": 1
             }, sort_keys=True)
        lista.append(row)
    else:
        row = json.dumps(
            {"msj": 'ERROR',
             "pk": 1
             }, sort_keys=True)
        lista.append(row)
    return JsonResponse({'lista': lista}, safe=False)


def loginProveedor(request):
    if request.method == 'POST':
        form = forms.FormLogin(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    auth_login(request, user)
                    # Redirect to a success page.
                    # print("Valid User %s" % username)
                    return redirect("/proveedor")
                else:
                    # Return a 'disabled account' error message
                    print("User %s has valid credentials, but a disabled account." % username)
            else:
                # Return an 'invalid login' error message.
                print("Invalid Login for \"%s\"" % username)
        else:
            print("Invalid Form Data")
        return redirect(soyProveedor)
    else:
        form = forms.FormLogin()
    return render(request, "proveedor/loginProveedor.html", {'form': form})

# ===================================

def pedidosEntregados(request):
    return render(request, 'proveedor/pedidosEntregados.html')
