from django import forms
from django.core.exceptions import ValidationError
from django.forms import Form, ModelForm, PasswordInput, CharField
from spaapp import forms as spaforms
from spaapp import models as mod
from django.template.defaultfilters import filesizeformat
from mimetypes import MimeTypes
from urllib.request import pathname2url

class DireccionForm(ModelForm):
    #prefix = 'direccion'
    class Meta:
        model = mod.Direccion
        fields = '__all__'
        widgets = {'id_perfil': forms.HiddenInput()}

# Esta vista nunca es usada:
# class PerfilForm(ModelForm):
#     # prefix = 'perfil'
#
#     # ruta_foto = spaforms.fotoField()
#
#     class Meta:
#         model = mod.Perfil
#         fields = '__all__'
#         widgets={'cedula':forms.HiddenInput(),'fecha_nacimiento':forms.HiddenInput(), 'genero':forms.HiddenInput(),'id_usuario':forms.HiddenInput(),'sim_dispositivo':forms.HiddenInput()}
#         # field_classes={'ruta_foto':spaforms.fotoField()}
#
#     # def clean(self):
#     #     data = super(PerfilForm, self).clean()
#     #     foto = data['ruta_foto']
#     #     # file = data.file
#     #     try:
#     #         mime = MimeTypes()
#     #         url = pathname2url(foto._name)
#     #         # content_type = file.content_type
#     #         content_type = mime.guess_type(url)[0]
#     #         correct_type = False
#     #         for image_type in settings.IMAGE_CONTENT_TYPES:
#     #             if re.search(image_type, content_type) is not None:
#     #                 correct_type = True
#     #                 break
#     #         if correct_type:
#     #             if data._size > settings.MAX_PHOTO_SIZE:
#     #                 tamanio = ValidationError('Por favor sube una foto menos que %s. Tamaño del foto subido es %s.' % (
#     #                 filesizeformat(settings.MAX_PHOTO_SIZE), filesizeformat(foto._size)))
#     #                 self.add_error('ruta_foto', tamanio)
#     #         else:
#     #             tipo = ValidationError('Tipo de archivo no es imagen.')
#     #             self.add_error('ruta_foto', tipo)
#     #     except AttributeError as ae:
#     #         servidor = ValidationError('Error de Servidor. Intenta de nuevo.')
#     #         self.add_error('ruta_foto', servidor)
#     #     return data
