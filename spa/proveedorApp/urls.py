from django.conf.urls import include, url, patterns
from . import views

urlpatterns = [
    url(r'^proveedor$', views.soyProveedor, name='proveedor'),
    url(r'^miMenu$', views.miMenu, name='miMenu'),
    url(r'^misDatos', views.misDatos, name='misDatos'),
    url(r'^clave', views.contraseniaUpdate),
    url(r'^obtenerDetalle$', views.obtenerDetalle),
    url(r'^guardarEstado$', views.guardarEstado),
    url(r'^openCloseRestaurante$', views.openCloseRestaurante),
    url(r'^openCloseRestauranteStatus$', views.openCloseRestauranteStatus),
    url(r'^guardarPlato$', views.guardarPlato),
    url(r'^obtenerIngredientes$', views.obtenerIngredientes),
    url(r'^obtenerPlatoCategoria$', views.obtenerPlatoCategoria),
    url(r'^obtenerTamanioPlato$', views.obtenerTamanioPlato),
    url(r'^listarPlatos$', views.listarPlatos),
    url(r'^eliminarPlato$', views.eliminarPlato),
    url(r'^iniciarActualizarPlato$', views.iniciarActualizarPlato),
    url(r'^actualizarPlato$', views.actualizarPlato),
    url(r'^obtenerPedidos$', views.obtenerPedidos),
    url(r'^obtenerPedidosNotificar$', views.obtenerPedidosNotificar),
    url(r'^pedidosEntregados$', views.datosProveedor),
    url(r'^pedidosEntregados$', views.pedidosEntregados),
    url(r'^todosMisPedidos$', views.todos_mis_pedidos, name='todosMisPedidos'),
]