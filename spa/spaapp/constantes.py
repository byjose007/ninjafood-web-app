
'''
    mod.CatalogoEstado.get_or_create(estado="En Espera") # cuando se pide
    mod.CatalogoEstado.get_or_create(estado="En Proceso") # cuando accepta el proveedor/sucursal
    mod.CatalogoEstado.get_or_create(estado="Transportista en Espera") # cuando accepta el transportista al envio
    mod.CatalogoEstado.get_or_create(estado="Enviado") # cuando se entrega al transportista
    mod.CatalogoEstado.get_or_create(estado="Entregado") # cuando se entrega al cliente
    mod.CatalogoEstado.get_or_create(estado="Problema de Proveedor") # cuando hay problema en el surcursal y no se puede hacer el plato
    mod.CatalogoEstado.get_or_create(estado="Problema en Entrega") # cuando hay problema con el transportista
    mod.CatalogoEstado.get_or_create(estado="Queja de Cliente") # cuando el cliente no esta satisfecho
'''
ESTADO_PEDIDO = "En Espera"  # cuando se pide
ESTADO_PROCESO = "En Proceso" # cuando accepta el proveedor/sucursal
ESTADO_TRANSPORTISTA = "Transportista en Espera" # cuando accepta el transportista al envio
ESTADO_ENVIADO = "Enviado" # cuando se entrega al transportista
ESTADO_ENTREGADO = "Entregado" # cuando se entrega al cliente
ESTADO_PROBLEMA_PROVEEDOR = "Problema de Proveedor" # cuando hay problema en el surcursal y no se puede hacer el plato
ESTADO_PROBLEMA_TRANSPORTISTA = "Problema en Entrega" # cuando hay problema con el transportista
ESTADO_QUEJA = "Queja de Cliente" # cuando el cliente no esta satisfecho
ESTADO_ESTOY_FUERA = "Estoy_fuera" # cuando el transportista esta fuera
ESTADO_CANCELADO_CLIENTE = "Cancelado por Cliente"  # cuando el pedido es cancelado por el cliente
ESTADO_CANCELADO_PROVEEDOR = "Cancelado por Proveedor" #cuando el pedido es cancelado por el proveedor



ESTADO_NOTIFICACION_INCICIAL = 'I'
ESTADO_NOTIFICACION_PERSISTENTE = 'P'
ESTADO_NOTIFICACION_TERMINADO = 'T'
