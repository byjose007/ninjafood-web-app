from . import models as mod
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, Field
from datetime import datetime, timedelta
from rest_framework.serializers import StringRelatedField, \
    PrimaryKeyRelatedField, SlugRelatedField,HyperlinkedModelSerializer, RelatedField, Serializer

class SucursalSerializer(ModelSerializer):
    # categorias = serializers.SerializerMethodField('getCategorias')
    class Meta:
        model = mod.Sucursal
        depth = 2

    # def getCategorias(self,obj):
    #     platos = mod.Plato.objects.filter(activo=True).order_by('id_platocategoria')



class ProveedorSerializer(ModelSerializer):
    sucursales = SucursalSerializer(many=True)
    class Meta:
        model = mod.Proveedor


class TamanioPlatoSerializer(ModelSerializer):
    class Meta:
        model = mod.TamanioPlato
        depth = 2


class TamanioPlatoDetalleSerializer(ModelSerializer):
    class Meta:
        model = mod.TamanioPlato

class PlatoSerializer(ModelSerializer):
    tamanio = TamanioPlatoSerializer(many=True)
    class Meta:
        model = mod.Plato
        depth = 2

#JY,Serializer datos ingredientes de cada plato
class PlatoIngredientesSerializer(ModelSerializer):
    #tamanio = TamanioPlatoSerializer(many=True)
    class Meta:
        model = mod.PlatoIngrediente
        depth = 1

class MenuSerializer(ModelSerializer):
    class Meta:
        model = mod.Menu

class CoordenadaSerializer(ModelSerializer):
    class Meta:
        model = mod.Coordenadas

# pedido
class PedidoSerializer(ModelSerializer):
    class Meta:
        model = mod.Pedido
        depth = 3

# Pedidos para los usuarios logueados (móvil)
class MisPedidosSerializer(ModelSerializer):
    deliveryTime = serializers.SerializerMethodField('computeDeliveryTime')

    def computeDeliveryTime(self, pedido):
        currentDate = datetime.now()
        print("currentDate: ",currentDate)
        estimateTimeMinutes = timedelta(0)
        if pedido.id_estado_id >= 1 & pedido.id_estado_id < 6:
            if pedido.tiempo == None:
                pedido.tiempo = 0
            #print(pedido.fecha)
            orderDate = (pedido.fecha.replace(tzinfo=None) + timedelta(minutes=pedido.tiempo) + timedelta(minutes=5))
            #print(orderDate)
            #Pedido no tine zona horia por lo que esta por defecto 00
            #Se resta -5 para que trabaje con la zona horario de Ecuador
            orderDate = orderDate - timedelta(hours=5)
            print("orderDate: ",orderDate)
            estimateTimeMinutes = orderDate - currentDate
            if estimateTimeMinutes < timedelta(0):
                estimateTimeMinutes = timedelta(0)
        #devuleve tiempo restante en segundos
        print("estimateTimeMinutes: ",estimateTimeMinutes)
        return estimateTimeMinutes

    class Meta:
        model = mod.Pedido
        exclude = ['usuario']
        depth = 3

class PedidoDetalleSerializer(ModelSerializer):
    class Meta:
        model = mod.DetallePedido
        depth = 4

class PedidoDetalleMovilSerializer(ModelSerializer):
    class Meta:
        model = mod.DetallePedido

#JY, Clase usada para serializar ingredintes de platos que no se desean
class PedidoDetalleIngrSerializer(ModelSerializer):
    class Meta:
        model = mod.Modificardetallepedido

#Transportista
class TransportistaSerializer(ModelSerializer):
    class Meta:
        model = mod.Transportista
        #depth = 2

class UsuarioSerializer(ModelSerializer):
    class Meta:
        model = mod.User
        # exclude = ['password', 'last_login', 'is_superuser', 'is_staff', 'groups']
        depth = 2

class PerfilSerializer(ModelSerializer):
    class Meta:
        model = mod.Perfil

class PerfilUsuarioSerializer(ModelSerializer):
    class Meta:
        model = mod.Perfil
        depth = 2

class PedidoEstadoSerializer(ModelSerializer):
    deliveryTime = serializers.SerializerMethodField('computeDeliveryTime')

    def computeDeliveryTime(self, pedido):
        currentDate = datetime.now()
        print("currentDate: ", currentDate)
        estimateTimeMinutes = timedelta(0)
        if pedido.id_estado_id >= 1 & pedido.id_estado_id < 6:
            if pedido.tiempo == None:
                pedido.tiempo = 0
            # print(pedido.fecha)
            orderDate = (pedido.fecha.replace(tzinfo=None) + timedelta(minutes=pedido.tiempo) + timedelta(minutes=5))
            # print(orderDate)
            # Pedido no tine zona horia por lo que esta por defecto 00
            # Se resta -5 para que trabaje con la zona horario de Ecuador
            orderDate = orderDate - timedelta(hours=5)
            print("orderDate: ", orderDate)
            estimateTimeMinutes = orderDate - currentDate
            if estimateTimeMinutes < timedelta(0):
                estimateTimeMinutes = timedelta(0)
        # devuleve tiempo restante en segundos
        print("estimateTimeMinutes: ", estimateTimeMinutes)
        return estimateTimeMinutes
    class Meta:
        model = mod.Pedido


# Platos con categorias
class PlatoCategoriaSerializer(ModelSerializer):
    platos = PlatoSerializer(many=True)
    class Meta:
        model = mod.PlatoCategoria


class CategoriaSerializer(ModelSerializer):
    class Meta:
        model = mod.PlatoCategoria

#JY, Serializar Categorias, API Rest
class CategoriasPSerializer(ModelSerializer):
    class Meta:
        model = mod.Categoria



# pedido desde el móvil
class PedidoMovilSerializer(ModelSerializer):
    class Meta:
        model = mod.Pedido


class DireccionesSerializer(ModelSerializer):
    class Meta:
        model = mod.Direccion


# API REST Sugerencias

class SugerenciasSerializer(ModelSerializer):
    class Meta:
        model = mod.Sugerencia


# API REST Administracion Transportistas

class TranspCoordSerializer(ModelSerializer):
    class Meta:
        model = mod.Coordenadas
        depth = 2

# API REST de APP Proveedor
class Plato2Serializer(ModelSerializer):
    class Meta:
        model = mod.Plato
