from spaapp import models as mod
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
#*from django.db import connection
from django.contrib.auth.management import create_permissions
#from django.db.utils import IntegrityError
#from psycopg2 import IntegrityError
#from django.contrib.auth.models import DoesNotExist

#def make_permissions(apps, schema_editor, with_create_permissions=True):
#def make_permissions(**kwargs):
def make_permissions(apps, schema_editor):
    apps.models_module = True
    create_permissions(apps, verbosity=0)
    apps.models_module = None

    #Group = apps.get_model("auth", "Group")
    #Group = ContentType.objects.get_for_model()
    #Permission = apps.get_model("auth", "Permission")
    tipo_contenido_perfil = ContentType.objects.get_for_model(mod.Perfil)
    tipo_contenido_transp = ContentType.objects.get_for_model(mod.Transportista)
    tipo_contenido_client = ContentType.objects.get_for_model(mod.Cliente)
    tipo_contenido_direcc = ContentType.objects.get_for_model(mod.Direccion)
    tipo_contenido_provee = ContentType.objects.get_for_model(mod.Proveedor)

    try:
        rpcr = Permission.objects.get_or_create(
            name="Read Profile Client Restricted", content_type=tipo_contenido_perfil, codename='rpcr')
        rpta = Permission.objects.get_or_create(
            name="Read Profile Transportista Acceso Total", content_type=tipo_contenido_perfil, codename='rpta')
        rtr = Permission.objects.get_or_create(
            name="Read Transportista Restricted", content_type=tipo_contenido_transp, codename='rtr')
        rrc = Permission.objects.get_or_create(
            name="Read Client Restricted", content_type=tipo_contenido_client, codename='rrc')
        rdcr = Permission.objects.get_or_create(
            name="Read Direccion Cliente Restricted", content_type=tipo_contenido_direcc, codename='rdcr')
        esCliente = Permission.objects.get_or_create(
            name="Es Cliente", content_type=tipo_contenido_client, codename='esCliente')
        esTransportista = Permission.objects.get_or_create(
            name="Es Transportista", content_type=tipo_contenido_transp, codename='esTransportista')
        esProveedor = Permission.objects.get_or_create(
            name="Es Proveedor", content_type=tipo_contenido_provee, codename='esProveedor')
    except Permission.DoesNotExist:
        '''if with_create_permissions:
            # Manually run create_permissions
            assert not getattr(apps, 'models_module', None)
            apps.models_module = True
            create_permissions(apps, verbosity=0)
            apps.models_module = None
            return make_permissions(
                apps, schema_editor, with_create_permissions=False)
        else:
            raise'''
        print("No existen los permisos estandares de Django")
        return
        #raise Permission.DoesNotExist

    cliente = Group.objects.get_or_create(name="Cliente")
    cliente_perms = cliente[0].permissions.all()
    cliente_no_target_perms = []
    cliente_target_perms = [rpta, rtr, esCliente]
    for perm in cliente_perms:
        for target_perm in cliente_target_perms:
            if perm == target_perm[0]:
                cliente_no_target_perms.append(target_perm[0])
                cliente_target_perms.remove(target_perm)
                break
    for target_perm in cliente_target_perms:
        cliente[0].permissions.add(target_perm[0])
    cliente[0].save()

    proveedor = Group.objects.get_or_create(name="Proveedor")
    proveedor_perms = proveedor[0].permissions.all()
    proveedor_no_target_perms = []
    proveedor_target_perms = [rpta, rpcr, rtr, rrc, rdcr, esProveedor]
    for perm in proveedor_perms:
        for target_perm in proveedor_target_perms:
            if perm == target_perm[0]:
                proveedor_no_target_perms.append(target_perm[0])
                proveedor_target_perms.remove(target_perm)
                break
    for target_perm in proveedor_target_perms:
        proveedor[0].permissions.add(target_perm[0])
    proveedor[0].save()

    transportista = Group.objects.get_or_create(name="Transportista")
    transportista_perms = transportista[0].permissions.all()
    transportista_no_target_perms = []
    transportista_target_perms = [rpcr, rrc, rdcr, esTransportista]
    for perm in transportista_perms:
        for target_perm in transportista_target_perms:
            if perm == target_perm[0]:
                transportista_no_target_perms.append(target_perm[0])
                transportista_target_perms.remove(target_perm)
                break
    for target_perm in transportista_target_perms:
        transportista[0].permissions.add(target_perm[0])
    transportista[0].save()

