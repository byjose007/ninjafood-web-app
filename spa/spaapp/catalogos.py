from spaapp import models as mod
from spaapp import constantes

def llenar_catalogos(apps, schema_editor):
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTADO_PEDIDO) # cuando se pide
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTADO_PROCESO) # cuando accepta el proveedor/sucursal
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTADO_TRANSPORTISTA) # cuando accepta el transportista al envio
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTADO_ENVIADO) # cuando se entrega al transportista
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTADO_ENTREGADO) # cuando se entrega al cliente
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTADO_PROBLEMA_PROVEEDOR) # cuando hay problema en el surcursal y no se puede hacer el plato
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTADO_PROBLEMA_TRANSPORTISTA) # cuando hay problema con el transportista
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTADO_QUEJA) # cuando el cliente no esta satisfecho
    mod.CatalogoEstado.objects.get_or_create(estado=constantes.ESTOY_FUERA)  # cuando el transportista esta fuera de la casa
