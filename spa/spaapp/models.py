# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from . import constantes as const
#####################################################################################
from PIL import Image
from io import StringIO, BytesIO

from django.core.files.base import ContentFile
#####################################################################################

class ActividadComercial(models.Model):
    id_actividad = models.AutoField(primary_key=True)
    tipo_actividad = models.CharField(max_length=100)
    detalles = models.CharField(max_length=250)

    def __str__(self):
        return '%s' % (self.tipo_actividad)

    class Meta:
        verbose_name = 'Actividad Comercial'
        verbose_name_plural = 'Actividades Comerciales'


CHOICE_GENERO = (('Masculino', 'Masculino'), ('Femenino', 'Femenino'), ('Otro', 'Otro'),)

import re

class Perfil(models.Model):
    id_perfil = models.AutoField(primary_key=True)
    cedula = models.CharField(max_length=11, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    telefono = models.CharField(max_length=10, blank=True, null=True)
    genero = models.CharField(null=True, blank=True, max_length=10, choices=CHOICE_GENERO)
    ruta_foto = models.ImageField(null=False, blank=True, upload_to='usuarioFoto', verbose_name='Ruta de foto')
    cuentaVerif = models.NullBooleanField(default=False)
    id_usuario = models.OneToOneField(User)
    sim_dispositivo = models.CharField(max_length=30,null=True, blank=True)

    def __str__(self):
        return '%s' % (self.id_usuario)

    def social(self):
        if re.match("https?://", self.ruta_foto.name) is not None:
            return True
        else:
            return False
    # social.boolean = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if re.match("https?://", self.ruta_foto.name) is None:
            image = Image.open(self.ruta_foto).convert('RGB')
            # img = image.resize((200, 200), Image.ANTIALIAS)

            new_img = BytesIO()
            image.save(new_img, format='JPEG', quality=70)
            temp_name = self.ruta_foto.name
            self.ruta_foto.delete(save=False)

            self.ruta_foto.save(
                temp_name,
                content=ContentFile(new_img.getvalue()),
                save=False
            )

        super(Perfil, self).save()

    class Meta:
        verbose_name_plural = 'Perfiles'

    # def __unicode__(self):
    #     return '%s' % (self.ruta_foto)


###########################  PRUEBA    #############################################

def perfil_existe(pk):
    user = User.objects.get(pk=pk)
    try:
        perfil = user.perfil
    except Perfil.DoesNotExist:
        perfil = Perfil(user=user)
        perfil.save()
    return

def perfil_crear(**kwargs):
    Perfil.objects.get_or_create(user=kwargs['user'])




################################################################

class Categoria(models.Model):
    id_categoria = models.AutoField(primary_key=True)
    categoria = models.CharField(max_length=150)
    detalles = models.CharField(max_length=250)
    ruta_foto = models.ImageField(null=True, blank=True, upload_to='categorias', verbose_name='Ruta de foto')

    class Meta:
        verbose_name_plural = 'categorias'
        #managed = True


    def __str__(self):
        return '%s' % (self.categoria)

    def __unicode__(self):
        return '%s' % (self.categoria)


class Proveedor(models.Model):
    id_proveedor = models.AutoField(primary_key=True )
    nombre_comercial = models.CharField(max_length=100)
    telefono = models.CharField(max_length=10)
    telefono_contacto = models.CharField(max_length=10, blank=True, null=True)
    prioridad = models.IntegerField()
    me_gusta = models.IntegerField(blank=True, null=True)
    id_actividad = models.ForeignKey(ActividadComercial)
    id_categoria = models.ForeignKey(Categoria)
    id_perfil = models.ForeignKey(Perfil,related_name='proveedores')
    razon_social = models.CharField(max_length=20)

    def __str__(self):
        return '%s' % (self.nombre_comercial)

    class Meta:
        verbose_name_plural = 'Proveedores'

    def __unicode__(self):
        return '%s' % (self.id_proveedor)


class Ciudad(models.Model):
    id_ciudad = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=80)
    descripcion = models.CharField(max_length=250)

    def __str__(self):
        return '%s' % (self.nombre)

    class Meta:
        verbose_name_plural = 'Ciudades'


class Direccion(models.Model):
    id_direccion = models.AutoField(primary_key=True)
    calle_principal = models.CharField(max_length=50)
    calle_secundaria = models.CharField(max_length=50)
    numero_casa = models.CharField(max_length=10, blank=True, null=True)
    latitud = models.CharField(max_length=100, blank=True, null=True)
    longitud = models.CharField(max_length=100, blank=True, null=True)
    referencia = models.CharField(max_length=250)
    id_perfil = models.ForeignKey(Perfil, blank=True, null=True)

    def __str__(self):
        return '%s - %s. %s. Número de casa: %s' % (self.calle_principal, self.calle_secundaria, self.referencia, self.numero_casa)

    class Meta:
        verbose_name_plural = 'Direcciones'


class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    fechainicio_suscripcion = models.DateField()
    fecha_pago = models.DateField(blank=True, null=True)
    detalles_adicionales = models.CharField(max_length=250)
    pedidos_activos = models.NullBooleanField()
    id_perfil = models.ForeignKey(Perfil)

    def __str__(self):
        return '%s' % (self.id_perfil)

    class Meta:
        verbose_name_plural = 'Clientes'



class Sucursal(models.Model):
    id_sucursal = models.AutoField(primary_key=True)
    nombre_sucursal = models.CharField(max_length=100)
    razon_social = models.CharField(max_length=200)
    telefono = models.CharField(max_length=10)
    telefono_contacto = models.CharField(max_length=10, blank=True, null=True)
    dias_atencion = models.CharField(max_length=80)
    hora_atencion = models.CharField(max_length=50)
    estado = models.IntegerField(blank=True, null=True)
    descripcion = models.CharField(max_length=300, blank=True, null=True)
    precio_envio = models.FloatField(blank=True, null=True)
    prioridad = models.IntegerField(blank=True, null=True)
    activo = models.BooleanField()
    hora_pico = models.TimeField()
    me_gusta = models.IntegerField()
    visitas = models.IntegerField(default=5)
    tiene_transporte = models.BooleanField(default=False)
    id_ciudad = models.ForeignKey(Ciudad)
    id_direccion = models.ForeignKey(Direccion,blank=True, null=True)
    id_proveedor = models.ForeignKey(Proveedor, related_name='sucursales')
    id_perfil = models.ForeignKey(Perfil, null=True)
    #id_menu = models.ForeignKey(Menu)


    def __str__(self):
        return '%s' % (self.nombre_sucursal)

    class Meta:
        verbose_name_plural = 'Sucursales'


    def __unicode__(self):
        return '%s' % (self.estado)


class Menu(models.Model):
    id_menu = models.AutoField(primary_key=True)
    id_proveedor = models.ForeignKey(Proveedor)
    id_sucursal = models.ForeignKey(Sucursal, blank=True, null=True)

    def __str__(self):
        return "%s" % (self.id_proveedor)

    class Meta:
        verbose_name_plural = 'Menu'


class Contrato(models.Model):
    id_contrato = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=20)
    detalles = models.CharField(max_length=250)
    vigencia = models.IntegerField()
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField(db_column='fechaFin')  # Field name made lowercase.
    valor = models.FloatField()
    ruta_contrato = models.CharField(max_length=100, blank=True, null=True)
    id_proveedor = models.ForeignKey(Proveedor, blank=True, null=True)
    id_sucursal = models.ForeignKey(Sucursal, blank=True, null=True)

    def __str__(self):
        return '%s - %s' % (self.codigo, self.vigencia)


class Transportista(models.Model):
    id_transportista = models.AutoField(primary_key=True)
    tipo_transporte = models.CharField(max_length=20)
    tipo_servicio = models.CharField(max_length=50)
    id_perfil = models.ForeignKey(Perfil, blank=True, null=True)
    sim_dispositivo = models.CharField(max_length=30, null=True, blank=True)
    cedula = models.CharField(max_length=20, null=True, blank=True)
    estado = models.CharField(max_length=20, null=True, blank=True)
    nombre = models.CharField(max_length=60, null=True, blank=True)
    numero = models.CharField(max_length=20, null=True, blank=True)# número de celular
    saldo = models.FloatField()
    def __str__(self):
        return '%s' % (self.id_perfil)

    class Meta:
        verbose_name_plural = 'Trasportistas'


class Coordenadas(models.Model):
    id_coordenadas = models.AutoField(primary_key=True)
    latitud = models.CharField(max_length=100)
    longitud = models.CharField(max_length=100)
    id_transportista = models.ForeignKey(Transportista, null=True)
    fecha = models.DateTimeField(null=True)
    estado = models.IntegerField(null=True)
    def __str__(self):
        return '%s - %s' % (self.latitud, self.longitud)

    class Meta:
        verbose_name_plural = 'Coordenadas'
        verbose_name = 'Coordenada'


class TipoPago(models.Model):
    id_tipo_pago = models.AutoField(primary_key=True)
    tipo_pago = models.CharField(max_length=50)
    observaciones = models.CharField(max_length=250)

    def __str__(self):
        return '%s' % (self.tipo_pago)


class TipoPedido(models.Model):
    id_tipo_pedido = models.AutoField(primary_key=True)
    tipo_pedido = models.CharField(max_length=50)
    observaciones = models.CharField(max_length=250)

    def __str__(self):
        return '%s' % (self.tipo_pedido)


class CatalogoEstado(models.Model):
    id_estado = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % self.estado

    class Meta:
        verbose_name_plural = "Catalogo Estados"


class Pedido(models.Model):
    id_pedido = models.AutoField(primary_key=True)
    fecha = models.DateTimeField()
    id_estado = models.ForeignKey(CatalogoEstado, null=True, blank=True, on_delete=models.CASCADE)
    observaciones = models.CharField(max_length=250,null=True,blank=True)
    valor = models.FloatField()
    tiempo = models.IntegerField(null=True)
    usuario = models.ForeignKey(User,null=True,blank=True,on_delete=models.CASCADE)
    notificado = models.BooleanField(default=False)
    notificadoPersistente = models.CharField(max_length=1, default=const.ESTADO_NOTIFICACION_INCICIAL)
    id_direccion = models.ForeignKey(Direccion,null=True,blank=True,on_delete=models.CASCADE)
    id_tipo_pago = models.ForeignKey(TipoPago,null=True,blank=True,on_delete=models.CASCADE)
    id_tipo_pedido = models.ForeignKey(TipoPedido,null=True,blank=True,on_delete=models.CASCADE)
    id_trasportista = models.ForeignKey(Transportista,null=True,blank=True,on_delete=models.CASCADE)
    id_sucursal = models.ForeignKey(Sucursal,null=True,blank=True,on_delete=models.CASCADE, related_name='pedidos')

    def __str__(self):
        return '%s - %s - %s - %s - %s' % (self.fecha, self.id_estado, self.valor, self.id_sucursal, self.usuario)

    class Meta:
        verbose_name_plural = "Pedidos"
        managed = True


class PlatoCategoria(models.Model):
    id_platocategoria= models.AutoField(primary_key=True)
    categoria = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % (self.categoria)


class Plato(models.Model):
    id_plato = models.AutoField(primary_key=True)
    nombre_producto = models.CharField(max_length=200)
    ruta_foto = models.ImageField(null=False, blank=True, upload_to='menuFoto', verbose_name='Ruta de foto')
    descripcion = models.CharField(max_length=200)
    cantidad_diaria = models.IntegerField(null=False)
    venta_aproximada = models.IntegerField(null=False)
    hora_mayor_venta = models.TimeField(null=False)
    cantidad_mayor_venta = models.IntegerField(null=False)
    existencia_actual = models.IntegerField(null=True)
    id_menu = models.ManyToManyField(Menu)
    id_platocategoria = models.ForeignKey(PlatoCategoria, related_name='platos')
    activo = models.BooleanField(default=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        image = Image.open(self.ruta_foto).convert('RGB')
        #img = image.resize((200, 200), Image.ANTIALIAS)

        new_img = BytesIO()
        image.save(new_img, format= 'JPEG', quality=50)
        temp_name = self.ruta_foto.name
        self.ruta_foto.delete(save=False)

        self.ruta_foto.save(
            temp_name,
            content = ContentFile(new_img.getvalue()),
            save=False
        )
        super(Plato, self).save()

    def __str__(self):
        return '%s' % (self.nombre_producto)

    class Meta:
        verbose_name_plural = 'Platos'


class Tamanio(models.Model):
    id_tamanio = models.AutoField(primary_key=True)
    tamanio = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % (self.tamanio)

    class Meta:
        verbose_name_plural = 'Tamaños'


class TamanioPlato(models.Model):
    id_tamanioplato = models.AutoField(primary_key=True)
    id_plato = models.ForeignKey(Plato, related_name='tamanio')
    id_tamanio = models.ForeignKey(Tamanio)
    valor = models.FloatField()
    valor_oferta = models.FloatField(null=True, blank=True)
    activo = models.BooleanField()

    def __str__(self):
        return '%s : %s - %s - %s ' % (self.id_plato, self.id_tamanio, self.valor,self.valor_oferta)

    def __unicode__(self):
        return '%s' % (self.id_tamanioplato)


class DetallePedido(models.Model):
    id_detalle = models.AutoField(primary_key=True)
    valor_oferta = models.FloatField(null=True, blank=True)
    valor = models.FloatField()
    tamanio = models.ForeignKey(TamanioPlato,null=True,blank=True,on_delete=models.CASCADE)
    cantidad_solicitada = models.IntegerField()
    id_pedido = models.ForeignKey(Pedido,null=True,blank=True,on_delete=models.CASCADE)
    id_plato = models.ForeignKey(Plato, null=True,blank=True,on_delete=models.CASCADE)


    def __str__(self):
        return '%s' % (self.id_pedido)

    class Meta:
        verbose_name = 'Detalle de pedido'
        verbose_name_plural = 'Detalles de pedido'


class Ingrediente(models.Model):
    id_ingrediente = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    id_proveedor = models.ForeignKey(Proveedor)

    def __str__(self):
        return '%s' % (self.nombre)


class HorarioTransportista(models.Model):
    id_horario = models.AutoField(primary_key=True)
    dias = models.CharField(max_length=80)
    horas = models.CharField(max_length=50)
    id_transportista = models.ForeignKey(Transportista)

    def __str__(self):
        return '%s - %s' % (self.dias, self.horas)

    class Meta:
        verbose_name = 'Horario de transportista'
        verbose_name_plural = 'Horarios de transportista'


class Modificardetallepedido(models.Model):
    id_modificardetallepedido = models.AutoField(primary_key=True)
    id_detallepedido = models.ForeignKey(DetallePedido, blank=True, null=True)
    id_ingrediente_quitar = models.ForeignKey(Ingrediente, blank=True, null=True)


class PlatoIngrediente(models.Model):
    id_plato_ingrediente = models.AutoField(primary_key=True)
    id_plato = models.ForeignKey(Plato)
    id_ingrediente = models.ForeignKey(Ingrediente)
    requerido = models.BooleanField()
    horafecha = models.DateTimeField()

    def __str__(self):
        return '%s' % (self.id_plato)


class RedSocial(models.Model):
    id_red_social = models.AutoField(primary_key=True)
    nombre_red_social = models.CharField(max_length=50)
    cuenta_persona = models.CharField(max_length=100)
    id_proveedor = models.ForeignKey(Proveedor)

    def __str__(self):
        return '%s - %s' % (self.nombre_red_social, self.cuenta_persona)

    class Meta:
        verbose_name = 'Red social'
        verbose_name_plural = 'Redes sociales'

class Valoracion(models.Model):
    id_valoracion = models.AutoField(primary_key=True)
    observaciones = models.CharField(max_length=200, null=True)
    usuario = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
    id_pedido = models.ForeignKey(Pedido, null=True)
    valoracion_general = models.IntegerField(null=True)
    id_sucursal = models.ForeignKey(Sucursal, null=True)
    valor_sucursal = models.IntegerField(null=True)

    def __str__(self):
        return '%s' % (self.valoracion_general)

    class Meta:
        verbose_name_plural = 'Valoraciones'
        managed = True


CHOICE_SUGERENCIA = (('Aplicacion_web', 'Aplicación Web'), ('Aplicacion_movil', 'Aplicación Móvil'),('nueva_funcionalidad', 'Nueva Funcionalidad'), ('Sucursal', 'Proveedor'),
                     ('Servicio_transportista', 'Servicio de Entrega'), ('Pedido', 'Pedido'), ('Otro', 'Otro'))
class Sugerencia(models.Model):
    id_sugerencia = models.AutoField(primary_key=True)
    id_sucursal = models.ForeignKey(Sucursal, null=True, blank=True)
    id_pedido = models.ForeignKey(Pedido, null=True, blank=True)
    observacion = models.CharField(max_length=200, null=True)
    usuario = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
    estado = models.IntegerField(null=True)
    fecha = models.DateTimeField(blank=True, null=True)
    tipo = models.CharField(null=True, blank=True, max_length=25, choices=CHOICE_SUGERENCIA)

    def __str__(self):
        return '%s' % (self.observacion)

    class Meta:
        verbose_name_plural = 'Sugerencias'
        managed = True


class UserSurcursal(models.Model):
    id_user = models.ForeignKey(User, null=False)
    id_sucursal = models.ForeignKey(Sucursal, null=False)

    def __str__(self):
        return '%s - %s' % (self.id_user.username, self.id_sucursal.nombre_sucursal)

    class Meta:
        verbose_name_plural = "Usuarios Sucursales"
        managed = True
