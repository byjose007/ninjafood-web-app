from unittest.case import _UnexpectedSuccess

############################################
from django.contrib.auth.models import User, Group
from django.views.generic import CreateView, UpdateView, ListView
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.tokens import default_token_generator
from django.db.models.query_utils import Q
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template import loader
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from spa.settings import DEFAULT_FROM_EMAIL
from django.views.generic import *
from django.core.mail import EmailMultiAlternatives
from django.utils.timezone import localtime
from django.db.models import F
from django.contrib import messages
############################################
from django.core.urlresolvers import resolve

from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import permission_required, login_required
from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.http import HttpResponseRedirect
from django.template import RequestContext
from spaapp.form import *
from spaapp.models import *
from spaapp import constantes
from . import models as mod
from . import cryptoaux as ca
from . import form as forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from dateutil.parser import parse
from django.template.loader import render_to_string
from django.core import serializers
from django.http import HttpResponse, Http404
from django.http import JsonResponse
import json

from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from rest_framework.viewsets import ModelViewSet
from rest_framework import generics
from . import serializers as ser
# from django.views.decorators.cache import never_cache

# import for D.A. -- INICIO
import bcrypt
from django.db.models import Avg
from spa import settings
from twilio.rest import TwilioRestClient
import datetime as dt
from datetime import datetime, timedelta
from geopy.geocoders import Nominatim
from django.db import connection
# import for D.A. -- FIN

import time
from dateutil import tz
import itertools
from datetime import datetime
from django.utils import timezone
from django.utils.timezone import localtime

from django.core.urlresolvers import reverse


from proveedorApp import views as pviews
from ubicacionApp import views as uviews
from django.contrib import auth

from django.db.models import Count, Sum

nombre_app = "NinjaFood"

from twisted.internet import reactor
import threading

def func1(x):
    minutosAlerta=3
    today = datetime.today()
    pedidos=mod.Pedido.objects.filter(fecha__year=today.year, fecha__month=today.month, fecha__day=today.day, id_estado__estado=constantes.ESTADO_PEDIDO)
    for pedidosNoti in pedidos:
        fechaPed=pedidosNoti.fecha
        fechaMax=fechaPed+timedelta(minutes=minutosAlerta)
        if(timezone.make_aware(today, timezone.get_default_timezone())>fechaMax):
            enviarCorreoAlerta(pedidosNoti)
        else:
            pass
    reactor.callLater(30, func1, "se esta ejecutando cada 30 segundo")
    #indicamos nuevamente que llame a la funcion func1...

def algo():
    reactor.disconnectAll()
    reactor.callLater(30, func1, "inicio")
    reactor.run(installSignalHandlers=False)

def enviarCorreoAlerta(pedido):
    telefonoSuc = pedido.id_sucursal.telefono
    userPro = mod.UserSurcursal.objects.get(id_sucursal=pedido.id_sucursal)
    #nombre = mod.Perfil.objects.get(id_usuario=userPro.id_user)
    peril = mod.Perfil.objects.get(id_usuario_id=pedido.usuario.pk)

    c = {
        'titulo': "¡Pedido NO Atendido!",
        'restaurante': userPro.id_sucursal.nombre_sucursal,
        'fecha': pedido.fecha,
        'cliente': pedido.usuario,
        'cedula': peril.cedula,
        'telefono': peril.telefono,
        'tipoEnvio': pedido.id_tipo_pedido.tipo_pedido,
        "valor": pedido.valor,
        "nombreProvee": userPro.id_user,
        "direccionPro": pedido.id_sucursal.id_direccion,
        "telSucur": telefonoSuc,
        'domain': 'goodappetit.com',
        'site_name': 'GoodAppetit',
        'protocol': 'https',
    }

    html_content = 'home/nuevoPedidoNotificationEmail.html'
    email = loader.render_to_string(html_content, c)
    email_html = loader.render_to_string(html_content, c)
    correos = []
    correos = settings.correosEnviarPedidosRealizados
    usSucu = mod.UserSurcursal.objects.get(id_sucursal=userPro.id_sucursal)
    correoPro=usSucu.id_user.email
    if correoPro in correos:
        pass
    else:
        correos.append(correoPro)
    send_mail('PEDIDO NO ATENDIDO | GoodApettit', email, DEFAULT_FROM_EMAIL, correos, fail_silently=False,
              html_message=email_html)

dowloand=threading.Thread(target=algo)
dowloand.start()


# Permisos: Todos tienen Acesso
def home(request):
    perfil = ""
    pedidosNu = ""
    profileType = None
    users=request.user
    if users.is_authenticated():
        pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
        user = request.user
        if user.has_perm("spaapp.esTransportista"):
            print('es transportista')
            # TODO poner imagen por defecto para todos los perfiles de los transportistas
            perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
        else:
            print('Cliente o Proveedor')
            perfil = mod.Perfil.objects.get(id_usuario=request.user)

        # JY, Determinar que tipo de Usuario esta Autenticado
        profileType = validarTipoUsuario(request)
        # FIN

    '''if request.user.is_authenticated():
        try:
            p = mod.Perfil.objects.get(id_usuario=request.user)
            c = mod.Cliente.objects.get(id_perfil=p)
            #return redirect(perfil)
        except ObjectDoesNotExist:
            return render(request, "home/home.html", {})'''
    return render(request, "home/home.html", {'perfil': perfil, 'pedidosNu': pedidosNu, 'profileType': profileType})
    '''if not request.user.is_authenticated():
        return render(request, "home/home.html", {})
    else:
        return HttpResponseRedirect("/perfil")'''


# Permisos: Acesso si no esta logeado
def login(request):
    if request.user.is_authenticated():  # Si el usuario ya esta logueado, no hay sentido mostrarle el logeo
        '''try:
            p = mod.Perfil.objects.get(id_usuario=request.user)
            c = mod.Cliente.objects.get(id_perfil=p)
        except ObjectDoesNotExist:
            return redirect(home)
        # TODO Arreglar perfil no encontrado:
        #return redirect(perfil)  # por lo tanto redirecionamos a su perfil'''
        return redirect(restaurantes)
    else:
        if request.method == 'POST':
            form = forms.FormLogin(request.POST)
            if form.is_valid():
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]
                user = authenticate(username=username, password=password)
                if user is None:
                    users = User.objects.filter(email=username)
                    if len(users) > 0:
                        user = authenticate(username=users[0].username, password=password)
                if user is not None:
                    if user.is_active:
                        auth_login(request, user)
                        if request.user.has_perm("spaapp.esAdmin"):
                            return redirect(home)
                        elif request.user.has_perm("spaapp.esTransportista"):
                            return redirect(uviews.soyTransportista)
                        elif request.user.has_perm("spaapp.esProveedor"):
                            return redirect(pviews.soyProveedor)
                        elif request.user.has_perm("spaapp.esCliente"):
                            return redirect(restaurantes)
                        else:
                            return redirect(home)
                            # raise PermissionDenied("No tienes ningun rol!")
                    else:
                        # Return a 'disabled account' error message
                        print("User %s has valid credentials, but a disabled account." % username)
                else:
                    # Return an 'invalid login' error message.
                    print("Invalid Login for \"%s\"" % username)
            else:
                print("Invalid Form Data")
            return render(request, "home/login.html", {'form': form, "nombre_app": nombre_app})
        else:
            form = forms.FormLogin()
            '''try:
                pt = mod.Perfil.objects.filter(id_usuario=request.user)
                ct = mod.Transportista.objects.get(id_perfil=pt[0])
                return redirect("/soyTransportista")
            except ObjectDoesNotExist:
                return redirect(home)
            # Redirect to a success page.
            #print("Valid User %s" % username)
                return redirect(home)'''
        return render(request, "home/login.html", {'form': form, "nombre_app": nombre_app})


# Permisos: Debe ser logeado
@login_required()
def perfil(request):
    template = 'home/login.html'
    if not request.user.is_authenticated():
        template = 'home/login.html'
    else:
        try:
            p = mod.Perfil.objects.get(id_usuario=request.user)
            c = mod.Cliente.objects.get(id_perfil=p)
            template = 'home/clientePerfil.html'
        except ObjectDoesNotExist:
            return redirect(perfilRead)
    return render_to_response('home/login.html', context_instance=RequestContext(request, locals()))


# Permisos: Debe ser superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def proveedorCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.ProveedorForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect(sucursalCreate)
    else:
        formulario = forms.ProveedorForm()
    ctx['formulario'] = formulario
    return render_to_response('home/proveedorCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Todos tienen Acceso
def proveedorRead(request):
    proveedor = mod.Proveedor.objects.all()
    return render(request, 'home/proveedorRead.html', {'proveedor': proveedor})


# Permisos: Debe ser superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def proveedorDelete(request, pk):
    instance = get_object_or_404(mod.Proveedor, id_proveedor=pk)
    instance.delete()
    return redirect(proveedorRead)


# Permisos: Debe ser Proveedor (implementado)
#           Debe ser dueño del entidad
@permission_required("spaapp.esProveedor", raise_exception=True)
def proveedorUpdate(request, pk):
    ctx = {}
    proveedor = get_object_or_404(mod.Proveedor, id_proveedor=pk)
    perfil = proveedor.id_perfil
    usuario = perfil.id_usuario
    # print("usuario = ", usuario, " ", "request.user = ", request.user)
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        if request.method == 'POST':
            formulario2 = forms.ProveedorForm(request.POST, instance=proveedor)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(proveedorRead)
        else:
            formulario2 = forms.ProveedorForm(instance=proveedor)
        ctx['formulario'] = formulario2
        return render_to_response('home/proveedorUpdate.html', ctx, context_instance=RequestContext(request))
    else:
        raise PermissionDenied('No tienes acceso a ese perfil')


# def clienteCreate(request):
#     ctx={}
#     if request.method == 'POST':
#         formulario=forms.ClienteForm(request.POST)
#         if formulario.is_valid():
#             formulario.save()
#             return redirect(clienteRead)
#     else:
#         formulario=forms.ClienteForm()
#     ctx['formulario']=formulario
#     return render_to_response('home/clienteCreate.html',ctx,context_instance=RequestContext(request))

# def clienteCreate(request):
#	return render_to_response("home/clienteCreate.html", context_instance = RequestContext(request, locals()))


# Permisos: Debe no estar logeado
def clienteRegister(request):
    if request.user.is_authenticated():
        return redirect(perfil)
    elif request.method == 'POST':
        nick = request.POST['nick']
        mail = request.POST['mail']
        password = request.POST['password']
        usuario = mod.User.objects.create(username=nick, email=mail, password=ca.get_hashed_password(password))
        mod.Cliente.objects.create(idUsuario=usuario, suscripcionActiva=False)
        mod.Perfil.objects.create(idUsuario=usuario)
        msj = "Usuario registrado exitosamente"
        return render_to_response("home/clienteCreate.html", context_instance=RequestContext(request, locals()))


# Permisos: Debe estar Logeado para tener Acceso (implementado)
#           Un cliente solo debe tener acceso a su propio perfil
#           Proveedores y Transportistas deben tener acceso minimo
#               y solo a aquellos clientes con quien han tenido pedidos
#                   (nombre del permiso en la bdd: rrc)
#           TODO falta establecer en que consiste la politica de acceso minimo
@login_required()
def clienteRead(request):
    usuario = request.user
    if usuario.has_perm("spaapp.esAdmin"):
        cliente = mod.Cliente.objects.all()
    elif usuario.has_perm("rcc"):
        if usuario.has_perm("spaapp.esTransportias"):  # Cuando transportistas son individuos y no un adminstrador total
            # Habrá que eliminar esa clasula para no dejarles con esos permisos
            # Totales
            pedidos = mod.Pedido.objects.all()
        else:
            perfil = mod.Perfil.objects.filter(id_usuario=usuario)
            if usuario.has_perm("esProveedor"):
                proveedor = mod.Proveedor.objects.filter(id_perfil=perfil)
                sucursales = mod.Sucursal.objects.filter(id_proveedor=proveedor)
                pedidos = mod.Pedido.objects.filter(id_sucursal=sucursales)
            elif usuario.has_perm("esTransportista"):
                transportista = mod.Transportista.objects.filter(id_perfil=perfil)
                pedidos = mod.Pedido.objects.filter(id_transportista=transportista)
        usuarios_pedidos = mod.User.objects.filter(id=pedidos.id_usuario)
        perfiles = mod.Perfil.objects.filter(id_usuario=usuarios_pedidos)
        cliente = mod.Cliente.objects.filter(id_perfil=perfiles)
    elif usuario.has_perm("esCliente"):
        perfil = mod.Perfil.objects.filter(id_usuario=usuario)
        cliente = mod.Cliente.objects.filter(id_perfil=perfil)
    else:
        raise PermissionDenied("Permiso Denegado")
    return render(request, 'home/clienteRead.html', {'cliente': cliente})


# Permisos: Debe ser super usuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def clienteDelete(request, pk):
    instance = get_object_or_404(mod.Cliente, idCliente=pk)
    instance.delete()
    return redirect(clienteRead)


# def clienteUpdate(request,pk):
#     ctx={}
#     cliente = get_object_or_404(mod.Cliente, idCliente=pk)
#     if request.method == 'POST':
#         formulario2=forms.ClienteForm(request.POST,instance=cliente)
#         if formulario2.is_valid():
#             formulario2.save()
#             return redirect(clienteRead)
#     else:
#         formulario2=forms.ClienteForm(instance=cliente)
#     ctx['formulario']=formulario2
#     return render_to_response('home/clienteUpdate.html',ctx,context_instance=RequestContext(request))

# Permisos: Debe ser Cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def clienteUpdate2(request, pk):
    u = get_object_or_404(mod.User, pk=pk)
    p = mod.Perfil.objects.filter(id_usuario=u)
    c = mod.Cliente.objects.filter(id_perfil=p)
    if not p:
        p = mod.Perfil.objects.create(id_usuario=request.user)
        c = mod.Cliente.objects.create(id_perfil=p, suscripcion_activa=False)
    else:
        p = mod.Perfil.objects.get(id_usuario=u)
        c = mod.Cliente.objects.get(id_perfil=p)

    if request.method == 'POST':
        formularioCliente = forms.ClienteForm(request.POST, instance=c)
        formularioPerfil = forms.PerfilForm(request.POST, request.FILES, instance=p)
        formularioUser = forms.UserForm(request.POST, instance=u)
        if formularioCliente.is_valid() and formularioPerfil.is_valid() and formularioUser.is_valid():
            objCliente = formularioCliente.save(commit=False)
            objPerfil = formularioPerfil.save(commit=False)
            objUser = formularioUser.save(commit=False)
            objPerfil.id_usuario = request.user
            objPerfil.save()
            objCliente.suscripcion_activa = True
            objCliente.id_perfil = objPerfil
            # Perfil.objects.create(idUsuario=objUser.pk)
            # Cliente.objects.create(id_perfil=objUser.pk, suscripcionActiva=False)

            objCliente.save()
            objUser.save()
            # return redirect(clienteRead)
            return HttpResponseRedirect("/perfil")
    else:
        formularioUser = forms.UserForm(instance=u)
        formularioPerfil = forms.PerfilForm(instance=p)
        formularioCliente = forms.ClienteForm(instance=c)
    # ctx['formularioCliente']=formularioCliente
    # ctx['formularioCliente']=formularioCliente
    return render_to_response('home/clienteUpdate.html', context_instance=RequestContext(request, locals()))


'''
@permission_required("esProveedor", raise_exception=True)
def menuCreate(request):
    ctx={}
    if request.method == 'POST':
        formulario=forms.MenuForm(request.POST, request.FILES)
        if formulario.is_valid():
            formulario.save()
            return redirect(menuRead)
    else:
        formulario=forms.MenuForm()
    ctx['formulario']=formulario
    return render_to_response('home/menuCreate.html',ctx,context_instance=RequestContext(request))


def menuRead(request):
    menu = mod.Menu.objects.all()
    return render(request, 'home/menuRead.html', {'menu':menu})


@permission_required("esProveedor", raise_exception=True)
def menuDelete(request,pk):
    instance=get_object_or_404(mod.Menu, id_menu=pk)
    instance.delete()
    return redirect(menuRead)


@permission_required("esProveedor", raise_exception=True)
def menuUpdate(request,pk):
    ctx={}
    menu = get_object_or_404(mod.Menu, id_menu=pk)
    if request.method == 'POST':
        formulario2=forms.MenuForm(request.POST, request.FILES,instance=menu)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(menuRead)
    else:
        formulario2=forms.MenuForm(instance=menu)
    ctx['formulario']=formulario2
    return render_to_response('home/menuUpdate.html',ctx,context_instance=RequestContext(request))
'''


# Permisos: Debe ser Proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def ingredienteCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.IngredienteForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect(ingredienteRead)
    else:
        formulario = forms.IngredienteForm()
    ctx['formulario'] = formulario
    return render_to_response('home/ingredienteCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe estar logeado para tener acceso
@login_required()
def ingredienteRead(request):
    ingrediente = mod.Ingrediente.objects.all()
    return render(request, 'home/ingredienteRead.html', {'ingrediente': ingrediente})


# Permisos: Debe ser proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def ingredienteDelete(request, pk):
    instance = get_object_or_404(mod.Ingrediente, id_ingrediente=pk)
    instance.delete()
    return redirect(ingredienteRead)


# Permisos: Debe ser proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def ingredienteUpdate(request, pk):
    ctx = {}
    ingrediente = get_object_or_404(mod.Ingrediente, id_ingrediente=pk)
    if request.method == 'POST':
        formulario2 = forms.IngredienteForm(request.POST, request.FILES, instance=ingrediente)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(ingredienteRead)
    else:
        formulario2 = forms.IngredienteForm(instance=ingrediente)
    ctx['formulario'] = formulario2
    return render_to_response('home/ingredienteUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def ciudadCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.CiudadForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect(ciudadRead)
    else:
        formulario = forms.CiudadForm()
    ctx['formulario'] = formulario
    return render_to_response('home/ciudadCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Todos tiene Acceso
def ciudadRead(request):
    ciudad = mod.Ciudad.objects.all()
    return render(request, 'home/ciudadRead.html', {'ciudad': ciudad})


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def ciudadDelete(request, pk):
    instance = get_object_or_404(mod.Ciudad, id_ciudad=pk)
    instance.delete()
    return redirect(ciudadRead)


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def ciudadUpdate(request, pk):
    ctx = {}
    ciudad = get_object_or_404(mod.Ciudad, id_ciudad=pk)
    if request.method == 'POST':
        formulario2 = forms.CiudadForm(request.POST, instance=ciudad)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(ciudadRead)
    else:
        formulario2 = forms.CiudadForm(instance=ciudad)
    ctx['formulario'] = formulario2
    return render_to_response('home/ciudadUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def actividadCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.ActividadForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect(actividadRead)
    else:
        formulario = forms.ActividadForm()
    ctx['formulario'] = formulario
    return render_to_response('home/actividadCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser logeado
@login_required()
def actividadRead(request):
    actividad = mod.ActividadComercial.objects.all()
    return render(request, 'home/actividadRead.html', {'actividad': actividad})


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def actividadDelete(request, pk):
    instance = get_object_or_404(mod.ActividadComercial, id_actividad=pk)
    instance.delete()
    return redirect(actividadRead)


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def actividadUpdate(request, pk):
    ctx = {}
    actividad = get_object_or_404(mod.ActividadComercial, id_actividad=pk)
    if request.method == 'POST':
        formulario2 = forms.ActividadForm(request.POST, instance=actividad)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(actividadRead)
    else:
        formulario2 = forms.ActividadForm(instance=actividad)
    ctx['formulario'] = formulario2
    return render_to_response('home/actividadUpdate.html', ctx, context_instance=RequestContext(request))


## Permisos: Debe ser proveedor
# @permission_required("spaapp.esProveedor", raise_exception=True)
# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def categoriaCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.CategoriaForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect(categoriaRead)
    else:
        formulario = forms.CategoriaForm()
    ctx['formulario'] = formulario
    return render_to_response('home/categoriaCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Todos tienen Acceso
def categoriaRead(request):
    categoria = mod.Categoria.objects.all()
    return render(request, 'home/categoriaRead.html', {'categoria': categoria})


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def categoriaDelete(request, pk):
    instance = get_object_or_404(mod.Categoria, id_categoria=pk)
    instance.delete()
    return redirect(categoriaRead)


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def categoriaUpdate(request, pk):
    ctx = {}
    categoria = get_object_or_404(mod.Categoria, id_categoria=pk)
    if request.method == 'POST':
        formulario2 = forms.CategoriaForm(request.POST, instance=categoria)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(categoriaRead)
    else:
        formulario2 = forms.CategoriaForm(instance=categoria)
    ctx['formulario'] = formulario2
    return render_to_response('home/categoriaUpdate.html', ctx, context_instance=RequestContext(request))

################# GESTION DE DIRECCIONES ################# INICIO
# Permisos: Debe ser logeado
@login_required()
def direccionCreate(request):
    ctx = {}
    profileType = None
    user = request.user.pk  # idusuario
    perfil = mod.Perfil.objects.get(id_usuario=user)
    if request.method == 'GET':
        formulario = forms.direccionCreateGD(initial={'user': perfil.pk})
    elif request.method == 'POST':
        formulario = forms.direccionCreateGD(request.POST, initial={'user': perfil.pk})
        direccion = formulario.is_valid()
        if direccion:
            print(formulario)
            formulario.save()
            return redirect(gestionDireccion)
    else:
        formulario = forms.direccionCreateGD()
    profileType = validarTipoUsuario(request)
    ctx['formulario'] = formulario
    ctx['perfil'] = perfil
    ctx['profileType'] = profileType
    return render_to_response('home/direccionCreate.html', ctx, context_instance=RequestContext(request))


############################# OBTENER CALLES ############################# INICIO
# Permisos: Debe ser logeado
@login_required()
def getStreet(request):
    if request.method == "POST" and request.user.is_authenticated():
        latitud = request.POST['lat']
        longitud = request.POST['lon']

        geolocator = Nominatim()
        lugar = geolocator.reverse(longitud+","+latitud)
        #si existe la calle
        if not lugar is None:
            calles=lugar.address
            print(calles)
            detalles=lugar.raw
            #print(detalles)
            #dataC = serializers.serialize('json', calles)
            return HttpResponse(calles, content_type="application/json")
        else:
            a=False
            return HttpResponse(a, content_type="application/json")

################################# OBTENER CALLES ########################### FIN


# Permisos: Debe ser logeado
@login_required()
def gestionDireccion(request):
    profileType = None
    usuario = request.user
    user = request.user.pk  # idusuario
    perfil = mod.Perfil.objects.get(id_usuario=user)
    if usuario.is_authenticated():
        ######### tipo de usuario ###################
        user = request.user
        if user.has_perm("spaapp.esTransportista"):
            print('es transportista')
            # TODO poner imagen por defecto para todos los perfiles de los transportistas
            perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
        else:
            print('Cliente o Proveedor')
            perfil = mod.Perfil.objects.get(id_usuario=request.user)

        # JY, Determinar que tipo de Usuario esta Autenticado
        profileType = validarTipoUsuario(request)
        ######### tipo de usuario ###################


        perf = mod.Perfil.objects.get(id_usuario=usuario)
        direccion=mod.Direccion.objects.filter(id_perfil=perf)
    return render(request, "home/gestionDireccion.html", {"direccion":direccion, "perfil":perfil, 'profileType': profileType})
################# GESTION DE DIRECCIONES ################# FIN



# Permisos: Todos pueden ver direcciones de proveedores
#           Proveedores y Transporistas pueden ver de forma
#               restringida las direcciones de clientes para
#               los cuales han hecho pedidos
#               (permiso en bdd: rdcr)
#           TODO establecer politicas de como se debe registrigir los datos de clientes y sus direcciones
#               en el contexto de pedidos
def direccionRead(request):
    # proveedores = mod.Proveedor.objects.all()
    sucursales = mod.Sucursal.objects.all()
    direccion = sucursales.id_direccion
    # direccion = mod.Direccion.objects.all()
    usuario = request.user
    perfil = mod.Perfil.objects.filter(id_usuario=usuario)
    if usuario.has_perm("spaapp.rdcr"):
        if usuario.has_perm("esProveedor"):
            proveedor = mod.Proveedor.objects.filter(id_perfil=perfil)
            sucursales = mod.Sucursal.objects.filter(id_proveedor=proveedor)
            pedidos = mod.Pedido.objects.filter(id_sucursal=sucursales)
        elif usuario.has_perm("esTransportista"):
            transportista = mod.Transportista.objects.filter(id_perfil=perfil)
            pedidos = mod.Pedido.objects.filter(id_transportista=transportista)
        for p in pedidos:
            direccion.append(p.id_direccion)
    if usuario.has_perm("esCliente"):
        direcciones = mod.Direccion.objects.filter(id_perfil=perfil)
        for d in direcciones:
            direccion.append(d)
    return render(request, 'home/direccionRead.html', {'direccion': direccion})


# Permisos: Se puede borrar sus propios direcciones
# implementar logica para prevenir la eliminacion de direcciones de otras personas/entidades
@login_required()
def direccionDelete(request, pk):
    usuario = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario)
    instance = get_object_or_404(mod.Direccion, id_direccion=pk)
    if usuario.has_perm("spaapp.esAdmin") or instance.id_perfil == perfil:
        instance.delete()
    else:
        raise PermissionDenied("Permiso denegado")
    return redirect(gestionDireccion)


# Permisos: Se puede actualizar sus propios direcciones
# implementar logica para prevenir la actualizacion de direcciones de otras personas/entidades
@login_required()
def direccionUpdate(request, pk):
    ctx = {}
    usuario = request.user
    perfil = mod.Perfil.objects.get(id_usuario=usuario)
    direccion = get_object_or_404(mod.Direccion, id_direccion=pk)
    if usuario.has_perm("spaapp.esAdmin") or direccion.id_perfil == perfil:
        if request.method == 'POST':
            formulario2 = forms.direccionUpdateGD(request.POST)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(gestionDireccion)
        elif request.method == 'GET':
            cp=direccion.calle_principal
            cs=direccion.calle_secundaria
            lt=direccion.latitud
            lo=direccion.longitud
            nc=direccion.numero_casa
            re=direccion.referencia

            formulario2 = forms.direccionUpdateGD(initial={'cPrincipal': cp, 'cSecundaria': cs, 'latitud': lt,
                                                           'longitud': lo, 'nroCasa': nc, 'referencia': re,
                                                           'direccion':direccion, 'user':pk})
            #pass
    else:
        raise PermissionDenied("Permiso denegado")
    profileType = validarTipoUsuario(request)
    ctx['formulario'] = formulario2
    ctx['direccion'] = direccion
    ctx['perfil'] = perfil
    ctx['profileType'] = profileType
    return render_to_response('home/direccionUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def sucursalCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.SucursalForm(request.POST)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(sucursalRead)
    else:
        formulario = forms.SucursalForm()
    ctx['formulario'] = formulario
    return render_to_response('home/sucursalCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Todos tienen acceso
def sucursalRead(request):
    sucursal = mod.Sucursal.objects.all()
    return render(request, 'home/sucursalRead.html', {'sucursal': sucursal})


# Permisos: Debe ser proveedor (implementado)
#           Debe ser dueño del sucrosal
@permission_required("spaapp.esProveedor", raise_exception=True)
def sucursalDelete(request, pk):
    instance = get_object_or_404(mod.Sucursal, id_sucursal=pk)
    proveedor = instance.id_proveedor
    perfil = proveedor.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esProveedor") or request.user == usuario:
        instance.delete()
    else:
        raise PermissionDenied("Permiso denegado")
    return redirect(sucursalRead)


# Permisos: Debe ser proveedor (implementado)
#           Debe ser dueño del sucrosal
@permission_required("spaapp.esProveedor", raise_exception=True)
def sucursalUpdate(request, pk):
    ctx = {}
    sucursal = get_object_or_404(mod.Sucursal, id_sucursal=pk)
    proveedor = sucursal.id_proveedor
    perfil = proveedor.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esProveedor") or request.user == usuario:
        if request.method == 'POST':
            formulario2 = forms.SucursalForm(request.POST, instance=sucursal)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(sucursalRead)
        else:
            formulario2 = forms.SucursalForm(instance=sucursal)
            id_perfil = sucursal.id_proveedor.id_perfil.id_perfil
            ctx['id_perfil'] = id_perfil
    else:
        raise PermissionDenied("Permiso Denegado")
    ctx['formulario'] = formulario2
    return render_to_response('home/sucursalUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def detallepedidoCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.DetallePedidoForm(request.POST)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(detallepedidoRead)
    else:
        formulario = forms.DetallePedidoForm()
    ctx['formulario'] = formulario
    return render_to_response('home/detallepedidoCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Proveedor o Cliente y que le pertenece el detalle pedido
# todo implementar nuevo permiso en la bdd
def detallepedidoRead(request):
    detallepedido = mod.DetallePedido.objects.all()
    return render(request, 'home/detallepedidoRead.html', {'detallepedido': detallepedido})


# Permisos: Debe ser Cliente y ser dueño del detalle pedido
#           verificar que el detalle pedido es del cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def detallepedidoDelete(request, pk):
    instance = get_object_or_404(mod.DetallePedido, id_detalle=pk)
    pedido = instance.id_pedido
    usuario = pedido.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        instance.delete()
    else:
        raise PermissionDenied("Permiso Denegado")
    return redirect(detallepedidoRead)


# Permisos: Debe ser Cliente y ser dueño del detalle pedido
#           verificar que el detalle pedido es del cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def detallepedidoUpdate(request, pk):
    ctx = {}
    detallepedido = get_object_or_404(mod.DetallePedido, id_detalle=pk)
    pedido = detallepedido.id_pedido
    usuario = pedido.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        if request.method == 'POST':
            formulario2 = forms.DetallePedidoForm(request.POST, instance=detallepedido)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(detallepedidoRead)
        else:
            formulario2 = forms.DetallePedidoForm(instance=detallepedido)
        ctx['formulario'] = formulario2
    else:
        raise PermissionDenied("Permiso Denegado")
    return render_to_response('home/detallepedidoUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def tipopagoCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.TipoPagoForm(request.POST)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(tipopagoRead)
    else:
        formulario = forms.TipoPagoForm()
    ctx['formulario'] = formulario
    return render_to_response('home/tipopagoCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Todos tienen acceso
def tipopagoRead(request):
    tipopago = mod.TipoPago.objects.all()
    return render(request, 'home/tipopagoRead.html', {'tipopago': tipopago})


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def tipopagoDelete(request, pk):
    instance = get_object_or_404(mod.TipoPago, id_tipo_pago=pk)
    instance.delete()
    return redirect(tipopagoRead)


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def tipopagoUpdate(request, pk):
    ctx = {}
    tipopago = get_object_or_404(mod.TipoPago, id_tipo_pago=pk)
    if request.method == 'POST':
        formulario2 = forms.TipoPagoForm(request.POST, instance=tipopago)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(tipopagoRead)
    else:
        formulario2 = forms.TipoPagoForm(instance=tipopago)
    ctx['formulario'] = formulario2
    return render_to_response('home/tipopagoUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def tipopedidoCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.TipoPedidoForm(request.POST)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(tipopedidoRead)
    else:
        formulario = forms.TipoPedidoForm()
    ctx['formulario'] = formulario
    return render_to_response('home/tipopedidoCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Todos tienen acceso
def tipopedidoRead(request):
    tipopedido = mod.TipoPedido.objects.all()
    return render(request, 'home/tipopedidoRead.html', {'tipopedido': tipopedido})


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def tipopedidoDelete(request, pk):
    instance = get_object_or_404(mod.TipoPedido, id_tipo_pedido=pk)
    instance.delete()
    return redirect(tipopedidoRead)


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def tipopedidoUpdate(request, pk):
    ctx = {}
    tipopedido = get_object_or_404(mod.TipoPedido, id_tipo_pedido=pk)
    if request.method == 'POST':
        formulario2 = forms.TipoPedidoForm(request.POST, instance=tipopedido)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(tipopedidoRead)
    else:
        formulario2 = forms.TipoPedidoForm(instance=tipopedido)
    ctx['formulario'] = formulario2
    return render_to_response('home/tipopedidoUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def transportistaCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.TransportistaForm(request.POST)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(transportistaRead)
    else:
        formulario = forms.TransportistaForm()
    ctx['formulario'] = formulario
    return render_to_response('home/transportistaCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser logeado
#           Un transportista solo puede ver sus propios datos
#           Un cliente y proveedor pueden ver datos de transportistas
#               con quienes han realizado pedidos
@login_required()
def transportistaRead(request):
    usuario = request.user
    perfil = mod.Perfil.objects.filter(id_usuario=usuario)
    if usuario.has_perm("spaapp,esAdmin"):
        transportista = mod.Transportista.objects.all()
    elif usuario.has_perm("spaapp.esTransportista"):
        transportista = mod.Transportista.objects.filter(id_perfil=perfil)
    else:
        if usuario.has_perm("spaapp.esProveedor"):
            proveedor = mod.Proveedor.objects.filter(id_perfil=perfil)
            sucursal = mod.Sucursal.objects.filter(id_proveedor=proveedor)
            pedidos = mod.Pedido.objects.filter(id_sucursal=sucursal)
            transportista = pedidos.id_transportista
        elif usuario.has_perm("spaapp.esCliente"):
            cliente = mod.Cliente.objects.filter(id_perfil=perfil)
            pedidos = mod.Pedido.objects.filter(id_cliente=cliente)
            transportista = pedidos.id_transportista
        else:
            raise PermissionDenied("Permiso Denegado")
    return render(request, 'home/transportistaRead.html', {'transportista': transportista})


# Permisos: Debe ser Administrador/Superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def transportistaDelete(request, pk):
    instance = get_object_or_404(mod.Transportista, id_transportista=pk)
    instance.delete()
    return redirect(transportistaRead)


# Permisos: Debe ser Transportista (implementado)
#           Debe ser dueño de los datos
@permission_required("spaapp.esTransportista", raise_exception=True)
def transportistaUpdate(request, pk):
    ctx = {}
    transportista = get_object_or_404(mod.Transportista, id_transportista=pk)
    perfil = transportista.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        if request.method == 'POST':
            formulario2 = forms.TransportistaForm(request.POST, instance=transportista)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(transportistaRead)
        else:
            formulario2 = forms.TransportistaForm(instance=transportista)
        ctx['formulario'] = formulario2
    return render_to_response('home/transportistaUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def horariotransportistaCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.HorarioTransportistaForm(request.POST)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(horariotransportistaRead)
    else:
        formulario = forms.HorarioTransportistaForm()
    ctx['formulario'] = formulario
    return render_to_response('home/horariotransportistaCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Transportista
#           debe ser dueño del horario
@permission_required("spaapp.esTransportista", raise_exception=True)
def horariotransportistaRead(request):
    usuario = request.user
    perfil = mod.Perfil.objects.filter(id_usuario=usuario)
    transportista = mod.Transportista.objects.filter(id_perfil=perfil)
    if usuario.has_perm("spaapp.esAdmin"):
        horariotransportista = mod.HorarioTransportista.objects.all()
    else:
        horariotransportista = mod.HorarioTransportista.objects.filter(id_transportista=transportista)
    return render(request, 'home/horariotransportistaRead.html', {'horariotransportista': horariotransportista})


# Permisos: Debe ser Transportista
#           debe ser dueño del horario
@permission_required("spaapp.esTransportista", raise_exception=True)
def horariotransportistaDelete(request, pk):
    instance = get_object_or_404(mod.HorarioTransportista, id_horario=pk)
    transportista = instance.id_transportista
    perfil = transportista.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        instance.delete()
    else:
        raise PermissionDenied("Permiso Denegado")
    return redirect(horariotransportistaRead)


# Permisos: Debe ser Transportista
#           debe ser dueño del horario
@permission_required("spaapp.esTransportista", raise_exception=True)
def horariotransportistaUpdate(request, pk):
    ctx = {}
    horariotransportista = get_object_or_404(mod.HorarioTransportista, id_horario=pk)
    transportista = horariotransportista.id_transportista
    perfil = transportista.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        if request.method == 'POST':
            formulario2 = forms.HorarioTransportistaForm(request.POST, instance=horariotransportista)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(horariotransportistaRead)
        else:
            formulario2 = forms.HorarioTransportistaForm(instance=horariotransportista)
        ctx['formulario'] = formulario2
    else:
        raise PermissionDenied("Permiso Denegado")
    return render_to_response('home/horariotransportistaUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def pedidoCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.PedidoForm(request.POST)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(pedidoRead)
    else:
        formulario = forms.PedidoForm()
    ctx['formulario'] = formulario
    return render_to_response('home/pedidoCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Logeado
#           si es cliente o proveedor  puede ver sus pedidos
#           si es transportista puede ver de forma restringida los pedidos que tiene
#           TODO definir como se debe restringir los datos del pedido que se presenta al transportista
@login_required()
def pedidoRead(request):
    usuario = request.user
    if usuario.has_perm("esAdmin"):
        pedido = mod.Pedido.objects.all()
    else:
        perfil = mod.Perfil.objects.filter(id_usuario=usuario)
        if usuario.has_perm("spaapp.esProveedor"):
            proveedor = mod.Proveedor.objects.filter(id_perfil=perfil)
            sucursal = mod.Sucursal.objects.filter(id_proveedor=proveedor)
            pedido = mod.Pedido.objects.filter(id_sucursal=sucursal)
        elif usuario.has_perm("spaapp.esTransportista"):
            transportista = mod.Transportista.objects.filter(id_perfil=perfil)
            pedido = mod.Pedido.objects.filter(id_transportista=transportista)
        elif usuario.has_perm("spaapp.esCliente"):
            cliente = mod.Cliente.objects.filter(id_perfil=perfil)
            pedido = mod.Pedido.objects.filter(id_cliente=cliente)
        else:
            raise PermissionDenied("Permiso Denegado")
    return render(request, 'home/pedidoRead.html', {'pedido': pedido})


# Permisos: Debe ser Cliente
#           Debe ser dueño del pedido
#           cliente solo puede eliminar pedido antes de que sea aceptado por el proveedor
@permission_required("spaapp.esCliente", raise_exception=True)
def pedidoDelete(request, pk):
    instance = get_object_or_404(mod.Pedido, id_pedido=pk)
    usuario = instance.id_cliente
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        if request.user.has_perm("spaapp.esAdmin") or instance.estado == "pendiente":
            instance.delete()
        else:
            raise PermissionDenied("Ya no se puede cancelar el pedido")
    else:
        raise PermissionDenied("Permiso Denegado")
    return redirect(pedidoRead)


# Permisos: Debe ser Cliente
#           Debe ser dueño del pedido
#           cliente solo puede actualizar pedido antes de que sea aceptado por el proveedor
@permission_required("spaapp.esCliente", raise_exception=True)
def pedidoUpdate(request, pk):
    ctx = {}
    pedido = get_object_or_404(mod.Pedido, id_pedido=pk)
    usuario = pedido.id_cliente
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        if request.user.has_perm("spaapp.esAdmin") or pedido.estado == "pendiente":
            if request.method == 'POST':
                formulario2 = forms.PedidoForm(request.POST, instance=pedido)
                if formulario2.is_valid():
                    formulario2.save()
                    return redirect(pedidoRead)
            else:
                formulario2 = forms.PedidoForm(instance=pedido)
            ctx['formulario'] = formulario2
        else:
            raise PermissionDenied("Ya no se puede cancelar el pedido")
    else:
        raise PermissionDenied("Permiso Denegado")
    return render_to_response('home/pedidoUpdate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Logeado
@login_required()
def coordenadasCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.CoordenadasForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect(coordenadasRead)
    else:
        formulario = forms.CoordenadasForm()
    ctx['formulario'] = formulario
    return render_to_response('home/coordenadasCreate.html', ctx, context_instance=RequestContext(request))


# Permisos: Debe ser Logeado
#           TODO, solo pueden leer coordenadas los involucrados
@login_required()
def coordenadasRead(request):
    coordenadas = mod.Coordenadas.objects.all()
    return render(request, 'home/coordenadasRead.html', {'coordenadas': coordenadas})


# Permisos: Debe ser superusuario
@permission_required("spaapp.esAdmin", raise_exception=True)
def coordenadasDelete(request, pk):
    instance = get_object_or_404(mod.Coordenadas, id_coordenadas=pk)
    instance.delete()
    return redirect(coordenadasRead)


# Permisos: Debe ser Logeado
#           TODO, solo pueden actualizar coordenadas suyos
@login_required()
def coordenadasUpdate(request, pk):
    ctx = {}
    coordenadas = get_object_or_404(mod.Coordenadas, id_coordenadas=pk)
    if request.method == 'POST':
        formulario2 = forms.CoordenadasForm(request.POST, instance=coordenadas)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(coordenadasRead)
    else:
        formulario2 = forms.CoordenadasForm(instance=coordenadas)
    ctx['formulario'] = formulario2
    return render_to_response('home/coordenadasUpdate.html', ctx, context_instance=RequestContext(request))


# TODO
# Permisos: Debe ser Cliente
#           TODO cliente debe haber hecho pedidos con ese proveedor
def valoracionCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.ValoracionForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect(valoracionRead)
    else:
        formulario = forms.ValoracionForm()
    ctx['formulario'] = formulario
    return render_to_response('home/valoracionCreate.html', ctx, context_instance=RequestContext(request))


# Permiso: Todos tienen acceso
def valoracionRead(request):
    valoracion = mod.Valoracion.objects.all()
    return render(request, 'home/valoracionRead.html', {'valoracion': valoracion})


# Permiso: Debe ser Cliente
#          si es cliente debe ser dueño de la valoración
@permission_required("spaapp.esCliente", raise_exception=True)
def valoracionDelete(request, pk):
    instance = get_object_or_404(mod.Valoracion, id_valoracion=pk)
    cliente = instance.id_cliente
    perfil = cliente.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or request.user == usuario:
        instance.delete()
    else:
        raise PermissionDenied("Permiso Denegado")
    return redirect(valoracionRead)


# Permiso: Debe ser Cliente
#          si es cliente debe ser dueño de la valoración
@permission_required("spaapp.esCliente", raise_exception=True)
def valoracionUpdate(request, pk):
    ctx = {}
    valoracion = get_object_or_404(mod.Valoracion, id_valoracion=pk)
    cliente = valoracion.id_cliente
    perfil = cliente.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or request.user == usuario:
        if request.method == 'POST':
            formulario2 = forms.ValoracionForm(request.POST, instance=valoracion)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(valoracionRead)
        else:
            formulario2 = forms.ValoracionForm(instance=valoracion)
        ctx['formulario'] = formulario2
    else:
        raise PermissionDenied("Permiso Denegado")
    return render_to_response('home/valoracionUpdate.html', ctx, context_instance=RequestContext(request))


# Permiso: Debe ser Proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def redsocialCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.RedSocialForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect(redsocialRead)
    else:
        formulario = forms.RedSocialForm()
    ctx['formulario'] = formulario
    return render_to_response('home/redsocialCreate.html', ctx, context_instance=RequestContext(request))


# Permiso: Toos tienen acceso
def redsocialRead(request):
    redsocial = mod.RedSocial.objects.all()
    return render(request, 'home/redsocialRead.html', {'redsocial': redsocial})


# Permiso: Debe ser Proveedor
#          debe ser dueño de esa entidad
@permission_required("spaapp.esProveedor", raise_exception=True)
def redsocialDelete(request, pk):
    instance = get_object_or_404(mod.RedSocial, id_red_social=pk)
    proveedor = instance.id_proveedor
    perfil = proveedor.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        instance.delete()
    else:
        raise PermissionDenied("Permiso Denegado")
    return redirect(redsocialRead)


# Permiso: Debe ser Proveedor
#          debe ser dueño de esa entidad
@permission_required("spaapp.esProveedor", raise_exception=True)
def redsocialUpdate(request, pk):
    ctx = {}
    redsocial = get_object_or_404(mod.RedSocial, id_red_social=pk)
    proveedor = redsocial.id_proveedor
    perfil = proveedor.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        if request.method == 'POST':
            formulario2 = forms.RedSocialForm(request.POST, instance=redsocial)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(redsocialRead)
        else:
            formulario2 = forms.RedSocialForm(instance=redsocial)
        ctx['formulario'] = formulario2
    else:
        raise PermissionDenied("Permiso Denegado")
    return render_to_response('home/redsocialUpdate.html', ctx, context_instance=RequestContext(request))


# Permiso: Debe ser Administrador
@permission_required("spaapp.esAdmin", raise_exception=True)
def contratoCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.ContratoForm(request.POST, request.FILES)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(contratoRead)
    else:
        formulario = forms.ContratoForm()
    ctx['formulario'] = formulario
    return render_to_response('home/contratoCreate.html', ctx, context_instance=RequestContext(request))


# Permiso: Debe ser Proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def contratoRead(request):
    contrato = mod.Contrato.objects.all()
    return render(request, 'home/contratoRead.html', {'contrato': contrato})


# Permiso: Debe ser Administrador
@permission_required("spaapp.esAdmin", raise_exception=True)
def contratoDelete(request, pk):
    instance = get_object_or_404(mod.Contrato, id_contrato=pk)
    instance.delete()
    return redirect(contratoRead)


# Permiso: Debe ser Administrador
@permission_required("spaapp.esAdmin", raise_exception=True)
def contratoUpdate(request, pk):
    ctx = {}
    contrato = get_object_or_404(mod.Contrato, id_contrato=pk)
    if request.method == 'POST':
        formulario2 = forms.ContratoForm(request.POST, request.FILES, instance=contrato)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(contratoRead)
    else:
        formulario2 = forms.ContratoForm(instance=contrato)
    ctx['formulario'] = formulario2
    return render_to_response('home/contratoUpdate.html', ctx, context_instance=RequestContext(request))


###### PROBANDO############################################

# TODO
class CrearPerfil(CreateView):
    def get(self, request, *args, **kwargs):
        form_class = forms.PerfilForm
        template_name = 'home/perfilCreate.html'
        model = mod.Perfil, User
        return render(request, template_name)

    def post(self, request, *args, **kwargs):
        model = mod.Perfil

        cedula = request.POST["cedula"]
        fecha_nacimiento = request.POST["fecha_nacimiento"]
        telefono = request.POST["telefono"]
        genero = request.POST["genero"]
        ruta_foto = request.POST["ruta_foto"]
        id_usuario = request.user
        sim_dispositivo = request.POST["sim_dispositivo"]

        perfil = mod.Perfil(cedula=cedula, fecha_nacimiento=fecha_nacimiento, telefono=telefono, genero=genero,
                            ruta_foto=ruta_foto, id_usuario=id_usuario)
        perfil.save()
        template_name = 'home/restaurantes.html'
        return render(request, template_name)


# TODO
class PerfilList(ListView):
    model = mod.Perfil
    template_name = '#'
    paginate_by = 2


# TODO
class PerfilUpdate(UpdateView):
    model = mod.Perfil
    form_class = forms.PerfilForm
    template_name = 'home/perfilCreate.html'
    success_url = reverse_lazy('spaapp:restaurantes')


'''''
class ActualizarPerfil(UpdateView):
    model = Perfil
    fields = ('cedula',
            'fecha_nacimiento',
            'telefono',
            'genero',
            'ruta_foto',
            'sim_dispositivo')

    def get(self, request, *args, **kwargs):
        perfil_existe(kwargs['pk'])
        return (super(ActualizarPerfil, self).get(self, request, *args, **kwards))

#############################################################
'''


# Permiso: Debe ser logeado
#          no debe tener un perfil
@login_required()
def perfilCreate(request):
    ctx = {}
    perfil = mod.Perfil.objects.filter(id_usuario=request.user)
    if perfil.exists():
        raise PermissionDenied("Ya existe su perfil")
    else:
        if request.method == 'POST':
            formulario = forms.PerfilForm(request.POST)
            if formulario.is_valid():
                formulario.save()
                return redirect(perfilRead)
        else:
            formulario = forms.PerfilForm()
        ctx['formulario'] = formulario
    return render_to_response('home/perfilCreate.html', ctx, context_instance=RequestContext(request))


# Permiso: Todos pueden ver perfiles de provedores
#          Proveedores y clientes pueden ver perfiles de Transportistas con los que interactuan
#          Transportistas pueden ver su perfil y de forma restringida lo de otros transportistas
#          TODO establecer politica de como se debe restringir informacion visible entre transportistas (con respecto
#               a sus perfiles)
#          Todos pueden ver su propio perfil
def perfilRead(request):
    usuario = request.user
    if usuario.has_perm("spaapp.esAdmin"):
        perfil = mod.Perfil.objects.all()
    else:
        miperfil = mod.Perfil.objects.filter(id_usuario=usuario)
        perfil = []
        perfil.append(miperfil)
        if usuario.has_perm("spaapp.esProveedor"):
            otros_proveedores = mod.Proveedor.objects.exclude(id_perfil=miperfil)
            perfil.append(otros_proveedores.id_perfil)
            mi_proveedor = mod.Proveedor.objects.filter(id_perfil=miperfil)
            mis_sucursales = mod.Sucursal.objects.filter(id_proveedor=mi_proveedor)
            pedidos = mod.Pedido.objects.filter(id_sucursal=mis_sucursales)
            perfil.append(pedidos.id_transportista.id_perfil)
        else:
            proveedores = mod.Proveedor.objects.all()
            perfil.append(proveedores.id_perfil)
            if usuario.has_perm("spaapp.esTransportista"):
                otros_transportistas = mod.Transportista.objects.exclude(id_perfil=miperfil)
                perfil.append(otros_transportistas.id_perfil)
            elif usuario.has_perm("spaapp.esCliente"):
                pedidos = mod.Pedido.objects.filter(usuario=usuario)
                perfil.append(pedidos.id_transportista.id_perfil)
    return render(request, 'home/perfilRead.html', {'perfil': perfil})


# Permiso: Debe ser Logeado
#          debe ser dueño de su perfil
@login_required()
def perfilDelete(request, pk):
    instance = get_object_or_404(mod.Perfil, id_perfil=pk)
    usuario = instance.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        instance.delete()
    else:
        raise PermissionDenied("Permiso Denegado")
    return redirect(perfilRead)


# Permiso: Debe ser Logeado
#          debe ser dueño de su perfil
# @login_required()
'''@permission_required("spaapp.esCliente", raise_exception=True)
def perfilUpdate(request, pk):
    ctx = {}
    user = request.user.pk
    perfil = get_object_or_404(mod.Perfil, id_usuario=user)

    if request.method == 'POST':
        formulario2 = forms.PerfilForm(request.POST, request.FILES, instance=perfil)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(restaurantes)
    else:
        formulario2 = forms.PerfilForm(instance=perfil)
        print(perfil.fecha_nacimiento)
        formulario2.initial['fecha_nacimiento']=str(perfil.fecha_nacimiento)
    ctx['formulario'] = formulario2
    ctx['perfil'] = perfil
    #else:
    #    raise PermissionDenied("Permiso Denegado")
    return render_to_response('home/perfilUpdate.html', ctx, context_instance=RequestContext(request))'''


# @permission_required("spaapp.esCliente", raise_exception=True)
# def perfilUpdate(request, pk):
#     ctx = {}
#     user = request.user.pk #idusuario
#     perfil = mod.Perfil.objects.get(id_usuario=user)
#     usuario = User.objects.get(id=user)
#
#     if request.method == 'POST':
#         formulario3 = forms.UpdateUserForm(request.POST, request.FILES, prefix='usuario')
#         formulario2 = forms.PerfilForm(request.POST, request.FILES, instance=perfil, prefix='perfil')
#
#         if formulario2.is_valid() and formulario3.is_valid():
#             formulario2.save()
#             formulario3.save()
#             return redirect(restaurantes)
#
#     else:
#         formulario2 = forms.PerfilForm(instance=perfil, prefix='perfil')
#         formulario2.initial['fecha_nacimiento']=str(perfil.fecha_nacimiento)
#         formulario3 = forms.UpdateUserForm(initial={'username':usuario.username, 'first_name':usuario.first_name,
#                                                     'last_name':usuario.last_name, 'email':usuario.email},
#                                            prefix='usuario')
#
#     profileType = validarTipoUsuario(request)
#
#     ctx['formulario2'] = formulario2
#     ctx['formulario3'] = formulario3
#     ctx['perfil'] = perfil
#     ctx['profileType'] = profileType
#
#
#     return render_to_response('home/perfilUpdate.html', ctx, context_instance=RequestContext(request))

# @permission_required("spaapp.esCliente", raise_exception=True)
# def perfilSocialUpdate{request, pk}:
#     # ctx['profileType'] = profileType
#     # ctx['pedidosNu'] = pedidosNu
#
#     return render_to_response('home/perfilUpdate.html', ctx, context_instance=RequestContext(request))

@permission_required("spaapp.esCliente", raise_exception=True)
def perfilUpdate(request, pk):
    ctx = {}
    user = request.user.pk  # idusuario
    perfil = mod.Perfil.objects.get(id_usuario=user)
    usuario = User.objects.get(id=user)
    pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()

    if request.method == 'POST':
        formulario3 = forms.UpdateUserNamesForm(request.POST, request.FILES, prefix='usuario')
        if(len(request.FILES))==0:
            print("foto no enviada")
            f2_v=False
        else:
            formulario2 = forms.PerfilForm(post_req=request.POST, files_req=request.FILES, prefix='perfil')
            f2_v = formulario2.is_valid()

        f3_v = formulario3.is_valid()

        if f2_v:
            formulario2.save()
            return HttpResponseRedirect('/perfil/Update/'+str(user)+'/')
        if f3_v:
            formulario3.save()
            #return redirect(perfilUpdate(pk=user))

    else:
        formulario2 = forms.PerfilForm(
            initial={'id_perfil': perfil.id_perfil, 'cedula': perfil.cedula, 'telefono': perfil.telefono,
                     #'genero': perfil.genero, 'ruta_foto': perfil.ruta_foto,
                     'ruta_foto': perfil.ruta_foto,
                     'fecha_nacimiento': perfil.fecha_nacimiento}, prefix='perfil')
        # formulario2.initial['fecha_nacimiento']=str(perfil.fecha_nacimiento)
        formulario3 = forms.UpdateUserNamesForm(initial={'username': usuario.username, 'genero': perfil.genero, 'first_name': usuario.first_name,
        #formulario3 = forms.UpdateUserNamesForm(initial={'username': usuario.username, 'genero': perfil.genero,
                                                         'last_name': usuario.last_name, 'email': usuario.email},
                                                prefix='usuario')
        ctx['formulario2'] = formulario2


    profileType = validarTipoUsuario(request)

    if (len(request.FILES)) != 0:
        ctx['formulario2'] = formulario2
    ctx['formulario3'] = formulario3
    ctx['perfil'] = perfil
    ctx['profileType'] = profileType
    ctx['pedidosNu'] = pedidosNu

    return render_to_response('home/perfilUpdate.html', ctx, context_instance=RequestContext(request))


@permission_required("spaapp.esCliente", raise_exception=True)
def contraseniaUpdateUser(request):
    ctx = {}
    usuario = request.user
    perfilid = mod.Perfil.objects.get(id_usuario=usuario)

    if(perfilid.social()):
        if(request.method=="POST"):
            formSocial=forms.setPasswordSocialForm(request.POST)
            f1=formSocial.is_valid()
            if(f1):
                formSocial.save()
                messages.success(request, 'Su contraseña se cambió correctamente')
                return redirect('/login')
        elif(request.method=="GET"):
            formSocial=forms.setPasswordSocialForm(initial={'username': usuario.username})
        ctx['formSocial'] = formSocial
    else:
        if request.method == 'POST':
            formulario2 = forms.UpdatePassProveedorForm(request.POST)

            if formulario2.is_valid():
                formulario2.save()
                messages.success(request, 'Su contraseña se cambió correctamente')
                return redirect('/login')
        else:

            formulario2 = forms.UpdatePassProveedorForm(initial={'username': usuario.username})
        ctx['formulario2'] = formulario2

    validar = validarTipoUsuario(request)

    ctx['perfil'] = perfilid
    ctx['profileType'] = validar

    return render_to_response('home/claveUser.html', ctx, context_instance=RequestContext(request))


# Permiso: Debe ser Proveedor
@permission_required("spaapp.esProveedor", raise_exception=True)
def platoCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.PlatoForm(request.POST, request.FILES)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(platoRead)
    else:
        formulario = forms.PlatoForm()
    ctx['formulario'] = formulario
    return render_to_response('home/platoCreate.html', ctx, context_instance=RequestContext(request))


# Permiso: Todos pueden ver platos
def platoRead(request):
    plato = mod.Plato.objects.all()
    return render(request, 'home/platoRead.html', {'plato': plato})


# Permiso: Debe ser Proveedor
#          El proveedor debe ser dueño de sus platos
@permission_required("spaapp.esProveedor", raise_exception=True)
def platoDelete(request, pk):
    instance = get_object_or_404(mod.Plato, id_plato=pk)
    menu = plato.id_menu
    proveedor = menu.id_proveedor
    perfil = proveedor.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        instance.delete()
    else:
        raise PermissionDenied("Permiso Denegado")
    return redirect(platoRead)


# Permiso: Debe ser Proveedor
#          El proveedor debe ser dueño de sus platos
@permission_required("spaapp.esProveedor", raise_exception=True)
def platoUpdate(request, pk):
    ctx = {}
    plato = get_object_or_404(mod.Plato, id_plato=pk)
    menu = plato.id_menu
    proveedor = menu.id_proveedor
    perfil = proveedor.id_perfil
    usuario = perfil.id_usuario
    if request.user.has_perm("spaapp.esAdmin") or usuario == request.user:
        if request.method == 'POST':
            formulario2 = forms.PlatoForm(request.POST, instance=plato)
            if formulario2.is_valid():
                formulario2.save()
                return redirect(platoRead)
        else:
            formulario2 = forms.PlatoForm(instance=plato)
        ctx['formulario'] = formulario2
    else:
        raise PermissionDenied("Permiso Denegado")
    return render_to_response('home/platoUpdate.html', ctx, context_instance=RequestContext(request))


# Permiso: Debe ser Administrador
@permission_required("spaapp.esAdmin", raise_exception=True)
def tamanioCreate(request):
    ctx = {}
    if request.method == 'POST':
        formulario = forms.TamanioForm(request.POST)
        if formulario.is_valid():
            print(formulario)
            formulario.save()
            return redirect(tamanioRead)
    else:
        formulario = forms.TamanioForm()
    ctx['formulario'] = formulario
    return render_to_response('home/tamanioCreate.html', ctx, context_instance=RequestContext(request))


# Permiso: Todos pueden ver tamanios
def tamanioRead(request):
    tamanio = mod.Tamanio.objects.all()
    return render(request, 'home/tamanioRead.html', {'tamanio': tamanio})


# Permiso: Debe ser Administrador
@permission_required("spaapp.esAdmin", raise_exception=True)
def tamanioDelete(request, pk):
    instance = get_object_or_404(mod.Tamanio, id_tamanio=pk)
    instance.delete()
    return redirect(tamanioRead)


# Permiso: Debe ser Administrador
@permission_required("spaapp.esAdmin", raise_exception=True)
def tamanioUpdate(request, pk):
    ctx = {}
    tamanio = get_object_or_404(mod.Tamanio, id_tamanio=pk)
    if request.method == 'POST':
        formulario2 = forms.TamanioForm(request.POST, instance=tamanio)
        if formulario2.is_valid():
            formulario2.save()
            return redirect(tamanioRead)
    else:
        formulario2 = forms.TamanioForm(instance=tamanio)
    ctx['formulario'] = formulario2
    return render_to_response('home/tamanioUpdate.html', ctx, context_instance=RequestContext(request))


'''
def usuarioCreate(request):
    ctx={}
    if request.method == 'POST':
        formulario3=forms.UsuarioForm(request.POST, request.FILES)
        if formulario3.is_valid():
            #print(request.POST)
            #formulario3.password = ca.get_hashed_password(formulario3.password)
            formulario3.save()
            return redirect(usuarioRead)
    else:
        formulario3=forms.UsuarioForm()
    ctx['formulario3']=formulario3
    return render_to_response('home/usuarioCreate.html',ctx,context_instance=RequestContext(request))

def usuarioRead(request):
    usuarios = mod.Usuario.objects.all()
    return render(request, 'home/usuarioRead.html', {'usuarios':usuarios})

def usuarioDelete(request,pk):
    instance = get_object_or_404(mod.Usuario, idUsuario=pk)
    instance.delete()
    return redirect(usuarioRead)

def mapa(request):
    return render(request, "home/mapa.html", {})

'''


# Permiso: Todos tienen acceso
class Menu(TemplateView):
    template_name = "home/clientePerfil.html"


# Views de Byron


# Permiso: Todos tienen acceso
def restaurantes(request):
    ############# NEW #############
    envio1 = []
    envio2 = []
    valoracion = mod.Valoracion.objects.all()
    sucursales = []
    sucursalesVAloracion = valoracion.values('id_sucursal').annotate(dcount=Sum('valoracion_general')).order_by(
        '-dcount')
    for valoracion in sucursalesVAloracion:
        # print (valoracion['id_sucursal'])
        sucursales.append(mod.Sucursal.objects.get(id_sucursal=valoracion['id_sucursal']))

    # sucursalesRes = mod.Sucursal.objects.all().order_by('estado').reverse()
    sucursalesRes = mod.Sucursal.objects.all().filter(estado=1).order_by('prioridad').reverse()
    sucursalesRes2 = mod.Sucursal.objects.all().filter(estado=0).order_by('prioridad').reverse()
    # Obtener todas las sucursales para despues recorer cuales tiene valoracion
    sucur = mod.Valoracion.objects.distinct('id_sucursal')
    sucursalesV = {}
    ############# NEW #############

    id_proveedor = 0
    proveedores = mod.Sucursal.objects.all().order_by('id_proveedor')
    for proveedor in proveedores:
        if id_proveedor != proveedor.id_proveedor:
            sucursales.append(proveedor)
            id_proveedor = proveedor.id_proveedor

    # print(sucursales)
    user = request.user

    perfil = ""
    pedidosNu = ""
    profileType = None
    socialR = False
    if user.is_authenticated():
        per = mod.Perfil.objects.get(id_usuario=user)
        valor=per.cedula
        exisID=False
        if(valor is not None):
            exisID=True
        if(request.method=="POST"):
            print("ENTRO AL METODO POST")
            formID=forms.cedulaValidoF(request.POST, initial={'perfil': per.pk})
            resformID=formID.is_valid()
            if(resformID):
                formID.save()
                socialR = False
            else:
                socialR = True

        elif(request.method=="GET"):
            cursor = connection.cursor()
            #res=cursor.execute("SELECT user_id FROM spaapp.social_auth_usersocialauth WHERE user_id= %s;", [users.pk])
            sql='''SELECT * FROM spaapp.social_auth_usersocialauth WHERE user_id=%d'''
            cursor.execute(sql%(user.pk))
            if(cursor.fetchall() and not exisID):
                socialR=True
                formID = forms.cedulaValidoF(initial={'perfil': per.pk})
            else:
                socialR=False

        pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()

        if user.has_perm("spaapp.esTransportista"):
            print('es transportista')
            # TODO poner imagen por defecto para todos los perfiles de los transportistas
            perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
        else:
            print('Cliente o Proveedor')
            perfil = mod.Perfil.objects.get(id_usuario=request.user)

        # JY, Determinar que tipo de Usuario esta Autenticado
        profileType = validarTipoUsuario(request)
        # print(profileType)
        # FIN

        """a = mod.Perfil.objects.filter(id_usuario = user.pk)
        if len(a) == 0 :
            perfil = mod.Perfil(cedula="1123456789", fecha_nacimiento="2016-01-01", telefono="555555", genero="genero",
                            ruta_foto="foto/foto.jpg", id_usuario=user.pk, sim_dispositivo="aaaaa")
            perfil.save()"""

    categoriasFijas = mod.Categoria.objects.all()[:7]
    categoriasDesplegables = mod.Categoria.objects.all()[7:]

    ########## MORE NEW #############
    oferta = []
    of = False
    for su in sucursalesRes:
        of = False
        listaMenu = mod.Menu.objects.filter(id_sucursal=su.id_sucursal)
        if listaMenu.count() == 1:
            menu = mod.Menu.objects.get(id_sucursal=su.id_sucursal)
            platos = mod.Plato.objects.filter(id_menu=menu)
            for plato in platos:
                if plato.activo == True:
                    tamanios = mod.TamanioPlato.objects.filter(id_plato=plato)
                    for tamanio in tamanios:
                        if tamanio.valor_oferta:
                            of = True
            if of:
                oferta.append(su.pk)

            con = mod.Valoracion.objects.filter(id_sucursal=su.id_sucursal).count()
            pro = mod.Valoracion.objects.filter(id_sucursal=su.id_sucursal).aggregate(avg=Avg('valoracion_general'))
            if pro['avg'] is None:
                sucursalesV = {'sucur': su, 'personas': con, 'puntuacion': '0'}
            else:
                sucursalesV = {'sucur': su, 'personas': con, 'puntuacion': str(pro['avg'])}
            envio1.append(sucursalesV)

    ########## MORE NEW #############



    ###################### NADIA #########################

    for su in sucursalesRes2:
        con = mod.Valoracion.objects.filter(id_sucursal=su.id_sucursal).count()
        pro = mod.Valoracion.objects.filter(id_sucursal=su.id_sucursal).aggregate(avg=Avg('valoracion_general'))
        if pro['avg'] is None:
            sucursalesV = {'sucur': su, 'personas': con, 'puntuacion': '0'}
        else:
            sucursalesV = {'sucur': su, 'personas': con, 'puntuacion': str(pro['avg'])}
        envio2.append(sucursalesV)

    ######################################################
    if(socialR):
        return render(request, "home/restaurantes.html",
                      {'sucursales': sucursales, 'categoriasFijas': categoriasFijas, 'puntuacion1': envio1,
                       'puntuacion2': envio2, 'perfil': perfil, 'pedidosNu': pedidosNu,
                       'categoriasDesplegables': categoriasDesplegables, 'profileType': profileType, 'oferta': oferta,
                       'rSocial': socialR, 'form': formID})
    else:
        return render(request, "home/restaurantes.html",
                      {'sucursales': sucursales, 'categoriasFijas': categoriasFijas, 'puntuacion1': envio1,
                       'puntuacion2': envio2, 'perfil': perfil, 'pedidosNu': pedidosNu,
                       'categoriasDesplegables': categoriasDesplegables, 'profileType': profileType, 'oferta': oferta,
                       'rSocial':socialR})

    # return render(request, "home/restaurantes.html", {'sucursales': sucursales,'categoriasFijas':categoriasFijas, 'puntuacion':envio, 'perfil':perfil,'pedidos':pedidos})

def verificarSucursales(request):
    id_sucursal = request.GET['sucursal']
    sucursal = mod.Sucursal.objects.get(pk=id_sucursal)
    proveedor = mod.Proveedor.objects.get(pk=sucursal.id_proveedor_id)
    sucursales = mod.Sucursal.objects.filter(id_proveedor=proveedor)
    datos = []
    cont = 0
    for suc in sucursales:
        if suc.estado == 1:
            datos.append(suc)
            cont = cont + 1
    if(cont > 1):
        datos.append(proveedor)
        datos = serializers.serialize('json', datos)
        return HttpResponse(datos)
    else :
        return HttpResponse("false", content_type="application/json")

# Permiso: Todos tienen acceso
def lista_menu(request, sucursal_id):

    menu_id = mod.Menu.objects.get(id_sucursal=sucursal_id).pk

    ############# VARIABLES NEW (VALORACION) #############
    envio1 = []
    sucursales = mod.Valoracion.objects.all()

    sucursalesRes = mod.Sucursal.objects.all()
    # Obtener todas las sucursales para despues recorer cuales tiene valoracion
    sucur = mod.Valoracion.objects.distinct('id_sucursal')
    sucursalesV = {}
    ############# VARIABLES NEW (VALORACION) #############
    ########## MORE NEW (VALORACION)#############
    con = mod.Valoracion.objects.filter(id_sucursal=sucursal_id).count()
    pro = mod.Valoracion.objects.filter(id_sucursal=sucursal_id).aggregate(avg=Avg('valoracion_general'))
    if pro['avg'] is None:
        sucursalesV = {'personas': con, 'puntuacion': '0'}
    else:
        sucursalesV = {'personas': con, 'puntuacion': str(pro['avg'])}
    envio1.append(sucursalesV)
    ########## MORE NEW (VALORACION) #############

    platos = mod.Plato.objects.filter(id_menu=menu_id, activo=True).order_by('id_platocategoria')
    sucursal = get_object_or_404(mod.Sucursal, id_sucursal=sucursal_id)
    tipo_pago = mod.TipoPago.objects.all()
    lista_categoria = mod.PlatoCategoria.objects.all()
    usuario = request.user
    # perfil = mod.Perfil.objects.get(id_usuario = usuario)
    pedidosNu = ""
    profileType = None
    if request.user.is_authenticated():
        pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
        usuario = request.user
        perfil = ''

        try:
            pt = mod.Perfil.objects.filter(id_usuario=usuario)
            ct = mod.Transportista.objects.get(id_perfil=pt[0])
            return redirect("/soyTransportista")
        except ObjectDoesNotExist:
            try:
                pr = mod.Perfil.objects.filter(id_usuario=usuario)
                p = mod.Proveedor.objects.get(id_perfil=pr[0])
                s = mod.Sucursal.objects.get(id_proveedor=p)
                return redirect("/proveedor")
            except ObjectDoesNotExist:
                perfil = mod.Perfil.objects.get(id_usuario=usuario)
                direcciones = mod.Direccion.objects.filter(id_perfil=perfil.id_perfil)
                id_perfil = perfil.id_perfil

        # JY, Determinar que tipo de Usuario esta Autenticado
        profileType = validarTipoUsuario(request)
        # FIN
    else:
        direcciones = ''
        id_perfil = 0
        # perfil = {'id_perfil' : None}

    '''lista_cat= []
    for p in platos:
        lista_cat.append(mod.PlatoCategoria.objects.get(id_platocategoria=p.id_platocategoria.id_platocategoria))

    copy_list = lista_cat
    set_categorias = list(set(lista_cat + copy_list))'''

    # =========== agrupar por categoría
    tipoPlatos = []
    categoriasPlatos = []
    for p in platos:
        categoriasPlatos.append(p.id_platocategoria)

    copy_list_categorias = categoriasPlatos
    set_categorias = list(set(categoriasPlatos + copy_list_categorias))

    for categoria in set_categorias:
        platosConCategoria = mod.Plato.objects.filter(id_menu=menu_id, id_platocategoria=categoria,
                                                      activo=True).order_by('id_platocategoria')
        tamano_precio = []
        listaTamanios = []
        precioEstandar = []

        tipoPlatos.append({
            "categoria": categoria,
            "platos": [],
        })
        indice = len(tipoPlatos) - 1
        for p in platosConCategoria:
            tamano_precio.append(
                mod.TamanioPlato.objects.filter(id_plato=p.id_plato, activo=True).order_by('valor_oferta', 'valor'))
            tamanios = mod.TamanioPlato.objects.filter(id_plato=p.id_plato, activo=True).order_by('valor_oferta',
                                                                                                  'valor')
            for t in tamanios:
                listaTamanios.append(t)
        for precio in tamano_precio:
            precioEstandar.append(precio.all()[0].valor)

        tipoPlatos[indice]["platos"] = zip(platosConCategoria, tamano_precio, listaTamanios, precioEstandar)

    print(tipoPlatos)
    # ===========

    tamano_precio = []
    for p in platos:
        tamano_precio.append(mod.TamanioPlato.objects.filter(id_plato=p.id_plato).order_by('valor_oferta', 'valor'))

    listaTamanios = []
    listaOfertas = []
    for p in platos:
        tamanios = mod.TamanioPlato.objects.filter(id_plato=p.id_plato, activo=True).order_by('valor_oferta', 'valor')
        for t in tamanios:
            listaTamanios.append(t)
            if t.valor_oferta:
                listaOfertas.append(t)

    precio_standard = []  # lista de precios de platos con el valor estándar (valor incial por defecto)
    for precio in tamano_precio:
        precio_standard.append(precio.all()[0].valor)
    if request.user.is_authenticated():
        return render(request, "home/menu.html", {'platos_tamanos_precios': zip(platos, tamano_precio, precio_standard),
                                                  'sucursal': sucursal, 'id_sucursal': sucursal_id,
                                                  'direcciones': direcciones, 'tipo_pago': tipo_pago,
                                                  'id_perfil': perfil.id_perfil, 'listaTamanios': listaTamanios,
                                                  'perfil': perfil,
                                                  'listaCategoria': set_categorias, 'pedidosNu': pedidosNu,
                                                  'tipoPlatos': tipoPlatos, 'tamano_precio': tamano_precio,
                                                  'puntuacion1': envio1, 'profileType': profileType,
                                                  'listaOfertas': listaOfertas})
    else:
        return render(request, "home/menu.html", {'platos_tamanos_precios': zip(platos, tamano_precio, precio_standard),
                                                  'sucursal': sucursal, 'id_sucursal': sucursal_id,
                                                  'direcciones': direcciones, 'tipo_pago': tipo_pago,
                                                  'id_perfil': None, 'listaTamanios': listaTamanios,
                                                  'listaCategoria': set_categorias, 'tipoPlatos': tipoPlatos,
                                                  'puntuacion1': envio1})


# Permisos: Debe ser Cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def guardarPedido(request):
    # if request.POST:
    productos = request.GET['productos']
    detalles = json.loads(productos)
    totales = request.GET['totales']
    # comentario = request.GET['comentario']
    if request.GET['comentario']:
        comentario = request.GET['comentario']
    else:
        comentario = 'Sin Comentario'
    # subtotal = totales['subtotal']
    # precio_envio = totales['precio_envio']
    strTotal = json.loads(totales)
    total = strTotal['total']
    id_sucursal = request.GET['id_sucursal']

    sucursal = []
    if request.GET['direccion']:
        id_direccion = request.GET['direccion']
        direccion = mod.Direccion.objects.get(id_direccion=id_direccion)
        sucursal = mod.Sucursal.objects.get(id_sucursal=id_sucursal)
    else:
        sucursal = mod.Sucursal.objects.get(id_sucursal=id_sucursal)
        direccion = sucursal.id_direccion

    ###### comprobar si la sucursal esta abierta INICIO ########
    res = sucursal.estado
    if (res == 0):
        return HttpResponse('false', content_type="application/json")
    else:
        ###### comprobar si la sucursal esta abierta FIN ########

        id_tipo_pago = request.GET['tipo_pago']
        str_tipo_pedido = request.GET['tipo_pedido']

        ###### NO PERMITE COMPRAR SI SU CUENTA NO ESTA VERIFICADA Y SI NO TIENE # DE CEDULA ###### INICIO
        usuario_actual = request.user
        if(str_tipo_pedido=="A domicilio" or str_tipo_pedido=="Para recoger"):
            per=mod.Perfil.objects.get(id_usuario_id=usuario_actual.pk )
            res2=cedulaFB(usuario_actual.pk)
            if(not per.cuentaVerif or not res2):
                return HttpResponse('false', content_type="application/json")
        ###### NO PERMITE COMPRAR SI SU CUENTA NO ESTA VERIFICADA Y SI NO TIENE # DE CEDULA ###### FIN

        # for p in d:
        #   print (p['nombre'],p['valor'],p['cantidad'])

        # perfil = mod.Perfil.objects.get(id_usuario=usuario_actual)
        # zona_de_tiempo_utc = tz.tzutc()
        zona_de_tiempo_local = tz.tzlocal()
        print(zona_de_tiempo_local)

        #######3 Verificar si ya ha hecho un pedido gratis
        '''pedido_gratis = False
        pedidos = mod.Pedido.objects.filter(usuario=request.user)
        for p in pedidos:
            dp = mod.DetallePedido.objects.filter(id_pedido=p)
            for det in dp :
                tamanioPGratis = mod.TamanioPlato.objects.filter(id_tamanioplato=det.tamanio.pk)
                if tamanioPGratis[0].valor_oferta :
                    for detalle in detalles:
                        plato = mod.Plato.objects.get(id_plato=detalle['pk'])
                        tamanioPlato = mod.TamanioPlato.objects.get(id_tamanio__id_tamanio=detalle['tamanio'], id_plato=plato)
                        if tamanioPlato.valor_oferta:
                            pedido_gratis = True

        if pedido_gratis:
            return HttpResponse('false', content_type="application/json")'''
        #######3 Verificar si ya ha hecho un pedido gratis finnn



        fecha = datetime.now()# .replace(tzinfo=zona_de_tiempo_local)
        fecha = fecha.strftime('%Y-%m-%d %H:%M:%S')
        print(fecha)
        tipo_pedido = mod.TipoPedido.objects.get(tipo_pedido=str_tipo_pedido)
        sucursal = mod.Sucursal.objects.get(id_sucursal=id_sucursal)

        tipo_pago = mod.TipoPago.objects.get(id_tipo_pago=id_tipo_pago)
        transportista = mod.Transportista.objects.get(id_transportista=1)
        estado = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_PEDIDO)
        pedido = mod.Pedido(fecha=fecha,
                            id_estado=estado,
                            observaciones=comentario,
                            valor=total,
                            usuario=usuario_actual,
                            id_sucursal=sucursal,
                            id_direccion=direccion,
                            id_tipo_pago=tipo_pago,
                            id_tipo_pedido=tipo_pedido,
                            id_trasportista=transportista)
        pedido.save()
        #######

        for detalle in detalles:
            plato = mod.Plato.objects.get(id_plato=detalle['pk'])
            tamanioPlato = mod.TamanioPlato.objects.get(id_tamanio__id_tamanio=detalle['tamanio'], id_plato=plato)

            miDetallePedido = mod.DetallePedido.objects.create(valor=detalle['total'],
                                                               cantidad_solicitada=detalle['cantidad'],
                                                               id_pedido=pedido,
                                                               id_plato=plato,
                                                               tamanio=tamanioPlato
                                                               )

            miDetallePedido.save()

            for ingrediente in detalle['modeloListaIngredientes']:

                if not ingrediente['seleccionado']:
                    # ingrediente_sel=mod.Ingrediente.objects.get(ingrediente['idIngrediente'])
                    ingrediente_sel = mod.Ingrediente.objects.filter(id_ingrediente=ingrediente['idIngrediente'])
                    miModificacion = mod.Modificardetallepedido.objects.create(id_detallepedido=miDetallePedido,
                                                                               id_ingrediente_quitar=ingrediente_sel[0]
                                                                               )
                    miModificacion.save()

        #### funcion para notificar al realizar nuevo pedido ### INICIO
        telefonoSuc=pedido.id_sucursal.telefono
        userPro=mod.UserSurcursal.objects.get(id_sucursal=pedido.id_sucursal)
        nombre=mod.Perfil.objects.get(id_usuario=userPro.id_user)
        peril=mod.Perfil.objects.get(id_usuario_id=usuario_actual.pk)
        c = {
            'titulo': "¡Nuevo Pedido Realizado!",
            'restaurante': sucursal.nombre_sucursal,
            'fecha': fecha,
            'cliente': usuario_actual,
            'cedula': peril.cedula,
            'telefono': peril.telefono,
            'tipoEnvio': str_tipo_pedido,
            "valor": total,
            "nombreProvee":userPro.id_user,
            "direccionPro": pedido.id_sucursal.id_direccion,
            "telSucur":telefonoSuc,
            'domain': 'goodappetit.com',
            'site_name': 'GoodAppetit',
            'protocol': 'https',
        }

        html_content = 'home/nuevoPedidoNotificationEmail.html'
        email = loader.render_to_string(html_content, c)
        email_html = loader.render_to_string(html_content, c)
        correos=[]
        correos=settings.correosEnviarPedidosRealizados
        usSucu=mod.UserSurcursal.objects.get(id_sucursal=id_sucursal)
        correos.append(usSucu.id_user.email)
        send_mail('Nuevo Pedido Realizado | GoodApettit', email, DEFAULT_FROM_EMAIL,  correos, fail_silently=False,
                  html_message=email_html)
        #### funcion para notificar al realizar nuevo pedido ### FIN

        return HttpResponse('true', content_type="application/json")

def cedulaFB(user):
    exist=False
    try:
        p = mod.Perfil.objects.get(id_usuario=user)
        if(p.cedula):
            exist=True
    except ObjectDoesNotExist:
        exist=False
    return exist

# Nicholas
###########################################################################################
### La siguiente vista requiere los siguientes datos en la base de datos:
############# En la tabla Permisos: #############
### Nombre: "Proveedor" (Puede ser cualquiera, es solo con fin de identificar el permiso)
### Tipo de Contenido: Proveedor (Tenemos que enlazar el permiso con una tabla de la base de datos,
###     me hizo mas sentido enlazarlo con la entidad que estamos representando pero podria ser cualquier otro entidad)
### Nombre en Código: "es_proveedor" (esa es importante ya que se enlaza con la linea de codigo que que comprueba el
###     permiso)
############# En la tabla Grupos: #############
### Un grupo, llamé el mio "Proveedor", a quien le das el permiso definido arriba, "es_proveedor"
############# En la tabla Usuarios: #############
### Un usuario, llamé el mio "prov", que pertence al grupo "Proveedor"
############# Otra forma: #############
### Tambien se puede agregar los permisos directamente al Usuario, pero nos más interesa asignar permisos a grandes
###     numeros de usuarios en base a roles.
###########################################################################################
# '''
#    Ese metodo comprueba si un usuario tiene permiso de proveedor. Si no tiene permiso da un "Error 403: Forbidden" y si
#    tiene el permiso adecuado carga una pagina que dice "Soy Proveedor".
# '''
# @permission_required('spaapp.es_proveedor', raise_exception=True)
# def proveedor_test(request):
#    return render(request, "home/soyProveedor.html", {})


# Views de Julio
# Permisos: Solo usuarios logeados tienen acceso
@login_required()
def mapa(request):
    return render(request, "home/mapa.html", {})


# Permisos: Solo usuarios logeados tienen acceso
@login_required()
def dialogPerfil(request):
    return render(request, "home/dialogPerfil.html", {})


# Permisos: Solo usuarios logeados tienen acceso
@login_required()
class dataPerfil():
    lista = {}
    total = {}


# Permisos: Solo usuarios logeados tienen acceso
#           TODO todos pueden ver perfiles de proveedores
#           TODO todos pueden ver de forma restringida perfiles de transportistas
#           TODO solo se puede ver de forma restrigida perfiles de clientes
#           TODO usuarios pueden ver de forma completa su propio perfil
@login_required()
def perfilDatos(request):
    count = request.GET['count']
    page = request.GET['page']
    lista = mod.Perfil.objects.all()
    paginador = Paginator(lista, count)
    perfiles = paginador.page(page)

    data = dataPerfil()
    data.lista = perfiles
    data.total = lista.count()

    data = serializers.serialize('json', data.lista)

    return HttpResponse(data, content_type="application/json")
    # return render_to_response( data,)


# Permisos: Solo usuarios logeados tienen acceso
#           solo se puede modificar su propio perfil
@login_required()
def perfilSave(request):
    ctx = {}
    id = request.GET['id']
    cedula = request.GET['cedula']
    telefono = request.GET['telefono']
    fecha_nacimiento = request.GET['fecha_nacimiento']
    id_usuario = request.GET['id_usuario']
    fecha_nacimiento = datetime.strptime(fecha_nacimiento, '%d/%m/%Y').strftime('%Y-%m-%d')
    genero = request.GET['genero']
    ruta_foto = request.GET['ruta_foto']
    sim_dispositivo = request.GET['sim_dispositivo']
    user = mod.User.objects.get(id=id_usuario)
    # id = 0
    if id == '0':
        # print('id - ' + id)
        mod.Perfil.objects.create(cedula=cedula, telefono=telefono, fecha_nacimiento=fecha_nacimiento,
                                  genero=genero,
                                  ruta_foto=ruta_foto,
                                  id_usuario=user,
                                  sim_dispositivo=sim_dispositivo)
    else:
        # perfil = mod.Perfil.objects.get(id_perfil=id)
        # perfil.cedula = cedula
        # perfil.save()
        mod.Perfil.objects.filter(id_perfil=id).update(cedula=cedula, telefono=telefono,
                                                       fecha_nacimiento=fecha_nacimiento,
                                                       genero=genero,
                                                       ruta_foto=ruta_foto,
                                                       id_usuario=user,
                                                       sim_dispositivo=sim_dispositivo)

    return HttpResponse("true", content_type="application/json")


# Permisos: Todos tienen acceso
def obtenerPlatos(request):
    platos = mod.Plato.objects.all()
    data = serializers.serialize('json', platos)
    # return json.dumps(data)
    return HttpResponse(data, content_type="application/json")


# Permisos: Todos tienen acceso
def obtenerPlato(request):
    idPlato = request.GET['idPlato']
    plato = mod.Plato.objects.get(id_plato=idPlato)
    Pingredientes = mod.PlatoIngrediente.objects.filter(id_plato=plato)
    pingred_req = Pingredientes.filter(requerido=True)
    pingred_no_req = Pingredientes.filter(requerido=False)

    lista_req = []
    for p in pingred_req:
        lista_req.append(mod.Ingrediente.objects.get(id_ingrediente=p.id_ingrediente.id_ingrediente))

    descripcion = ""
    for i in range(len(lista_req)):
        descripcion += lista_req[i].nombre
        if i < (len(lista_req) - 1):
            descripcion += ", "
    print(descripcion)

    lista = []
    for p in pingred_no_req:
        lista.append(mod.Ingrediente.objects.get(id_ingrediente=p.id_ingrediente.id_ingrediente))
    data = serializers.serialize('json', lista)
    row = json.dumps(
        {"pk": plato.pk,
         "nombre_producto": plato.nombre_producto,
         "descripcion": plato.descripcion,
         "ingredientes": data,
         "ingredientes_ob": descripcion
         },
        sort_keys=True)

    return HttpResponse(row, content_type="application/json")


# ------Filtros Sucrusales-------------
# Permisos: Todos tienen acceso
def obtenerSucursales(request):
    idCategoria = request.GET['idCategoria']
    txtBuscar = request.GET['txtBuscar']
    sucursales = []
    proveedores = []
    if (idCategoria != '0'):
        proveedores = mod.Sucursal.objects.filter(id_proveedor__id_categoria__id_categoria=idCategoria).order_by(
            'id_proveedor', 'estado')
    else:
        proveedores = mod.Sucursal.objects.filter(nombre_sucursal__icontains=txtBuscar).order_by('id_proveedor', 'estado')

    id_proveedor = 0
    estado = -1
    for proveedor in proveedores:
        if id_proveedor != proveedor.id_proveedor or estado!=proveedor.estado:
            sucursales.append(proveedor)
            id_proveedor = proveedor.id_proveedor
            estado = proveedor.estado

    data = serializers.serialize('json', sucursales)
    return HttpResponse(data, content_type="application/json")


def getSucursalesOrdenadas(request):
    idCriterio = request.GET['idCriterio']
    sucursales = mod.Sucursal.objects.all().order_by('nombre_sucursal')
    data = serializers.serialize('json', sucursales)

    return HttpResponse(data, content_type="application/json")


# Función obtenrPedidos se paso a su aplicación

# Permisos: Todos pueden ver direcciones de Proveedores
#           TODO Se puede ver sus propios direcciones
#           TODO Transportistas pueden ver direcciones de Clientes y Proveedores cuando estan involucrados en un pedido
def obtenerDireccion(request):
    id_direccion = request.GET['id_direccion']
    sucursales = mod.Direccion.objects.filter(id_direccion=id_direccion)
    data = serializers.serialize('json', sucursales)
    return HttpResponse(data, content_type="application/json")


# Permisos: Solo usuarios logeados tienen acceso
#           TODO todos pueden ver perfiles de proveedores
#           TODO todos pueden ver de forma restringida perfiles de transportistas
#           TODO solo se puede ver de forma restrigida perfiles de clientes
#           TODO usuarios pueden ver de forma completa su propio perfil
@login_required()
def obtenerPerfil(request):
    id_perfil = request.GET['id_perfil']
    perfil = mod.Perfil.objects.filter(id_direccion=id_perfil)
    data = serializers.serialize('json', perfil)
    return HttpResponse(data, content_type="application/json")


# Permisos: Todos tienen acceso
def obtenerPrecio(request):
    tamanio_plato = request.GET['tamanio_plato']
    plato = mod.Plato.objects.get(pk=request.GET['id'])
    tamanio = mod.Tamanio.objects.get(pk=tamanio_plato)
    platos = mod.TamanioPlato.objects.filter(id_plato=plato, id_tamanio=tamanio)
    valor = 0
    if (platos[0].valor_oferta):
        valor = platos.get(id_plato=plato, id_tamanio=tamanio).valor_oferta
    else:
        valor = platos.get(id_plato=plato, id_tamanio=tamanio).valor
    lista = json.dumps({
        "nombreTamanio": str(tamanio.tamanio),
        "id_tamanio": platos.get(id_plato=plato, id_tamanio=tamanio).id_tamanio.id_tamanio,
        "valor": valor
    }, sort_keys=True)
    return HttpResponse(lista, content_type="application/json")
    # return JsonResponse({'lista': lista}, safe=False)


# Permisos: Solo usuarios logeados
@login_required()
def dialogProducto(request):
    return render(request, "home/dialogProducto.html", {})


# Nicholas
# Permisos: Solo usuarios no autenticados tienen acceso
def registro(request):
    template_name = 'home/perfilCreate.html'

    if request.user.is_authenticated():  # Si el usuario ya esta logueado, no hay sentido mostrarle el logeo
        '''try:
            p = mod.Perfil.objects.get(id_usuario=request.user)
        except ObjectDoesNotExist:
            return redirect(home)
        # TODO perfil no encontrado
        #return redirect(perfil)  # por lo tanto redirecionamos a su perfil'''
        return redirect(restaurantes)
    else:
        if request.method == 'POST':
            form = forms.NewUserForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]
                # email = form.cleaned_data["email"]
                first_name = form.cleaned_data["first_name"]

                ##################### Envio de correo de saludo ######################
                to_admin1 = username

                c = {
                    'email': username,
                    'password': password,
                    'first_name': first_name,
                    'domain': 'goodappetit.com',
                    'site_name': 'GoodAppetit',
                    'protocol': 'https',
                }

                html_content = 'home/registro_email.html'
                email = loader.render_to_string(html_content, c)
                email_html = loader.render_to_string(html_content, c)


                send_mail('Bienvenido a GoodAppetit', email, DEFAULT_FROM_EMAIL, [username], fail_silently=False, html_message= email_html)

                ######################################################################

                user = authenticate(username=username, password=password)
                if user is not None:
                    if user.is_active:
                        auth_login(request, user)
                        users = mod.User.objects.filter(id=user.id)
                        if len(users) > 0:
                            user = users[0]
                            if user.has_perm("spaapp.esAdmin"):
                                return redirect(home)
                            elif user.has_perm("spaapp.esTransportista"):
                                return redirect(uviews.soyTransportista)
                            elif user.has_perm("spaapp.esProveedor"):
                                return redirect(pviews.soyProveedor)
                            elif user.has_perm("spaapp.esCliente"):
                                return redirect(restaurantes)
                return redirect(home)
                # return redirect("/login/0/")
            else:
                # return redirect(registro)
                return render(request, "home/registro.html", {"form": form})
        else:
            form = forms.NewUserForm
            return render(request, "home/registro.html", {"form": form})


# TODO Para el API REST se requiere reviar los permisos mas al fondo
# Nicholas REST API
class ProveedorViewSet(ModelViewSet):
    queryset = mod.Proveedor.objects.all()
    serializer_class = ser.ProveedorSerializer


class SucursalViewSet(ModelViewSet):
    queryset = mod.Sucursal.objects.all()
    serializer_class = ser.SucursalSerializer


class PlatoViewSet(ModelViewSet):
    # platos = mod.Plato.objects.filter(activo=True).order_by('id_platocategoria')
    queryset = mod.Plato.objects.filter(activo=True)
    serializer_class = ser.PlatoSerializer


class PlatoCategoriaViewSet(ModelViewSet):
    # platos = mod.Plato.objects.filter(activo=True).order_by('id_platocategoria')
    queryset = mod.PlatoCategoria.objects.all()
    serializer_class = ser.PlatoCategoriaSerializer


class MenuViewSet(ModelViewSet):
    queryset = mod.Menu.objects.all()
    serializer_class = ser.MenuSerializer


class CoordenadaViewSet(ModelViewSet):
    queryset = mod.Coordenadas.objects.all()
    serializer_class = ser.CoordenadaSerializer


class TamanioPlatoViewSet(ModelViewSet):
    queryset = mod.TamanioPlato.objects.filter(activo=True)
    serializer_class = ser.TamanioPlatoSerializer


class ListaTamaniosPlatos(generics.ListAPIView):
    queryset = mod.TamanioPlato.objects.all()
    serializer_class = ser.TamanioPlatoDetalleSerializer

    def get_queryset(self):
        id_plato = self.kwargs['id_plato']
        id_tamanio = self.kwargs['id_tamanio']
        return mod.TamanioPlato.objects.all().filter(id_plato=id_plato, id_tamanio = id_tamanio)


# Pedidos para transportistas
class PedidoViewSet(ModelViewSet):
    #today = datetime.now()#.date()
    queryset = mod.Pedido.objects.filter(id_estado=2, id_tipo_pedido=1).order_by('-id_pedido')
    contador = 0
    for pedido in queryset:
        queryset[contador].fecha = localtime(pedido.fecha)# pedido.fecha - timedelta(hours=5)
        contador = contador + 1

    serializer_class = ser.PedidoSerializer


# Pedidos para usuarios movil
class PedidoMovilViewSet(ModelViewSet):
    queryset = mod.Pedido.objects.all()
    serializer_class = ser.PedidoMovilSerializer

class PedidoPendienteViewSet(ModelViewSet):
    queryset = mod.Pedido.objects.all()
    serializer_class = ser.PedidoSerializer

#Total pediddos por usuario
class TotalPedidos(generics.ListAPIView):
    serializer_class = ser.PedidoMovilSerializer

    def get_queryset(self):
        id_usuario = self.kwargs['id_usuario']
        return mod.Pedido.objects.filter(usuario=id_usuario)

######## INICIO de API para Proveedor #########

# Pedidos para Proveedor
class PedidoProveedorViewSet(generics.ListAPIView):
    serializer_class = ser.PedidoSerializer

    def get_queryset(self):
        perfil = self.kwargs['perfil']
        id_usuario = mod.Perfil.objects.get(id_perfil=perfil)
        proveedor = mod.Proveedor.objects.get(id_perfil=perfil)
        sucursal = mod.UserSurcursal.objects.get(id_user=id_usuario.id_usuario)
        #sucursal = mod.Sucursal.objects.get(id_proveedor=proveedor)
        datos = mod.Pedido.objects.filter(Q(id_sucursal=sucursal.id_sucursal) & ( Q(id_estado=1) | Q(id_estado=3)) ).order_by('-id_pedido')
        paginador = Paginator(datos, 50)
        pedidos = paginador.page(1)
        return pedidos

class AllPedidoProvViewSet(generics.ListAPIView):
    serializer_class = ser.PedidoSerializer

    def get_queryset(self):
        id_sucursal = self.kwargs['id_sucursal']

        datos = mod.Pedido.objects.filter(Q(id_sucursal=id_sucursal)).order_by('-id_pedido')
        paginador = Paginator(datos, 30)
        pedidos = paginador.page(1)
        return pedidos

class PedidoProveedorPendienteViewSet(generics.ListAPIView):
    serializer_class = ser.PedidoSerializer

    def get_queryset(self):
        perfil = self.kwargs['perfil']
        id_usuario = mod.Perfil.objects.get(id_perfil=perfil)
        #proveedor = mod.Proveedor.objects.get(id_perfil=perfil)
        sucursal = mod.UserSurcursal.objects.get(id_user=id_usuario.id_usuario)
        datos = mod.Pedido.objects.filter(
            Q(id_sucursal=sucursal.id_sucursal) & (Q(id_estado=1) | Q(id_estado=2) | Q(id_estado=3) | Q(id_estado=4))).order_by('-id_pedido')
        paginador = Paginator(datos, 50)
        pedidos = paginador.page(1)
        return pedidos

class PedidoDetalleProveedorViewSet(generics.ListAPIView):
    serializer_class = ser.PedidoDetalleSerializer

    def get_queryset(self):
        id_pedido = self.kwargs['pedido']
        datos = mod.DetallePedido.objects.filter(id_pedido=id_pedido)
        return datos

class PedidoDiaProveedorViewSet(generics.ListAPIView):
    serializer_class = ser.PedidoSerializer

    def get_queryset(self):
        perfil = self.kwargs['perfil']
        id_usuario = mod.Perfil.objects.get(id_perfil=perfil)
        sucursal = mod.UserSurcursal.objects.get(id_user=id_usuario.id_usuario)

        today = datetime.now()
        dia_actual = today.day
        mes_actual = today.month
        #ultimo_dia_mes = calendar.monthrange(today.year, mes_actual)[1]
        rango1 = str(today.year) + '-' + str(mes_actual) + "-" + str(dia_actual) + ' 00:00:00'
        rango2 = str(today.year) + '-' + str(mes_actual) + '-' + str(dia_actual) + ' 23:59:59'
        fechaActual = str(today.year) + '-' + str(mes_actual) + '-' + str(dia_actual)

        datos = mod.Pedido.objects.filter(
            Q(id_sucursal=sucursal.id_sucursal) & (
            Q(id_estado=1) | Q(id_estado=2) | Q(id_estado=3) | Q(id_estado=4) | Q(id_estado=10) | Q(id_estado=11)) & (
                Q(fecha__range=[rango1, rango2])
            )).order_by('-fecha')
        paginador = Paginator(datos, 50)
        pedidos = paginador.page(1)
        return pedidos

class SucursalDatosViewSet(generics.ListAPIView):
    serializer_class = ser.SucursalSerializer

    def get_queryset(self):
        id_usuario = self.kwargs['id_usuario']
        id_sucursal = mod.UserSurcursal.objects.get(id_user=id_usuario)
        print(id_sucursal.id_sucursal.pk)
        datos_sucursal = mod.Sucursal.objects.filter(id_sucursal=id_sucursal.id_sucursal.pk)
        return datos_sucursal

class PlatosSucursalViewSet(generics.ListAPIView):
    serializer_class = ser.PlatoSerializer

    def get_queryset(self):
        id_sucursal = self.kwargs['id_sucursal']
        return mod.Plato.objects.filter(id_menu__id_sucursal= id_sucursal)

### API para poder editar la información del plato en la APP de Proveedor
class Plato2ViewSet(ModelViewSet):
    queryset = mod.Plato.objects.all()
    serializer_class = ser.Plato2Serializer

### API para poder editar la información del tamaño plato en la APP de Proveedor
class TamanioPlato2ViewSet(ModelViewSet):
    queryset = mod.TamanioPlato.objects.all()
    serializer_class = ser.TamanioPlatoSerializer



    #### FIN de API para Proveedor ########

    #Lista de pedidos usuario movil
class ListaPedidos(generics.ListAPIView):
    serializer_class = ser.MisPedidosSerializer

    def get_queryset(self):
        id_usuario = self.kwargs['id_usuario']
        pagina = self.kwargs['pagina']
        tamanio = self.kwargs['tamanio']
        datos = mod.Pedido.objects.filter(usuario=id_usuario).order_by('-id_pedido')
        paginador = Paginator(datos, tamanio)
        pedidos = paginador.page(pagina)
        return pedidos


class ListaDirecciones(generics.ListAPIView):
    #queryset = mod.Direccion.objects.all()
    serializer_class = ser.DireccionesSerializer

    def get_queryset(self):
        id_usuario = self.kwargs['id_usuario']
        perfil = mod.Perfil.objects.get(id_usuario__id=id_usuario)
        return mod.Direccion.objects.all().filter(id_perfil=perfil).order_by('id_direccion')

class removeDirecciones(generics.ListAPIView):
    serializer_class = ser.DireccionesSerializer

    def get_queryset(self):
        idDireccion = self.kwargs['id_direccion']
        idUsuario = self.kwargs['id_usuario']
        mod.Direccion.objects.get(id_direccion=idDireccion).delete()
        perfil = mod.Perfil.objects.get(id_usuario__id=idUsuario)
        return mod.Direccion.objects.all().filter(id_perfil=perfil).order_by('id_direccion')


class PedidoEstadoViewSet(ModelViewSet):
    queryset = mod.Pedido.objects.all().filter(Q(id_estado=2) | Q(id_estado=4)| Q(id_estado=9))
    serializer_class = ser.PedidoEstadoSerializer

class PedidoEstadoProvViewSet(ModelViewSet):
    queryset = mod.Pedido.objects.all().filter(Q(id_estado=1) | Q(id_estado=3))
    serializer_class = ser.PedidoEstadoSerializer

class PedidoEntregadoViewSet(generics.ListAPIView):
    serializer_class = ser.PedidoSerializer

    def get_queryset(self):
        #today = datetime.now().date()
        id_trasportista = self.kwargs['id_trasportista']
        return mod.Pedido.objects.all().filter(Q(id_estado=9) | Q(id_estado=4) | Q(id_estado=3), id_trasportista=id_trasportista)
        #fecha__year=today.year,fecha__month=today.month, fecha__day=today.day)


class PedidoAllViewSet(ModelViewSet):
    queryset = mod.Pedido.objects.all()
    serializer_class = ser.PedidoEstadoSerializer

    # def update(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     instance.id_pedido = request.data.get("id_pedido")
    #     instance.save()


# Direcciones
class DirecionesViewSet(ModelViewSet):
    queryset = mod.Direccion.objects.all()
    serializer_class = ser.DireccionesSerializer


# Perfil
class ListaPerfil(generics.ListAPIView):
    serializer_class = ser.PerfilSerializer
    def get_queryset(self):
        id_usuario = self.kwargs['id_usuario']
        return mod.Perfil.objects.filter(id_usuario=id_usuario)


# Pedidos
class TransportistaViewSet(ModelViewSet):
    queryset = mod.Transportista.objects.all()
    serializer_class = ser.TransportistaSerializer


class TransportistaExistenteViewSet(generics.ListAPIView):
    serializer_class = ser.TransportistaSerializer

    def get_queryset(self):
        sim = self.kwargs['sim']
        numero = self.kwargs['numero']
        if numero == 0:
            return mod.Transportista.objects.filter(Q(sim_dispositivo=sim))
        else:
            return mod.Transportista.objects.filter(Q(sim_dispositivo=sim) | Q(numero=numero))


class TraspCoordenadasViewSet(generics.ListAPIView):
    queryset = mod.Coordenadas.objects.all()
    serializer_class = ser.TranspCoordSerializer



# Usuarios
class UsuarioTransportistaViewSet(generics.ListAPIView):
    serializer_class = ser.UsuarioSerializer

    def get_queryset(self):
        usuario = self.kwargs['usuario']
        return mod.User.objects.filter(username=usuario)


# Usuarios movil
class UsuariosMovilViewSet(generics.ListAPIView):
    serializer_class = ser.PerfilUsuarioSerializer

    def get_queryset(self):
        email = self.kwargs['email']
        first_name = self.kwargs['first_name']
        last_name = self.kwargs['last_name']
        usuario = mod.User.objects.filter(email=email)
        if len(usuario) == 0:
            usuario = mod.User.objects.create(last_login=None, is_superuser=False, username=email,
                                              first_name=first_name, last_name=last_name, email=email,
                                              is_staff=False, is_active=True)
            cliente = Group.objects.get(name="Cliente")
            usuario.groups.add(cliente)
            usuario.save()

        # usuario = mod.Perfil.objects.filter(email=email)
        perfil = mod.Perfil.objects.filter(id_usuario=usuario)

        return perfil

# Usuarios movil
class ActualizarUsuariosMovilViewSet(generics.ListAPIView):
    serializer_class = ser.PerfilUsuarioSerializer

    def get_queryset(self):
        #email = self.kwargs['usuario']
        email = self.kwargs['email']
        first_name = self.kwargs['first_name']
        last_name = self.kwargs['last_name']
        cedula = self.kwargs['cedula']

        #mod.User.objects.filter(email=email).update(username=email, first_name=first_name, last_name=last_name)
        mod.User.objects.filter(email=email).update(first_name=first_name, last_name=last_name)
        usuario = mod.User.objects.filter(email=email)

        mod.Perfil.objects.filter(id_usuario=usuario).update(cedula=str(cedula))
        perfil = mod.Perfil.objects.filter(id_usuario=usuario)

        return perfil

class borrarNumeroMovilViewSet(generics.ListAPIView):
    serializer_class = ser.PerfilUsuarioSerializer

    def get_queryset(self):
        idPerfil = self.kwargs['idPerfil']
        numero = self.kwargs['numeroTel']
        perfil = mod.Perfil.objects.get(id_perfil=idPerfil)
        if(numero=="0000"):
            perfil.telefono = None
            perfil.cuentaVerif = False
        else:
            perfil.telefono = numero
            perfil.cuentaVerif = True
        perfil.save()

        perfil = mod.Perfil.objects.filter(id_perfil=idPerfil)

        return perfil

class comprobarNumeroMovilViewSet(generics.ListAPIView):
    serializer_class = ser.PerfilUsuarioSerializer

    def get_queryset(self):
        idPerfil = self.kwargs['idPerfil']
        numero = self.kwargs['numeroTel']

        try:
            perfil = mod.Perfil.objects.get(telefono=numero)
            perfilSend = mod.Perfil.objects.filter(id_perfil=perfil.id_perfil)
        except ObjectDoesNotExist:
            perfilSend = mod.Perfil.objects.filter(id_perfil=idPerfil)

        return perfilSend


class sendCodeEmailMovilViewSet(generics.ListAPIView):
    serializer_class = ser.UsuarioSerializer

    def get_queryset(self):
        email = self.kwargs['email']
        codeSend = self.kwargs['code']
        user = User.objects.get(email=email)
        per=mod.Perfil.objects.get(id_usuario_id=user.pk)
        enviarCodigoEmail(user, codeSend, per.telefono)


class loginUsuarioMovilViewSet(generics.ListAPIView):
    serializer_class = ser.PerfilUsuarioSerializer
    def get_queryset(self):
        email = self.kwargs['email']
        password = self.kwargs['pass']
        perfil = None
        try:
            # login por usuario y contrasena
            u = User.objects.get(email=email)
            user = authenticate(username=u.username, password=password)
            perfil = mod.Perfil.objects.filter(id_usuario=user)
        except ObjectDoesNotExist:
            # login por mail y contrasena
            user = authenticate(username=email, password=password)
            perfil = mod.Perfil.objects.filter(id_usuario=user)

        return perfil

class registrarUsuarioMovilViewSet(generics.ListAPIView):
    serializer_class = ser.PerfilUsuarioSerializer

    def get_queryset(self):
        # usuario = self.kwargs['usuario']
        usuario = self.kwargs['email']
        nombres = self.kwargs['nombres']
        apellidos = self.kwargs['apellidos']
        cedula = self.kwargs['cedula']
        email = self.kwargs['email']
        password = self.kwargs['pass']
        perfil = None

        user = User(username=usuario, email=email, first_name=nombres, last_name=apellidos)
        user.set_password(password)
        user.save()

        cliente = Group.objects.get(name="Cliente")
        user.groups.add(cliente)
        user.save()

        perfil = mod.Perfil.objects.filter(id_usuario=user).update(cedula=cedula)
        perfil = mod.Perfil.objects.filter(id_usuario=user)

        return perfil

class comprobarUsuarioMovilViewSet(generics.ListAPIView):
    serializer_class = ser.UsuarioSerializer

    def get_queryset(self):
        usuario = self.kwargs['usuario']
        user = User.objects.filter(username=usuario)

        return user

class comprobarCedulaMovilViewSet(generics.ListAPIView):
    serializer_class = ser.PerfilUsuarioSerializer

    def get_queryset(self):
        cedula = self.kwargs['cedula']
        perfil = mod.Perfil.objects.filter(cedula=cedula)

        return perfil

class comprobarEmailMovilViewSet(generics.ListAPIView):
    serializer_class = ser.UsuarioSerializer

    def get_queryset(self):
        email = self.kwargs['email']
        user = User.objects.filter(email=email)

        return user


# lsitar categorias de platos de un determinado menú
class listaPlatosCategoria(generics.ListAPIView):
    serializer_class = ser.PlatoSerializer

    def get_queryset(self):
        menu_id = self.kwargs['menu_id']
        return mod.Plato.objects.filter(id_menu=menu_id, activo=True)


class ListaDetallePedidos(ModelViewSet):
    queryset = mod.DetallePedido.objects.all()
    serializer_class = ser.PedidoDetalleSerializer

class ListaDetallePedidosMovil(ModelViewSet):
    queryset = mod.DetallePedido.objects.all()
    serializer_class = ser.PedidoDetalleMovilSerializer

#JY, clase usada para modificar los ingrdientes que no se desean en un producto
class ListaDetallePedidosIngr(ModelViewSet):
    queryset = mod.Modificardetallepedido.objects.all()
    serializer_class = ser.PedidoDetalleIngrSerializer

# listar platos de una determinada categoria
class PlatoLista(generics.ListAPIView):
    serializer_class = ser.PlatoSerializer

    def get_queryset(self):
        menu_id = self.kwargs['menu_id']
        id_categoria = self.kwargs['id_categoria']
        return mod.Plato.objects.filter(id_menu=menu_id, activo=True, id_platocategoria__id_platocategoria=id_categoria)


# JY, Listar Ingredientes para un plato especifico pasado como parametro
class PlatoIngredienteLista(generics.ListAPIView):
    serializer_class = ser.PlatoIngredientesSerializer

    def get_queryset(self):
        plato_id = self.kwargs['plato_id']
        return mod.PlatoIngrediente.objects.filter(id_plato_id=plato_id)

class CategoriasViewSet(ModelViewSet):
    queryset = mod.PlatoCategoria.objects.all()
    serializer_class = ser.CategoriaSerializer


# JY, Claseusada para traer todas las categorias principales
# y se envian a serializarlas para el API Rest
class CategoriasPViewSet(generics.ListAPIView):
    serializer_class = ser.CategoriasPSerializer
    def get_queryset(self):
        proveedor = mod.Proveedor.objects.distinct('id_categoria_id')
        print(proveedor)
        idCatProveedores=[];
        for idCatProv in proveedor:
            idCatProveedores.append(idCatProv.id_categoria_id)
        print(idCatProveedores)
        queryset = mod.Categoria.objects.filter(id_categoria__in=idCatProveedores)
        return queryset

# Sugerencias
class SugerenciasViewSet(ModelViewSet):
    queryset = mod.Sugerencia.objects.all()
    serializer_class = ser.SugerenciasSerializer

class recuperarPassViewSet(generics.ListAPIView):
    serializer_class = ser.UsuarioSerializer

    def get_queryset(self):
        email = self.kwargs['email']
        print(email)

        user = User.objects.get(email = email)

        c = {
            'email': email,
            # 'domain': request.META['HTTP_HOST'],
            # 'domain': 'www.ajamar.com',
            'domain': 'goodappetit.com',  # para producción
            # 'domain': '127.0.0.1:8000',  #para probar localmente
            'site_name': 'GoodAppetit',
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': default_token_generator.make_token(user),
            'protocol': 'https',
            # 'protocol': 'http',    #para probar localmente
        }

        subject_template_name = 'registration/password_reset_subject.txt'
        email_template_name = 'reset_password/password_reset_email.html'

        subject = loader.render_to_string(subject_template_name, c)

        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(email_template_name, c)
        emailHtml = loader.render_to_string(email_template_name, c)
        send_mail(subject, email, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False, html_message=emailHtml)
        # send_mail(subject, msg.as_string(), DEFAULT_FROM_EMAIL, [user.email])

############# API para administración de Trasportistas #############

class PedidosDiaTodosViewSet(generics.ListAPIView):
    serializer_class = ser.PedidoSerializer

    def get_queryset(self):
        today = datetime.now()
        dia_actual = today.day
        mes_actual = today.month

        rango1 = str(today.year) + '-' + str(mes_actual) + "-" + str(dia_actual) + ' 00:00:00'
        rango2 = str(today.year) + '-' + str(mes_actual) + '-' + str(dia_actual) + ' 23:59:59'

        datos = mod.Pedido.objects.filter(Q(fecha__range=[rango1, rango2]) & Q(id_tipo_pedido=1))
        return datos

class TranspViewSet(generics.ListAPIView):
    serializer_class = ser.TransportistaSerializer

    def get_queryset(self):
        sim = self.kwargs['sim']
        return mod.Transportista.objects.filter(Q(sim_dispositivo=sim))

class CoordenadaExisteViewSet(generics.ListAPIView):
    serializer_class = ser.CoordenadaSerializer

    def get_queryset(self):
        id_transportista = self.kwargs['id_transportista']
        return mod.Coordenadas.objects.filter(id_transportista=id_transportista)


# ==============================FIN API REST===================================



# ==============================FIN API REST===================================


def loginUser(request):
    if request.user.is_authenticated():  # Si el usuario ya esta logueado, no hay sentido mostrarle el logeo
        try:
            p = mod.Perfil.objects.get(id_usuario=request.user)
            c = mod.Cliente.objects.get(id_perfil=p)
        except ObjectDoesNotExist:
            return redirect(home)
        # TODO Arreglar perfil no encontrado:
        # return redirect(perfil)  # por lo tanto redirecionamos a su perfil
        return redirect(home)
    else:
        if request.method == 'POST':
            form = forms.FormLogin(request.POST)
            if form.is_valid():
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]
                user = authenticate(username=username, password=password)
                if user is not None:
                    if user.is_active:
                        auth_login(request, user)
                        # Redirect to a success page.
                        # print("Valid User %s" % username)
                        # return redirect("/")

                    else:
                        # Return a 'disabled account' error message
                        print("User %s has valid credentials, but a disabled account." % username)
                else:
                    # Return an 'invalid login' error message.
                    print("Invalid Login for \"%s\"" % username)
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            else:
                print("Invalid Form Data")
                # return redirect(home)
        else:
            form = forms.FormLogin()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# =========================
def registroUser(request):
    template_name = 'home/perfilCreate.html'

    if request.user.is_authenticated():  # Si el usuario ya esta logueado, no hay sentido mostrarle el logeo
        return redirect(perfil)  # por lo tanto redirecionamos a su perfil
    else:
        if request.method == 'POST':
            form = forms.NewUserForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect(login)
            else:
                # return redirect(registro)
                return render(request.META.get('HTTP_REFERER'), {"form": form})
        else:
            form = forms.NewUserForm
            return render(request.META.get('HTTP_REFERER'), {"form": form})


# ======================REGISTRO==============
def registro_usuario_view(request):
    if request.method == 'POST':
        first_name = request.GET['first_name']
        last_name = request.GET['first_name']
        username = request.GET['username']
        password = request.GET["password"]
        email = request.GET['email']
        mod.User.objects.create_user(username=username, password=password, first_name=first_name, last_name=last_name,
                                     email=email)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                cliente = Group.objects.get(name="Cliente")
                user.groups.add(cliente)
                user.save()
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        # Si el mthod es GET, instanciamos un objeto RegistroUserForm vacio
        form = forms.RegistroUserForm
        context = {'form': form}
        return redirect(request.META.get('HTTP_REFERER'))


# ================== Validar correo y usuario si existen en la base ===============
def validarCorreo(request):
    correo = request.GET["mail"]
    if User.objects.filter(email=correo):
        return HttpResponse('false', content_type="application/json")
    return HttpResponse('true', content_type="application/json")


def validarUsuario(request):
    usuario = request.GET["usuario"]
    if User.objects.filter(username=usuario):
        return HttpResponse('false', content_type="application/json")
    return HttpResponse('true', content_type="application/json")


# ========================Notificar CLiente=============== INICIO ====
@login_required()
def notificarPedidoCliente(request):
    if True:  # request.is_ajax():

        usuario = request.user
        estadoDomicilio = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_TRANSPORTISTA)
        estadoCancelado = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_CANCELADO_PROVEEDOR)
        estadoRecoger = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_PROCESO)


        pedidos = mod.Pedido.objects.filter(Q(usuario=usuario, id_estado__estado=estadoDomicilio, notificado=True) |
                                            Q(usuario=usuario, id_estado__estado=estadoRecoger, notificado=False) |
                                            Q(usuario=usuario, id_estado__estado=estadoCancelado, notificado=True)
                                           ).order_by(
            'fecha')

        if len(pedidos) < 1:
            return HttpResponse('false', content_type="application/json")
        else:
            pedido = [pedidos[0]]

            # if pedido.count() > 0:
            dataPer = serializers.serialize('json', pedido)

            pedido[0].notificado = True
            pedido[0].save()
            #if (pedido[0].id_tipo_pedido_id == 2):

            if (pedido[0].id_estado_id == 11):
                pedido[0].notificado = False
                pedido[0].save()
            elif (pedido[0].id_estado_id == 3):
                pedido[0].notificado = False
                pedido[0].save()

                #pedido[0].id_estado_id = 5
                #pedido[0].save()
            return HttpResponse(dataPer, content_type="application/json")
            # return render('true', "application/json", {'pedido': pedido})


@login_required()
def notificarEstadoPedidoCliente(request):
    if True:  # request.is_ajax():
        usuario = request.user
        estado = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_ENVIADO)
        pedidos = mod.Pedido.objects.filter(usuario=usuario, id_estado__estado=estado,
                                            notificadoPersistente=False).order_by('fecha')
        if len(pedidos) < 1:
            return HttpResponse('false', content_type="application/json")
        else:
            pedido = [pedidos[0]]
            # if pedido.count() > 0:
            dataPer = serializers.serialize('json', pedido)
            # pedido[0].notificadoPersistente = True
            # pedido[0].save()
            return HttpResponse(dataPer, content_type="application/json")
            # return render('true', "application/json", {'pedido': pedido})


# ========================Notificar CLiente================ FIN ====


# ========================Validar Cuenta con SMS ================ INICIO ====
@login_required()
def validarCuentaSMS(request):
    profileType = None
    if request.user.is_authenticated():
        ######### tipo de usuario ###################
        user = request.user
        if user.has_perm("spaapp.esTransportista"):
            print('es transportista')
            # TODO poner imagen por defecto para todos los perfiles de los transportistas
            perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
        else:
            print('Cliente o Proveedor')
            perfil = mod.Perfil.objects.get(id_usuario=request.user)

        # JY, Determinar que tipo de Usuario esta Autenticado
        profileType = validarTipoUsuario(request)
        ######### tipo de usuario ###################


        try:
            user = request.user
            perfil = mod.Perfil.objects.get(id_usuario=user)
            if request.method == 'GET':
                form = forms.celularValido(initial={'id_perfil': perfil.id_perfil}, prefix="validar")
                form2 = forms.celularExistente(initial={'id_perfil': perfil.id_perfil, 'celular': perfil.telefono},
                                               prefix="existente")
                form3 = forms.celularValido(initial={'id_perfil': perfil.id_perfil}, prefix="validar2")
            elif request.method == 'POST':

                # form = forms.celularValido(initial={'id_perfil': perfil.id_perfil}, prefix="validar")
                # form2 = forms.celularExistente(initial={'id_perfil': perfil.id_perfil, 'celular': perfil.telefono},
                #                                prefix="existente")
                form3 = forms.celularValido(initial={'id_perfil': perfil.id_perfil}, prefix="validar2")

                form = forms.celularValido(request.POST, prefix="validar")
                form2 = forms.celularExistente(request.POST, prefix="existente")
                form3 = forms.celularValido(request.POST, prefix="validar2")

                v_f1 = form.is_valid()
                # print('v_f1', v_f1)
                # v_f2 = form2.is_valid()
                # print('v_f2', v_f2)
                # v_f3 = form3.is_valid()
                # print('v_f3', v_f3)
                # if (v_f1 or v_f2 ) and (not v_f1 or not v_f2):
                if (v_f1):
                    if v_f1:
                        form.save()
                        # elif v_f3:
                        #     form3.save()
                        # elif v_f2:
                        #     form2.save()
                        # else:
                        #raise PermissionDenied("Falta de Permisos #vcsms #1")
                    return redirect(validarCuentaSMS)
            else:
                raise PermissionDenied("Falta de Permisos #vcsms #2")
        except ObjectDoesNotExist:
            perfil = None
    return render(request, 'home/validarCuenta.html', {'perfil': perfil, 'form': form, "form2": form2, 'form3': form3, 'profileType': profileType})
    # return render(request,'home/validarCuenta.html', {'perfil':perfil,'form':form, "form2": form2})


def guardarCelular(request):
    if request.method == 'POST':
        form = forms.celularValido(request.POST)
        if form.is_valid():
            form.save()
            celular = form.cleaned_data["celular"]
            return redirect(enviarCodigo)
    form = forms.celularValido()
    return render(request, "home/registro.html", {"form": form})


def enviarCodigo(request):

    extraT = datetime.now().strftime("%Y%m%d%H%M%S")
    extra = str(extraT)
    # extra=extraStr.replace(":", "").replace("-", "").replace(" ", "").replace(".", "")
    # extra=extra.replace(": .-","")
    codigoPais = "593"
    user = request.user
    perfil = mod.Perfil.objects.get(id_usuario=user)
    telefono = perfil.telefono


    # username=user.username
    idc = perfil.cedula
    # genero=perfil.genero
    # sumaTodo=username+telefono+idc+genero
    sumaTodo = telefono + extra
    totalTelf = codigoPais + telefono[1:10]
    print(telefono)
    print(totalTelf)
    print(extra)
    print(sumaTodo)
    sal = settings.salKey
    print(sal)
    hash = bcrypt.hashpw(sumaTodo.encode('utf-8'), sal)

    # code=str(hash)
    # codigoSend=hash[-5]+hash[-4]+hash[-3]+hash[-2]+hash[-1]
    codigoSend = hash[-3] * hash[-2] * hash[-1]

    ###### CODIGO TEMPORAL ####### INICIO

    tem = open("archivoTemporal.txt", "a")
    tem.write('Usuario: '+user.username+'\n')
    tem.write('Fecha: '+str(datetime.now())+'\n')

    ###### CODIGO TEMPORAL ####### FIN


    enviarCodigoEmail(user, codigoSend, telefono)
    payload = "{\"from\":\"GoodAppetit\",\"to\":\"%s\",\"text\":\"Use %s como codigo de verificacion de su cuenta GoodAppetit" \
              "\"}"%(totalTelf, codigoSend)
    settings.conn.request("POST", "/sms/1/text/single", payload, settings.headers)
    res = settings.conn.getresponse()
    data = res.read()

    tem.write('ESTADO: '+data.decode("utf-8")+'\n')
    tem.write("##########################################\n\n")

    print(">>>>>>>",data.decode("utf-8"))

    ###### CODIGO TEMPORAL ####### INICIO

    tem.close()
    ###### CODIGO TEMPORAL ####### FIN


    #client = TwilioRestClient(settings.account_sid, settings.auth_token)
    #print(codigoSend)
    #message = client.messages.create(to=codigoPais + telefono, from_=settings.numeroFrom, body="Use '" + str(
    #    codigoSend) + "' como codigo de verificación de su cuenta en GoodAppetit")
    # print(str(message)+" - " +codigoPais+str(telefono))
# dataPer = serializers.serialize('json', pedido)
    return HttpResponse(extra, content_type="application/json")
    # return redirect(verificarCodigo)

def enviarCodigoEmail(user, code, telf):
    to_admin1 = user
    #perf=user.user.first_name

    c = {
        'username': to_admin1.username,
        'code' : code,
        'domain': 'goodappetit.com',
        'site_name': 'GoodAppetit',
        'protocol': 'https',
    }

    html_content = 'home/codigoVerificacionEmail.html'
    email = loader.render_to_string(html_content, c)
    email_html = loader.render_to_string(html_content, c)

    send_mail('Código de Verificación | GoodAppetit', email, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False, html_message=email_html)
    '''
    html_content = "<h2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Código de Verificación </h2>" \
                   "<br>Hola <b>%s</b> puedes usar <strong><a href='https://www.goodappetit.com/perfil/validarCuenta'>%s</a></strong> para la verificación de tu " \
                   "cuenta en Good Appetit </br><br></br><br></br>" \
                   "Saludos<br>El equipo de GoodAppetit<br>" \
                   "www.goodappetit.com<br>" \
                   "Crisantemos y Anturios 13-15<br>" \
                   "Loja-Ecuador" % (
        to_admin1.username, code)

    #msg1 = EmailMultiAlternatives('GoodAppetit', html_content, 'from@server.com', [user])
    #msg1.attach_alternative(html_content, 'text/html')
    #msg1.send()

    res=send_mail("Código de Verificación | GooodAppetit", "segundo componente", DEFAULT_FROM_EMAIL, [user.email], fail_silently=False, html_message=html_content)
    print(">>>>>>> ",res)
    '''

def verificarCodigo(request):
    if request.method == "POST":
        codigoR = request.POST['coder']
        codigoEx = request.POST['extraC']
        # print(codigoR)
        # print(codigoEx)
        user = request.user
        perfil = mod.Perfil.objects.get(id_usuario=user)
        telefono = perfil.telefono
        # username=user.username
        idc = perfil.cedula
        # genero=perfil.genero
        # sumaTodo=username+telefono+idc+genero
        sumaTodo = telefono + codigoEx
        # print(sumaTodo)

        sal = settings.salKey
        # print(sal)
        hash = bcrypt.hashpw(sumaTodo.encode('utf-8'), sal)
        # print(hash)
        codigoSend = hash[-3] * hash[-2] * hash[-1]
        # print(str(codigoSend)+ "<-- generado")
        # print(str(codigoR)+ "<-- extraido")
        if (str(codigoSend) == codigoR):
            # print("cuenta verificada")
            # return render(request, "home/registro.html", {"form": 'true'})
            perfil.cuentaVerif = True
            perfil.save()
            return HttpResponse('true', content_type="application/json")
        else:
            # print("cuenta NO verificada")
            # return render(request, "home/registro.html", {"form": 'false'})
            return HttpResponse('false', content_type="application/json")

    form = ""
    return render(request, "home/registro.html", {"form": 'false'})


def borrarNumeroC(request):
    if request.method == "POST" and request.user.is_authenticated():
        user = request.user
        perfil = mod.Perfil.objects.get(id_usuario=user)
        perfil.telefono = None
        perfil.cuentaVerif = False
        perfil.save()
        return HttpResponse('true', content_type="application/json")
    return HttpResponse('false', content_type="application/json")


# ========================Validar Cuenta con SMS ================ FIN ====


def validarUsuarioLogin(request):
    usuario = request.GET["usuario"]
    password = request.GET["password"]
    user = authenticate(username=usuario, password=password)
    if user is not None:
        if user.is_active:
            auth_login(request, user)
            return HttpResponse('true', content_type="application/json")
    return HttpResponse('false', content_type="application/json")


def cerrarProveedor(request):
    if request.user.has_perm("spaapp.esProveedor"):
        usuarioActual = request.user

        try:
            perfil = mod.Perfil.objects.get(id_usuario=usuarioActual)
            proveedor = mod.Proveedor.objects.get(id_perfil=perfil)
            # onpen_close = 0
            onpen_close = 0

            mod.Sucursal.objects.filter(id_proveedor=proveedor).update(estado=onpen_close)

            auth.logout(request)
        except ObjectDoesNotExist:
            auth.logout(request)
    else:
        auth.logout(request)
    # Redirect to a success page.
    return HttpResponseRedirect("/")

    ##return redirect(soyProveedor)
    # else:
    #    raise Http404


# JY, Funcion usada para determinar que tipo de usuario ingreso a applicacion
# 0 Admin
# 1 Cliente
# 2 Proveedor
# 3 Transportista
def validarTipoUsuario(request):
    if request.user.has_perm("spaapp.esAdmin"):
        return 0  # Admin
    elif request.user.has_perm("spaapp.esTransportista"):
        return 3  # Proveedor
    elif request.user.has_perm("spaapp.esProveedor"):
        return 2  # Proveedor
    elif request.user.has_perm("spaapp.esCliente"):
        return 1  # Cliente
    else:
        return None


############################################################
## Clase para recuperar la contraseña por medio del correo o del username

# from django.contrib.staticfiles.templatetags.staticfiles import static as st
# from django.templatetags.static import static
# from django.core.mail import EmailMessage
# from email.mime.image import MIMEImage
# from email.mime.multipart import MIMEMultipart
# from email.mime.text import MIMEText
# import os
class ResetPasswordRequestView(FormView):
    template_name = "reset_password/password_reset_form.html"
    success_url = '/account/reset_password'
    form_class = forms.PasswordResetRequestForm

    @staticmethod
    def validate_email_address(email):

        try:
            validate_email(email)
            return True
        except ValidationError:
            return False
    def post(self, request, *args, **kwargs):

        form = self.form_class(request.POST)
        if form.is_valid():
            data = form.cleaned_data["email_or_username"]
            if self.validate_email_address(data) is True:
                '''
                Si la entrada es un email
                '''
                associated_users = User.objects.filter(Q(email=data) | Q(username=data))
                if associated_users.exists():
                    for user in associated_users:
                        c = {
                            'email': user.email,
                            # 'domain': request.META['HTTP_HOST'],
                            #'domain': 'www.ajamar.com',
                            'domain': 'goodappetit.com',   #para producción
                            #'domain': '127.0.0.1:8000',  #para probar localmente
                            'site_name': 'GoodAppetit',
                            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                            'user': user,
                            'token': default_token_generator.make_token(user),
                            'protocol': 'https',
                            #'protocol': 'http',    #para probar localmente
                        }
                        subject_template_name = 'registration/password_reset_subject.txt'
                        email_template_name = 'reset_password/password_reset_email.html'



                        # img_data = open(os.path.join(os.path.dirname(__file__), 'static/home/img/email/logo_email.png'), 'rb').read()
                        #
                        # msg = MIMEMultipart(_subtype='related')
                        #
                        # body = MIMEText("", _subtype='html')
                        #
                        # msg.attach(body)
                        #
                        # img = MIMEImage(img_data, 'jpeg')
                        # img.add_header('Content-Id', '<logo>')  # angle brackets are important
                        # msg.attach(img)

                        # img_data = open(os.path.join(os.path.dirname(__file__), 'static/home/img/email/logo_email.png'), 'rb').read()
                        # msg = MIMEMultipart(_subtype='related')
                        # body = MIMEText('<p>Hello <img src="cid:myimage" /></p>', _subtype='html')
                        # msg.attach(body)
                        #
                        # # Now create the MIME container for the image
                        # img = MIMEImage(img_data, 'jpeg')
                        # img.add_header('Content-Id', '<myimage>')  # angle brackets are important
                        # msg.attach(img)
                        # subject="lalalalala"


                        subject = loader.render_to_string(subject_template_name, c)

                        subject = ''.join(subject.splitlines())
                        email = loader.render_to_string(email_template_name, c)
                        emailHtml = loader.render_to_string(email_template_name, c)
                        send_mail(subject, email, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False, html_message=emailHtml)
                        #send_mail(subject, msg.as_string(), DEFAULT_FROM_EMAIL, [user.email])
                    result = self.form_valid(form)
                    messages.success(request,
                                     'Se ha enviado un enlace a la dirección '+ data + ". Por favor revisa tu bandeja de entrada para continuar con el restablecimiento de la contraseña.")
                    return result
                result = self.form_invalid(form)
                messages.error(request, 'No existe un usuario asociado a esta dirección de correo')
                return result
            else:
                '''
                Si la entrada es un username
                '''
                associated_users = User.objects.filter(username=data)
                if associated_users.exists():
                    for user in associated_users:
                        c = {
                            'email': user.email,
                            #'domain': 'www.ajamar.com',
                            'domain': 'www.goodappetit.com',   #para producción
                            #'domain': '127.0.0.1:8000',  #para probar localmente
                            'site_name': 'GoodAppetit',
                            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                            'user': user,
                            'token': default_token_generator.make_token(user),
                            'protocol': 'https',
                            # 'protocol': 'http',    #para probar localmente
                        }
                        subject_template_name = 'registration/password_reset_subject.txt'
                        email_template_name = 'reset_password/password_reset_email.html'
                        subject = loader.render_to_string(subject_template_name, c)
                        # Email subject *must not* contain newlines
                        subject = ''.join(subject.splitlines())
                        email = loader.render_to_string(email_template_name, c)
                        send_mail(subject, email, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
                    result = self.form_valid(form)
                    messages.success(request,
                                     'Se ha enviado un link a la dirección de correo de ' + data + ". Por favor revisa tu bandeja de entrada para continuar con el reestablecimiento de la contraseña.")
                    return result
                result = self.form_invalid(form)
                messages.error(request, 'Este usuario no existe en el sistema!')
                return result
        messages.error(request, 'Entrada inválida')
        return self.form_invalid(form)



