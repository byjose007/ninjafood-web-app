from django.db import models
from django.contrib.auth.models import User

# Create your models here


class Ciudad(models.Model):
    id_ciudad = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=80)
    descripcion = models.CharField(max_length=250)
    def __str__(self):
        return '%s' %(self.nombre)
    class Meta:
        verbose_name_plural = 'Ciudades'

class ActividadComercial(models.Model):
    id_actividad = models.AutoField(primary_key=True)
    tipo_actividad = models.CharField('Tipo de actividad', max_length=100)
    detalles = models.CharField(max_length=250)
    def __str__(self):
        return '%s' %(self.tipo_actividad)
    class Meta:
        verbose_name = 'Actividad Comercial'
        verbose_name_plural = 'Actividades Comerciales'

CHOICE_GENERO = (('Masculino', 'Masculino'), ('Femenino', 'Femenino'),)
class Perfil(models.Model):
    id_perfil = models.AutoField(primary_key=True)
    cedula = models.CharField(null=True, blank=True, max_length=10)
    fecha_nacimiento = models.DateField(null=True, blank=True, verbose_name='Fecha de nacimiento')
    telefono = models.CharField(null=True, blank=True, max_length=10)
    genero = models.CharField(null=True, blank=True, max_length=10, choices=CHOICE_GENERO)
    ruta_foto = models.ImageField(null=True, blank=True, upload_to='usuarioFoto', verbose_name='Ruta de foto')
    id_usuario = models.ForeignKey(User)
    sim_dispositivo = models.CharField(null=False, max_length=30)
    def __str__(self):
        return '%s' %(self.id_usuario)
    class Meta:
        verbose_name_plural = 'Perfiles'

class Transportista(models.Model):
    id_transportista = models.AutoField(primary_key=True)
    tipo_transporte = models.CharField('Tipo de transporte', max_length=20)
    tipo_servicio = models.CharField('Tipo de servicio', max_length=50)
    id_perfil = models.ForeignKey(Perfil)
    def __str__(self):
        return '%s - %s' %(self.tipo_transporte, self.tipo_servicio)

class HorarioTransportista(models.Model):
    id_horario = models.AutoField(primary_key=True)
    dias = models.CharField(max_length=80)
    horas = models.CharField(max_length=50)
    id_transportista = models.ForeignKey(Transportista)
    def __str__(self):
        return '%s - %s' %(self.dias, self.horas)
    class Meta:
        verbose_name = 'Horario de transportista'
        verbose_name_plural = 'Horarios de transportista'

class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    fechainicio_suscripcion = models.DateField(auto_now=True, verbose_name='Fecha de inicio de suscripcion')
    suscripcion_activa = models.BooleanField(verbose_name='Suscripcion activa')
    fecha_pago = models.DateField(null=True, blank=True)
    detalles_adicionales = models.CharField('Detalles adicionales', null=False, blank=True, max_length=250)
    pedidos_activos = models.NullBooleanField(null=False, blank=True, verbose_name='Pedidos activos')
    valor_suscripcion = models.FloatField(null=True, blank=True, verbose_name='Valor de suscripcion')
    id_perfil = models.ForeignKey(Perfil)
    def __str__(self):
        return '%s' %(self.id_perfil)

class TipoPago(models.Model):
    id_tipo_pago = models.AutoField(primary_key=True)
    tipo_pago = models.CharField('Tipo de pago', max_length=50)
    observaciones = models.CharField(max_length=250)
    def __str__(self):
        return '%s' %(self.tipo_pago)
    class Meta:
        verbose_name = 'Tipo de pago'
        verbose_name_plural = 'Tipos de pago'

class TipoPedido(models.Model):
    id_tipo_pedido = models.AutoField(primary_key=True)
    tipo_pedido = models.CharField('Tipo de pedido', max_length=50)
    observaciones = models.CharField(max_length=250)
    def __str__(self):
        return '%s' %(self.tipo_pedido)
    class Meta:
        verbose_name = 'Tipo de pedido'
        verbose_name_plural = 'Tipos de pedido'

class Direccion(models.Model):
    id_direccion = models.AutoField(primary_key=True)
    calle_principal = models.CharField('Calle principal', blank=True, max_length=100)
    calle_secundario = models.CharField('Calle secundaria', blank=True, max_length=100)
    numero_casa = models.CharField(null=True, blank=True, max_length=5)
    latitud = models.CharField(max_length=100)
    longitud = models.CharField(max_length=100)
    referencia = models.CharField(max_length=250)
    id_perfil = models.ForeignKey(Perfil)
    def __str__(self):
        return '%s - %s' %(self.calle_principal, self.calle_secundario)
    class Meta:
        verbose_name_plural = 'Direcciones'

class Pedido(models.Model):
    id_pedido = models.AutoField(primary_key=True)
    fecha = models.DateField()
    estado = models.CharField(max_length=50)
    valor = models.FloatField()
    id_tipo_pago = models.ForeignKey(TipoPago)
    id_tipo_pedido = models.ForeignKey(TipoPedido)
    id_trasportista = models.ForeignKey(Transportista)
    id_cliente = models.ForeignKey(Cliente)
    id_direccion = models.ForeignKey(Direccion)
    def __str__(self):
        return '%s - %s - %s' %(self.fecha, self.estado, self.valor)

class Categoria(models.Model):
    id_categoria = models.AutoField(primary_key=True)
    categoria = models.CharField(max_length=150)
    detalles = models.CharField(max_length=250)
    def __str__(self):
        return '%s' %(self.categoria)

class Proveedor(models.Model):
    id_proveedor = models.AutoField(primary_key=True)
    nombre_comercial = models.CharField('Nombre comercial', max_length=100)
    razon_social = models.CharField(max_length=20)
    telefono = models.CharField(max_length=10)
    telefono_contacto = models.CharField(null=True, blank=True, max_length=10)
    dias_atencion = models.CharField('Dias de atención', max_length=80)
    horario_atencion = models.CharField('Horario de atención', max_length=50)
    prioridad = models.IntegerField()
    activo = models.BooleanField()
    hora_pico = models.TimeField(null=True, blank=True, verbose_name='Hora pico')
    me_gusta = models.IntegerField(null=True, blank=True)
    id_ciudad = models.ForeignKey(Ciudad)
    id_perfil = models.ForeignKey(Perfil)
    id_actividad = models.ForeignKey(ActividadComercial)
    id_categoria = models.ForeignKey(Categoria)
    id_direccion = models.ForeignKey(Direccion)
    def __str__(self):
        return '%s' %(self.nombre_comercial)
    class Meta:
        verbose_name_plural = 'Proveedores'

class Valoracion(models.Model):
    id_valoracion = models.AutoField(primary_key=True)
    valor_servicio = models.IntegerField(verbose_name='valor de servicio')
    valor_proveedor = models.IntegerField(verbose_name='valor de proveedor')
    valor_transportista = models.IntegerField(verbose_name='valor de transportista')
    valoracion_general = models.IntegerField(verbose_name='Valoracion general')
    observaciones = models.CharField(max_length=200)
    id_proveedor = models.ForeignKey(Proveedor)
    id_transportista = models.ForeignKey(Transportista)
    id_pedido = models.ForeignKey(Pedido)
    id_cliente = models.ForeignKey(Cliente)
    def __str__(self):
        return '%s - %s - %s' %(self.valor_servicio, self.valor_proveedor, self.valor_transportista)
    class Meta:
        verbose_name_plural = 'Valoraciones'

class RedSocial(models.Model):
    id_red_social = models.AutoField(primary_key=True)
    nombre_red_social = models.CharField('Nombre de red social', max_length=50)
    cuenta_persona = models.CharField('Cuenta personal', max_length=100)
    id_proveedor = models.ForeignKey(Proveedor)
    def __str__(self):
        return '%s - %s' %(self.nombre_red_social, self.cuenta_persona)
    class Meta:
        verbose_name = 'Red social'
        verbose_name_plural = 'Redes sociales'

class Menu(models.Model):
    id_menu = models.AutoField(primary_key=True)
    nombre_producto = models.CharField('Nombre del producto', max_length=200)
    foto = models.ImageField(upload_to='menuFoto', verbose_name='Foto')
    descripcion = models.CharField(max_length=200)
    cantidad_diaria = models.IntegerField(verbose_name='Cantidad diaria')
    venta_aproximada = models.IntegerField(verbose_name='Venta aproximada')
    hora_mayor_venta = models.TimeField(verbose_name='Hora de mayo venta')
    cantidad_mayor_venta = models.IntegerField(verbose_name='Cantidad de mayor venta')
    existencia_actual = models.IntegerField(verbose_name='Existencia actual')
    valor = models.FloatField()
    id_proveedor = models.ForeignKey(Proveedor)
    def __str__(self):
        return "%s" %(self.nombre_producto)
    class Meta:
        verbose_name_plural = 'Menu'

class Ingrediente(models.Model):
    id_ingrediente = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    tipo = models.CharField(null=True, blank=True, max_length=20)
    disponible = models.BooleanField()
    valor = models.FloatField(null=True, blank=True)
    id_menu = models.ForeignKey(Menu)
    def __str__(self):
        return '%s - %s' %(self.nombre, self.tipo)

class DetallePedido(models.Model):
    id_detalle = models.AutoField(primary_key=True)
    valor = models.FloatField()
    observaciones = models.CharField(max_length=250)
    cantidad_solicitada = models.IntegerField()
    id_pedido = models.ForeignKey(Pedido)
    id_proveedor = models.ForeignKey(Proveedor)
    id_menu = models.ForeignKey(Menu)
    id_ingrediente  = models.ForeignKey(Ingrediente, null=True, blank=True)
    def __str__(self):
        return '%s' %(self.id_pedido)
    class Meta:
        verbose_name = 'Detalle de pedido'
        verbose_name_plural = 'Detalles de pedido'

class Sucursal(models.Model):
    id_sucursal = models.AutoField(primary_key=True)
    nombre_sucursal = models.CharField(max_length=100)
    razon_social = models.CharField(max_length=200)
    telefono = models.CharField(max_length=10)
    telefono_contacto = models.CharField(null=True, blank=True, max_length=10)
    dias_atencion = models.CharField(max_length=80)
    hora_atencion = models.CharField(max_length=50)
    prioridad = models.IntegerField()
    activo = models.BooleanField()
    hora_pico = models.TimeField()
    me_gusta = models.IntegerField()
    id_ciudad = models.ForeignKey(Ciudad)
    id_proveedor = models.ForeignKey(Proveedor)
    id_direccion = models.ForeignKey(Direccion)
    def __str__(self):
        return '%s' %(self.nombre_sucursal)
    class Meta:
        verbose_name_plural = 'Sucursales'

class Contrato(models.Model):
    id_contrato = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=20)
    detalles = models.CharField(max_length=250)
    vigencia = models.IntegerField()
    fecha_inicio = models.DateField()
    fechaFin = models.DateField()
    valor = models.FloatField()
    ruta_contrato = models.FileField(null=True, blank=True, upload_to='filesContratos')
    id_proveedor = models.ForeignKey(Proveedor, null=True, blank=True)
    id_sucursal = models.ForeignKey(Sucursal, null=True, blank=True)
    def __str__(self):
        return '%s - %s' %(self.codigo, self.vigencia)

class Coordenadas(models.Model):
    id_coordenadas = models.AutoField(primary_key=True)
    latitud = models.CharField(max_length=100)
    longitud = models.CharField(max_length=100)
    id_perfil = models.ForeignKey(Perfil)
    def __str__(self):
        return '%s - %s' %(self.latitud, self.longitud)
    class Meta:
        verbose_name_plural = 'Coordenadas'
        verbose_name = 'Coordenada'



class UbicacionMovil():
    id_coordenadas = models.AutoField(primary_key=True)
    fecha = models.DateField(auto_now=True, null=True, blank=True, verbose_name='Fecha de recorrido')
    hora = models.TimeField(null=True, blank=True, verbose_name='Hora de recorrido')
    estado = models.BooleanField(verbose_name='Activo')
    latitud = models.CharField(max_length=100)
    longitud = models.CharField(max_length=100)
    id_perfil = models.ForeignKey(Perfil)

    def __str__(self):
        return '%s - %s' % (self.latitud, self.longitud)