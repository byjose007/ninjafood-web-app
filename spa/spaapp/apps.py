from django.apps import AppConfig
from django.db.models.signals import post_migrate, post_init
from spaapp.db_triggers import create_partition_triggers



class MyAppConfig(AppConfig):
    name = 'spaapp'
    verbose_name = "spaapp"

    def ready(self):
        post_migrate.connect(create_partition_triggers, sender=self)