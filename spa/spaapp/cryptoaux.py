import bcrypt as bcr


def get_hashed_password(plain_text_password):
    # Hash a password for the first time
    #   (Using bcrypt, the salt is saved into the hash itself)
    print(plain_text_password)
    return bcr.hashpw(plain_text_password.encode('UTF_8'), bcr.gensalt())


def check_password(plain_text_password, hashed_password):
    # Check hased password. Useing bcrypt, the salt is saved into the hash itself
    return bcr.checkpw(plain_text_password.encode('UTF_8'), hashed_password)
