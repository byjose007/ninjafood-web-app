from django.db import connection



def create_partition_triggers(**kwargs):
    print('  (re)creating partition triggers for myApp...')
    trigger_sql = "CREATE OR REPLACE FUNCTION spaapp.insertar_perfil() RETURNS TRIGGER AS $$\
	BEGIN\
		INSERT INTO spaapp_perfil (id_usuario_id,ruta_foto, sim_dispositivo) VALUES (NEW.id,'usuarioFoto/userDefault.jpg',0);\
		RETURN NULL;\
	END;\
$$ LANGUAGE plpgsql;\
DROP TRIGGER IF EXISTS trigger_perfil ON spaapp.auth_user;\
CREATE TRIGGER trigger_perfil\n\
AFTER INSERT ON spaapp.auth_user\n\
	FOR EACH ROW EXECUTE PROCEDURE spaapp.insertar_perfil();"

    cursor = connection.cursor()
    cursor.execute(trigger_sql)


    trigger_sql = "CREATE OR REPLACE FUNCTION spaapp.insertar_menu() RETURNS TRIGGER AS $$\
	BEGIN\
		INSERT INTO spaapp_menu (id_proveedor_id) VALUES (NEW.id_proveedor);\
		RETURN NULL;\
	END;\
$$ LANGUAGE plpgsql;\
DROP TRIGGER IF EXISTS trigger_menu ON spaapp.spaapp_proveedor;\
CREATE TRIGGER trigger_menu\n\
AFTER INSERT ON spaapp.spaapp_proveedor\n\
	FOR EACH ROW EXECUTE PROCEDURE spaapp.insertar_menu();"
    cursor.execute(trigger_sql)

    print('  Done creating partition triggers.')