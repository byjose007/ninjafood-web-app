# from views import CrearPerfil


from django.conf.urls import include, url, patterns
from django.contrib.auth.views import login, logout_then_login
from . import views
# from spaapp.views import CrearPerfil, PerfilUpdate, PerfilList  #  GENERABA UN ERROR AL LISTAR PLATOS
from spaapp.views import CrearPerfil, PerfilUpdate
from ubicacionApp import views as U
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
rest_base_url = "api/rest/"
router.register(rest_base_url + "proveedor", views.ProveedorViewSet)
router.register(rest_base_url + "sucursal", views.SucursalViewSet)
router.register(rest_base_url + "plato", views.PlatoViewSet)
router.register(rest_base_url + "menu", views.MenuViewSet)
router.register(rest_base_url + "coordenadas", U.CoordenadasViewSet)
router.register(rest_base_url + "tamanioplato", views.TamanioPlatoViewSet)
router.register(rest_base_url + "categorias", views.CategoriasViewSet)
router.register(rest_base_url + "platoCategoria", views.PlatoCategoriaViewSet)
router.register(rest_base_url + "pedidoEstado", views.PedidoEstadoViewSet)
router.register(rest_base_url + "pedidoEstadoProv", views.PedidoEstadoProvViewSet)
router.register(rest_base_url + "pedido", views.PedidoViewSet)
router.register(rest_base_url + "pedidoPendiente", views.PedidoPendienteViewSet)
router.register(rest_base_url + "detallepedido", views.ListaDetallePedidos)
router.register(rest_base_url + "detallepedidoMovil", views.ListaDetallePedidosMovil)
router.register(rest_base_url + "detallepedidoIngr", views.ListaDetallePedidosIngr)
router.register(rest_base_url + "pedidoMovil", views.PedidoMovilViewSet)
router.register(rest_base_url + "direcciones", views.DirecionesViewSet)
router.register(rest_base_url + "transportista", views.TransportistaViewSet)
router.register(rest_base_url + "pedidoAll", views.PedidoAllViewSet)
router.register(rest_base_url + "sugerencias", views.SugerenciasViewSet)
router.register(rest_base_url + "plato2", views.Plato2ViewSet)
router.register(rest_base_url + "tamanioplato2", views.TamanioPlato2ViewSet)
# JY, Definicion de URL para las categorias principales
#router.register(rest_base_url + "categoriasP", views.CategoriasPViewSet)

from .views import Menu

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^login$', views.login, name="login"),
    url(r'^perfil/claveUser$', views.contraseniaUpdateUser, name="changePass"),

    # registro antes de hacer un pedido
    url(r'^loginUser$', views.loginUser),
    url(r'^registroUser$', views.registroUser),
    url(r'^registro_usuario_view$', views.registro_usuario_view, name="register"),

    url(r'^clienteRegister$', views.clienteRegister),
    url(r'^clienteRead$', views.clienteRead),
    url(r'^cliente/(?P<pk>\d+)/delete/$', views.clienteDelete, name='clienteDelete'),
    # url(r'^clienteUpdate/(?P<pk>\d+)/$', views.clienteUpdate, name='clienteUpdate'),
    url(r'^clienteUpdate/(?P<pk>\d+)/$', views.clienteUpdate2, name='clienteUpdate'),

    url(r'^coordenadasCreate$', views.coordenadasCreate),
    url(r'^coordenadasRead$', views.coordenadasRead),
    url(r'^coordenadas(?P<pk>\d+)/delete/$', views.coordenadasDelete, name='coordenadasDelete'),
    url(r'^coordenadas/Update/(?P<pk>\d+)/$', views.coordenadasUpdate, name='coordenadasUpdate'),

    url(r'^valoracionCreate$', views.valoracionCreate),
    url(r'^valoracionRead$', views.valoracionRead),
    url(r'^valoracion(?P<pk>\d+)/delete/$', views.valoracionDelete, name='valoracionDelete'),
    url(r'^valoracion/Update/(?P<pk>\d+)/$', views.valoracionUpdate, name='valoracionUpdate'),

    url(r'^redsocialCreate$', views.redsocialCreate),
    url(r'^redsocialRead$', views.redsocialRead),
    url(r'^redsocial(?P<pk>\d+)/delete/$', views.redsocialDelete, name='redsocialDelete'),
    url(r'^redsocial/Update/(?P<pk>\d+)/$', views.redsocialUpdate, name='redsocialUpdate'),

    url(r'^contratoCreate$', views.contratoCreate),
    url(r'^contratoRead$', views.contratoRead),
    url(r'^contrato(?P<pk>\d+)/delete/$', views.contratoDelete, name='contratoDelete'),
    url(r'^contrato/Update/(?P<pk>\d+)/$', views.contratoUpdate, name='contratoUpdate'),

    url(r'^perfilRead$', views.perfilRead),
    # url(r'^perfilCreate$', views.perfilCreate),
    url(r'^perfilCreate$', CrearPerfil.as_view()),  ##################################################################
    url(r'^perfil/(?P<pk>\d+)/delete/$', views.perfilDelete, name='perfilDelete'),
    url(r'^perfil/Update/(?P<pk>\d+)/$', views.perfilUpdate, name='perfilUpdate'),

    # Filtros
    url(r'^obtenerPlatos$', views.obtenerPlatos),
    url(r'^obtenerPlato$', views.obtenerPlato),
    url(r'^obtenerSucursales$', views.obtenerSucursales),
    url(r'^verificarSucursales$', views.verificarSucursales),
    # url(r'^obtenerPedidos$', views.obtenerPedidos),
    url(r'^obtenerDireccion$', views.obtenerDireccion),
    url(r'^obtenerPrecio$', views.obtenerPrecio),
    url(r'^obtenerPerfil$', views.obtenerPerfil),
    url(r'^validarCorreo$', views.validarCorreo),
    url(r'^validarUsuario$', views.validarUsuario),
    url(r'^getSucursalesOrdenadas$', views.getSucursalesOrdenadas),

    # url(r'^sucursalCreate$', views.sucursalCreate),
    url(r'^sucursalRead$', views.sucursalRead),
    url(r'^sucursal(?P<pk>\d+)/delete/$', views.sucursalDelete, name='sucursalDelete'),
    url(r'^sucursal/Update/(?P<pk>\d+)/$', views.sucursalUpdate, name='sucursalUpdate'),

    url(r'^platoCreate$', views.platoCreate, name='platoCreate'),
    url(r'^platoRead$', views.platoRead),
    url(r'^plato(?P<pk>\d+)/delete/$', views.platoDelete, name='platoDelete'),
    url(r'^plato/Update/(?P<pk>\d+)/$', views.platoUpdate, name='platoUpdate'),

    url(r'^tamanioCreate$', views.tamanioCreate),
    url(r'^tamanioRead$', views.tamanioRead),
    url(r'^tamanio(?P<pk>\d+)/delete/$', views.tamanioDelete, name='tamanioDelete'),
    url(r'^tamanio/Update/(?P<pk>\d+)/$', views.tamanioUpdate, name='tamanioUpdate'),

    # url(r'^proveedorTest$', views.proveedor_test, name="proveedorTest"), #Prueba de Roles/Permisos
    url(r'^mapa$', views.mapa, name='mapa'),
    url(r'^restaurantes$', views.restaurantes, name='restaurantes'),
    url(r'^guardarPedido$', views.guardarPedido),
    url(r'^menu/$', login_required(Menu.as_view(), login_url=reverse_lazy("login")), name="menu"),
    url(r'^registro$', views.registro, name='registro'),
    url(r'^enviarCo$', views.enviarCodigo, name='validarCuenta'),
    url(r'^verificarC$', views.verificarCodigo),
    url(r'^borrarN$', views.borrarNumeroC),
    url(r'^guardarCelular$', views.guardarCelular),
    url('', include('social.apps.django_app.urls', namespace='social')),
    # url(r'^cerrar/$', logout_then_login, name='logout'),
    # cerrar sesion
    url(r'^cerrarP/$', views.cerrarProveedor),
    url(r'^cerrar/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name="user-logout"),
    url(r'^$', login, {'template_name': 'home.html'}, name="login"),
    url(r'^lista_menu/(?P<sucursal_id>\d+)/$', views.lista_menu),

    # Nicholas API REST
    url(r'^', include(router.urls)),

    # API REST para app movi de usuario
    url('^api/rest/plato/m/(?P<menu_id>[0-9]+)/c/(?P<id_categoria>\w+)/', views.PlatoLista.as_view()),
    url('^api/rest/platoIngredientes/(?P<plato_id>[0-9]+)/', views.PlatoIngredienteLista.as_view()),
    url('^api/rest/listaPlatosCategoria/m/(?P<menu_id>[0-9]+)/', views.listaPlatosCategoria.as_view()),
    url('^api/rest/direccion/(?P<id_usuario>[0-9]+)/', views.ListaDirecciones.as_view()),
    url('^api/rest/removeDireccion/(?P<id_direccion>[0-9]+)/(?P<id_usuario>[0-9]+)/', views.removeDirecciones.as_view()),
    #url('^api/rest/usuario_movil/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)/(?P<first_name>[a-zA-Z0-9\s_.+-]+)/(?P<last_name>[a-zA-Z0-9\s_.+-]+)$',
    url('^api/rest/usuario_movil/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)/(?P<first_name>[^/?]+)/(?P<last_name>[^/?]+)$',
        views.UsuariosMovilViewSet.as_view()),
    #url('^api/rest/actualizar_usuario_movil/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)/(?P<first_name>[a-zA-Z0-9\s_.+-]+)/(?P<last_name>[a-zA-Z0-9\s_.+-]+)/(?P<cedula>[0-9]+)$',
    #    views.ActualizarUsuariosMovilViewSet.as_view()),
    url('^api/rest/actualizar_usuario_movil/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)/(?P<first_name>[^/?]+)/(?P<last_name>[^/?]+)/(?P<cedula>[0-9]+)$',
        views.ActualizarUsuariosMovilViewSet.as_view()),

    url('^api/rest/borrar_numero_movil/(?P<idPerfil>[0-9]+)/(?P<numeroTel>[0-9]+)$',views.borrarNumeroMovilViewSet.as_view()),
    url('^api/rest/comprobar_numero_movil/(?P<idPerfil>[0-9]+)/(?P<numeroTel>[0-9]+)$',views.comprobarNumeroMovilViewSet.as_view()),
    url('^api/rest/sendCodeEmailMovil/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)/(?P<code>[0-9]+)$', views.sendCodeEmailMovilViewSet.as_view()),

    url('^api/rest/loginUsuarioMovil/(?P<email>[a-zA-Z0-9_.+-@$!%*#?&]+)/(?P<pass>[a-zA-Z0-9_.+-@$!%*#?&]+)$',
        views.loginUsuarioMovilViewSet.as_view()),
    # url('^api/rest/registrarUsuarioMovil/(?P<usuario>[a-zA-Z0-9_.+-]+)/(?P<nombres>[a-zA-Z0-9_.+-]+)/(?P<apellidos>[a-zA-Z0-9_.+-]+)/(?P<cedula>[0-9]+)/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)/(?P<pass>[a-zA-Z0-9_.+-@$!%*#?&]+)$',
    #     views.registrarUsuarioMovilViewSet.as_view()),
    url('^api/rest/registrarUsuarioMovil/(?P<nombres>[^/?]+)/(?P<apellidos>[^/?]+)/(?P<cedula>[0-9]+)/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)/(?P<pass>[a-zA-Z0-9_.+-@$!%*#?&]+)$',
        views.registrarUsuarioMovilViewSet.as_view()),
    url(
        '^api/rest/comprobarUsuarioMovil/(?P<usuario>[a-zA-Z0-9_.+-]+)$',
        views.comprobarUsuarioMovilViewSet.as_view()),
    url(
        '^api/rest/comprobarCedulaMovil/(?P<cedula>[0-9]+)$',
        views.comprobarCedulaMovilViewSet.as_view()),
    url(
        '^api/rest/comprobarEmailMovil/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)$',
        views.comprobarEmailMovilViewSet.as_view()),

    url('^api/rest/tamanioPLatoDetalle/(?P<id_plato>[0-9]+)/(?P<id_tamanio>\w+)/', views.ListaTamaniosPlatos.as_view()),
    url('^api/rest/perfil/(?P<id_usuario>[0-9]+)/', views.ListaPerfil.as_view()),
    url('^api/rest/listaPedidos/(?P<id_usuario>[0-9]+)/(?P<pagina>[0-9]+)/(?P<tamanio>[0-9]+)', views.ListaPedidos.as_view()),
    url('^api/rest/totalPedidos/(?P<id_usuario>[0-9]+)/', views.TotalPedidos.as_view()),

    url('^api/rest/recuperarPassMovil/(?P<email>[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)$',
        views.recuperarPassViewSet.as_view()),
    url('^api/rest/categoriasP/',
        views.CategoriasPViewSet.as_view()),


    # para transportista
    url('^api/rest/usuario/(?P<usuario>\w+)/', views.UsuarioTransportistaViewSet.as_view()),
    url('^api/rest/transportistaExistente/(?P<sim>\w+)/(?P<numero>\w+)/',
        views.TransportistaExistenteViewSet.as_view()),
    url('^api/rest/PedidoEntregado/(?P<id_trasportista>\w+)/',
        views.PedidoEntregadoViewSet.as_view()),
    url('^api/rest/Transp/(?P<sim>\w+)/',
        views.TranspViewSet.as_view()),

    # N.P.C para app proveedor
    url('^api/rest/PedidoProveedor/(?P<perfil>\w+)/',
        views.PedidoProveedorViewSet.as_view()),
    url('^api/rest/PedidoProveedorPendiente/(?P<perfil>\w+)/',
        views.PedidoProveedorPendienteViewSet.as_view()),
    url('^api/rest/PedidoDetalleProveedor/(?P<pedido>\w+)/',
        views.PedidoDetalleProveedorViewSet.as_view()),
    url('^api/rest/PedidoDiaProveedor/(?P<perfil>\w+)/',
        views.PedidoDiaProveedorViewSet.as_view()),
    url('^api/rest/getSucursal/(?P<id_usuario>\w+)/', views.SucursalDatosViewSet.as_view()),
    url('^api/rest/AllPedidoProv/(?P<id_sucursal>\w+)/', views.AllPedidoProvViewSet.as_view()),
    url('^api/rest/platoSucursal/(?P<id_sucursal>\w+)/', views.PlatosSucursalViewSet.as_view()),

    # D.A. notificar a usuario
    url(r'^notificarPedidoCliente$', views.notificarPedidoCliente),
    url(r'^validarUsuarioLogin$', views.validarUsuarioLogin),
    url(r'^perfil/validarCuenta$', views.validarCuentaSMS, name='validarCuenta'),

    # D.A. CRUD direcciones
    url(r'^perfil/gestionDireccion$', views.gestionDireccion, name='gestionDirecion'),
    url(r'^direccionCreate$', views.direccionCreate, name='direccionCreate'),
    url(r'^direccion/(?P<pk>\d+)/delete/$', views.direccionDelete, name='direccionDelete'),
    url(r'^direccion/Update/(?P<pk>\d+)/$', views.direccionUpdate, name='direccionUpdate'),
    url(r'^street$', views.getStreet),

    # Para administración de transportistas
    url('^api/rest/PedidosDiaTodos$', views.PedidosDiaTodosViewSet.as_view()),
    url('^api/rest/CoordenadaExiste/(?P<id_transportista>\d+)$', views.CoordenadaExisteViewSet.as_view()),
    url('^api/rest/TranspCoord$', views.TraspCoordenadasViewSet.as_view()),

]
