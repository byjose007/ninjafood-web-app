## DESDE AQUI SE MANIPULAN LOS DATOS QUE SE OBTIENEN DESDE LAS REDES SOCIALES

from social.pipeline.user import user_details
from . import models as mod
from datetime import datetime
from django.contrib.auth.models import User, Group
from django.contrib.auth import login as auth_login


def detalles_usuario(strategy, details, user=None, *args, **kwargs):
    if kwargs['is_new']:
        user_details(strategy, details, user, args, kwargs)
        cliente = Group.objects.get(name="Cliente")
        user.groups.add(cliente)
        user.save()


def get_avatar(request, backend, strategy, details, response, user=None, *args, **kwargs):
    url = None
    if backend.name == 'facebook':
        url = "https://graph.facebook.com/%s/picture?type=large" % response['id']
    if backend.name == 'twitter':
        url = response.get('profile_image_url', '').replace('_normal', '')
    if backend.name == 'google-oauth2':
        url = response['image'].get('url')
        ext = url.split('.')[-1]
    if url:
        perfil = mod.Perfil.objects.get(id_usuario=user)
        today = datetime.now()
        try:
            cliente = mod.Cliente.objects.get(id_perfil=perfil)
        except mod.Cliente.DoesNotExist:
            cliente = None
            cliente = mod.Cliente(fechainicio_suscripcion=today, detalles_adicionales='', pedidos_activos=False,
                                  id_perfil=perfil)
            cliente.save()
        perfil = mod.Perfil.objects.filter(id_usuario=user).update(ruta_foto=url)
