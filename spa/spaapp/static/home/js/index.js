'use strict';

Vue.component('star-rating', {
  template: '#template-star-rating',
  data: function data() {
    return {
      value: null,
      temp_value: null,
      ratings: [1, 2, 3, 4, 5]
    };
  },
  props: {
    'name': String,
    'value': null,
    'id': String,
    'disabled': Boolean,
    'required': Boolean
  },
  methods: {
    star_over: function star_over(index) {
      if (this.disabled) {
        return;
      }

      this.temp_value = this.value;
      this.value = index;
    },
    star_out: function star_out() {
      if (this.disabled) {
        return;
      }

      this.value = this.temp_value;
    },
    set: function set(value) {
      if (this.disabled) {
        return;
      }

      this.temp_value = value;
      this.value = value;
    }
  }
});

new Vue({
  el: '#app'
});
