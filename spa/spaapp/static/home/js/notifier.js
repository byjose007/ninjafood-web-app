/*libreria pra notificaciones*/

/**
$(function(){
            $('#infoBtn').click(function(){
                $.notifier('info','info header','I am info notice message','10000');
            });
            $('#warningBtn').click(function(){
                $.notifier('warning','Warning header','I am a warning notice message','10000');
            });
            $('#dangerBtn').click(function(){
                $.notifier('danger','Danger notice header','I am a danger notice message','10000');
            });
            $('#successBtn').click(function(){

               $.notifier('success','Compra realizada','Puedes verificar el estado de tu <br>compra desde tus pedidos','10000');

            });
            $('#reminderBtn').click(function(){
                $.notifier('reminder','Reminder header','I am a reminder message','10000');
            });
            $('#todoBtn').click(function(){
                $.notifier('todo','Todo hdaer','I am a todo message','10000');
            });
        });
**/

;(function($){
	var counter;
	var soundIn=[];
	var method = {
		init:function(type,title,content,more,duration,url){
			var container = this.createContainer();
			this.createNotification(type,title,content,container,more,duration);
		},
		createContainer:function(){
			if($('#notifier-container').length>0){
				counter += 1;
				return $('#notifier-container');
			} else {
				counter = 0;
				var element = this._createElement('div',{id:'notifier-container',class:'notify container'});
				document.body.appendChild(element);
				return $('#notifier-container');
			}
		},
		createNotification:function(type,title,content,container,more,duration){
			var itemId = 'notifier-item-'+counter;
			var itemEl = this._createElement('div',{class:'notify item ' + type,id:itemId});
			var titleEl = this._createElement('div',{class:'header'});
			var contentEl = this._createElement('div',{class:'content2'});
			var moreEl = this._createElement('div',{class:'text-center '});
			var clsEl = this._createElement('a', {class:'close-btn'});
			var iconEl = this._createElement('div',{class:'img img-' + type});
			
			
			titleEl.innerHTML = title;
			contentEl.innerHTML = content;
			moreEl.innerHTML = more;
			clsEl.innerHTML = 'x';
			clsEl.addEventListener('click', function() {
				if(soundIn.length>0){
					for(var i=0; i<=soundIn.length; i++){
						clearInterval(soundIn[i]);
						soundIn=[];
					}
				}
			   method.closeNotification(itemId);
			});
			itemEl.appendChild(clsEl);
			itemEl.appendChild(titleEl);
			itemEl.appendChild(iconEl);
			itemEl.appendChild(contentEl);
			// itemEl.appendChild(moreEl);
			container.append(itemEl);

			if(more=="repeat" || more=="Repeat"){
				$(itemEl).addClass('show-notifier');
				soundIn.push(setInterval(function () {
				// 	x.audioElement.currentTime=2;
					x.play();

				},400));
			}else{
				itemEl.appendChild(moreEl);
			container.append(itemEl);
				setTimeout(function(){
				$(itemEl).addClass('show-notifier');
			},100);

			if(duration>0){
				setTimeout(function(){
					method.closeNotification(itemId);
				},duration);
			}
			}

		},
		closeNotification(currentEl){
			$('#' + currentEl).removeClass('show-notifier');
			setTimeout(function() {
				$('#' + currentEl).remove();
		      }, 600);
		},
		_createElement : function(el,attrs){
			var element = document.createElement(el);
			 for (var prop in attrs) {
				 element.setAttribute(prop, attrs[prop]);
			 }
			 return element;
			
		}
	};
	$.notifier = function(type,title,content,more,duration){
		return method.init(type,title,content,more,duration);
	};
})(jQuery);