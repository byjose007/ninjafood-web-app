from django.contrib import admin

from .models import *
from django.contrib.auth.models import Permission

admin.site.register(Perfil)
admin.site.register(Transportista)
admin.site.register(HorarioTransportista)
admin.site.register(TipoPago)
admin.site.register(Proveedor)
admin.site.register(Pedido)
admin.site.register(TipoPedido)
admin.site.register(Coordenadas)
admin.site.register(Valoracion)
admin.site.register(Categoria)
admin.site.register(ActividadComercial)
admin.site.register(RedSocial)
admin.site.register(Menu)
admin.site.register(Ingrediente)
admin.site.register(DetallePedido)
admin.site.register(Ciudad)
admin.site.register(Direccion)
admin.site.register(Contrato)
admin.site.register(Cliente)
admin.site.register(Sucursal)
admin.site.register(Permission)
admin.site.register(Plato)
admin.site.register(PlatoCategoria)
admin.site.register(PlatoIngrediente)
admin.site.register(TamanioPlato)
admin.site.register(Tamanio)
admin.site.register(CatalogoEstado)
admin.site.register(Modificardetallepedido)
admin.site.register(Sugerencia)
admin.site.register(UserSurcursal)





