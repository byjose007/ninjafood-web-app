from django.core.exceptions import ValidationError
from django.forms import Form, ModelForm, PasswordInput, CharField
#import spaapp.models
from django import forms
#from models import TipoUsuario, Persona, Usuario
#from . import cryptoaux as ca
from django.core.validators import validate_email
# from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from . import models as mod
from spa import settings
from django.contrib.auth import authenticate
import re
from django.core.exceptions import NON_FIELD_ERRORS
from django.template.defaultfilters import filesizeformat
from mimetypes import MimeTypes
from urllib.request import pathname2url


from django.contrib.auth import login as auth_login

class ExistentUsernameField(CharField):
    def validate(self, value):
        if len(mod.User.objects.filter(username__exact=value)) == 1:
            return True
        else:
            raise forms.ValidationError(
                    ('El ssuario %(value)s no existe'),
                    code='usuario_no_existente',
                    params={'value': value},
                    )
        # return len(mod.Usuario.objects.filter(nick__exact=value)) == 1

#Valida si el campo esta lleno
class FilledCharField(CharField):
    def validate(self, value):
        if len(value) > 0:
            return True
        else:
            raise forms.ValidationError(
                ('Se requiere llenar este campo'),
                code='campo_requerido',
                params={'value': value},
            )


#Valida password mas de 6 caracteres
class PassCharField(CharField):
    def validate(self, value):
        if len(value) > 5:
            return True
        else:
            raise forms.ValidationError(
                ('Mínimo 6 caracteres'),
                code='campo_requerido',
                params={'value': value},
            )
        return

class UpdatePassCharField(PassCharField):
    def validate(self, value):
        if len(value) == 0:
            return True
        elif len(value) > 5:
            return True
        else:
            raise forms.ValidationError(
                ('Mínimo 6 caracteres'),
                code='campo_requerido',
                params={'value': value},
            )



#Valida que la letra inicial sea del abecedario minúsculo o mayúsculo
class ValidNameField(FilledCharField):
    def validate(self, value):
        if len(value) > 0:
            if value.strip() == value:
                first_char = value[0]
                if ('a' <= first_char <= 'z') or ('A' <= first_char <= 'Z'):
                    if len(value) > 2:
                        return True
                    else:
                        raise  ValidationError(
                            ('Los nombres deben de contener mínimo 3 caracteres'),
                            code='nombre_invalido',
                            params={'value':value},
                        )
                else:
                    raise ValidationError(
                        ('No es un nombre válido'),
                        code='nombre_invalido',
                        params={'value': value},
                    )

            else:
                raise ValidationError(
                    ('No es un nombre válido'),
                    code='nombre_invalido',
                    params={'value': value},
                )
        else:
            raise ValidationError(
                ('Se requiere llenar éste campo'),
                code='campo_requerido',
                params={'value': value},
            )

#Este form no vale FormLogin
class FormLoginOld(Form):
    #username = FilledCharField()
    #password = FilledCharField(widget=forms.PasswordInput)abel='',required=True,min_length=5, widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'Contraseña'})
    username = forms.CharField(label='',required=True,min_length=5,widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Nombre de usuario'}))
    password = forms.CharField(label='', required=True, min_length=5, widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'Contraseña'}))

    '''class Meta:
        model = Usuario'''
    def clean_username(self):
        """Comprueba que no exista un username igual en la db"""
        username = self.cleaned_data['username']
        password = self.data['password']
        users = User.objects.filter(username=username, password=password)
        if users.count() == 0:
            #raise forms.ValidationError('Nombre de usuario o clave inválidos')
            raise ValidationError(
                ('Nombre de usuario o clave incorrectos'),

            )

        return username

class nonExistentUsernameField(CharField):
    def validate(self, value):
        #print("neuf: ", len(Usuario.objects.filter(nick__exact=value)) == 0)
        if len(mod.User.objects.filter(username__exact=value)) == 0:
            if re.fullmatch("^[a-zA-Z]$|^[a-zA-Z][a-zA-Z0-9_]*[a-zA-Z0-9]$", value) is not None:
                return True
            elif re.fullmatch("^[a-zA-Z].*$", value) is None:
                raise ValidationError(
                    ('Primer caracter debe ser una letra'),
                )
            elif re.fullmatch("^.*[a-zA-Z0-9]$", value) is None:
                raise ValidationError(
                    ('Último caracter debe ser letra o número'),
                )
            else:
                raise ValidationError(
                    ('El nombre de usuario solo puede contener letras, numeros y guiones bajos.')
                )
        else:
            raise ValidationError(
                    ('El usuario %(value)s ya existe: '),
                    code='usuario_existente',
                    params={'value': value},
            )

class numericFilledCharField(CharField):
    def contieneNumero(self, valor):
        return True
        '''try:
            item = iter(valor)
            if type(item) == str:
                if item >= '0' and item <= '9':
                    return True
                else:
                    for item in valor:
                        if item >= '0' and item <= '9':
                            return True
                    return False
            else:
                return False
        except TypeError:
            return type(valor) in (int, double, float)'''

    def validate(self, value):
        #print("nfcf: ", len(value) > 0 and self.contieneNumero(value))
        return len(value) > 0 and self.contieneNumero(value)

class validateBooleanField(forms.BooleanField):
    def validate(self, value):
        return True

class validateFileField(forms.FileField):
    def validate(self, value):
        return True


class cedulaInput(CharField):
    def validar_cedula(self, cedula, digito_comprobar):
        multi = [2, 1, 2, 1, 2, 1, 2, 1, 2]
        posicion = 0
        suma = 0
        for caracter in cedula:
            digito = int(caracter)
            calculo = (digito * multi[posicion])
            if calculo >= 10:
                calculo = calculo - 9
            # print(calculo)
            suma = suma + calculo
            posicion = posicion + 1
        if suma % 10 == 0:
            return int(digito_comprobar) == 0
        else:
            # print(suma)
            suma = suma % 10
            # print(suma)
            suma = 10 - suma
            # print(suma)
            return int(digito_comprobar) == suma

    def validate(self, value):
        if re.search("^(((\d){9,10})|((\d){9}-(\d)))$", value) is None:
            raise ValidationError(
                ('Número de cédula incorrecto')
            )
        else:
            if re.search("^(\d){9}$", value) is None:
                if re.search("^(\d){10}$", value) is not None:
                    cedula = value[0:9]
                    digito_comprobar = value[9]
                    if not self.validar_cedula(cedula, digito_comprobar):
                        raise ValidationError(
                            ('Número de cédula incorrecto')
                        )
                    else:
                        cedulas = mod.Perfil.objects.filter(cedula=value)
                        if len(cedulas) == 0:
                            return True
                        else:
                            raise ValidationError(
                                ('Número de cédula ya registrado')
                            )
                else:
                    cedula = value[0:9]
                    digito_comprobar = value[10]
                    if not self.validar_cedula(cedula, digito_comprobar):
                        raise ValidationError(
                            ('Número de cédula incorrecto')
                        )
                    else:
                        cedulas = mod.Perfil.objects.filter(cedula=cedula+digito_comprobar)
                        if len(cedulas) == 0:
                            return True
                        else:
                            raise ValidationError(
                                ('Número de cédula ya registrado')
                            )
            else:
                return True


class fotoField(forms.FileField):

    def clean(self, *args, **kwargs):
        data = super(fotoField, self).clean(*args, **kwargs)
        #file = data.file
        try:
            mime = MimeTypes()
            url = pathname2url(data.name)
            #content_type = file.content_type
            content_type = mime.guess_type(url)[0]
            correct_type = False
            for image_type in settings.IMAGE_CONTENT_TYPES:
                if re.search(image_type, content_type) is not None:
                    correct_type = True
                    break
            if correct_type:
                if data.size > settings.MAX_PHOTO_SIZE:
                    #print("Imagen demaciado grande")
                    raise forms.ValidationError(
                        ('Por favor sube una foto con un tamaño menor a %(max)s. El Tamaño de la foto subida es %(size)s.'),
                        code="error_tamano_fotofield",
                        # params={"max": "1 MB", "size": "muy grande"}
                        params={"max": filesizeformat(settings.MAX_PHOTO_SIZE), "size": filesizeformat(data.size)}
                    )# % (filesizeformat(settings.MAX_PHOTO_SIZE), filesizeformat(data.size)))
            else:
                raise forms.ValidationError(
                    ('Tipo de archivo no es imagen.'),
                    code="error_tipo_archivo_fotofield",
                    params={}
                )
        except AttributeError as ae:
            #except AttributeError:
            #print("ERROR: %s" % (ae))
            #raise forms.ValidationError('Error de Servidor. Intenta de nuevo. %s' % (ae))
            raise forms.ValidationError(
                ('Error de Servidor. Intenta de nuevo. %(error)s'),
                code="error_atributo_fotofield_servidor",
                params={"error": ae}
            )
        return data


class validateModelChoiceField(forms.ModelChoiceField):

    def validate(self, value):
        return True


# class UsuarioForm(Form):
#     celular = numericFilledCharField()
#     nick = nonExistentUsernameField()
#     password = FilledCharField(widget=forms.PasswordInput)
#     activo = validateBooleanField()
#     rutaFoto = validateFileField()
#     idPersona = validateModelChoiceField(mod.Persona.objects.all())
#     idTipoUsuario = validateModelChoiceField(mod.TipoUsuario.objects.all())
#
#     '''class Meta:
#         model = Usuario
#         fields = ['celular', 'nick', 'password', 'activo', 'rutaFoto', 'idPersona', 'idTipoUsuario']'''
#
#     def save(self, commit=True):
#         data = self.cleaned_data
#         passwd = data["password"]
#         passwd = ca.get_hashed_password(passwd)
#         data["password"] = passwd
#         data["activo"] = True
#         u = mod.Usuario(**data)
#         u.save()


# class TipoUsForm(ModelForm):
#     class Meta:
#         model = mod.TipoUsuario
#         fields = '__all__'


# class UpdateUsuarioForm(ModelForm):
#
#     class Meta:
#         model = mod.Usuario
#         fields = ['celular', 'nick', 'password', 'activo', 'rutaFoto', 'idPersona', 'idTipoUsuario']
#
#     def save(self, commit=True, instance=None):
#         data = self.cleaned_data
#         passwd = data["password"]
#
#         if len(passwd) != 60:
#             passwd = ca.get_hashed_password(passwd)
#             data["password"] = passwd
#
#         if instance is None:
#             u = mod.Usuario(**data)
#         else:
#             u = mod.Usuario(instance.idUsuario, **data)
#
#         if commit:
#             u.save()


class ProveedorForm(ModelForm):
    class Meta:
        model = mod.Proveedor
        fields = '__all__'

class ClienteForm(ModelForm):
    class Meta:
            model = mod.Cliente
            exclude = ('id_perfil', 'suscripcion_activa', 'pedidos_activos', 'fecha_pago', 'valor_suscripcion',)
            fields = '__all__'

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',)


        # class entradaFechaWidget(forms.DateInput):
        #     input_type = "date"

class entradaFechaWidget(forms.TextInput):
    input_type = "date"

    def __init__(self, **kwargs):
        super(entradaFechaWidget, self).__init__(kwargs)


# class PerfilForm(ModelForm):
#     class Meta:
#         model = mod.Perfil
#         #exclude = ('id_usuario',)
#         fields = [
#             'cedula',
#             'fecha_nacimiento',
#             'telefono',
#             'genero',
#             'ruta_foto',
#             'sim_dispositivo',
#         ]
#
#         widgets = {'cedula': CedulaInput(attrs={'class': 'form-control fields-pl','placeholder': 'Cedula'}),
#                    'fecha_nacimiento': EntradaFechaWidget(attrs={'class': 'form-control fields-pl','placeholder': 'Fecha de Nacimiento'}),
#                    'telefono': forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Teléfono'}),
#                    'genero': forms.Select(attrs={'class': 'form-control fields-pl', 'placeholder': 'Genero'}),
#                    'ruta_foto': forms.FileInput(attrs={'placeholder': 'Foto'}),
#                    'sim_dispositivo': forms.HiddenInput()}



class PerfilForm(Form):

    id_perfil= CharField(label="", required=True, widget=forms.HiddenInput())
    #cedula = CharField(label="", required=True, widget=forms.HiddenInput())

    # cedula = cedulaInput(label="",required=True,widget=forms.TextInput(
    #     attrs={'class': 'form-control fields-pl','placeholder': 'Cedula'}))

    #fecha_nacimiento= CharField(label="", required=True, widget=forms.HiddenInput())

    # fecha_nacimiento= forms.DateField(label="",required=True,widget=entradaFechaWidget(
    #      format=('%d-%m-%Y'), attrs={'class': 'form-control fields-pl','placeholder': 'Fecha de Nacimiento'}))

    #telefono = forms.CharField(label="",required=True,widget=forms.TextInput(
    #    attrs={'class': 'form-control fields-pl', 'placeholder': 'Teléfono'}))

    #genero = forms.CharField(label="",required=True,widget=forms.Select(choices=mod.CHOICE_GENERO,
    #    attrs={'class': 'form-control fields-pl', 'placeholder': 'Genero'}))

    ruta_foto=fotoField(required=False,widget=forms.FileInput(attrs={'placeholder': 'Foto'}))

    def __init__(self, post_req=None, files_req=None, **kwargs):
        if (files_req.__class__.__name__ == "NoneType"):
            print("")
        else:
            if (len(files_req) == 0):
                files_req = None
        if post_req is not None and files_req is not None:
            super(PerfilForm, self).__init__(post_req, files_req, **kwargs)
        elif post_req is not None:
            super(PerfilForm, self).__init__(post_req, **kwargs)
        elif files_req is not None:
            super(PerfilForm, self).__init__(files_req, **kwargs)
        else:
            super(PerfilForm, self).__init__(**kwargs)

    def save(self):
        data = self.cleaned_data
        data2 = {}
        for llave, valor in data.items():
            if llave != 'id_perfil':
                if llave == 'ruta_foto' and (valor == '' or valor is None):
                    continue
                data2[llave] = valor
        perfil = mod.Perfil.objects.get(id_perfil=data['id_perfil'])
        perfil.__dict__.update(**data2)
        perfil.save()
        # def __init__(self, logged_in  _user, *args, **kwargs):
        #     super(self.__class__, self).__init__(*args, **kwargs)
        #     self.fields['my_user_field'].initial = logged_in_user



class ProveedorPerfilForm(ModelForm):
    class Meta:
        model = mod.Perfil
        #exclude = ('id_usuario',)
        fields = '__all__'
        widgets = {'cedula': forms.HiddenInput(), 'fecha_nacimiento': forms.HiddenInput(),
                   'genero': forms.HiddenInput(), 'id_usuario': forms.HiddenInput(), 'sim_dispositivo': forms.HiddenInput(),
                   #'telefono': forms.HiddenInput(),
                   'ruta_foto': forms.FileInput(attrs={'placeholder': 'Foto'})
                   }
        field_classes = {
            'ruta_foto': fotoField
        }

###################################### GESTION DE DIRECCIONES PARA CLIENTES ####################################### INICIO

class direccionCreateGD(Form):
    user = CharField(label="", required=True, widget=forms.HiddenInput())

    cPrincipal = ValidNameField(label="", required=True,
                                 widget=forms.TextInput(attrs={'id':'cPrincipal','class': 'form-control fields-pl', 'placeholder': 'Calle Principal'}))

    cSecundaria = ValidNameField(label="", required=True,
                                widget=forms.TextInput(attrs={'id':'cSecundaria','class': 'form-control fields-pl', 'placeholder': 'Calle Secundaria'}))

    nroCasa = CharField(label="", required=False,
                                widget=forms.TextInput(attrs={'id':'nroCasa','class': 'form-control fields-pl', 'placeholder': 'Número de Casa'}))

    latitud = CharField(label="", required=True, widget=forms.HiddenInput(attrs={'id':'latitud'}))

    longitud = CharField(label="", required=True, widget=forms.HiddenInput(attrs={'id':'longitud'}))

    referencia = ValidNameField(label="", required=True,
                                widget=forms.TextInput(attrs={'id':'referencia','class': 'form-control fields-pl', 'placeholder': 'Referencia', 'size': '150'}))



    def clean(self):
        clean_data = super(direccionCreateGD, self).clean()
        user = clean_data.get("user")
        cprincipal = clean_data.get("cPrincipal")
        csecundaria = clean_data.get("cSecundaria")
        nroCasa = clean_data.get("nroCasa")
        latitud = clean_data.get("latitud")
        longitud = clean_data.get("longitud")
        referencia = clean_data.get("referencia")

        if cprincipal is None:
            self.cPrincipal="N/A"
        elif csecundaria is None:
            self.cSecundaria="N/A"

        if longitud is None and latitud is None:
            geolocation = forms.ValidationError("Por favor seleccione una dirección en el mapa para tener una mejor eficiencia en la entrega de su pedido.")
            self.add_error('longitud', geolocation)
            self.add_error('latitud', geolocation)


    def save(self):
        data_orig = self.cleaned_data
        cp=data_orig['cPrincipal']
        cs=data_orig['cSecundaria']
        nc=data_orig['nroCasa']
        lat=data_orig['latitud']
        lon=data_orig['longitud']
        ref=data_orig['referencia']
        per=data_orig['user']
        direc=mod.Direccion(calle_principal=cp, calle_secundaria=cs, numero_casa=nc, latitud=lat, longitud=lon, referencia=ref, id_perfil_id=per)
        direc.save()


class direccionUpdateGD(Form):
    user = CharField(label="", required=True, widget=forms.HiddenInput())

    cPrincipal = ValidNameField(label="", required=True,
                                 widget=forms.TextInput(attrs={'id':'cPrincipal','class': 'form-control fields-pl', 'placeholder': 'Calle Principal'}))

    cSecundaria = ValidNameField(label="", required=True,
                                widget=forms.TextInput(attrs={'id':'cSecundaria','class': 'form-control fields-pl', 'placeholder': 'Calle Secundaria'}))

    nroCasa = CharField(label="", required=True,
                                widget=forms.TextInput(attrs={'id':'nroCasa','class': 'form-control fields-pl', 'placeholder': 'Número de Casa'}))

    latitud = CharField(label="", required=True, widget=forms.HiddenInput(attrs={'id':'latitud'}))

    longitud = CharField(label="", required=True, widget=forms.HiddenInput(attrs={'id':'longitud'}))

    referencia = ValidNameField(label="", required=True,
                                widget=forms.TextInput(attrs={'id':'referencia','class': 'form-control fields-pl', 'placeholder': 'Referencia', 'size': '150'}))



    def clean(self):
        clean_data = super(direccionUpdateGD, self).clean()
        user = clean_data.get("user")
        cprincipal = clean_data.get("cPrincipal")
        csecundaria = clean_data.get("cSecundaria")
        nroCasa = clean_data.get("nroCasa")
        latitud = clean_data.get("latitud")
        longitud = clean_data.get("longitud")
        referencia = clean_data.get("referencia")
        if nroCasa is None:
            self.nroCasa="N/A"
        elif cprincipal is None:
            self.cPrincipal="N/A"
        elif csecundaria is None:
            self.cSecundaria="N/A"

        if longitud is None and latitud is None:
            geolocation = forms.ValidationError("Por favor seleccione una dirección en el mapa para tener una mejor eficiencia en la entrega de su pedido.")
            self.add_error('longitud2', geolocation)
            self.add_error('latitud2', geolocation)


    def save(self):
        data_orig = self.cleaned_data
        cp=data_orig['cPrincipal']
        cs=data_orig['cSecundaria']
        nc=data_orig['nroCasa']
        lat=data_orig['latitud']
        lon=data_orig['longitud']
        ref=data_orig['referencia']
        per=data_orig['user']

        dir = mod.Direccion.objects.get(pk=per)
        dir.calle_principal = cp
        dir.calle_secundaria= cs
        dir.numero_casa = nc
        dir.latitud = lat
        dir.longitud = lon
        dir.referencia = ref

        dir.save()
###################################### GESTION DE DIRECCIONES PARA CLIENTES ####################################### FIN


class ProveedorDireccionForm(ModelForm):
    class Meta:
        model = mod.Direccion
        fields = '__all__'
        #exclude = ('latitud','longitud')
        #widgets = {'id_perfil': forms.HiddenInput()}
        #'latitud': forms.HiddenInput(), 'longitud': forms.HiddenInput(),
        widgets = {'id_perfil': forms.HiddenInput(),'latitud': forms.HiddenInput(), 'longitud': forms.HiddenInput(),
                   'calle_principal': forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Calle Principal'}),
                   'calle_secundaria': forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Calle Secundaria'}),
                   'numero_casa': forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Número de Casa'}),
                   'referencia':forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Referencia'})}

class ProveedorSucursalForm(ModelForm):
    class Meta:
        model = mod.Sucursal
        fields = '__all__'
        exclude = ('nombre_sucursal', 'razon_social','telefono_contacto','estado','prioridad','me_gusta','id_ciudad',
                   'id_menu','id_proveedor')
        widgets = {'telefono': forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Teléfono'}),
                   'dias_atencion':forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Dias de Atención'}),
                   'hora_atencion':forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Horario de Atención'}),
                   'hora_pico': forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Horario Pico'}),
                   'visitas': forms.HiddenInput(),
                   'descripcion': forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Descripción del Sucursal'}),
                   'precio_envio': forms.TextInput( attrs={'class': 'form-control fields-pl', 'placeholder': 'Precio de envío'}),}



class MenuForm(ModelForm):
     class Meta:
            model = mod.Menu
            fields = '__all__'


class IngredienteForm(ModelForm):
    class Meta:
        model = mod.Ingrediente
        fields = '__all__'

class CiudadForm(ModelForm):
    class Meta:
        model = mod.Ciudad
        fields = '__all__'

class ActividadForm(ModelForm):
    class Meta:
        model = mod.ActividadComercial
        fields = '__all__'

class CategoriaForm(ModelForm):
    class Meta:
        model = mod.Categoria
        fields = '__all__'

class DireccionForm(ModelForm):
    class Meta:
        model = mod.Direccion
        fields = '__all__'

class SucursalForm(ModelForm):
    class Meta:
        model = mod.Sucursal
        fields = '__all__'

class DetallePedidoForm(ModelForm):
    class Meta:
        model = mod.DetallePedido
        fields = '__all__'

class TipoPagoForm(ModelForm):
    class Meta:
        model = mod.TipoPago
        fields = '__all__'

class TipoPedidoForm(ModelForm):
    class Meta:
        model = mod.TipoPedido
        fields = '__all__'

class TransportistaForm(ModelForm):
    class Meta:
        model = mod.Transportista
        fields = '__all__'

class HorarioTransportistaForm(ModelForm):
    class Meta:
        model = mod.HorarioTransportista
        fields = '__all__'

class PedidoForm(ModelForm):
    class Meta:
        model = mod.Pedido
        fields = '__all__'


class CoordenadasForm(ModelForm):
    class Meta:
        model = mod.Coordenadas
        fields = '__all__'

class ValoracionForm(ModelForm):
    class Meta:
        model = mod.Valoracion
        fields = '__all__'

class RedSocialForm(ModelForm):
    class Meta:
        model = mod.RedSocial
        fields = '__all__'


class ContratoForm(ModelForm):
    class Meta:
        model = mod.Contrato
        fields = '__all__'

#class PerfilForm(ModelForm):
#    class Meta:
#        model = mod.Perfil
#        fields = '__all__'

class PlatoForm(ModelForm):
    class Meta:
        model = mod.Plato
        fields = '__all__'
        field_classes = {
            'ruta_foto': fotoField
        }

class TamanioForm(ModelForm):
    class Meta:
        model = mod.Tamanio
        fields = '__all__'


class emailFilledCharField(FilledCharField):
    def validate(self, value):
        super(emailFilledCharField, self).validate(value)
        users = mod.User.objects.filter(email=value)
        if len(users) > 0:
            raise ValidationError(
                ('El correo ya está en uso por otro usuario')
            )
        validate_email(value)


class emailGenericFilledCharField(FilledCharField):
    def validate(self, value):
        super(emailGenericFilledCharField, self).validate(value)
        validate_email(value)

class celularExistente(forms.Form):
    id_perfil = forms.IntegerField(required=True, widget=forms.HiddenInput())
    celular=forms.CharField(label='', required=False, min_length=5, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Celular'}), disabled=True)

    def clean(self):
        clean_data = super(celularExistente, self).clean()
        perfil = mod.Perfil.objects.get(id_perfil=clean_data.get('id_perfil', None))
        if perfil is not None:
            if perfil.telefono == clean_data['celular']:
                return clean_data
            else:
                inconsistencia = ValidationError(
                    ('Numero inconsistente'),
                    code='Numero_De_Celular_Inconsistente',
                )
                self.add_error('celular', inconsistencia)
        else:
            raise ValidationError("Formulario no llenado")

    def save(self):
        pass

class cedulaValidoF(forms.Form):
    perfil = forms.IntegerField(required=True, widget=forms.HiddenInput())
    id=cedulaInput(label="",required=True,widget=forms.TextInput( attrs={'class': 'form-control fields-pl','placeholder': 'ID Cédula'}))
    def clean(self):
        #user= self.cleaned_data['user']
        #id=self.cleaned_data['id']
        pass
    def save(self):
        per=self.cleaned_data['perfil']
        id=self.cleaned_data['id']
        perfil = mod.Perfil.objects.get(id_perfil=per)
        perfil.__dict__.update(cedula=id)
        perfil.save()


class celularValido(forms.Form):
    id_perfil = forms.IntegerField(required=True, widget=forms.HiddenInput())
    celular=forms.CharField(label='', required=False, min_length=5, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Número de celular'}))

    def clean(self):
        clean_data = super(celularValido, self).clean()
        value = clean_data.get('celular', None)
        if value is None:
            return clean_data
            # print('Ingresa un numero de celular')
            # sinNumero = ValidationError(
            #     ('Ingresa un numero de celular'),
            # )
            # self.add_error('celular', sinNumero)
        if value is not None:
            numbero_de_telefono_regex = re.compile(settings.ERNT)
            numero_telefono = numbero_de_telefono_regex.search(value)
            print(value+" <-- numero celular ingresado a cambiar")
            if numero_telefono is not None:
                valor = ""
                for numeros in numero_telefono.groups():
                    valor = valor + numeros
                clean_data['celular'] = valor
                perfiles = mod.Perfil.objects.exclude(id_perfil=clean_data['id_perfil']).filter(telefono=valor)
                if len(perfiles) == 0:
                    return clean_data
                else:
                    yaExiste = ValidationError(
                        ('Número de celular ya existe')
                    )
                    print('Número de celular ya existe')
                    self.add_error('celular', yaExiste)
            else:
                noValido = ValidationError(
                    ('Número de celular no válido'),
                )
                self.add_error('celular', noValido)

    def save(self):
        clean_data = self.clean()
        perfil = mod.Perfil.objects.get(id_perfil=clean_data['id_perfil'])
        perfil.__dict__.update(telefono = clean_data['celular'])
        perfil.save()

class NewUserForm(Form):
    #username = nonExistentUsernameField(label="", required=True, widget=forms.TextInput(
    #    attrs={'class': 'form-control', 'placeholder': 'Nombre de usuario'}))
    username = emailFilledCharField(label="", required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Correo Electrónico', 'required': 'true'}))
    password = PassCharField(label="", required=True,
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Contraseña', 'required': 'true'}))
    confirmPassword = PassCharField(label="", required=True, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Confirmar contraseña', 'required': 'true'}))
    first_name = ValidNameField(label="", required=True,
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombres', 'required': 'true'}))
    last_name = ValidNameField(label="", required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellidos', 'required': 'true'}))
    #email = emailFilledCharField(label="", required=True, widget=forms.EmailInput(
    #    attrs={'class': 'form-control', 'placeholder': 'Correo Electrónico'}))
    cedula = cedulaInput(label="",required=True,widget=forms.TextInput(
        attrs={'class': 'form-control fields-pl','placeholder': 'Cédula'}))

    #     '''class Meta:
    #         model = Usuario
    #         fields = ['celular', 'nick', 'password', 'activo', 'rutaFoto', 'idPersona', 'idTipoUsuario']'''
    #
    def clean(self):
        clean_data = super(NewUserForm, self).clean()
        passwd = clean_data.get("password")
        confirm_passwd = clean_data.get("confirmPassword")
        if passwd and confirm_passwd and passwd != confirm_passwd:
            noMatch = ValidationError(
                ('Las contraseñas no coinciden'),
            )
            self.add_error('password', noMatch)
            self.add_error('confirmPassword', noMatch)



    def save(self, commit=True):
        data_orig = self.cleaned_data
        username = self.cleaned_data['username']


        data = {'email': username}
        for key in data_orig:
            if key != "confirmPassword" and key != 'cedula':
                data[key] = data_orig[key]
        u = mod.User(**data)
        u.set_password(u.password)
        u.save()

        cliente = Group.objects.get(name="Cliente")
        u.groups.add(cliente)
        u.save()

        dni = data_orig['cedula']
        if re.search("^(\d){9}$", dni) is None:
            cedulaRegex = re.compile("^(\d{9})-?(\d)$")
            cedula_split = cedulaRegex.search(dni)
            data_perfil = {'cedula': cedula_split.group(1)+cedula_split.group(2)}
        else:
            data_perfil = {'cedula': dni}

        perfil = mod.Perfil.objects.get(id_usuario_id=u)
        perfil.cuentaVerif=False
        perfil.__dict__.update(**data_perfil)
        perfil.save()


        #class Meta:
    #    model = mod.User
    #    fields = ['username', 'password', 'first_name', 'last_name', 'email']

#registro antes de hacer un pedido
class RegistroUserForm(forms.Form):
    first_name = forms.CharField(label='',widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Nombre'}))
    last_name = forms.CharField(label='',widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Apellidos'}))
    username = forms.CharField(label='',required=True,min_length=5,widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Nombre de usuario'}))
    email = forms.EmailField(label='',required=True,widget=forms.EmailInput(attrs={'class': 'form-control','placeholder': 'Correo electrónico'}))
    password = forms.CharField(label='',required=True,min_length=5, widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'Contraseña'}))
    password2 = forms.CharField(label='',required=True,widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'Confirmar contraseña'}))

    def clean_username(self):
        """Comprueba que no exista un username igual en la db"""
        username = self.cleaned_data['username']
        if User.objects.filter(username=username):
            raise forms.ValidationError('Nombre de usuario ya registrado.')
        return username
    def clean_email(self):
        """Comprueba que no exista un email igual en la db"""
        email = self.cleaned_data['email']
        if User.objects.filter(email=email):
            raise forms.ValidationError('Ya existe un email igual.')
        return email
    def clean_password2(self):
        """Comprueba que password y password2 sean iguales."""
        password = self.cleaned_data['password']
        password2 = self.cleaned_data['password2']
        if password != password2:
         raise forms.ValidationError('Las contraseñas no coinciden.')
        return password2


# class UpdateUserForm(Form):
#     username = CharField(label="", required=True, widget=forms.HiddenInput())
#     curPassword = CharField(label="", required=False,
#                              widget=forms.PasswordInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Contraseña Actual'}))
#     password = UpdatePassCharField(label="", required=False,
#                                widget=forms.PasswordInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Nueva Contraseña'}))
#     confirmPassword = UpdatePassCharField(label="", required=False, widget=forms.PasswordInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Confirmar Nueva Contraseña '}))
#     first_name = ValidNameField(label="", required=True,
#                                  widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Nombres'}))
#     last_name = ValidNameField(label="", required=True,
#                                 widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Apellidos'}))
#     email = emailFilledCharField(label="", required=True, widget=forms.EmailInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Correo Electrónico'}))
#
#
#     def clean(self):
#         clean_data = super(UpdateUserForm, self).clean()
#         curPasswd = clean_data.get("curPassword")
#         passwd = clean_data.get("password")
#         confirm_passwd = clean_data.get("confirmPassword")
#         username = clean_data.get("username")
#         if curPasswd and confirm_passwd and passwd:
#             #user = authenticate(user = username, password = curPasswd)
#             #if user is not None:
#             user = User.objects.filter(username=username)
#             if user and user[0].check_password(curPasswd):
#                 if passwd == '' or confirm_passwd == '':
#                     noEmpty = ValidationError(
#                         ('Clave Nueva no puede ser vacía'),
#                     )
#                     if passwd == '':
#                         self.add_error('password', noEmpty)
#                     if confirm_passwd == '':
#                         self.add_error('confirmPassword', noEmpty)
#                 if passwd != confirm_passwd:
#                     noMatch = ValidationError(
#                         ('Claves no coinciden'),
#                     )
#                     self.add_error('password', noMatch)
#                     self.add_error('confirmPassword', noMatch)
#
#             else:
#                 login_failure = ValidationError(
#                     ('No coincide con la contraseña actual'),
#                 )
#                 self.add_error('curPassword', login_failure)
#         # else:
#         #
#         #     if curPasswd!=''  or confirm_passwd!=''  or  passwd!='':
#         #         noEmptyCurrent = ValidationError(
#         #             ('Llena todos los campos'),
#         #         )
#         #         self.add_error('curPassword', noEmptyCurrent)
#         #
#
#
#     def save(self, commit=True):
#         data_orig = self.cleaned_data
#         data = {}
#         chg_pwd = False
#         for key in data_orig:
#             if key != "confirmPassword" and key != 'curPassword' and key != 'username':
#                 if key == 'password' and data_orig[key] is None:
#                     data_orig[key] = ''
#                 if key == 'password' and len(data_orig[key]) > 0:
#                     chg_pwd = True
#                 elif key == 'password':
#                     continue
#                 data[key] = data_orig[key]
#         my_user = mod.User.objects.filter(username=data_orig['username'])
#         u = my_user[0]
#         u.__dict__.update(**data)
#         if chg_pwd:
#             u.set_password(u.password)
#         u.save()


class UpdateUserNamesForm(Form):
    username = CharField(label="", required=True, widget=forms.HiddenInput())

    genero = forms.CharField(label="",required=False,widget=forms.Select(choices=mod.CHOICE_GENERO,
        attrs={'class': 'form-control fields-pl', 'placeholder': 'Genero'}))

    first_name = ValidNameField(label="", required=True,
                                 widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Nombres'}))
    last_name = ValidNameField(label="", required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Apellidos'}))
    email = emailGenericFilledCharField(label="", required=True, widget=forms.EmailInput(
        attrs={'class': 'form-control fields-pl', 'placeholder': 'Correo Electrónico', 'readonly': 'true' }))


    def clean(self):
        clean_data = super(UpdateUserNamesForm, self).clean()
        users = mod.User.objects.filter(email=clean_data.get("email")).exclude(username=clean_data.get("username"))
        if len(users) > 0:
            correo_no_unico = forms.ValidationError(
                ('El correo electrónico ya está en uso por otro usuario.')
            )
            self.add_error('email', correo_no_unico)
        # else:
        #     return clean_data

    def save(self, commit=True):
        data_orig = self.cleaned_data
        data = {}
        for key, value in data_orig.items():
            if key != 'username':
                data[key] = value
        my_user = mod.User.objects.filter(username=data_orig['username'])
        u = my_user[0]
        per=mod.Perfil.objects.get(id_usuario_id=u.pk)
        per.genero=data_orig['genero']
        per.save()
        u.__dict__.update(**data)
        u.save()





class FormLogin(Form):
    #username = FilledCharField()
    #password = FilledCharField(widget=forms.PasswordInput)
    username = forms.CharField(label='',required=True, widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Correo electrónico'}))
    password = forms.CharField(label='', required=True,  widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'Contraseña'}))

    '''class Meta:
        model = Usuario'''

    def clean(self):
        """Comprueba que no exista un username igual en la db"""
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if user is None:
            users = User.objects.filter(email=username)
            if len(users) > 0:
                user = authenticate(username=users[0].username, password=password)
        invalido = forms.ValidationError(
            ('Nombre de usuario o clave inválidos'),
        )

        if user is None:
            print('usernameeeeeeee')
            # raise forms.ValidationError('Nombre de usuario o clave inválidos')
            self.add_error('password', invalido)
        '''elif users[0].check_password(password):
            print('passssss')
            self.add_error('username', invalido)
            self.add_error('password', invalido)'''

class setPasswordSocialForm(Form):
    username = CharField(label="", required=True, widget=forms.HiddenInput())
    password = UpdatePassCharField(label="", required=False,widget=forms.PasswordInput(
        attrs={'class': 'form-control fields-pl', 'placeholder': 'Nueva Contraseña'}))
    confirmPassword = UpdatePassCharField(label="", required=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control fields-pl', 'placeholder': 'Confirmar Nueva Contraseña '}))

    def clean(self):
        clean_data = super(setPasswordSocialForm, self).clean()
        passwd = clean_data.get("password")
        confirm_passwd = clean_data.get("confirmPassword")

        if(confirm_passwd and passwd):
            if passwd == '' or confirm_passwd == '':
                noEmpty = forms.ValidationError(
                    ('Clave Nueva no puede ser vacía'),
                )
                if passwd == '':
                    self.add_error('password', noEmpty)
                if confirm_passwd == '':
                    self.add_error('confirmPassword', noEmpty)
            if passwd != confirm_passwd:
                noMatch = forms.ValidationError(
                    ('Claves no coinciden'),
                )
                self.add_error('password', noMatch)
                self.add_error('confirmPassword', noMatch)

        elif(confirm_passwd!=''  or  passwd!=''):
            noEmptyCurrent = forms.ValidationError(('Llena todos los campos'),)
            self.add_error('curPassword', noEmptyCurrent)

    def save(self, commit=True):
        data_orig = self.cleaned_data
        data = {}
        chg_pwd = False
        for key in data_orig:
            if key != "confirmPassword" and key != 'username':
                if key == 'password' and data_orig[key] is None:
                    data_orig[key] = ''
                if key == 'password' and len(data_orig[key]) > 0:
                    chg_pwd = True
                elif key == 'password':
                    continue
                data[key] = data_orig[key]
        my_user = mod.User.objects.filter(username=data_orig['username'])
        u = my_user[0]
        u.__dict__.update(**data)
        if chg_pwd:
            u.set_password(u.password)
        u.save()


class UpdatePassProveedorForm(Form):
    username = CharField(label="", required=True, widget=forms.HiddenInput())
    curPassword = FilledCharField(label="", required=False,
                             widget=forms.PasswordInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Contraseña Actual'}))
    password = UpdatePassCharField(label="", required=False,
                               widget=forms.PasswordInput(attrs={'class': 'form-control fields-pl', 'placeholder': 'Nueva Contraseña'}))
    confirmPassword = UpdatePassCharField(label="", required=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control fields-pl', 'placeholder': 'Confirmar Nueva Contraseña '}))


    def clean(self):
        clean_data = super(UpdatePassProveedorForm, self).clean()
        curPasswd = clean_data.get("curPassword")
        passwd = clean_data.get("password")
        confirm_passwd = clean_data.get("confirmPassword")
        username = clean_data.get("username")
        if curPasswd and confirm_passwd and passwd:

            user = User.objects.filter(username=username)
            if user and user[0].check_password(curPasswd):
                if passwd == '' or confirm_passwd == '':
                    noEmpty = forms.ValidationError(
                        ('Clave Nueva no puede ser vacía'),
                    )
                    if passwd == '':
                        self.add_error('password', noEmpty)
                    if confirm_passwd == '':
                        self.add_error('confirmPassword', noEmpty)
                if passwd != confirm_passwd:
                    noMatch = forms.ValidationError(
                        ('Claves no coinciden'),
                    )
                    self.add_error('password', noMatch)
                    self.add_error('confirmPassword', noMatch)
            else:
                login_failure = forms.ValidationError(
                    ('No coincide con la contraseña actual'),
                )
                self.add_error('curPassword', login_failure)
        else:
            if curPasswd!=''  or confirm_passwd!=''  or  passwd!='':
                noEmptyCurrent = forms.ValidationError(
                    ('Llena todos los campos'),
                )
                self.add_error('curPassword', noEmptyCurrent)




    def save(self, commit=True):
        data_orig = self.cleaned_data
        data = {}
        chg_pwd = False
        for key in data_orig:
            if key != "confirmPassword" and key != 'curPassword' and key != 'username':
                if key == 'password' and data_orig[key] is None:
                    data_orig[key] = ''
                if key == 'password' and len(data_orig[key]) > 0:
                    chg_pwd = True
                elif key == 'password':
                    continue
                data[key] = data_orig[key]
        my_user = mod.User.objects.filter(username=data_orig['username'])
        u = my_user[0]
        u.__dict__.update(**data)
        if chg_pwd:
            u.set_password(u.password)
        u.save()

#############################################################################################

class  PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(label=("Correo electrónico o nombre de usuario"), max_length=254,
                                        widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Correo electrónico'}))

class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        }
    new_password1 = forms.CharField(label=("New password"),
                                    widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=("New password confirmation"),
                                    widget=forms.PasswordInput)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                    )
        return password2




# class PerfilProveedorForm(Form):
#
#     id_perfil= CharField(label="", required=True, widget=forms.HiddenInput())
#
#     ruta_foto=fotoField(required=False,widget=forms.FileInput(attrs={'placeholder': 'Foto'}))
#
#     calle_principal = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Calle Principal'}))
#
#     calle_secundaria = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Calle Secundaria'}))
#
#     numero_casa = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Número de Casa'}))
#
#     referencia = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Referencia'}))
#
#     telefono = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Teléfono'}))
#
#     dias_atencion = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Dias de Atención'}))
#
#     hora_atencion = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Horario de Atención'}))
#
#     hora_pico = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Horario Pico'}))
#
#     descripcion = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Descripción del Sucursal'}))
#
#     precio_envio = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Precio de envío'}))
#
#
#     def __init__(self, post_req=None, files_req=None, **kwargs):
#         #print(kwargs['initial'])
#         if post_req is not None and files_req is not None:
#             super(PerfilForm, self).__init__(post_req, files_req, **kwargs)
#         elif post_req is not None:
#             super(PerfilForm, self).__init__(post_req, **kwargs)
#         elif files_req is not None:
#             super(PerfilForm, self).__init__(files_req, **kwargs)
#         else:
#             super(PerfilForm, self).__init__(**kwargs)
#
#     def save(self):
#         data = self.cleaned_data
#         data2 = {}
#         for llave, valor in data.items():
#             if llave != 'id_perfil':
#                 if llave == 'ruta_foto' and (valor == '' or valor is None):
#                     continue
#                 data2[llave] = valor
#         perfil = mod.Perfil.objects.get(id_perfil=data['id_perfil'])
#         perfil.__dict__.update(**data2)
#         perfil.save()

# PAmeeeeeeeeeee

# class PerfilProveedorForm2(Form):
#
#     id_perfil= CharField(label="", required=True, widget=forms.HiddenInput())
#     ruta_foto= fotoField(required=False, widget=forms.FileInput(attrs={'placeholder': 'Foto'}))
#
#     def __init__(self, post_req=None, files_req=None, **kwargs):
#
#         if post_req is not None and files_req is not None:
#             super(PerfilProveedorForm2, self).__init__(post_req, files_req, **kwargs)
#         elif files_req is not None:
#             super(PerfilProveedorForm2, self).__init__(files_req, **kwargs)
#         else:
#             super(PerfilProveedorForm2, self).__init__(**kwargs)
#
#     def clean(self):
#         clean_data = super(PerfilProveedorForm2, self).clean()
#         perfil = mod.Perfil.objects.filter(id_perfil=clean_data.get("id_perfil"))
#
#         return True
#
#     def save(self):
#         data = self.cleaned_data
#         data2 = {}
#         for llave, valor in data.items():
#             if llave != 'id_perfil':
#                 if llave == 'ruta_foto' and (valor == '' or valor is None):
#                     continue
#                 data2[llave] = valor
#         perfil = mod.Perfil.objects.get(id_perfil=data['id_perfil'])
#         perfil.__dict__.update(**data2)
#         perfil.save()
#
#
#
#
# class DireccionProveedorForm2(Form):
#     id_direccion= CharField(label="", required=True, widget=forms.HiddenInput())
#
#     calle_principal = forms.CharField(label="", required=True, widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Calle Principal'}))
#
#     calle_secundaria = forms.CharField(label="", required=True, widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Calle Secundaria'}))
#
#     numero_casa = forms.CharField(label="", required=True, widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Número de Casa'}))
#
#     referencia = forms.CharField(label="", required=True, widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Referencia'}))
#
#     latitud = forms.CharField(label="", required=True, widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Latitud'}))
#
#     longitud = forms.CharField(label="", required=True, widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Longitud'}))
#
#     def clean(self):
#         clean_data = super(DireccionProveedorForm2, self).clean()
#         return True
#
#     def save(self, commit=True):
#         data_orig = self.cleaned_data
#         data = {}
#         for key, value in data_orig.items():
#             if key != 'id_direccion':
#                 data[key] = value
#         my_dir = mod.Direccion.objects.filter(id_direccion=data_orig['id_direccion'])
#         u = my_dir[0]
#         u.__dict__.update(**data)
#         u.save()
#
#
# class SucursalProveedorForm2(Form):
#     id_sucursal = CharField(label="", required=True, widget=forms.HiddenInput())
#
#     telefono = forms.CharField(label="", required=True,widget=forms.TextInput(
#             attrs={'class': 'form-control fields-pl', 'placeholder': 'Teléfono'}))
#
#     dias_atencion = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Dias de Atención'}))
#
#     hora_atencion = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Horario de Atención'}))
#
#     descripcion = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Descripción del Sucursal'}))
#
#     precio_envio = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Precio de envío'}))
#
#     hora_pico = forms.CharField(label="",required=True,widget=forms.TextInput(
#         attrs={'class': 'form-control fields-pl', 'placeholder': 'Hora pico'}))
#
#     def clean(self):
#         clean_data = super(SucursalProveedorForm2, self).clean()
#         return True
#
#     def save(self, commit=True):
#         data_orig = self.cleaned_data
#         data = {}
#         for key, value in data_orig.items():
#             if key != 'id_sucursal':
#                 data[key] = value
#         my_suc = mod.Sucursal.objects.filter(id_sucursal=data_orig['id_sucursal'])
#         u = my_suc[0]
#         u.__dict__.update(**data)
#         u.save()


