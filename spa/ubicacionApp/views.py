from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import permission_required, login_required
from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.http import HttpResponseRedirect
from django.template import RequestContext
from spaapp.form import *
from spaapp.models import *
from spaapp import models as mod
from django.forms import Form, ModelForm, PasswordInput, CharField
#from . import cryptoaux as ca
#from . import form as forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from dateutil.parser import parse
from django.template.loader import render_to_string
from django.core import serializers
from django.http import HttpResponse
from django.http import JsonResponse
from django.db.models import Q
import json

from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.viewsets import ModelViewSet
from . import serializers as ser
from spaapp import constantes
from django.views.generic import TemplateView
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.viewsets import ModelViewSet
#from . import serializers as ser
# from django.views.decorators.cache import never_cache
import datetime

import datetime
import time
from django.utils.timezone import localtime


# Create your views here.

# Permiso: Debe ser Transportista
#@permission_required("spaapp.esTransportista", raise_exception=True)
@permission_required("spaapp.esTransportista", raise_exception=True)
def ubicacion(request):
    #plato = mod.Plato.objects.all()
    return render(request, 'ubicacion/ubicacion.html')


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def soyTransportista(request):
    user = request.user
    perfil=''
    profileType = None
    if user.has_perm("spaapp.esTransportista"):
        print('es transportista')
        # TODO poner imagen por defecto para todos los perfiles de los transportistas
        perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
    else:
        print('Cliente o Proveedor')
        perfil = mod.Perfil.objects.get(id_usuario=request.user)

    # JY, Determinar que tipo de Usuario esta Autenticado
    profileType = validarTipoUsuario(request)
    # FIN

    try:
        if request.user.is_active is False:
            return redirect("/")
        pt = mod.Perfil.objects.filter(id_usuario=request.user)
        # ct = mod.Transportista.objects.get(id_perfil=pt[0])
        return render(request, 'ubicacion/soyTransportista.html', {'perfil':perfil, 'profileType':profileType})
    except ObjectDoesNotExist:
        return redirect("/")
    return render(request, 'ubicacion/soyTransportista.html', {'perfil':perfil, 'profileType':profileType})


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def pedidosTransportista(request):
    estado = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_PROCESO)
    pedidos = mod.Pedido.objects.filter(id_estado=estado).order_by('fecha').reverse()
    return render(request, 'ubicacion/pedidosTransportista.html', {'pedidos':pedidos})


# eliminar función repetida
# Permiso: Debe ser Transportista
# @permission_required("spaapp.esTransportista", raise_exception=True)
# def obtenerTodosPedidos(request):
#
#     count = request.GET['count']
#     page = request.GET['page']
#
#     datos = mod.Pedido.objects.all().order_by('fecha').all().order_by('estado')
#     #.reverse()
#     usuario = request.user
#     #perfilActual = mod.Perfil.objects.get(id_usuario=usuario)
#     paginador = Paginator(datos, count)
#     pedidos = paginador.page(page)
#
#     lista = []
#     for pedido in pedidos:
#         usuario = mod.User.objects.get(pk=pedido.usuario.pk)
#         direccion = mod.Direccion.objects.get(id_direccion=pedido.id_direccion_id)
#         tipo_pago = mod.TipoPago.objects.get(pk=pedido.id_tipo_pago_id)
#         row = []
#
#         # result = localtime(pedido.fecha)
#         # dt = ("%s/%s/%s-%s:%s" % (result.day, result.month, result.year, result.hour, result.month))
#         row = json.dumps(
#             {"pk": pedido.pk,
#              "cliente": usuario.username,
#              "fecha": str(dt),
#              "tipo_pago": tipo_pago.tipo_pago,
#              "valor": pedido.valor,
#              "estado": pedido.estado,
#              "total": datos.count(),
#              "direccion": direccion.calle_principal+" - "+direccion.calle_secundaria},
#              sort_keys=True)
#         lista.append(row)
#
#     return JsonResponse({'lista':lista},safe=False)


@permission_required("spaapp.esTransportista", raise_exception=True)
def obtenerUltimosPedidos(request):

    count = request.GET['count']
    page = request.GET['page']
    datos = mod.Pedido.objects.filter(Q(id_estado__id_estado = 2) | Q(id_estado__id_estado = 3) | Q(id_estado__id_estado = 4)).all().order_by('-fecha').all()#.order_by('id_estado')
    #.reverse()
    usuario = request.user
    #perfilActual = mod.Perfil.objects.get(id_usuario=usuario)
    paginador = Paginator(datos, count)
    pedidos = paginador.page(page)
    lista = []
    for pedido in pedidos:
        usuario = mod.User.objects.get(pk=pedido.usuario.pk)
        direccion = mod.Direccion.objects.get(id_direccion=pedido.id_direccion_id)
        tipo_pago = mod.TipoPago.objects.get(pk=pedido.id_tipo_pago_id)

        # dt_string = str(pedido.fecha)
        # new_dt = dt_string[:19]
        # dt = datetime.datetime.strptime(new_dt, '%Y-%m-%d %H:%M:%S')

        result = localtime(pedido.fecha)
        dt = ("%s/%s/%s-%s:%s" % (result.day, result.month, result.year, result.hour, result.minute))

        estado = mod.CatalogoEstado.objects.get(id_estado=pedido.id_estado.id_estado)
        row = []
        row = json.dumps(
            {"pk": pedido.pk,
             "cliente": usuario.first_name +' '+usuario.last_name,
             "fecha": str(dt),
             "tipo_pago": tipo_pago.tipo_pago,
             "valor": pedido.valor,
             "estado": estado.estado,
             "tiempo": pedido.tiempo,
             "sucursal": pedido.id_sucursal.nombre_sucursal,
             "total": datos.count(),
             "direccion": direccion.calle_principal+" - "+direccion.calle_secundaria+" - "+direccion.referencia+" - "+direccion.numero_casa },
             sort_keys=True)
        lista.append(row)

    return JsonResponse({'lista':lista},safe=False)


@permission_required("spaapp.esTransportista", raise_exception=True)
def obtenerTodosPedidos(request):

    count = request.GET['count']
    page = request.GET['page']

    datos = mod.Pedido.objects.all().order_by('id_estado').all().order_by('-fecha')
    #.reverse()
    usuario = request.user
    #perfilActual = mod.Perfil.objects.get(id_usuario=usuario)
    paginador = Paginator(datos, count)
    pedidos = paginador.page(page)

    lista = []

    for pedido in pedidos:
        usuario = mod.User.objects.get(pk=pedido.usuario.pk)
        direccion = mod.Direccion.objects.get(id_direccion=pedido.id_direccion_id)
        tipo_pago = mod.TipoPago.objects.get(pk=pedido.id_tipo_pago_id)
        estado = mod.CatalogoEstado.objects.get(id_estado=pedido.id_estado.id_estado)
        row = []
        result = localtime(pedido.fecha)
        dt = ("%s/%s/%s-%s:%s" % (result.day, result.month, result.year, result.hour, result.minute))
        row = json.dumps(
            {"pk": pedido.pk,
             "cliente": usuario.username,
             "fecha": str(dt),
             "tipo_pago": tipo_pago.tipo_pago,
             "valor": pedido.valor,
             "estado": estado.estado,
             "total": datos.count(),
             "direccion": direccion.calle_principal+" - "+direccion.calle_secundaria},
             sort_keys=True)
        lista.append(row)

    return JsonResponse({'lista':lista},safe=False)

@permission_required("spaapp.esTransportista", raise_exception=True)
def ultimosPedidos(request):
    return render(request, 'ubicacion/ultimosPedidos.html')


@permission_required("spaapp.esTransportista", raise_exception=True)
def todosPedidos(request):
    estado = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_PROCESO)
    pedidos = mod.Pedido.objects.filter(id_estado=estado).order_by('fecha').reverse()
    return render(request, 'ubicacion/todosPedidos.html', {'pedidos':pedidos})


@permission_required("spaapp.esTransportista", raise_exception=True)
def yesno(request):
    return render(request, 'ubicacion/yesno.html')

@permission_required("spaapp.esTransportista", raise_exception=True)
def modalDireccion(request):
    return render(request, 'proveedor/modalDireccion.html')

@permission_required("spaapp.esTransportista", raise_exception=True)
def datosPersonales(request):
    return render(request, 'ubicacion/datosPersonales.html')

@permission_required("spaapp.esTransportista", raise_exception=True)
def datosPedido(request):
    return render(request, 'ubicacion/datosPedido.html')

@permission_required("spaapp.esTransportista", raise_exception=True)
def index(request):
    return render(request, 'ubicacion/index.html')

# Permiso: Debe ser Transortista
@permission_required("spaapp.esTransportista", raise_exception=True)
def adminTransportista(request):
    return render(request, 'ubicacion/adminTransportista.html')


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def datosTransportista(request):
    CHOICE_GENERO = (('Masculino', 'Masculino'), ('Femenino', 'Femenino'),)
    ctx = {}

    ctx['formulario'] = CHOICE_GENERO
    return render_to_response('ubicacion/datosTransportista.html', ctx, context_instance=RequestContext(request))
    #return render(request, 'ubicacion/datosTransportista.html')


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def adminDatosTransportista(request):

    count = request.GET['count']
    page = request.GET['page']

    datos = mod.Pedido.objects.all().order_by('fecha').reverse()
    paginador = Paginator(datos, count)
    pedidos = paginador.page(page)

    lista = []
    for pedido in pedidos:
        usuario = mod.User.objects.get(pk=pedido.usuario.pk)
        direccion = mod.Direccion.objects.get(id_direccion=pedido.id_direccion_id)
        tipo_pago = mod.TipoPago.objects.get(pk=pedido.id_tipo_pago_id)
        row = []
        row = json.dumps(
            {"pk": pedido.pk,
             "cliente": usuario.username,
             "fecha": str(pedido.fecha),
             "tipo_pago": tipo_pago.tipo_pago,
             "valor": pedido.valor,
             "estado": pedido.estado,
             "total": datos.count(),
             "direccion": direccion.calle_principal+" - "+direccion.calle_secundaria},
             sort_keys=True)
        lista.append(row)

    return JsonResponse({'lista':lista},safe=False)


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def listaTransportistas(request):

    count = request.GET['count']
    page = request.GET['page']

    usuario = request.user
    #perfilActual = mod.Perfil.objects.get(id_usuario=usuario)
    datos = mod.Transportista.objects.all()
    paginador = Paginator(datos, count)
    transportistas = paginador.page(page)

    lista = []
    for transportista in transportistas:
        perfil = mod.Perfil.objects.get(pk=transportista.id_perfil.id_perfil)
        row = []
        row = json.dumps(
            {"pk_transportista": transportista.pk,
             "pk_perfil": perfil.pk,
             "tipo_servicio": transportista.tipo_servicio ,
             "tipo_transporte": transportista.tipo_transporte,
             "sim": transportista.sim_dispositivo,
             "cedula": transportista.cedula,
             "telefono": transportista.numero,
             #"genero": perfil.genero,
             #"fecha": str(perfil.fecha_nacimiento),
             "total": datos.count(),
             "nombre": transportista.nombre
             },
             sort_keys=True)
        lista.append(row)

    return JsonResponse({'lista':lista},safe=False)


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def ubicacionDatos(request):
    usuario = request.user
    pedidos = mod.Pedido.objects.filter(id_estado=2).order_by('pk').reverse()
    paginador = Paginator(pedidos, 10)
    pedidos = paginador.page(1)
    lista = []
    for pedido in pedidos:
        ubicacionProveedor = mod.Direccion.objects.get(id_direccion=pedido.id_sucursal.id_direccion.id_direccion)
        ubicacionCliente = mod.Direccion.objects.get(id_direccion=pedido.id_direccion.id_direccion)
        row = json.dumps(
            {"calle_principal": ubicacionProveedor.calle_principal,
             "calle_secundaria": ubicacionProveedor.calle_secundaria,
             "numero_casa": ubicacionProveedor.numero_casa,
             "referencia": ubicacionProveedor.referencia,
             "longitud": ubicacionProveedor.longitud,
             "latitud": ubicacionProveedor.latitud,
             "nombre":pedido.id_sucursal.nombre_sucursal,
             "calle_principal_": ubicacionCliente.calle_principal,
             "calle_secundaria_": ubicacionCliente.calle_secundaria,
             "numero_casa_": ubicacionCliente.numero_casa,
             "referencia_": ubicacionCliente.referencia,
             "longitud_": ubicacionCliente.longitud,
             "latitud_": ubicacionCliente.latitud,
             "nombre_": pedido.usuario.first_name,
             }, sort_keys=True)
        lista.append(row)


    return JsonResponse({'lista': lista}, safe=False)
    #data = serializers.serialize('json', lista)
    #return HttpResponse(data, content_type="application/json")


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def pedidoDatos(request):
    lista = mod.Pedido.objects.all()
    data = serializers.serialize('json', lista)

    return HttpResponse(data, content_type="application/json")


# Permiso: Debe ser Cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def guardarDireccion(request):
    longitud = request.GET['longitud']
    latitud = request.GET['latitud']
    calle_principal = request.GET['calle_principal']
    calle_secundaria = request.GET['calle_secundaria']
    referencia = request.GET['referencia']
    numero_casa = request.GET['numero_casa']
    id_perfil = request.GET['id_perfil']
    perfil = mod.Perfil.objects.get(id_perfil=id_perfil)
    mod.Direccion.objects.create(
        calle_principal=calle_principal,
        calle_secundaria=calle_secundaria,
        numero_casa=numero_casa,
        referencia=referencia,
        longitud=longitud,
        latitud=latitud,
        id_perfil=perfil
    )

    return HttpResponse("true", content_type="application/json")


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def guardarTransportista(request):
    pk_transportista = request.GET['pk_transportista']
    #pk_perfil = request.GET['pk_perfil']

    cedula = request.GET['cedula']
    #fecha_nacimiento = request.GET['fecha']
    #fecha_nacimiento = datetime.datetime.strptime(fecha, '%d/%m/%Y').strftime('%Y-%m-%d')
    telefono = request.GET['telefono']
    #genero = request.GET['genero']
    #foto = request.GET['foto']
    sim = request.GET['sim']
    nombre = request.GET['nombre']
    tipo_transporte = request.GET['tipo_transporte']
    tipo_servicio = request.GET['tipo_servicio']
    usuario = request.user

    perfil = mod.Perfil.objects.get(id_usuario = usuario)
    '''if pk_perfil!='0':
        perfil = mod.Perfil(
            pk=pk_perfil,
            cedula=cedula,
            fecha_nacimiento=fecha_nacimiento,
            telefono=telefono,
            genero=genero,
            ruta_foto=foto,
            #sim_dispositivo=sim,
            id_usuario=usuario
        )
    else:
        perfil = mod.Perfil(
            cedula=cedula,
            fecha_nacimiento=fecha_nacimiento,
            telefono=telefono,
            genero=genero,
            ruta_foto=foto,
            #sim_dispositivo=sim,
            id_usuario=usuario
        )

    #perfil.save()'''
    if pk_transportista != '0':
        transportista = mod.Transportista(
            pk=pk_transportista,
            tipo_transporte=tipo_transporte,
            tipo_servicio=tipo_servicio,
            nombre = nombre,
            cedula=cedula,
            numero=telefono,
            sim_dispositivo = sim,
            id_perfil = perfil,
            #slaifermilton
            saldo = 0
        )
    else:
        transportista = mod.Transportista(
            tipo_transporte=tipo_transporte,
            tipo_servicio=tipo_servicio,
            nombre=nombre,
            cedula=cedula,
            numero=telefono,
            sim_dispositivo=sim,
            id_perfil = perfil,
            # slaifermilton
            saldo = 0
        )
    transportista.save()

    return HttpResponse("true", content_type="application/json")


# Permiso: Debe ser Cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def obtenerDirecciones(request):
    userobj = mod.User.objects.get(id=request.user.pk)
    perfil = mod.Perfil.objects.get(id_usuario=userobj)
    direcciones = mod.Direccion.objects.filter(id_perfil=perfil.id_perfil).order_by('id_direccion')
    data = serializers.serialize('json', direcciones)
    return HttpResponse(data, content_type="application/json")


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def obtenerDetallesPedido(request):
    id_pedido = request.GET['id_pedido']
    pedido = mod.Pedido.objects.get(id_pedido=id_pedido)
    detalles = mod.DetallePedido.objects.filter(id_pedido=pedido)

    lista = []
    for detalle in detalles:
        plato = mod.Plato.objects.get(id_plato=detalle.id_plato.id_plato)
        row = json.dumps({"cantidad_solicitada": detalle.cantidad_solicitada,
                          "valor": detalle.valor,
                          "tiempo": pedido.tiempo,
                          "nombre_producto":plato.nombre_producto
                          }, sort_keys=True)
        lista.append(row)

    return JsonResponse({'lista':lista},safe=False)


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def obtenerUbicacionTransportista(request):
    coordenadas = mod.Coordenadas.objects.all()

    lista = []
    for coordenada in coordenadas:
        row = json.dumps(
            {
             "fecha": str(coordenada.fecha),
             "longitud": coordenada.longitud,
             "latitud": coordenada.latitud
             }, sort_keys=True)
        lista.append(row)

    return JsonResponse({'lista':lista},safe=False)


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def obtenerTodasDirecciones(request):
    direcciones = mod.Direccion.objects.all()

    lista = []
    for direccion in direcciones:
        row = json.dumps(
            {"calle_principal": direccion.calle_principal,
             "calle_secundaria": direccion.calle_secundaria,
             "numero_casa": direccion.numero_casa,
             "referencia": direccion.referencia,
             "longitud": direccion.longitud,
             "latitud": direccion.latitud
             }, sort_keys=True)
        lista.append(row)

    return JsonResponse({'lista':lista},safe=False)


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def obtenerPlato(request):
    id_plato = request.GET['id_plato']
    lista = mod.Plato.objects.filter(id_plato=id_plato)
    data = serializers.serialize('json', lista)
    return HttpResponse(data, content_type="application/json")


# Permiso: Debe ser Transportista
@permission_required("spaapp.esTransportista", raise_exception=True)
def guardarEnvio(request):
    id_pedido = request.GET['id_pedido']
    tiempo = request.GET['tiempo']
    estado = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_TRANSPORTISTA)
    mod.Pedido.objects.filter(id_pedido=id_pedido).update(
        id_estado=estado,
        tiempo=tiempo
    )
    return HttpResponse("true", content_type="application/json")

def cambiarEstadoEntregado(request):
    id_pedido = request.GET['id_pedido']
    estado = mod.CatalogoEstado.objects.get(estado=constantes.ESTADO_ENTREGADO)
    mod.Pedido.objects.filter(id_pedido=id_pedido).update(id_estado=estado)
    return HttpResponse("true", content_type="application/json")

@permission_required("spaapp.esTransportista", raise_exception=True)
def guardarCoordenada(request):
    '''longitud = request.GET['longitud']
    latitud = request.GET['latitud']
    pk_perfil = request.GET['pk_perfil']
    perfil = mod.Perfil.get(pk=1);
    coordenada = mod.Coordenadas(
        longitud=longitud,
        latitud=latitud,
        id_perfil = perfil
    )
    coordenada.save()
    '''
    return HttpResponse("true", content_type="application/json")

#JY, Funcion usada para determinar que tipo de usuario ingreso a applicacion
#0 Admin
#1 Cliente
#2 Proveedor
#3 Transportista
def validarTipoUsuario(request):
    if request.user.has_perm("spaapp.esAdmin"):
        return 0  # Admin
    elif request.user.has_perm("spaapp.esTransportista"):
        return 3  # Proveedor
    elif request.user.has_perm("spaapp.esProveedor"):
        return 2  # Proveedor
    elif request.user.has_perm("spaapp.esCliente"):
        return 1  # Cliente
    else:
        return None

# Permiso: Debe ser Transportista
#@permission_required("spaapp.esTransportista", raise_exception=True)
class CoordenadasViewSet(ModelViewSet):
    queryset = mod.Coordenadas.objects.all()
    serializer_class = ser.CoordenadasSerializer
