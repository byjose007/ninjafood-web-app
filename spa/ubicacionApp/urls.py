from django.conf.urls import include, url, patterns
from django.contrib.auth.views import login, logout_then_login
from . import views
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from rest_framework.routers import SimpleRouter


urlpatterns = [
    url(r'^ubicacion$', views.ubicacion),
    url(r'^soyTransportista$', views.soyTransportista, name='soyTransportista'),
    url(r'^ubicacionDatos$', views.ubicacionDatos),
    url(r'^pedidoDatos$', views.pedidoDatos),
    url(r'^adminTransportista$', views.adminTransportista),
    url(r'^guardarDireccion$', views.guardarDireccion),
    url(r'^obtenerDirecciones$', views.obtenerDirecciones),
    url(r'^obtenerDetallesPedido$', views.obtenerDetallesPedido),
    url(r'^obtenerUbicacionTransportista$', views.obtenerUbicacionTransportista),
    url(r'^obtenerPlato$', views.obtenerPlato),
    url(r'^guardarEnvio$', views.guardarEnvio),
    url(r'^obtenerUltimosPedidos$', views.obtenerUltimosPedidos),
    url(r'^obtenerTodosPedidos$', views.obtenerTodosPedidos),
    url(r'^adminDatosTransportista$', views.adminDatosTransportista),
    url(r'^datosTransportista$', views.datosTransportista),
    url(r'^listaTransportistas$', views.listaTransportistas),
    url(r'^guardarTransportista$', views.guardarTransportista),
    url(r'^obtenerTodasDirecciones$', views.obtenerTodasDirecciones),
    url(r'^guardarCoordenada$', views.guardarCoordenada),
    url(r'^ultimosPedidos$', views.ultimosPedidos),
    url(r'^todosPedidos$', views.todosPedidos),
    url(r'^index$', views.index),
    url(r'^yesno$', views.yesno),
    url(r'^datosPersonales$', views.datosPersonales),
    url(r'^datosPedido$', views.datosPedido),
    url(r'^modalDireccion$', views.modalDireccion),
    url(r'^cambiarEstadoEntregado$', views.cambiarEstadoEntregado),
]
