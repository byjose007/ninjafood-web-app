from django.db import models

# Create your models here.
class Contacto(models.Model):
    nombre_propietario = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=50)
    nombre_empresa = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    tipo_negocio = models.CharField(max_length=50)
    telefono = models.CharField(max_length=10, null=True, blank=True)
    correo = models.EmailField(null=True)
    
