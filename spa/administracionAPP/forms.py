from django import forms
from administracionAPP.models import Contacto
from django.contrib.auth.models import User, Group

class ContactoForm(forms.ModelForm):

    class Meta:
        model = Contacto

        fields = [
            'nombre_propietario',
            'apellidos',
            'ciudad',
            'nombre_empresa',
            'direccion',
            'tipo_negocio',
            'telefono',
        ]
        labels = {
            'nombre_propietario' : 'Nombre Propietario',
            'apellidos' : 'Apellidos',
            'ciudad' : 'Ciudad',
            'nombre_empresa' : 'Nombre Empresa',
            'direccion' : 'Direccion',
            'tipo_negocio' : 'Tipo de Negocio',
            'telefono' : 'Teléfono',
        }
        widgets = {
            'nombre_propietario' : forms.TextInput(
                attrs={'class':'form-control fields-pl', 'required':'true'}),
            'apellidos': forms.TextInput(
                attrs={'class': 'form-control fields-pl', 'required':'true'}),
            'ciudad': forms.TextInput(
                attrs={'class': 'form-control fields-pl', 'required':'true'}),
            'nombre_empresa': forms.TextInput(
                attrs={'class': 'form-control fields-pl', 'required':'true'}),
            'direccion': forms.TextInput(
                attrs={'class': 'form-control fields-pl', 'required':'true'}),
            'tipo_negocio': forms.TextInput(
                attrs={'class': 'form-control fields-pl', 'required':'true'}),
            'telefono': forms.TextInput(
                attrs={'class': 'form-control fields-pl', 'onkeypress': 'return numeros(event)', 'required':'true'}),

        }
    


DIA_CHOICES = (
    ('Lunes', 'Lunes'),
    ('Martes', 'Martes'),
    ('Miércoles', 'Miércoles'),
    ('Jueves', 'Jueves'),
    ('Viernes', 'Viernes'),
    ('Sábado', 'Sábado'),
    ('Domingo', 'Domingo'),
)



HORA_CHOICES = (
    ('00', '00'),
    ('01', '01'),
    ('02', '02'),
    ('03', '03'),
    ('04', '04'),
    ('05', '05'),
    ('06', '06'),
    ('07', '07'),
    ('08', '08'),
    ('09', '09'),
    ('10', '10'),
    ('11', '11'),
    ('12', '12'),
    ('13', '13'),
    ('14', '14'),
    ('15', '15'),
    ('16', '16'),
    ('17', '17'),
    ('18', '18'),
    ('19', '19'),
    ('20', '20'),
    ('21', '21'),
    ('22', '22'),
    ('23', '23'),

)

MINUTO_CHOICES = (
('00', '00'),
    ('01', '01'),
    ('02', '02'),
    ('03', '03'),
    ('04', '04'),
    ('05', '05'),
    ('06', '06'),
    ('07', '07'),
    ('08', '08'),
    ('09', '09'),
    ('10', '10'),
    ('11', '11'),
    ('12', '12'),
    ('13', '13'),
    ('14', '14'),
    ('15', '15'),
    ('16', '16'),
    ('17', '17'),
    ('18', '18'),
    ('19', '19'),
    ('20', '20'),
    ('21', '21'),
    ('22', '22'),
    ('23', '23'),
    ('24', '24'),
    ('25', '25'),
    ('26', '26'),
    ('27', '27'),
    ('28', '28'),
    ('29', '29'),
    ('30', '30'),
    ('31', '31'),
    ('32', '32'),
    ('33', '33'),
    ('34', '34'),
    ('35', '35'),
    ('36', '36'),
    ('37', '37'),
    ('38', '38'),
    ('39', '39'),
    ('40', '40'),
    ('41', '41'),
    ('42', '42'),
    ('43', '43'),
    ('44', '44'),
    ('45', '45'),
    ('46', '46'),
    ('47', '47'),
    ('48', '48'),
    ('49', '49'),
    ('50', '50'),
    ('51', '52'),
    ('52', '52'),
    ('53', '53'),
    ('54', '54'),
    ('55', '55'),
    ('56', '56'),
    ('57', '57'),
    ('58', '58'),
    ('59', '59'),
)

class NuevoProveedor(forms.Form):

    ###### Form Usuario
    username = forms.EmailField(label="Correo", required=True, widget=forms.TextInput(attrs={'class': 'form-control fields-pl',
                                                                                             'type':'email', 'placeholder': 'Correo Electrónico',
                                                                                             'name': 'username', 'required': 'true'}))
    password1 = forms.CharField(error_messages={'password_mismatch': 'Las contraseñas no coinciden'} ,label ="password",
                                required=True, widget=forms.PasswordInput(attrs={'class':'form-control fields-pl', 'name':'password',
                                                                                 'type':'password', 'required': 'true', 'min':'6'}))
    confirmPassword = forms.CharField(error_messages={'required':'Obligatorio'} ,required=True, widget=forms.PasswordInput(attrs={'class':'form-control fields-pl',
                                                                                                                                  'required': 'true'}))
    first_name = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs={'class':'form-control fields-pl', 'required': 'true'}))
    last_name = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs={'class':'form-control fields-pl', 'required': 'true'}))

    ##### Form Proveedor
    nombre_comercial = forms.CharField(required=True, max_length=50,widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    telefono = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control fields-pl',
                                                                            'required': 'true'}))
    telefono_contacto = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    prioridad = forms.IntegerField(required=True, widget=forms.NumberInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    razon_social = forms.CharField(required=True,max_length=20, widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))

    #### Form Sucursal
    nombre_sucursal = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    dias_atencion_desde = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}), choices=DIA_CHOICES)
    dias_atencion_hasta = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                            choices=DIA_CHOICES)
    hora_atencion_hora_inicio = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required':'true'}), choices=HORA_CHOICES)
    hora_atencion_minuto_inicio = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required':'true'}), choices=MINUTO_CHOICES)
    hora_atencion_hora_fin = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                                  choices=HORA_CHOICES)
    hora_atencion_minuto_fin = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                                 choices=MINUTO_CHOICES)
    activo = forms.BooleanField(initial=True)
    hora_pico_hora = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required':'true'}), choices=HORA_CHOICES)
    hora_pico_minuto = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required':'true'}), choices=MINUTO_CHOICES)
    #descripcion = forms.CharField(required=True,max_length=50, widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    descripcion = forms.CharField(required=True, max_length=300,
                                  widget=forms.Textarea(attrs={'class': 'form-control fields-pl', 'required': 'true', 'rows': '2', 'size':'300'}))

    #### Form_Dirección
    calle_principal = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    calle_secundaria = forms.CharField(required=True, max_length=50,widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    numero_casa = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    referencia = forms.CharField(required=True, max_length=50, widget=forms.Textarea(attrs={'class': 'form-control fields-pl', 'required': 'true', 'rows':'2', 'size':'250'}))


    def clean(self):
        username = self.cleaned_data['username']
        password1 = self.cleaned_data['password1']
        confirmPassword = self.cleaned_data['confirmPassword']
        if User.objects.filter(username=username):
            correo_repetido = forms.ValidationError('Este correo ya existe')
            self.add_error('username', correo_repetido)

        if password1 != confirmPassword:
            pass_error = forms.ValidationError('Contraseñas no coinciden')
            self.add_error('confirmPassword', pass_error)

        if len(password1)<6:
            pass_error2 = forms.ValidationError('La contraseña debe ser de 6 o más caracteres')
            self.add_error('password1', pass_error2)

class NuevaSucursal(forms.Form):
    ###### Form Usuario
    username = forms.EmailField(label="Correo", required=True,
                                widget=forms.TextInput(attrs={'class': 'form-control fields-pl',
                                                              'type': 'email', 'placeholder': 'Correo Electrónico',
                                                              'name': 'username', 'required': 'true'}))
    password1 = forms.CharField(error_messages={'password_mismatch': 'Las contraseñas no coinciden'}, label="password",
                                required=True,
                                widget=forms.PasswordInput(attrs={'class': 'form-control fields-pl', 'name': 'password',
                                                                  'type': 'password', 'required': 'true', 'min': '6'}))
    confirmPassword = forms.CharField(error_messages={'required': 'Obligatorio'}, required=True,
                                      widget=forms.PasswordInput(attrs={'class': 'form-control fields-pl',
                                                                        'required': 'true'}))
    first_name = forms.CharField(required=True,max_length=50,
                                 widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    last_name = forms.CharField(required=True, max_length=50,
                                widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))

    #### Form Sucursal
    nombre_sucursal = forms.CharField(required=True, max_length=50, widget=forms.TextInput(
        attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    dias_atencion_desde = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                            choices=DIA_CHOICES)
    dias_atencion_hasta = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                            choices=DIA_CHOICES)
    telefono = forms.CharField(required=True, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control fields-pl',
                                                                            'required': 'true'}))
    hora_atencion_hora_inicio = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                                  choices=HORA_CHOICES)
    hora_atencion_minuto_inicio = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                                    choices=MINUTO_CHOICES)
    hora_atencion_hora_fin = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                               choices=HORA_CHOICES)
    hora_atencion_minuto_fin = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                                 choices=MINUTO_CHOICES)
    activo = forms.BooleanField(initial=True)
    hora_pico_hora = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                       choices=HORA_CHOICES)
    hora_pico_minuto = forms.ChoiceField(required=True, widget=forms.Select(attrs={'required': 'true'}),
                                         choices=MINUTO_CHOICES)
    descripcion = forms.CharField(required=True, max_length=50,
                                  widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))

    #### Form_Dirección
    calle_principal = forms.CharField(required=True,max_length=50, widget=forms.TextInput(
        attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    calle_secundaria = forms.CharField(required=True, max_length=50, widget=forms.TextInput(
        attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    numero_casa = forms.CharField(required=True,max_length=50,
                                  widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))
    referencia = forms.CharField(required=True, max_length=50,
                                 widget=forms.TextInput(attrs={'class': 'form-control fields-pl', 'required': 'true'}))

    def clean(self):
        username = self.cleaned_data['username']
        password1 = self.cleaned_data['password1']
        confirmPassword = self.cleaned_data['confirmPassword']
        if User.objects.filter(username=username):
            correo_repetido = forms.ValidationError('Este correo ya existe')
            self.add_error('username', correo_repetido)

        if password1 != confirmPassword:
            pass_error = forms.ValidationError('Contraseñas no coinciden')
            self.add_error('confirmPassword', pass_error)

        if len(password1) < 6:
            pass_error2 = forms.ValidationError('La contraseña debe ser de 6 o más caracteres')
            self.add_error('password1', pass_error2)