from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from administracionAPP.views import registro_contacto, proveedores_admin, crear_proveedor_usuario, \
    admin_seguimiento, seguimiento, save_estado, sugerencias_admin, info_status, nueva_sucursal, acerca_de, \
    terminos_condiciones, admin_transportista

urlpatterns = [

    #url(r'^contacto_adm$', RegistroContacto.as_view(), name='contacto_adm'),
    url(r'^contacto_adm$', registro_contacto, name='contacto_adm'),
    url(r'^adminspa/proveedor_admin$', login_required(proveedores_admin), name='proveedor_admin'),
    url(r'^adminspa/proveedor_admin/nuevo$', login_required(crear_proveedor_usuario), name='nuevo_proveedor'),
    url(r'^admin_seguimiento$', admin_seguimiento, name='admin_seguimiento'),
    url(r'^adminspa/superadmin$', seguimiento, name='seguimiento'),
    url(r'^save_estado$', save_estado, name='save_estado'),
    url(r'^adminspa/sugerencias$', sugerencias_admin, name='sugerencias_admin'),
    #url(r'^holaaa$', infobip_example, name='infobip'),
    url(r'^status$', info_status, name='status'),
    url(r'^nueva_sucursal/(?P<pk>\d+)/$', login_required(nueva_sucursal), name='nueva_sucursal'),
    url(r'^acerca_de/$', acerca_de, name='acerca_de'),
    url(r'^terminos/$', terminos_condiciones, name='terminos_condiciones'),
    url(r'^adminspa/admin_transportista/$', admin_transportista, name='admin_transportista'),

]
