from django.contrib.auth import authenticate
from django.http import HttpResponse
from django.contrib.auth import login as auth_login
from django.shortcuts import render, redirect, render_to_response
from django.core.urlresolvers import reverse_lazy
from administracionAPP.forms import ContactoForm, NuevoProveedor, NuevaSucursal
from administracionAPP.models import Contacto
from django.views.generic import CreateView
from django.core.mail import EmailMultiAlternatives
from django.contrib import messages
from spaapp.models import Proveedor, Perfil, ActividadComercial, Categoria, Menu, Direccion, Ciudad, Sucursal, \
    Pedido, Direccion, TipoPago, TipoPedido, CatalogoEstado, Sugerencia, UserSurcursal
from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError
from django.template import RequestContext
from django.http import JsonResponse
import json
from datetime import datetime
from django.utils.timezone import localtime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

import http.client


'''
def infobip_example(request):
    conn = http.client.HTTPSConnection("api.infobip.com")

    payload = "{\"from\":\"Cidsecure\",\"to\":\"0985116900\",\"text\":\"vvvvvvvvvv\"}"

    headers = {
        'authorization': "Basic Q2lkc2VjdXJlOlNNU2NkczQ1Ng==",
        'content-type': "application/json",
        'accept': "application/json"
    }

    conn.request("POST", "/sms/1/text/single", payload, headers)

    res = conn.getresponse()
    data = res.read()

    print(data.decode("utf-8"))
    return HttpResponse('siiiiiii')
'''
def info_status(request):
    conn = http.client.HTTPSConnection("api.infobip.com")

    headers = {
        'authorization': "Basic Q2lkc2VjdXJlOlNNU2NkczQ1Ng==",
        'accept': "application/json"
    }

    conn.request("GET", "/sms/1/reports", headers=headers)

    res = conn.getresponse()
    data = res.read()

    print(data.decode("utf-8"))
    return HttpResponse('status')


def registro_contacto(request):
    args = {}

    if request.method == 'POST':
        form = ContactoForm(request.POST)
        if form.is_valid():
            nombre_propietario = request.POST['nombre_propietario']
            apellidos = request.POST['apellidos']
            ciudad = request.POST['ciudad']
            nombre_empresa = request.POST['nombre_empresa']
            direccion = request.POST['direccion']
            tipo_negocio = request.POST['tipo_negocio']
            telefono = request.POST['telefono']
            email = request.POST['email']
            model = Contacto(nombre_propietario=nombre_propietario, apellidos=apellidos, ciudad=ciudad,
                             nombre_empresa=nombre_empresa, direccion=direccion, tipo_negocio=tipo_negocio,
                             telefono=telefono, correo=email)
            model.save()

            ### Configuracion para enviar por email
            to_admin1 = 'info@goodappetit.com'
            html_content = "<b>Contacto recibido de la empresa:</b> %s <br><br><b>Nombre del propietario: </b>%s <br>" \
                           "<b>Apellidos: </b>%s<br><b>Ciudad: </b>%s<br><b>Dirección: </b>%s<br><b>Tipo de negocio: </b>%s<br>" \
                           "<b>Teléfono: </b>%s<br><b>Correo electrónico: </b>%s" % (
                           nombre_empresa, nombre_propietario, apellidos, ciudad, direccion, tipo_negocio, telefono,
                           email)
            msg1 = EmailMultiAlternatives('Correo Contacto', html_content, 'from@server.com', [to_admin1])
            msg1.attach_alternative(html_content, 'text/html')
            msg1.send()
            messages.success(request, '!Gracias!, nos pondremos en contacto contigo')
            return redirect('contacto_adm')
    else:
        if request.user.is_authenticated():
            perfil = Perfil.objects.get(id_usuario=request.user)
            args['perfil'] = perfil
        form = ContactoForm()
        #return render(request, 'administracion/contacto_form.html', {'form':form})
    args['form'] = form
    return render_to_response('administracion/contacto_form.html', args, context_instance=RequestContext(request))

def proveedores_admin(request):
    ctx = {}
    if request.user.is_superuser:
        proveedores = Proveedor.objects.all().order_by('pk').reverse()
        sucursales = Sucursal.objects.all()
        perfil = Perfil.objects.get(id_usuario=request.user)

        ctx['prov'] = proveedores
        ctx['perfil'] = perfil
        ctx['sucursal'] = sucursales


        return render(request, 'administracion/proveedores_admin.html', ctx)
    else:
        return HttpResponse('Acceso Denegado')

def crear_proveedor_usuario(request):

    ctx = {}

    if request.user.is_superuser:
        if request.method == 'POST':
            form = NuevoProveedor(request.POST)
            if form.is_valid():
                ##### Crea nuevo usuario, se añade a grupo proveedor y nuevo perfil
                username = form.cleaned_data["username"]
                password = request.POST['password1']
                password2 = form.cleaned_data["confirmPassword"]
                first_name = form.cleaned_data["first_name"]
                last_name = form.cleaned_data["last_name"]
                usuario = User.objects.create_user(username=username, password=password, first_name=first_name,
                                                   last_name=last_name, email=username)
                usuario.save()

                group = Group.objects.get(name='Proveedor')
                user = User.objects.get(username=username)
                user.groups.add(group)
                ####################################################################
                ##### Crear nuevo Proveedor
                nombre_comercial = form.cleaned_data["nombre_comercial"]
                telefono = form.cleaned_data["telefono"]
                telefono_contacto = form.cleaned_data["telefono_contacto"]
                prioridad = form.cleaned_data["prioridad"]
                razon_social = form.cleaned_data["razon_social"]
                actividad = request.POST['actividad']
                categoria = request.POST['categoria']

                perfil = Perfil.objects.get(id_usuario=user.pk)
                proveedor = Proveedor(nombre_comercial=nombre_comercial, telefono=telefono,
                                      telefono_contacto=telefono_contacto, prioridad=prioridad,
                                      razon_social=razon_social, id_categoria_id=categoria,
                                      id_actividad_id=actividad, id_perfil_id=perfil.pk)
                proveedor.save()

                prov = Proveedor.objects.get(id_perfil=perfil.pk)

                #####################################################################
                ########### Crea nueva dirección

                calle_principal = form.cleaned_data["calle_principal"]
                calle_secundaria = form.cleaned_data["calle_secundaria"]
                numero_casa = form.cleaned_data["numero_casa"]
                referencia = form.cleaned_data["referencia"]

                direccion = Direccion(calle_principal=calle_principal, calle_secundaria=calle_secundaria,
                                      numero_casa=numero_casa, referencia=referencia,
                                      latitud='-3.98662568999', longitud='-79.196838475852', id_perfil_id=perfil.pk)
                direccion.save()

                dir = Direccion.objects.get(id_perfil=perfil.pk)

                ######################################################################
                ########## Crear Sucursal ###########################################

                nombre_sucursal = form.cleaned_data["nombre_sucursal"]
                dias_atencion_desde = form.cleaned_data["dias_atencion_desde"]
                dias_atencion_hasta = form.cleaned_data["dias_atencion_hasta"]
                dias_atencion = dias_atencion_desde + '-' + dias_atencion_hasta
                hora_atencion_hora_inicio = form.cleaned_data["hora_atencion_hora_inicio"]
                hora_atencion_minuto_inicio = form.cleaned_data["hora_atencion_minuto_inicio"]
                hora_atencion_hora_fin = form.cleaned_data["hora_atencion_hora_fin"]
                hora_atencion_minuto_fin = form.cleaned_data["hora_atencion_minuto_fin"]
                hora_atencion = hora_atencion_hora_inicio + ':' + hora_atencion_minuto_inicio + '-' + hora_atencion_hora_fin +\
                                ':' + hora_atencion_minuto_fin
                activo = form.cleaned_data["activo"]
                hora_pico_hora = form.cleaned_data["hora_pico_hora"]
                hora_pico_minuto = form.cleaned_data["hora_pico_minuto"]
                hora_pico = hora_pico_hora + ':' + hora_pico_minuto
                descripcion = form.cleaned_data["descripcion"]
                ciudad = request.POST['ciudad']

                menu = Menu.objects.get(id_proveedor=prov.pk)

                sucursal = Sucursal(nombre_sucursal=nombre_sucursal, razon_social=razon_social, telefono=telefono,
                                    telefono_contacto=telefono_contacto,
                                    dias_atencion=dias_atencion, hora_atencion=hora_atencion, prioridad=prioridad,
                                    activo=activo, hora_pico=hora_pico,
                                    me_gusta=1, id_ciudad_id=ciudad, id_direccion_id=dir.pk,
                                    id_proveedor_id=prov.pk, descripcion=descripcion,
                                    visitas=5, estado=1, precio_envio = 1)
                sucursal.save()
                sucur = Sucursal.objects.get(id_direccion=dir.pk)
                user_sucursal = UserSurcursal(id_user_id=user.pk, id_sucursal_id=sucur.pk)
                user_sucursal.save()
                menu.id_sucursal_id = sucur.pk
                menu.save()
                return redirect('proveedor_admin')


        else:
            form = NuevoProveedor
            actividad = ActividadComercial.objects.all()
            categoria = Categoria.objects.all()
            ciudad = Ciudad.objects.all()

            return render(request, 'administracion/crear_proveedor.html',
                          {'form': form, 'actividad': actividad, 'categoria': categoria,
                           'ciudad': ciudad})
    else:
        return HttpResponse('Acceso Denegado')

    ctx['form'] = form
    return render_to_response('administracion/crear_proveedor.html', ctx, context_instance=RequestContext(request))


def admin_seguimiento(request):

    if request.user.is_superuser:
        count = request.GET['count']
        page = request.GET['page']

        datos = Pedido.objects.all().order_by('fecha').reverse()
        paginador = Paginator(datos, count)
        pedidos = paginador.page(page)

        lista = []
        # tz = pytimezone('America/Guayaquil')


        for pedido in pedidos:
            usuario = User.objects.get(pk=pedido.usuario.pk)
            try:
                direccion = Direccion.objects.get(id_direccion=pedido.id_direccion_id)
            except Direccion.DoesNoExist:
                direccion = None
            tipo_pago = TipoPago.objects.get(pk=pedido.id_tipo_pago_id)
            tipo_pedido = TipoPedido.objects.get(id_tipo_pedido=pedido.id_tipo_pedido.id_tipo_pedido)
            estado = CatalogoEstado.objects.get(id_estado=pedido.id_estado.id_estado)
            sucursal = Sucursal.objects.get(id_sucursal = pedido.id_sucursal_id)
            row = []

            result = localtime(pedido.fecha)
            fecha = ("%s/%s/%s-%s:%s" % (result.day, result.month, result.year, result.hour, result.minute))

            row = json.dumps(
                {"pk": pedido.pk,
                 "cliente": usuario.first_name + ' ' + usuario.last_name,
                 "fecha": str(fecha),
                 "tipo_pago": tipo_pago.tipo_pago,
                 "tipo_pedido": tipo_pedido.tipo_pedido,
                 "valor": pedido.valor,
                 "estado": estado.estado,
                 "direccion": direccion.calle_principal + " - " + direccion.calle_secundaria,
                 "sucursal" : sucursal.nombre_sucursal,
                 "total": datos.count()
                 },
                sort_keys=True)
            lista.append(row)

        # data = serializers.serialize('json', pedidos)
        return JsonResponse({'lista': lista}, safe=False)
    return HttpResponse('Acceso Denegado')


def seguimiento(request):
    if request.user.is_superuser:
        perfil = Perfil.objects.get(id_usuario=request.user)
        return render(request, 'administracion/administracion_seguimiento.html', {'perfil':perfil})
    return HttpResponse('Acceso Denegado')

def save_estado(request):
    if request.is_ajax():
        id_pedido = request.GET['id_pedido']
        par_estado = request.GET['estado']
        estado = CatalogoEstado.objects.get(estado=par_estado)
        pedido = Pedido.objects.get(id_pedido=id_pedido)
        sucursal = pedido.id_sucursal
        proveedor = sucursal.id_proveedor
        perfil = proveedor.id_perfil
        usuario = perfil.id_usuario

        if request.GET['tiempo']!='0':
            tiempo = request.GET['tiempo']
            Pedido.objects.filter(id_pedido=id_pedido).update(id_estado=estado, tiempo=tiempo, notificado=False)
        else:
            Pedido.objects.filter(id_pedido=id_pedido).update(id_estado=estado, notificado=False)

    return redirect(seguimiento)


def sugerencias_admin(request):
    if request.user.is_superuser:
        perfil = Perfil.objects.get(id_usuario=request.user)
        sugerencia = Sugerencia.objects.all().order_by('fecha').reverse()
        return render(request, 'administracion/sugerencias_admin.html', {'sugerencias':sugerencia, 'perfil':perfil})
    return HttpResponse('Acceso Denegado')

def nueva_sucursal(request, pk):
    ctx = {}
    if request.user.is_superuser:
        ctx = {}
        proveedor = Proveedor.objects.get(id_proveedor = pk)

        if request.method == 'POST':
            form = NuevaSucursal(request.POST)
            if form.is_valid():

                ##### Crea nuevo usuario, se añade a grupo proveedor y nuevo perfil
                username = form.cleaned_data["username"]
                password = request.POST['password1']
                password2 = form.cleaned_data["confirmPassword"]
                first_name = form.cleaned_data["first_name"]
                last_name = form.cleaned_data["last_name"]
                usuario = User.objects.create_user(username=username, password=password, first_name=first_name,
                                                   last_name=last_name, email=username)
                usuario.save()

                group = Group.objects.get(name='Proveedor')
                user = User.objects.get(username=username)
                user.groups.add(group)
                ####################################################################
                perfil = Perfil.objects.get(id_usuario = user.pk)

                ########### Crea nueva dirección ################
                calle_principal = form.cleaned_data["calle_principal"]
                calle_secundaria = form.cleaned_data["calle_secundaria"]
                numero_casa = form.cleaned_data["numero_casa"]
                referencia = form.cleaned_data["referencia"]

                direccion = Direccion(calle_principal=calle_principal, calle_secundaria=calle_secundaria,
                                      numero_casa=numero_casa, referencia=referencia,
                                      latitud='-3.98662568999', longitud='-79.196838475852', id_perfil_id=perfil.pk)
                direccion.save()

                dir = Direccion.objects.get(id_perfil=perfil.pk)

                ######################################################################
                ########## Crear Sucursal ###########################################

                nombre_sucursal = form.cleaned_data["nombre_sucursal"]
                dias_atencion_desde = form.cleaned_data["dias_atencion_desde"]
                dias_atencion_hasta = form.cleaned_data["dias_atencion_hasta"]
                dias_atencion = dias_atencion_desde + '-' + dias_atencion_hasta
                telefono_contacto = form.cleaned_data["telefono"]
                hora_atencion_hora_inicio = form.cleaned_data["hora_atencion_hora_inicio"]
                hora_atencion_minuto_inicio = form.cleaned_data["hora_atencion_minuto_inicio"]
                hora_atencion_hora_fin = form.cleaned_data["hora_atencion_hora_fin"]
                hora_atencion_minuto_fin = form.cleaned_data["hora_atencion_minuto_fin"]
                hora_atencion = hora_atencion_hora_inicio + ':' + hora_atencion_minuto_inicio + '-' + hora_atencion_hora_fin + \
                                ':' + hora_atencion_minuto_fin
                activo = form.cleaned_data["activo"]
                hora_pico_hora = form.cleaned_data["hora_pico_hora"]
                hora_pico_minuto = form.cleaned_data["hora_pico_minuto"]
                hora_pico = hora_pico_hora + ':' + hora_pico_minuto
                descripcion = form.cleaned_data["descripcion"]
                ciudad = request.POST['ciudad']

                #menu = Menu.objects.latest('pk')
                #print(menu)
                #menu = Menu.objects.get(id_proveedor=proveedor.pk)

                sucursal = Sucursal(nombre_sucursal=nombre_sucursal, razon_social=proveedor.razon_social, telefono=proveedor.telefono,
                                    telefono_contacto=telefono_contacto,
                                    dias_atencion=dias_atencion, hora_atencion=hora_atencion, prioridad=proveedor.prioridad,
                                    activo=activo, hora_pico=hora_pico,
                                    me_gusta=1, id_ciudad_id=ciudad, id_direccion_id=dir.pk,
                                    id_proveedor_id=proveedor.pk, descripcion=descripcion,
                                    visitas=5, estado=1, precio_envio = 1)
                sucursal.save()
                sucur = Sucursal.objects.get(id_direccion = dir.pk)
                men = Menu(id_proveedor_id=proveedor.pk, id_sucursal_id = sucur.pk)
                men.save()
                user_sucursal = UserSurcursal(id_user_id = user.pk, id_sucursal_id = sucur.pk)
                user_sucursal.save()

                return redirect('proveedor_admin')

        else:
            form = NuevaSucursal

            ciudad = Ciudad.objects.all()
            ctx['form'] = form
            ctx['ciudad'] = ciudad
            return render(request, 'administracion/crear_sucursal.html', ctx)
    else:
        return HttpResponse('Acceso Denegado')
    ctx['form'] = form
    return render_to_response('administracion/crear_sucursal.html', ctx, context_instance=RequestContext(request))


def acerca_de(request):
    return render(request, 'administracion/acerca_de.html')

def terminos_condiciones(request):
    return render(request, 'administracion/terminos_condiciones.html')

''' Administración de transportistas '''

def admin_transportista(request):
    return render(request, 'administracion/admin_transportista.html')

''' Fin de Administración de transportistas '''