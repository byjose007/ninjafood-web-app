--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = spaapp, pg_catalog;

--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO django_content_type (id, app_label, model) VALUES (1, 'admin', 'logentry');
INSERT INTO django_content_type (id, app_label, model) VALUES (2, 'auth', 'permission');
INSERT INTO django_content_type (id, app_label, model) VALUES (3, 'auth', 'group');
INSERT INTO django_content_type (id, app_label, model) VALUES (4, 'auth', 'user');
INSERT INTO django_content_type (id, app_label, model) VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO django_content_type (id, app_label, model) VALUES (6, 'sessions', 'session');
INSERT INTO django_content_type (id, app_label, model) VALUES (7, 'spaapp', 'actividadcomercial');
INSERT INTO django_content_type (id, app_label, model) VALUES (8, 'spaapp', 'perfil');
INSERT INTO django_content_type (id, app_label, model) VALUES (9, 'spaapp', 'categoria');
INSERT INTO django_content_type (id, app_label, model) VALUES (10, 'spaapp', 'proveedor');
INSERT INTO django_content_type (id, app_label, model) VALUES (11, 'spaapp', 'ciudad');
INSERT INTO django_content_type (id, app_label, model) VALUES (12, 'spaapp', 'direccion');
INSERT INTO django_content_type (id, app_label, model) VALUES (13, 'spaapp', 'cliente');
INSERT INTO django_content_type (id, app_label, model) VALUES (14, 'spaapp', 'menu');
INSERT INTO django_content_type (id, app_label, model) VALUES (15, 'spaapp', 'sucursal');
INSERT INTO django_content_type (id, app_label, model) VALUES (16, 'spaapp', 'contrato');
INSERT INTO django_content_type (id, app_label, model) VALUES (17, 'spaapp', 'transportista');
INSERT INTO django_content_type (id, app_label, model) VALUES (18, 'spaapp', 'coordenadas');
INSERT INTO django_content_type (id, app_label, model) VALUES (19, 'spaapp', 'tipopago');
INSERT INTO django_content_type (id, app_label, model) VALUES (20, 'spaapp', 'tipopedido');
INSERT INTO django_content_type (id, app_label, model) VALUES (21, 'spaapp', 'pedido');
INSERT INTO django_content_type (id, app_label, model) VALUES (22, 'spaapp', 'platocategoria');
INSERT INTO django_content_type (id, app_label, model) VALUES (23, 'spaapp', 'plato');
INSERT INTO django_content_type (id, app_label, model) VALUES (24, 'spaapp', 'tamanio');
INSERT INTO django_content_type (id, app_label, model) VALUES (25, 'spaapp', 'tamanioplato');
INSERT INTO django_content_type (id, app_label, model) VALUES (26, 'spaapp', 'detallepedido');
INSERT INTO django_content_type (id, app_label, model) VALUES (27, 'spaapp', 'ingrediente');
INSERT INTO django_content_type (id, app_label, model) VALUES (28, 'spaapp', 'horariotransportista');
INSERT INTO django_content_type (id, app_label, model) VALUES (29, 'spaapp', 'modificardetallepedido');
INSERT INTO django_content_type (id, app_label, model) VALUES (30, 'spaapp', 'platoingrediente');
INSERT INTO django_content_type (id, app_label, model) VALUES (31, 'spaapp', 'redsocial');
INSERT INTO django_content_type (id, app_label, model) VALUES (32, 'spaapp', 'valoracion');
INSERT INTO django_content_type (id, app_label, model) VALUES (33, 'default', 'usersocialauth');
INSERT INTO django_content_type (id, app_label, model) VALUES (34, 'default', 'nonce');
INSERT INTO django_content_type (id, app_label, model) VALUES (35, 'default', 'association');
INSERT INTO django_content_type (id, app_label, model) VALUES (36, 'default', 'code');


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (4, 'Can add permission', 2, 'add_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (5, 'Can change permission', 2, 'change_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (6, 'Can delete permission', 2, 'delete_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (7, 'Can add group', 3, 'add_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (8, 'Can change group', 3, 'change_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (9, 'Can delete group', 3, 'delete_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (10, 'Can add user', 4, 'add_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (11, 'Can change user', 4, 'change_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (12, 'Can delete user', 4, 'delete_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (13, 'Can add content type', 5, 'add_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (14, 'Can change content type', 5, 'change_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (15, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (16, 'Can add session', 6, 'add_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (17, 'Can change session', 6, 'change_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (18, 'Can delete session', 6, 'delete_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (19, 'Can add Actividad Comercial', 7, 'add_actividadcomercial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (20, 'Can change Actividad Comercial', 7, 'change_actividadcomercial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (21, 'Can delete Actividad Comercial', 7, 'delete_actividadcomercial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (22, 'Can add perfil', 8, 'add_perfil');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (23, 'Can change perfil', 8, 'change_perfil');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (24, 'Can delete perfil', 8, 'delete_perfil');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (25, 'Can add categoria', 9, 'add_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (26, 'Can change categoria', 9, 'change_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (27, 'Can delete categoria', 9, 'delete_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (28, 'Can add proveedor', 10, 'add_proveedor');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (29, 'Can change proveedor', 10, 'change_proveedor');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (30, 'Can delete proveedor', 10, 'delete_proveedor');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (31, 'Can add ciudad', 11, 'add_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (32, 'Can change ciudad', 11, 'change_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (33, 'Can delete ciudad', 11, 'delete_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (34, 'Can add direccion', 12, 'add_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (35, 'Can change direccion', 12, 'change_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (36, 'Can delete direccion', 12, 'delete_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (37, 'Can add cliente', 13, 'add_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (38, 'Can change cliente', 13, 'change_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (39, 'Can delete cliente', 13, 'delete_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (40, 'Can add menu', 14, 'add_menu');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (41, 'Can change menu', 14, 'change_menu');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (42, 'Can delete menu', 14, 'delete_menu');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (43, 'Can add sucursal', 15, 'add_sucursal');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (44, 'Can change sucursal', 15, 'change_sucursal');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (45, 'Can delete sucursal', 15, 'delete_sucursal');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (46, 'Can add contrato', 16, 'add_contrato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (47, 'Can change contrato', 16, 'change_contrato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (48, 'Can delete contrato', 16, 'delete_contrato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (49, 'Can add transportista', 17, 'add_transportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (50, 'Can change transportista', 17, 'change_transportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (51, 'Can delete transportista', 17, 'delete_transportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (52, 'Can add Coordenada', 18, 'add_coordenadas');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (53, 'Can change Coordenada', 18, 'change_coordenadas');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (54, 'Can delete Coordenada', 18, 'delete_coordenadas');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (55, 'Can add tipo pago', 19, 'add_tipopago');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (56, 'Can change tipo pago', 19, 'change_tipopago');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (57, 'Can delete tipo pago', 19, 'delete_tipopago');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (58, 'Can add tipo pedido', 20, 'add_tipopedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (59, 'Can change tipo pedido', 20, 'change_tipopedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (60, 'Can delete tipo pedido', 20, 'delete_tipopedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (61, 'Can add pedido', 21, 'add_pedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (62, 'Can change pedido', 21, 'change_pedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (63, 'Can delete pedido', 21, 'delete_pedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (64, 'Can add plato categoria', 22, 'add_platocategoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (65, 'Can change plato categoria', 22, 'change_platocategoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (66, 'Can delete plato categoria', 22, 'delete_platocategoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (67, 'Can add plato', 23, 'add_plato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (68, 'Can change plato', 23, 'change_plato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (69, 'Can delete plato', 23, 'delete_plato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (70, 'Can add tamanio', 24, 'add_tamanio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (71, 'Can change tamanio', 24, 'change_tamanio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (72, 'Can delete tamanio', 24, 'delete_tamanio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (73, 'Can add tamanio plato', 25, 'add_tamanioplato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (74, 'Can change tamanio plato', 25, 'change_tamanioplato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (75, 'Can delete tamanio plato', 25, 'delete_tamanioplato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (76, 'Can add Detalle de pedido', 26, 'add_detallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (77, 'Can change Detalle de pedido', 26, 'change_detallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (78, 'Can delete Detalle de pedido', 26, 'delete_detallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (79, 'Can add ingrediente', 27, 'add_ingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (80, 'Can change ingrediente', 27, 'change_ingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (81, 'Can delete ingrediente', 27, 'delete_ingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (82, 'Can add Horario de transportista', 28, 'add_horariotransportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (83, 'Can change Horario de transportista', 28, 'change_horariotransportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (84, 'Can delete Horario de transportista', 28, 'delete_horariotransportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (85, 'Can add modificardetallepedido', 29, 'add_modificardetallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (86, 'Can change modificardetallepedido', 29, 'change_modificardetallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (87, 'Can delete modificardetallepedido', 29, 'delete_modificardetallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (88, 'Can add plato ingrediente', 30, 'add_platoingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (89, 'Can change plato ingrediente', 30, 'change_platoingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (90, 'Can delete plato ingrediente', 30, 'delete_platoingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (91, 'Can add Red social', 31, 'add_redsocial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (92, 'Can change Red social', 31, 'change_redsocial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (93, 'Can delete Red social', 31, 'delete_redsocial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (94, 'Can add valoracion', 32, 'add_valoracion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (95, 'Can change valoracion', 32, 'change_valoracion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (96, 'Can delete valoracion', 32, 'delete_valoracion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (97, 'Can add user social auth', 33, 'add_usersocialauth');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (98, 'Can change user social auth', 33, 'change_usersocialauth');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (99, 'Can delete user social auth', 33, 'delete_usersocialauth');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (100, 'Can add nonce', 34, 'add_nonce');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (101, 'Can change nonce', 34, 'change_nonce');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (102, 'Can delete nonce', 34, 'delete_nonce');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (103, 'Can add association', 35, 'add_association');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (104, 'Can change association', 35, 'change_association');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (105, 'Can delete association', 35, 'delete_association');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (106, 'Can add code', 36, 'add_code');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (107, 'Can change code', 36, 'change_code');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (108, 'Can delete code', 36, 'delete_code');


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_permission_id_seq', 108, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (3, 'pbkdf2_sha256$24000$64oZUcILaY8W$xqTBvRYRsUQbWddqUzTnponf2CA5bJ7mqDcn+wkMQXc=', NULL, false, 'Jorge', '', '', '', false, true, '2016-06-15 12:04:54.613631-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (2, 'pbkdf2_sha256$24000$7UqOxH4QajlU$/YtFSWz6bSDk5Gnj/phUHR2XoT84RdrKn2RLJWKbV4w=', '2016-06-29 16:31:58.500686-05', false, 'Rommel', '', '', '', false, true, '2016-06-15 12:04:40.244059-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (22, 'pbkdf2_sha256$24000$4CriB3w1dGjD$neN/G/jUO3FRDTtsqHOg5dKH0hpa+H+WSRoow7HYcio=', NULL, false, 'ELISABETH14', '', '', '', false, true, '2016-07-08 13:00:03-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (23, 'pbkdf2_sha256$24000$VGg36aPKM48N$odu6ym3ji3SjedP8w7fyyww54XITSSsuvXLodVT35qA=', NULL, false, 'ELOISA15', '', '', '', false, true, '2016-07-08 13:00:51-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (24, 'pbkdf2_sha256$24000$bR82dnouDaEL$fPBxlpnsLSCG2OTHHS9GkMQWtV0QLOocaE+j/wuQ6zU=', NULL, false, 'ELOY16', '', '', '', false, true, '2016-07-08 13:01:23-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (25, 'pbkdf2_sha256$24000$5mOlIOtTWByN$h6kC8PhrgzzVYborPOuQd1ctVLmnP9xRpDzGe1pHoBM=', NULL, false, 'ELSA17', '', '', '', false, true, '2016-07-08 13:01:49-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (26, 'pbkdf2_sha256$24000$5krEVRmOthgT$Ti7LN1MhmktgOK8hWSmSaz+D3FxIWPjoqus2tNPhzSo=', NULL, false, 'ELVIRA18', '', '', '', false, true, '2016-07-08 13:02:12-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (1, 'pbkdf2_sha256$24000$SdsH6fNP97Sy$HlRXZrKEdaPWzXUd4dM/pG20lNTZOA+w/mKUSA1GAkI=', '2016-07-06 17:02:10.772046-05', true, 'admin', '', '', '', true, true, '2016-06-15 12:00:26.312897-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (4, 'pbkdf2_sha256$24000$kBNWwbv33Shp$hvWKOf56vLLjuKgqev6TL618zpf1WshtckXTLZW2gBs=', NULL, false, 'Pamefer', '', '', '', false, true, '2016-07-06 17:39:37-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (5, 'pbkdf2_sha256$24000$WDcuHA0hlX3j$B8b/+NIxzG3tAD/gWNaLm59o2fh1HFUrl8hFXV/AK6g=', NULL, false, 'Nicolas_True', '', '', '', false, true, '2016-07-06 18:05:21-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (6, 'pbkdf2_sha256$24000$Aj1tLmr7DbPO$k7c6H8fttqRkYqUl/5tk/e74BHNA+SDHEcBWWSpKDO8=', NULL, false, 'Marcelo', '', '', '', false, true, '2016-07-06 18:30:37-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (7, 'pbkdf2_sha256$24000$fcaVUUb0YsWy$Gl+n0nwbKNYjnJFyRIbnzum6dqxWEmdu30K716McOyI=', NULL, false, 'Manuel', '', '', '', false, true, '2016-07-06 18:41:33-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (8, 'pbkdf2_sha256$24000$zns6JKsAJq0K$XVrkAirFUl/cbw4WvYofIqHP4HQFhohrasBiDJzH/E0=', NULL, false, 'daniel1', '', '', '', false, true, '2016-07-08 12:52:26-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (9, 'pbkdf2_sha256$24000$1CAGO3yyG4Qq$GwUe5MPe0vd5TiCIuq6l5nRZ0jYzwW4D8NY2Jor6KjM=', NULL, false, 'david2', '', '', '', false, true, '2016-07-08 12:52:55-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (10, 'pbkdf2_sha256$24000$gJmdiTvknSlY$bCLRhjeiVBCSRhNDMYCXDmYelDJkaH+0JLKa7dX0WC4=', NULL, false, 'pedro3', '', '', '', false, true, '2016-07-08 12:53:12-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (11, 'pbkdf2_sha256$24000$2NjEDfMoKFZJ$oM9AWsF9X3QCyJk0d9mH9s4qronQAEAXaNNwKtVrScU=', NULL, false, 'Rodrigo4', '', '', '', false, true, '2016-07-08 12:53:34-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (12, 'pbkdf2_sha256$24000$elqW2HN4QFG4$U4YQ0rGprIQPO9Ialj8lAXLOig8D+cggwPaRdHQZPSA=', NULL, false, 'EDGARDO5', '', '', '', false, true, '2016-07-08 12:55:01-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (13, 'pbkdf2_sha256$24000$NUBMCnRsHnc9$VpBSF/U7hOb1otjBxY5dy1xhaS9j18DSCuNoXeBzPx4=', NULL, false, 'EDITH6', '', '', '', false, true, '2016-07-08 12:55:29-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (14, 'pbkdf2_sha256$24000$20CoV6jjuFMI$hP5zUIl9rbrBw/1e98Pc8tkzCVPDoI5WFkhMlMHPUfM=', NULL, false, 'EDMUNDO7', '', '', '', false, true, '2016-07-08 12:56:13-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (15, 'pbkdf2_sha256$24000$IZKTXzhI0mwH$zju4jyB3Ea0YrQqN5WSOLLU5ZbUlbtCLtvP6jumWEg8=', NULL, false, 'EDUARDO8', '', '', '', false, true, '2016-07-08 12:56:42-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (16, 'pbkdf2_sha256$24000$kNGqcsUXnBFK$b61bhxBw3uCI88/y6JY7E6C78n82KdToXj3xpBsTt5E=', NULL, false, 'EFRAÍN9', '', '', '', false, true, '2016-07-08 12:57:09-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (17, 'pbkdf2_sha256$24000$SwpyRMNLKMnS$7OPnBYwNtnC3YSaO8eXFzQPkxvA/xQqEtFY91IzXqOE=', NULL, false, 'EFRÉN10', '', '', '', false, true, '2016-07-08 12:57:36-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (18, 'pbkdf2_sha256$24000$OF8fswR6QQsD$H1AkV/7d6ubZLcb56OWkQbUTA5Ilj33qpv+/0BBwaKY=', NULL, false, 'ELENA11', '', '', '', false, true, '2016-07-08 12:58:05-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (19, 'pbkdf2_sha256$24000$jbO6m4aIt82V$maBwQGG59RUg8Jc+3BiSN1l9e5grEVOWnlBSBiJmaBo=', NULL, false, 'ELEONORGT350', '', '', '', false, true, '2016-07-08 12:58:38-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (20, 'pbkdf2_sha256$24000$6xKr6zOsXr1q$69CdfOGsZH10rzJeoapN6ALbn/GooqdqHegYkBWXRRE=', NULL, false, 'ELÍAS12', '', '', '', false, true, '2016-07-08 12:59:14-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (21, 'pbkdf2_sha256$24000$p1NR9zFnsvqS$GGTvMvGAE/O3wSQv1Z4REAnHs+huCDjKHTufvgFGwPc=', NULL, false, 'ELISA13', '', '', '', false, true, '2016-07-08 12:59:41-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (27, 'pbkdf2_sha256$24000$jWTdJ1qJadQx$lgHpX26k8d/EIquDm0q1+yu9aT30lUynadMdCjYW7og=', NULL, false, 'EMILIA19', '', '', '', false, true, '2016-07-08 13:02:40-05');


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_id_seq', 27, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (1, '2016-06-15 12:04:40.277081-05', '2', 'Rommel', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (2, '2016-06-15 12:04:54.646653-05', '3', 'Jorge', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (3, '2016-06-15 12:06:12.978731-05', '1', 'Loja', 1, 'Añadido.', 11, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (4, '2016-06-15 12:08:11.878738-05', '1', 'Rommel', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (5, '2016-06-15 12:08:49.777126-05', '1', 'Orillas del zamora - guallaquil', 1, 'Añadido.', 12, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (6, '2016-06-15 12:09:06.866384-05', '1', 'Restaurante', 1, 'Añadido.', 7, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (7, '2016-06-15 12:10:24.654273-05', '1', 'Pescaderia', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (8, '2016-06-15 12:10:30.44958-05', '1', 'El Chinito', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (9, '2016-06-15 12:10:35.617207-05', '1', 'El Chinito', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (10, '2016-06-15 12:10:39.67797-05', '1', 'El chinito', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (11, '2016-06-15 12:11:42.059124-05', '2', 'Jorge', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (12, '2016-06-15 12:11:43.420196-05', '2', 'Av 8 de diciembre - Tribuno', 1, 'Añadido.', 12, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (13, '2016-06-15 12:12:11.226773-05', '2', 'Jugos y Frutas', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (14, '2016-06-15 12:12:18.598107-05', '2', 'Fruques', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (15, '2016-06-15 12:12:25.116803-05', '2', 'Fruques', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (16, '2016-06-15 12:12:26.838028-05', '2', 'Fruques', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (17, '2016-06-15 12:13:46.298956-05', '1', 'Hamburguesas', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (18, '2016-06-15 12:13:48.885274-05', '1', 'Cangre Burguer', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (19, '2016-06-15 12:14:24.001652-05', '2', 'Pescaderia', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (20, '2016-06-15 12:14:29.659738-05', '2', 'Encebollado sin cebolla', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (21, '2016-06-15 12:14:56.795491-05', '1', 'standard', 1, 'Añadido.', 24, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (22, '2016-06-15 12:15:03.88212-05', '1', 'Cangre Burguer : standard - 2.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (23, '2016-06-15 12:15:14.946072-05', '2', 'mediano', 1, 'Añadido.', 24, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (24, '2016-06-15 12:15:21.73469-05', '2', 'Cangre Burguer : mediano - 4.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (25, '2016-06-15 12:15:33.468906-05', '3', 'Grande', 1, 'Añadido.', 24, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (26, '2016-06-15 12:15:40.110761-05', '3', 'Cangre Burguer : Grande - 6.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (27, '2016-06-15 12:15:58.610857-05', '4', 'Encebollado sin cebolla : standard - 3.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (28, '2016-06-22 10:39:20.704869-05', '1', 'Orillas del zamora - guayaquil', 2, 'Modificado/a calle_secundaria.', 12, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (29, '2016-06-25 11:05:20.290618-05', '3', 'Ceviche', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (30, '2016-06-25 11:05:38.953088-05', '5', 'Ceviche : standard - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (31, '2016-06-25 11:53:03.88257-05', '1', 'Rommel', 1, 'Añadido.', 13, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (32, '2016-06-25 12:08:50.009174-05', '1', 'Jorge', 1, 'Añadido.', 17, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (33, '2016-06-25 12:10:53.37035-05', '1', 'Efectivo', 1, 'Añadido.', 19, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (34, '2016-06-25 12:11:23.157423-05', '1', 'A domicilio', 1, 'Añadido.', 20, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (35, '2016-06-27 19:38:23.91877-05', '12', '2016-06-27 00:00:00-05:00 - confirmado - 3.5', 2, 'Modificado/a estado y valor.', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (36, '2016-06-30 19:08:00.992838-05', '17', '2016-07-01 00:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (37, '2016-06-30 19:08:18.187708-05', '16', '2016-06-29 00:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (38, '2016-06-30 19:08:18.20468-05', '15', '2016-06-28 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (39, '2016-06-30 19:08:18.212898-05', '14', '2016-06-28 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (40, '2016-06-30 19:08:18.220965-05', '13', '2016-06-28 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (41, '2016-06-30 19:08:18.229436-05', '12', '2016-06-27 05:00:00+00:00 - confirmado - 3.5', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (42, '2016-06-30 19:08:18.237501-05', '11', '2016-06-27 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (43, '2016-06-30 19:08:18.245912-05', '10', '2016-06-27 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (44, '2016-06-30 19:08:18.254047-05', '9', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (45, '2016-06-30 19:08:18.262248-05', '8', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (46, '2016-06-30 19:08:18.270767-05', '7', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (47, '2016-06-30 19:08:18.278953-05', '6', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (48, '2016-06-30 19:08:18.286714-05', '5', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (49, '2016-06-30 19:08:18.295312-05', '4', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (50, '2016-06-30 19:08:18.303626-05', '3', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (51, '2016-06-30 19:08:18.311636-05', '2', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (52, '2016-06-30 19:08:18.31993-05', '1', '2016-06-25 05:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (53, '2016-06-30 19:11:36.516093-05', '19', '2016-07-01 00:00:00+00:00 - pendiente - 0.0', 2, 'Modificado/a id_sucursal.', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (54, '2016-06-30 19:20:12.91587-05', '21', '2016-07-01 00:00:00+00:00 - pendiente - 4.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (55, '2016-06-30 19:20:12.978213-05', '20', '2016-07-01 00:00:00+00:00 - pendiente - 4.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (56, '2016-06-30 19:20:12.986467-05', '19', '2016-07-01 00:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (57, '2016-06-30 19:20:12.994678-05', '18', '2016-07-01 00:00:00+00:00 - pendiente - 0.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (58, '2016-06-30 19:23:12.815505-05', '23', '2016-07-01 00:00:00+00:00 - pendiente - 4.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (59, '2016-06-30 19:23:12.873597-05', '22', '2016-07-01 00:00:00+00:00 - pendiente - 4.0', 3, '', 21, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (60, '2016-07-06 17:39:37.866195-05', '4', 'Pamefer', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (61, '2016-07-06 17:39:52.50409-05', '4', 'Pamefer', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (62, '2016-07-06 17:44:41.310402-05', '4', 'Pamefer', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (63, '2016-07-06 17:46:00.036887-05', '3', 'Papas', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (64, '2016-07-06 17:46:18.675285-05', '3', 'PapaTron', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (65, '2016-07-06 17:46:30.153134-05', '3', 'PapaTron', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (66, '2016-07-06 17:53:44.785455-05', '3', 'Papas', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (67, '2016-07-06 17:53:47.25773-05', '4', 'Choripapa', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (68, '2016-07-06 17:54:10.513827-05', '6', 'Choripapa : mediano - 1.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (69, '2016-07-06 17:57:18.118873-05', '5', 'Papa sola', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (70, '2016-07-06 18:01:15.622334-05', '6', 'papa re chori', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (71, '2016-07-06 18:02:23.902555-05', '7', 'papa voladora', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (72, '2016-07-06 18:02:53.039313-05', '7', 'Papa sola : mediano - 0.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (73, '2016-07-06 18:03:17.538435-05', '8', 'papa re chori : Grande - 2.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (74, '2016-07-06 18:03:35.360484-05', '9', 'papa voladora : Grande - 3.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (75, '2016-07-06 18:05:21.671763-05', '5', 'Nicolas_True', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (76, '2016-07-06 18:05:32.744938-05', '5', 'Nicolas_True', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (77, '2016-07-06 18:06:18.46416-05', '5', 'Nicolas_True', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (78, '2016-07-06 18:07:12.276049-05', '4', 'pizzas', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (79, '2016-07-06 18:07:22.055139-05', '4', 'Pizzaredonda', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (80, '2016-07-06 18:07:34.618252-05', '4', 'Pizzaredonda', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (81, '2016-07-06 18:15:27.91267-05', '3', 'capulis - duraznos', 1, 'Añadido.', 12, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (82, '2016-07-06 18:15:51.084364-05', '3', 'PapaTronSucursal', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (83, '2016-07-06 18:21:13.292189-05', '4', 'Hualtacos - Arabiscos', 1, 'Añadido.', 12, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (84, '2016-07-06 18:21:30.983184-05', '4', 'PizzaRedondaSucursal', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (85, '2016-07-06 18:21:45.000935-05', '5', 'Pizzaredonda', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (86, '2016-07-06 18:21:58.098267-05', '5', 'Pizzaredonda', 3, '', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (87, '2016-07-06 18:23:21.899952-05', '4', 'pizzeria', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (88, '2016-07-06 18:24:05.172289-05', '4', 'Pizzeria', 2, 'Modificado/a categoria.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (89, '2016-07-06 18:24:29.874617-05', '4', 'Pizza', 2, 'Modificado/a categoria.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (90, '2016-07-06 18:24:47.499228-05', '8', 'pizza personal', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (91, '2016-07-06 18:25:33.777185-05', '9', 'pizza mediana', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (92, '2016-07-06 18:26:11.425219-05', '10', 'pizza grande', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (93, '2016-07-06 18:26:39.837228-05', '10', 'pizza personal : standard - 10.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (94, '2016-07-06 18:27:07.213812-05', '11', 'pizza mediana : mediano - 15.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (95, '2016-07-06 18:27:21.597636-05', '12', 'pizza grande : Grande - 20.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (96, '2016-07-06 18:30:37.978304-05', '6', 'Marcelo', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (97, '2016-07-06 18:30:46.743648-05', '6', 'Marcelo', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (98, '2016-07-06 18:31:23.62349-05', '6', 'Marcelo', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (99, '2016-07-06 18:32:17.188755-05', '2', 'Heladeria', 1, 'Añadido.', 7, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (100, '2016-07-06 18:32:58.654595-05', '5', 'Heladeria', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (101, '2016-07-06 18:33:12.645127-05', '5', 'Iceballs', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (102, '2016-07-06 18:36:20.859607-05', '5', 'Rio Pastaza - Cononaco', 1, 'Añadido.', 12, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (103, '2016-07-06 18:37:00.551946-05', '6', 'Iceballs', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (104, '2016-07-06 18:37:09.075438-05', '5', 'iceballsSucursal', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (105, '2016-07-06 18:38:33.569306-05', '5', 'Helados', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (106, '2016-07-06 18:38:35.45076-05', '11', 'Ice one sm', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (107, '2016-07-06 18:39:11.013365-05', '12', 'ice two md', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (108, '2016-07-06 18:40:12.466991-05', '13', 'magnum ice', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (109, '2016-07-06 18:40:32.833966-05', '13', 'Ice one sm : standard - 2.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (110, '2016-07-06 18:40:49.754673-05', '14', 'ice two md : mediano - 3.75', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (111, '2016-07-06 18:41:08.195459-05', '15', 'magnum ice : Grande - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (112, '2016-07-06 18:41:34.011904-05', '7', 'Manuel', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (113, '2016-07-06 18:41:40.554409-05', '7', 'Manuel', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (114, '2016-07-06 18:42:23.0086-05', '7', 'Manuel', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (115, '2016-07-06 18:43:22.968446-05', '6', 'Comida China', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (116, '2016-07-06 18:43:29.305245-05', '6', 'Chino Rapidito al Pasito', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (117, '2016-07-06 18:43:38.635052-05', '7', 'Chino Rapidito al Pasito', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (118, '2016-07-06 18:45:06.018712-05', '6', 'Chaulafan', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (119, '2016-07-06 18:45:09.087801-05', '14', 'Chaulafan', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (120, '2016-07-06 18:45:29.050208-05', '16', 'Chaulafan : standard - 3.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (121, '2016-07-06 18:45:42.116176-05', '17', 'Chaulafan : mediano - 4.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (122, '2016-07-06 18:45:56.716492-05', '18', 'Chaulafan : Grande - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (123, '2016-07-06 18:46:54.485979-05', '7', 'Pollo', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (124, '2016-07-06 18:46:56.545454-05', '15', 'Pollo agridulce', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (125, '2016-07-06 18:47:16.065297-05', '19', 'Pollo agridulce : mediano - 6.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (126, '2016-07-06 18:47:28.81696-05', '20', 'Pollo agridulce : Grande - 7.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (127, '2016-07-06 18:50:06.4284-05', '6', 'Rio Morona - Rio Ucayali', 1, 'Añadido.', 12, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (128, '2016-07-08 11:23:30.846643-05', '10', 'pizza de carne grande', 2, 'Modificado/a nombre_producto.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (129, '2016-07-08 11:23:45.949757-05', '9', 'pizza de chorizo', 2, 'Modificado/a nombre_producto.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (130, '2016-07-08 11:23:56.845966-05', '10', 'pizza de carne', 2, 'Modificado/a nombre_producto.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (131, '2016-07-08 11:24:09.025914-05', '8', 'pizza de queso', 2, 'Modificado/a nombre_producto.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (132, '2016-07-08 11:25:31.049398-05', '5', 'IceBalls', 2, 'Modificado/a nombre_sucursal.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (133, '2016-07-08 11:25:57.58565-05', '4', 'PizzaRedonda', 2, 'Modificado/a nombre_sucursal.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (134, '2016-07-08 11:26:15.793181-05', '3', 'PapaTron', 2, 'Modificado/a nombre_sucursal.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (135, '2016-07-08 11:28:40.425372-05', '5', 'Nicolas_True', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (136, '2016-07-08 11:28:49.220491-05', '4', 'Pizzaredonda', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (137, '2016-07-08 11:29:34.722117-05', '6', 'Marcelo', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (138, '2016-07-08 11:29:37.122491-05', '5', 'Iceballs', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (139, '2016-07-08 11:29:57.72607-05', '5', 'Nicolas_True', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (140, '2016-07-08 11:30:00.648538-05', '4', 'Pizzaredonda', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (141, '2016-07-08 11:30:20.248873-05', '7', 'Manuel', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (142, '2016-07-08 11:30:22.06402-05', '6', 'Chino Rapidito al Pasito', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (143, '2016-07-08 11:31:53.771097-05', '4', 'Pamefer', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (144, '2016-07-08 11:31:58.222376-05', '3', 'PapaTron', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (145, '2016-07-08 12:52:26.341601-05', '8', 'daniel', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (146, '2016-07-08 12:52:41.01859-05', '8', 'daniel1', 2, 'Modificado/a username.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (147, '2016-07-08 12:52:55.951248-05', '9', 'david2', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (148, '2016-07-08 12:53:01.784822-05', '9', 'david2', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (149, '2016-07-08 12:53:13.006265-05', '10', 'pedro3', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (150, '2016-07-08 12:53:16.143142-05', '10', 'pedro3', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (151, '2016-07-08 12:53:34.631159-05', '11', 'Rodrigo4', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (152, '2016-07-08 12:53:39.09261-05', '11', 'Rodrigo4', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (153, '2016-07-08 12:55:01.479097-05', '12', 'EDGARDO5', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (154, '2016-07-08 12:55:04.309199-05', '12', 'EDGARDO5', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (155, '2016-07-08 12:55:29.117804-05', '13', 'EDITH6', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (156, '2016-07-08 12:55:32.244661-05', '13', 'EDITH6', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (157, '2016-07-08 12:56:13.087152-05', '14', 'EDMUNDO7', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (158, '2016-07-08 12:56:16.918618-05', '14', 'EDMUNDO7', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (159, '2016-07-08 12:56:42.554229-05', '15', 'EDUARDO8', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (160, '2016-07-08 12:56:45.961805-05', '15', 'EDUARDO8', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (161, '2016-07-08 12:57:09.520452-05', '16', 'EFRAÍN9', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (162, '2016-07-08 12:57:14.775523-05', '16', 'EFRAÍN9', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (163, '2016-07-08 12:57:36.969758-05', '17', 'EFRÉN10', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (164, '2016-07-08 12:57:41.187698-05', '17', 'EFRÉN10', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (165, '2016-07-08 12:58:05.12151-05', '18', 'ELENA11', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (166, '2016-07-08 12:58:11.482668-05', '18', 'ELENA11', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (167, '2016-07-08 12:58:38.189385-05', '19', 'ELEONORGT350', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (168, '2016-07-08 12:58:42.313546-05', '19', 'ELEONORGT350', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (169, '2016-07-08 12:59:14.180196-05', '20', 'ELÍAS12', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (170, '2016-07-08 12:59:19.315979-05', '20', 'ELÍAS12', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (171, '2016-07-08 12:59:41.4112-05', '21', 'ELISA13', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (172, '2016-07-08 12:59:46.410842-05', '21', 'ELISA13', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (173, '2016-07-08 13:00:03.626933-05', '22', 'ELISABETH14', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (174, '2016-07-08 13:00:07.272593-05', '22', 'ELISABETH14', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (175, '2016-07-08 13:00:51.135093-05', '23', 'ELOISA15', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (176, '2016-07-08 13:00:55.025186-05', '23', 'ELOISA15', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (177, '2016-07-08 13:01:23.607173-05', '24', 'ELOY16', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (178, '2016-07-08 13:01:27.442368-05', '24', 'ELOY16', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (179, '2016-07-08 13:01:50.009043-05', '25', 'ELSA17', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (180, '2016-07-08 13:01:53.645499-05', '25', 'ELSA17', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (181, '2016-07-08 13:02:12.068282-05', '26', 'ELVIRA18', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (182, '2016-07-08 13:02:15.875547-05', '26', 'ELVIRA18', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (183, '2016-07-08 13:02:40.27942-05', '27', 'EMILIA19', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (184, '2016-07-08 13:02:43.889119-05', '27', 'EMILIA19', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (185, '2016-07-08 13:05:45.700566-05', '8', 'daniel1', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (186, '2016-07-08 13:08:02.905667-05', '7', 'frezze helados', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (187, '2016-07-08 13:08:24.92234-05', '8', 'frezze helados', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (188, '2016-07-08 13:08:25.083687-05', '9', 'frezze helados', 1, 'Añadido.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (189, '2016-07-08 13:08:38.728236-05', '9', 'frezze helados', 3, '', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (190, '2016-07-08 13:08:43.474946-05', '8', 'frezze helados', 2, 'No ha cambiado ningún campo.', 14, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (191, '2016-07-08 13:09:50.624955-05', '16', 'helado de mora', 1, 'Añadido.', 23, 1);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 191, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_content_type_id_seq', 36, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO django_migrations (id, app, name, applied) VALUES (16, 'sessions', '0001_initial', '2016-06-15 00:00:00-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (5, 'contenttypes', '0002_remove_content_type_name', '2016-06-15 00:00:00-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (12, 'default', '0001_initial', '2016-06-15 00:00:00-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (17, 'spaapp', '0001_initial', '2016-06-15 00:00:00-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (3, 'admin', '0001_initial', '2016-06-15 00:00:00-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (2, 'auth', '0001_initial', '2016-06-15 00:00:00-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (1, 'contenttypes', '0001_initial', '2016-06-15 00:00:00-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (20, 'admin', '0002_logentry_remove_auto_add', '2016-07-06 17:24:36.785821-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (21, 'auth', '0002_alter_permission_name_max_length', '2016-07-06 17:24:36.885356-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (22, 'auth', '0003_alter_user_email_max_length', '2016-07-06 17:24:36.918021-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (23, 'auth', '0004_alter_user_username_opts', '2016-07-06 17:24:36.942605-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (24, 'auth', '0005_alter_user_last_login_null', '2016-07-06 17:24:36.964092-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (25, 'auth', '0006_require_contenttypes_0002', '2016-07-06 17:24:36.976023-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (26, 'auth', '0007_alter_validators_add_error_messages', '2016-07-06 17:24:36.995443-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (27, 'default', '0002_add_related_name', '2016-07-06 17:24:37.156232-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (28, 'default', '0003_alter_email_max_length', '2016-07-06 17:24:37.301181-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (29, 'default', '0004_auto_20160423_0400', '2016-07-06 17:24:37.349925-05');


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_migrations_id_seq', 29, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('6sadf9hguvm7nrzondm3iqp18viabkkd', 'NWQ3MmI2Yjk1ZTM3YTVmZTAzODk4MmU2NjU5ZjA5NGI1ZGE5MTE3MDp7Il9hdXRoX3VzZXJfaGFzaCI6IjgxMTE1YzQwNzcxYzRlYzZlNmFkNjczM2Y0ZDJjMWU2NGNiZDNiNmYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2016-07-06 10:38:55.084118-05');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('6zdew9nlsn2py1wykh9zgt5r28d05f3d', 'ODAzN2JmM2ZiNTUxNTkyZDQ1ZjkwZDIzZGFlMjNlYzMyMzgxOTk2NDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI4MTExNWM0MDc3MWM0ZWM2ZTZhZDY3MzNmNGQyYzFlNjRjYmQzYjZmIn0=', '2016-07-09 11:04:48.107926-05');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('5zxmq23ex1sd4gz2g81w3ods6tfntefa', 'ODAzN2JmM2ZiNTUxNTkyZDQ1ZjkwZDIzZGFlMjNlYzMyMzgxOTk2NDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI4MTExNWM0MDc3MWM0ZWM2ZTZhZDY3MzNmNGQyYzFlNjRjYmQzYjZmIn0=', '2016-07-11 19:37:07.869293-05');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('lmxaypq66oa80dt9y18qewxqzdkco9jl', 'NjEzY2FkMTcxZjdjNmUxZWYwMzU3MzkzOTdjMjY1NmZlNmNhZjA1ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzI1OTIyN2QwZDBkMDY2MTliYTZlYzJkYWE2YWYzNWU3ZTQwZjI4ZSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2016-07-13 16:31:58.503686-05');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('e4xzaqfutd0oxmxegzwbzc9wdu7yl2y0', 'ODBlYWU4NGY0MTk2ZmM0MmQxMDU0NjExNGVjODdmYmQ1ZGNiNTE3YTp7Il9hdXRoX3VzZXJfaGFzaCI6IjgxMTE1YzQwNzcxYzRlYzZlNmFkNjczM2Y0ZDJjMWU2NGNiZDNiNmYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2016-07-20 17:02:10.798154-05');


--
-- Data for Name: social_auth_association; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: social_auth_association_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_association_id_seq', 1, false);


--
-- Data for Name: social_auth_code; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: social_auth_code_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_code_id_seq', 1, false);


--
-- Data for Name: social_auth_nonce; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_nonce_id_seq', 1, false);


--
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_usersocialauth_id_seq', 1, false);


--
-- Data for Name: spaapp_actividadcomercial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_actividadcomercial (id_actividad, tipo_actividad, detalles) VALUES (1, 'Restaurante', 'Restaurante');
INSERT INTO spaapp_actividadcomercial (id_actividad, tipo_actividad, detalles) VALUES (2, 'Heladeria', 'vende helados frios');


--
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_actividadcomercial_id_actividad_seq', 2, true);


--
-- Data for Name: spaapp_categoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (1, 'Pescaderia', 'Detalle de categoría del restaurante Detalle de categoría del restaurante Detalle de categoría del restaurante Detalle de categoría del restaurante Detalle de categoría del restaurante');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (2, 'Jugos y Frutas', 'Detalle de categoría del restaurante');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (3, 'Papas', 'papitas');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (4, 'pizzas', 'vende pizzas');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (5, 'Heladeria', 'vende helados frios');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (6, 'Comida China', 'venta de comida china en general');


--
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_categoria_id_categoria_seq', 6, true);


--
-- Data for Name: spaapp_ciudad; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_ciudad (id_ciudad, nombre, descripcion) VALUES (1, 'Loja', 'Loja');


--
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ciudad_id_ciudad_seq', 1, true);


--
-- Data for Name: spaapp_perfil; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (2, '2222222', '2016-05-29', '454646', 'Masculino', 'usuarioFoto/fruques.png', '160', 3);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (3, '66666666', '2016-06-28', '52752', 'f', 'fgfhh', '44s4', 1);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (1, '9999999', '2016-06-22', '2696969', 'Masculino', 'usuarioFoto/chinito.jpg', '444', 2);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (6, '1104039288', '2016-07-06', '2345384', 'Masculino', 'usuarioFoto/icecreamsocial.jpg', 'android', 6);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (5, '1104039288', '2016-07-06', '2564734', 'Masculino', 'usuarioFoto/8128726-cocinar-con-pizza--Foto-de-archivo.jpg', 'android', 5);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (7, '1104039288', '2016-07-06', '2696969', 'Masculino', 'usuarioFoto/68479-chifa-casa-china-logo.png', 'windows', 7);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (4, '1102394200', '2016-07-06', '2590324', 'Femenino', 'usuarioFoto/papalogo.jpg', 'android', 4);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (8, '1102394200', '2016-07-08', '2590324', 'Masculino', 'usuarioFoto/helados_OT7G2lJ.jpg', '1234', 8);


--
-- Data for Name: spaapp_cliente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_cliente (id_cliente, fechainicio_suscripcion, fecha_pago, detalles_adicionales, pedidos_activos, id_perfil_id) VALUES (1, '2016-06-05', '2016-06-05', 'gg', true, 1);


--
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_cliente_id_cliente_seq', 1, true);


--
-- Data for Name: spaapp_direccion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) VALUES (2, 'Av 8 de diciembre', 'Tribuno', '15-65', '-3.982338', '-79.211080', 'Coca cola', 2);
INSERT INTO spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) VALUES (1, 'Orillas del zamora', 'guayaquil', '', '-3.967484', '-6855444', 'Hiper valle', 1);
INSERT INTO spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) VALUES (3, 'capulis', 'duraznos', '1232', '-4.020353672441599', '-79.19751584529877', 'alado del de alado', 4);
INSERT INTO spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) VALUES (4, 'Hualtacos', 'Arabiscos', '1234', '-4.018186426551485', '-79.19822126626968', 'alado del de alado', 5);
INSERT INTO spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) VALUES (5, 'Rio Pastaza', 'Cononaco', '1234', '-4.005257748819187', '-79.18784111738205', 'alado del chifa', 6);
INSERT INTO spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) VALUES (6, 'Rio Morona', 'Rio Ucayali', '4212', '-4.004457726653868', '-79.1868232190609', 'al otro lado del rio', 7);


--
-- Data for Name: spaapp_proveedor; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (1, 'El Chinito', '1131', '', 2, NULL, '14565', 1, 1, 1);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (2, 'Fruques', '21484848', '', 3, NULL, '22131', 1, 2, 2);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (5, 'Iceballs', '2045345', '2504000', 1, 1234, 'Ventas', 1, 5, 6);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (4, 'Pizzaredonda', '2045345', '2504000', 1, 1234, 'Ventas', 1, 4, 5);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (6, 'Chino Rapidito al Pasito', '2564734', '2504000', 1, 1234, 'vender', 1, 6, 7);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (3, 'PapaTron', '2045345', '2504000', 1, 1234, 'Ventas', 1, 3, 4);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (7, 'frezze helados', '2045345', '2504000', 2, 312, 'ventas', 2, 5, 8);


--
-- Data for Name: spaapp_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (1, 1);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (2, 2);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (3, 3);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (4, 4);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (6, 5);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (7, 6);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (8, 7);


--
-- Data for Name: spaapp_sucursal; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (1, 'El chinito', '102102', '1215448', '', 'lunes a viernes', '9:00 - 18:00', 2, false, '20:14:41', 41, 1, 1, 1, 1);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (2, 'Fruques', '145242', '4546', '', 'lunes a viernes', '9:00 - 18:00', 2, false, '20:14:41', 58, 1, 2, 2, 2);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (5, 'IceBalls', 'ventas', '2345384', '2504000', 'lunes a viernes', '24h', 1, true, '23:33:45', 1234, 1, 5, 6, 5);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (4, 'PizzaRedonda', 'venta', '2590324', '2504000', 'lunes a viernes', '24h', 1, true, '23:19:01', 1234, 1, 4, 4, 4);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (3, 'PapaTron', 'ventas', '2590324', '2504000', 'lunes a viernes', '24h', 1, false, '23:12:59', 1234, 1, 3, 3, 3);


--
-- Data for Name: spaapp_contrato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_contrato_id_contrato_seq', 1, false);


--
-- Data for Name: spaapp_coordenadas; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_coordenadas_id_coordenadas_seq', 1, false);


--
-- Data for Name: spaapp_tipopago; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_tipopago (id_tipo_pago, tipo_pago, observaciones) VALUES (1, 'Efectivo', 'Efectivo');


--
-- Data for Name: spaapp_tipopedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_tipopedido (id_tipo_pedido, tipo_pedido, observaciones) VALUES (1, 'A domicilio', 'Llamada');


--
-- Data for Name: spaapp_transportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_transportista (id_transportista, tipo_transporte, tipo_servicio, id_perfil_id) VALUES (1, 'moto', 'tramsprte', 2);


--
-- Data for Name: spaapp_pedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Data for Name: spaapp_platocategoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (1, 'Hamburguesas');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (2, 'Pescaderia');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (3, 'Papas');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (4, 'Pizza');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (5, 'Helados');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (6, 'Chaulafan');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (7, 'Pollo');


--
-- Data for Name: spaapp_plato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (2, 'Encebollado sin cebolla', 'menuFoto/encebollado.jpg', 'Pescado, Yuca, Platano', 20, 15, '18:00:00', 25, 12, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (3, 'Ceviche', '', 'Pescado, Yuca, Platano', 20, 15, '11:05:11', 25, 12, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (1, 'Cangre Burguer', 'menuFoto/hamburguesa.jpg', 'Trozos de pollo tikka en salsa de nata, tomate y mantequilla', 20, 20, '18:00:00', 25, 12, 1);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (4, 'Choripapa', 'menuFoto/papas.jpg', 'Papas ricas', 150, 100, '22:51:31', 200, 150, 3);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (5, 'Papa sola', 'menuFoto/papas_PJCSWZH.jpg', 'papas sin chori', 200, 100, '22:57:05', 90, 150, 3);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (6, 'papa re chori', '', 'papa doble chori', 200, 150, '22:58:03', 140, 140, 3);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (7, 'papa voladora', 'menuFoto/papas_fXJUuD3.jpg', 'papa con pollo', 300, 200, '23:02:03', 160, 150, 3);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (11, 'Ice one sm', 'menuFoto/helados.jpg', 'helado pequenio', 300, 300, '23:38:12', 300, 300, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (12, 'ice two md', 'menuFoto/helados_jdPxPPZ.jpg', 'helado mediano', 400, 400, '23:39:03', 400, 400, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (13, 'magnum ice', 'menuFoto/comidachina.jpg', 'triple bola grade de helado', 100, 100, '23:40:00', 100, 100, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (14, 'Chaulafan', 'menuFoto/comidachina_4JnynkQ.jpg', 'Suculento chaulafan', 400, 400, '23:44:35', 400, 400, 6);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (15, 'Pollo agridulce', 'menuFoto/comidachina_vPW0e3Q.jpg', 'pollo agrio y dulce', 500, 500, '23:46:36', 500, 400, 7);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (9, 'pizza de chorizo', 'menuFoto/pizza_3T9fUpm.jpeg', 'pizza mediana', 200, 200, '23:25:22', 100, 100, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (10, 'pizza de carne', 'menuFoto/pizza_ig5TLkz.jpeg', 'pizza grande', 200, 200, '23:25:53', 100, 100, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (8, 'pizza de queso', 'menuFoto/pizza.jpeg', 'pizza tamanio personal', 200, 200, '23:22:44', 140, 100, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (16, 'helado de mora', 'menuFoto/helados_ccnzoMZ.jpg', 'mora y helado', 123, 123, '18:09:38', 123, 123, 5);


--
-- Data for Name: spaapp_detallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_detallepedido_id_detalle_seq', 1, false);


--
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_direccion_id_direccion_seq', 6, true);


--
-- Data for Name: spaapp_horariotransportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_horariotransportista_id_horario_seq', 1, false);


--
-- Data for Name: spaapp_ingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ingrediente_id_ingrediente_seq', 1, false);


--
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_menu_id_menu_seq', 9, true);


--
-- Data for Name: spaapp_modificardetallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_modificardetallepedido_id_modificardetallepedido_seq', 1, false);


--
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_pedido_id_pedido_seq', 26, true);


--
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_perfil_id_perfil_seq', 8, true);


--
-- Data for Name: spaapp_plato_id_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (1, 1, 1);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (2, 2, 1);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (3, 3, 1);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (4, 4, 3);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (5, 5, 3);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (6, 6, 3);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (7, 7, 3);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (8, 8, 4);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (9, 9, 4);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (10, 10, 4);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (11, 11, 6);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (12, 12, 6);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (13, 13, 6);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (14, 14, 7);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (15, 15, 7);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (16, 16, 8);


--
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_menu_id_seq', 16, true);


--
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_plato_seq', 16, true);


--
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platocategoria_id_platocategoria_seq', 7, true);


--
-- Data for Name: spaapp_platoingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platoingrediente_id_plato_ingrediente_seq', 1, false);


--
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_proveedor_id_proveedor_seq', 7, true);


--
-- Data for Name: spaapp_redsocial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_redsocial_id_red_social_seq', 1, false);


--
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_sucursal_id_sucursal_seq', 5, true);


--
-- Data for Name: spaapp_tamanio; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_tamanio (id_tamanio, tamanio) VALUES (1, 'standard');
INSERT INTO spaapp_tamanio (id_tamanio, tamanio) VALUES (2, 'mediano');
INSERT INTO spaapp_tamanio (id_tamanio, tamanio) VALUES (3, 'Grande');


--
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanio_id_tamanio_seq', 3, true);


--
-- Data for Name: spaapp_tamanioplato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (1, 2.5, 1, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (2, 4.5, 1, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (3, 6, 1, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (4, 3, 2, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (5, 3.5, 3, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (6, 1.5, 4, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (7, 0.5, 5, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (8, 2, 6, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (9, 3, 7, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (10, 10, 8, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (11, 15, 9, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (12, 20, 10, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (13, 2, 11, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (14, 3.75, 12, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (15, 3.5, 13, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (16, 3, 14, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (17, 4, 14, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (18, 5, 14, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (19, 6, 15, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (20, 7.5, 15, 3);


--
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanioplato_id_tamanioplato_seq', 20, true);


--
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopago_id_tipo_pago_seq', 1, true);


--
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopedido_id_tipo_pedido_seq', 1, true);


--
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_transportista_id_transportista_seq', 1, true);


--
-- Data for Name: spaapp_valoracion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_valoracion_id_valoracion_seq', 1, false);


--
-- PostgreSQL database dump complete
--

