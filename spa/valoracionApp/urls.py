from django.conf.urls import include, url, patterns
from django.contrib.auth.views import login, logout_then_login
from . import views


urlpatterns = [
    url(r'^perfil$', views.perfil, name='perfil'),
    url(r'^actualizarEstadoPedido$', views.actualizarEstadoPedido),
    url(r'^guardarValoracion$', views.guardarValoracion),
    url(r'^ordenarPorPrecio$', views.ordenarPorPrecio),
    url(r'^ordenarPorNombre$', views.ordenarPorNombre),
    url(r'^ordenarPorFecha$', views.ordenarPorFecha),
    url(r'^ordenarPorEstado$', views.ordenarPorEstado),
    url(r'^buscarTxt$', views.buscarTxt),
    url(r'^cancelarPedido$', views.cancelarPedido),
]
