from django.shortcuts import render, redirect, render_to_response
from django.template.context_processors import request

from spaapp import models as mod
import datetime
from django.core import serializers
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.db.models import Avg, Q
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required, login_required
from django.core.exceptions import ObjectDoesNotExist
# Create your views here.

# Permisos: Debe ser Cliente
@permission_required("spaapp.esCliente", raise_exception=True)
def perfil(request):
    print('seeee')
    user = request.user.id
    users = request.user
    profileType = None
    if users.has_perm("spaapp.esTransportista"):
        print('es transportista')
        # TODO poner imagen por defecto para todos los perfiles de los transportistas
        perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
    else:
        print('Cliente o Proveedor')
        perfil = mod.Perfil.objects.get(id_usuario=request.user)

    # JY, Determinar que tipo de Usuario esta Autenticado
    profileType = validarTipoUsuario(request)
    # FIN

    if request.method == 'POST':
        res=request.POST['opcion']
        pedidosNu = ""
        pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
        ###### numero de pedidos nuevos #######

        resPediDeta = []
        dic = {}

        ################### VALORACION #######(SE NECESITA AVG)###########
        sucursales = mod.Sucursal.objects.distinct('id_sucursal')
        puntuacion = {}

        # user = request.user.id
        # users = request.user
        # if users.has_perm("spaapp.esTransportista"):
        #     print('es transportista')
        #     # TODO poner imagen por defecto para todos los perfiles de los transportistas
        #     perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
        # else:
        #     print('Cliente o Proveedor')
        #     perfil = mod.Perfil.objects.get(id_usuario=request.user)

        for sucur in sucursales:
            pro = mod.Valoracion.objects.filter(id_sucursal=sucur.id_sucursal).aggregate(Avg('valoracion_general'))
            puntuacion = {sucur: pro}

        sucursales = mod.Sucursal.objects.all()
        valoracion = mod.Valoracion.objects.all()
        if (res == 'Fecha'):
            pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('fecha')
        elif(res=="Nombre"):
            pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('id_sucursal__nombre_sucursal')
        elif (res == "Precio"):
            pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('-valor')
        elif (res == "Estado de Entrega"):
            pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('-id_estado_id')
        else:
            pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('id_sucursal__nombre_sucursal')
        detalles = mod.DetallePedido.objects.all()
        for pe in pedido:
            detal = detalles.filter(id_pedido=pe.id_pedido)
            fechapPedido = pe.fecha + datetime.timedelta(hours=5);
            try:
                valor = mod.Valoracion.objects.get(id_pedido=pe.id_pedido)
            except ObjectDoesNotExist:
                valor = None
            dic = {'pedido': pe, 'detalle': detal, 'valor': valor, 'fechaModifi': fechapPedido}
            resPediDeta.append(dic)
    else:
        ###### numero de pedidos nuevos #######
        pedidosNu = ""
        pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
        ###### numero de pedidos nuevos #######

        resPediDeta=[]
        dic={}

        ################### VALORACION #######(SE NECESITA AVG)###########
        sucursales = mod.Sucursal.objects.distinct('id_sucursal')
        puntuacion={}



        # user = request.user.id
        # users = request.user
        # profileType = None
        # if users.has_perm("spaapp.esTransportista"):
        #     print('es transportista')
        #     # TODO poner imagen por defecto para todos los perfiles de los transportistas
        #     perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
        # else:
        #     print('Cliente o Proveedor')
        #     perfil = mod.Perfil.objects.get(id_usuario=request.user)
        #
        # # JY, Determinar que tipo de Usuario esta Autenticado
        # profileType = validarTipoUsuario(request)
        # # FIN



        for sucur in sucursales:
            pro = mod.Valoracion.objects.filter(id_sucursal=sucur.id_sucursal).aggregate(Avg('valoracion_general'))
            puntuacion={sucur:pro}

        sucursales = mod.Sucursal.objects.all()
        valoracion = mod.Valoracion.objects.all()
        pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('-fecha')
        #for fechaP in pedido:
        #    fechapPedido=fechaP.fecha + datetime.timedelta(hours=5);
        #    print(fechaP.fecha,'<- normal')
        #    print(fechapPedido)
        detalles=mod.DetallePedido.objects.all()
        for pe in pedido:
            detal=detalles.filter(id_pedido=pe.id_pedido)
            fechapPedido = pe.fecha
            try:
                valor = mod.Valoracion.objects.get(id_pedido=pe.id_pedido)
            except ObjectDoesNotExist:
                valor = None
            dic={'pedido':pe,'detalle':detal,'valor':valor, 'fechaModifi':fechapPedido}
            resPediDeta.append(dic)


    return render(request, "home/perfilUser.html", {'sucursales': sucursales,'valoracion': valoracion,'resPediDeta':resPediDeta,
                                                    'perfil':perfil,'pedidosNu':pedidosNu, 'profileType':profileType})


def actualizarEstadoPedido(request):
    if request.is_ajax():
        usuario = request.user
        pedido = mod.Pedido.objects.filter(usuario_id=usuario).order_by('-fecha')

        if len(pedido) < 1:
            return HttpResponse('false', content_type="application/json")
        else:
            #pedido = pedidos
            #if pedido.count() > 0:
            dataPers = serializers.serialize('json', pedido)
            return HttpResponse(dataPers, content_type="application/json")
            #return render('true', "application/json", {'pedido': pedido})

def guardarValoracion(request):
    if request.method == "POST" and request.is_ajax():
        valoracionG = request.POST['valoracion']
        idSucursal = request.POST['idSucursal']
        idPedido = request.POST['idPedido']
        id_ped=mod.Pedido.objects.get(id_pedido=idPedido)
        if mod.Valoracion.objects.filter(id_pedido=idPedido).exists():
            status = "exists"
            return HttpResponse(status)
        else:
            if(int(valoracionG)==0 or int(valoracionG)>5):
                ## comprobar si los datos recibidos estan en el ragon requerido, para que si valoracion no sea mayor a 5
                status = "hack"
                return HttpResponse(status)
            else:
                ####### VALOR QUEMADO DEL LA VARIABLE ID_CLIENTE, RETIFICAR CODIGO Y MODIFICAR EL MODELS ######
                user = request.user
                #usuario=mod.Cliente.objects.get(id_cliente=1)
                id_provee=mod.Proveedor.objects.get(id_proveedor=id_ped.id_sucursal.id_proveedor_id)
                id_trans=mod.Transportista.objects.get(id_transportista=id_ped.id_trasportista_id)
                id_sucur=mod.Sucursal.objects.get(id_sucursal=id_ped.id_sucursal_id)
                #id_provee=transpor.id_sucursal.id_proveedor.pk

                # valor=mod.Valoracion(valor_transportista = valoracionG, observaciones = "", usuario = user, id_pedido = id_ped,
                #                  id_proveedor = id_provee, id_transportista = id_trans, valor_proveedor = valoracionG,
                #                  valor_servicio = valoracionG, valoracion_general = valoracionG, id_sucursal = id_sucur)

                valor=mod.Valoracion(observaciones = "", usuario = user, id_pedido = id_ped,
                                 valoracion_general = valoracionG, id_sucursal = id_sucur)
                valor.save()
                #f = Folder.objects.create(name=name, user=user)
                status = "Good"
                return HttpResponse(status)
    else:
        status = "hack"
        return HttpResponse(status)


################# VALORR ########################
################# VALORR ########################
################# VALORR ########################
def ordenarPorPrecio(request):
    if request.method == "POST" and request.is_ajax():
        idOrden = request.POST['idOrden']

    pedidosNu = ""
    pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
    ###### numero de pedidos nuevos #######

    resPediDeta = []
    dic = {}

    ################### VALORACION #######(SE NECESITA AVG)###########
    sucursales = mod.Sucursal.objects.distinct('id_sucursal')
    puntuacion = {}
    user = request.user.id
    users = request.user
    if users.has_perm("spaapp.esTransportista"):
        print('es transportista')
        # TODO poner imagen por defecto para todos los perfiles de los transportistas
        perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
    else:
        print('Cliente o Proveedor')
        perfil = mod.Perfil.objects.get(id_usuario=request.user)

    for sucur in sucursales:
        pro = mod.Valoracion.objects.filter(id_sucursal=sucur.id_sucursal).aggregate(Avg('valoracion_general'))
        puntuacion = {sucur: pro}

    sucursales = mod.Sucursal.objects.all()
    valoracion = mod.Valoracion.objects.all()
    if(idOrden=='0'):
        pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('-valor')
    else:
        pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('valor')
    detalles = mod.DetallePedido.objects.all()
    for pe in pedido:
        detal = detalles.filter(id_pedido=pe.id_pedido)
        fechapPedido = pe.fecha + datetime.timedelta(hours=5);
        try:
            valor = mod.Valoracion.objects.get(id_pedido=pe.id_pedido)
        except ObjectDoesNotExist:
            valor = None
        dic = {'pedido': pe, 'detalle': detal, 'valor': valor, 'fechaModifi': fechapPedido}
        resPediDeta.append(dic)

    return render(request, "home/perfilUser.html",
                  {'sucursales': sucursales, 'valoracion': valoracion, 'resPediDeta': resPediDeta, 'perfil': perfil,
                   'pedidosNu': pedidosNu})

####################### NOMBRE ##############################
####################### NOMBRE ##############################
####################### NOMBRE ##############################
def ordenarPorNombre(request):
    if request.method == "POST" and request.is_ajax():
        idOrden = request.POST['idOrden']
    pedidosNu = ""
    pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
    ###### numero de pedidos nuevos #######

    resPediDeta = []
    dic = {}

    ################### VALORACION #######(SE NECESITA AVG)###########
    sucursales = mod.Sucursal.objects.distinct('id_sucursal')
    puntuacion = {}
    user = request.user.id
    users = request.user
    if users.has_perm("spaapp.esTransportista"):
        print('es transportista')
        # TODO poner imagen por defecto para todos los perfiles de los transportistas
        perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
    else:
        print('Cliente o Proveedor')
        perfil = mod.Perfil.objects.get(id_usuario=request.user)

    for sucur in sucursales:
        pro = mod.Valoracion.objects.filter(id_sucursal=sucur.id_sucursal).aggregate(Avg('valoracion_general'))
        puntuacion = {sucur: pro}

    sucursales = mod.Sucursal.objects.all()
    valoracion = mod.Valoracion.objects.all()
    if (idOrden == '0'):
        pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('-id_sucursal__nombre_sucursal')
    else:
        pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('id_sucursal__nombre_sucursal')
    detalles = mod.DetallePedido.objects.all()
    for pe in pedido:
        detal = detalles.filter(id_pedido=pe.id_pedido)
        fechapPedido = pe.fecha + datetime.timedelta(hours=5);
        try:
            valor = mod.Valoracion.objects.get(id_pedido=pe.id_pedido)
        except ObjectDoesNotExist:
            valor = None
        dic = {'pedido': pe, 'detalle': detal, 'valor': valor, 'fechaModifi': fechapPedido}
        resPediDeta.append(dic)

    return render(request, "home/perfilUser.html",
                  { 'valoracion': valoracion, 'resPediDeta': resPediDeta})

##################### FECHA ####################
##################### FECHA ####################
##################### FECHA ####################
def ordenarPorFecha(request):
    if request.method == "POST" and request.is_ajax():
        idOrden = request.POST['idOrden']
    pedidosNu = ""
    pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
    ###### numero de pedidos nuevos #######

    resPediDeta = []
    dic = {}

    ################### VALORACION #######(SE NECESITA AVG)###########
    sucursales = mod.Sucursal.objects.distinct('id_sucursal')
    puntuacion = {}
    user = request.user.id
    users = request.user
    if users.has_perm("spaapp.esTransportista"):
        print('es transportista')
        # TODO poner imagen por defecto para todos los perfiles de los transportistas
        perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
    else:
        print('Cliente o Proveedor')
        perfil = mod.Perfil.objects.get(id_usuario=request.user)

    for sucur in sucursales:
        pro = mod.Valoracion.objects.filter(id_sucursal=sucur.id_sucursal).aggregate(Avg('valoracion_general'))
        puntuacion = {sucur: pro}

    sucursales = mod.Sucursal.objects.all()
    valoracion = mod.Valoracion.objects.all()
    if (idOrden == '0'):
        pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('-fecha')
    else:
        pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('fecha')
    detalles = mod.DetallePedido.objects.all()
    for pe in pedido:
        detal = detalles.filter(id_pedido=pe.id_pedido)
        fechapPedido = pe.fecha + datetime.timedelta(hours=5);
        try:
            valor = mod.Valoracion.objects.get(id_pedido=pe.id_pedido)
        except ObjectDoesNotExist:
            valor = None
        dic = {'pedido': pe, 'detalle': detal, 'valor': valor, 'fechaModifi': fechapPedido}
        resPediDeta.append(dic)

    return render(request, "home/perfilUser.html",
                  {'sucursales': sucursales, 'valoracion': valoracion, 'resPediDeta': resPediDeta, 'perfil': perfil,
                   'pedidosNu': pedidosNu})

####################### ESTADO #############################
####################### ESTADO #############################
####################### ESTADO #############################
def ordenarPorEstado(request):
    if request.method == "POST" and request.is_ajax():
        idOrden = request.POST['idOrden']
    pedidosNu = ""
    pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
    ###### numero de pedidos nuevos #######

    resPediDeta = []
    dic = {}

    ################### VALORACION #######(SE NECESITA AVG)###########
    sucursales = mod.Sucursal.objects.distinct('id_sucursal')
    puntuacion = {}
    user = request.user.id
    users = request.user
    if users.has_perm("spaapp.esTransportista"):
        print('es transportista')
        # TODO poner imagen por defecto para todos los perfiles de los transportistas
        perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
    else:
        print('Cliente o Proveedor')
        perfil = mod.Perfil.objects.get(id_usuario=request.user)

    for sucur in sucursales:
        pro = mod.Valoracion.objects.filter(id_sucursal=sucur.id_sucursal).aggregate(Avg('valoracion_general'))
        puntuacion = {sucur: pro}

    sucursales = mod.Sucursal.objects.all()
    valoracion = mod.Valoracion.objects.all()
    pedido = mod.Pedido.objects.filter(usuario_id=user).order_by('id_estado_id')
    detalles = mod.DetallePedido.objects.all()
    for pe in pedido:
        detal = detalles.filter(id_pedido=pe.id_pedido)
        fechapPedido = pe.fecha + datetime.timedelta(hours=5);
        try:
            valor = mod.Valoracion.objects.get(id_pedido=pe.id_pedido)
        except ObjectDoesNotExist:
            valor = None
        dic = {'pedido': pe, 'detalle': detal, 'valor': valor, 'fechaModifi': fechapPedido}
        resPediDeta.append(dic)

    return render(request, "home/perfilUser.html",
                  {'sucursales': sucursales, 'valoracion': valoracion, 'resPediDeta': resPediDeta, 'perfil': perfil,
                   'pedidosNu': pedidosNu})

def buscarTxt(request):
    if request.method == "POST" and request.is_ajax():
        txtSearch = request.POST['txtSearch']
    pedidosNu = ""
    pedidosNu = mod.Pedido.objects.filter(usuario_id=request.user.pk).filter(id_estado=1).count()
    ###### numero de pedidos nuevos #######

    resPediDeta = []
    dic = {}

    ################### VALORACION #######(SE NECESITA AVG)###########
    sucursales = mod.Sucursal.objects.distinct('id_sucursal')
    puntuacion = {}
    user = request.user.id
    users = request.user
    if users.has_perm("spaapp.esTransportista"):
        print('es transportista')
        # TODO poner imagen por defecto para todos los perfiles de los transportistas
        perfil = mod.Perfil.objects.filter(id_usuario=request.user)[0]
    else:
        print('Cliente o Proveedor')
        perfil = mod.Perfil.objects.get(id_usuario=request.user)

    for sucur in sucursales:
        pro = mod.Valoracion.objects.filter(id_sucursal=sucur.id_sucursal).aggregate(Avg('valoracion_general'))
        puntuacion = {sucur: pro}

    sucursales = mod.Sucursal.objects.all()
    valoracion = mod.Valoracion.objects.all()

    detalles = mod.DetallePedido.objects.filter(id_plato__nombre_producto__icontains=txtSearch)
    #detalles2 = mod.DetallePedido.objects.get(id_pedido=detalles.id_pedido)
    for pe in detalles:
        pedido = mod.Pedido.objects.filter(usuario_id=user, id_pedido=pe.id_pedido_id)

    print(len(pedido))
    if len(pedido) ==0:
        return HttpResponse('false', content_type="application/json")
    else:
        for pe in pedido:
            detal = detalles.filter(id_pedido=pe.id_pedido)
            fechapPedido = pe.fecha + datetime.timedelta(hours=5);
            try:
                valor = mod.Valoracion.objects.get(id_pedido=pe.id_pedido)
            except ObjectDoesNotExist:
                valor = None
            dic = {'pedido': pe, 'detalle': detal, 'valor': valor, 'fechaModifi': fechapPedido}
            resPediDeta.append(dic)

        return render(request, "home/perfilUser.html",
                      {'sucursales': sucursales, 'valoracion': valoracion, 'resPediDeta': resPediDeta, 'perfil': perfil,
                       'pedidosNu': pedidosNu})

#JY, Funcion usada para determinar que tipo de usuario ingreso a applicacion
#0 Admin
#1 Cliente
#2 Proveedor
#3 Transportista
def validarTipoUsuario(request):
    if request.user.has_perm("spaapp.esAdmin"):
        return 0  # Admin
    elif request.user.has_perm("spaapp.esTransportista"):
        return 3  # Proveedor
    elif request.user.has_perm("spaapp.esProveedor"):
        return 2  # Proveedor
    elif request.user.has_perm("spaapp.esCliente"):
        return 1  # Cliente
    else:
        return None

def cancelarPedido(request):
    if request.is_ajax():
        id_pedido = request.GET['id_pedido']
        par_estado = request.GET['estado']
        usuario = request.user

        estado = mod.CatalogoEstado.objects.get(estado = par_estado)
        pedido = mod.Pedido.objects.filter(usuario_id=usuario).order_by('-fecha')
        mod.Pedido.objects.filter(id_pedido=id_pedido).update(id_estado=estado)
        dataPers = serializers.serialize('json', pedido)
        return HttpResponse(dataPers, content_type="application/json")
    else:
        raise  Http404