# Sistema de Provisionamiento de Alimentación

## Entorno Virtual:

#### Crea un entorno virtual con el commando:
```$ virtualenv -p `which python3` ninjaFood```

#### Siempre se requiere activar ese entorno virtual antes de trabajar con:
`$ source ruta/al/entorno/virtual/ninjaFood/bin/activate`

## Dependencias:

#### Para installar las dependencias del proyecto, corre:
`$ pip install -r requirements.txt`

dentro su entorno virtual creado y desde la carpeta raiz del proyecto.

#### Al agregar una dependencia, corre:
`$ pip freeze > requirements.txt`

dentro su entorno virtual creado y desde la carpeta raiz del proyecto.
