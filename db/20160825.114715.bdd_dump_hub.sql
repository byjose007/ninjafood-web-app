--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.15
-- Dumped by pg_dump version 9.5.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: spaapp; Type: SCHEMA; Schema: -; Owner: spauser
--

CREATE SCHEMA spaapp;


ALTER SCHEMA spaapp OWNER TO spauser;

SET search_path = spaapp, pg_catalog;

--
-- Name: insertar_menu(); Type: FUNCTION; Schema: spaapp; Owner: spauser
--

CREATE FUNCTION insertar_menu() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	BEGIN		INSERT INTO spaapp_menu (id_proveedor_id) VALUES (NEW.id_proveedor);		RETURN NULL;	END;$$;


ALTER FUNCTION spaapp.insertar_menu() OWNER TO spauser;

--
-- Name: insertar_perfil(); Type: FUNCTION; Schema: spaapp; Owner: spauser
--

CREATE FUNCTION insertar_perfil() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	BEGIN		INSERT INTO spaapp_perfil (id_usuario_id,ruta_foto, sim_dispositivo) VALUES (NEW.id,'usuarioFoto/userDefault.jpg',0);		RETURN NULL;	END;$$;


ALTER FUNCTION spaapp.insertar_perfil() OWNER TO spauser;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: administracionAPP_contacto; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE "administracionAPP_contacto" (
    id integer NOT NULL,
    nombre_propietario character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    ciudad character varying(50) NOT NULL,
    nombre_empresa character varying(50) NOT NULL,
    direccion character varying(50) NOT NULL,
    tipo_negocio character varying(50) NOT NULL,
    telefono double precision NOT NULL
);


ALTER TABLE "administracionAPP_contacto" OWNER TO spauser;

--
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE "administracionAPP_contacto_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "administracionAPP_contacto_id_seq" OWNER TO spauser;

--
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE "administracionAPP_contacto_id_seq" OWNED BY "administracionAPP_contacto".id;


--
-- Name: auth_group; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO spauser;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO spauser;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO spauser;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO spauser;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO spauser;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO spauser;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO spauser;

--
-- Name: auth_user_groups; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO spauser;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO spauser;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO spauser;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO spauser;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO spauser;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO spauser;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO spauser;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO spauser;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO spauser;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO spauser;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO spauser;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO spauser;

--
-- Name: social_auth_association; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_association (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    handle character varying(255) NOT NULL,
    secret character varying(255) NOT NULL,
    issued integer NOT NULL,
    lifetime integer NOT NULL,
    assoc_type character varying(64) NOT NULL
);


ALTER TABLE social_auth_association OWNER TO spauser;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_association_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_association_id_seq OWNER TO spauser;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_association_id_seq OWNED BY social_auth_association.id;


--
-- Name: social_auth_code; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_code (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    code character varying(32) NOT NULL,
    verified boolean NOT NULL
);


ALTER TABLE social_auth_code OWNER TO spauser;

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_code_id_seq OWNER TO spauser;

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_code_id_seq OWNED BY social_auth_code.id;


--
-- Name: social_auth_nonce; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_nonce (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    "timestamp" integer NOT NULL,
    salt character varying(65) NOT NULL
);


ALTER TABLE social_auth_nonce OWNER TO spauser;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_nonce_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_nonce_id_seq OWNER TO spauser;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_nonce_id_seq OWNED BY social_auth_nonce.id;


--
-- Name: social_auth_usersocialauth; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_usersocialauth (
    id integer NOT NULL,
    provider character varying(32) NOT NULL,
    uid character varying(255) NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE social_auth_usersocialauth OWNER TO spauser;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_usersocialauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_usersocialauth_id_seq OWNER TO spauser;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_usersocialauth_id_seq OWNED BY social_auth_usersocialauth.id;


--
-- Name: spaapp_actividadcomercial; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_actividadcomercial (
    id_actividad integer NOT NULL,
    tipo_actividad character varying(100) NOT NULL,
    detalles character varying(250) NOT NULL
);


ALTER TABLE spaapp_actividadcomercial OWNER TO spauser;

--
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_actividadcomercial_id_actividad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_actividadcomercial_id_actividad_seq OWNER TO spauser;

--
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_actividadcomercial_id_actividad_seq OWNED BY spaapp_actividadcomercial.id_actividad;


--
-- Name: spaapp_catalogoestado; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_catalogoestado (
    id_estado integer NOT NULL,
    estado character varying(50) NOT NULL
);


ALTER TABLE spaapp_catalogoestado OWNER TO spauser;

--
-- Name: spaapp_catalogoestado_id_estado_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_catalogoestado_id_estado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_catalogoestado_id_estado_seq OWNER TO spauser;

--
-- Name: spaapp_catalogoestado_id_estado_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_catalogoestado_id_estado_seq OWNED BY spaapp_catalogoestado.id_estado;


--
-- Name: spaapp_categoria; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_categoria (
    id_categoria integer NOT NULL,
    categoria character varying(150) NOT NULL,
    detalles character varying(250) NOT NULL,
    ruta_foto character varying(255)
);


ALTER TABLE spaapp_categoria OWNER TO spauser;

--
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_categoria_id_categoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_categoria_id_categoria_seq OWNER TO spauser;

--
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_categoria_id_categoria_seq OWNED BY spaapp_categoria.id_categoria;


--
-- Name: spaapp_ciudad; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_ciudad (
    id_ciudad integer NOT NULL,
    nombre character varying(80) NOT NULL,
    descripcion character varying(250) NOT NULL
);


ALTER TABLE spaapp_ciudad OWNER TO spauser;

--
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_ciudad_id_ciudad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_ciudad_id_ciudad_seq OWNER TO spauser;

--
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_ciudad_id_ciudad_seq OWNED BY spaapp_ciudad.id_ciudad;


--
-- Name: spaapp_cliente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_cliente (
    id_cliente integer NOT NULL,
    fechainicio_suscripcion date NOT NULL,
    fecha_pago date,
    detalles_adicionales character varying(250) NOT NULL,
    pedidos_activos boolean,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_cliente OWNER TO spauser;

--
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_cliente_id_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_cliente_id_cliente_seq OWNER TO spauser;

--
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_cliente_id_cliente_seq OWNED BY spaapp_cliente.id_cliente;


--
-- Name: spaapp_contrato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_contrato (
    id_contrato integer NOT NULL,
    codigo character varying(20) NOT NULL,
    detalles character varying(250) NOT NULL,
    vigencia integer NOT NULL,
    fecha_inicio date NOT NULL,
    "fechaFin" date NOT NULL,
    valor double precision NOT NULL,
    ruta_contrato character varying(100),
    id_proveedor_id integer,
    id_sucursal_id integer
);


ALTER TABLE spaapp_contrato OWNER TO spauser;

--
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_contrato_id_contrato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_contrato_id_contrato_seq OWNER TO spauser;

--
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_contrato_id_contrato_seq OWNED BY spaapp_contrato.id_contrato;


--
-- Name: spaapp_coordenadas; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_coordenadas (
    id_coordenadas integer NOT NULL,
    latitud character varying(100) NOT NULL,
    longitud character varying(100) NOT NULL,
    fecha timestamp with time zone,
    estado integer,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_coordenadas OWNER TO spauser;

--
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_coordenadas_id_coordenadas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_coordenadas_id_coordenadas_seq OWNER TO spauser;

--
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_coordenadas_id_coordenadas_seq OWNED BY spaapp_coordenadas.id_coordenadas;


--
-- Name: spaapp_detallepedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_detallepedido (
    id_detalle integer NOT NULL,
    valor double precision NOT NULL,
    cantidad_solicitada integer NOT NULL,
    id_pedido_id integer,
    id_plato_id integer,
    tamanio_id integer
);


ALTER TABLE spaapp_detallepedido OWNER TO spauser;

--
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_detallepedido_id_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_detallepedido_id_detalle_seq OWNER TO spauser;

--
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_detallepedido_id_detalle_seq OWNED BY spaapp_detallepedido.id_detalle;


--
-- Name: spaapp_direccion; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_direccion (
    id_direccion integer NOT NULL,
    calle_principal character varying(100) NOT NULL,
    calle_secundaria character varying(100) NOT NULL,
    numero_casa character varying(5),
    latitud character varying(100),
    longitud character varying(100),
    referencia character varying(250) NOT NULL,
    id_perfil_id integer
);


ALTER TABLE spaapp_direccion OWNER TO spauser;

--
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_direccion_id_direccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_direccion_id_direccion_seq OWNER TO spauser;

--
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_direccion_id_direccion_seq OWNED BY spaapp_direccion.id_direccion;


--
-- Name: spaapp_horariotransportista; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_horariotransportista (
    id_horario integer NOT NULL,
    dias character varying(80) NOT NULL,
    horas character varying(50) NOT NULL,
    id_transportista_id integer NOT NULL
);


ALTER TABLE spaapp_horariotransportista OWNER TO spauser;

--
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_horariotransportista_id_horario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_horariotransportista_id_horario_seq OWNER TO spauser;

--
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_horariotransportista_id_horario_seq OWNED BY spaapp_horariotransportista.id_horario;


--
-- Name: spaapp_ingrediente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_ingrediente (
    id_ingrediente integer NOT NULL,
    nombre character varying(100) NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_ingrediente OWNER TO spauser;

--
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_ingrediente_id_ingrediente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_ingrediente_id_ingrediente_seq OWNER TO spauser;

--
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_ingrediente_id_ingrediente_seq OWNED BY spaapp_ingrediente.id_ingrediente;


--
-- Name: spaapp_menu; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_menu (
    id_menu integer NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_menu OWNER TO spauser;

--
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_menu_id_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_menu_id_menu_seq OWNER TO spauser;

--
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_menu_id_menu_seq OWNED BY spaapp_menu.id_menu;


--
-- Name: spaapp_modificardetallepedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_modificardetallepedido (
    id_modificardetallepedido integer NOT NULL,
    id_ingrediente_quitar_id integer,
    id_detallepedido_id integer
);


ALTER TABLE spaapp_modificardetallepedido OWNER TO spauser;

--
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_modificardetallepedido_id_modificardetallepedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_modificardetallepedido_id_modificardetallepedido_seq OWNER TO spauser;

--
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_modificardetallepedido_id_modificardetallepedido_seq OWNED BY spaapp_modificardetallepedido.id_modificardetallepedido;


--
-- Name: spaapp_pedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_pedido (
    id_pedido integer NOT NULL,
    fecha timestamp with time zone NOT NULL,
    observaciones character varying(250),
    valor double precision NOT NULL,
    tiempo integer,
    id_direccion_id integer,
    id_sucursal_id integer,
    id_tipo_pago_id integer,
    id_tipo_pedido_id integer,
    id_trasportista_id integer,
    usuario_id integer,
    id_estado_id integer,
    notificado boolean NOT NULL
);


ALTER TABLE spaapp_pedido OWNER TO spauser;

--
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_pedido_id_pedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_pedido_id_pedido_seq OWNER TO spauser;

--
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_pedido_id_pedido_seq OWNED BY spaapp_pedido.id_pedido;


--
-- Name: spaapp_perfil; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_perfil (
    id_perfil integer NOT NULL,
    cedula character varying(10),
    fecha_nacimiento date,
    telefono character varying(10),
    genero character varying(10),
    ruta_foto character varying(100) NOT NULL,
    sim_dispositivo character varying(30),
    id_usuario_id integer NOT NULL
);


ALTER TABLE spaapp_perfil OWNER TO spauser;

--
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_perfil_id_perfil_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_perfil_id_perfil_seq OWNER TO spauser;

--
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_perfil_id_perfil_seq OWNED BY spaapp_perfil.id_perfil;


--
-- Name: spaapp_plato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_plato (
    id_plato integer NOT NULL,
    nombre_producto character varying(200) NOT NULL,
    ruta_foto character varying(100) NOT NULL,
    descripcion character varying(200) NOT NULL,
    cantidad_diaria integer NOT NULL,
    venta_aproximada integer NOT NULL,
    hora_mayor_venta time without time zone NOT NULL,
    cantidad_mayor_venta integer NOT NULL,
    existencia_actual integer,
    id_platocategoria_id integer NOT NULL,
    activo boolean NOT NULL
);


ALTER TABLE spaapp_plato OWNER TO spauser;

--
-- Name: spaapp_plato_id_menu; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_plato_id_menu (
    id integer NOT NULL,
    plato_id integer NOT NULL,
    menu_id integer NOT NULL
);


ALTER TABLE spaapp_plato_id_menu OWNER TO spauser;

--
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_plato_id_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_plato_id_menu_id_seq OWNER TO spauser;

--
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_plato_id_menu_id_seq OWNED BY spaapp_plato_id_menu.id;


--
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_plato_id_plato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_plato_id_plato_seq OWNER TO spauser;

--
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_plato_id_plato_seq OWNED BY spaapp_plato.id_plato;


--
-- Name: spaapp_platocategoria; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_platocategoria (
    id_platocategoria integer NOT NULL,
    categoria character varying(50) NOT NULL
);


ALTER TABLE spaapp_platocategoria OWNER TO spauser;

--
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_platocategoria_id_platocategoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_platocategoria_id_platocategoria_seq OWNER TO spauser;

--
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_platocategoria_id_platocategoria_seq OWNED BY spaapp_platocategoria.id_platocategoria;


--
-- Name: spaapp_platoingrediente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_platoingrediente (
    id_plato_ingrediente integer NOT NULL,
    requerido boolean NOT NULL,
    horafecha timestamp with time zone NOT NULL,
    id_ingrediente_id integer NOT NULL,
    id_plato_id integer NOT NULL
);


ALTER TABLE spaapp_platoingrediente OWNER TO spauser;

--
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_platoingrediente_id_plato_ingrediente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_platoingrediente_id_plato_ingrediente_seq OWNER TO spauser;

--
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_platoingrediente_id_plato_ingrediente_seq OWNED BY spaapp_platoingrediente.id_plato_ingrediente;


--
-- Name: spaapp_proveedor; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_proveedor (
    id_proveedor integer NOT NULL,
    nombre_comercial character varying(100) NOT NULL,
    telefono character varying(10) NOT NULL,
    telefono_contacto character varying(10),
    prioridad integer NOT NULL,
    me_gusta integer,
    razon_social character varying(20) NOT NULL,
    id_actividad_id integer NOT NULL,
    id_categoria_id integer NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_proveedor OWNER TO spauser;

--
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_proveedor_id_proveedor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_proveedor_id_proveedor_seq OWNER TO spauser;

--
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_proveedor_id_proveedor_seq OWNED BY spaapp_proveedor.id_proveedor;


--
-- Name: spaapp_redsocial; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_redsocial (
    id_red_social integer NOT NULL,
    nombre_red_social character varying(50) NOT NULL,
    cuenta_persona character varying(100) NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_redsocial OWNER TO spauser;

--
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_redsocial_id_red_social_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_redsocial_id_red_social_seq OWNER TO spauser;

--
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_redsocial_id_red_social_seq OWNED BY spaapp_redsocial.id_red_social;


--
-- Name: spaapp_sucursal; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_sucursal (
    id_sucursal integer NOT NULL,
    nombre_sucursal character varying(100) NOT NULL,
    razon_social character varying(200) NOT NULL,
    telefono character varying(10) NOT NULL,
    telefono_contacto character varying(10),
    dias_atencion character varying(80) NOT NULL,
    hora_atencion character varying(50) NOT NULL,
    estado integer,
    prioridad integer,
    activo boolean NOT NULL,
    hora_pico time without time zone NOT NULL,
    me_gusta integer NOT NULL,
    id_ciudad_id integer NOT NULL,
    id_direccion_id integer,
    id_menu_id integer NOT NULL,
    id_proveedor_id integer NOT NULL,
    descripcion character varying(300),
    precio_envio double precision
);


ALTER TABLE spaapp_sucursal OWNER TO spauser;

--
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_sucursal_id_sucursal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_sucursal_id_sucursal_seq OWNER TO spauser;

--
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_sucursal_id_sucursal_seq OWNED BY spaapp_sucursal.id_sucursal;


--
-- Name: spaapp_tamanio; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tamanio (
    id_tamanio integer NOT NULL,
    tamanio character varying(50) NOT NULL
);


ALTER TABLE spaapp_tamanio OWNER TO spauser;

--
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tamanio_id_tamanio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tamanio_id_tamanio_seq OWNER TO spauser;

--
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tamanio_id_tamanio_seq OWNED BY spaapp_tamanio.id_tamanio;


--
-- Name: spaapp_tamanioplato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tamanioplato (
    id_tamanioplato integer NOT NULL,
    valor double precision NOT NULL,
    id_plato_id integer NOT NULL,
    id_tamanio_id integer NOT NULL,
    activo boolean NOT NULL
);


ALTER TABLE spaapp_tamanioplato OWNER TO spauser;

--
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tamanioplato_id_tamanioplato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tamanioplato_id_tamanioplato_seq OWNER TO spauser;

--
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tamanioplato_id_tamanioplato_seq OWNED BY spaapp_tamanioplato.id_tamanioplato;


--
-- Name: spaapp_tipopago; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tipopago (
    id_tipo_pago integer NOT NULL,
    tipo_pago character varying(50) NOT NULL,
    observaciones character varying(250) NOT NULL
);


ALTER TABLE spaapp_tipopago OWNER TO spauser;

--
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tipopago_id_tipo_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tipopago_id_tipo_pago_seq OWNER TO spauser;

--
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tipopago_id_tipo_pago_seq OWNED BY spaapp_tipopago.id_tipo_pago;


--
-- Name: spaapp_tipopedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tipopedido (
    id_tipo_pedido integer NOT NULL,
    tipo_pedido character varying(50) NOT NULL,
    observaciones character varying(250) NOT NULL
);


ALTER TABLE spaapp_tipopedido OWNER TO spauser;

--
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tipopedido_id_tipo_pedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tipopedido_id_tipo_pedido_seq OWNER TO spauser;

--
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tipopedido_id_tipo_pedido_seq OWNED BY spaapp_tipopedido.id_tipo_pedido;


--
-- Name: spaapp_transportista; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_transportista (
    id_transportista integer NOT NULL,
    tipo_transporte character varying(20) NOT NULL,
    tipo_servicio character varying(50) NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_transportista OWNER TO spauser;

--
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_transportista_id_transportista_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_transportista_id_transportista_seq OWNER TO spauser;

--
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_transportista_id_transportista_seq OWNED BY spaapp_transportista.id_transportista;


--
-- Name: spaapp_valoracion; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_valoracion (
    id_valoracion integer NOT NULL,
    valor_transportista integer NOT NULL,
    observaciones character varying(200) NOT NULL,
    valor_proveedor integer NOT NULL,
    valor_servicio integer NOT NULL,
    valoracion_general integer NOT NULL,
    id_cliente_id integer NOT NULL,
    id_pedido_id integer NOT NULL,
    id_proveedor_id integer NOT NULL,
    id_sucursal_id integer NOT NULL,
    id_transportista_id integer NOT NULL
);


ALTER TABLE spaapp_valoracion OWNER TO spauser;

--
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_valoracion_id_valoracion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_valoracion_id_valoracion_seq OWNER TO spauser;

--
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_valoracion_id_valoracion_seq OWNED BY spaapp_valoracion.id_valoracion;


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY "administracionAPP_contacto" ALTER COLUMN id SET DEFAULT nextval('"administracionAPP_contacto_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_association ALTER COLUMN id SET DEFAULT nextval('social_auth_association_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code ALTER COLUMN id SET DEFAULT nextval('social_auth_code_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce ALTER COLUMN id SET DEFAULT nextval('social_auth_nonce_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth ALTER COLUMN id SET DEFAULT nextval('social_auth_usersocialauth_id_seq'::regclass);


--
-- Name: id_actividad; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_actividadcomercial ALTER COLUMN id_actividad SET DEFAULT nextval('spaapp_actividadcomercial_id_actividad_seq'::regclass);


--
-- Name: id_estado; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_catalogoestado ALTER COLUMN id_estado SET DEFAULT nextval('spaapp_catalogoestado_id_estado_seq'::regclass);


--
-- Name: id_categoria; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_categoria ALTER COLUMN id_categoria SET DEFAULT nextval('spaapp_categoria_id_categoria_seq'::regclass);


--
-- Name: id_ciudad; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ciudad ALTER COLUMN id_ciudad SET DEFAULT nextval('spaapp_ciudad_id_ciudad_seq'::regclass);


--
-- Name: id_cliente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente ALTER COLUMN id_cliente SET DEFAULT nextval('spaapp_cliente_id_cliente_seq'::regclass);


--
-- Name: id_contrato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato ALTER COLUMN id_contrato SET DEFAULT nextval('spaapp_contrato_id_contrato_seq'::regclass);


--
-- Name: id_coordenadas; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas ALTER COLUMN id_coordenadas SET DEFAULT nextval('spaapp_coordenadas_id_coordenadas_seq'::regclass);


--
-- Name: id_detalle; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido ALTER COLUMN id_detalle SET DEFAULT nextval('spaapp_detallepedido_id_detalle_seq'::regclass);


--
-- Name: id_direccion; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion ALTER COLUMN id_direccion SET DEFAULT nextval('spaapp_direccion_id_direccion_seq'::regclass);


--
-- Name: id_horario; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista ALTER COLUMN id_horario SET DEFAULT nextval('spaapp_horariotransportista_id_horario_seq'::regclass);


--
-- Name: id_ingrediente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente ALTER COLUMN id_ingrediente SET DEFAULT nextval('spaapp_ingrediente_id_ingrediente_seq'::regclass);


--
-- Name: id_menu; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu ALTER COLUMN id_menu SET DEFAULT nextval('spaapp_menu_id_menu_seq'::regclass);


--
-- Name: id_modificardetallepedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido ALTER COLUMN id_modificardetallepedido SET DEFAULT nextval('spaapp_modificardetallepedido_id_modificardetallepedido_seq'::regclass);


--
-- Name: id_pedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido ALTER COLUMN id_pedido SET DEFAULT nextval('spaapp_pedido_id_pedido_seq'::regclass);


--
-- Name: id_perfil; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil ALTER COLUMN id_perfil SET DEFAULT nextval('spaapp_perfil_id_perfil_seq'::regclass);


--
-- Name: id_plato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato ALTER COLUMN id_plato SET DEFAULT nextval('spaapp_plato_id_plato_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu ALTER COLUMN id SET DEFAULT nextval('spaapp_plato_id_menu_id_seq'::regclass);


--
-- Name: id_platocategoria; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platocategoria ALTER COLUMN id_platocategoria SET DEFAULT nextval('spaapp_platocategoria_id_platocategoria_seq'::regclass);


--
-- Name: id_plato_ingrediente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente ALTER COLUMN id_plato_ingrediente SET DEFAULT nextval('spaapp_platoingrediente_id_plato_ingrediente_seq'::regclass);


--
-- Name: id_proveedor; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor ALTER COLUMN id_proveedor SET DEFAULT nextval('spaapp_proveedor_id_proveedor_seq'::regclass);


--
-- Name: id_red_social; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial ALTER COLUMN id_red_social SET DEFAULT nextval('spaapp_redsocial_id_red_social_seq'::regclass);


--
-- Name: id_sucursal; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal ALTER COLUMN id_sucursal SET DEFAULT nextval('spaapp_sucursal_id_sucursal_seq'::regclass);


--
-- Name: id_tamanio; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanio ALTER COLUMN id_tamanio SET DEFAULT nextval('spaapp_tamanio_id_tamanio_seq'::regclass);


--
-- Name: id_tamanioplato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato ALTER COLUMN id_tamanioplato SET DEFAULT nextval('spaapp_tamanioplato_id_tamanioplato_seq'::regclass);


--
-- Name: id_tipo_pago; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopago ALTER COLUMN id_tipo_pago SET DEFAULT nextval('spaapp_tipopago_id_tipo_pago_seq'::regclass);


--
-- Name: id_tipo_pedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopedido ALTER COLUMN id_tipo_pedido SET DEFAULT nextval('spaapp_tipopedido_id_tipo_pedido_seq'::regclass);


--
-- Name: id_transportista; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista ALTER COLUMN id_transportista SET DEFAULT nextval('spaapp_transportista_id_transportista_seq'::regclass);


--
-- Name: id_valoracion; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion ALTER COLUMN id_valoracion SET DEFAULT nextval('spaapp_valoracion_id_valoracion_seq'::regclass);


--
-- Data for Name: administracionAPP_contacto; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY "administracionAPP_contacto" (id, nombre_propietario, apellidos, ciudad, nombre_empresa, direccion, tipo_negocio, telefono) FROM stdin;
\.


--
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('"administracionAPP_contacto_id_seq"', 1, false);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_group (id, name) FROM stdin;
2	Proveedor
3	Transportista
1	Cliente
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_id_seq', 3, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	116
2	1	117
3	1	120
4	2	116
5	2	115
6	2	117
7	2	118
8	2	119
9	2	122
10	3	115
11	3	118
12	3	119
13	3	121
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 14, true);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	2	add_logentry
2	Can change log entry	2	change_logentry
3	Can delete log entry	2	delete_logentry
4	Can add permission	3	add_permission
5	Can change permission	3	change_permission
6	Can delete permission	3	delete_permission
7	Can add group	4	add_group
8	Can change group	4	change_group
9	Can delete group	4	delete_group
10	Can add user	5	add_user
11	Can change user	5	change_user
12	Can delete user	5	delete_user
13	Can add content type	6	add_contenttype
14	Can change content type	6	change_contenttype
15	Can delete content type	6	delete_contenttype
16	Can add session	7	add_session
17	Can change session	7	change_session
18	Can delete session	7	delete_session
19	Can add Actividad Comercial	8	add_actividadcomercial
20	Can change Actividad Comercial	8	change_actividadcomercial
21	Can delete Actividad Comercial	8	delete_actividadcomercial
22	Can add perfil	9	add_perfil
23	Can change perfil	9	change_perfil
24	Can delete perfil	9	delete_perfil
25	Can add categoria	10	add_categoria
26	Can change categoria	10	change_categoria
27	Can delete categoria	10	delete_categoria
28	Can add proveedor	11	add_proveedor
29	Can change proveedor	11	change_proveedor
30	Can delete proveedor	11	delete_proveedor
31	Can add ciudad	12	add_ciudad
32	Can change ciudad	12	change_ciudad
33	Can delete ciudad	12	delete_ciudad
34	Can add direccion	13	add_direccion
35	Can change direccion	13	change_direccion
36	Can delete direccion	13	delete_direccion
37	Can add cliente	14	add_cliente
38	Can change cliente	14	change_cliente
39	Can delete cliente	14	delete_cliente
40	Can add menu	15	add_menu
41	Can change menu	15	change_menu
42	Can delete menu	15	delete_menu
43	Can add sucursal	16	add_sucursal
44	Can change sucursal	16	change_sucursal
45	Can delete sucursal	16	delete_sucursal
46	Can add contrato	17	add_contrato
47	Can change contrato	17	change_contrato
48	Can delete contrato	17	delete_contrato
49	Can add transportista	18	add_transportista
50	Can change transportista	18	change_transportista
51	Can delete transportista	18	delete_transportista
52	Can add Coordenada	19	add_coordenadas
53	Can change Coordenada	19	change_coordenadas
54	Can delete Coordenada	19	delete_coordenadas
55	Can add tipo pago	20	add_tipopago
56	Can change tipo pago	20	change_tipopago
57	Can delete tipo pago	20	delete_tipopago
58	Can add tipo pedido	21	add_tipopedido
59	Can change tipo pedido	21	change_tipopedido
60	Can delete tipo pedido	21	delete_tipopedido
61	Can add pedido	22	add_pedido
62	Can change pedido	22	change_pedido
63	Can delete pedido	22	delete_pedido
64	Can add plato categoria	23	add_platocategoria
65	Can change plato categoria	23	change_platocategoria
66	Can delete plato categoria	23	delete_platocategoria
67	Can add plato	24	add_plato
68	Can change plato	24	change_plato
69	Can delete plato	24	delete_plato
70	Can add tamanio	25	add_tamanio
71	Can change tamanio	25	change_tamanio
72	Can delete tamanio	25	delete_tamanio
73	Can add tamanio plato	26	add_tamanioplato
74	Can change tamanio plato	26	change_tamanioplato
75	Can delete tamanio plato	26	delete_tamanioplato
76	Can add Detalle de pedido	27	add_detallepedido
77	Can change Detalle de pedido	27	change_detallepedido
78	Can delete Detalle de pedido	27	delete_detallepedido
79	Can add ingrediente	28	add_ingrediente
80	Can change ingrediente	28	change_ingrediente
81	Can delete ingrediente	28	delete_ingrediente
82	Can add Horario de transportista	29	add_horariotransportista
83	Can change Horario de transportista	29	change_horariotransportista
84	Can delete Horario de transportista	29	delete_horariotransportista
85	Can add modificardetallepedido	30	add_modificardetallepedido
86	Can change modificardetallepedido	30	change_modificardetallepedido
87	Can delete modificardetallepedido	30	delete_modificardetallepedido
88	Can add plato ingrediente	31	add_platoingrediente
89	Can change plato ingrediente	31	change_platoingrediente
90	Can delete plato ingrediente	31	delete_platoingrediente
91	Can add Red social	32	add_redsocial
92	Can change Red social	32	change_redsocial
93	Can delete Red social	32	delete_redsocial
94	Can add valoracion	33	add_valoracion
95	Can change valoracion	33	change_valoracion
96	Can delete valoracion	33	delete_valoracion
97	Can add contacto	34	add_contacto
98	Can change contacto	34	change_contacto
99	Can delete contacto	34	delete_contacto
100	Can add cors model	35	add_corsmodel
101	Can change cors model	35	change_corsmodel
102	Can delete cors model	35	delete_corsmodel
103	Can add user social auth	36	add_usersocialauth
104	Can change user social auth	36	change_usersocialauth
105	Can delete user social auth	36	delete_usersocialauth
106	Can add nonce	37	add_nonce
107	Can change nonce	37	change_nonce
108	Can delete nonce	37	delete_nonce
109	Can add association	38	add_association
110	Can change association	38	change_association
111	Can delete association	38	delete_association
112	Can add code	39	add_code
113	Can change code	39	change_code
114	Can delete code	39	delete_code
115	Read Profile Client Restricted	9	rpcr
116	Read Profile Transportista Acceso Total	9	rpta
117	Read Transportista Restricted	18	rtr
118	Read Client Restricted	14	rrc
119	Read Direccion Cliente Restricted	13	rdcr
120	Es Cliente	14	esCliente
121	Es Transportista	18	esTransportista
122	Es Proveedor	11	esProveedor
123	Can add catalogo estado	40	add_catalogoestado
124	Can change catalogo estado	40	change_catalogoestado
125	Can delete catalogo estado	40	delete_catalogoestado
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_permission_id_seq', 125, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
12	pbkdf2_sha256$24000$HtuXmxTeRqyL$Njr1uHceH9EZxGJiFe4gXIkGCJJbwCuqkh5M1yYPwJQ=	2016-08-05 09:50:32.752293-05	f	mileiva	Milton	Leiva	mileiva@utpl.edu.ec	f	t	2016-08-01 14:33:19.44345-05
8	pbkdf2_sha256$24000$qNZ7WHHyykAy$b2mSftD201hNXb7dyy+usYfMa4NDyAbC4NA8UwYgH9Y=	\N	f	chamelion				f	t	2016-07-28 20:11:47.163683-05
9	pbkdf2_sha256$24000$ey7AkLLlNEzT$TtYnVywsZikZa1yFunbtK8q2xwXJ+yJLCWXR3ooS5IE=	\N	f	papalogo				f	t	2016-07-28 20:13:21.318461-05
10	pbkdf2_sha256$24000$6bgVcVaYAXwS$NU+zXJH/GgDBbl2eWk0clCxHnqyM/IsVng1kGmyqiO4=	\N	f	tonys				f	t	2016-07-28 20:14:12.452188-05
29	pbkdf2_sha256$24000$rWQhXSVXEqUT$3jpAuJqbaHY9LeMWeSck5Xpwxw9fIv3w6HjLiiNfkws=	\N	f	posix	posix	posix	p@g.com	f	t	2016-08-08 11:24:41.153563-05
20	pbkdf2_sha256$24000$8tUyY5ocvtPb$6znCQT7l8mjDlh6lMs3iD50NraNTEYQ6NlpbFlmh/dg=	\N	f	hola1	hola	hola	hola@gmail.com	f	t	2016-08-05 19:52:01.007523-05
14	pbkdf2_sha256$24000$bdfVfg5iZZ9h$+fr24IGm0bACx/2yE0HqDS8W2TUoi/gLNeuk9MIXuf4=	2016-08-01 18:48:42.009257-05	f	hola	Hola	Hola	aaaa@hhh.com	f	t	2016-08-01 18:48:35.863843-05
40	pbkdf2_sha256$24000$eOm7DjXmgPYw$OrH6LjYQkEJo0GAKVV9pQAzSkJE3oanSQ2h1OHJ8Mh4=	\N	f	ggggg	ggggg	ggg	byjose007@gmailk.com	f	t	2016-08-09 16:39:52.722905-05
32	pbkdf2_sha256$24000$LaYbtKRE4P65$yLVi3uaj2ms5M4uhq1aFgaehGO1MKZawJqQuxSD1XRs=	2016-08-08 12:43:57.56415-05	f	pameliz	pameliz	pameliz	pam@gmail.com	f	t	2016-08-08 12:37:23.399844-05
6	pbkdf2_sha256$24000$0j2DG22e2YHH$fo/V5vWPWU3iIhWUaQbJp6KoUAHlH9ethCCPyD7N7fg=	2016-07-28 20:44:49-05	f	chino				f	t	2016-07-28 20:08:58-05
11	pbkdf2_sha256$24000$HrliZ4y5yUNX$fvKgnoXbuOxep2DR9fE6qicpX0UJLn3D/KONvwgjFvk=	2016-08-11 17:43:50.313018-05	f	cliente1				f	t	2016-07-28 20:46:07.635535-05
13	pbkdf2_sha256$24000$Sz60GFfE1kLG$zH+oz3wOIk5vxngO0xey67vJUU0pPD0nYQUp1g4WnCY=	2016-08-09 15:24:26.1701-05	f	nadia	Nadia	aaaa	aaaa@hhh.com	f	t	2016-08-01 17:28:47.462339-05
41	pbkdf2_sha256$24000$l6JJUJQbiFht$tHibNZFFB+BFkCNdICFE8T/qAVn1la8za62urt/Pq1I=	\N	f	dickson	1234	á	a@b.com	f	t	2016-08-09 19:15:59.095716-05
45	pbkdf2_sha256$24000$4ph6pyXiCrVq$Lxk6rWRH0654OjCAapN8CG57YjFzcs0FWd093MGQ5aQ=	\N	f	hfh	hdhd	hshs	hs@vdv.es	f	t	2016-08-09 19:51:32.811464-05
16	pbkdf2_sha256$24000$7lmMQlLRKqBQ$SThCWqPoo2ib/S0qlzjhqFmkUJyo+i6WsJWWtj5+AXE=	2016-08-02 17:37:56.387539-05	f	pppp	PPPP	pPPP	aaaa@hhh.com	f	t	2016-08-01 18:59:59.33102-05
42	pbkdf2_sha256$24000$vLAIcswLW1fo$TgdujnhcrX/XPF2Z1kPRA58VgNfHP9JIp7duyLoJQz8=	\N	f	dickson2	123456	12345	dk@armijos.es	f	t	2016-08-09 19:23:18.809872-05
22	pbkdf2_sha256$24000$fdoln7v3p3bP$D38T5HjBaxf5MvSvbpR1AbYUQjxjp93km/gKCG0oxQc=	\N	f	armijos	armijos	armijos	a@gmail.com	f	t	2016-08-08 10:50:45.620835-05
23	pbkdf2_sha256$24000$x4K27HieSbIh$sFomLx85rsa3PfT0KrnyvWkU0iVRThXO3l3xFzwgQn8=	\N	f	lola	lola	lola	lola@gmail.com	f	t	2016-08-08 10:52:35.311279-05
17	pbkdf2_sha256$24000$DAf1nJvKiWCU$zPIhLzMZCbL/52Z19+Xd268H0gxhzhrJql2Yj5yZ104=	2016-08-04 18:13:43.618189-05	f	nadia2	nnnnn	ppppp	aaaa@hhh.com	f	t	2016-08-02 16:25:32.907172-05
24	pbkdf2_sha256$24000$YzyCSIGo4tBx$nFiqksbSwNhADVVyTcMFhRF1rHgZPvP636Sy9GgoLjc=	\N	f	n	n	n	ne@gmail.com	f	t	2016-08-08 10:53:29.428634-05
33	pbkdf2_sha256$24000$qRL4JaadYyuz$jlvcSlYhwPyaQFkLjRtAMnYuUOAy74ISYygZjquDkd0=	2016-08-08 13:23:02.567125-05	f	prueba	prueba	prueba	prueba@g.com	f	t	2016-08-08 13:22:57.470733-05
35	pbkdf2_sha256$24000$IXj7etw1ymAT$3qabLqp5xjZyRAzxgFOJIIuLpbmKTUyawOOclD5WDoA=	2016-08-09 06:53:43.881826-05	f	pamm	pam	pam	p@g.com	f	t	2016-08-09 05:54:02.815123-05
18	pbkdf2_sha256$24000$WrOICHZliOuI$CO/04wIW2F7rgpBDggBQarA7iJwSIxqMYaQpwB9qvEY=	2016-08-03 17:33:18.90328-05	f	nadia3	aaaaa	bbbbb	aaaa@hhh.com	f	t	2016-08-02 18:42:59.561961-05
27	pbkdf2_sha256$24000$D1LoQwU5rAnh$Q7TzJlxWCbdNTvXMCtJAke73gGtGfxJOn+dAsucmVHQ=	\N	f	lik	lik	lik	lik@gm.com	f	t	2016-08-08 11:12:57.590545-05
28	pbkdf2_sha256$24000$dVQdyT5holv8$dW9bat1mwUjjHS4aqu9tUqEOSgagLdT5qj9mv+l+pWc=	\N	f	lim	lim	lim	lim@g.com	f	t	2016-08-08 11:13:51.257095-05
47	pbkdf2_sha256$24000$nm3Uz6fJlo1s$uLkw8KZXzOJSO6pird2uqnLlcBfN0XROOhyyYkA5l1Y=	2016-08-11 10:07:48.26449-05	f	clienete2	test	test	test@sds.com	f	t	2016-08-10 13:39:43.598523-05
30	pbkdf2_sha256$24000$f8ilTqq8jFyM$IymYONPv/IGCfmi5WOIZCR/O5hIr5PGwScPS+DuYkRs=	2016-08-09 05:15:08.683906-05	f	nich	nic	nic	nic@gmail.com	f	t	2016-08-08 11:30:51.859126-05
39	pbkdf2_sha256$24000$AH0NDEBr5YOP$0C52aH+LQTUBvpCZtltiq4ttUH7ZOJ5sCUoDYzJqSnU=	\N	f	byjose007			byjose007@gmail.com	f	t	2016-08-09 16:34:04.336207-05
43	pbkdf2_sha256$24000$j5lgTTFcK6Zs$KPpLm7wUkw0DX7zG6R2wcedBRxId5tvH6gxljIH/Izk=	2016-08-09 19:32:14.658534-05	f	dickson4	dickson	armijos	darmijos@cid.com	f	t	2016-08-09 19:32:04.80755-05
46	pbkdf2_sha256$24000$XTM2ANtKeOJ9$ggptbuv/HwKT/k+skv+TZAlSBvy9zR3zbY/+HGJAFHQ=	\N	f	jiplaza	cheo	plaza	jiplaza@hotmail.com	f	t	2016-08-10 10:53:59.921709-05
44	pbkdf2_sha256$24000$GnDI7xJh3AvU$3j6CZHtiWbze5pJRax2hCH5PYc+PYKYYgfCM+mMlksQ=	\N	f	:Carlos	huds	hsus	lala@lala.com	f	t	2016-08-09 19:44:30.674755-05
48	pbkdf2_sha256$24000$5rrscuA7H6Xx$mAa5n5eRRe9mFNALSnDw+De1Wwgcx/PBKZoyiml3dPI=	\N	f	fsfds	fsdfs	fsdfs	admin@gmal.com	f	t	2016-08-10 13:56:30.11389-05
34	pbkdf2_sha256$24000$EEoyJFCTc35v$4yonVwLoOl2bMffGVIBrMi7WdCPMavNE3UrU0/vt0ho=	2016-08-10 12:49:58.637184-05	f	pam	pam	pam	pam@g.com	f	t	2016-08-08 13:47:18.679485-05
49	pbkdf2_sha256$24000$ECZLlIV3CBFY$tyaEGp1c14a8x7talemqFUhY+iLd96dEbwwjNdkM51A=	\N	f	jiplaza123	julio	julio	jiplaza123@gmail.com	f	t	2016-08-10 14:15:05.821013-05
50	pbkdf2_sha256$24000$g2O02VcFzgzy$ge+D8ISpcV8nKJGpgwJa193vxEZ6LFXYSSPnNjDy86M=	\N	f	usuario	ggfg	ggfg	usaurio@g.com	f	t	2016-08-10 14:16:56.515237-05
51	pbkdf2_sha256$24000$DlXBalScBeN5$xkHOObPYobCG43/DDrjwgKNxTDVbikjhBEQX6LXDGPI=	\N	f	usuario100	usuario	usuario	usuario100@gmail.com	f	t	2016-08-10 14:20:50.565852-05
52	pbkdf2_sha256$24000$JQsCPwoMigxa$UN+TUp1A4c7WCw86L5Q+Jjw9zYQtaYX/FxKIXPm1ZGM=	2016-08-10 14:23:00.398317-05	f	otro	otro	otro	otro@g.com	f	t	2016-08-10 14:23:00.225131-05
53	pbkdf2_sha256$24000$6Q0sQ1aAgsUj$mHHyAU3cY38zdUt0aBaDpyamPDTYPNhsM8Ip95QZBMA=	2016-08-10 14:26:40.038601-05	f	lili90	lili	lili	lili90@gmial.com	f	t	2016-08-10 14:26:39.878526-05
54	pbkdf2_sha256$24000$YYQIee4Cn5LH$pYpRA+scdS60DuoGhI76HMLgP9LnpqYFW9zWl78uMS4=	2016-08-10 16:22:07.788335-05	f	John	John	Ramirez	john@gmail.com	f	t	2016-08-10 16:21:50.189036-05
56	pbkdf2_sha256$24000$1abfbcTbPOaN$mRS0jHdIMvzkHgQuWq3mxT8kK4z04pjvIT1+qrruGr8=	\N	f	fruques2				f	t	2016-08-10 18:35:39.383756-05
57	pbkdf2_sha256$24000$QinbjW8Hl45T$QQe5i6ZzJviAx07ls8SGze3MIU/Rj43aNkn0yGY6ywA=	2016-08-10 18:35:37.735577-05	f	joselo	josel	joselo	jo@gmail.com	f	t	2016-08-10 18:35:26.383901-05
58	pbkdf2_sha256$24000$sQCrglD5MIya$/NXgFDVXfFSJ+ouzRRtIUmNqrCNJtTbZkMBTrMCEJpQ=	2016-08-10 18:46:15.316144-05	f	hhhh	hhhh	hhhh	hhh@gmail.com	f	t	2016-08-10 18:46:15.09987-05
5	pbkdf2_sha256$24000$tbf3Jp4deRty$zN4o4r9lhHZk6SEN/t4iC4DnnNwg/UkREFlFqrO4ZSc=	2016-08-24 11:51:21.995271-05	f	chinito				f	t	2016-07-28 20:07:12-05
15	pbkdf2_sha256$24000$cdzVFc3c83jc$YcXa4t0l8dEbmtdQ9D8jinLRihs+qcSEM80BhBlQQpo=	2016-08-25 10:11:48.927006-05	f	cliente2	Cliente	Cliente	aaaa@hhh.com	f	t	2016-08-01 18:53:25-05
4	pbkdf2_sha256$24000$QHY0JgIxGO9D$wuYMTVHeL9UK9xtD2JbVBlWbLK54clvqJ3Obv4Iz4Is=	2016-08-24 17:15:25.568313-05	f	pizza				f	t	2016-07-28 20:05:40-05
7	pbkdf2_sha256$24000$Y2i7bGayjd5J$VeG6xu3ek/0efjYyfU2RTGJugYU6rC+dOSTHzSQpReo=	\N	f	icecreamsocial				f	t	2016-07-28 20:11:05-05
55	pbkdf2_sha256$24000$2ypgkkhYXxxY$jp02ldBZKDXz7jEa8gTV0k0RJR9pFSaiplOaCn8d484=	2016-08-18 16:36:20.086203-05	f	fruques				f	t	2016-08-10 18:35:16.025117-05
3	pbkdf2_sha256$24000$KqApxeOZFuHb$j6umz0UhXih1KqOfFr2BFm3XiPWfeRKsZ/KPE3rNarU=	2016-08-24 12:20:30.78706-05	f	transportista				f	t	2016-07-28 19:59:20-05
61	pbkdf2_sha256$24000$wT8KRWyOoFQ8$Me/1U4w54Yu3ImCI8BMj+EJDbrp8LChnJfqxKTVpgng=	2016-08-11 12:02:07.011236-05	f	fdfdfdas	gdgdf	gdgdf	fdf@dd.com	f	t	2016-08-11 12:02:06.750506-05
62	pbkdf2_sha256$24000$akP8z4yyn4Zh$SlW3NVuE1vb5UkRj6UcD3bgJFrDYTiRNg6XZ2bMzd+s=	\N	f	gusy2				f	t	2016-08-11 14:05:00-05
64	pbkdf2_sha256$24000$JsKK6QbPPJLi$2zc3pzkVGewVl5kZn8xQpgn4B6M1DTnhsYAxpGmFQ4s=	2016-08-11 16:13:44.685119-05	f	rosi	rosi	rosi	rosi@gmail.com	f	t	2016-08-11 16:13:31.550396-05
65	pbkdf2_sha256$24000$GKJ8bIMqcOpE$y+7f75xx3xLwBfMG0RxcruW7slTy0KfkUYLl8FPD54k=	\N	f	roch	roch	roch	ro@g.com	f	t	2016-08-11 16:23:58.503125-05
66	pbkdf2_sha256$24000$yKS3s9YMXd0X$Rhmvk8P9FvBCGPk4J8rYfZKBIhy/wtbOKnoEUSsQ8e8=	2016-08-11 16:24:30.246739-05	f	patch	patch	patch	p@g.com	f	t	2016-08-11 16:24:21.562072-05
67	pbkdf2_sha256$24000$BiqNQiNbd52W$I2kKmg5sXbB7gUgUneXvnDTYB54C9Nr+QGRr80g8fIg=	\N	f	pfguaman	pfguaman	pfguaman	pfguaman95@gmail.com	f	t	2016-08-11 16:28:59.908207-05
69	pbkdf2_sha256$24000$Nt5i51EwJk79$jZP8VL6LKCm/CckwOd/3fmsV5tB+d/zzxBNIa1e6TFE=	\N	f	pamelixx	pam	gu	alex@gmail.com	f	t	2016-08-11 17:28:56.902664-05
36	pbkdf2_sha256$24000$YCwFQVUB2gl5$eT7TGNW6XdwtEBgKaJnXFfQV4Itrx9aIrXleFPw2f5I=	2016-08-11 17:59:13.644364-05	f	alex	Alex	Guaman	alex@gmail.com	f	t	2016-08-09 07:19:20.199137-05
70	pbkdf2_sha256$24000$294T1AdjiXX1$fPlN6Wcxbj/6t1t4yP/k4He1PiZFShbbFpYS0DmtBVo=	\N	f	pamellix	pam	gu	g@gm.com	f	t	2016-08-11 18:05:37.296967-05
71	pbkdf2_sha256$24000$6QzuAg8lkEtT$xz8IZSpDh7PcTWWq0O/JzvK5v8FJB3coZXSwSSoOwSY=	\N	f	pal	@po	kl	kl@gm.com	f	t	2016-08-11 18:06:13.507026-05
72	pbkdf2_sha256$24000$XHhRYrUJezW2$d0eOXaWLkJ86ES12Y4dEvXMvsRuXFrXnbijhoviuHWk=	2016-08-11 18:10:38.707912-05	f	nicolll	po	po	pamefer.95@gmail.com	f	t	2016-08-11 18:10:29.285309-05
86	pbkdf2_sha256$24000$efubwaak6f24$iMT2hLv+ryot4VPPjOgHYe9bNBbqvCcGmgGipRW6sVo=	\N	f	ncspqilr	aidxeqvw	uihjixga	sample@email.tst	f	t	2016-08-19 18:24:23.81667-05
73	pbkdf2_sha256$24000$xHIBoj6CCvdX$D4CBVWoRfmfajivBrlmi1mnp93Zr2kv3vefB+sJQ3HE=	2016-08-16 12:52:36.37046-05	f	Nicho	nicholas	earley	ad@gmail.com	f	t	2016-08-15 11:44:17.280138-05
82	pbkdf2_sha256$24000$ihEgs6oRddSn$pOWmUMnHpjK8ihj4L/ziGAsjZvN43S8kv6tkonrjrPs=	2016-08-18 18:15:46.531217-05	f	nyx	Neptune	Nishedcob	nishedcob@gmail.com	f	t	2016-08-18 18:15:34.251616-05
2	pbkdf2_sha256$24000$VZBS5yhJk5U5$GhXFXCKGJ+XpgWULSIxDvbeUO259YcxyOtBtJaA38nc=	2016-08-24 19:54:13.644586-05	t	admin			bsbnadiapc@hotmail.com	t	t	2016-07-28 19:56:26.739528-05
74	pbkdf2_sha256$24000$9uLxnAok7kjT$68lQqTPiW5wA2o30argLRPt9XrodMINjTjlBZFVPflM=	\N	f	pamelixxx	pame	guaman	pa@gmail.co	f	t	2016-08-17 10:47:06.197692-05
68	pbkdf2_sha256$24000$VQ4PjZ1MfcSb$b/fAadFAv86RCQk+vAxxVmP28PTLG21uYgpIiSd6wJM=	2016-08-17 10:47:13.216147-05	f	pamelix	pame	guaman	pab@g.com	f	t	2016-08-11 17:11:43.549756-05
79	pbkdf2_sha256$24000$64aGSdHIuqlc$3oGRmQZLsgYuELcAf1Fhah4UEaALG7Qv65GcRjZ1/3g=	2016-08-17 18:26:10.841691-05	f	lovi	lovi	lovi	lovi@gm.com	f	t	2016-08-17 18:26:02.337403-05
19	pbkdf2_sha256$24000$pfB7o1M68V2N$w6wC7s6FnZAel49w/8iiaCuf5QD7z+WDkIm1sOvMsVA=	2016-08-17 18:30:45.986614-05	f	cliente3	nico	git	nico@git.com	f	t	2016-08-03 15:13:07.92167-05
75	pbkdf2_sha256$24000$8zpeq9sXWiPl$g1oBSEo9xjferxyrR/vPb84Uj/tl5wZfoA4EfuXGyZQ=	2016-08-17 11:16:33.258914-05	f	loxad	loxad	loxad	l@gmail.com	f	t	2016-08-17 11:09:23.00789-05
59	pbkdf2_sha256$24000$SxK81uzqt4jw$Vt/f6qFiX0ZF+tkckIMsMBofauRjtCS0Bnc4N+HmqBQ=	2016-08-17 19:15:26.276446-05	f	rommac	Rommel	Macas	rommelmacas@gmail.com	f	t	2016-08-10 19:00:10.588523-05
76	pbkdf2_sha256$24000$QUQD9DJ4VhPP$HP3bs127PH61O3z1z+Mh7tB3qB6aTH9fzGzaoBAZeMA=	2016-08-17 11:32:44.875637-05	f	pfguaman2	Pamela	Pamela	pama@gmail.co	f	t	2016-08-17 11:31:36.002937-05
83	pbkdf2_sha256$24000$6AHGsW3CcZx5$eHx21Zw+Vq20GFW85Z6YrVmwHM7ycghojiibj3QVdog=	\N	f	robin	robon	lopez	pame@g.com	f	t	2016-08-19 10:00:16.917548-05
80	pbkdf2_sha256$24000$UYxLZsKmyuCA$VZh+7frC57dKWcSzBiSrToIoA0/gP4Jdgg7HblhGQ+I=	2016-08-18 08:59:26.087132-05	f	cliente4	milton	milton	slaass@gmail.com	f	t	2016-08-18 08:27:30.724531-05
87	pbkdf2_sha256$24000$U9xV6zmdeiqK$pULo/nmlRTdvlGYQhMyxqbs/crp566qEK6EMcZ8WHnw=	\N	f	gkxbgomd	gkxbgomd	gkxbgomd	sample@email.tst	f	t	2016-08-19 18:28:50.871374-05
77	pbkdf2_sha256$24000$6rQuh1WTIWYe$rWRsORTBgKn2warGLuqpLUYH5zKs09VK0oWEPtViWUs=	2016-08-17 12:04:43.808969-05	f	rommel	rommel	macas	romel@g.com	f	t	2016-08-17 11:53:30.810066-05
84	pbkdf2_sha256$24000$rzira7tjBU2u$7Np+1/Cyj610rqVMcA3FqXPbq5jFK6JuZLO7fMOEoRc=	2016-08-19 10:01:38.843224-05	f	gaux	gaux	gaux	gaux@g.com	f	t	2016-08-19 10:01:26.795141-05
88	pbkdf2_sha256$24000$JIoeG2s3U64f$d+546AUkxJirX1+IhIKbisP0XD67VvsiBvgtgeJ9NZA=	\N	f	eovayehp	WWY3OGFqVnc=	eovayehp	sample@email.tst	f	t	2016-08-19 18:28:51.221046-05
85	pbkdf2_sha256$24000$S8FYiBphMqdr$VpVNT5i0Yu59BjfnnbBHmmImilPuOi67VA7f42p/PUo=	2016-08-19 11:27:46.420762-05	f	nyxx	Neptune Nyx	Nishedcob	nyx@nishedcob.net	f	t	2016-08-19 11:27:30.952565-05
81	pbkdf2_sha256$24000$KgcWmm3xRm56$ovT4ISFKpiLbueSpcXZjC2fQmk3/Fx2jcp/BiibI5x4=	2016-08-18 16:33:04.023229-05	f	milton13	milton	leiva	slaifermilton@gmail.com	f	t	2016-08-18 15:15:10.662832-05
89	pbkdf2_sha256$24000$HkeJUpYwCncV$mLB+7WLxX9MrRW/ZAAEvbP6SXxqpnxQZZglcID9mkPE=	\N	f	bawrlfkj	bawrlfkj&n957491=v979777	bawrlfkj	sample@email.tst	f	t	2016-08-19 18:28:51.303108-05
97	pbkdf2_sha256$24000$uDFOF1QHgDm0$ZgZFRjyLMrLZQzOIEHshefapKlkNpofyaHykJo8bzdM=	2016-08-23 18:54:52.252495-05	f	set|set&set	buawirjc	buawirjc	sample@email.tst	f	t	2016-08-19 18:28:53.366401-05
90	pbkdf2_sha256$24000$52IPVByXb0f2$GXDzco7xjKajAA4ExXe5JFFA0Uceddi53r2lYB4Po0o=	\N	f	hhnbxytj	kBgnwTpi	hhnbxytj	sample@email.tst	f	t	2016-08-19 18:28:51.375812-05
91	pbkdf2_sha256$24000$LcMOefvKFX4R$xM9LLNmB+vRu+Fj4TFCf5KUsNCTHKvZjtWS5kUUk8FA=	\N	f	kaofppfb	kaofppfb	kaofppfb&n928080=v982551	sample@email.tst	f	t	2016-08-19 18:28:51.514778-05
92	pbkdf2_sha256$24000$ZaC1UL5epaSI$faYnUrPbHuevF3iglqb0Si1899zpekUqsSQg40za7uA=	\N	f	dgyirymx&n986049=v969706	dgyirymx	dgyirymx	sample@email.tst	f	t	2016-08-19 18:28:51.779095-05
93	pbkdf2_sha256$24000$SPcCaldbiXtq$ZozMNgG8M5M28NK/uqDqZZ6w2B05EF5srVTP5aXnJwc=	\N	f	OVZodDMzQUE=	eovayehp	eovayehp	sample@email.tst	f	t	2016-08-19 18:28:52.029548-05
94	pbkdf2_sha256$24000$LYefqC6alEYe$Lm/8oqW4FvviYvs8HjJ2GQkPg7ynbXswWgZ5YDj1X60=	\N	f	${10000066+9999839}	ghlkrfhy	ghlkrfhy	sample@email.tst	f	t	2016-08-19 18:28:52.096344-05
95	pbkdf2_sha256$24000$oJomC4tv8GyK$uGdsdfLkzWVBZcSOHQzYUDilFQSAQO0aupIjWLrEpA8=	\N	f	gJUH3mRR	hhnbxytj	hhnbxytj	sample@email.tst	f	t	2016-08-19 18:28:52.343609-05
96	pbkdf2_sha256$24000$oN2FOateotNY$YMVTzxIwsDU58WVO5H8BMpYUQghkv1mW4tHbRTHTr6o=	\N	f	buawirjc	set|set&set	buawirjc	sample@email.tst	f	t	2016-08-19 18:28:52.422948-05
63	pbkdf2_sha256$24000$ps0kXBqbRyF9$alEIcmLTh1D/GXxNNl/IPioEdPKrU+RyPZB0xLJVan0=	\N	f	gusy				f	t	2016-08-11 14:09:37-05
98	pbkdf2_sha256$24000$Nh2pH0Mg4vy9$sVvLY+qnQgkBx1rBI4CKTSGxzj2E5C4uD3h1+7gTtIY=	\N	f	$(nslookup 03KVASjK)	buawirjc	buawirjc	sample@email.tst	f	t	2016-08-19 18:28:53.49986-05
99	pbkdf2_sha256$24000$vqtT0siTwj7R$8RaH5/MX3XK3JPk9OwNr7FjFviyhikBIsl5RBdoqx5M=	\N	f	sjmdajdu	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:28:53.585275-05
60	pbkdf2_sha256$24000$sJv3oB5slz05$tk5qVPLrkIWIiua9cMEAskdlWpM5UH1MSIj5sNAyxb4=	2016-08-23 20:30:49.367097-05	f	geantonxx	Jorge Antonio	Jorge Antonio	geantonxx@gmail.com	f	t	2016-08-10 19:02:25.13738-05
78	pbkdf2_sha256$24000$bDNkU0Mow9FG$fIJDR801nLaJnKQI0JCyWoq/7uv6VFOyttYk2SFGBgo=	2016-08-22 21:10:29.188035-05	f	rosax	rosa	guaman	rosi@gmail.com	f	t	2016-08-17 17:59:08.032466-05
100	pbkdf2_sha256$24000$o8S2YiDBHHmV$Av0BaDAX4eAPUGnmglbTw7XgCwy7mC0/NAewU5/WKsA=	\N	f	xjqcbpid	testasp.vulnweb.com	xjqcbpid	sample@email.tst	f	t	2016-08-19 18:28:53.616665-05
101	pbkdf2_sha256$24000$t9joGhwZoOqR$rabx46FezWyxzHm6Xguu2ZXexIiRT74w59+y6PxNfPU=	\N	f		uibkcjoy	uibkcjoy	sample@email.tst	f	t	2016-08-19 18:28:53.67564-05
118	pbkdf2_sha256$24000$ueCVtJiK8Z6W$DXMCnBzAwjaA8FztfjpMywUMKQC8wL2m43G1f9rXvXE=	2016-08-23 18:54:56.427746-05	f	"	rlyrdqvb	rlyrdqvb	sample@email.tst	f	t	2016-08-19 18:28:57.18425-05
102	pbkdf2_sha256$24000$lnUCGkfHneTz$ijOR+7QBOSO95C3q9dFKLZZXO6FQwSGmmaEr7JENIbE=	2016-08-23 18:54:55.602683-05	f	'"()	uibkcjoy	uibkcjoy	sample@email.tst	f	t	2016-08-19 18:28:53.809709-05
117	pbkdf2_sha256$24000$T9pKobcdUJ0X$rQTO71dQyNTh340h2gDNnnSf3VJWRPppCikFVXqRcd8=	2016-08-23 18:54:56.319432-05	f	'	rlyrdqvb	rlyrdqvb	sample@email.tst	f	t	2016-08-19 18:28:56.979918-05
105	pbkdf2_sha256$24000$J0kVasQbBvE3$ecectQ2db+yO7b/y+RFYNtwNQnwppsitN62t0LGK85k=	\N	f	rdjgoedh	rdjgoedh	rdjgoedh	sample@email.tst	f	t	2016-08-19 18:28:54.127138-05
119	pbkdf2_sha256$24000$jwJetKeBgtz9$xv2+z6MSA/znp5vVxQwheeowpzGK6QMNLOT+s11ic6w=	2016-08-23 18:54:56.986178-05	f	/www.vulnweb.com	camtxkbx	camtxkbx	sample@email.tst	f	t	2016-08-19 18:28:58.223253-05
107	pbkdf2_sha256$24000$5TABatxWSILg$q80FJIjlrB22nzI2Pb9Mf0WMX7DSJqdUE4N75EJwmC8=	\N	f	wniogsox	wniogsox	wniogsox	sample@email.tst	f	t	2016-08-19 18:28:54.891113-05
108	pbkdf2_sha256$24000$jxWLzpVznKRM$8SG6Z0Ossac7aCLwIKGIQy2YQsc0WZw1fTykqKvqMik=	\N	f	wkmufjla	http://hitjCgsU4opEK.bxss.me/	wkmufjla	sample@email.tst	f	t	2016-08-19 18:28:54.892995-05
104	pbkdf2_sha256$24000$ZLbu39SW4aRT$e1m6DakbcSkkZph3OYHRFYzJB4q6K7IosF0ub+ciNjg=	2016-08-23 18:54:55.444519-05	f	!(()&&!|*|*|	gybcihhf	gybcihhf	sample@email.tst	f	t	2016-08-19 18:28:54.047039-05
110	pbkdf2_sha256$24000$7tcbAnxPAn7p$bSbyOD62LD1HJxUG+qE6qtHQADq1nDN/vqAL4B3nCaE=	\N	f	camtxkbx	http://www.vulnweb.com	camtxkbx	sample@email.tst	f	t	2016-08-19 18:28:55.46476-05
111	pbkdf2_sha256$24000$bcNE4ccT4oIQ$bBiFFT1biEQiZG5cRwMQv2lTURrCXH8BKGddFu3H60Y=	\N	f	http://hitcWug1nkJbN.bxss.me/	wkmufjla	wkmufjla	sample@email.tst	f	t	2016-08-19 18:28:55.850271-05
113	pbkdf2_sha256$24000$wPiOIljnrW5F$kPFJs5gnPtrfQF+3YrGDz5vJ0IdeBlcyyUA+opfJNXU=	2016-08-23 18:54:57.269433-05	f	'"	whpbufyu	whpbufyu	sample@email.tst	f	t	2016-08-19 18:28:56.424221-05
124	pbkdf2_sha256$24000$kfK2gVsksqha$C2bZqQh6iOmOVWv1wCizwSMLIJvaS0GLK3IfnEjqBek=	2016-08-23 18:54:57.444574-05	f	\\	wniogsox	wniogsox	sample@email.tst	f	t	2016-08-19 18:29:01.726408-05
114	pbkdf2_sha256$24000$8Ih9xeikmAov$nnEPwL1RNx6cUTSjlS+PfQcrW3kF5p8s9o4GR0eVX+g=	2016-08-23 18:54:57.394435-05	f	<!--	whpbufyu	whpbufyu	sample@email.tst	f	t	2016-08-19 18:28:56.599541-05
115	pbkdf2_sha256$24000$SB1gwsGCRdz5$xNUPsKMPAUbwsoZj+pi7HuI7G9hyEAIT8uQ0rtV7ZGg=	\N	f	registro	rdjgoedh	rdjgoedh	sample@email.tst	f	t	2016-08-19 18:28:56.606277-05
116	pbkdf2_sha256$24000$lyDaT4rbmnt7$IKWLcsh+PdS0zhltuVVDuNguBhbhSk2x24aPxZ5yFVk=	\N	f	registro/.	rdjgoedh	rdjgoedh	sample@email.tst	f	t	2016-08-19 18:28:56.907763-05
123	pbkdf2_sha256$24000$XrWQ8fHhbQGg$KWIbPUbLk/ltoAzrTns+eREcVmtNO3ncHsaFi/50EeQ=	2016-08-23 18:54:57.319826-05	f	1'"	wniogsox	wniogsox	sample@email.tst	f	t	2016-08-19 18:29:01.595891-05
125	pbkdf2_sha256$24000$HPUzqyWhWGjb$5FNk27dWnRP66g9HIGbn/lWl+rXclM1LangGQpmSJb0=	2016-08-23 18:54:57.56121-05	f	1	wniogsox	wniogsox	sample@email.tst	f	t	2016-08-19 18:29:01.85232-05
127	pbkdf2_sha256$24000$1nWzxQM6eZkz$hGexmMan/ak3qgQ9ODw6//SPtA1wHXhO2m4bCMM8lCk=	2016-08-23 18:54:57.786564-05	f	JyI=	wniogsox	wniogsox	sample@email.tst	f	t	2016-08-19 18:29:02.158683-05
120	pbkdf2_sha256$24000$44TyGjfUR8R5$0pxwXdEwUg4eyueSqVHHWGofZpICBH0ezrNgqWqd6m4=	\N	f	vdiktgxm	rgnjfwyi9390872	vdiktgxm	sample@email.tst	f	t	2016-08-19 18:28:59.710501-05
121	pbkdf2_sha256$24000$G7DM3IeF0ba1$imILbp5DuVz92QAvOFSXHgpPCjGTfOrQlKqgUSJ+HGQ=	\N	f	efneohai	efneohai	vdiktgxm9993714	sample@email.tst	f	t	2016-08-19 18:29:00.378766-05
122	pbkdf2_sha256$24000$LL6829aSKvQo$ljGkFSnlI3RyKGl+16ImQIwv6hW3o1lwmQ2cVMTzcgs=	\N	f	wdcuhupl9687522	pwrcxxar	pwrcxxar	sample@email.tst	f	t	2016-08-19 18:29:01.49272-05
128	pbkdf2_sha256$24000$haXFPbZB2AhA$0LnqcgG4mx9P3uADLQgNcMheej294bZu1osE2xlKaqs=	2016-08-23 18:54:57.928044-05	f	�'�"	wniogsox	wniogsox	sample@email.tst	f	t	2016-08-19 18:29:02.318359-05
129	pbkdf2_sha256$24000$f2B7K7EhpzCo$LPlRrMsBf05EaGluWhx/S2daIZf3Qmo0v7LFVs89BaQ=	2016-08-23 18:54:58.053499-05	f	�''�""	wniogsox	wniogsox	sample@email.tst	f	t	2016-08-19 18:29:02.467705-05
142	pbkdf2_sha256$24000$39HXtJFUunRW$9hFVqOl2w/A7nV+IxKp154YJHT0ollwGLPDrhfUQk1U=	2016-08-23 18:54:58.278591-05	f	/etc/passwd	pxykygen	pxykygen	sample@email.tst	f	t	2016-08-19 18:29:26.008662-05
126	pbkdf2_sha256$24000$J6XCHtjGh2GJ$vYb6qed6eDRvZk3uqUvHBppk2imueQ1xFwv/HkkNRiM=	\N	f	@@dZrEN	wniogsox	wniogsox	sample@email.tst	f	t	2016-08-19 18:29:02.015061-05
109	pbkdf2_sha256$24000$eF3QbIGQJAMD$KEKUpAP6fpGyEqBon/1f2yaUH80/OTln9X4miJnkR+A=	2016-08-23 18:54:53.735709-05	f	12345'"\\'\\")	gkxbgomd	gkxbgomd	sample@email.tst	f	t	2016-08-19 18:28:55.364539-05
130	pbkdf2_sha256$24000$05L6v4YBuSOX$k4u5FUXxkMVi4RbWouFxutdNVOqASWggQweKJonpbPk=	\N	f	pxykygen	file:///etc/passwd	pxykygen	sample@email.tst	f	t	2016-08-19 18:29:07.165706-05
131	pbkdf2_sha256$24000$6igl2XGlFOTX$81JXnDVyjLVi7JdBIqHkW9XR7rq/HTfq31ma7f3C/ks=	\N	f	ISMraND0	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:08.49284-05
132	pbkdf2_sha256$24000$y9p6HruKwQRs$1F9SraaiI+kLNDl7t7/j0Lc2h2l6A3yx0pdRmUDaYoM=	\N	f	-1 OR 2+975-975-1=0+0+0+1 --	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:09.239274-05
133	pbkdf2_sha256$24000$CXEnohZZmKFT$JHQygspWEhzbEFPsxmicKhq8rHmwclfLuFA50ollIHE=	\N	f	-1 OR 3+975-975-1=0+0+0+1 --	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:09.431298-05
134	pbkdf2_sha256$24000$GcrWGBdMWNCV$4ZBuksuDj7P692zvgLTW7COfqC2TIJYRsUtpf+mt6ow=	\N	f	-1 OR 2+527-527-1=0+0+0+1	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:09.596733-05
135	pbkdf2_sha256$24000$U9zSC0pwSpLt$BToBD/XkBL4twATuq3G2VUhhIuZ624S3xfP8l0237wk=	\N	f	-1 OR 3+527-527-1=0+0+0+1	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:10.091783-05
103	pbkdf2_sha256$24000$38kxK6L9yXoZ$3guUmX1xHebLDEXnhSdtTtSxRCUXISlTxJzwJ1ObyF8=	2016-08-23 18:54:55.194128-05	f	)	gybcihhf	gybcihhf	sample@email.tst	f	t	2016-08-19 18:28:53.905579-05
137	pbkdf2_sha256$24000$eiwkZLqBASWO$ZrFNAw67ZWxMs8CPCRhXGqWgS4W8O6f6TqzR1IveN3o=	\N	f	NdWjL6Dd'	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:10.740738-05
138	pbkdf2_sha256$24000$7Mr9Xg5FVGuy$mZw5gRR5H2BgIZDWAmvtRqqvBmv4Y1o2VIRRmdXNQ10=	\N	f	rwbws4Hi'	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:10.913074-05
139	pbkdf2_sha256$24000$D2tJQMGI8o7q$jTjBic7Pgf2hP9MiB5zmPTFRbAnG/U0NcYOv8ZwCoOc=	\N	f	plPOtzEH')	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:11.131912-05
140	pbkdf2_sha256$24000$54SCy91RzXBe$QOplCXDb+q5Qh684JXMUcwioFJ3EJ2egtqWyrkx/dhk=	\N	f	1agHhZc2'))	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:11.304969-05
141	pbkdf2_sha256$24000$Pt0thhnrSNFG$Oxlfqa01oeUT0d9AQPOHJ4GVG+Te2EIRB6puDavFL04=	\N	f	543'	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:11.500995-05
136	pbkdf2_sha256$24000$KdZXVSu3f6o6$NSzwFC3U34EayD1tGjwPg1ywyvk1dhnbigP9jJM5uNg=	2016-08-23 18:54:54.635782-05	f	1 waitfor delay '0:0:9' --	sjmdajdu	sjmdajdu	sample@email.tst	f	t	2016-08-19 18:29:10.59473-05
151	pbkdf2_sha256$24000$Oe2gOveWjpGr$IBygI6XGk7Mbrg2zLI/6e2nkslraWhk05DbcMuLT/qo=	2016-08-23 20:25:49.381515-05	f	edison_fabricio	Edison	Chuquimarca	edisonfabricioch@gmail.com	f	t	2016-08-23 20:25:39.635744-05
152	pbkdf2_sha256$24000$Snvz3UrjDMuR$CX3oUlOZ6FRk7jHwPTVRGQC8ekIcpfrrumshK2A1ns0=	\N	f	dickson3	dickson	armijos	ddsd@gmail.com	f	t	2016-08-24 08:59:49.853134-05
153	pbkdf2_sha256$24000$f4F4qI0aaavp$4fFgQVIS0pbr6HNHWh+D8bpr0t47lR79GAhQmmPz62Y=	2016-08-24 09:00:14.911135-05	f	dickson5	dickson	armijos	ddsd@gmail.com	f	t	2016-08-24 09:00:05.812387-05
154	pbkdf2_sha256$24000$AjWn8g7fbdIE$MmnY3jvJOw0sT4c8hXKgGdf+4fjSQRn8rysKaN1GAvY=	2016-08-24 11:36:54.323654-05	f	pamefer	pamelita	pamelita	pa@h.com	f	t	2016-08-24 11:36:54.18237-05
155	pbkdf2_sha256$24000$iiMIZ4U30cVd$iYqyD9tChnTeFrCQDhSPeAyuJj0AXcWzMnbLf6Mk88E=	2016-08-24 11:47:48.682137-05	f	julio	Julio	Plaza	jiplaza@utpl.edu.ec	f	t	2016-08-24 11:47:34.954323-05
149	pbkdf2_sha256$24000$9UfL3cSwN5Sr$vuj4vRhLhiPZa9WMEu1+6/pAB7RrX/SAU4KSLt9Bmz4=	\N	f	1acuTYybzL2x5q	pvugikkf	pvugikkf	sample@email.tst	f	t	2016-08-19 19:11:56.909-05
156	pbkdf2_sha256$24000$zpU5ZPecbuYe$2cvvYzBpI0BOV+IBHmqvzRau70z4++mueCMBSNYBUqo=	2016-08-24 19:43:28.314535-05	f	locochavez				f	t	2016-08-24 19:25:17-05
150	pbkdf2_sha256$24000$ebgT2vD3iEVT$JzeUctBoQ+CdwA8cH11eU570xgg9HlF08raec4fT3c8=	2016-08-23 18:07:33.20603-05	f	cosmesin	rossa	guaman	rosa@g.com	f	t	2016-08-23 17:54:45.298105-05
112	pbkdf2_sha256$24000$cWDKNqDS6N62$uIJHeLiWv+m7ZGpjCrpFUzQpFkAAIbBo/Ny8hK86ib8=	2016-08-23 18:54:55.027233-05	f	testasp.vulnweb.com	xjqcbpid	xjqcbpid	sample@email.tst	f	t	2016-08-19 18:28:55.872648-05
106	pbkdf2_sha256$24000$47SWqqACicPu$rsDwgEfqGR9tGQ0zlBZBtO1YihZEcDN0m59TQ25aKb0=	2016-08-23 18:54:55.611371-05	f	^(#$!@#$)(()))******	gybcihhf	gybcihhf	sample@email.tst	f	t	2016-08-19 18:28:54.237467-05
143	pbkdf2_sha256$24000$khfCYSKSEqrb$7l39Urdj9TqinxmMi/wu3tOGZnmY0QoxxxKDakrHnrk=	2016-08-23 18:54:58.419832-05	f	%2fetc%2fpasswd	pxykygen	pxykygen	sample@email.tst	f	t	2016-08-19 18:29:26.203318-05
144	pbkdf2_sha256$24000$n5Pl8yegO82s$hEqTQwvVCAmvTGeZcFdHKskv71ftrmjpIJQaraX0fug=	2016-08-23 18:54:59.069821-05	f	file:///etc/passwd	pxykygen	pxykygen	sample@email.tst	f	t	2016-08-19 18:29:27.300482-05
157	pbkdf2_sha256$24000$MeIjSyxMbNf1$hIiw7vusAYiI4f5vayXDltzWe+mzAhFJz5OyLHFkIdo=	2016-08-24 20:03:04.269326-05	f	soyard				f	t	2016-08-24 19:54:48-05
146	pbkdf2_sha256$24000$OxOFJfNatsE3$0ZezXkTmc029cnHv1Mh0OAuX8lzedXaf3Orw/PDd990=	2016-08-23 18:54:59.470147-05	f	/WEB-INF/web.xml	pxykygen	pxykygen	sample@email.tst	f	t	2016-08-19 18:29:27.916617-05
148	pbkdf2_sha256$24000$xewxjPoORZSD$jnhGT5LQAveeB2tz7jg2XgkpqRBS1eJgJ3BzO65/wa4=	2016-08-23 18:55:00.30362-05	f	................windowswin.ini	pxykygen	pxykygen	sample@email.tst	f	t	2016-08-19 18:29:29.108451-05
145	pbkdf2_sha256$24000$syMhR9WJ3BlK$L65u1+WWnkwpBMU5fzpvgi7GkwRTlgaRNMy9WB5ud0E=	2016-08-23 18:55:00.878256-05	f	WEB-INF/web.xml	pxykygen	pxykygen	sample@email.tst	f	t	2016-08-19 18:29:27.700568-05
147	pbkdf2_sha256$24000$7eho9vcfV2rY$RVKX5V0zXz9aA06fRPozbAM124XQW3wKBz2sKhmzmb0=	2016-08-23 18:55:01.020111-05	f	WEB-INF\\web.xml	pxykygen	pxykygen	sample@email.tst	f	t	2016-08-19 18:29:28.118876-05
158	pbkdf2_sha256$24000$4NVmtD8T6EjC$LsAqPVIf6waw7iLzP6VNT92jcIsigEb5aMg0vmGJNrM=	2016-08-25 07:50:07.995574-05	f	pamefer95	Pamelix	pamefer95	pan@gmail.com	f	t	2016-08-25 07:48:54.818783-05
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
2	29	1
3	30	1
4	32	1
5	33	1
6	34	1
7	15	1
8	35	1
9	4	2
10	3	3
11	6	2
12	5	2
13	36	1
14	41	1
15	42	1
16	43	1
17	44	1
18	45	1
19	47	1
20	54	1
21	57	1
22	59	1
23	62	2
24	64	1
25	65	1
26	66	1
27	67	1
28	68	1
29	69	1
30	70	1
31	71	1
32	72	1
33	73	1
34	74	1
35	75	1
36	76	1
37	77	1
38	78	1
39	79	1
40	80	1
41	81	1
42	82	1
43	83	1
44	84	1
45	85	1
46	86	1
47	87	1
48	88	1
49	89	1
50	90	1
51	91	1
52	92	1
53	93	1
54	94	1
55	95	1
56	96	1
57	97	1
58	98	1
59	99	1
60	100	1
61	101	1
62	102	1
63	103	1
64	104	1
65	105	1
66	106	1
67	107	1
68	108	1
69	109	1
70	110	1
71	111	1
72	112	1
73	113	1
74	114	1
75	115	1
76	116	1
77	117	1
78	118	1
79	119	1
80	120	1
81	121	1
82	122	1
83	123	1
84	124	1
85	125	1
86	126	1
87	127	1
88	128	1
89	129	1
90	130	1
91	131	1
92	132	1
93	133	1
94	134	1
95	135	1
96	136	1
97	137	1
98	138	1
99	139	1
100	140	1
101	141	1
102	142	1
103	143	1
104	144	1
105	145	1
106	146	1
107	147	1
108	148	1
109	149	1
110	63	2
111	150	1
112	151	1
113	152	1
114	153	1
115	154	1
116	155	1
117	156	2
118	157	2
119	158	1
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 119, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_id_seq', 158, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2016-07-28 19:59:20.390682-05	3	transportista	1	Añadido.	5	2
2	2016-07-28 19:59:23.662164-05	1	1	1	Añadido.	9	2
3	2016-07-28 19:59:25.000849-05	1	1	1	Añadido.	18	2
4	2016-07-28 20:01:23.543102-05	1	Loja	1	Añadido.	12	2
5	2016-07-28 20:01:23.231307-05	2	Loja	1	Añadido.	12	2
6	2016-07-28 20:05:40.805126-05	4	pizza	1	Añadido.	5	2
7	2016-07-28 20:05:44.135955-05	2	pizza	1	Añadido.	9	2
8	2016-07-28 20:05:45.592826-05	1	Guaranda - Tulcan	1	Añadido.	13	2
9	2016-07-28 20:06:16.423005-05	1	Restaurante	1	Añadido.	8	2
10	2016-07-28 20:06:27.333526-05	1	Restaurante	1	Añadido.	10	2
11	2016-07-28 20:06:39.26334-05	1	Pizza	2	Modificado/a categoria.	10	2
12	2016-07-28 20:06:58.370523-05	1	Pizza	1	Añadido.	11	2
13	2016-07-28 20:07:03.652279-05	1	Pizza	1	Añadido.	15	2
14	2016-07-28 20:07:19.515262-05	1	Pizza	1	Añadido.	16	2
15	2016-07-28 20:07:12.08977-05	5	chinito	1	Añadido.	5	2
16	2016-07-28 20:07:16.552552-05	3	chinito	1	Añadido.	9	2
17	2016-07-28 20:07:21.834778-05	2	sucre - miguel riofrío	1	Añadido.	13	2
18	2016-07-28 20:08:00.869683-05	2	Chinito	1	Añadido.	11	2
19	2016-07-28 20:08:09.773635-05	2	Chinito	1	Añadido.	15	2
20	2016-07-28 20:08:18.043651-05	2	Chinito	1	Añadido.	16	2
21	2016-07-28 20:08:58.307718-05	6	chino	1	Añadido.	5	2
22	2016-07-28 20:09:00.811683-05	4	chino	1	Añadido.	9	2
23	2016-07-28 20:09:03.001269-05	3	Vince - Ambato	1	Añadido.	13	2
24	2016-07-28 20:09:25.161411-05	2	Comida Asiática	1	Añadido.	10	2
25	2016-07-28 20:09:59.543746-05	3	Casa China	1	Añadido.	11	2
26	2016-07-28 20:10:07.490487-05	3	Casa China	1	Añadido.	15	2
27	2016-07-28 20:10:13.315091-05	3	Casa china	1	Añadido.	16	2
28	2016-07-28 20:09:54.330835-05	3	Heladería	1	Añadido.	10	2
29	2016-07-28 20:11:05.998379-05	7	icecreamsocial	1	Añadido.	5	2
30	2016-07-28 20:11:47.204706-05	8	chamelion	1	Añadido.	5	2
31	2016-07-28 20:11:11.395078-05	5	icecreamsocial	1	Añadido.	9	2
32	2016-07-28 20:11:49.769628-05	6	chamelion	1	Añadido.	9	2
33	2016-07-28 20:11:51.730859-05	4	Latacunga - Bolivar	1	Añadido.	13	2
34	2016-07-28 20:11:15.29552-05	4	Ice Cream Social	1	Añadido.	11	2
35	2016-07-28 20:11:24.471836-05	4	Ice Cream Social	1	Añadido.	15	2
36	2016-07-28 20:11:26.747109-05	4	Ice Cream Social	1	Añadido.	16	2
37	2016-07-28 20:12:11.435858-05	4	Comida Casera	1	Añadido.	10	2
38	2016-07-28 20:12:55.152681-05	5	Chamelion	1	Añadido.	11	2
39	2016-07-28 20:13:01.261927-05	5	Chamelion	1	Añadido.	15	2
40	2016-07-28 20:13:02.643552-05	5	Chamelion	1	Añadido.	16	2
41	2016-07-28 20:12:45.508447-05	5	Comida rápida	1	Añadido.	10	2
42	2016-07-28 20:13:21.348026-05	9	papalogo	1	Añadido.	5	2
43	2016-07-28 20:13:24.568677-05	7	papalogo	1	Añadido.	9	2
44	2016-07-28 20:13:28.421589-05	6	PapaLogo	1	Añadido.	11	2
45	2016-07-28 20:14:12.495246-05	10	tonys	1	Añadido.	5	2
46	2016-07-28 20:14:16.354245-05	8	tonys	1	Añadido.	9	2
47	2016-07-28 20:14:22.382596-05	7	Tony's	1	Añadido.	11	2
48	2016-07-28 20:14:28.926592-05	6	Tony's	1	Añadido.	15	2
49	2016-07-28 20:14:30.19485-05	6	Tony's	1	Añadido.	16	2
50	2016-07-28 20:13:55.29901-05	7	PapaLogo	1	Añadido.	15	2
51	2016-07-28 20:13:57.573875-05	7	PapaLogo	1	Añadido.	16	2
52	2016-07-28 20:19:15.815955-05	1	Helados	1	Añadido.	23	2
53	2016-07-28 20:19:18.781995-05	1	Helado napolitano	1	Añadido.	24	2
54	2016-07-28 20:19:43.678705-05	2	Helado de chocolate	1	Añadido.	24	2
55	2016-07-28 20:21:43.90168-05	2	Pizza	1	Añadido.	23	2
56	2016-07-28 20:21:59.823934-05	3	Hawaiana	1	Añadido.	24	2
57	2016-07-28 20:23:00.670329-05	4	Napolitana	1	Añadido.	24	2
58	2016-07-28 20:22:23.470628-05	3	Hamburguesa	1	Añadido.	23	2
59	2016-07-28 20:22:28.21659-05	5	Hamburguesa	1	Añadido.	24	2
60	2016-07-28 20:23:19.996509-05	1	Standard	1	Añadido.	25	2
61	2016-07-28 20:23:32.471793-05	2	Mediana	1	Añadido.	25	2
62	2016-07-28 20:23:37.969029-05	3	Grande	1	Añadido.	25	2
63	2016-07-28 20:23:31.223306-05	4	Bebida	1	Añadido.	23	2
64	2016-07-28 20:24:09.793627-05	1	Helado napolitano : Standard - 1.0	1	Añadido.	26	2
65	2016-07-28 20:23:33.036534-05	6	bebida	1	Añadido.	24	2
66	2016-07-28 20:24:16.638918-05	2	Helado de chocolate : Mediana - 1.5	1	Añadido.	26	2
67	2016-07-28 20:24:24.723157-05	3	Helado de chocolate : Grande - 2.0	1	Añadido.	26	2
68	2016-07-28 20:24:07.1025-05	7	refresco	1	Añadido.	24	2
69	2016-07-28 20:24:49.665641-05	4	Normal	1	Añadido.	25	2
70	2016-07-28 20:24:56.158889-05	4	Hawaiana : Normal - 3.5	1	Añadido.	26	2
71	2016-07-28 20:25:13.03056-05	5	Hawaiana : Grande - 5.5	1	Añadido.	26	2
72	2016-07-28 20:25:21.62963-05	6	Hawaiana : Grande - 12.5	1	Añadido.	26	2
73	2016-07-28 20:25:30.330695-05	7	Napolitana : Mediana - 3.0	1	Añadido.	26	2
74	2016-07-28 20:25:42.471823-05	8	Napolitana : Grande - 12.0	1	Añadido.	26	2
75	2016-07-28 20:25:58.157964-05	9	Hamburguesa : Normal - 2.5	1	Añadido.	26	2
76	2016-07-28 20:26:53.376773-05	10	Hamburguesa : Mediana - 6.0	1	Añadido.	26	2
77	2016-07-28 20:27:05.899275-05	11	bebida : Normal - 5.0	1	Añadido.	26	2
78	2016-07-28 20:27:19.880165-05	12	bebida : Grande - 3.0	1	Añadido.	26	2
79	2016-07-28 20:27:40.220621-05	13	refresco : Grande - 4.0	1	Añadido.	26	2
80	2016-07-28 20:45:24.074742-05	3	chinito	2	No ha cambiado ningún campo.	9	2
81	2016-07-28 20:46:07.67556-05	11	cliente1	1	Añadido.	5	2
82	2016-07-28 20:46:48.647758-05	9	cliente1	1	Añadido.	9	2
83	2016-07-29 10:58:24.935239-05	1	Efectivo	1	Añadido.	20	2
84	2016-07-29 10:58:50.507783-05	2	Dinero Electrónico	1	Añadido.	20	2
85	2016-07-29 11:14:11.749621-05	1	A domicilio	1	Añadido.	21	2
86	2016-07-29 11:23:20.446587-05	2	Para recoger	1	Añadido.	21	2
87	2016-07-29 11:33:13.840393-05	4	Napolitana	2	Modificado/a ruta_foto.	24	2
88	2016-07-29 11:33:33.020604-05	5	Hamburguesa	2	Modificado/a ruta_foto.	24	2
89	2016-07-29 11:33:54.048344-05	6	Coca Cola	2	Modificado/a nombre_producto y ruta_foto.	24	2
90	2016-07-29 11:34:05.715615-05	7	Fanta naranja	2	Modificado/a nombre_producto y ruta_foto.	24	2
91	2016-07-29 11:34:42.278738-05	10	admin	1	Añadido.	9	2
92	2016-08-02 10:12:13.135851-05	1	2016-07-29 15:45:42.091906+00:00 - bueno - 5.0	3		22	2
93	2016-08-02 10:13:59.614327-05	15	2016-08-02 15:02:16+00:00 - pendiente - 3.5	3		22	2
94	2016-08-02 10:13:59.620334-05	14	2016-08-02 15:01:53+00:00 - pendiente - 15.5	3		22	2
95	2016-08-02 10:13:59.623329-05	13	2016-08-01 14:56:05+00:00 - pendiente - 4.5	3		22	2
96	2016-08-02 10:13:59.626329-05	12	2016-06-29 17:09:53+00:00 - pendiente - 16.5	3		22	2
97	2016-08-02 10:13:59.629329-05	11	2016-06-29 17:09:42+00:00 - pendiente - 11.5	3		22	2
98	2016-08-02 10:13:59.633329-05	10	2016-07-29 17:09:02+00:00 - pendiente - 34.5	3		22	2
99	2016-08-02 10:13:59.636329-05	9	2016-07-29 16:25:20+00:00 - confirmado - 8.0	3		22	2
100	2016-08-03 13:03:27.252154-05	42	cliente1	3		9	2
101	2016-08-03 13:03:27.28951-05	41	cliente1	3		9	2
102	2016-08-03 13:03:27.292022-05	9	cliente1	3		9	2
103	2016-08-03 13:03:27.294013-05	0	cliente1	3		9	2
104	2016-08-03 13:14:22.114097-05	43	cliente1	3		9	2
105	2016-08-08 12:55:43.791906-05	49	cliente1	1	Añadido.	9	2
106	2016-08-08 07:58:02.886167-05	21	nicholas	1	Añadido.	5	2
107	2016-08-08 07:58:11.009641-05	21	nicholas	2	Modificado/a groups.	5	2
108	2016-08-08 18:15:26.946411-05	8	Helado oreo	1	Añadido.	24	2
109	2016-08-08 18:16:20.860469-05	14	Helado oreo : Normal - 1.5	1	Añadido.	26	2
110	2016-08-09 10:08:51.017187-05	15	cliente2	2	Modificado/a groups.	5	2
111	2016-08-09 10:27:52.031728-05	1	Cliente	2	Modificado/a permissions.	4	2
112	2016-08-09 10:29:37.790904-05	1	Cliente	2	No ha cambiado ningún campo.	4	2
113	2016-08-09 10:30:12.46179-05	1	Cliente	2	Modificado/a permissions.	4	2
114	2016-08-09 06:01:02.689397-05	4	pizza	2	Modificado/a groups.	5	2
115	2016-08-09 06:25:24.149953-05	3	transportista	2	Modificado/a groups.	5	2
116	2016-08-09 06:50:05.400948-05	6	chino	2	Modificado/a groups.	5	2
117	2016-08-09 06:51:04.49793-05	5	chinito	2	Modificado/a groups.	5	2
118	2016-08-09 14:27:41.198696-05	2	pizza	2	Modificado/a ruta_foto.	9	2
119	2016-08-09 15:43:57.601132-05	10	pollo	2	No ha cambiado ningún campo.	24	2
120	2016-08-09 15:44:39.387203-05	18	pollo : Mediana - 12.0	1	Añadido.	26	2
121	2016-08-09 16:30:18.107677-05	38	bbbbbbb	3		5	2
122	2016-08-09 16:30:18.134171-05	31	byjose007	3		5	2
123	2016-08-09 16:30:18.137675-05	37	ggggg	3		5	2
124	2016-08-09 16:30:18.140745-05	26	ni	3		5	2
125	2016-08-09 16:30:18.144237-05	21	nicholas	3		5	2
126	2016-08-09 16:30:18.148239-05	25	q	3		5	2
127	2016-08-09 16:46:50.928489-05	19	aaaaaaaaa : Mediana - 2.0	1	Añadido.	26	2
128	2016-08-09 16:48:14.967534-05	11	popi	3		24	2
129	2016-08-09 16:48:32.56696-05	20	aaaaaaaaa : Mediana - 12.0	1	Añadido.	26	2
130	2016-08-09 16:48:56.610075-05	20	aaaaaaaaa : Mediana - 12.0	3		26	2
131	2016-08-09 17:43:23.253083-05	14	zanahoria	3		28	2
132	2016-08-09 17:43:23.283525-05	13	Arroz	3		28	2
133	2016-08-09 17:43:23.304724-05	12	Cebolla	3		28	2
134	2016-08-09 17:43:23.31773-05	11	Pollo	3		28	2
135	2016-08-09 17:44:24.115457-05	4	Cebolla	3		28	2
136	2016-08-09 17:44:24.14272-05	2	Pescado	3		28	2
137	2016-08-09 17:44:24.153308-05	1	Arroz	3		28	2
138	2016-08-09 18:27:57.983382-05	5	Marisco	1	Añadido.	23	2
139	2016-08-09 18:28:15.612142-05	6	Pescado	1	Añadido.	23	2
140	2016-08-09 18:29:06.834228-05	7	Pollo	1	Añadido.	23	2
141	2016-08-09 18:29:12.41578-05	8	Carnes	1	Añadido.	23	2
142	2016-08-10 17:30:56.563683-05	116	2016-08-10 22:29:33+00:00 - pendiente - 8.5	3		22	2
143	2016-08-10 17:30:56.617106-05	115	2016-08-10 22:26:37+00:00 - pendiente - 4.5	3		22	2
144	2016-08-10 17:30:56.630524-05	114	2016-08-10 22:24:03+00:00 - pendiente - 12.5	3		22	2
145	2016-08-10 17:30:56.649959-05	113	2016-08-10 22:23:25+00:00 - pendiente - 12.5	3		22	2
146	2016-08-10 17:30:56.659284-05	112	2016-08-10 22:23:22+00:00 - pendiente - 12.5	3		22	2
147	2016-08-10 17:30:56.667474-05	111	2016-08-10 22:22:00+00:00 - pendiente - 12.5	3		22	2
148	2016-08-10 17:30:56.675305-05	110	2016-08-10 22:20:07+00:00 - pendiente - 2.5	3		22	2
149	2016-08-10 17:30:56.707886-05	109	2016-08-10 22:18:28+00:00 - pendiente - 11.5	3		22	2
150	2016-08-10 17:30:56.724567-05	108	2016-08-10 22:16:25+00:00 - pendiente - 59.5	3		22	2
151	2016-08-10 17:30:56.741252-05	107	2016-08-10 22:15:57+00:00 - pendiente - 31.5	3		22	2
152	2016-08-10 17:30:56.758082-05	106	2016-08-10 22:15:15+00:00 - pendiente - 11.5	3		22	2
153	2016-08-10 17:30:56.774884-05	105	2016-08-10 22:14:44+00:00 - pendiente - 21.5	3		22	2
154	2016-08-10 17:30:56.807825-05	104	2016-08-10 22:14:18+00:00 - pendiente - 21.5	3		22	2
155	2016-08-10 17:30:56.835433-05	103	2016-08-10 22:13:18+00:00 - pendiente - 16.5	3		22	2
156	2016-08-10 17:30:56.857968-05	102	2016-08-10 22:12:04+00:00 - pendiente - 41.5	3		22	2
157	2016-08-10 17:30:56.887552-05	101	2016-08-10 22:10:49+00:00 - pendiente - 41.5	3		22	2
158	2016-08-10 17:30:56.895467-05	100	2016-08-10 22:04:13+00:00 - pendiente - 11.5	3		22	2
159	2016-08-10 17:30:56.907666-05	99	2016-08-10 21:51:38+00:00 - pendiente - 29.5	3		22	2
160	2016-08-10 17:30:56.918046-05	98	2016-08-10 21:51:27+00:00 - pendiente - 29.5	3		22	2
161	2016-08-10 17:30:56.932541-05	97	2016-08-10 21:46:55+00:00 - pendiente - 15.5	3		22	2
162	2016-08-10 17:30:56.944276-05	96	2016-08-10 21:44:43+00:00 - pendiente - 21.0	3		22	2
163	2016-08-10 17:30:56.957556-05	95	2016-08-10 21:43:12+00:00 - pendiente - 7.0	3		22	2
164	2016-08-10 17:30:56.966383-05	94	2016-08-10 21:42:30+00:00 - pendiente - 4.5	3		22	2
165	2016-08-10 17:30:56.974936-05	93	2016-08-10 21:41:06+00:00 - pendiente - 4.5	3		22	2
166	2016-08-10 17:30:56.983074-05	92	2016-08-10 21:39:01+00:00 - pendiente - 4.5	3		22	2
167	2016-08-10 17:30:56.995683-05	91	2016-08-10 21:38:41+00:00 - pendiente - 15.5	3		22	2
168	2016-08-10 17:30:57.009319-05	90	2016-08-10 21:38:41+00:00 - pendiente - 15.5	3		22	2
169	2016-08-10 17:30:57.017449-05	89	2016-08-10 21:38:40+00:00 - pendiente - 15.5	3		22	2
170	2016-08-10 17:30:57.03254-05	88	2016-08-10 21:38:40+00:00 - pendiente - 15.5	3		22	2
171	2016-08-10 17:30:57.045248-05	87	2016-08-10 21:37:01+00:00 - pendiente - 17.0	3		22	2
172	2016-08-10 17:30:57.059528-05	86	2016-08-10 21:37:01+00:00 - pendiente - 17.0	3		22	2
173	2016-08-10 17:30:57.076904-05	85	2016-08-10 21:37:01+00:00 - pendiente - 17.0	3		22	2
174	2016-08-10 17:30:57.092472-05	84	2016-08-10 21:36:57+00:00 - pendiente - 17.0	3		22	2
175	2016-08-10 17:30:57.100376-05	83	2016-08-10 21:36:57+00:00 - pendiente - 17.0	3		22	2
176	2016-08-10 17:30:57.111697-05	82	2016-08-10 21:36:57+00:00 - pendiente - 17.0	3		22	2
177	2016-08-10 17:30:57.119474-05	81	2016-08-10 21:36:56+00:00 - pendiente - 17.0	3		22	2
178	2016-08-10 17:30:57.135455-05	80	2016-08-10 21:36:56+00:00 - pendiente - 17.0	3		22	2
179	2016-08-10 17:30:57.149248-05	79	2016-08-10 21:36:56+00:00 - pendiente - 17.0	3		22	2
180	2016-08-10 17:30:57.156701-05	78	2016-08-10 21:36:55+00:00 - pendiente - 17.0	3		22	2
181	2016-08-10 17:30:57.165946-05	77	2016-08-10 21:36:21+00:00 - pendiente - 17.0	3		22	2
182	2016-08-10 17:30:57.183579-05	76	2016-08-10 21:36:20+00:00 - pendiente - 17.0	3		22	2
183	2016-08-10 17:30:57.199855-05	75	2016-08-10 21:36:02+00:00 - pendiente - 17.0	3		22	2
184	2016-08-10 17:30:57.216601-05	74	2016-08-10 21:33:34+00:00 - pendiente - 6.5	3		22	2
185	2016-08-10 17:30:57.245069-05	73	2016-08-10 21:33:07+00:00 - pendiente - 19.0	3		22	2
186	2016-08-10 17:30:57.266318-05	72	2016-08-10 21:33:07+00:00 - pendiente - 19.0	3		22	2
187	2016-08-10 17:30:57.283009-05	71	2016-08-10 21:33:07+00:00 - pendiente - 19.0	3		22	2
188	2016-08-10 17:30:57.299752-05	70	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
189	2016-08-10 17:30:57.316609-05	69	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
190	2016-08-10 17:30:57.341257-05	68	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
191	2016-08-10 17:30:57.358362-05	67	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
192	2016-08-10 17:30:57.369999-05	66	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
193	2016-08-10 17:30:57.376659-05	65	2016-08-10 21:31:25+00:00 - pendiente - 19.0	3		22	2
194	2016-08-10 17:30:57.383204-05	64	2016-08-10 21:30:10+00:00 - pendiente - 15.5	3		22	2
195	2016-08-10 17:30:57.399581-05	63	2016-08-10 21:27:54+00:00 - pendiente - 6.5	3		22	2
196	2016-08-10 17:30:57.407432-05	62	2016-08-10 21:24:22+00:00 - pendiente - 17.0	3		22	2
197	2016-08-10 17:30:57.418838-05	61	2016-08-10 21:19:18+00:00 - pendiente - 29.5	3		22	2
198	2016-08-10 17:30:57.426159-05	60	2016-08-10 21:18:15+00:00 - pendiente - 29.5	3		22	2
199	2016-08-10 17:30:57.440484-05	59	2016-08-10 21:07:16+00:00 - pendiente - 20.0	3		22	2
200	2016-08-10 17:30:57.449994-05	58	2016-08-10 20:59:14+00:00 - pendiente - 28.5	3		22	2
201	2016-08-10 17:30:57.465775-05	57	2016-08-10 20:58:53+00:00 - pendiente - 35.5	3		22	2
202	2016-08-10 17:30:57.474079-05	56	2016-08-10 20:58:44+00:00 - pendiente - 30.5	3		22	2
203	2016-08-10 17:30:57.482438-05	55	2016-08-10 20:57:11+00:00 - pendiente - 35.5	3		22	2
204	2016-08-10 17:30:57.488862-05	54	2016-08-10 18:45:34+00:00 - pendiente - 27.5	3		22	2
205	2016-08-10 17:30:57.495384-05	53	2016-08-10 16:27:17+00:00 - pendiente - 3.0	3		22	2
206	2016-08-10 17:30:57.507564-05	52	2016-08-10 00:33:46+00:00 - confirmado - 21.5	3		22	2
207	2016-08-10 17:30:57.514402-05	51	2016-08-10 00:31:06+00:00 - pendiente - 3.5	3		22	2
208	2016-08-10 17:30:57.525592-05	50	2016-08-09 21:22:06+00:00 - enviado - 28.5	3		22	2
209	2016-08-10 17:30:57.533579-05	49	2016-08-09 21:21:25+00:00 - pendiente - 28.5	3		22	2
210	2016-08-10 17:30:57.54383-05	48	2016-08-09 21:20:56+00:00 - pendiente - 28.5	3		22	2
211	2016-08-10 17:30:57.55744-05	47	2016-08-09 21:20:04+00:00 - pendiente - 28.5	3		22	2
212	2016-08-10 17:30:57.564322-05	46	2016-08-09 21:17:43+00:00 - pendiente - 13.5	3		22	2
213	2016-08-10 17:30:57.574102-05	45	2016-08-09 20:41:10+00:00 - pendiente - 43.5	3		22	2
214	2016-08-10 17:30:57.582318-05	44	2016-08-09 19:21:06+00:00 - pendiente - 6.5	3		22	2
215	2016-08-10 17:30:57.588592-05	43	2016-08-09 19:21:02+00:00 - pendiente - 6.5	3		22	2
216	2016-08-10 17:30:57.615824-05	42	2016-08-09 19:19:43+00:00 - pendiente - 6.5	3		22	2
217	2016-08-10 17:30:57.632788-05	41	2016-08-09 18:50:26+00:00 - enviado - 41.5	3		22	2
218	2016-08-10 17:30:57.649493-05	40	2016-08-09 18:50:12+00:00 - pendiente - 11.5	3		22	2
219	2016-08-10 17:30:57.666589-05	39	2016-08-09 13:04:46+00:00 - pendiente - 105.5	3		22	2
220	2016-08-10 17:30:57.683007-05	38	2016-08-09 13:03:26+00:00 - pendiente - 5.5	3		22	2
221	2016-08-10 17:30:57.700877-05	37	2016-08-09 13:02:10+00:00 - pendiente - 5.5	3		22	2
222	2016-08-10 17:30:57.727871-05	36	2016-08-09 13:01:33+00:00 - pendiente - 5.5	3		22	2
223	2016-08-10 17:30:57.743856-05	35	2016-08-09 12:57:57+00:00 - pendiente - 5.5	3		22	2
224	2016-08-10 17:30:57.758098-05	34	2016-08-09 12:46:45+00:00 - pendiente - 11.5	3		22	2
225	2016-08-10 17:30:57.777238-05	33	2016-08-09 12:31:23+00:00 - pendiente - 14.5	3		22	2
226	2016-08-10 17:30:57.784851-05	32	2016-08-09 12:28:38+00:00 - pendiente - 14.5	3		22	2
227	2016-08-10 17:30:57.791659-05	31	2016-08-09 12:00:11+00:00 - pendiente - 11.5	3		22	2
228	2016-08-10 17:30:57.810147-05	30	2016-08-09 11:08:10+00:00 - enviado - 6.5	3		22	2
229	2016-08-10 17:30:57.817697-05	29	2016-08-09 11:03:05+00:00 - enviado - 7.5	3		22	2
230	2016-08-10 17:30:57.832586-05	28	2016-08-09 10:59:52+00:00 - enviado - 2.5	3		22	2
231	2016-08-10 17:30:57.847138-05	27	2016-08-09 15:44:32+00:00 - enviado - 11.5	3		22	2
232	2016-08-10 17:30:57.860094-05	26	2016-08-06 00:39:28+00:00 - pendiente - 2.5	3		22	2
233	2016-08-10 17:30:57.869979-05	25	2016-08-04 20:07:15+00:00 - pendiente - 2.5	3		22	2
234	2016-08-10 17:30:57.88288-05	24	2016-08-04 18:31:03+00:00 - pendiente - 21.5	3		22	2
235	2016-08-10 17:30:57.891491-05	23	2016-08-03 22:35:21+00:00 - enviado - 2.5	3		22	2
236	2016-08-10 17:30:57.899672-05	22	2016-08-03 21:45:47+00:00 - enviado - 2.5	3		22	2
237	2016-08-10 17:30:57.911899-05	21	2016-08-03 19:40:33+00:00 - enviado - 4.5	3		22	2
238	2016-08-10 17:30:57.920193-05	20	2016-08-03 19:39:29+00:00 - enviado - 2.5	3		22	2
239	2016-08-10 17:30:57.934156-05	19	2016-08-03 17:29:53+00:00 - enviado - 2.5	3		22	2
240	2016-08-10 18:34:31.506326-05	6	Frutas y Jugos	1	Añadido.	10	2
241	2016-08-10 18:35:16.079128-05	55	fruques	1	Añadido.	5	2
242	2016-08-10 18:35:39.428714-05	56	fruques2	1	Añadido.	5	2
243	2016-08-10 18:36:59.705463-05	8	Fruques	1	Añadido.	11	2
244	2016-08-10 18:37:07.462498-05	9	Fruques	1	Añadido.	15	2
245	2016-08-10 18:37:11.299691-05	8	Fruques	1	Añadido.	16	2
246	2016-08-10 18:37:51.821612-05	86	fruques	2	Modificado/a ruta_foto.	9	2
247	2016-08-10 18:37:53.055749-05	8	Fruques	2	No ha cambiado ningún campo.	11	2
248	2016-08-10 18:37:55.144419-05	8	Fruques	2	No ha cambiado ningún campo.	16	2
249	2016-08-10 18:42:44.121248-05	8	Fruques	3		16	2
250	2016-08-10 18:43:06.854409-05	6	Frutas y Jugos	3		10	2
251	2016-08-11 09:45:37.54196-05	122	2016-08-10 22:48:45+00:00 - pendiente - 30.0	3		22	2
252	2016-08-11 09:45:37.56597-05	121	2016-08-10 22:47:55+00:00 - pendiente - 6.0	3		22	2
253	2016-08-11 09:45:37.58389-05	120	2016-08-10 22:46:48+00:00 - pendiente - 2.5	3		22	2
254	2016-08-11 09:45:37.596208-05	119	2016-08-10 22:39:44+00:00 - pendiente - 11.5	3		22	2
255	2016-08-11 09:45:37.622398-05	118	2016-08-10 22:32:53+00:00 - pendiente - 19.5	3		22	2
256	2016-08-11 09:45:37.630836-05	117	2016-08-10 22:31:33+00:00 - pendiente - 6.5	3		22	2
257	2016-08-11 09:50:10.554505-05	1	4	1	Añadido.	33	2
258	2016-08-11 09:52:04.331675-05	2	3	1	Añadido.	33	2
259	2016-08-11 09:53:19.163716-05	3	5	1	Añadido.	33	2
260	2016-08-11 09:57:59.479413-05	4	4	1	Añadido.	33	2
261	2016-08-11 11:49:37.018911-05	139	2016-08-11 16:21:52+00:00 - pendiente - 10.0	3		22	2
262	2016-08-11 11:49:37.048401-05	138	2016-08-11 16:12:09+00:00 - pendiente - 10.0	3		22	2
263	2016-08-11 11:49:37.078803-05	137	2016-08-11 16:11:45+00:00 - pendiente - 4.5	3		22	2
264	2016-08-11 11:49:37.087521-05	136	2016-08-11 15:48:27+00:00 - pendiente - 3.5	3		22	2
265	2016-08-11 11:49:37.103426-05	135	2016-08-11 15:38:50+00:00 - pendiente - 3.5	3		22	2
266	2016-08-11 11:49:37.122096-05	134	2016-08-11 15:27:02+00:00 - enviado - 37.5	3		22	2
267	2016-08-11 11:49:37.135019-05	133	2016-08-11 00:11:38+00:00 - enviado - 17.0	3		22	2
268	2016-08-11 11:49:37.156985-05	132	2016-08-11 00:09:28+00:00 - pendiente - 9.0	3		22	2
269	2016-08-11 11:49:37.165883-05	131	2016-08-11 00:04:55+00:00 - pendiente - 81.5	3		22	2
270	2016-08-11 11:49:37.18034-05	130	2016-08-11 00:04:51+00:00 - pendiente - 81.5	3		22	2
271	2016-08-11 11:49:37.188706-05	129	2016-08-11 00:03:59+00:00 - pendiente - 81.5	3		22	2
272	2016-08-11 11:49:37.196671-05	128	2016-08-10 23:36:26+00:00 - pendiente - 3.0	3		22	2
273	2016-08-11 11:49:37.208086-05	127	2016-08-10 23:36:32+00:00 - pendiente - 42.5	3		22	2
274	2016-08-11 11:49:37.222485-05	126	2016-08-10 23:36:21+00:00 - pendiente - 3.0	3		22	2
275	2016-08-11 11:49:37.230231-05	125	2016-08-10 23:35:51+00:00 - pendiente - 7.0	3		22	2
276	2016-08-11 11:49:37.245423-05	124	2016-08-10 22:50:36+00:00 - pendiente - 41.0	3		22	2
277	2016-08-11 11:49:37.25511-05	123	2016-08-10 22:50:06+00:00 - pendiente - 41.0	3		22	2
278	2016-08-11 12:01:42.39682-05	5	3	1	Añadido.	33	2
279	2016-08-11 12:04:20.107037-05	6	4	1	Añadido.	33	2
280	2016-08-11 12:04:58.646755-05	7	3	1	Añadido.	33	2
281	2016-08-11 14:02:36.530052-05	7	Pollos	1	Añadido.	10	2
282	2016-08-11 14:05:00.915294-05	62	gusy	1	Añadido.	5	2
283	2016-08-11 14:06:04.483602-05	62	gusy2	2	Modificado/a username.	5	2
284	2016-08-11 14:06:40.824247-05	62	gusy2	2	Modificado/a groups.	5	2
285	2016-08-11 14:09:37.41132-05	63	gusy	1	Añadido.	5	2
286	2016-08-11 14:11:26.034102-05	94	gusy	2	Modificado/a ruta_foto.	9	2
287	2016-08-11 14:12:02.742939-05	9	Gusy	1	Añadido.	11	2
288	2016-08-11 14:12:28.228425-05	11	Gusy	1	Añadido.	15	2
289	2016-08-11 14:12:30.760267-05	9	Gusy	1	Añadido.	16	2
290	2016-08-11 16:06:37.128618-05	43	Helado de chocolate	1	Añadido.	31	2
291	2016-08-11 16:07:15.503629-05	19	chocolate	1	Añadido.	28	2
292	2016-08-11 16:07:30.591689-05	43	Helado de chocolate	2	Modificado/a id_ingrediente y requerido.	31	2
293	2016-08-13 17:37:18.06672-05	2	pizza	2	Modificado/a ruta_foto.	9	2
294	2016-08-15 12:25:03.922926-05	94	gusy	2	Modificado/a ruta_foto.	9	2
295	2016-08-15 12:38:04.06346-05	20	Tallarin Salteado de Pollo	2	Modificado/a ruta_foto.	24	2
296	2016-08-15 12:39:13.894264-05	22	Burrito	2	Modificado/a ruta_foto.	24	2
297	2016-08-15 12:41:29.786898-05	10	pollo	2	Modificado/a ruta_foto.	24	2
298	2016-08-15 12:43:20.439097-05	21	tallarin y pollo	2	Modificado/a ruta_foto.	24	2
299	2016-08-15 12:44:50.520948-05	23	Arroz Relleno	2	Modificado/a ruta_foto.	24	2
300	2016-08-15 12:47:32.208441-05	25	Pizza de tomate	2	Modificado/a ruta_foto.	24	2
301	2016-08-17 11:29:43.924899-05	106	loxad	3		9	2
302	2016-08-17 11:29:43.951364-05	105	pamelixxx	3		9	2
303	2016-08-17 11:29:43.959735-05	104	Nicho	3		9	2
304	2016-08-17 11:29:43.967996-05	103	nicolll	3		9	2
305	2016-08-17 11:29:43.976406-05	102	pal	3		9	2
306	2016-08-17 11:29:43.984888-05	101	pamellix	3		9	2
307	2016-08-17 11:29:43.993213-05	100	pamelixx	3		9	2
308	2016-08-17 11:29:44.001524-05	99	pamelix	3		9	2
309	2016-08-17 11:29:44.009879-05	98	pfguaman	3		9	2
310	2016-08-17 11:29:44.018111-05	97	patch	3		9	2
311	2016-08-17 11:29:44.026525-05	96	roch	3		9	2
312	2016-08-17 11:29:44.034879-05	95	rosi	3		9	2
313	2016-08-17 11:29:44.043103-05	92	fdfdfdas	3		9	2
314	2016-08-17 11:29:44.051448-05	89	hhhh	3		9	2
315	2016-08-17 11:29:44.059885-05	88	joselo	3		9	2
316	2016-08-17 11:29:44.068188-05	85	John	3		9	2
317	2016-08-17 11:29:44.076555-05	84	lili90	3		9	2
318	2016-08-17 11:29:44.084849-05	83	otro	3		9	2
319	2016-08-17 11:29:44.093101-05	82	usuario100	3		9	2
320	2016-08-17 11:29:44.101465-05	79	fsfds	3		9	2
321	2016-08-17 11:29:44.109858-05	78	clienete2	3		9	2
322	2016-08-17 11:29:44.118098-05	76	hfh	3		9	2
323	2016-08-17 11:29:44.126658-05	75	:Carlos	3		9	2
324	2016-08-17 11:29:44.134906-05	74	dickson4	3		9	2
325	2016-08-17 11:29:44.143208-05	73	dickson2	3		9	2
326	2016-08-17 11:29:44.151654-05	72	dickson	3		9	2
327	2016-08-17 11:29:44.159839-05	71	ggggg	3		9	2
328	2016-08-17 11:29:44.168208-05	65	pamm	3		9	2
329	2016-08-17 11:29:44.176622-05	64	pam	3		9	2
330	2016-08-17 11:29:44.18494-05	63	prueba	3		9	2
331	2016-08-17 11:29:44.193269-05	62	pameliz	3		9	2
332	2016-08-17 11:29:44.201634-05	59	nich	3		9	2
333	2016-08-17 11:29:44.210043-05	58	posix	3		9	2
334	2016-08-17 11:29:44.218269-05	57	lim	3		9	2
335	2016-08-17 11:29:44.226635-05	56	lik	3		9	2
336	2016-08-17 11:29:44.23494-05	53	n	3		9	2
337	2016-08-17 11:29:44.243167-05	52	lola	3		9	2
338	2016-08-17 11:29:44.25162-05	51	armijos	3		9	2
339	2016-08-17 11:29:44.261138-05	40	nadia3	3		9	2
340	2016-08-17 11:29:44.269547-05	39	nadia2	3		9	2
341	2016-08-17 11:29:44.277927-05	37	pppp	3		9	2
342	2016-08-17 11:29:44.286199-05	35	hola	3		9	2
343	2016-08-17 11:29:44.294546-05	34	nadia	3		9	2
344	2016-08-17 11:42:33.390348-05	2	pizza	2	Modificado/a ruta_foto.	9	2
345	2016-08-20 08:29:44.11229-05	85	Helado de chocolate : Standard - 3.0	1	Añadido.	26	2
346	2016-08-21 21:45:21.345255-05	200	2016-08-21 21:43:49+00:00 - En Espera - 14.06	3		22	2
347	2016-08-21 21:45:21.364529-05	199	2016-08-21 21:41:49+00:00 - En Espera - 14.06	3		22	2
348	2016-08-21 21:45:21.372879-05	198	2016-08-21 21:40:18+00:00 - En Espera - 14.06	3		22	2
349	2016-08-21 21:45:21.381212-05	197	2016-08-21 21:34:40+00:00 - En Espera - 14.06	3		22	2
350	2016-08-21 21:45:21.3895-05	196	2016-08-21 21:29:57+00:00 - En Espera - 14.06	3		22	2
351	2016-08-21 21:45:21.397869-05	195	2016-08-21 21:24:25+00:00 - En Espera - 14.06	3		22	2
352	2016-08-21 21:45:21.40622-05	194	2016-08-21 21:21:10+00:00 - En Espera - 14.06	3		22	2
353	2016-08-21 21:45:21.414502-05	193	2016-08-21 21:11:39+00:00 - En Espera - 20.0	3		22	2
354	2016-08-21 21:45:21.422833-05	192	2016-08-21 20:55:55+00:00 - En Espera - 14.06	3		22	2
355	2016-08-21 21:45:21.431215-05	191	2016-08-21 20:44:45+00:00 - En Espera - 14.06	3		22	2
356	2016-08-21 21:45:21.439561-05	190	2016-08-21 18:55:32+00:00 - En Espera - 14.06	3		22	2
357	2016-08-21 21:45:21.447948-05	189	2016-08-21 18:50:09+00:00 - En Espera - 64.3	3		22	2
358	2016-08-22 16:28:48.920529-05	7	icecreamsocial	2	No ha cambiado ningún campo.	5	2
359	2016-08-22 16:30:15.124766-05	63	gusy	2	Modificado/a groups.	5	2
360	2016-08-24 19:25:17.743886-05	156	locochavez	1	Añadido.	5	2
361	2016-08-24 19:25:30.416452-05	156	locochavez	2	Modificado/a groups.	5	2
362	2016-08-24 19:28:35.33007-05	10	Loco Chavez	1	Añadido.	11	2
363	2016-08-24 19:34:52.454217-05	88	MErcadiilo - MErcadillo. En el Hub. Número de casa: N/A	1	Añadido.	13	2
364	2016-08-24 19:38:11.32301-05	10	Loco Chavez	1	Añadido.	16	2
365	2016-08-24 19:54:48.462509-05	157	soyard	1	Añadido.	5	2
366	2016-08-24 19:55:05.634461-05	157	soyard	2	Modificado/a groups.	5	2
367	2016-08-24 19:57:22.014099-05	8	Bebidas	1	Añadido.	10	2
368	2016-08-24 19:58:19.162367-05	11	Soyard	1	Añadido.	11	2
369	2016-08-24 20:01:19.72643-05	89	Santa - Sasnta. Esquina. Número de casa: 12-45	1	Añadido.	13	2
370	2016-08-24 20:02:34.516733-05	11	Soyard	1	Añadido.	16	2
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 370, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_content_type (id, app_label, model) FROM stdin;
2	admin	logentry
3	auth	permission
4	auth	group
5	auth	user
6	contenttypes	contenttype
7	sessions	session
8	spaapp	actividadcomercial
9	spaapp	perfil
10	spaapp	categoria
11	spaapp	proveedor
12	spaapp	ciudad
13	spaapp	direccion
14	spaapp	cliente
15	spaapp	menu
16	spaapp	sucursal
17	spaapp	contrato
18	spaapp	transportista
19	spaapp	coordenadas
20	spaapp	tipopago
21	spaapp	tipopedido
22	spaapp	pedido
23	spaapp	platocategoria
24	spaapp	plato
25	spaapp	tamanio
26	spaapp	tamanioplato
27	spaapp	detallepedido
28	spaapp	ingrediente
29	spaapp	horariotransportista
30	spaapp	modificardetallepedido
31	spaapp	platoingrediente
32	spaapp	redsocial
33	spaapp	valoracion
34	administracionAPP	contacto
35	corsheaders	corsmodel
36	default	usersocialauth
37	default	nonce
38	default	association
39	default	code
40	spaapp	catalogoestado
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_content_type_id_seq', 40, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2016-07-28 19:51:48.787104-05
2	auth	0001_initial	2016-07-28 19:51:49.5139-05
3	spaapp	0001_initial	2016-07-28 19:51:53.27189-05
4	admin	0001_initial	2016-07-28 19:54:38.535332-05
5	admin	0002_logentry_remove_auto_add	2016-07-28 19:54:38.590942-05
6	administracionAPP	0001_initial	2016-07-28 19:54:38.653486-05
7	contenttypes	0002_remove_content_type_name	2016-07-28 19:54:38.738091-05
8	auth	0002_alter_permission_name_max_length	2016-07-28 19:54:38.76934-05
9	auth	0003_alter_user_email_max_length	2016-07-28 19:54:38.807789-05
10	auth	0004_alter_user_username_opts	2016-07-28 19:54:38.83907-05
11	auth	0005_alter_user_last_login_null	2016-07-28 19:54:38.870279-05
12	auth	0006_require_contenttypes_0002	2016-07-28 19:54:38.889376-05
13	auth	0007_alter_validators_add_error_messages	2016-07-28 19:54:38.908148-05
14	default	0001_initial	2016-07-28 19:54:39.527389-05
15	default	0002_add_related_name	2016-07-28 19:54:39.675726-05
16	default	0003_alter_email_max_length	2016-07-28 19:54:39.697855-05
17	default	0004_auto_20160423_0400	2016-07-28 19:54:39.745077-05
18	sessions	0001_initial	2016-07-28 19:54:39.876252-05
19	spaapp	0002_detallepedido_tamanio	2016-08-04 13:29:59.024192-05
20	spaapp	1000_permisos	2016-08-08 07:42:38.729428-05
21	spaapp	1001_tamanioplato_activo	2016-08-09 10:08:05.982222-05
22	spaapp	1001_auto_20160809_2046	2016-08-09 15:46:49.71865-05
23	spaapp	1002_auto_20160809_2048	2016-08-09 15:49:04.468895-05
24	spaapp	1003_auto_20160809_2048	2016-08-09 15:51:40.288235-05
25	spaapp	1004_auto_20160809_2052	2016-08-09 15:52:33.585714-05
26	spaapp	0002_auto_20160809_1537	2016-08-11 14:00:24.587069-05
27	spaapp	1001_merge	2016-08-11 14:00:24.591069-05
28	spaapp	1002_auto_20160811_1355	2016-08-11 14:00:25.551934-05
29	spaapp	0500_catalogo_estados	2016-08-11 14:00:25.589216-05
30	spaapp	1003_merge	2016-08-11 14:00:25.592216-05
31	spaapp	0501_auto_20160816_1744	2016-08-16 12:44:32.203947-05
32	spaapp	0502_auto_20160817_2310	2016-08-17 18:10:37.448907-05
33	spaapp	0503_auto_20160819_1603	2016-08-19 11:03:57.915895-05
34	spaapp	0504_auto_20160823_1751	2016-08-23 17:51:36.671793-05
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_migrations_id_seq', 34, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
ppj6kicf3jq9qp14in8osrwxyzvyeccb	ZDA1MzA1NDA1YjY2NzZlZGRmYjQxZWQwMTA3ZWY4MTQ5MjFjN2FiZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-11 19:56:47.864751-05
byyjg5tr2lz8a75b7c2mmliyrulhelmn	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:50:25.121346-05
r8m0atiszs8wyzbm0pvq6m40y3kz8s7p	ZWMyZWIwZTJiYWFlMDczYzRhODhlMWU0Y2M2YzI2MGE5YTljMzUxNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-11 20:20:31.486666-05
629ms707i92p3h9lxr6l82is7mprz3pi	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-17 13:12:25.735389-05
p1ruep8o7eq076v57rzgybu0el5ocbot	Y2ZkZWQ4OTRlZDQwNTRhZjI5MDViMDE2NDFmYWJmNjM3MjJmYjNmZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-17 13:32:49.86211-05
ylf1lnnjy8djcgm278nff0s1z9ndn2z1	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:51:54.426512-05
qicye6ty655n7h5d7hadcqg0ay3wm1wt	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:54:26.189291-05
iqmq1opk3wwyfn1qrs1rpz8org64flh3	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-12 09:47:28.13426-05
ll4fa4qy76k81x55mm3n6gnn4qpi35dk	NzU0NDI1NTE2ZmM2NTFkZTBjM2IxOWFjNGZlMGNmZDJkZGY5MGUxNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDg1Yjk4ZWJjZmMzYzk4Y2MzZjBiNDI2MTA0MTAwMjA5MTI3Yjc3OSIsIl9hdXRoX3VzZXJfaWQiOiI1In0=	2016-08-12 10:26:24.92008-05
agwowo7ygokt7301dv34pdwlw0c2avqe	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:00:32.628856-05
jk1xb61xhok3bn9errznmbtripf8ziir	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:01:26.54987-05
92jp9m02v310e1dbl3id5kqcihznnu3j	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:02:37.675142-05
tef6cdhg87ijg2n3sesh6gt2fprypow5	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:04:05.819932-05
8dt7o50nmaz51q0475x5hkkcf4s58ab2	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:06:18.776876-05
ohyakvkhwhrg344rv1axf8vj33k9lvul	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:07:33.506678-05
xmqm3z1nwcshedj2j3rmrfoaqqxgo7z9	ZWY5MWMzMDczYzY5YjkzMTg1Y2ZkYTBmOTc2NjRlOTYwODkyODk1Njp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE2IiwiX2F1dGhfdXNlcl9oYXNoIjoiMjQ1MGJiYmU3ZTAxYzkwNzE0NDFlN2UwOGVkNmIyZDg1NzA0MWZmMyJ9	2016-08-15 19:00:06.442762-05
fbfmnokg6ds0n6v2b77md75kro2zxhgv	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:08:33.922357-05
69931dblja1ngvaqaablty4dic7mnekf	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-12 11:46:15.503914-05
hieocm5ufi9vr9mn2uakge9nklm0or51	Y2ZkZWQ4OTRlZDQwNTRhZjI5MDViMDE2NDFmYWJmNjM3MjJmYjNmZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-16 16:44:17.221614-05
c77q8j8lpupnnhfwctjq06v47dmnncib	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:09:32.99599-05
dmuyidbk4yjiar7zoekt9r8euit3yc5o	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:11:20.387611-05
60a28po4ajzfi64lcuy4yl2oaxt3exka	YjlhZTcyNmUyNjcyNWZkNjMzZWM0MmFjYWE4MzVmYzI5ZDNhNTc3Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCIsIl9hdXRoX3VzZXJfaWQiOiIxMSJ9	2016-08-16 16:13:40.970215-05
60yv4yb71z7j5gpkfcunhuhksx87ny4z	NzBkNDRkZmQwZThkMzgxNTg0NTY4ZDkxMjdiNGQ2YzBlZGQ5NWEyNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjIxMjZkNzBlY2Q4NGI4MzdlMTJkODUwMDRhNWNmYTU3YTg0YTJiMTEiLCJfYXV0aF91c2VyX2lkIjoiMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 18:47:39.240926-05
4i86no7b1ec4wewwws5bdgr2wpo3ans9	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-16 19:41:50.740148-05
evtirjme540ojzrh7ehbiwjx22p4n2v3	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-08-19 18:47:42.733928-05
3ybo7v1j7ey0ljhqv7uzjovh7ta1jjwb	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-24 18:44:05.407222-05
klep4lrbq90uwaswln4bsnriz1h62rkc	NmExMWY4OGYwNjQ2NzU4NDUzZjcwOWEzYTY1NmMwYTVhOWFkY2JjMDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-23 16:58:34.240676-05
1lr1tqnzdo2np9on07zc9r0oobvyamnu	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-23 17:31:07.945934-05
kytdr35051wmubtsqa4vqs2tp8w9vfxp	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-23 05:15:42.789643-05
ze1r4i88d4r26y42ds2c0jrbvelzb0cz	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:15:59.871789-05
z68i0whmlxvuljujrmyajl7g3zgljevj	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 19:20:06.562403-05
9ly8onkb53mwi075rk7pbannfzqcqzga	ZDA1MzA1NDA1YjY2NzZlZGRmYjQxZWQwMTA3ZWY4MTQ5MjFjN2FiZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-22 12:38:12.891261-05
b2znfm1lm616xxq7o6z8har2uhn6v7sm	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 13:35:02.816758-05
7ltwkygblhldn3ez72zfhl67rocb4nyu	OTI2ZTQwZTcyNDVlNzE5MzFjYjg1Mjk3OWJmYTYzOTk1N2ZhZDVjYjp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 17:43:50.34102-05
6on07f95xaicuepctq9wv1epqkn7ofan	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-24 17:56:58.569473-05
knqzjjzntzh0fnmbqohz29v7v6681dyx	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-17 09:58:36.279725-05
97f99r15y8v31uekudgu94yhbtflcvl1	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:52:37.461965-05
9rckvf5kzjcrgrapl9uxrqjdml84kd6i	M2NhYThhYTVjMDFhZTM4NmQxYmMxMDI5NGFiYmRhMzBkOGE5ZmVmYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjYwIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTQwMmZlMTNlZjA1ODVjMDIyNzYyN2Q2ZGZjOTNlYzg0MWMzNzgwMSJ9	2016-08-24 19:06:35.025527-05
haohankyyn1vaqqvkdu9do5ntxl04rsb	M2JkZWMxZWJiMDRkYTJiYzE0MDc0NWY0YjJkNzgxNTAwZTQxNTQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjVmNmQyNzRjZmVmNzMyYTQ3NjlhZjIwOTk3MGVjNjQwODczZTI4MDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMyJ9	2016-08-19 15:29:30.612875-05
v2a616t4h3fou3lfmpbfodsswp94o7vm	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 18:58:05.177444-05
6r13xr1pwic7gk2da7a38g95mxjlmswi	OWU5MDZjMzcwODU0OTJkYmQ3MmMwOGRmN2NiMjAzMmU4ZDk3NmE1ZDp7ImZhY2Vib29rX3N0YXRlIjoiQVh5NEgyd21Kd3hFZml0VVJNTXBwcGhQTldXcElabzQifQ==	2016-08-23 12:38:15.781441-05
0n7p7dxbdvrgyfrgomt94oqbm689fqio	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-18 18:11:00.613094-05
d812mif0fza9glx9hmuc6oq3fpfxd3m3	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 19:00:13.095156-05
i8ojuq4kc47nfzj6ka53s7hc7lpqrefw	NGM0MDQwOTM1YmVjMDFjYmFlMzMyYmIyZjZlYjI5MjM5NGQyNWQyZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkMThiN2U5ZDFiNzc5ZDQyZjJlY2JhYWZmMGY2ODEyZTdlNWNhYTgiLCJfYXV0aF91c2VyX2lkIjoiMyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 19:23:02.228264-05
30sspuhkbhyasr7dmlnyvtl9c5c6ql10	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-18 18:32:23.211228-05
0d4hy45xys5xh2p1p894r6xu08tgb5vx	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:16:31.408975-05
gjhnqtcwa2c4np85vus9nv5vn23pd32j	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-25 10:23:55.109557-05
o8ft3afaf2bbvf6fbxjz3mpu1tp7nz1f	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-23 05:16:59.421808-05
r2mnkfqc48jfgag7qxpq240u7swma98c	NDE0YjAzYWZjZDVhOWQ0NWM1ZmJjYmRiZTA0OWQ2NTVjNzNkMzMzNzp7ImZhY2Vib29rX3N0YXRlIjoiZ283OEZKbWlSZ3NLcEQ3V1V6c1daVWtMcGRvc09PaHUifQ==	2016-09-03 16:54:11.702323-05
sizmi1n04eiafgirv8wxrtlpwrgyud95	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-22 11:19:34.676542-05
vlyavao82e1h3x9z1qxpvwbc68ypg1xl	YjlhZTcyNmUyNjcyNWZkNjMzZWM0MmFjYWE4MzVmYzI5ZDNhNTc3Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCIsIl9hdXRoX3VzZXJfaWQiOiIxMSJ9	2016-08-23 15:40:39.532232-05
07w4enkhj4wldnbmf8xgkj4p3goydw4u	NmMyMTJlZmFhODZlZTViY2I4OGZkMmFkZmYxNGFmOTA4NDc5Zjg3YTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQ4NWI5OGViY2ZjM2M5OGNjM2YwYjQyNjEwNDEwMDIwOTEyN2I3NzkiLCJfYXV0aF91c2VyX2lkIjoiNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-23 18:21:54.059464-05
6hlx6f6dqh31r1f3awpbolopknf1i1c8	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-25 10:41:37.826063-05
t3pefx82p657mnszzcccaf09g0bf97q6	ZDI5YzBlOTI1NjU0NDU0MmZlZDQ0Y2EwMTEyYTI3MmQ5MDIzYzM2Yjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-25 13:58:23.795385-05
8vbealdgqru37rlelimdm886oigzd7jf	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 10:16:20.85285-05
be9phshrn2591vno1istb3dno4m74v2h	Y2ZkZWQ4OTRlZDQwNTRhZjI5MDViMDE2NDFmYWJmNjM3MjJmYjNmZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-23 06:46:55.079654-05
mprpxxw2f4bbzgetii01nlizoczzge3e	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-24 10:21:10.422381-05
b53tckl2skz5o5ukcy3rzch6u2vafory	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-25 16:12:18.846511-05
7s5sv44unuaj75akivkj5l25sjzvb4rt	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-08-25 17:14:13.755431-05
uwtlobbr4fvl1ag2bemnmekk60a68bss	ZDI5YzBlOTI1NjU0NDU0MmZlZDQ0Y2EwMTEyYTI3MmQ5MDIzYzM2Yjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-24 18:42:33.626202-05
l5av3g3l9cwj8nwk76gk9ccwoeqs95b6	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:50:44.500941-05
vcarmf4br7hb8y48wlz9if1zybovs1v7	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-25 18:39:55.97987-05
t8z6hciq9s543bz4fagd4wmpoutnp6f4	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 20:01:37.274315-05
1ufmuae690t5zuyraf4w6t87uekzy47b	MDBjYmQ0MTU3NjNmMDZkY2I3NGMxMGExZTgxNTZlNzhiZGQ2YmI2OTp7ImZhY2Vib29rX3N0YXRlIjoiRmo5NG9KTmx5M2NoYlpTdlZyblFJaWpkZlJLWnVWSDcifQ==	2016-08-23 12:43:03.416885-05
1vbvwpxwn9juq94zofqk5k0wbkevur63	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 19:07:59.703039-05
8bkb51daf06vfgntm41fzj81squ257fk	NzBkNDRkZmQwZThkMzgxNTg0NTY4ZDkxMjdiNGQ2YzBlZGQ5NWEyNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjIxMjZkNzBlY2Q4NGI4MzdlMTJkODUwMDRhNWNmYTU3YTg0YTJiMTEiLCJfYXV0aF91c2VyX2lkIjoiMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-17 15:26:45.337013-05
436gy6wymsim4kp5x5py0twlqlx20pnc	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-22 19:19:45.142173-05
ak354aecsz4ry3s6j0zifamzvvz6rh2w	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 19:49:59.18873-05
rp5vg45iu24z8d2qngjommdgxrt2y015	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-22 11:18:31.396348-05
3zqelyq9agjhr1s1ke8i9shsfxqwane6	NjY0MjNkOThmNmIzNTc2YmFiMDQ2Njc1ZTcwOTYyMGNkY2JhMzhkNzp7Il9hdXRoX3VzZXJfaWQiOiI2OCIsIl9hdXRoX3VzZXJfaGFzaCI6ImI2YjA4YjBhOGJmNzM1OTY2M2RlZjU3NzM2MmZlYTNkM2M4Y2IwMTciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-26 11:20:24.948739-05
tgqz2xgxcyrrsxgp2tqsj8av5czqcv56	M2JkZWMxZWJiMDRkYTJiYzE0MDc0NWY0YjJkNzgxNTAwZTQxNTQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjVmNmQyNzRjZmVmNzMyYTQ3NjlhZjIwOTk3MGVjNjQwODczZTI4MDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMyJ9	2016-08-19 17:17:37.144736-05
e8mcz2tl7qafw68oykti2i0fchlcko8p	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-26 13:02:32.566872-05
74vg5gvq5ud1bidt9n01c2kdkqva90bd	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-24 15:08:10.836448-05
p2xhlf20wmoh20sb1dn8w3ocghh62f1z	ZTM1NGMyZWEyNTVhYjJlMzg4MmVkYTkzYTQ2N2M3NmIyY2FhYmI0Mjp7ImZhY2Vib29rX3N0YXRlIjoieThOc2pyeGxjMTJnY3BSanFHcUV4S1NxS1BmTVZ5TnYifQ==	2016-09-02 18:44:17.984704-05
hme61ycis1ksrln4cy0c57cpvetwqnxh	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-26 15:26:53.816808-05
daaqb9hl5gm0h36l8c2xj2nbjem77tnc	ZDhjYWNmZmMwNDU2YzE1NThiODMwMmZhOGE4ZjEyZWE2NzBjNzhjYzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiZmFjZWJvb2tfc3RhdGUiOiJJQjd6WTRkTVpjVzVtZzdMeE56OXYzd3FWRXpvYlZobiIsIl9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2lkIjoiMiJ9	2016-08-25 13:21:58.538828-05
rernz898vpvxx652lynug6wlxjjhm1ji	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 10:23:18.50895-05
rru2d0zfpsbrikx7ec3rg65ntgtoa01y	ZDA1MzA1NDA1YjY2NzZlZGRmYjQxZWQwMTA3ZWY4MTQ5MjFjN2FiZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-24 18:42:27.378344-05
emmf1l8cwo2dwk11qfdrxuve71bqbvr7	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-17 11:43:49.982016-05
7wv2cqf7egvxgqm3kps3cp6xy92q0unf	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:51:56.248083-05
crl9q5frwybe28s2ogu4y0q0iz56wwdu	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:52:58.365229-05
8q06zc9kxtloqn6acibaq0jk74rt130m	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 18:58:05.191455-05
b90hjqjxddieetw0v3b7c7frf9wqds09	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-25 19:47:40.576281-05
annfhv5mdbbvxkp7d7bj7ig3366iokpi	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-25 20:02:44.374968-05
y7jbgns389c0ie22slraice0rliamrvo	ODgxYTNmM2Q1ZDkwNDEwZDY1ODJiMWMwYzA5OTM2M2ExNmEzZWNkNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2016-08-23 14:26:59.799133-05
ds4i3wdenys5gn56lw2kzeqtzofvigei	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:18:28.363795-05
0m6nc0qhptb6j5vhrbc9q5naic7u2gch	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-24 15:58:07.003286-05
hj9hti1sb8jkq5h14o17yd4d293is2y3	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-19 18:07:50.929245-05
b4j0byl9jefyww3rl86bfh97kt8vq88d	ZWMyZWIwZTJiYWFlMDczYzRhODhlMWU0Y2M2YzI2MGE5YTljMzUxNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-25 12:06:06.316089-05
j1l9fwj3045pxmckggfab50j1mw2pf0d	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-26 11:21:29.067618-05
m4vb03ow8glvot1zcqinlo3nrw6wc00s	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 10:18:24.991075-05
xu0tksigrtuq74b2t0g8xpkehkcb7w7r	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 13:22:02.29852-05
zmkzlym30cub8l8njs6i7a4uktj7ntbh	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-24 18:37:29.884472-05
uqn22cz9r6sze11e7bt2nepi42b220rv	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-26 13:42:06.446134-05
tslbu2m283oro2sdi3870z45c9qwuuuy	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-26 19:28:00.37671-05
b6vh3msv729e7tlvump8lzakitozqghz	ZWM4OWU1ZDUzYjUyODU1OGY4M2I3ZDRjMWRiMzMxMjFlYjQxNzUzMDp7Il9hdXRoX3VzZXJfaWQiOiI1OSIsIl9hdXRoX3VzZXJfaGFzaCI6ImY5NDhkZmUxMzU0YmE2ZDIyMGJhODU3MzlmNGIwOWVkZDc4OTI4NDQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 17:43:50.006002-05
cn45r6z3kpla29dv9fl4yi2gnlwb3tk7	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-09-01 15:12:27.582384-05
2zei3qq0o6hzi72h1c8027fj8nwj931m	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-09-02 17:15:47.358984-05
sgbsgbdpgz3p951y7owv6e28n6phbhuj	NTk3ZTFhNDUyODUzNGQ5OTcwZjk5MzRmYzYxZWI4Mzc3YTZiMTZhZjp7ImZhY2Vib29rX3N0YXRlIjoieWJHOUxoWE5kNzVDdmZKS210dmN6aHdmVUNjc1lVSGYifQ==	2016-09-06 19:03:06.840929-05
dk2p65ff2ae3k24l8co29oqkkkjfcj94	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-01 13:19:41.833072-05
4qs0dmxow1aoggwxmrzga2dqmvxbx0ue	YzkwMTY2MDI5NGEyMWYwY2VkMDgyZjIwMjlkYTFiNzIxNTgwMmNiZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2016-08-30 12:51:22.747665-05
vwkziswg0k7ulbd600zttbtj2xrqqy8y	NWNiNzNhNDY4NTFmMTEzMmM3ZTIyYzhhMTFhYmMyZTI2ZWJkNzExZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjM0MDExYzkxZDNkMTVlMjIzYWYzYTRhMzY0ZTJmNzhhOGEwM2YyNjUiLCJfYXV0aF91c2VyX2lkIjoiMTU4IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==	2016-09-08 07:50:08.013681-05
s6krr1j4wjbcb7uoxeyepjhg2xlnrq7s	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 17:49:52.798995-05
lx1usud33qvnso60w1r385dlhz2ux71u	N2I3OGNmM2JjYzMzZWY1MTMwODRjMWViNzJhOGVjMmQzN2NiNTUyNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzJkNzJjMmZjODcyNGU2YjE4MjhkYzM1ZDY2MTFhMzE2ZGY3NjdlZCIsIl9hdXRoX3VzZXJfaWQiOiIxMjgifQ==	2016-09-06 18:54:57.936663-05
mqe7v2q28u5jxgkx1epdoeyly8wuiwjc	YzEyZDg1MjlhODIxYzA5MDY5NjhiMWUxNjA2YmY2M2U0NzA2MWNlNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTA1YThiNjE3ZDhkNDM5MjIwZGJjZWY4Y2Y5ZjE1NzgzYWZhMmQwOSIsImZhY2Vib29rX3N0YXRlIjoiWXF3cGlZNHpEYXlRYWc4VlJ0QllCc3pvSU5Xd2c1WVEiLCJfYXV0aF91c2VyX2lkIjoiOTcifQ==	2016-09-02 18:29:42.471472-05
xfidzdmmd42xexzx9kqm4mdpe8xrzzcb	MDhjMDY0MDcxODQwYjE1ZDY2NmU4ZjFlNGU5YjUyMzYzZTI3NGM4OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZGFhYWE2OWYxYjhjZDY5MzYzZWY2YTY5MjA5YWZkNzc1Y2RiZTZiNiIsIl9hdXRoX3VzZXJfaWQiOiIxMDkifQ==	2016-09-02 18:29:44.037966-05
x1qf3hy4ezl1qo48s19nicwcfd1lni5g	MGRkZDEzNTBhMjdmMzViZGI4YWJhZTUzM2ViNmMzZmJjYTMzYWNmZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImU0MDJmZTEzZWYwNTg1YzAyMjc2MjdkNmRmYzkzZWM4NDFjMzc4MDEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI2MCJ9	2016-08-30 21:19:39.912867-05
q1rlmhux3gfbcai2sqsd5gv30ky6jog7	MTZmNTAyYWE4NTlkNzcxYjM0NmRhODkwYWUwY2RiMjViODE0NjBjODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTc2ODI1YjVmMDc3N2MxN2Y5MzJmZDk5NjljODYyYTk2YWExMDA1MiIsIl9hdXRoX3VzZXJfaWQiOiIxMTIifQ==	2016-09-02 18:29:45.32147-05
184dlvpzeudfdinljs5cdxaqzg1sbos8	MzkzYmZhODM1ZjQ3Zjc3ZjBkMDQ0NTBiZGIxZGZiY2JlOWFhMGI5Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMmFkZGIwYTM0M2NiMDFjYmJkNTVkOWUwYzlkNjQzOGIxMmVlOTM3NCIsIl9hdXRoX3VzZXJfaWQiOiIxMDMifQ==	2016-09-02 18:29:45.713154-05
pw3hp1zj5kc3fgcatl4zmztjdrsu1u69	YjY2YjgzMmIyZTBkMDZmNzhlNmY1YjE5MThmNWE4ZmM5MDk2NmM5Mjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDAxZGExYzMwYzQwOTExN2U4NmZmMDE4MTZkMjJiZTZjODdiM2RmYSIsIl9hdXRoX3VzZXJfaWQiOiIxMDQifQ==	2016-09-02 18:29:45.863181-05
ckwf1gjyh50vbxsv7xe2r87f78kmp1rs	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-01 18:15:16.607522-05
hr78szune04c5z5ghn84xeafxgbxv6ee	ZTVmMGIyODM0ZTJiYzM5NTRjZjFiMGZiMDhlZmUyZGJjODc1ZjhiODp7Il9hdXRoX3VzZXJfaWQiOiI1IiwiX2F1dGhfdXNlcl9oYXNoIjoiZDg1Yjk4ZWJjZmMzYzk4Y2MzZjBiNDI2MTA0MTAwMjA5MTI3Yjc3OSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-01 18:44:09.613914-05
6cy1t717q8xww2v2rgxyd3y7znezczzw	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-09-02 16:30:21.926085-05
7reytvxdqmsnxztq5svze4aq315g0rzk	YzM5YTAwNjVkZTc3MWM5YjUwMzZhOTc1ZGZjYzEyZWE0MWY5NjcxNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImI2N2FlZDY5NzkyZmJkNzQ4MzFjMjNjZmUwMjRhM2FlZTIxZjIzYzAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3NyJ9	2016-08-31 12:04:43.826151-05
nxu8ddz6aop80giv964xm0akuv5woowq	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 16:58:58.675946-05
rlf7ts34sdgnmtbxok46jxigdn9ha8qr	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-31 18:18:43.016869-05
3r39z2011zpzr3m6xxfcfu2m0is9o6gy	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-01 02:14:32.58518-05
nrwu7nwlfns4dt3rs06ejuiokhzbjfsf	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-09-01 10:19:50.154974-05
hkgnkao0u4kvasel2dm39ndpeu033r3g	YjA1NTdkNjZmMjdlMTE1YTZhZTViMDc0NDljODkxY2ZkNDllNjBkZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDkzYzIyYzJiYWVmYzkzYzBmZDcwMDdjMTQzZWNlZTk1NmY5ODBiOSIsIl9hdXRoX3VzZXJfaWQiOiIxMDYifQ==	2016-09-02 18:29:46.013287-05
665weib6oikmiymba7zsnlbxtuzdh5mm	NDZhOTA2ZTMyY2YzZGNhNTkyNjlkNGRjYjNmZDljMjZlNDU4NjQ5Mzp7ImZhY2Vib29rX3N0YXRlIjoiTjlibTNkTkFrbHJZRDNha0N2MVJkSGZoYUZlQTRoUG8ifQ==	2016-09-06 19:03:15.174398-05
uqedk86r1ijfvsdyyvwlic9vijeo3b0t	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-05 10:12:06.547433-05
v4ik1qw02rrcnt13os1z3pf3s86m2rpo	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-08 09:50:30.174827-05
dgo8pouy655iutpgyoh4amtimyrw3c49	Y2I1Yzc3OTczMGYyZDZmNDI5NDE0NGE1NmEzZWQ3MzVlYTNkNjc1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTVlZThmMTk0YjliMWJhNGIzZmYzM2RmNDJjOTNhZWMxMWFkOTE2MCIsIl9hdXRoX3VzZXJfaWQiOiIxMjkifQ==	2016-09-06 18:54:58.061722-05
q3bh6p3c5g9viqy3mzya8uj6d7uk5dwt	MTc4NmFiM2QyMGU1NjE2NGFmNThlMGQ2ZmE1MDQ1MTk4ZTQzOWNlOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDg0YjNhNjQ2NDA4ZmVmYmFkZDRlNjMyMDAzYTI1NzRmNjY1YzAxNCIsIl9hdXRoX3VzZXJfaWQiOiIxMDIifQ==	2016-09-02 18:29:46.171515-05
3vlxpl0rtbzspofqym702ea5qggz80sx	MDk4YTIwMjgwNjM0MTcxMDdlMzBhNTVlNTYzNzU4NThlNDk1MTEwYjp7ImZhY2Vib29rX3N0YXRlIjoiWTk1R0xEZnlFRjNTQmZ3Z3VlSXBuSFpxdHNZWUF2VTkifQ==	2016-09-03 20:05:58.082689-05
feomms5m9yesmxpbq2y0x3op9l34i7em	OTkxZDAyOWM4MWNlYjAzNmE5YTkzY2Q2N2M5NGEzMzI4YWRmNTNiNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjY2ZDdmMDZjZTY0OWFlODdmMGU3Y2VjZTA5NTIxMTRiMzM4NGUxYyIsIl9hdXRoX3VzZXJfaWQiOiIxNTEifQ==	2016-09-06 20:25:49.401171-05
jab21frgprr3b4xz283s61zhgwkxlkb0	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-08 09:59:29.866591-05
alb839nnwvgyh65adscyn0jmeby0yv2i	NTIxM2EzYjg2M2NlYTI0NWIxZTBjNWE0ZmM5MjdmY2RmMDhiN2VlMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDQxZmFhYjU5ZWI2ZjAyMzQ4MTgwMzg5OTgzZTEzY2NlNDViNGZiZSIsIl9hdXRoX3VzZXJfaWQiOiIxNDIifQ==	2016-09-06 18:54:58.286712-05
zqfvyihoo93top4r866k46ry51ei06sp	N2UxMWJkZTlhNGFhMmZiMjE0NjRlMDBlMGViNDVjODdlYzllZjYzOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzlhZjNkMmI1ZTllNDM4YmYzNjJlNjJiZjkxMDY3YjJlMWJjYjhiNSIsIl9hdXRoX3VzZXJfaWQiOiIxMTcifQ==	2016-09-02 18:29:46.563271-05
51ozbtlv7mq1a40w6jlg57utze3mdvib	NGI0NTg5OWZhNGQ1ZmJjMTY4NGMxYWI0YTU2ZWI5OGQzNGI3MjVjNzp7ImZhY2Vib29rX3N0YXRlIjoiMU9KajYwcU9HaVNEWG1PNGFMaFFEeXpNbEtGMnZwTmwifQ==	2016-09-03 20:18:04.816536-05
m37vpxbcm89jjuss9dd9i4b63i3z61fd	NGJjN2NiOTZmYjllNDgwNTJlNjI1NGQ2NWQ3M2YxNTMzMDFmYjg0Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTQwMmZlMTNlZjA1ODVjMDIyNzYyN2Q2ZGZjOTNlYzg0MWMzNzgwMSIsIl9hdXRoX3VzZXJfaWQiOiI2MCJ9	2016-09-06 20:30:49.375804-05
mgt1fedutyri669t04fgevzx40wjgkfa	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-08 10:11:48.944357-05
85o9i6e0stqz1tdl2i239u3j0k44j7iz	ZGVmMDRlNDgzOTRhYjA3OTQ2NjdiNzNmNDk1M2MxNTM0MWM4MDQ3Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjliNjE4MjdiOTdjOGY5NjAzMmRiZWZkMzJmZmU2NjcyNWNhMzQwNyIsIl9hdXRoX3VzZXJfaWQiOiIxNDMifQ==	2016-09-06 18:54:58.428456-05
7g7k068l0ydt9e58yj2gvr2c7rc87x6i	NTM5MGM1N2E3Y2E3YjM1ZjdmNWIxMWQzNDliY2FmNjU0ZWFjMTYwNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTQ2ODAwNjE0Y2ZhOWUxNDA4ZDgyNzk0NGFlYzY3MGU2OGM5YTU1OSIsIl9hdXRoX3VzZXJfaWQiOiIxMTgifQ==	2016-09-02 18:29:46.746705-05
5xyalabbet5wkznetm2v953nsp027392	NWFlNmE5M2VmZmRiNTBlYTBlM2QxNDEwNjE5ZTA3YWJhYmU2OGYyNDp7ImZhY2Vib29rX3N0YXRlIjoiNmg3V0JtWE84bTJOZ2tMS1lCdGRmNmVVNVRaU2JYQUYifQ==	2016-09-03 20:20:58.980465-05
lxw2ge56tqznnp4ltpf7jz6wdw9rwez2	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-06 20:35:28.920897-05
duv2dhi01owc6zq7b2pcfubef3t7br9j	YTNmNjc2NDRhMTBmOWZlMWUxYTUyYmViZDYyNmVlNTBkN2RhOTkxMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGYyOGEyYzU2ZTM3NzNkMDRmOWFjZDY4ZWQ2OTIyNDljODEyMjEzOCIsIl9hdXRoX3VzZXJfaWQiOiIxNDQifQ==	2016-09-06 18:54:59.078232-05
746qzbj3nb390q4ip02jmvloluzwdswy	MWFmODY3YTVlYmRiOTE5OWE3OTYyMjI0NzlmOGYyMTNjOTI1YTgyZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjhlOThkMjAxNDJlZTc3YjZkNzFhOTQyOWNhYzFkNGMwNjdjMzM3YyIsIl9hdXRoX3VzZXJfaWQiOiIxMTkifQ==	2016-09-02 18:29:47.113744-05
syge4jl14gr9z1nc4ayvmlqs7wd8kh2t	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-09-06 20:43:14.80411-05
lqr75upy3kt7tvo4rw87xukql95ns8gt	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-09-06 17:57:39.148673-05
5v2htvk9dkelt5uzti74dv18fcuub421	NWFjYTBlYWY1ZWFkZDMyMDg3MzUzYWQ4ODRlZDIyMmM1YmI0YjUwMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjVmMTk2YWE0ZjIxZDBmNzk4M2YxMGQxMDQ0YjYwODlkNTM0OGRmMiIsIl9hdXRoX3VzZXJfaWQiOiIxNDUifQ==	2016-09-06 18:54:59.287179-05
nndf7rtjximflule6izykuky05bnit0v	NDY1OTMzN2U1Y2VmYmYxZjFjMTI5MTE1ZmMwMzU1Nzk2ZWY1YzI0OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYmNlYjQyMzJlMjRjNTA2MDA4MjZlMTA5ZDgzNjU1MjJmNWEzYTdlMCIsIl9hdXRoX3VzZXJfaWQiOiIxMTMifQ==	2016-09-02 18:29:47.947014-05
egiz9ou14yqxnol7qxquvsuxzsl8d1wf	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-09-06 17:57:58.450653-05
7lavnfzvvk9lqkoz0ntvrkal2ioh0wax	YjdmMmJkZGE5MmQ1MzlhM2U5ZDkyMjdkZjUzNzJjMmZmZjc5YTJhYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNGRmMzJlZmQ1YmQzZjJiZTQ5OTYyOWRjZjdiNDQ3MjA5ZDQ2ZmRiYiIsIl9hdXRoX3VzZXJfaWQiOiIxNDYifQ==	2016-09-06 18:54:59.478596-05
afp3adedu01sen45o1fhls1eqjbb4c8e	MTE5ZjgxMWJjYzMyODdhZDQ3NjAwZDU2OGY3YmQ4MzRkODk3YmZmZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzI5ZmMxOTVkYjU2ZmNmMmM3NjNmNzllMzFiZTQ2Y2YwNGY3M2JmMiIsIl9hdXRoX3VzZXJfaWQiOiIxMTQifQ==	2016-09-02 18:29:48.088235-05
4th428jl9zjhuvsxxkjfrlpr2txhjqwc	YTNhZGY3Mzk3NGNkMmE4MDc2ZjUyNDg4ZWQ5MTE0OWNhZDZhNDZkYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDY1NjJkNmRmNzg5ZGVhZDgwMDczNzk3OTM1M2IwYmU1MjJjMzgzMCIsIl9hdXRoX3VzZXJfaWQiOiIxNDcifQ==	2016-09-06 18:54:59.628403-05
knr452yz3nks3shxzebzd7kxacexdxtw	ZDdjZjdjNjljYmYzODExNjMxMGY2MDUyZGQxMjhhZmQ3MThiZGNjYzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTc3ZWE3YWY5NjdmOGJlZmI1YzFjOTk3OThhZmRlN2ExODdlZjAyZiIsIl9hdXRoX3VzZXJfaWQiOiIxMjMifQ==	2016-09-02 18:29:48.613789-05
ydjw271xxh5lfwhhfhuj6hv7htuvfldr	N2I5Yjc3MTI1Y2E1ZjU1ZTcwNmU4ODdjOWNhN2RkM2Q1NGJhYmYwYTp7ImZhY2Vib29rX3N0YXRlIjoia2oxVXlGcFZNUGhFdXIxMDZYUG5IdU5SWm0xajBuVksifQ==	2016-09-04 11:56:55.34425-05
zvw9yb1e5ykjk5bz7bunuy2uw7uvqac5	N2I4YjgyNTA3YTQ1ZDBkZTY3OGE3NDk1MTczNjE5ZGFjYzgyZWFlOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTE5YjgxOTIzZGUyODliODQwOGI3OGRjNGZiMjIzYjg0Mjc4OGY1NSIsIl9hdXRoX3VzZXJfaWQiOiIxNDgifQ==	2016-09-06 18:55:00.312142-05
7a2mmubyaj5w1hav3vrt6sbzyiv7eqk4	Mjg3MjhkZDJkOGE2MjA2NzdhOTVhMmM3NzY2OWM2ZmIwNDc2YjdlMTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjg5YzY5Yzk4NTdhZTViMWJlZDVmNDdjNjNhZTUwOWVhZWFiZDkwOSIsIl9hdXRoX3VzZXJfaWQiOiIxMjQifQ==	2016-09-02 18:29:48.788677-05
4iuew0n590o4xtemt7abuqccsmk68fez	OTNkNzBjZDM2MTFiZDY4Zjc1NWE1M2Q1OGE3YzgwYjRjYTk0NWExYTp7ImZhY2Vib29rX3N0YXRlIjoiT0RFSWRNdTkyck52RmxZd0F0WTlxV1dzR1I1U1lHN0gifQ==	2016-09-04 12:39:40.776491-05
b70etfy3gddynpriq1k2lzlodhl00gz8	NWFjYTBlYWY1ZWFkZDMyMDg3MzUzYWQ4ODRlZDIyMmM1YmI0YjUwMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjVmMTk2YWE0ZjIxZDBmNzk4M2YxMGQxMDQ0YjYwODlkNTM0OGRmMiIsIl9hdXRoX3VzZXJfaWQiOiIxNDUifQ==	2016-09-06 18:55:00.886761-05
f3vgy27la22k4j617agn2swbr6ptkknf	MjhlNGQ4ZjdmMTQ5MmMzM2Y1MTViYTlhMjZmNTFlN2EwYWVhZTg5Mjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2U4NmM0NzU3ODM2MWYwNzY3MjUyM2FkYWNkYjc5ZjdiMTYwYzIwYyIsIl9hdXRoX3VzZXJfaWQiOiIxMjUifQ==	2016-09-02 18:29:48.963404-05
p8jnckmoepknczmh0b86id2p7mlnznvt	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-06 18:47:13.837492-05
3dw1d8lnsl4k9tednj6jz89v5kwqs2h2	YTNhZGY3Mzk3NGNkMmE4MDc2ZjUyNDg4ZWQ5MTE0OWNhZDZhNDZkYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDY1NjJkNmRmNzg5ZGVhZDgwMDczNzk3OTM1M2IwYmU1MjJjMzgzMCIsIl9hdXRoX3VzZXJfaWQiOiIxNDcifQ==	2016-09-06 18:55:01.028593-05
0ijxomeuhjrwytci7wjcr6xkgllk1jzs	MDRkYTkzNTc1YjE0MWY0NDFkOTRmODk4OGZmZTE1NDQ4ZjY5NThlNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODQ1OGMxNWI5NTJiN2FmOWQ1Y2QxNTFmN2VmODY5OWMwMTgzMDUzNiIsIl9hdXRoX3VzZXJfaWQiOiIxMjcifQ==	2016-09-02 18:29:49.263448-05
w6na4gm36e3cfpfpgu7wlhsjpnizkdag	ZWJmMjc5MzE4OWNhYjFhYjEyYTZmZjFjZDdlNTc1N2ZjNjBhY2I2Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTBhNzcxOGEyYzFjMGQxMTcwZmJmMzliNTYzNWE1YTVhNGZkODQ1OCIsIl9hdXRoX3VzZXJfaWQiOiIxNTQifQ==	2016-09-07 11:36:54.348974-05
gnvj3ie08brh75dz4h78usi4vcc05t5l	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-06 18:47:53.563552-05
z0o5qpwhj7urhss9osvkk06o4d89oo6t	N2I3OGNmM2JjYzMzZWY1MTMwODRjMWViNzJhOGVjMmQzN2NiNTUyNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzJkNzJjMmZjODcyNGU2YjE4MjhkYzM1ZDY2MTFhMzE2ZGY3NjdlZCIsIl9hdXRoX3VzZXJfaWQiOiIxMjgifQ==	2016-09-02 18:29:49.422157-05
v65atrm45hsfz1izktgrxxjelywjcmdx	Y2I1Yzc3OTczMGYyZDZmNDI5NDE0NGE1NmEzZWQ3MzVlYTNkNjc1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTVlZThmMTk0YjliMWJhNGIzZmYzM2RmNDJjOTNhZWMxMWFkOTE2MCIsIl9hdXRoX3VzZXJfaWQiOiIxMjkifQ==	2016-09-02 18:29:49.622165-05
77f5zkfdesn1ldlo9hip0bj6lnmdq4gn	NTIxM2EzYjg2M2NlYTI0NWIxZTBjNWE0ZmM5MjdmY2RmMDhiN2VlMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDQxZmFhYjU5ZWI2ZjAyMzQ4MTgwMzg5OTgzZTEzY2NlNDViNGZiZSIsIl9hdXRoX3VzZXJfaWQiOiIxNDIifQ==	2016-09-02 18:29:52.338734-05
ny2ai5gwef56q186ceca9n8k0je94ob4	NmRiMmU5NDg4N2JlY2QyZGMzOTgzZTFlYWExNTA0MDA0Yzc0NWZmYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTA1YThiNjE3ZDhkNDM5MjIwZGJjZWY4Y2Y5ZjE1NzgzYWZhMmQwOSIsIl9hdXRoX3VzZXJfaWQiOiI5NyIsImZhY2Vib29rX3N0YXRlIjoicXNXaE5hejg4U09WOU1JZzhBQklvOVFBa21XZzV0M0cifQ==	2016-09-06 18:54:52.260692-05
a3l3u2kj92r0zsu1sv16o3wii74jeyxg	ZGVmMDRlNDgzOTRhYjA3OTQ2NjdiNzNmNDk1M2MxNTM0MWM4MDQ3Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjliNjE4MjdiOTdjOGY5NjAzMmRiZWZkMzJmZmU2NjcyNWNhMzQwNyIsIl9hdXRoX3VzZXJfaWQiOiIxNDMifQ==	2016-09-02 18:29:52.530171-05
b1ujykbdomwuqexp5hbwy01paww535j2	MDhjMDY0MDcxODQwYjE1ZDY2NmU4ZjFlNGU5YjUyMzYzZTI3NGM4OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZGFhYWE2OWYxYjhjZDY5MzYzZWY2YTY5MjA5YWZkNzc1Y2RiZTZiNiIsIl9hdXRoX3VzZXJfaWQiOiIxMDkifQ==	2016-09-06 18:54:53.744365-05
sub753a212xe48bx5j4dsof7ys9sgdrk	YTNmNjc2NDRhMTBmOWZlMWUxYTUyYmViZDYyNmVlNTBkN2RhOTkxMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGYyOGEyYzU2ZTM3NzNkMDRmOWFjZDY4ZWQ2OTIyNDljODEyMjEzOCIsIl9hdXRoX3VzZXJfaWQiOiIxNDQifQ==	2016-09-02 18:29:54.072148-05
dxvpa9mokl6c0ibkvk7eo3ygb8ag41gl	MjNlZGNhYjUwMDNiYTk5Yzk2OGJiMTg2NWEyZWVkNmM3NTQwNjkxMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzRjMmJjN2ViM2Q0NzE5YjEyMDEwNDllMzE5MjViNDU0OTRhY2E0MiIsIl9hdXRoX3VzZXJfaWQiOiIxMzYifQ==	2016-09-06 18:54:54.644177-05
r1zrolblel4vl1bbrz3m3frxwthlqz1u	NWFjYTBlYWY1ZWFkZDMyMDg3MzUzYWQ4ODRlZDIyMmM1YmI0YjUwMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjVmMTk2YWE0ZjIxZDBmNzk4M2YxMGQxMDQ0YjYwODlkNTM0OGRmMiIsIl9hdXRoX3VzZXJfaWQiOiIxNDUifQ==	2016-09-02 18:29:54.354822-05
1m7extyklz1ywerr544bokzlztubnr5v	MTZmNTAyYWE4NTlkNzcxYjM0NmRhODkwYWUwY2RiMjViODE0NjBjODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTc2ODI1YjVmMDc3N2MxN2Y5MzJmZDk5NjljODYyYTk2YWExMDA1MiIsIl9hdXRoX3VzZXJfaWQiOiIxMTIifQ==	2016-09-06 18:54:55.035888-05
o3tu6p41ehrfuqibhccs6pgkom20xaz0	YjdmMmJkZGE5MmQ1MzlhM2U5ZDkyMjdkZjUzNzJjMmZmZjc5YTJhYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNGRmMzJlZmQ1YmQzZjJiZTQ5OTYyOWRjZjdiNDQ3MjA5ZDQ2ZmRiYiIsIl9hdXRoX3VzZXJfaWQiOiIxNDYifQ==	2016-09-02 18:29:54.530455-05
j4x3gnte1k18er193zfsjpnertqrvo75	MzkzYmZhODM1ZjQ3Zjc3ZjBkMDQ0NTBiZGIxZGZiY2JlOWFhMGI5Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMmFkZGIwYTM0M2NiMDFjYmJkNTVkOWUwYzlkNjQzOGIxMmVlOTM3NCIsIl9hdXRoX3VzZXJfaWQiOiIxMDMifQ==	2016-09-06 18:54:55.244525-05
j2oahi2l4bgm54fcqywm6twq4rnh1zmw	YTNhZGY3Mzk3NGNkMmE4MDc2ZjUyNDg4ZWQ5MTE0OWNhZDZhNDZkYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDY1NjJkNmRmNzg5ZGVhZDgwMDczNzk3OTM1M2IwYmU1MjJjMzgzMCIsIl9hdXRoX3VzZXJfaWQiOiIxNDcifQ==	2016-09-02 18:29:54.746844-05
162oz6uw7o4zjn3kyiqzxvylzj2g34fs	YjY2YjgzMmIyZTBkMDZmNzhlNmY1YjE5MThmNWE4ZmM5MDk2NmM5Mjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDAxZGExYzMwYzQwOTExN2U4NmZmMDE4MTZkMjJiZTZjODdiM2RmYSIsIl9hdXRoX3VzZXJfaWQiOiIxMDQifQ==	2016-09-06 18:54:55.452737-05
vr6dp23mivfcwdskt36mhignooz0p1b9	N2I4YjgyNTA3YTQ1ZDBkZTY3OGE3NDk1MTczNjE5ZGFjYzgyZWFlOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTE5YjgxOTIzZGUyODliODQwOGI3OGRjNGZiMjIzYjg0Mjc4OGY1NSIsIl9hdXRoX3VzZXJfaWQiOiIxNDgifQ==	2016-09-02 18:29:55.388368-05
vtagoxfhu63psznqc2r9880ts4hue9pf	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-09-07 12:20:30.803346-05
00993dixyyyz6xpta6slb8r04uy45d4y	MTc4NmFiM2QyMGU1NjE2NGFmNThlMGQ2ZmE1MDQ1MTk4ZTQzOWNlOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDg0YjNhNjQ2NDA4ZmVmYmFkZDRlNjMyMDAzYTI1NzRmNjY1YzAxNCIsIl9hdXRoX3VzZXJfaWQiOiIxMDIifQ==	2016-09-06 18:54:55.619473-05
dy3z8c0hulal4bsjbkx7du8bh4ruockj	NWFjYTBlYWY1ZWFkZDMyMDg3MzUzYWQ4ODRlZDIyMmM1YmI0YjUwMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjVmMTk2YWE0ZjIxZDBmNzk4M2YxMGQxMDQ0YjYwODlkNTM0OGRmMiIsIl9hdXRoX3VzZXJfaWQiOiIxNDUifQ==	2016-09-02 18:29:56.706836-05
gizzigayc7f9gvpvtx6i32z3giwe25yg	YjA1NTdkNjZmMjdlMTE1YTZhZTViMDc0NDljODkxY2ZkNDllNjBkZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDkzYzIyYzJiYWVmYzkzYzBmZDcwMDdjMTQzZWNlZTk1NmY5ODBiOSIsIl9hdXRoX3VzZXJfaWQiOiIxMDYifQ==	2016-09-06 18:54:55.628169-05
9wyhngwqw09yyh5r906h2xu0shfb1pi2	YTNhZGY3Mzk3NGNkMmE4MDc2ZjUyNDg4ZWQ5MTE0OWNhZDZhNDZkYTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDY1NjJkNmRmNzg5ZGVhZDgwMDczNzk3OTM1M2IwYmU1MjJjMzgzMCIsIl9hdXRoX3VzZXJfaWQiOiIxNDcifQ==	2016-09-02 18:29:56.89023-05
l3go64lhv5t14lmh20g4tgoovjd0tg5w	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-07 12:53:47.851757-05
09j1itxigbtv9yunlim6630vpue9s3zn	N2UxMWJkZTlhNGFhMmZiMjE0NjRlMDBlMGViNDVjODdlYzllZjYzOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzlhZjNkMmI1ZTllNDM4YmYzNjJlNjJiZjkxMDY3YjJlMWJjYjhiNSIsIl9hdXRoX3VzZXJfaWQiOiIxMTcifQ==	2016-09-06 18:54:56.327858-05
770wcan4nkeamdf51w0i3o84hom31a1a	MjZmOTg1MzIxMTNjODllMjhhYmM4ZGIzNDMzMjI0ZDNlZjY4ODZjMDp7ImZhY2Vib29rX3N0YXRlIjoiRTV3RWdTY1BHMnB0STVUMmVzN1hNZE90bXM2cXp6R20ifQ==	2016-09-02 18:44:25.919119-05
3h3086e1d4m3daifbxcnwnze3f7hcsd0	ODcxODA1NTFjMzNjMzhlMGYzOTk5NzA2ZDdiZjFjZmYwMjFkMTMyZjp7ImZhY2Vib29rX3N0YXRlIjoibDkzc2pjOTlVOEQ0VVkxbEd0MlVsUHdZWGlreFBmM2EifQ==	2016-09-02 19:11:59.156192-05
arc53k7acmvb6n8xpo599fcukvqz2w2s	NTM5MGM1N2E3Y2E3YjM1ZjdmNWIxMWQzNDliY2FmNjU0ZWFjMTYwNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTQ2ODAwNjE0Y2ZhOWUxNDA4ZDgyNzk0NGFlYzY3MGU2OGM5YTU1OSIsIl9hdXRoX3VzZXJfaWQiOiIxMTgifQ==	2016-09-06 18:54:56.436671-05
kjnvm9usln2t4d1mkxaeokk5gl85tsjq	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-07 13:42:39.049738-05
fgfgvpyo531ozo951zquvqb4u9poyi8s	MWFmODY3YTVlYmRiOTE5OWE3OTYyMjI0NzlmOGYyMTNjOTI1YTgyZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjhlOThkMjAxNDJlZTc3YjZkNzFhOTQyOWNhYzFkNGMwNjdjMzM3YyIsIl9hdXRoX3VzZXJfaWQiOiIxMTkifQ==	2016-09-06 18:54:56.994813-05
qi169wwl88yeadyna2903542qnr3funf	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-07 17:15:01.521957-05
f0en0sevew8efdrhej1i821xgkkbpokp	NDY1OTMzN2U1Y2VmYmYxZjFjMTI5MTE1ZmMwMzU1Nzk2ZWY1YzI0OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYmNlYjQyMzJlMjRjNTA2MDA4MjZlMTA5ZDgzNjU1MjJmNWEzYTdlMCIsIl9hdXRoX3VzZXJfaWQiOiIxMTMifQ==	2016-09-06 18:54:57.278104-05
234935w0kr0vxndppioa6ur3g2m9151g	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-09-04 21:22:24.69083-05
rvaqr62mcz8endsy1wjxlts0a1iw1b41	MGRlNGY3ZmY0OGIxOGE2NzMwZTlkN2VmMzZlOWY4ZTJkZDcxODZjNjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0IiwiZmFjZWJvb2tfc3RhdGUiOiJsaUJ1ajVIemtqWWNOcVI4V3NXMnczS1JxdWxOQnI1ayJ9	2016-09-07 17:15:25.583852-05
axhk6al5p8r0nupxddsj7a8jmudy5j9i	ZDdjZjdjNjljYmYzODExNjMxMGY2MDUyZGQxMjhhZmQ3MThiZGNjYzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTc3ZWE3YWY5NjdmOGJlZmI1YzFjOTk3OThhZmRlN2ExODdlZjAyZiIsIl9hdXRoX3VzZXJfaWQiOiIxMjMifQ==	2016-09-06 18:54:57.328354-05
p0t7m3prmcsu0s46fu20w2kam4yhvfwt	MTE5ZjgxMWJjYzMyODdhZDQ3NjAwZDU2OGY3YmQ4MzRkODk3YmZmZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzI5ZmMxOTVkYjU2ZmNmMmM3NjNmNzllMzFiZTQ2Y2YwNGY3M2JmMiIsIl9hdXRoX3VzZXJfaWQiOiIxMTQifQ==	2016-09-06 18:54:57.403291-05
p1pm37aobscryxxz4iri8gz4qxmmsfbx	N2QyMjhlYzMyMjI5ZDhmZTU0Y2Q0OTYyOTA0ZmZlZjlkZWUyYTI3ZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjg0MTQ4YjBhYjFlOWYxOTQ5MGI3MzkxOGE3YTc1OWM4NWU4YmJkZSIsIl9hdXRoX3VzZXJfaWQiOiI3OCJ9	2016-09-04 23:39:56.066182-05
4xvpjio92yp377vmzpzd5labskbndhpj	Mjg3MjhkZDJkOGE2MjA2NzdhOTVhMmM3NzY2OWM2ZmIwNDc2YjdlMTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjg5YzY5Yzk4NTdhZTViMWJlZDVmNDdjNjNhZTUwOWVhZWFiZDkwOSIsIl9hdXRoX3VzZXJfaWQiOiIxMjQifQ==	2016-09-06 18:54:57.453088-05
w4ggt1nn6upoamgnmnx5hceghduwlegl	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-09-04 23:41:30.884523-05
iyv1a34u4ujuim6srbedo1c5njz79wj4	MjhlNGQ4ZjdmMTQ5MmMzM2Y1MTViYTlhMjZmNTFlN2EwYWVhZTg5Mjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2U4NmM0NzU3ODM2MWYwNzY3MjUyM2FkYWNkYjc5ZjdiMTYwYzIwYyIsIl9hdXRoX3VzZXJfaWQiOiIxMjUifQ==	2016-09-06 18:54:57.569794-05
m7n3bspn6ax70r2bwa7xbn5ahgsuqgyy	ZGVhZWU4YWVhYzYyM2ZjNmY0OWRmMmJiNjg4MWRiNzRlNWVlZjAxZjp7ImZhY2Vib29rX3N0YXRlIjoiOFhIdVFYbm9SNkR0VEpBaHJxeGhPTDZTTUd3YWpPUmgifQ==	2016-09-03 12:30:41.248523-05
m6g18wn5my2sttytim5r0dyjfqm5km8p	MDRkYTkzNTc1YjE0MWY0NDFkOTRmODk4OGZmZTE1NDQ4ZjY5NThlNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODQ1OGMxNWI5NTJiN2FmOWQ1Y2QxNTFmN2VmODY5OWMwMTgzMDUzNiIsIl9hdXRoX3VzZXJfaWQiOiIxMjcifQ==	2016-09-06 18:54:57.795218-05
\.


--
-- Data for Name: social_auth_association; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_association (id, server_url, handle, secret, issued, lifetime, assoc_type) FROM stdin;
\.


--
-- Name: social_auth_association_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_association_id_seq', 1, false);


--
-- Data for Name: social_auth_code; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_code (id, email, code, verified) FROM stdin;
\.


--
-- Name: social_auth_code_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_code_id_seq', 1, false);


--
-- Data for Name: social_auth_nonce; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_nonce (id, server_url, "timestamp", salt) FROM stdin;
\.


--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_nonce_id_seq', 1, false);


--
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_usersocialauth (id, provider, uid, extra_data, user_id) FROM stdin;
\.


--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_usersocialauth_id_seq', 1, false);


--
-- Data for Name: spaapp_actividadcomercial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_actividadcomercial (id_actividad, tipo_actividad, detalles) FROM stdin;
1	Restaurante	aadadasa
\.


--
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_actividadcomercial_id_actividad_seq', 1, true);


--
-- Data for Name: spaapp_catalogoestado; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_catalogoestado (id_estado, estado) FROM stdin;
1	En Espera
2	En Proceso
3	Transportista en Espera
4	Enviado
5	Entregado
6	Problema de Proveedor
7	Problema en Entrega
8	Queja de Cliente
\.


--
-- Name: spaapp_catalogoestado_id_estado_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_catalogoestado_id_estado_seq', 8, true);


--
-- Data for Name: spaapp_categoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_categoria (id_categoria, categoria, detalles, ruta_foto) FROM stdin;
7	Pollos	Pollos a la braza	categorias/pollos.jpg
1	Pizza	dadadasdq	categorias/pizza-banner.jpg
2	Comida Asiática	dadadasd	categorias/banner_china.jpg
4	Comida Casera	dasasd	categorias/cocina_casera.jpg
5	Comida rápida	dasdasd	categorias/rapida.jpg
3	Heladería	sadadsa	categorias/helado.jpg
8	Bebidas	Bebidas	categorias/bebidas.jpeg
\.


--
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_categoria_id_categoria_seq', 8, true);


--
-- Data for Name: spaapp_ciudad; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_ciudad (id_ciudad, nombre, descripcion) FROM stdin;
1	Loja	dasda
2	Loja	asdasdas
\.


--
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ciudad_id_ciudad_seq', 2, true);


--
-- Data for Name: spaapp_cliente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_cliente (id_cliente, fechainicio_suscripcion, fecha_pago, detalles_adicionales, pedidos_activos, id_perfil_id) FROM stdin;
1	2016-07-29	2016-07-29	nada	t	3
\.


--
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_cliente_id_cliente_seq', 1, false);


--
-- Data for Name: spaapp_contrato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_contrato (id_contrato, codigo, detalles, vigencia, fecha_inicio, "fechaFin", valor, ruta_contrato, id_proveedor_id, id_sucursal_id) FROM stdin;
\.


--
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_contrato_id_contrato_seq', 1, false);


--
-- Data for Name: spaapp_coordenadas; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_coordenadas (id_coordenadas, latitud, longitud, fecha, estado, id_perfil_id) FROM stdin;
\.


--
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_coordenadas_id_coordenadas_seq', 1, false);


--
-- Data for Name: spaapp_detallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_detallepedido (id_detalle, valor, cantidad_solicitada, id_pedido_id, id_plato_id, tamanio_id) FROM stdin;
259	15	1	201	20	30
221	105	7	174	25	61
260	15	1	202	20	30
261	28	2	202	22	32
262	12.5600000000000005	1	203	31	84
227	91.1999999999999886	6	176	27	70
228	42	3	176	22	32
229	183.120000000000005	12	177	28	73
263	30	10	204	4	86
232	200.960000000000008	16	177	28	72
233	180	15	178	26	80
234	12	1	179	26	80
235	30	2	180	20	30
236	56	4	180	22	32
237	14	1	181	22	32
238	37.6799999999999997	3	182	31	84
239	15	1	182	25	61
241	12.5600000000000005	1	183	31	84
242	75.3599999999999994	6	184	31	84
264	4.5	1	205	30	83
265	15	1	205	25	61
266	13.5	3	206	30	83
267	30	3	206	25	62
268	61.0399999999999991	4	206	28	73
217	120	8	172	25	61
269	13.5	3	207	30	83
243	37.6799999999999997	3	185	31	84
244	62.8000000000000043	5	186	31	84
245	12.5600000000000005	1	187	31	84
246	12.5600000000000005	1	188	31	84
270	20	2	208	32	102
271	30.6000000000000014	2	208	32	101
272	12.5600000000000005	1	209	31	84
273	56	4	210	22	32
274	45	3	211	20	30
275	15	1	212	20	30
276	15	1	213	20	30
277	4.5	1	214	30	83
278	15	1	215	20	30
279	9	2	216	30	83
280	15	1	217	20	30
281	13.5	3	218	30	83
\.


--
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_detallepedido_id_detalle_seq', 281, true);


--
-- Data for Name: spaapp_direccion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) FROM stdin;
2	sucre	miguel riofrío		1312312	12312312	1312321	3
3	Vince	Ambato		-3.9870951803235783	-79.20502334833145	coca-cola	4
4	Latacunga	Bolivar		-3.9870951803235783	-79.20502334833145	coca-cola	6
7	Princiapl	Secundaria	nn	-4.031867404538332	-79.20914054178733	referencia	1
8	we	w	sdf	-4.02147042659097	-79.21773362438964	we	36
55	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
56	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
16	colombia	filipinas	15-36	-4.029060609640873	-79.21910691540485	frente a casa	67
17	colombia2	filipinas2	15-36	-3.9747766385787027	-79.22028708737186	frente a casa	67
18	colombia3	filipinas2	15-36	-3.9747766385787027	-79.22028708737186	frente a casa	67
19	dad	dasd	666	-4.014706469127719	-79.19867921154801	dasd	67
20	virginia	floresta	45	-4.0237041356312115	-79.21035218518112	flores	67
21	virginia	floresta	45	-4.0237041356312115	-79.21035218518112	flores	67
22	America	Vive	15-65	-4.0237333581737325	-79.21095300000009	Frente a canchas	67
57	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
58	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
59	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
60	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
61	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
62	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
63	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
23	loli	loli	888	-4.019073080996123	-79.20408654492148	loli	67
64	lipo	lipo	45	-4.001007143174379	-79.20897889416504	lipo	67
65	blasto	blasto	456	-4.029775462128773	-79.21713280957069	blasto	67
66	iguana	iguana	12	-4.0183025041294895	-79.21369958203135	fdsf	67
67	pedrito	pedrito	456	-4.0161620090135335	-79.21498704235839	pe	67
68	pedrito2	pedrito2	456	-3.941820070710962	-79.2721931962278	pe	67
24	fds	fs	77	-4.0269500471768795	-79.20297074597171	fsd	10
25	fds	fs	77	-4.0269500471768795	-79.20297074597171	fsd	10
26	locoNico	fs	777	-3.95776714658344	-79.32545113842802	fsd	10
27	palacios	da	14	-4.022528985642917	-79.2080347565916	das	67
28	palacios888	da	14	-4.029264771527588	-79.22382760327115	das	67
29	rosales	da	14	-4.001951999755964	-79.16305947582971	das	67
30	fdsgsd	sgdfsd	1	-4.009483628180423	-79.21644616406263	sdgfsdg	36
31	dahgdas	asdgjasd	25	-4.016852319301914	-79.21168256085177	agsdjgaj	36
34	op	popo	128	-4.020731238119841	-79.21490121167021	huh	67
38	ssssssss	sssssss	14	-4.024486076056955	-79.21369958203135	ss	67
40	Principal	La que cruza	15-63	-4.011754340333924	-79.19663536827844	Casa Blanca	90
42	monetero	zambrano	777	-4.0099973517213146	-79.21284127514699	Esquina	67
43	Pinto	Pinto	45	-4.015276858293662	-79.21618867199723	poi	67
44	fd	fsd	41	-3.998010385054946	-79.167093518188	oi	67
45	fdppppppppppppppp	fsd	4141	-3.998010385054946	-79.167093518188	oi	67
46	ko	cs	14	-4.024358716153307	-79.20606065075654	14	67
47	ko	cs	14	-4.024358716153307	-79.20606065075654	14	67
48	ko	cs	14	-4.024358716153307	-79.20606065075654	14	67
49	ko	cs	14	-4.024358716153307	-79.20606065075654	14	67
50	ko	cs	14	-4.024358716153307	-79.20606065075654	op	67
51	ko	cs	14	-4.024358716153307	-79.20606065075654	14	67
52	ko	cs	14	-4.024358716153307	-79.20606065075654	op	67
53	ko	cs	14	-4.024358716153307	-79.20606065075654	op	67
54	rococo	rococo	256	-3.9467213190922603	-79.21703625004615	op	67
75	avenida los Rios	cuxibamba	12-78	-3.9866100999999996	-79.1968898	Esquina	107
1	Av. Universitaria	Mercadillo	25-65	-3.9910791505578893	-79.20707463774131	Frente al León	2
76	Avenidaaaaaa	Secundariaaa	12-58	-3.9866106	-79.1968886	Esquina	108
77	prendho	prendho	prend	-3.9825732	-79.19434230000002	prendho	36
78	Loxa	dance	N/A	-3.9865974	-79.1968936	esq	109
79	paris	av.los paltas	58	-3.9866092	-79.1968663	utpl	112
80	Machala	Bailon	12-65	-3.9866170999999997	-79.1968871	Esq	109
81	preeeendho	prendho	1234	-3.9865985	-79.1968863	preeendho	113
82	Ruiseniores	Gabiotas	12-45	-3.9866008	-79.1968794	Esq	109
86	po	po	12345	-3.9865955000000004	-79.1968446	po	185
83	preeeeendho	preeeeendho	12345	-3.9865948	-79.1968869	preendho	116
87	Marcelino	Paris	utpl	-3.9865676000000003	-79.1968836	utpl	186
88	MErcadiilo	MErcadillo	N/A	-4.015370	-79.202242	En el Hub	187
84	Colmbia	Filipinas	14-58	-4.0120312	-79.2090151	Esq	109
85	Paris	Loquis	N/A	-3.9865918000000002	-79.1968444	N/A	181
89	Santa	Sasnta	12-45	-4.015664	-79.204291	Esquina	188
\.


--
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_direccion_id_direccion_seq', 89, true);


--
-- Data for Name: spaapp_horariotransportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_horariotransportista (id_horario, dias, horas, id_transportista_id) FROM stdin;
\.


--
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_horariotransportista_id_horario_seq', 1, false);


--
-- Data for Name: spaapp_ingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_ingrediente (id_ingrediente, nombre, id_proveedor_id) FROM stdin;
3	Pollo	1
5	Carne de res	1
6	Arroz	1
7	Pescado	1
8	Cebolla	1
9	pimiento	1
10	zanahoria	1
15	tomate	1
16	culantro	1
17	helado	1
18	tallarin	1
19	chocolate	1
20	harina	1
21	camarón	1
22	huevo	1
23	champi	1
24	carne de chancho	1
25	mote	1
26	jamón	1
27	queso	1
28	pan	10
29	jamon	10
30	queso	10
31	maracuyá	11
\.


--
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ingrediente_id_ingrediente_seq', 31, true);


--
-- Data for Name: spaapp_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_menu (id_menu, id_proveedor_id) FROM stdin;
1	1
2	2
3	3
4	4
5	5
6	7
7	6
8	8
9	8
10	9
11	9
12	10
13	11
\.


--
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_menu_id_menu_seq', 13, true);


--
-- Data for Name: spaapp_modificardetallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_modificardetallepedido (id_modificardetallepedido, id_ingrediente_quitar_id, id_detallepedido_id) FROM stdin;
\.


--
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_modificardetallepedido_id_modificardetallepedido_seq', 2, true);


--
-- Data for Name: spaapp_pedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_pedido (id_pedido, fecha, observaciones, valor, tiempo, id_direccion_id, id_sucursal_id, id_tipo_pago_id, id_tipo_pedido_id, id_trasportista_id, usuario_id, id_estado_id, notificado) FROM stdin;
150	2016-08-11 17:08:20-05	Helado Mediano	3	15	30	1	1	1	1	15	1	f
187	2016-08-20 16:42:54-05	Sin Comentario	14.0600000000000005	\N	8	1	1	1	1	15	1	f
151	2016-08-11 20:08:04-05	gggg	23.5	\N	8	1	2	2	1	15	1	f
188	2016-08-21 10:13:15-05	"/><img src=x onerror=prompt(1)>\n"/><svg/onload=prompt(1)>	14.0600000000000005	\N	8	1	1	1	1	15	1	f
162	2016-08-12 13:04:57-05	Sin Comentario	13.5	\N	8	2	1	1	1	15	1	f
201	2016-08-21 17:06:07-05	Sin Comentario	20	\N	24	2	1	1	1	2	1	f
202	2016-08-21 17:06:45-05	Sin Comentario	48	\N	24	2	2	1	1	2	1	f
203	2016-08-21 18:40:55-05	pruebas	14.0600000000000005	2	84	1	1	1	1	78	2	f
204	2016-08-22 16:11:17-05	hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh	31.5	\N	78	1	1	1	1	78	1	f
167	2016-08-17 11:59:03-05	Caliente por favor	54.5	12	76	1	1	1	1	77	5	f
168	2016-08-18 09:26:25-05	Sin Comentario	10.5	\N	77	1	1	1	1	15	1	f
166	2016-08-17 11:34:31-05	con cola	86.5	2	75	1	1	1	1	76	5	f
170	2016-08-18 10:16:00-05	Sin Comentario	12.5	\N	78	1	1	1	1	78	1	f
149	2016-08-11 16:54:50-05	coment dad	24.5	5	8	1	2	1	1	15	5	f
172	2016-08-18 10:18:15-05	Calientito por faovor y rapido	194.5	15	78	1	1	1	1	78	5	f
169	2016-08-18 09:29:33-05	Sin Comentario	10.5	40	77	1	1	1	1	15	2	f
173	2016-08-18 10:25:01-05	Sin Comentario	37	7	78	1	1	1	1	78	5	f
175	2016-08-18 13:27:24-05	hoooooooola	60	1	81	1	1	1	1	82	5	f
176	2016-08-18 13:43:33-05	Caliente por favor	138.199999999999989	5	82	2	1	1	1	78	2	f
177	2016-08-19 06:34:43-05	quiero muchos huevos en mis pizzas	517.580000000000041	8	83	1	1	1	1	85	5	f
178	2016-08-19 06:57:54-05	opoooooooooooooooooooooooooooooooooooooooooo	181.5	\N	78	1	1	1	1	78	1	f
179	2016-08-19 11:45:53-05	Sin Comentario	13.5	\N	77	1	1	1	1	15	1	f
180	2016-08-19 12:00:50-05	Sin Comentario	91	\N	77	2	1	1	1	15	1	f
181	2016-08-19 12:03:40-05	Sin Comentario	19	\N	8	2	1	1	1	15	1	f
183	2016-08-20 03:13:31-05	Sin Comentario	14.0600000000000005	\N	8	1	1	1	1	15	1	f
171	2016-08-18 10:17:20-05	Sin Comentario	23.5	5	79	1	1	1	1	81	4	f
184	2016-08-20 09:45:20-05	Sin Comentario	76.8599999999999994	\N	77	1	1	1	1	15	1	f
185	2016-08-20 11:25:50-05	Sin Comentario	39.1799999999999997	\N	77	1	1	1	1	15	1	f
186	2016-08-20 11:26:37-05	Sin Comentario	64.3000000000000114	\N	77	1	1	1	1	15	1	f
182	2016-08-19 12:51:58-05	Sin Comentario	92.6800000000000068	2	77	1	1	2	1	15	2	f
174	2016-08-18 12:18:42-05	Con cola por favor	163	10	80	1	1	1	1	78	4	t
205	2016-08-23 12:55:46-05	345678	21	3	8	1	1	1	1	15	5	t
206	2016-08-23 13:00:25-05	Caliente por favor :D!	106.039999999999992	1	85	1	1	1	1	150	2	f
207	2016-08-23 13:03:28-05	Sin Comentario	14.75	\N	85	1	1	1	1	150	1	f
208	2016-08-23 13:15:07-05	La mejor pizza!!!! wiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii	51.8500000000000014	\N	85	1	1	1	1	150	1	f
209	2016-08-23 13:24:25-05	poko	12.5600000000000005	\N	85	1	1	2	1	150	1	f
210	2016-08-23 15:41:08-05	Sin Comentario	61	\N	30	2	2	1	1	15	1	f
211	2016-08-24 06:37:24-05	lol	50	\N	86	2	1	1	1	154	1	f
212	2016-08-24 06:38:41-05	ioio	20	\N	86	2	1	1	1	154	1	f
213	2016-08-24 06:40:20-05	Sin Comentario	20	\N	86	2	1	1	1	154	1	f
216	2016-08-24 06:48:59-05	Sin Comentario	10.25	5	87	1	1	1	1	155	2	f
214	2016-08-24 06:45:52-05	Sin Comentario	5.75	6	8	1	1	1	1	15	2	f
215	2016-08-24 06:47:24-05	Sin Comentario	20	1	86	2	1	1	1	154	2	f
217	2016-08-24 06:49:40-05	Sin Comentario	20	2	86	2	1	1	1	154	5	t
218	2016-08-24 06:50:55-05	Sin Comentario	14.75	6	86	1	1	1	1	154	5	t
\.


--
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_pedido_id_pedido_seq', 218, true);


--
-- Data for Name: spaapp_perfil; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) FROM stdin;
86		\N		\N	usuarioFoto/descarga.png	0	55
4		\N		\N	usuarioFoto/rest1_GTgybUv.png	14	6
5	1122334455	2016-07-28	213131	Femenino	usuarioFoto/icecreamsocial_yj6ICi3.jpg	sdada	7
6		\N		\N	usuarioFoto/rest10_Tb8n5Vo.jpg	14	8
7	123123312	2016-07-28	1312312	Femenino	usuarioFoto/papalogo_T69w2cx_chpNt8u.jpg	sdasdsa	9
8	1212121	\N		\N	usuarioFoto/rest12_pgD61rK.png	12	10
3	1122334455	2016-03-16	1111111	Masculino	usuarioFoto/chinito_HYopzZ2.jpg	12312312	5
46	1109483737	2016-08-26	25467877	Masculino	noname	7	3
49	1212121	2016-08-08	1526464643	Masculino		7	11
147	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	116
148	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	117
149	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	118
150	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	119
151	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	120
109	1102578787	2016-08-06	2588897	\N	usuarioFoto/new_york_city_aerial_view-wallpaper-1366x768_OflKSbl.jpg		78
110	8988888888	\N	2656548	Masculino	usuarioFoto/userDefault.jpg		79
111	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	80
152	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	121
66	1111111111	2016-08-08	12	Masculino	noname	12	3
1	1103819338	2016-07-26	0984532	Masculino	noname	22222	3
45	1102984765	2016-07-28	09876543	Masculino	noname	30000	3
10	1102323232	2016-05-20	254494	Masculino	usuarioFoto/3ccca5fc-6ee8-401c-a514-27d51e4f3ac4.jpg		2
90	\N	\N	\N	\N	/spa/media/userDefault.jpg	0	59
153	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	122
91	\N	\N	\N	\N	/spa/media/userDefault.jpg	0	60
154	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	123
93	\N	\N	\N	\N	/spa/media/userDefault.jpg	0	62
44	1104857563	2016-07-28	12345	Masculino	noname	5554321	3
112	11	1992-08-16	234	Masculino	usuarioFoto/descarga_jKJOy5S.jpg		81
2	2222222	2016-02-20	555	Masculino	usuarioFoto/logo.png	14	4
113	12345678	2016-08-18	12345678	Masculino	usuarioFoto/lftc_evolution_sysadmin.jpg		82
114	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	83
115	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	84
155	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	124
67	1104666332	2015-03-20	2464	Masculino	/spa/media/userDefault.jpg	0	36
70	\N	\N	\N	\N	/spa/media/userDefault.jpg	0	39
47	1123456789	2016-01-01	555555	genero	foto/foto.jpg	aaaaa	19
156	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	125
116	12345678	2016-08-19	1234	Masculino	usuarioFoto/archlinux-darkflower.png		85
157	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	126
117	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	86
118	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	87
119	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	88
77	\N	\N	\N	\N	/spa/media/userDefault.jpg	0	46
120	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	89
121	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	90
122	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	91
80	\N	\N	\N	\N	/spa/media/userDefault.jpg	0	49
81	\N	\N	\N	\N	/spa/media/userDefault.jpg	0	50
48	1104322333	2000-10-10	2584179	Masculino	noname	2584179	3
123	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	92
124	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	93
125	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	94
126	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	95
87	\N	\N	\N	\N	/spa/media/userDefault.jpg	0	56
127	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	96
128	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	97
129	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	98
94		\N		\N	usuarioFoto/pollos-gusy.jpeg	0	63
130	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	99
131	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	100
132	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	101
133	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	102
134	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	103
135	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	104
136	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	105
107	1104666332	2016-03-20	2588797	Femenino	usuarioFoto/minion_party-wallpaper-2048x1152.jpg		76
137	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	106
138	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	107
158	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	127
139	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	108
108	11025487	2016-05-20	258897	Masculino	usuarioFoto/travel_the_world-wallpaper-1366x768_fAPTAQI.jpg		77
140	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	109
141	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	110
142	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	111
143	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	112
144	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	113
145	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	114
146	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	115
159	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	128
160	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	129
161	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	130
162	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	131
163	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	132
164	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	133
165	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	134
166	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	135
167	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	136
168	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	137
169	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	138
170	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	139
171	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	140
172	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	141
173	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	142
174	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	143
175	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	144
176	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	145
177	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	146
178	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	147
179	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	148
180	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	149
181	1104666332	1994-06-09	255897	Masculino	usuarioFoto/fondolinux.png		150
182	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	151
183	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	152
184	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	153
185	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	154
186	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	155
187		\N		\N	usuarioFoto/loco_Chavez.logo.png	0	156
188		\N		\N	usuarioFoto/Logo_Soyard_-_pantone.jpg	0	157
189		\N		\N	usuarioFoto/FSCN3834.JPG		158
36	1123456783	\N	567893	Masculino	usuarioFoto/14721364847381353867013.jpg		15
\.


--
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_perfil_id_perfil_seq', 189, true);


--
-- Data for Name: spaapp_plato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id, activo) FROM stdin;
20	Tallarin Salteado de Pollo	menuFoto/carta-tallarin-saltado.jpg	dasdas	5	5	12:00:00	5	5	1	t
22	Burrito	menuFoto/classicgrilledchickenburrito.png	fds	5	5	12:00:00	5	5	1	t
10	pollo	menuFoto/patas-de-pollo.jpg	pollo	5	5	12:00:00	5	5	4	t
2	Helado de chocolate	menuFoto/he2_sL69ayP.jpg	Helado de chocolate	2	4	20:19:35	8	12	1	f
25	Pizza de tomate	menuFoto/pizza_tomate.jpeg	Deliciosa Pizza	5	5	12:00:00	5	5	2	t
3	Hawaiana	menuFoto/plato6_lEA1j32.jpg	lleva jamón , piñaa	2	4	11:51:03	8	12	2	t
27	Chaulafán	menuFoto/chaulafan.jpg	Delicioso plato con camarones	5	5	12:00:00	5	5	5	t
28	Pizza Americana	menuFoto/pizza_9k8TCqk.JPG	Deliciosa pizza con huevo	5	5	12:00:00	5	5	2	t
21	tallarin y pollo	menuFoto/tallarin_y_pollo.jpg	Con los mejores tallarines del mercado	5	5	12:00:00	5	5	2	t
26	Pizza Chiquis	menuFoto/tomate_Bfl5wtz.jpg	Deliciosa pizza para niños	5	5	12:00:00	5	5	2	t
29	Rellenito	menuFoto/relleno.jpg	LOL	5	5	12:00:00	5	5	7	t
30	fritada	menuFoto/fritada.jpg	fritada con mote	5	5	12:00:00	5	5	8	t
31	Pizza5	menuFoto/pizzaamericana.jpg	Deliciosa Pizza	5	5	12:00:00	5	5	2	t
4	Napolitana	menuFoto/napo.jpg	La pizza napolitana puede contener anchoas, orégano, alcaparras y ajo, entre otros ingredientes.	2	4	11:51:03	8	12	2	t
23	Arroz Relleno	menuFoto/receta-arroz-relleno.jpg	Delicioso arroz con los mejores embutidos	5	5	12:00:00	5	5	7	t
32	Pizza de tomate Las Vegas	menuFoto/vegas.jpg	La mejor pizza de tomate al estilo Las Vegas!	5	5	12:00:00	5	5	2	t
33	Sanduche Submarino	menuFoto/sanduche.jpeg	Sanduche Submarino	5	5	12:00:00	5	5	3	t
34	Soyard Maracuyá	menuFoto/S2.png	Bebida	5	5	12:00:00	5	5	4	t
\.


--
-- Data for Name: spaapp_plato_id_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_plato_id_menu (id, plato_id, menu_id) FROM stdin;
23	20	2
25	22	2
26	10	2
33	2	1
37	25	1
40	3	1
41	27	2
42	28	1
44	21	1
46	26	1
47	29	1
48	30	1
49	31	1
50	4	1
52	23	1
55	32	1
56	33	12
57	34	13
\.


--
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_menu_id_seq', 57, true);


--
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_plato_seq', 34, true);


--
-- Data for Name: spaapp_platocategoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_platocategoria (id_platocategoria, categoria) FROM stdin;
1	Helados
2	Pizza
3	Hamburguesa
4	Bebida
5	Marisco
6	Pescado
7	Pollo
8	Carnes
\.


--
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platocategoria_id_platocategoria_seq', 8, true);


--
-- Data for Name: spaapp_platoingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_platoingrediente (id_plato_ingrediente, requerido, horafecha, id_ingrediente_id, id_plato_id) FROM stdin;
24	t	2016-08-09 18:07:19.00023-05	3	20
25	t	2016-08-09 18:07:19.00023-05	18	20
28	t	2016-08-09 18:22:42.147262-05	5	22
29	t	2016-08-09 18:22:42.147262-05	18	22
30	t	2016-08-09 18:25:35.557768-05	3	10
50	t	2016-08-17 10:47:54.807805-05	19	2
64	f	2016-08-17 11:45:36.416445-05	8	25
65	f	2016-08-17 11:45:36.416445-05	9	25
66	t	2016-08-17 11:45:36.416445-05	15	25
67	t	2016-08-17 11:45:36.416445-05	20	25
73	t	2016-08-18 18:32:23.917527-05	3	3
74	f	2016-08-18 18:41:01.096304-05	6	27
75	t	2016-08-18 18:41:01.096304-05	10	27
76	t	2016-08-18 18:41:01.096304-05	18	27
77	t	2016-08-18 18:41:01.096304-05	21	27
78	f	2016-08-19 11:32:31.216595-05	8	28
79	t	2016-08-19 11:32:31.216595-05	15	28
80	t	2016-08-19 11:32:31.216595-05	20	28
81	t	2016-08-19 11:32:31.216595-05	22	28
86	f	2016-08-19 11:44:41.301116-05	3	21
87	t	2016-08-19 11:44:41.301116-05	18	21
92	t	2016-08-19 11:46:45.064667-05	8	26
93	t	2016-08-19 11:46:45.064667-05	9	26
94	f	2016-08-19 11:46:45.064667-05	10	26
95	t	2016-08-19 11:46:45.064667-05	15	26
96	t	2016-08-19 11:46:45.064667-05	20	26
97	t	2016-08-19 11:46:45.064667-05	23	26
98	t	2016-08-19 13:39:05.577792-05	3	29
99	f	2016-08-19 13:39:05.577792-05	5	29
100	t	2016-08-19 16:28:08.571735-05	24	30
101	f	2016-08-19 16:28:08.571735-05	25	30
102	t	2016-08-19 16:31:14.112662-05	6	31
103	t	2016-08-21 23:43:03.760873-05	3	4
109	t	2016-08-23 18:05:55.570197-05	3	23
110	f	2016-08-23 18:05:55.570197-05	5	23
111	t	2016-08-23 18:05:55.570197-05	6	23
112	t	2016-08-23 18:05:55.570197-05	10	23
113	t	2016-08-23 18:05:55.570197-05	16	23
114	t	2016-08-23 18:05:55.570197-05	26	23
123	t	2016-08-23 18:14:13.460886-05	15	32
124	t	2016-08-23 18:14:13.460886-05	20	32
125	f	2016-08-23 18:14:13.460886-05	21	32
126	t	2016-08-23 18:14:13.460886-05	27	32
127	t	2016-08-24 19:47:46.67475-05	28	33
128	t	2016-08-24 19:47:46.67475-05	29	33
129	t	2016-08-24 19:47:46.67475-05	30	33
130	t	2016-08-24 20:05:16.456246-05	31	34
\.


--
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platoingrediente_id_plato_ingrediente_seq', 130, true);


--
-- Data for Name: spaapp_proveedor; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) FROM stdin;
1	Pizza	65821212		4	5	5216454645	1	1	2
2	Chinito	111111		1	\N	adasdasda	1	1	3
3	Casa China	1526464643		4	\N	221212	1	2	4
4	Ice Cream Social	111111		1	\N	sadasdas	1	3	5
5	Chamelion	65821212		4	\N	5216454645	1	4	6
6	PapaLogo	213123		1	\N	asdadsa	1	5	7
7	Tony's	65821212		4	\N	12	1	3	8
8	Fruques	1526464643		4	\N	5216454645	1	3	86
9	Gusy	343434		2	\N	232	1	7	94
10	Loco Chavez	2222222222		1	\N	2222222222	1	5	187
11	Soyard	4546465	4454646	1	1	11121212	1	8	188
\.


--
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_proveedor_id_proveedor_seq', 11, true);


--
-- Data for Name: spaapp_redsocial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_redsocial (id_red_social, nombre_red_social, cuenta_persona, id_proveedor_id) FROM stdin;
\.


--
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_redsocial_id_red_social_seq', 1, false);


--
-- Data for Name: spaapp_sucursal; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id, descripcion, precio_envio) FROM stdin;
4	Ice Cream Social	heladería	111111		7	08:00	\N	1	f	12:00:00	1	1	2	4	4	\N	1
5	Chamelion	212121	1526464643		Lunes - Sábado	8:00 - 18:00	\N	4	f	20:11:15	5	1	4	5	5	\N	2
6	Tony's	212121	65821212		Lunes - Sábado	8:00 - 18:00	\N	4	f	11:39:43	5	1	4	6	7	\N	3
7	PapaLogo	asdadasd	dasdasda		7	08:00	\N	1	f	12:00:00	1	1	2	7	6	\N	4
9	Gusy	12121	12121		Lunes - Sabado	09:00 - 21:00	\N	2	t	14:00:09	4	1	17	11	9	\N	6
2	Chinito	Restaurante	12345678		7	08:00	0	1	f	12:00:00	1	1	2	2	2	\N	5
10	Loco Chavez	1215131	25888o	456465641	Lunes - Viernes	9:00 - 18:00	1	1	f	13:00:00	1	1	88	12	10	Deliciosa comida	\N
11	Soyard	1215454	2486764	4565465	Lunes - Viernes	9:00 - 18:00	1	1	f	12:00:00	1	1	89	13	11	koolkokl;	\N
1	Pizzaa	5216454645	2588864		Lunes - Sábado	13:00 - 20:00	1	1	f	15:00:00	5	1	1	1	1	Ven y disfruta de deliciosas pizzas al estilo italiano	1.25
3	Casa china	5216454645	65821212		Lunes - Sábado	8:00 - 18:00	\N	4	f	20:08:04	5	1	3	3	3	\N	1.5
\.


--
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_sucursal_id_sucursal_seq', 11, true);


--
-- Data for Name: spaapp_tamanio; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tamanio (id_tamanio, tamanio) FROM stdin;
1	Standard
2	Mediana
3	Grande
4	Normal
\.


--
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanio_id_tamanio_seq', 4, true);


--
-- Data for Name: spaapp_tamanioplato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id, activo) FROM stdin;
30	15	20	2	t
32	14	22	3	t
33	12	10	2	t
49	1.5	2	2	t
50	2	2	3	t
60	12	25	2	f
61	15	25	3	t
62	10	25	4	t
67	3.5	3	4	t
68	5.5	3	3	t
69	12.3599999999999994	27	2	t
70	15.1999999999999993	27	3	t
71	10.3200000000000003	28	1	t
72	12.5600000000000005	28	2	t
73	15.2599999999999998	28	3	t
76	14	21	3	t
77	10	21	2	t
80	12	26	2	t
81	14	26	4	t
82	12.5999999999999996	29	3	t
83	4.5	30	2	t
84	12.5600000000000005	31	1	t
85	3	2	1	f
86	3	4	2	t
87	12	4	3	t
91	15	23	2	t
92	10	23	4	t
93	20	23	3	t
100	12.3000000000000007	32	2	t
101	15.3000000000000007	32	3	t
102	10	32	4	t
103	2.5	33	1	t
104	5	33	3	t
105	1.10000000000000009	34	1	t
\.


--
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanioplato_id_tamanioplato_seq', 105, true);


--
-- Data for Name: spaapp_tipopago; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tipopago (id_tipo_pago, tipo_pago, observaciones) FROM stdin;
1	Efectivo	Efectivo
2	Dinero Electrónico	Dinero Electrónico
\.


--
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopago_id_tipo_pago_seq', 2, true);


--
-- Data for Name: spaapp_tipopedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tipopedido (id_tipo_pedido, tipo_pedido, observaciones) FROM stdin;
1	A domicilio	A domicilio
2	Para recoger	Para recoger
\.


--
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopedido_id_tipo_pedido_seq', 2, true);


--
-- Data for Name: spaapp_transportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_transportista (id_transportista, tipo_transporte, tipo_servicio, id_perfil_id) FROM stdin;
8	MotoTaxi	Entrega	48
7	Taxi	Entrega	46
5	Moto	Servicio	44
9	1	1	66
1	Moto	Entrega	1
6	Taxi	Entrega	45
\.


--
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_transportista_id_transportista_seq', 9, true);


--
-- Data for Name: spaapp_valoracion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_valoracion (id_valoracion, valor_transportista, observaciones, valor_proveedor, valor_servicio, valoracion_general, id_cliente_id, id_pedido_id, id_proveedor_id, id_sucursal_id, id_transportista_id) FROM stdin;
\.


--
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_valoracion_id_valoracion_seq', 7, true);


--
-- Name: administracionAPP_contacto_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY "administracionAPP_contacto"
    ADD CONSTRAINT "administracionAPP_contacto_pkey" PRIMARY KEY (id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: social_auth_association_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_association
    ADD CONSTRAINT social_auth_association_pkey PRIMARY KEY (id);


--
-- Name: social_auth_code_email_801b2d02_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code
    ADD CONSTRAINT social_auth_code_email_801b2d02_uniq UNIQUE (email, code);


--
-- Name: social_auth_code_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code
    ADD CONSTRAINT social_auth_code_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce_server_url_f6284463_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_server_url_f6284463_uniq UNIQUE (server_url, "timestamp", salt);


--
-- Name: social_auth_usersocialauth_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth_provider_e6b5e668_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_provider_e6b5e668_uniq UNIQUE (provider, uid);


--
-- Name: spaapp_actividadcomercial_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_actividadcomercial
    ADD CONSTRAINT spaapp_actividadcomercial_pkey PRIMARY KEY (id_actividad);


--
-- Name: spaapp_catalogoestado_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_catalogoestado
    ADD CONSTRAINT spaapp_catalogoestado_pkey PRIMARY KEY (id_estado);


--
-- Name: spaapp_categoria_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_categoria
    ADD CONSTRAINT spaapp_categoria_pkey PRIMARY KEY (id_categoria);


--
-- Name: spaapp_ciudad_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ciudad
    ADD CONSTRAINT spaapp_ciudad_pkey PRIMARY KEY (id_ciudad);


--
-- Name: spaapp_cliente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente
    ADD CONSTRAINT spaapp_cliente_pkey PRIMARY KEY (id_cliente);


--
-- Name: spaapp_contrato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaapp_contrato_pkey PRIMARY KEY (id_contrato);


--
-- Name: spaapp_coordenadas_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas
    ADD CONSTRAINT spaapp_coordenadas_pkey PRIMARY KEY (id_coordenadas);


--
-- Name: spaapp_detallepedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detallepedido_pkey PRIMARY KEY (id_detalle);


--
-- Name: spaapp_direccion_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion
    ADD CONSTRAINT spaapp_direccion_pkey PRIMARY KEY (id_direccion);


--
-- Name: spaapp_horariotransportista_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista
    ADD CONSTRAINT spaapp_horariotransportista_pkey PRIMARY KEY (id_horario);


--
-- Name: spaapp_ingrediente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente
    ADD CONSTRAINT spaapp_ingrediente_pkey PRIMARY KEY (id_ingrediente);


--
-- Name: spaapp_menu_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu
    ADD CONSTRAINT spaapp_menu_pkey PRIMARY KEY (id_menu);


--
-- Name: spaapp_modificardetallepedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT spaapp_modificardetallepedido_pkey PRIMARY KEY (id_modificardetallepedido);


--
-- Name: spaapp_pedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_pedido_pkey PRIMARY KEY (id_pedido);


--
-- Name: spaapp_perfil_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil
    ADD CONSTRAINT spaapp_perfil_pkey PRIMARY KEY (id_perfil);


--
-- Name: spaapp_plato_id_menu_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_pkey PRIMARY KEY (id);


--
-- Name: spaapp_plato_id_menu_plato_id_a7b45c73_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_plato_id_a7b45c73_uniq UNIQUE (plato_id, menu_id);


--
-- Name: spaapp_plato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato
    ADD CONSTRAINT spaapp_plato_pkey PRIMARY KEY (id_plato);


--
-- Name: spaapp_platocategoria_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platocategoria
    ADD CONSTRAINT spaapp_platocategoria_pkey PRIMARY KEY (id_platocategoria);


--
-- Name: spaapp_platoingrediente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT spaapp_platoingrediente_pkey PRIMARY KEY (id_plato_ingrediente);


--
-- Name: spaapp_proveedor_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveedor_pkey PRIMARY KEY (id_proveedor);


--
-- Name: spaapp_redsocial_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial
    ADD CONSTRAINT spaapp_redsocial_pkey PRIMARY KEY (id_red_social);


--
-- Name: spaapp_sucursal_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursal_pkey PRIMARY KEY (id_sucursal);


--
-- Name: spaapp_tamanio_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanio
    ADD CONSTRAINT spaapp_tamanio_pkey PRIMARY KEY (id_tamanio);


--
-- Name: spaapp_tamanioplato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tamanioplato_pkey PRIMARY KEY (id_tamanioplato);


--
-- Name: spaapp_tipopago_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopago
    ADD CONSTRAINT spaapp_tipopago_pkey PRIMARY KEY (id_tipo_pago);


--
-- Name: spaapp_tipopedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopedido
    ADD CONSTRAINT spaapp_tipopedido_pkey PRIMARY KEY (id_tipo_pedido);


--
-- Name: spaapp_transportista_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista
    ADD CONSTRAINT spaapp_transportista_pkey PRIMARY KEY (id_transportista);


--
-- Name: spaapp_valoracion_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valoracion_pkey PRIMARY KEY (id_valoracion);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: social_auth_code_c1336794; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_code_c1336794 ON social_auth_code USING btree (code);


--
-- Name: social_auth_code_code_a2393167_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_code_code_a2393167_like ON social_auth_code USING btree (code varchar_pattern_ops);


--
-- Name: social_auth_usersocialauth_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_usersocialauth_e8701ad4 ON social_auth_usersocialauth USING btree (user_id);


--
-- Name: spaapp_cliente_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_cliente_7d3abc31 ON spaapp_cliente USING btree (id_perfil_id);


--
-- Name: spaapp_contrato_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_contrato_ca253e0c ON spaapp_contrato USING btree (id_proveedor_id);


--
-- Name: spaapp_contrato_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_contrato_e163c0f9 ON spaapp_contrato USING btree (id_sucursal_id);


--
-- Name: spaapp_coordenadas_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_coordenadas_7d3abc31 ON spaapp_coordenadas USING btree (id_perfil_id);


--
-- Name: spaapp_detallepedido_55ef5a99; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_55ef5a99 ON spaapp_detallepedido USING btree (tamanio_id);


--
-- Name: spaapp_detallepedido_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_8690b5df ON spaapp_detallepedido USING btree (id_plato_id);


--
-- Name: spaapp_detallepedido_9b93490d; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_9b93490d ON spaapp_detallepedido USING btree (id_pedido_id);


--
-- Name: spaapp_direccion_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_direccion_7d3abc31 ON spaapp_direccion USING btree (id_perfil_id);


--
-- Name: spaapp_horariotransportista_113016bf; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_horariotransportista_113016bf ON spaapp_horariotransportista USING btree (id_transportista_id);


--
-- Name: spaapp_ingrediente_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_ingrediente_ca253e0c ON spaapp_ingrediente USING btree (id_proveedor_id);


--
-- Name: spaapp_menu_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_menu_ca253e0c ON spaapp_menu USING btree (id_proveedor_id);


--
-- Name: spaapp_modificardetallepedido_61139c8c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_modificardetallepedido_61139c8c ON spaapp_modificardetallepedido USING btree (id_detallepedido_id);


--
-- Name: spaapp_modificardetallepedido_99518874; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_modificardetallepedido_99518874 ON spaapp_modificardetallepedido USING btree (id_ingrediente_quitar_id);


--
-- Name: spaapp_pedido_21740aad; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_21740aad ON spaapp_pedido USING btree (id_tipo_pedido_id);


--
-- Name: spaapp_pedido_9639f13b; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_9639f13b ON spaapp_pedido USING btree (id_direccion_id);


--
-- Name: spaapp_pedido_98181a38; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_98181a38 ON spaapp_pedido USING btree (id_estado_id);


--
-- Name: spaapp_pedido_abfe0f96; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_abfe0f96 ON spaapp_pedido USING btree (usuario_id);


--
-- Name: spaapp_pedido_ccfa12af; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_ccfa12af ON spaapp_pedido USING btree (id_tipo_pago_id);


--
-- Name: spaapp_pedido_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_e163c0f9 ON spaapp_pedido USING btree (id_sucursal_id);


--
-- Name: spaapp_pedido_f09d4acb; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_f09d4acb ON spaapp_pedido USING btree (id_trasportista_id);


--
-- Name: spaapp_perfil_76a74f43; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_perfil_76a74f43 ON spaapp_perfil USING btree (id_usuario_id);


--
-- Name: spaapp_plato_06d53be8; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_06d53be8 ON spaapp_plato USING btree (id_platocategoria_id);


--
-- Name: spaapp_plato_id_menu_20104dab; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_id_menu_20104dab ON spaapp_plato_id_menu USING btree (plato_id);


--
-- Name: spaapp_plato_id_menu_93e25458; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_id_menu_93e25458 ON spaapp_plato_id_menu USING btree (menu_id);


--
-- Name: spaapp_platoingrediente_4b077a6c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_platoingrediente_4b077a6c ON spaapp_platoingrediente USING btree (id_ingrediente_id);


--
-- Name: spaapp_platoingrediente_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_platoingrediente_8690b5df ON spaapp_platoingrediente USING btree (id_plato_id);


--
-- Name: spaapp_proveedor_0e29f854; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_0e29f854 ON spaapp_proveedor USING btree (id_categoria_id);


--
-- Name: spaapp_proveedor_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_7d3abc31 ON spaapp_proveedor USING btree (id_perfil_id);


--
-- Name: spaapp_proveedor_c1f5aad7; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_c1f5aad7 ON spaapp_proveedor USING btree (id_actividad_id);


--
-- Name: spaapp_redsocial_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_redsocial_ca253e0c ON spaapp_redsocial USING btree (id_proveedor_id);


--
-- Name: spaapp_sucursal_9639f13b; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_9639f13b ON spaapp_sucursal USING btree (id_direccion_id);


--
-- Name: spaapp_sucursal_b1a01987; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_b1a01987 ON spaapp_sucursal USING btree (id_ciudad_id);


--
-- Name: spaapp_sucursal_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_ca253e0c ON spaapp_sucursal USING btree (id_proveedor_id);


--
-- Name: spaapp_sucursal_e1161949; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_e1161949 ON spaapp_sucursal USING btree (id_menu_id);


--
-- Name: spaapp_tamanioplato_05d6b9c9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_tamanioplato_05d6b9c9 ON spaapp_tamanioplato USING btree (id_tamanio_id);


--
-- Name: spaapp_tamanioplato_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_tamanioplato_8690b5df ON spaapp_tamanioplato USING btree (id_plato_id);


--
-- Name: spaapp_transportista_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_transportista_7d3abc31 ON spaapp_transportista USING btree (id_perfil_id);


--
-- Name: spaapp_valoracion_113016bf; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_113016bf ON spaapp_valoracion USING btree (id_transportista_id);


--
-- Name: spaapp_valoracion_6d98a18e; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_6d98a18e ON spaapp_valoracion USING btree (id_cliente_id);


--
-- Name: spaapp_valoracion_9b93490d; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_9b93490d ON spaapp_valoracion USING btree (id_pedido_id);


--
-- Name: spaapp_valoracion_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_ca253e0c ON spaapp_valoracion USING btree (id_proveedor_id);


--
-- Name: spaapp_valoracion_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_e163c0f9 ON spaapp_valoracion USING btree (id_sucursal_id);


--
-- Name: trigger_menu; Type: TRIGGER; Schema: spaapp; Owner: spauser
--

CREATE TRIGGER trigger_menu AFTER INSERT ON spaapp_proveedor FOR EACH ROW EXECUTE PROCEDURE insertar_menu();


--
-- Name: trigger_perfil; Type: TRIGGER; Schema: spaapp; Owner: spauser
--

CREATE TRIGGER trigger_perfil AFTER INSERT ON auth_user FOR EACH ROW EXECUTE PROCEDURE insertar_perfil();


--
-- Name: D16bd3bac72562dcaa9ab5f9f6fb85ae; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista
    ADD CONSTRAINT "D16bd3bac72562dcaa9ab5f9f6fb85ae" FOREIGN KEY (id_transportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D7b946988cbfb9911c65af920b3935cb; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT "D7b946988cbfb9911c65af920b3935cb" FOREIGN KEY (id_detallepedido_id) REFERENCES spaapp_detallepedido(id_detalle) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D893b6353e00f54fed4819335a2ca882; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT "D893b6353e00f54fed4819335a2ca882" FOREIGN KEY (id_transportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D91d9aaf1a328bd31979d1581c55c5ec; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT "D91d9aaf1a328bd31979d1581c55c5ec" FOREIGN KEY (id_actividad_id) REFERENCES spaapp_actividadcomercial(id_actividad) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D944537a2d489f0d21d6406b48ddb641; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT "D944537a2d489f0d21d6406b48ddb641" FOREIGN KEY (id_trasportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ba7c74bed2241fbcd786c213851660dc; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT ba7c74bed2241fbcd786c213851660dc FOREIGN KEY (id_ingrediente_id) REFERENCES spaapp_ingrediente(id_ingrediente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dce350061342863e912703a9dafef09e; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato
    ADD CONSTRAINT dce350061342863e912703a9dafef09e FOREIGN KEY (id_platocategoria_id) REFERENCES spaapp_platocategoria(id_platocategoria) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: e7cb8922641d3a71e0518cc63d3617b2; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT e7cb8922641d3a71e0518cc63d3617b2 FOREIGN KEY (id_ingrediente_quitar_id) REFERENCES spaapp_ingrediente(id_ingrediente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: id_tipo_pedido_id_cde83768_fk_spaapp_tipopedido_id_tipo_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT id_tipo_pedido_id_cde83768_fk_spaapp_tipopedido_id_tipo_pedido FOREIGN KEY (id_tipo_pedido_id) REFERENCES spaapp_tipopedido(id_tipo_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaa_tamanio_id_8427c076_fk_spaapp_tamanioplato_id_tamanioplato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaa_tamanio_id_8427c076_fk_spaapp_tamanioplato_id_tamanioplato FOREIGN KEY (tamanio_id) REFERENCES spaapp_tamanioplato(id_tamanioplato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_categoria_id_d672ac86_fk_spaapp_categoria_id_categoria; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaap_id_categoria_id_d672ac86_fk_spaapp_categoria_id_categoria FOREIGN KEY (id_categoria_id) REFERENCES spaapp_categoria(id_categoria) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_direccion_id_80462fd2_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaap_id_direccion_id_80462fd2_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_direccion_id_e2c4fbe4_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaap_id_direccion_id_e2c4fbe4_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_11675f72_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaap_id_proveedor_id_11675f72_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_18c32e0e_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial
    ADD CONSTRAINT spaap_id_proveedor_id_18c32e0e_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_540b8b72_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaap_id_proveedor_id_540b8b72_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_66760433_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaap_id_proveedor_id_66760433_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_e39d0124_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu
    ADD CONSTRAINT spaap_id_proveedor_id_e39d0124_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_f119de44_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente
    ADD CONSTRAINT spaap_id_proveedor_id_f119de44_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_c_id_sucursal_id_54ec1010_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaapp_c_id_sucursal_id_54ec1010_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_cliente_id_perfil_id_6a66d1dd_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente
    ADD CONSTRAINT spaapp_cliente_id_perfil_id_6a66d1dd_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_coorden_id_perfil_id_40dcc2a5_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas
    ADD CONSTRAINT spaapp_coorden_id_perfil_id_40dcc2a5_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_detalle_id_pedido_id_d433e145_fk_spaapp_pedido_id_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detalle_id_pedido_id_d433e145_fk_spaapp_pedido_id_pedido FOREIGN KEY (id_pedido_id) REFERENCES spaapp_pedido(id_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_detalleped_id_plato_id_5455a09f_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detalleped_id_plato_id_5455a09f_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_direcci_id_perfil_id_ff0b3f89_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion
    ADD CONSTRAINT spaapp_direcci_id_perfil_id_ff0b3f89_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_id_estado_id_a224fdfb_fk_spaapp_catalogoestado_id_estado; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_id_estado_id_a224fdfb_fk_spaapp_catalogoestado_id_estado FOREIGN KEY (id_estado_id) REFERENCES spaapp_catalogoestado(id_estado) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_id_tipo_pago_id_1cbd151a_fk_spaapp_tipopago_id_tipo_pago; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_id_tipo_pago_id_1cbd151a_fk_spaapp_tipopago_id_tipo_pago FOREIGN KEY (id_tipo_pago_id) REFERENCES spaapp_tipopago(id_tipo_pago) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_p_id_sucursal_id_b6e7aa23_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_p_id_sucursal_id_b6e7aa23_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_pedido_usuario_id_03fad4c7_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_pedido_usuario_id_03fad4c7_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_perfil_id_usuario_id_ecaf41b3_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil
    ADD CONSTRAINT spaapp_perfil_id_usuario_id_ecaf41b3_fk_auth_user_id FOREIGN KEY (id_usuario_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_plato_id_menu_menu_id_f192d1d8_fk_spaapp_menu_id_menu; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_menu_id_f192d1d8_fk_spaapp_menu_id_menu FOREIGN KEY (menu_id) REFERENCES spaapp_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_plato_id_menu_plato_id_1bbe6660_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_plato_id_1bbe6660_fk_spaapp_plato_id_plato FOREIGN KEY (plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_platoingre_id_plato_id_fe44a1d9_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT spaapp_platoingre_id_plato_id_fe44a1d9_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_proveed_id_perfil_id_fff821d0_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveed_id_perfil_id_fff821d0_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_sucursa_id_ciudad_id_f5184cf6_fk_spaapp_ciudad_id_ciudad; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursa_id_ciudad_id_f5184cf6_fk_spaapp_ciudad_id_ciudad FOREIGN KEY (id_ciudad_id) REFERENCES spaapp_ciudad(id_ciudad) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_sucursal_id_menu_id_81f57738_fk_spaapp_menu_id_menu; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursal_id_menu_id_81f57738_fk_spaapp_menu_id_menu FOREIGN KEY (id_menu_id) REFERENCES spaapp_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_tama_id_tamanio_id_a9692104_fk_spaapp_tamanio_id_tamanio; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tama_id_tamanio_id_a9692104_fk_spaapp_tamanio_id_tamanio FOREIGN KEY (id_tamanio_id) REFERENCES spaapp_tamanio(id_tamanio) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_tamaniopla_id_plato_id_5f8f9ca8_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tamaniopla_id_plato_id_5f8f9ca8_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_transpo_id_perfil_id_9a457afd_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista
    ADD CONSTRAINT spaapp_transpo_id_perfil_id_9a457afd_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_v_id_sucursal_id_a7f67c7c_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_v_id_sucursal_id_a7f67c7c_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_valo_id_cliente_id_a7d43e7d_fk_spaapp_cliente_id_cliente; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valo_id_cliente_id_a7d43e7d_fk_spaapp_cliente_id_cliente FOREIGN KEY (id_cliente_id) REFERENCES spaapp_cliente(id_cliente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_valorac_id_pedido_id_119ed90a_fk_spaapp_pedido_id_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valorac_id_pedido_id_119ed90a_fk_spaapp_pedido_id_pedido FOREIGN KEY (id_pedido_id) REFERENCES spaapp_pedido(id_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp; Type: ACL; Schema: -; Owner: spauser
--

REVOKE ALL ON SCHEMA spaapp FROM PUBLIC;
REVOKE ALL ON SCHEMA spaapp FROM spauser;
GRANT ALL ON SCHEMA spaapp TO spauser;
GRANT ALL ON SCHEMA spaapp TO postgres;


--
-- PostgreSQL database dump complete
--

