--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: spaapp; Type: SCHEMA; Schema: -; Owner: spauser
--

CREATE SCHEMA spaapp;


ALTER SCHEMA spaapp OWNER TO spauser;

SET search_path = spaapp, pg_catalog;

--
-- Name: insertar_menu(); Type: FUNCTION; Schema: spaapp; Owner: spauser
--

CREATE FUNCTION insertar_menu() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	BEGIN		INSERT INTO spaapp_menu (id_proveedor_id) VALUES (NEW.id_proveedor);		RETURN NULL;	END;$$;


ALTER FUNCTION spaapp.insertar_menu() OWNER TO spauser;

--
-- Name: insertar_perfil(); Type: FUNCTION; Schema: spaapp; Owner: spauser
--

CREATE FUNCTION insertar_perfil() RETURNS trigger
    LANGUAGE plpgsql
    AS $$	BEGIN		INSERT INTO spaapp_perfil (id_usuario_id,ruta_foto, sim_dispositivo) VALUES (NEW.id,'usuarioFoto/userDefault.jpg',0);		RETURN NULL;	END;$$;


ALTER FUNCTION spaapp.insertar_perfil() OWNER TO spauser;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: administracionAPP_contacto; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE "administracionAPP_contacto" (
    id integer NOT NULL,
    nombre_propietario character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    ciudad character varying(50) NOT NULL,
    nombre_empresa character varying(50) NOT NULL,
    direccion character varying(50) NOT NULL,
    tipo_negocio character varying(50) NOT NULL,
    telefono double precision NOT NULL
);


ALTER TABLE "administracionAPP_contacto" OWNER TO spauser;

--
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE "administracionAPP_contacto_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "administracionAPP_contacto_id_seq" OWNER TO spauser;

--
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE "administracionAPP_contacto_id_seq" OWNED BY "administracionAPP_contacto".id;


--
-- Name: auth_group; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO spauser;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO spauser;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO spauser;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO spauser;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO spauser;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO spauser;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO spauser;

--
-- Name: auth_user_groups; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO spauser;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO spauser;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO spauser;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO spauser;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO spauser;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO spauser;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO spauser;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO spauser;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO spauser;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO spauser;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO spauser;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO spauser;

--
-- Name: social_auth_association; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_association (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    handle character varying(255) NOT NULL,
    secret character varying(255) NOT NULL,
    issued integer NOT NULL,
    lifetime integer NOT NULL,
    assoc_type character varying(64) NOT NULL
);


ALTER TABLE social_auth_association OWNER TO spauser;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_association_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_association_id_seq OWNER TO spauser;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_association_id_seq OWNED BY social_auth_association.id;


--
-- Name: social_auth_code; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_code (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    code character varying(32) NOT NULL,
    verified boolean NOT NULL
);


ALTER TABLE social_auth_code OWNER TO spauser;

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_code_id_seq OWNER TO spauser;

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_code_id_seq OWNED BY social_auth_code.id;


--
-- Name: social_auth_nonce; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_nonce (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    "timestamp" integer NOT NULL,
    salt character varying(65) NOT NULL
);


ALTER TABLE social_auth_nonce OWNER TO spauser;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_nonce_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_nonce_id_seq OWNER TO spauser;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_nonce_id_seq OWNED BY social_auth_nonce.id;


--
-- Name: social_auth_usersocialauth; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_usersocialauth (
    id integer NOT NULL,
    provider character varying(32) NOT NULL,
    uid character varying(255) NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE social_auth_usersocialauth OWNER TO spauser;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_usersocialauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_usersocialauth_id_seq OWNER TO spauser;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_usersocialauth_id_seq OWNED BY social_auth_usersocialauth.id;


--
-- Name: spaapp_actividadcomercial; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_actividadcomercial (
    id_actividad integer NOT NULL,
    tipo_actividad character varying(100) NOT NULL,
    detalles character varying(250) NOT NULL
);


ALTER TABLE spaapp_actividadcomercial OWNER TO spauser;

--
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_actividadcomercial_id_actividad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_actividadcomercial_id_actividad_seq OWNER TO spauser;

--
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_actividadcomercial_id_actividad_seq OWNED BY spaapp_actividadcomercial.id_actividad;


--
-- Name: spaapp_catalogoestado; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_catalogoestado (
    id_estado integer NOT NULL,
    estado character varying(50) NOT NULL
);


ALTER TABLE spaapp_catalogoestado OWNER TO spauser;

--
-- Name: spaapp_catalogoestado_id_estado_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_catalogoestado_id_estado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_catalogoestado_id_estado_seq OWNER TO spauser;

--
-- Name: spaapp_catalogoestado_id_estado_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_catalogoestado_id_estado_seq OWNED BY spaapp_catalogoestado.id_estado;


--
-- Name: spaapp_categoria; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_categoria (
    id_categoria integer NOT NULL,
    categoria character varying(150) NOT NULL,
    detalles character varying(250) NOT NULL,
    ruta_foto character varying(255)
);


ALTER TABLE spaapp_categoria OWNER TO spauser;

--
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_categoria_id_categoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_categoria_id_categoria_seq OWNER TO spauser;

--
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_categoria_id_categoria_seq OWNED BY spaapp_categoria.id_categoria;


--
-- Name: spaapp_ciudad; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_ciudad (
    id_ciudad integer NOT NULL,
    nombre character varying(80) NOT NULL,
    descripcion character varying(250) NOT NULL
);


ALTER TABLE spaapp_ciudad OWNER TO spauser;

--
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_ciudad_id_ciudad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_ciudad_id_ciudad_seq OWNER TO spauser;

--
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_ciudad_id_ciudad_seq OWNED BY spaapp_ciudad.id_ciudad;


--
-- Name: spaapp_cliente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_cliente (
    id_cliente integer NOT NULL,
    fechainicio_suscripcion date NOT NULL,
    fecha_pago date,
    detalles_adicionales character varying(250) NOT NULL,
    pedidos_activos boolean,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_cliente OWNER TO spauser;

--
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_cliente_id_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_cliente_id_cliente_seq OWNER TO spauser;

--
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_cliente_id_cliente_seq OWNED BY spaapp_cliente.id_cliente;


--
-- Name: spaapp_contrato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_contrato (
    id_contrato integer NOT NULL,
    codigo character varying(20) NOT NULL,
    detalles character varying(250) NOT NULL,
    vigencia integer NOT NULL,
    fecha_inicio date NOT NULL,
    "fechaFin" date NOT NULL,
    valor double precision NOT NULL,
    ruta_contrato character varying(100),
    id_proveedor_id integer,
    id_sucursal_id integer
);


ALTER TABLE spaapp_contrato OWNER TO spauser;

--
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_contrato_id_contrato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_contrato_id_contrato_seq OWNER TO spauser;

--
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_contrato_id_contrato_seq OWNED BY spaapp_contrato.id_contrato;


--
-- Name: spaapp_coordenadas; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_coordenadas (
    id_coordenadas integer NOT NULL,
    latitud character varying(100) NOT NULL,
    longitud character varying(100) NOT NULL,
    fecha timestamp with time zone,
    estado integer,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_coordenadas OWNER TO spauser;

--
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_coordenadas_id_coordenadas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_coordenadas_id_coordenadas_seq OWNER TO spauser;

--
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_coordenadas_id_coordenadas_seq OWNED BY spaapp_coordenadas.id_coordenadas;


--
-- Name: spaapp_detallepedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_detallepedido (
    id_detalle integer NOT NULL,
    valor double precision NOT NULL,
    cantidad_solicitada integer NOT NULL,
    id_pedido_id integer,
    id_plato_id integer,
    tamanio_id integer
);


ALTER TABLE spaapp_detallepedido OWNER TO spauser;

--
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_detallepedido_id_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_detallepedido_id_detalle_seq OWNER TO spauser;

--
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_detallepedido_id_detalle_seq OWNED BY spaapp_detallepedido.id_detalle;


--
-- Name: spaapp_direccion; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_direccion (
    id_direccion integer NOT NULL,
    calle_principal character varying(100) NOT NULL,
    calle_secundaria character varying(100) NOT NULL,
    numero_casa character varying(5),
    latitud character varying(100) NOT NULL,
    longitud character varying(100) NOT NULL,
    referencia character varying(250) NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_direccion OWNER TO spauser;

--
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_direccion_id_direccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_direccion_id_direccion_seq OWNER TO spauser;

--
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_direccion_id_direccion_seq OWNED BY spaapp_direccion.id_direccion;


--
-- Name: spaapp_horariotransportista; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_horariotransportista (
    id_horario integer NOT NULL,
    dias character varying(80) NOT NULL,
    horas character varying(50) NOT NULL,
    id_transportista_id integer NOT NULL
);


ALTER TABLE spaapp_horariotransportista OWNER TO spauser;

--
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_horariotransportista_id_horario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_horariotransportista_id_horario_seq OWNER TO spauser;

--
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_horariotransportista_id_horario_seq OWNED BY spaapp_horariotransportista.id_horario;


--
-- Name: spaapp_ingrediente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_ingrediente (
    id_ingrediente integer NOT NULL,
    nombre character varying(100) NOT NULL,
    id_proveedor_id integer
);


ALTER TABLE spaapp_ingrediente OWNER TO spauser;

--
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_ingrediente_id_ingrediente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_ingrediente_id_ingrediente_seq OWNER TO spauser;

--
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_ingrediente_id_ingrediente_seq OWNED BY spaapp_ingrediente.id_ingrediente;


--
-- Name: spaapp_menu; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_menu (
    id_menu integer NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_menu OWNER TO spauser;

--
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_menu_id_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_menu_id_menu_seq OWNER TO spauser;

--
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_menu_id_menu_seq OWNED BY spaapp_menu.id_menu;


--
-- Name: spaapp_modificardetallepedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_modificardetallepedido (
    id_modificardetallepedido integer NOT NULL,
    id_ingrediente_quitar_id integer,
    id_detallepedido_id integer
);


ALTER TABLE spaapp_modificardetallepedido OWNER TO spauser;

--
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_modificardetallepedido_id_modificardetallepedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_modificardetallepedido_id_modificardetallepedido_seq OWNER TO spauser;

--
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_modificardetallepedido_id_modificardetallepedido_seq OWNED BY spaapp_modificardetallepedido.id_modificardetallepedido;


--
-- Name: spaapp_pedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_pedido (
    id_pedido integer NOT NULL,
    fecha timestamp with time zone NOT NULL,
    observaciones character varying(250),
    valor double precision NOT NULL,
    tiempo integer,
    id_direccion_id integer,
    id_sucursal_id integer,
    id_tipo_pago_id integer,
    id_tipo_pedido_id integer,
    id_trasportista_id integer,
    usuario_id integer,
    id_estado_id integer,
    notificado boolean NOT NULL
);


ALTER TABLE spaapp_pedido OWNER TO spauser;

--
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_pedido_id_pedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_pedido_id_pedido_seq OWNER TO spauser;

--
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_pedido_id_pedido_seq OWNED BY spaapp_pedido.id_pedido;


--
-- Name: spaapp_perfil; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_perfil (
    id_perfil integer NOT NULL,
    cedula character varying(10),
    fecha_nacimiento date,
    telefono character varying(10),
    genero character varying(10),
    ruta_foto character varying(100) NOT NULL,
    sim_dispositivo character varying(30),
    id_usuario_id integer NOT NULL
);


ALTER TABLE spaapp_perfil OWNER TO spauser;

--
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_perfil_id_perfil_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_perfil_id_perfil_seq OWNER TO spauser;

--
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_perfil_id_perfil_seq OWNED BY spaapp_perfil.id_perfil;


--
-- Name: spaapp_plato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_plato (
    id_plato integer NOT NULL,
    nombre_producto character varying(200) NOT NULL,
    ruta_foto character varying(100) NOT NULL,
    descripcion character varying(200) NOT NULL,
    cantidad_diaria integer NOT NULL,
    venta_aproximada integer NOT NULL,
    hora_mayor_venta time without time zone NOT NULL,
    cantidad_mayor_venta integer NOT NULL,
    existencia_actual integer,
    id_platocategoria_id integer NOT NULL,
    activo boolean NOT NULL
);


ALTER TABLE spaapp_plato OWNER TO spauser;

--
-- Name: spaapp_plato_id_menu; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_plato_id_menu (
    id integer NOT NULL,
    plato_id integer NOT NULL,
    menu_id integer NOT NULL
);


ALTER TABLE spaapp_plato_id_menu OWNER TO spauser;

--
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_plato_id_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_plato_id_menu_id_seq OWNER TO spauser;

--
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_plato_id_menu_id_seq OWNED BY spaapp_plato_id_menu.id;


--
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_plato_id_plato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_plato_id_plato_seq OWNER TO spauser;

--
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_plato_id_plato_seq OWNED BY spaapp_plato.id_plato;


--
-- Name: spaapp_platocategoria; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_platocategoria (
    id_platocategoria integer NOT NULL,
    categoria character varying(50) NOT NULL
);


ALTER TABLE spaapp_platocategoria OWNER TO spauser;

--
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_platocategoria_id_platocategoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_platocategoria_id_platocategoria_seq OWNER TO spauser;

--
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_platocategoria_id_platocategoria_seq OWNED BY spaapp_platocategoria.id_platocategoria;


--
-- Name: spaapp_platoingrediente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_platoingrediente (
    id_plato_ingrediente integer NOT NULL,
    requerido boolean NOT NULL,
    horafecha timestamp with time zone NOT NULL,
    id_ingrediente_id integer NOT NULL,
    id_plato_id integer NOT NULL
);


ALTER TABLE spaapp_platoingrediente OWNER TO spauser;

--
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_platoingrediente_id_plato_ingrediente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_platoingrediente_id_plato_ingrediente_seq OWNER TO spauser;

--
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_platoingrediente_id_plato_ingrediente_seq OWNED BY spaapp_platoingrediente.id_plato_ingrediente;


--
-- Name: spaapp_proveedor; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_proveedor (
    id_proveedor integer NOT NULL,
    nombre_comercial character varying(100) NOT NULL,
    telefono character varying(10) NOT NULL,
    telefono_contacto character varying(10),
    prioridad integer NOT NULL,
    me_gusta integer,
    razon_social character varying(20) NOT NULL,
    id_actividad_id integer NOT NULL,
    id_categoria_id integer NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_proveedor OWNER TO spauser;

--
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_proveedor_id_proveedor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_proveedor_id_proveedor_seq OWNER TO spauser;

--
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_proveedor_id_proveedor_seq OWNED BY spaapp_proveedor.id_proveedor;


--
-- Name: spaapp_redsocial; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_redsocial (
    id_red_social integer NOT NULL,
    nombre_red_social character varying(50) NOT NULL,
    cuenta_persona character varying(100) NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_redsocial OWNER TO spauser;

--
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_redsocial_id_red_social_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_redsocial_id_red_social_seq OWNER TO spauser;

--
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_redsocial_id_red_social_seq OWNED BY spaapp_redsocial.id_red_social;


--
-- Name: spaapp_sucursal; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_sucursal (
    id_sucursal integer NOT NULL,
    nombre_sucursal character varying(100) NOT NULL,
    razon_social character varying(200) NOT NULL,
    telefono character varying(10) NOT NULL,
    telefono_contacto character varying(10),
    dias_atencion character varying(80) NOT NULL,
    hora_atencion character varying(50) NOT NULL,
    estado integer,
    prioridad integer NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    hora_pico time without time zone NOT NULL,
    me_gusta integer NOT NULL,
    id_ciudad_id integer NOT NULL,
    id_direccion_id integer,
    id_menu_id integer NOT NULL,
    id_proveedor_id integer NOT NULL,
    descripcion character varying(300),
    precio_envio double precision
);


ALTER TABLE spaapp_sucursal OWNER TO spauser;

--
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_sucursal_id_sucursal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_sucursal_id_sucursal_seq OWNER TO spauser;

--
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_sucursal_id_sucursal_seq OWNED BY spaapp_sucursal.id_sucursal;


--
-- Name: spaapp_tamanio; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tamanio (
    id_tamanio integer NOT NULL,
    tamanio character varying(50) NOT NULL
);


ALTER TABLE spaapp_tamanio OWNER TO spauser;

--
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tamanio_id_tamanio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tamanio_id_tamanio_seq OWNER TO spauser;

--
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tamanio_id_tamanio_seq OWNED BY spaapp_tamanio.id_tamanio;


--
-- Name: spaapp_tamanioplato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tamanioplato (
    id_tamanioplato integer NOT NULL,
    valor double precision NOT NULL,
    id_plato_id integer NOT NULL,
    id_tamanio_id integer NOT NULL,
    activo boolean NOT NULL
);


ALTER TABLE spaapp_tamanioplato OWNER TO spauser;

--
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tamanioplato_id_tamanioplato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tamanioplato_id_tamanioplato_seq OWNER TO spauser;

--
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tamanioplato_id_tamanioplato_seq OWNED BY spaapp_tamanioplato.id_tamanioplato;


--
-- Name: spaapp_tipopago; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tipopago (
    id_tipo_pago integer NOT NULL,
    tipo_pago character varying(50) NOT NULL,
    observaciones character varying(250) NOT NULL
);


ALTER TABLE spaapp_tipopago OWNER TO spauser;

--
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tipopago_id_tipo_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tipopago_id_tipo_pago_seq OWNER TO spauser;

--
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tipopago_id_tipo_pago_seq OWNED BY spaapp_tipopago.id_tipo_pago;


--
-- Name: spaapp_tipopedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tipopedido (
    id_tipo_pedido integer NOT NULL,
    tipo_pedido character varying(50) NOT NULL,
    observaciones character varying(250) NOT NULL
);


ALTER TABLE spaapp_tipopedido OWNER TO spauser;

--
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tipopedido_id_tipo_pedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tipopedido_id_tipo_pedido_seq OWNER TO spauser;

--
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tipopedido_id_tipo_pedido_seq OWNED BY spaapp_tipopedido.id_tipo_pedido;


--
-- Name: spaapp_transportista; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_transportista (
    id_transportista integer NOT NULL,
    tipo_transporte character varying(20) NOT NULL,
    tipo_servicio character varying(50) NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_transportista OWNER TO spauser;

--
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_transportista_id_transportista_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_transportista_id_transportista_seq OWNER TO spauser;

--
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_transportista_id_transportista_seq OWNED BY spaapp_transportista.id_transportista;


--
-- Name: spaapp_valoracion; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_valoracion (
    id_valoracion integer NOT NULL,
    valor_transportista integer NOT NULL,
    observaciones character varying(200) NOT NULL,
    valor_proveedor integer NOT NULL,
    valor_servicio integer NOT NULL,
    valoracion_general integer NOT NULL,
    id_cliente_id integer NOT NULL,
    id_pedido_id integer NOT NULL,
    id_proveedor_id integer NOT NULL,
    id_sucursal_id integer NOT NULL,
    id_transportista_id integer NOT NULL
);


ALTER TABLE spaapp_valoracion OWNER TO spauser;

--
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_valoracion_id_valoracion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_valoracion_id_valoracion_seq OWNER TO spauser;

--
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_valoracion_id_valoracion_seq OWNED BY spaapp_valoracion.id_valoracion;


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY "administracionAPP_contacto" ALTER COLUMN id SET DEFAULT nextval('"administracionAPP_contacto_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_association ALTER COLUMN id SET DEFAULT nextval('social_auth_association_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code ALTER COLUMN id SET DEFAULT nextval('social_auth_code_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce ALTER COLUMN id SET DEFAULT nextval('social_auth_nonce_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth ALTER COLUMN id SET DEFAULT nextval('social_auth_usersocialauth_id_seq'::regclass);


--
-- Name: id_actividad; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_actividadcomercial ALTER COLUMN id_actividad SET DEFAULT nextval('spaapp_actividadcomercial_id_actividad_seq'::regclass);


--
-- Name: id_estado; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_catalogoestado ALTER COLUMN id_estado SET DEFAULT nextval('spaapp_catalogoestado_id_estado_seq'::regclass);


--
-- Name: id_categoria; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_categoria ALTER COLUMN id_categoria SET DEFAULT nextval('spaapp_categoria_id_categoria_seq'::regclass);


--
-- Name: id_ciudad; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ciudad ALTER COLUMN id_ciudad SET DEFAULT nextval('spaapp_ciudad_id_ciudad_seq'::regclass);


--
-- Name: id_cliente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente ALTER COLUMN id_cliente SET DEFAULT nextval('spaapp_cliente_id_cliente_seq'::regclass);


--
-- Name: id_contrato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato ALTER COLUMN id_contrato SET DEFAULT nextval('spaapp_contrato_id_contrato_seq'::regclass);


--
-- Name: id_coordenadas; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas ALTER COLUMN id_coordenadas SET DEFAULT nextval('spaapp_coordenadas_id_coordenadas_seq'::regclass);


--
-- Name: id_detalle; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido ALTER COLUMN id_detalle SET DEFAULT nextval('spaapp_detallepedido_id_detalle_seq'::regclass);


--
-- Name: id_direccion; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion ALTER COLUMN id_direccion SET DEFAULT nextval('spaapp_direccion_id_direccion_seq'::regclass);


--
-- Name: id_horario; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista ALTER COLUMN id_horario SET DEFAULT nextval('spaapp_horariotransportista_id_horario_seq'::regclass);


--
-- Name: id_ingrediente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente ALTER COLUMN id_ingrediente SET DEFAULT nextval('spaapp_ingrediente_id_ingrediente_seq'::regclass);


--
-- Name: id_menu; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu ALTER COLUMN id_menu SET DEFAULT nextval('spaapp_menu_id_menu_seq'::regclass);


--
-- Name: id_modificardetallepedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido ALTER COLUMN id_modificardetallepedido SET DEFAULT nextval('spaapp_modificardetallepedido_id_modificardetallepedido_seq'::regclass);


--
-- Name: id_pedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido ALTER COLUMN id_pedido SET DEFAULT nextval('spaapp_pedido_id_pedido_seq'::regclass);


--
-- Name: id_perfil; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil ALTER COLUMN id_perfil SET DEFAULT nextval('spaapp_perfil_id_perfil_seq'::regclass);


--
-- Name: id_plato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato ALTER COLUMN id_plato SET DEFAULT nextval('spaapp_plato_id_plato_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu ALTER COLUMN id SET DEFAULT nextval('spaapp_plato_id_menu_id_seq'::regclass);


--
-- Name: id_platocategoria; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platocategoria ALTER COLUMN id_platocategoria SET DEFAULT nextval('spaapp_platocategoria_id_platocategoria_seq'::regclass);


--
-- Name: id_plato_ingrediente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente ALTER COLUMN id_plato_ingrediente SET DEFAULT nextval('spaapp_platoingrediente_id_plato_ingrediente_seq'::regclass);


--
-- Name: id_proveedor; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor ALTER COLUMN id_proveedor SET DEFAULT nextval('spaapp_proveedor_id_proveedor_seq'::regclass);


--
-- Name: id_red_social; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial ALTER COLUMN id_red_social SET DEFAULT nextval('spaapp_redsocial_id_red_social_seq'::regclass);


--
-- Name: id_sucursal; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal ALTER COLUMN id_sucursal SET DEFAULT nextval('spaapp_sucursal_id_sucursal_seq'::regclass);


--
-- Name: id_tamanio; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanio ALTER COLUMN id_tamanio SET DEFAULT nextval('spaapp_tamanio_id_tamanio_seq'::regclass);


--
-- Name: id_tamanioplato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato ALTER COLUMN id_tamanioplato SET DEFAULT nextval('spaapp_tamanioplato_id_tamanioplato_seq'::regclass);


--
-- Name: id_tipo_pago; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopago ALTER COLUMN id_tipo_pago SET DEFAULT nextval('spaapp_tipopago_id_tipo_pago_seq'::regclass);


--
-- Name: id_tipo_pedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopedido ALTER COLUMN id_tipo_pedido SET DEFAULT nextval('spaapp_tipopedido_id_tipo_pedido_seq'::regclass);


--
-- Name: id_transportista; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista ALTER COLUMN id_transportista SET DEFAULT nextval('spaapp_transportista_id_transportista_seq'::regclass);


--
-- Name: id_valoracion; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion ALTER COLUMN id_valoracion SET DEFAULT nextval('spaapp_valoracion_id_valoracion_seq'::regclass);


--
-- Data for Name: administracionAPP_contacto; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY "administracionAPP_contacto" (id, nombre_propietario, apellidos, ciudad, nombre_empresa, direccion, tipo_negocio, telefono) FROM stdin;
\.


--
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('"administracionAPP_contacto_id_seq"', 1, false);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_group (id, name) FROM stdin;
2	Proveedor
3	Transportista
1	Cliente
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_id_seq', 3, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	116
2	1	117
3	1	120
4	2	116
5	2	115
6	2	117
7	2	118
8	2	119
9	2	122
10	3	115
11	3	118
12	3	119
13	3	121
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 14, true);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	2	add_logentry
2	Can change log entry	2	change_logentry
3	Can delete log entry	2	delete_logentry
4	Can add permission	3	add_permission
5	Can change permission	3	change_permission
6	Can delete permission	3	delete_permission
7	Can add group	4	add_group
8	Can change group	4	change_group
9	Can delete group	4	delete_group
10	Can add user	5	add_user
11	Can change user	5	change_user
12	Can delete user	5	delete_user
13	Can add content type	6	add_contenttype
14	Can change content type	6	change_contenttype
15	Can delete content type	6	delete_contenttype
16	Can add session	7	add_session
17	Can change session	7	change_session
18	Can delete session	7	delete_session
19	Can add Actividad Comercial	8	add_actividadcomercial
20	Can change Actividad Comercial	8	change_actividadcomercial
21	Can delete Actividad Comercial	8	delete_actividadcomercial
22	Can add perfil	9	add_perfil
23	Can change perfil	9	change_perfil
24	Can delete perfil	9	delete_perfil
25	Can add categoria	10	add_categoria
26	Can change categoria	10	change_categoria
27	Can delete categoria	10	delete_categoria
28	Can add proveedor	11	add_proveedor
29	Can change proveedor	11	change_proveedor
30	Can delete proveedor	11	delete_proveedor
31	Can add ciudad	12	add_ciudad
32	Can change ciudad	12	change_ciudad
33	Can delete ciudad	12	delete_ciudad
34	Can add direccion	13	add_direccion
35	Can change direccion	13	change_direccion
36	Can delete direccion	13	delete_direccion
37	Can add cliente	14	add_cliente
38	Can change cliente	14	change_cliente
39	Can delete cliente	14	delete_cliente
40	Can add menu	15	add_menu
41	Can change menu	15	change_menu
42	Can delete menu	15	delete_menu
43	Can add sucursal	16	add_sucursal
44	Can change sucursal	16	change_sucursal
45	Can delete sucursal	16	delete_sucursal
46	Can add contrato	17	add_contrato
47	Can change contrato	17	change_contrato
48	Can delete contrato	17	delete_contrato
49	Can add transportista	18	add_transportista
50	Can change transportista	18	change_transportista
51	Can delete transportista	18	delete_transportista
52	Can add Coordenada	19	add_coordenadas
53	Can change Coordenada	19	change_coordenadas
54	Can delete Coordenada	19	delete_coordenadas
55	Can add tipo pago	20	add_tipopago
56	Can change tipo pago	20	change_tipopago
57	Can delete tipo pago	20	delete_tipopago
58	Can add tipo pedido	21	add_tipopedido
59	Can change tipo pedido	21	change_tipopedido
60	Can delete tipo pedido	21	delete_tipopedido
61	Can add pedido	22	add_pedido
62	Can change pedido	22	change_pedido
63	Can delete pedido	22	delete_pedido
64	Can add plato categoria	23	add_platocategoria
65	Can change plato categoria	23	change_platocategoria
66	Can delete plato categoria	23	delete_platocategoria
67	Can add plato	24	add_plato
68	Can change plato	24	change_plato
69	Can delete plato	24	delete_plato
70	Can add tamanio	25	add_tamanio
71	Can change tamanio	25	change_tamanio
72	Can delete tamanio	25	delete_tamanio
73	Can add tamanio plato	26	add_tamanioplato
74	Can change tamanio plato	26	change_tamanioplato
75	Can delete tamanio plato	26	delete_tamanioplato
76	Can add Detalle de pedido	27	add_detallepedido
77	Can change Detalle de pedido	27	change_detallepedido
78	Can delete Detalle de pedido	27	delete_detallepedido
79	Can add ingrediente	28	add_ingrediente
80	Can change ingrediente	28	change_ingrediente
81	Can delete ingrediente	28	delete_ingrediente
82	Can add Horario de transportista	29	add_horariotransportista
83	Can change Horario de transportista	29	change_horariotransportista
84	Can delete Horario de transportista	29	delete_horariotransportista
85	Can add modificardetallepedido	30	add_modificardetallepedido
86	Can change modificardetallepedido	30	change_modificardetallepedido
87	Can delete modificardetallepedido	30	delete_modificardetallepedido
88	Can add plato ingrediente	31	add_platoingrediente
89	Can change plato ingrediente	31	change_platoingrediente
90	Can delete plato ingrediente	31	delete_platoingrediente
91	Can add Red social	32	add_redsocial
92	Can change Red social	32	change_redsocial
93	Can delete Red social	32	delete_redsocial
94	Can add valoracion	33	add_valoracion
95	Can change valoracion	33	change_valoracion
96	Can delete valoracion	33	delete_valoracion
97	Can add contacto	34	add_contacto
98	Can change contacto	34	change_contacto
99	Can delete contacto	34	delete_contacto
100	Can add cors model	35	add_corsmodel
101	Can change cors model	35	change_corsmodel
102	Can delete cors model	35	delete_corsmodel
103	Can add user social auth	36	add_usersocialauth
104	Can change user social auth	36	change_usersocialauth
105	Can delete user social auth	36	delete_usersocialauth
106	Can add nonce	37	add_nonce
107	Can change nonce	37	change_nonce
108	Can delete nonce	37	delete_nonce
109	Can add association	38	add_association
110	Can change association	38	change_association
111	Can delete association	38	delete_association
112	Can add code	39	add_code
113	Can change code	39	change_code
114	Can delete code	39	delete_code
115	Read Profile Client Restricted	9	rpcr
116	Read Profile Transportista Acceso Total	9	rpta
117	Read Transportista Restricted	18	rtr
118	Read Client Restricted	14	rrc
119	Read Direccion Cliente Restricted	13	rdcr
120	Es Cliente	14	esCliente
121	Es Transportista	18	esTransportista
122	Es Proveedor	11	esProveedor
123	Can add catalogo estado	40	add_catalogoestado
124	Can change catalogo estado	40	change_catalogoestado
125	Can delete catalogo estado	40	delete_catalogoestado
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_permission_id_seq', 125, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
12	pbkdf2_sha256$24000$HtuXmxTeRqyL$Njr1uHceH9EZxGJiFe4gXIkGCJJbwCuqkh5M1yYPwJQ=	2016-08-05 09:50:32.752293-05	f	mileiva	Milton	Leiva	mileiva@utpl.edu.ec	f	t	2016-08-01 14:33:19.44345-05
8	pbkdf2_sha256$24000$qNZ7WHHyykAy$b2mSftD201hNXb7dyy+usYfMa4NDyAbC4NA8UwYgH9Y=	\N	f	chamelion				f	t	2016-07-28 20:11:47.163683-05
10	pbkdf2_sha256$24000$6bgVcVaYAXwS$NU+zXJH/GgDBbl2eWk0clCxHnqyM/IsVng1kGmyqiO4=	\N	f	tonys				f	t	2016-07-28 20:14:12.452188-05
29	pbkdf2_sha256$24000$rWQhXSVXEqUT$3jpAuJqbaHY9LeMWeSck5Xpwxw9fIv3w6HjLiiNfkws=	\N	f	posix	posix	posix	p@g.com	f	t	2016-08-08 11:24:41.153563-05
20	pbkdf2_sha256$24000$8tUyY5ocvtPb$6znCQT7l8mjDlh6lMs3iD50NraNTEYQ6NlpbFlmh/dg=	\N	f	hola1	hola	hola	hola@gmail.com	f	t	2016-08-05 19:52:01.007523-05
14	pbkdf2_sha256$24000$bdfVfg5iZZ9h$+fr24IGm0bACx/2yE0HqDS8W2TUoi/gLNeuk9MIXuf4=	2016-08-01 18:48:42.009257-05	f	hola	Hola	Hola	aaaa@hhh.com	f	t	2016-08-01 18:48:35.863843-05
40	pbkdf2_sha256$24000$eOm7DjXmgPYw$OrH6LjYQkEJo0GAKVV9pQAzSkJE3oanSQ2h1OHJ8Mh4=	\N	f	ggggg	ggggg	ggg	byjose007@gmailk.com	f	t	2016-08-09 16:39:52.722905-05
32	pbkdf2_sha256$24000$LaYbtKRE4P65$yLVi3uaj2ms5M4uhq1aFgaehGO1MKZawJqQuxSD1XRs=	2016-08-08 12:43:57.56415-05	f	pameliz	pameliz	pameliz	pam@gmail.com	f	t	2016-08-08 12:37:23.399844-05
6	pbkdf2_sha256$24000$0j2DG22e2YHH$fo/V5vWPWU3iIhWUaQbJp6KoUAHlH9ethCCPyD7N7fg=	2016-07-28 20:44:49-05	f	chino				f	t	2016-07-28 20:08:58-05
2	pbkdf2_sha256$24000$VZBS5yhJk5U5$GhXFXCKGJ+XpgWULSIxDvbeUO259YcxyOtBtJaA38nc=	2016-08-25 10:17:39.312034-05	t	admin			bsbnadiapc@hotmail.com	t	t	2016-07-28 19:56:26.739528-05
13	pbkdf2_sha256$24000$Sz60GFfE1kLG$zH+oz3wOIk5vxngO0xey67vJUU0pPD0nYQUp1g4WnCY=	2016-08-09 15:24:26.1701-05	f	nadia	Nadia	aaaa	aaaa@hhh.com	f	t	2016-08-01 17:28:47.462339-05
3	pbkdf2_sha256$24000$KqApxeOZFuHb$j6umz0UhXih1KqOfFr2BFm3XiPWfeRKsZ/KPE3rNarU=	2016-08-25 11:20:39.08256-05	f	transportista				f	t	2016-07-28 19:59:20-05
41	pbkdf2_sha256$24000$l6JJUJQbiFht$tHibNZFFB+BFkCNdICFE8T/qAVn1la8za62urt/Pq1I=	\N	f	dickson	1234	á	a@b.com	f	t	2016-08-09 19:15:59.095716-05
45	pbkdf2_sha256$24000$4ph6pyXiCrVq$Lxk6rWRH0654OjCAapN8CG57YjFzcs0FWd093MGQ5aQ=	\N	f	hfh	hdhd	hshs	hs@vdv.es	f	t	2016-08-09 19:51:32.811464-05
16	pbkdf2_sha256$24000$7lmMQlLRKqBQ$SThCWqPoo2ib/S0qlzjhqFmkUJyo+i6WsJWWtj5+AXE=	2016-08-02 17:37:56.387539-05	f	pppp	PPPP	pPPP	aaaa@hhh.com	f	t	2016-08-01 18:59:59.33102-05
42	pbkdf2_sha256$24000$vLAIcswLW1fo$TgdujnhcrX/XPF2Z1kPRA58VgNfHP9JIp7duyLoJQz8=	\N	f	dickson2	123456	12345	dk@armijos.es	f	t	2016-08-09 19:23:18.809872-05
7	pbkdf2_sha256$24000$Y2i7bGayjd5J$VeG6xu3ek/0efjYyfU2RTGJugYU6rC+dOSTHzSQpReo=	2016-08-23 16:39:28.700675-05	f	icecreamsocial				f	t	2016-07-28 20:11:05-05
22	pbkdf2_sha256$24000$fdoln7v3p3bP$D38T5HjBaxf5MvSvbpR1AbYUQjxjp93km/gKCG0oxQc=	\N	f	armijos	armijos	armijos	a@gmail.com	f	t	2016-08-08 10:50:45.620835-05
23	pbkdf2_sha256$24000$x4K27HieSbIh$sFomLx85rsa3PfT0KrnyvWkU0iVRThXO3l3xFzwgQn8=	\N	f	lola	lola	lola	lola@gmail.com	f	t	2016-08-08 10:52:35.311279-05
17	pbkdf2_sha256$24000$DAf1nJvKiWCU$zPIhLzMZCbL/52Z19+Xd268H0gxhzhrJql2Yj5yZ104=	2016-08-04 18:13:43.618189-05	f	nadia2	nnnnn	ppppp	aaaa@hhh.com	f	t	2016-08-02 16:25:32.907172-05
24	pbkdf2_sha256$24000$YzyCSIGo4tBx$nFiqksbSwNhADVVyTcMFhRF1rHgZPvP636Sy9GgoLjc=	\N	f	n	n	n	ne@gmail.com	f	t	2016-08-08 10:53:29.428634-05
33	pbkdf2_sha256$24000$qRL4JaadYyuz$jlvcSlYhwPyaQFkLjRtAMnYuUOAy74ISYygZjquDkd0=	2016-08-08 13:23:02.567125-05	f	prueba	prueba	prueba	prueba@g.com	f	t	2016-08-08 13:22:57.470733-05
35	pbkdf2_sha256$24000$IXj7etw1ymAT$3qabLqp5xjZyRAzxgFOJIIuLpbmKTUyawOOclD5WDoA=	2016-08-09 06:53:43.881826-05	f	pamm	pam	pam	p@g.com	f	t	2016-08-09 05:54:02.815123-05
18	pbkdf2_sha256$24000$WrOICHZliOuI$CO/04wIW2F7rgpBDggBQarA7iJwSIxqMYaQpwB9qvEY=	2016-08-03 17:33:18.90328-05	f	nadia3	aaaaa	bbbbb	aaaa@hhh.com	f	t	2016-08-02 18:42:59.561961-05
27	pbkdf2_sha256$24000$D1LoQwU5rAnh$Q7TzJlxWCbdNTvXMCtJAke73gGtGfxJOn+dAsucmVHQ=	\N	f	lik	lik	lik	lik@gm.com	f	t	2016-08-08 11:12:57.590545-05
28	pbkdf2_sha256$24000$dVQdyT5holv8$dW9bat1mwUjjHS4aqu9tUqEOSgagLdT5qj9mv+l+pWc=	\N	f	lim	lim	lim	lim@g.com	f	t	2016-08-08 11:13:51.257095-05
47	pbkdf2_sha256$24000$nm3Uz6fJlo1s$uLkw8KZXzOJSO6pird2uqnLlcBfN0XROOhyyYkA5l1Y=	2016-08-11 10:07:48.26449-05	f	clienete2	test	test	test@sds.com	f	t	2016-08-10 13:39:43.598523-05
30	pbkdf2_sha256$24000$f8ilTqq8jFyM$IymYONPv/IGCfmi5WOIZCR/O5hIr5PGwScPS+DuYkRs=	2016-08-09 05:15:08.683906-05	f	nich	nic	nic	nic@gmail.com	f	t	2016-08-08 11:30:51.859126-05
39	pbkdf2_sha256$24000$AH0NDEBr5YOP$0C52aH+LQTUBvpCZtltiq4ttUH7ZOJ5sCUoDYzJqSnU=	\N	f	byjose007			byjose007@gmail.com	f	t	2016-08-09 16:34:04.336207-05
43	pbkdf2_sha256$24000$j5lgTTFcK6Zs$KPpLm7wUkw0DX7zG6R2wcedBRxId5tvH6gxljIH/Izk=	2016-08-09 19:32:14.658534-05	f	dickson4	dickson	armijos	darmijos@cid.com	f	t	2016-08-09 19:32:04.80755-05
46	pbkdf2_sha256$24000$XTM2ANtKeOJ9$ggptbuv/HwKT/k+skv+TZAlSBvy9zR3zbY/+HGJAFHQ=	\N	f	jiplaza	cheo	plaza	jiplaza@hotmail.com	f	t	2016-08-10 10:53:59.921709-05
4	pbkdf2_sha256$24000$QHY0JgIxGO9D$wuYMTVHeL9UK9xtD2JbVBlWbLK54clvqJ3Obv4Iz4Is=	2016-08-25 11:19:28.575114-05	f	pizza				f	t	2016-07-28 20:05:40-05
44	pbkdf2_sha256$24000$GnDI7xJh3AvU$3j6CZHtiWbze5pJRax2hCH5PYc+PYKYYgfCM+mMlksQ=	\N	f	:Carlos	huds	hsus	lala@lala.com	f	t	2016-08-09 19:44:30.674755-05
19	pbkdf2_sha256$24000$pfB7o1M68V2N$w6wC7s6FnZAel49w/8iiaCuf5QD7z+WDkIm1sOvMsVA=	2016-08-17 19:10:05.036627-05	f	cliente3	nico	git	nico@git.com	f	t	2016-08-03 15:13:07.92167-05
48	pbkdf2_sha256$24000$5rrscuA7H6Xx$mAa5n5eRRe9mFNALSnDw+De1Wwgcx/PBKZoyiml3dPI=	\N	f	fsfds	fsdfs	fsdfs	admin@gmal.com	f	t	2016-08-10 13:56:30.11389-05
55	pbkdf2_sha256$24000$2ypgkkhYXxxY$jp02ldBZKDXz7jEa8gTV0k0RJR9pFSaiplOaCn8d484=	\N	f	fruques				f	t	2016-08-10 18:35:16.025117-05
34	pbkdf2_sha256$24000$EEoyJFCTc35v$4yonVwLoOl2bMffGVIBrMi7WdCPMavNE3UrU0/vt0ho=	2016-08-10 12:49:58.637184-05	f	pam	pam	pam	pam@g.com	f	t	2016-08-08 13:47:18.679485-05
11	pbkdf2_sha256$24000$HrliZ4y5yUNX$fvKgnoXbuOxep2DR9fE6qicpX0UJLn3D/KONvwgjFvk=	2016-08-16 15:21:04.88231-05	f	cliente1				f	t	2016-07-28 20:46:07.635535-05
49	pbkdf2_sha256$24000$ECZLlIV3CBFY$tyaEGp1c14a8x7talemqFUhY+iLd96dEbwwjNdkM51A=	\N	f	jiplaza123	julio	julio	jiplaza123@gmail.com	f	t	2016-08-10 14:15:05.821013-05
50	pbkdf2_sha256$24000$g2O02VcFzgzy$ge+D8ISpcV8nKJGpgwJa193vxEZ6LFXYSSPnNjDy86M=	\N	f	usuario	ggfg	ggfg	usaurio@g.com	f	t	2016-08-10 14:16:56.515237-05
51	pbkdf2_sha256$24000$DlXBalScBeN5$xkHOObPYobCG43/DDrjwgKNxTDVbikjhBEQX6LXDGPI=	\N	f	usuario100	usuario	usuario	usuario100@gmail.com	f	t	2016-08-10 14:20:50.565852-05
52	pbkdf2_sha256$24000$JQsCPwoMigxa$UN+TUp1A4c7WCw86L5Q+Jjw9zYQtaYX/FxKIXPm1ZGM=	2016-08-10 14:23:00.398317-05	f	otro	otro	otro	otro@g.com	f	t	2016-08-10 14:23:00.225131-05
53	pbkdf2_sha256$24000$6Q0sQ1aAgsUj$mHHyAU3cY38zdUt0aBaDpyamPDTYPNhsM8Ip95QZBMA=	2016-08-10 14:26:40.038601-05	f	lili90	lili	lili	lili90@gmial.com	f	t	2016-08-10 14:26:39.878526-05
5	pbkdf2_sha256$24000$tbf3Jp4deRty$zN4o4r9lhHZk6SEN/t4iC4DnnNwg/UkREFlFqrO4ZSc=	2016-08-23 09:44:52.928906-05	f	chinito				f	t	2016-07-28 20:07:12-05
54	pbkdf2_sha256$24000$YYQIee4Cn5LH$pYpRA+scdS60DuoGhI76HMLgP9LnpqYFW9zWl78uMS4=	2016-08-10 16:22:07.788335-05	f	John	John	Ramirez	john@gmail.com	f	t	2016-08-10 16:21:50.189036-05
56	pbkdf2_sha256$24000$1abfbcTbPOaN$mRS0jHdIMvzkHgQuWq3mxT8kK4z04pjvIT1+qrruGr8=	\N	f	fruques2				f	t	2016-08-10 18:35:39.383756-05
9	pbkdf2_sha256$24000$ey7AkLLlNEzT$TtYnVywsZikZa1yFunbtK8q2xwXJ+yJLCWXR3ooS5IE=	2016-08-18 10:00:32.619914-05	f	papalogo				f	t	2016-07-28 20:13:21-05
77	pbkdf2_sha256$24000$xO3TbUc8yF3x$0TTzQGzFLfyzwaEvJUr96mEaCH2QIgQJpjD1Bx5EIp4=	2016-08-16 17:47:29.027042-05	f	chochi	Rosa	Cañar	rosa@gmail.com	f	t	2016-08-16 16:59:35.98184-05
57	pbkdf2_sha256$24000$QinbjW8Hl45T$QQe5i6ZzJviAx07ls8SGze3MIU/Rj43aNkn0yGY6ywA=	2016-08-10 18:35:37.735577-05	f	joselo	josel	joselo	jo@gmail.com	f	t	2016-08-10 18:35:26.383901-05
58	pbkdf2_sha256$24000$sQCrglD5MIya$/NXgFDVXfFSJ+ouzRRtIUmNqrCNJtTbZkMBTrMCEJpQ=	2016-08-10 18:46:15.316144-05	f	hhhh	hhhh	hhhh	hhh@gmail.com	f	t	2016-08-10 18:46:15.09987-05
90		2016-08-17 13:52:13.735058-05	f	ee	ee	ee	ee@g.com	f	t	2016-08-17 13:52:08.401997-05
68	pbkdf2_sha256$24000$VQ4PjZ1MfcSb$b/fAadFAv86RCQk+vAxxVmP28PTLG21uYgpIiSd6wJM=	2016-08-16 18:01:43.392105-05	f	pamelix	pame	guaman	pab@g.com	f	t	2016-08-11 17:11:43.549756-05
60	pbkdf2_sha256$24000$sJv3oB5slz05$tk5qVPLrkIWIiua9cMEAskdlWpM5UH1MSIj5sNAyxb4=	2016-08-10 19:06:34.980909-05	f	geantonxx	Jorge Antonio	Jorge Antonio	geantonxx@gmail.com	f	t	2016-08-10 19:02:25.13738-05
73	pbkdf2_sha256$24000$lcKcmZNCnGE6$SkUA3OYQm0wl/nX/iaSp4YzbeCUEGX6XHFcxMTG90pA=	2016-08-15 10:27:31.46194-05	f	otro2	otro	otro	otro@gmil.com	f	t	2016-08-15 10:27:31.091866-05
61	pbkdf2_sha256$24000$wT8KRWyOoFQ8$Me/1U4w54Yu3ImCI8BMj+EJDbrp8LChnJfqxKTVpgng=	2016-08-11 12:02:07.011236-05	f	fdfdfdas	gdgdf	gdgdf	fdf@dd.com	f	t	2016-08-11 12:02:06.750506-05
62	pbkdf2_sha256$24000$akP8z4yyn4Zh$SlW3NVuE1vb5UkRj6UcD3bgJFrDYTiRNg6XZ2bMzd+s=	\N	f	gusy2				f	t	2016-08-11 14:05:00-05
63	pbkdf2_sha256$24000$ps0kXBqbRyF9$alEIcmLTh1D/GXxNNl/IPioEdPKrU+RyPZB0xLJVan0=	\N	f	gusy				f	t	2016-08-11 14:09:37.346277-05
64	pbkdf2_sha256$24000$JsKK6QbPPJLi$2zc3pzkVGewVl5kZn8xQpgn4B6M1DTnhsYAxpGmFQ4s=	2016-08-11 16:13:44.685119-05	f	rosi	rosi	rosi	rosi@gmail.com	f	t	2016-08-11 16:13:31.550396-05
65	pbkdf2_sha256$24000$GKJ8bIMqcOpE$y+7f75xx3xLwBfMG0RxcruW7slTy0KfkUYLl8FPD54k=	\N	f	roch	roch	roch	ro@g.com	f	t	2016-08-11 16:23:58.503125-05
87		2016-08-17 13:37:14.762244-05	f	polo	polo2	polo	polo@g.com	f	t	2016-08-17 13:37:09.258242-05
66	pbkdf2_sha256$24000$yKS3s9YMXd0X$Rhmvk8P9FvBCGPk4J8rYfZKBIhy/wtbOKnoEUSsQ8e8=	2016-08-11 16:24:30.246739-05	f	patch	patch	patch	p@g.com	f	t	2016-08-11 16:24:21.562072-05
67	pbkdf2_sha256$24000$BiqNQiNbd52W$I2kKmg5sXbB7gUgUneXvnDTYB54C9Nr+QGRr80g8fIg=	\N	f	pfguaman	pfguaman	pfguaman	pfguaman95@gmail.com	f	t	2016-08-11 16:28:59.908207-05
83		2016-08-17 12:33:59.289641-05	f	cosme	cosme	guerrero	cosme@gmail.com	f	t	2016-08-17 12:23:14.151109-05
69	pbkdf2_sha256$24000$Nt5i51EwJk79$jZP8VL6LKCm/CckwOd/3fmsV5tB+d/zzxBNIa1e6TFE=	\N	f	pamelixx	pam	gu	alex@gmail.com	f	t	2016-08-11 17:28:56.902664-05
59	pbkdf2_sha256$24000$SxK81uzqt4jw$Vt/f6qFiX0ZF+tkckIMsMBofauRjtCS0Bnc4N+HmqBQ=	2016-08-11 17:43:50.003002-05	f	rommac	Rommel	Macas	rommelmacas@gmail.com	f	t	2016-08-10 19:00:10.588523-05
70	pbkdf2_sha256$24000$294T1AdjiXX1$fPlN6Wcxbj/6t1t4yP/k4He1PiZFShbbFpYS0DmtBVo=	\N	f	pamellix	pam	gu	g@gm.com	f	t	2016-08-11 18:05:37.296967-05
71	pbkdf2_sha256$24000$6QzuAg8lkEtT$xz8IZSpDh7PcTWWq0O/JzvK5v8FJB3coZXSwSSoOwSY=	\N	f	pal	@po	kl	kl@gm.com	f	t	2016-08-11 18:06:13.507026-05
72	pbkdf2_sha256$24000$XHhRYrUJezW2$d0eOXaWLkJ86ES12Y4dEvXMvsRuXFrXnbijhoviuHWk=	2016-08-11 18:10:38.707912-05	f	nicolll	po	po	pamefer.95@gmail.com	f	t	2016-08-11 18:10:29.285309-05
98	pbkdf2_sha256$24000$zQNxhXpohsMr$XdWdzkZiVB3+mrr01xi1RN5pm7GziuPZ9vPwqM+BayA=	2016-08-17 17:22:00.057227-05	f	loxadance	loxadance	loxadance	loxa@gml.com	f	t	2016-08-17 17:21:59.859358-05
75	pbkdf2_sha256$24000$HcHRy2pGSCdX$4zgvzfDXdsQaRWA7OZWQH06C5zZDJJUtuBl36pXC/ZA=	2016-08-15 13:26:21.082385-05	f	cliente4	cliente4	cliente4	ette@sds.com	f	t	2016-08-15 13:26:01.48519-05
85	pbkdf2_sha256$24000$S1fN5qCpz4bf$5/4Y6bmxLAhwtZPn+jheYtRGdpyQ8KKdTy6Xq2QoVL4=	2016-08-17 13:27:54.953883-05	f	medicenter	medicenter	medicenter	med@gmail.com	f	t	2016-08-17 13:27:44.280023-05
36	pbkdf2_sha256$24000$YCwFQVUB2gl5$eT7TGNW6XdwtEBgKaJnXFfQV4Itrx9aIrXleFPw2f5I=	2016-08-17 13:30:26.789991-05	f	alex	Alex	Guaman	alex@gmail.com	f	t	2016-08-09 07:19:20.199137-05
81	pbkdf2_sha256$24000$kld08MuZ7Kb2$+yjYkLlQc5iQPn7h+WRoRBIPYWx5gdZZz7K+/QB0CN0=	2016-08-17 11:07:53.013739-05	f	loxad	loxa	loxa	adf@gmail.com	f	t	2016-08-17 11:07:45.900051-05
78	pbkdf2_sha256$24000$izytqEXwRxJp$hRQMIute0qTHGjzuWoSNjZw13jvm5QzV0vyH+cwQiGw=	2016-08-16 17:10:38.308729-05	f	milton13	milton	milton	mil@g.com	f	t	2016-08-16 17:10:38.160711-05
97	pbkdf2_sha256$24000$dln396v7L9Iq$wQQWfrqyi0CbZWCm2T0BME0JpqFAHTRkq9U+SJMnd2k=	2016-08-17 17:23:37.712393-05	f	rolax	rolax	rolax	rolax@g.com	f	t	2016-08-17 16:38:22.549634-05
80	pbkdf2_sha256$24000$Ma0sXLrf1WFm$+Nl5V1qI4pFDBmwNi/27f5psrB5P2ny281tVYoPAWeU=	2016-08-17 09:29:20.392993-05	t	by-default	Dickson	Armijos	by-default@hotmail.com	f	t	2016-08-17 09:28:57.627684-05
79	pbkdf2_sha256$24000$E9Yhk6qcg52f$xboRaxEhWcqIolBzyhbJuX3O77/FRwL9KmD+FKyTI8Q=	2016-08-16 17:13:48.639453-05	f	cheo22	cheo22	cheo22	cheo@g.com	f	t	2016-08-16 17:13:48.497496-05
91		2016-08-17 13:54:04.609421-05	f	tt	tt	tt	tt@g.com	f	t	2016-08-17 13:53:59.444435-05
88		2016-08-17 13:47:07.060031-05	f	loco	loco2	loco	loco@g.com	f	t	2016-08-17 13:46:58.691684-05
86		2016-08-17 13:31:12.237169-05	f	aaa	aaa2	aaa	aa@gmail.commm	f	t	2016-08-17 13:31:05.632706-05
82	pbkdf2_sha256$24000$ZZVzdkZivBhp$1rBrrZgNeAGniOeCVYBYQb7ezG5RXbdNq3mYzG99eMI=	2016-08-19 14:38:01.000449-05	f	rosax	Pamela	Guamán	rosa_1994@gmail.com	f	t	2016-08-17 12:09:27.225927-05
89		2016-08-17 13:50:02.738196-05	f	qq	qq	qq	qq@gm.com	f	t	2016-08-17 13:49:56.987566-05
92		2016-08-17 13:55:24.831058-05	f	jj	jj	jj	jud@g.com	f	t	2016-08-17 13:55:15.208017-05
93	pbkdf2_sha256$24000$U4EbsGSEEjD7$z1k2QY/Ofk7GGKcZVW31ODO2A5lPDSEI9fUP2TL3muI=	2016-08-17 13:56:54.716878-05	f	re	re	re	aee@g.com	f	t	2016-08-17 13:56:50.164547-05
96	pbkdf2_sha256$24000$coLrLFHdGxiv$wzlwbEd3WAUZsmPZp2epZKPoOLnUTnBbPq1M9K5w/ZE=	2016-08-17 16:30:52.396154-05	f	puedes-borrarlo	pudes	borrarlo	puedes-borrarlos@hotmail.com	f	t	2016-08-17 16:30:28.522798-05
94	pbkdf2_sha256$24000$rs5fk0vK8GOL$YsRxL0TnqwXUzzFSRZld9CN9S0VK4GTKNs2hBZd/nW8=	2016-08-17 14:49:48.459335-05	f	david	Pame	Guaman	pame@gmail.com	f	t	2016-08-17 14:49:33.798918-05
102	pbkdf2_sha256$24000$91XPbAUIia1F$QTHNfeKt6W/KFRqUfKiWyA7CxbnLDw5U4lJLr14Gvpg=	2016-08-23 16:26:51.485008-05	f	cosmesin	perrys	guerrero	guerreo@g.com	f	t	2016-08-19 16:05:22.681992-05
95	!4s1gr75OB2RZ70EsJzHNDsALMN35kNQSzzK3aYG2	2016-08-17 17:50:09.428881-05	f	MiltonLeiva	Milton	Leiva		f	t	2016-08-17 16:09:54.325305-05
101	pbkdf2_sha256$24000$ZPYHiMZr7CoR$0gdQBSo72H+/bAM0/iURvVX2ovut6bZXgr4xGMDOZXo=	2016-08-19 14:28:26.415053-05	f	Posix	Posix Linux	Unix	po@six.unix	f	t	2016-08-19 13:42:43.03774-05
100	pbkdf2_sha256$24000$tN9MlIDjAlXX$ZZIbxJbPcuvFmh4VxsLulzUi8hrRnLck/VjqQNlka+w=	2016-08-19 12:32:19.280567-05	f	gia	Gianela	Patinio	gia@g.com	f	t	2016-08-19 12:29:10.931578-05
99	pbkdf2_sha256$24000$QIJCR0dPFA8W$Tqp34sFNlSN2KaAMuxK+k5UNcKXLM5j/ygLwpAcF97M=	2016-08-22 11:23:35.928789-05	f	ChifaBeijing				f	t	2016-08-18 10:10:18-05
76	pbkdf2_sha256$24000$7PgqsVMVQeEi$/3SSR/5v+9vYzA6Z8IZltUWoVLhMqD2QOo5ewkujREM=	2016-08-24 16:25:11.835174-05	f	cliente5	cliente5	cliente5	cliente5@fdf.com	f	t	2016-08-15 13:28:16.772061-05
74	pbkdf2_sha256$24000$Xzurdb4dbU4s$rF35AH4iWpscNMa2xRCRC+DF+zkqtS8BK+w1SKbNy44=	2016-08-24 13:55:36.688936-05	f	otro3	otro3	otro3	otro3@gmai.com	f	t	2016-08-15 10:33:26.014883-05
104	pbkdf2_sha256$24000$NRq1cxGeeOwA$LmF2sW7ygsb01d+FR7BZWZ1ylm1OXcesG6pvaoJ1VKw=	2016-08-19 17:51:27.52627-05	f	pepito	pepito	pepito	pepitp@gmail.com	f	t	2016-08-19 17:10:24.973265-05
103	pbkdf2_sha256$24000$jihbKTm1I046$+wx4KwtWXJIlIMMWcb90DZouwcdMj/RNc5LDpoCcLVk=	2016-08-23 16:12:12.615622-05	f	pizzaHut				f	t	2016-08-19 16:32:15-05
105	pbkdf2_sha256$24000$dRNqLLHF41aK$lrX3AVMGuHw/FuXFjt3KJK0Hpzifze5JC2h00+VQysI=	\N	f	jonas	josue	rivero	rivero@gmail.com	f	t	2016-08-23 16:54:59.680199-05
15	pbkdf2_sha256$24000$cdzVFc3c83jc$YcXa4t0l8dEbmtdQ9D8jinLRihs+qcSEM80BhBlQQpo=	2016-08-24 16:45:29.980364-05	f	cliente2	Cliente	Cliente	aaaa@hhh.com	f	t	2016-08-01 18:53:25-05
106	pbkdf2_sha256$24000$GqFh8wNW4jSr$P5VcXdWQqb+tyUyTaG+nG2dBjCJ4Spo0g+3qD4OlkaQ=	2016-08-23 17:36:19.966631-05	f	unixx	nix	gua	gua@g.com	f	t	2016-08-23 16:55:41.497047-05
107	pbkdf2_sha256$24000$cyMgBYUxkd1l$KUoW8G1P4JHV/ZxEebhW4FTHmY0gwpEWKYTei9iLkIg=	2016-08-23 17:47:31.059955-05	f	brother	MAmi	Papi	papito@gmail.com	f	t	2016-08-23 17:47:21.986633-05
108	pbkdf2_sha256$24000$Gklm24DnaKwb$aj1wg2iEF3+vuigR4WcXXPJKcEkNZR3Y852RNHsBKiU=	\N	f	pamelix95	pame	guaman	pam@gm.com	f	t	2016-08-24 11:12:57.550788-05
110	pbkdf2_sha256$24000$0HwAEqTz742F$PniySTXrz3JB0IHhwcbyK6mChYv4es3P95FB1nJsb8o=	\N	f	Pedido	Pedido	Pedido	pedio@gmail.com	f	t	2016-08-24 13:25:44.524607-05
109	pbkdf2_sha256$24000$YFO6naiLOIYt$xqcmRMQVEQTlTVAltlEvj7HzMXKNQtOu0ypylhaYRcA=	2016-08-24 13:26:19.558978-05	f	pedido	Pedido	Pedido	pedido@gmail.com	f	t	2016-08-24 11:48:44.196806-05
112	pbkdf2_sha256$24000$v6ALyioqDCf7$vGfPO5ASIa2ttLsTuiw5KV+c3yM2+YBuvpAT06tAy+U=	2016-08-24 16:54:22.942-05	f	nadia28	Nadia	PC	nspaucarc@unl.edu.ec	f	t	2016-08-24 16:14:45-05
111	pbkdf2_sha256$24000$WagaIABo4p3W$m2DVmYDhXdnJlEaWveo2pnzNa2HVjMN3BKCwKbH4zyQ=	2016-08-24 13:58:54.883936-05	f	guaicha	guaicha	guaicha	guaich@g.com	f	t	2016-08-24 13:55:43.110645-05
113	pbkdf2_sha256$24000$vnfKQhqwYXR7$tqNDpHWGQKXIUjHzE2dF7kHg1H2SuyC5gHUAvkorT+Q=	2016-08-25 09:55:14.904743-05	f	pelito	Clemencio	Verde	cleverde@gmail.com	f	t	2016-08-24 16:46:26.611975-05
114	pbkdf2_sha256$24000$UPS4cBWOAQfR$vRPvtrsFhlAAGyZWyTaqJutn5f6Iw7xiU+QMWcOdFaM=	2016-08-25 09:56:09.058898-05	f	soyard				f	t	2016-08-24 20:09:11-05
115	pbkdf2_sha256$24000$pNrJ36Xb6GZp$aJcodnjvjHwEehBydQAFjLitlsoibrmyd+/dxRZ5/bU=	\N	f	locochavez				f	t	2016-08-25 11:17:50-05
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
2	29	1
3	30	1
4	32	1
5	33	1
6	34	1
7	15	1
8	35	1
9	4	2
10	3	3
11	6	2
12	5	2
13	36	1
14	41	1
15	42	1
16	43	1
17	44	1
18	45	1
19	47	1
20	54	1
21	57	1
22	59	1
23	62	2
24	64	1
25	65	1
26	66	1
27	67	1
28	68	1
29	69	1
30	70	1
31	71	1
32	72	1
33	74	1
34	75	1
35	76	1
36	77	1
37	78	1
38	79	1
39	80	1
40	81	1
41	82	1
42	83	1
43	85	1
44	86	1
45	87	1
46	88	1
47	89	1
48	90	1
49	91	1
50	92	1
51	93	1
52	94	1
53	96	1
54	97	1
55	98	1
56	9	2
57	7	2
58	99	2
59	100	1
60	101	1
61	102	1
62	103	2
63	104	1
64	105	1
65	106	1
66	107	1
67	108	1
68	109	1
69	110	1
70	111	1
71	112	1
72	113	1
73	114	2
74	115	2
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 74, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_id_seq', 115, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2016-07-28 19:59:20.390682-05	3	transportista	1	Añadido.	5	2
2	2016-07-28 19:59:23.662164-05	1	1	1	Añadido.	9	2
3	2016-07-28 19:59:25.000849-05	1	1	1	Añadido.	18	2
4	2016-07-28 20:01:23.543102-05	1	Loja	1	Añadido.	12	2
5	2016-07-28 20:01:23.231307-05	2	Loja	1	Añadido.	12	2
6	2016-07-28 20:05:40.805126-05	4	pizza	1	Añadido.	5	2
7	2016-07-28 20:05:44.135955-05	2	pizza	1	Añadido.	9	2
8	2016-07-28 20:05:45.592826-05	1	Guaranda - Tulcan	1	Añadido.	13	2
9	2016-07-28 20:06:16.423005-05	1	Restaurante	1	Añadido.	8	2
10	2016-07-28 20:06:27.333526-05	1	Restaurante	1	Añadido.	10	2
11	2016-07-28 20:06:39.26334-05	1	Pizza	2	Modificado/a categoria.	10	2
12	2016-07-28 20:06:58.370523-05	1	Pizza	1	Añadido.	11	2
13	2016-07-28 20:07:03.652279-05	1	Pizza	1	Añadido.	15	2
14	2016-07-28 20:07:19.515262-05	1	Pizza	1	Añadido.	16	2
15	2016-07-28 20:07:12.08977-05	5	chinito	1	Añadido.	5	2
16	2016-07-28 20:07:16.552552-05	3	chinito	1	Añadido.	9	2
17	2016-07-28 20:07:21.834778-05	2	sucre - miguel riofrío	1	Añadido.	13	2
18	2016-07-28 20:08:00.869683-05	2	Chinito	1	Añadido.	11	2
19	2016-07-28 20:08:09.773635-05	2	Chinito	1	Añadido.	15	2
20	2016-07-28 20:08:18.043651-05	2	Chinito	1	Añadido.	16	2
21	2016-07-28 20:08:58.307718-05	6	chino	1	Añadido.	5	2
22	2016-07-28 20:09:00.811683-05	4	chino	1	Añadido.	9	2
23	2016-07-28 20:09:03.001269-05	3	Vince - Ambato	1	Añadido.	13	2
24	2016-07-28 20:09:25.161411-05	2	Comida Asiática	1	Añadido.	10	2
25	2016-07-28 20:09:59.543746-05	3	Casa China	1	Añadido.	11	2
26	2016-07-28 20:10:07.490487-05	3	Casa China	1	Añadido.	15	2
27	2016-07-28 20:10:13.315091-05	3	Casa china	1	Añadido.	16	2
28	2016-07-28 20:09:54.330835-05	3	Heladería	1	Añadido.	10	2
29	2016-07-28 20:11:05.998379-05	7	icecreamsocial	1	Añadido.	5	2
30	2016-07-28 20:11:47.204706-05	8	chamelion	1	Añadido.	5	2
31	2016-07-28 20:11:11.395078-05	5	icecreamsocial	1	Añadido.	9	2
32	2016-07-28 20:11:49.769628-05	6	chamelion	1	Añadido.	9	2
33	2016-07-28 20:11:51.730859-05	4	Latacunga - Bolivar	1	Añadido.	13	2
34	2016-07-28 20:11:15.29552-05	4	Ice Cream Social	1	Añadido.	11	2
35	2016-07-28 20:11:24.471836-05	4	Ice Cream Social	1	Añadido.	15	2
36	2016-07-28 20:11:26.747109-05	4	Ice Cream Social	1	Añadido.	16	2
37	2016-07-28 20:12:11.435858-05	4	Comida Casera	1	Añadido.	10	2
38	2016-07-28 20:12:55.152681-05	5	Chamelion	1	Añadido.	11	2
39	2016-07-28 20:13:01.261927-05	5	Chamelion	1	Añadido.	15	2
40	2016-07-28 20:13:02.643552-05	5	Chamelion	1	Añadido.	16	2
41	2016-07-28 20:12:45.508447-05	5	Comida rápida	1	Añadido.	10	2
42	2016-07-28 20:13:21.348026-05	9	papalogo	1	Añadido.	5	2
43	2016-07-28 20:13:24.568677-05	7	papalogo	1	Añadido.	9	2
44	2016-07-28 20:13:28.421589-05	6	PapaLogo	1	Añadido.	11	2
45	2016-07-28 20:14:12.495246-05	10	tonys	1	Añadido.	5	2
46	2016-07-28 20:14:16.354245-05	8	tonys	1	Añadido.	9	2
47	2016-07-28 20:14:22.382596-05	7	Tony's	1	Añadido.	11	2
48	2016-07-28 20:14:28.926592-05	6	Tony's	1	Añadido.	15	2
49	2016-07-28 20:14:30.19485-05	6	Tony's	1	Añadido.	16	2
50	2016-07-28 20:13:55.29901-05	7	PapaLogo	1	Añadido.	15	2
51	2016-07-28 20:13:57.573875-05	7	PapaLogo	1	Añadido.	16	2
52	2016-07-28 20:19:15.815955-05	1	Helados	1	Añadido.	23	2
53	2016-07-28 20:19:18.781995-05	1	Helado napolitano	1	Añadido.	24	2
54	2016-07-28 20:19:43.678705-05	2	Helado de chocolate	1	Añadido.	24	2
55	2016-07-28 20:21:43.90168-05	2	Pizza	1	Añadido.	23	2
56	2016-07-28 20:21:59.823934-05	3	Hawaiana	1	Añadido.	24	2
57	2016-07-28 20:23:00.670329-05	4	Napolitana	1	Añadido.	24	2
58	2016-07-28 20:22:23.470628-05	3	Hamburguesa	1	Añadido.	23	2
59	2016-07-28 20:22:28.21659-05	5	Hamburguesa	1	Añadido.	24	2
60	2016-07-28 20:23:19.996509-05	1	Standard	1	Añadido.	25	2
61	2016-07-28 20:23:32.471793-05	2	Mediana	1	Añadido.	25	2
62	2016-07-28 20:23:37.969029-05	3	Grande	1	Añadido.	25	2
63	2016-07-28 20:23:31.223306-05	4	Bebida	1	Añadido.	23	2
64	2016-07-28 20:24:09.793627-05	1	Helado napolitano : Standard - 1.0	1	Añadido.	26	2
65	2016-07-28 20:23:33.036534-05	6	bebida	1	Añadido.	24	2
66	2016-07-28 20:24:16.638918-05	2	Helado de chocolate : Mediana - 1.5	1	Añadido.	26	2
67	2016-07-28 20:24:24.723157-05	3	Helado de chocolate : Grande - 2.0	1	Añadido.	26	2
68	2016-07-28 20:24:07.1025-05	7	refresco	1	Añadido.	24	2
69	2016-07-28 20:24:49.665641-05	4	Normal	1	Añadido.	25	2
70	2016-07-28 20:24:56.158889-05	4	Hawaiana : Normal - 3.5	1	Añadido.	26	2
71	2016-07-28 20:25:13.03056-05	5	Hawaiana : Grande - 5.5	1	Añadido.	26	2
72	2016-07-28 20:25:21.62963-05	6	Hawaiana : Grande - 12.5	1	Añadido.	26	2
73	2016-07-28 20:25:30.330695-05	7	Napolitana : Mediana - 3.0	1	Añadido.	26	2
74	2016-07-28 20:25:42.471823-05	8	Napolitana : Grande - 12.0	1	Añadido.	26	2
75	2016-07-28 20:25:58.157964-05	9	Hamburguesa : Normal - 2.5	1	Añadido.	26	2
76	2016-07-28 20:26:53.376773-05	10	Hamburguesa : Mediana - 6.0	1	Añadido.	26	2
77	2016-07-28 20:27:05.899275-05	11	bebida : Normal - 5.0	1	Añadido.	26	2
78	2016-07-28 20:27:19.880165-05	12	bebida : Grande - 3.0	1	Añadido.	26	2
79	2016-07-28 20:27:40.220621-05	13	refresco : Grande - 4.0	1	Añadido.	26	2
80	2016-07-28 20:45:24.074742-05	3	chinito	2	No ha cambiado ningún campo.	9	2
81	2016-07-28 20:46:07.67556-05	11	cliente1	1	Añadido.	5	2
82	2016-07-28 20:46:48.647758-05	9	cliente1	1	Añadido.	9	2
83	2016-07-29 10:58:24.935239-05	1	Efectivo	1	Añadido.	20	2
84	2016-07-29 10:58:50.507783-05	2	Dinero Electrónico	1	Añadido.	20	2
85	2016-07-29 11:14:11.749621-05	1	A domicilio	1	Añadido.	21	2
86	2016-07-29 11:23:20.446587-05	2	Para recoger	1	Añadido.	21	2
87	2016-07-29 11:33:13.840393-05	4	Napolitana	2	Modificado/a ruta_foto.	24	2
88	2016-07-29 11:33:33.020604-05	5	Hamburguesa	2	Modificado/a ruta_foto.	24	2
89	2016-07-29 11:33:54.048344-05	6	Coca Cola	2	Modificado/a nombre_producto y ruta_foto.	24	2
90	2016-07-29 11:34:05.715615-05	7	Fanta naranja	2	Modificado/a nombre_producto y ruta_foto.	24	2
91	2016-07-29 11:34:42.278738-05	10	admin	1	Añadido.	9	2
92	2016-08-02 10:12:13.135851-05	1	2016-07-29 15:45:42.091906+00:00 - bueno - 5.0	3		22	2
93	2016-08-02 10:13:59.614327-05	15	2016-08-02 15:02:16+00:00 - pendiente - 3.5	3		22	2
94	2016-08-02 10:13:59.620334-05	14	2016-08-02 15:01:53+00:00 - pendiente - 15.5	3		22	2
95	2016-08-02 10:13:59.623329-05	13	2016-08-01 14:56:05+00:00 - pendiente - 4.5	3		22	2
96	2016-08-02 10:13:59.626329-05	12	2016-06-29 17:09:53+00:00 - pendiente - 16.5	3		22	2
97	2016-08-02 10:13:59.629329-05	11	2016-06-29 17:09:42+00:00 - pendiente - 11.5	3		22	2
98	2016-08-02 10:13:59.633329-05	10	2016-07-29 17:09:02+00:00 - pendiente - 34.5	3		22	2
99	2016-08-02 10:13:59.636329-05	9	2016-07-29 16:25:20+00:00 - confirmado - 8.0	3		22	2
100	2016-08-03 13:03:27.252154-05	42	cliente1	3		9	2
101	2016-08-03 13:03:27.28951-05	41	cliente1	3		9	2
102	2016-08-03 13:03:27.292022-05	9	cliente1	3		9	2
103	2016-08-03 13:03:27.294013-05	0	cliente1	3		9	2
104	2016-08-03 13:14:22.114097-05	43	cliente1	3		9	2
105	2016-08-08 12:55:43.791906-05	49	cliente1	1	Añadido.	9	2
106	2016-08-08 07:58:02.886167-05	21	nicholas	1	Añadido.	5	2
107	2016-08-08 07:58:11.009641-05	21	nicholas	2	Modificado/a groups.	5	2
108	2016-08-08 18:15:26.946411-05	8	Helado oreo	1	Añadido.	24	2
109	2016-08-08 18:16:20.860469-05	14	Helado oreo : Normal - 1.5	1	Añadido.	26	2
110	2016-08-09 10:08:51.017187-05	15	cliente2	2	Modificado/a groups.	5	2
111	2016-08-09 10:27:52.031728-05	1	Cliente	2	Modificado/a permissions.	4	2
112	2016-08-09 10:29:37.790904-05	1	Cliente	2	No ha cambiado ningún campo.	4	2
113	2016-08-09 10:30:12.46179-05	1	Cliente	2	Modificado/a permissions.	4	2
114	2016-08-09 06:01:02.689397-05	4	pizza	2	Modificado/a groups.	5	2
115	2016-08-09 06:25:24.149953-05	3	transportista	2	Modificado/a groups.	5	2
116	2016-08-09 06:50:05.400948-05	6	chino	2	Modificado/a groups.	5	2
117	2016-08-09 06:51:04.49793-05	5	chinito	2	Modificado/a groups.	5	2
118	2016-08-09 14:27:41.198696-05	2	pizza	2	Modificado/a ruta_foto.	9	2
119	2016-08-09 15:43:57.601132-05	10	pollo	2	No ha cambiado ningún campo.	24	2
120	2016-08-09 15:44:39.387203-05	18	pollo : Mediana - 12.0	1	Añadido.	26	2
121	2016-08-09 16:30:18.107677-05	38	bbbbbbb	3		5	2
122	2016-08-09 16:30:18.134171-05	31	byjose007	3		5	2
123	2016-08-09 16:30:18.137675-05	37	ggggg	3		5	2
124	2016-08-09 16:30:18.140745-05	26	ni	3		5	2
125	2016-08-09 16:30:18.144237-05	21	nicholas	3		5	2
126	2016-08-09 16:30:18.148239-05	25	q	3		5	2
127	2016-08-09 16:46:50.928489-05	19	aaaaaaaaa : Mediana - 2.0	1	Añadido.	26	2
128	2016-08-09 16:48:14.967534-05	11	popi	3		24	2
129	2016-08-09 16:48:32.56696-05	20	aaaaaaaaa : Mediana - 12.0	1	Añadido.	26	2
130	2016-08-09 16:48:56.610075-05	20	aaaaaaaaa : Mediana - 12.0	3		26	2
131	2016-08-09 17:43:23.253083-05	14	zanahoria	3		28	2
132	2016-08-09 17:43:23.283525-05	13	Arroz	3		28	2
133	2016-08-09 17:43:23.304724-05	12	Cebolla	3		28	2
134	2016-08-09 17:43:23.31773-05	11	Pollo	3		28	2
135	2016-08-09 17:44:24.115457-05	4	Cebolla	3		28	2
136	2016-08-09 17:44:24.14272-05	2	Pescado	3		28	2
137	2016-08-09 17:44:24.153308-05	1	Arroz	3		28	2
138	2016-08-09 18:27:57.983382-05	5	Marisco	1	Añadido.	23	2
139	2016-08-09 18:28:15.612142-05	6	Pescado	1	Añadido.	23	2
140	2016-08-09 18:29:06.834228-05	7	Pollo	1	Añadido.	23	2
141	2016-08-09 18:29:12.41578-05	8	Carnes	1	Añadido.	23	2
142	2016-08-10 17:30:56.563683-05	116	2016-08-10 22:29:33+00:00 - pendiente - 8.5	3		22	2
143	2016-08-10 17:30:56.617106-05	115	2016-08-10 22:26:37+00:00 - pendiente - 4.5	3		22	2
144	2016-08-10 17:30:56.630524-05	114	2016-08-10 22:24:03+00:00 - pendiente - 12.5	3		22	2
145	2016-08-10 17:30:56.649959-05	113	2016-08-10 22:23:25+00:00 - pendiente - 12.5	3		22	2
146	2016-08-10 17:30:56.659284-05	112	2016-08-10 22:23:22+00:00 - pendiente - 12.5	3		22	2
147	2016-08-10 17:30:56.667474-05	111	2016-08-10 22:22:00+00:00 - pendiente - 12.5	3		22	2
148	2016-08-10 17:30:56.675305-05	110	2016-08-10 22:20:07+00:00 - pendiente - 2.5	3		22	2
149	2016-08-10 17:30:56.707886-05	109	2016-08-10 22:18:28+00:00 - pendiente - 11.5	3		22	2
150	2016-08-10 17:30:56.724567-05	108	2016-08-10 22:16:25+00:00 - pendiente - 59.5	3		22	2
151	2016-08-10 17:30:56.741252-05	107	2016-08-10 22:15:57+00:00 - pendiente - 31.5	3		22	2
152	2016-08-10 17:30:56.758082-05	106	2016-08-10 22:15:15+00:00 - pendiente - 11.5	3		22	2
153	2016-08-10 17:30:56.774884-05	105	2016-08-10 22:14:44+00:00 - pendiente - 21.5	3		22	2
154	2016-08-10 17:30:56.807825-05	104	2016-08-10 22:14:18+00:00 - pendiente - 21.5	3		22	2
155	2016-08-10 17:30:56.835433-05	103	2016-08-10 22:13:18+00:00 - pendiente - 16.5	3		22	2
156	2016-08-10 17:30:56.857968-05	102	2016-08-10 22:12:04+00:00 - pendiente - 41.5	3		22	2
157	2016-08-10 17:30:56.887552-05	101	2016-08-10 22:10:49+00:00 - pendiente - 41.5	3		22	2
158	2016-08-10 17:30:56.895467-05	100	2016-08-10 22:04:13+00:00 - pendiente - 11.5	3		22	2
159	2016-08-10 17:30:56.907666-05	99	2016-08-10 21:51:38+00:00 - pendiente - 29.5	3		22	2
160	2016-08-10 17:30:56.918046-05	98	2016-08-10 21:51:27+00:00 - pendiente - 29.5	3		22	2
161	2016-08-10 17:30:56.932541-05	97	2016-08-10 21:46:55+00:00 - pendiente - 15.5	3		22	2
162	2016-08-10 17:30:56.944276-05	96	2016-08-10 21:44:43+00:00 - pendiente - 21.0	3		22	2
163	2016-08-10 17:30:56.957556-05	95	2016-08-10 21:43:12+00:00 - pendiente - 7.0	3		22	2
164	2016-08-10 17:30:56.966383-05	94	2016-08-10 21:42:30+00:00 - pendiente - 4.5	3		22	2
165	2016-08-10 17:30:56.974936-05	93	2016-08-10 21:41:06+00:00 - pendiente - 4.5	3		22	2
166	2016-08-10 17:30:56.983074-05	92	2016-08-10 21:39:01+00:00 - pendiente - 4.5	3		22	2
167	2016-08-10 17:30:56.995683-05	91	2016-08-10 21:38:41+00:00 - pendiente - 15.5	3		22	2
168	2016-08-10 17:30:57.009319-05	90	2016-08-10 21:38:41+00:00 - pendiente - 15.5	3		22	2
169	2016-08-10 17:30:57.017449-05	89	2016-08-10 21:38:40+00:00 - pendiente - 15.5	3		22	2
170	2016-08-10 17:30:57.03254-05	88	2016-08-10 21:38:40+00:00 - pendiente - 15.5	3		22	2
171	2016-08-10 17:30:57.045248-05	87	2016-08-10 21:37:01+00:00 - pendiente - 17.0	3		22	2
172	2016-08-10 17:30:57.059528-05	86	2016-08-10 21:37:01+00:00 - pendiente - 17.0	3		22	2
173	2016-08-10 17:30:57.076904-05	85	2016-08-10 21:37:01+00:00 - pendiente - 17.0	3		22	2
174	2016-08-10 17:30:57.092472-05	84	2016-08-10 21:36:57+00:00 - pendiente - 17.0	3		22	2
175	2016-08-10 17:30:57.100376-05	83	2016-08-10 21:36:57+00:00 - pendiente - 17.0	3		22	2
176	2016-08-10 17:30:57.111697-05	82	2016-08-10 21:36:57+00:00 - pendiente - 17.0	3		22	2
177	2016-08-10 17:30:57.119474-05	81	2016-08-10 21:36:56+00:00 - pendiente - 17.0	3		22	2
178	2016-08-10 17:30:57.135455-05	80	2016-08-10 21:36:56+00:00 - pendiente - 17.0	3		22	2
179	2016-08-10 17:30:57.149248-05	79	2016-08-10 21:36:56+00:00 - pendiente - 17.0	3		22	2
180	2016-08-10 17:30:57.156701-05	78	2016-08-10 21:36:55+00:00 - pendiente - 17.0	3		22	2
181	2016-08-10 17:30:57.165946-05	77	2016-08-10 21:36:21+00:00 - pendiente - 17.0	3		22	2
182	2016-08-10 17:30:57.183579-05	76	2016-08-10 21:36:20+00:00 - pendiente - 17.0	3		22	2
183	2016-08-10 17:30:57.199855-05	75	2016-08-10 21:36:02+00:00 - pendiente - 17.0	3		22	2
184	2016-08-10 17:30:57.216601-05	74	2016-08-10 21:33:34+00:00 - pendiente - 6.5	3		22	2
185	2016-08-10 17:30:57.245069-05	73	2016-08-10 21:33:07+00:00 - pendiente - 19.0	3		22	2
186	2016-08-10 17:30:57.266318-05	72	2016-08-10 21:33:07+00:00 - pendiente - 19.0	3		22	2
187	2016-08-10 17:30:57.283009-05	71	2016-08-10 21:33:07+00:00 - pendiente - 19.0	3		22	2
188	2016-08-10 17:30:57.299752-05	70	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
189	2016-08-10 17:30:57.316609-05	69	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
190	2016-08-10 17:30:57.341257-05	68	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
191	2016-08-10 17:30:57.358362-05	67	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
192	2016-08-10 17:30:57.369999-05	66	2016-08-10 21:33:06+00:00 - pendiente - 19.0	3		22	2
193	2016-08-10 17:30:57.376659-05	65	2016-08-10 21:31:25+00:00 - pendiente - 19.0	3		22	2
194	2016-08-10 17:30:57.383204-05	64	2016-08-10 21:30:10+00:00 - pendiente - 15.5	3		22	2
195	2016-08-10 17:30:57.399581-05	63	2016-08-10 21:27:54+00:00 - pendiente - 6.5	3		22	2
196	2016-08-10 17:30:57.407432-05	62	2016-08-10 21:24:22+00:00 - pendiente - 17.0	3		22	2
197	2016-08-10 17:30:57.418838-05	61	2016-08-10 21:19:18+00:00 - pendiente - 29.5	3		22	2
198	2016-08-10 17:30:57.426159-05	60	2016-08-10 21:18:15+00:00 - pendiente - 29.5	3		22	2
199	2016-08-10 17:30:57.440484-05	59	2016-08-10 21:07:16+00:00 - pendiente - 20.0	3		22	2
200	2016-08-10 17:30:57.449994-05	58	2016-08-10 20:59:14+00:00 - pendiente - 28.5	3		22	2
201	2016-08-10 17:30:57.465775-05	57	2016-08-10 20:58:53+00:00 - pendiente - 35.5	3		22	2
202	2016-08-10 17:30:57.474079-05	56	2016-08-10 20:58:44+00:00 - pendiente - 30.5	3		22	2
203	2016-08-10 17:30:57.482438-05	55	2016-08-10 20:57:11+00:00 - pendiente - 35.5	3		22	2
204	2016-08-10 17:30:57.488862-05	54	2016-08-10 18:45:34+00:00 - pendiente - 27.5	3		22	2
205	2016-08-10 17:30:57.495384-05	53	2016-08-10 16:27:17+00:00 - pendiente - 3.0	3		22	2
206	2016-08-10 17:30:57.507564-05	52	2016-08-10 00:33:46+00:00 - confirmado - 21.5	3		22	2
207	2016-08-10 17:30:57.514402-05	51	2016-08-10 00:31:06+00:00 - pendiente - 3.5	3		22	2
208	2016-08-10 17:30:57.525592-05	50	2016-08-09 21:22:06+00:00 - enviado - 28.5	3		22	2
209	2016-08-10 17:30:57.533579-05	49	2016-08-09 21:21:25+00:00 - pendiente - 28.5	3		22	2
210	2016-08-10 17:30:57.54383-05	48	2016-08-09 21:20:56+00:00 - pendiente - 28.5	3		22	2
211	2016-08-10 17:30:57.55744-05	47	2016-08-09 21:20:04+00:00 - pendiente - 28.5	3		22	2
212	2016-08-10 17:30:57.564322-05	46	2016-08-09 21:17:43+00:00 - pendiente - 13.5	3		22	2
213	2016-08-10 17:30:57.574102-05	45	2016-08-09 20:41:10+00:00 - pendiente - 43.5	3		22	2
214	2016-08-10 17:30:57.582318-05	44	2016-08-09 19:21:06+00:00 - pendiente - 6.5	3		22	2
215	2016-08-10 17:30:57.588592-05	43	2016-08-09 19:21:02+00:00 - pendiente - 6.5	3		22	2
216	2016-08-10 17:30:57.615824-05	42	2016-08-09 19:19:43+00:00 - pendiente - 6.5	3		22	2
217	2016-08-10 17:30:57.632788-05	41	2016-08-09 18:50:26+00:00 - enviado - 41.5	3		22	2
218	2016-08-10 17:30:57.649493-05	40	2016-08-09 18:50:12+00:00 - pendiente - 11.5	3		22	2
219	2016-08-10 17:30:57.666589-05	39	2016-08-09 13:04:46+00:00 - pendiente - 105.5	3		22	2
220	2016-08-10 17:30:57.683007-05	38	2016-08-09 13:03:26+00:00 - pendiente - 5.5	3		22	2
221	2016-08-10 17:30:57.700877-05	37	2016-08-09 13:02:10+00:00 - pendiente - 5.5	3		22	2
222	2016-08-10 17:30:57.727871-05	36	2016-08-09 13:01:33+00:00 - pendiente - 5.5	3		22	2
223	2016-08-10 17:30:57.743856-05	35	2016-08-09 12:57:57+00:00 - pendiente - 5.5	3		22	2
224	2016-08-10 17:30:57.758098-05	34	2016-08-09 12:46:45+00:00 - pendiente - 11.5	3		22	2
225	2016-08-10 17:30:57.777238-05	33	2016-08-09 12:31:23+00:00 - pendiente - 14.5	3		22	2
226	2016-08-10 17:30:57.784851-05	32	2016-08-09 12:28:38+00:00 - pendiente - 14.5	3		22	2
227	2016-08-10 17:30:57.791659-05	31	2016-08-09 12:00:11+00:00 - pendiente - 11.5	3		22	2
228	2016-08-10 17:30:57.810147-05	30	2016-08-09 11:08:10+00:00 - enviado - 6.5	3		22	2
229	2016-08-10 17:30:57.817697-05	29	2016-08-09 11:03:05+00:00 - enviado - 7.5	3		22	2
230	2016-08-10 17:30:57.832586-05	28	2016-08-09 10:59:52+00:00 - enviado - 2.5	3		22	2
231	2016-08-10 17:30:57.847138-05	27	2016-08-09 15:44:32+00:00 - enviado - 11.5	3		22	2
232	2016-08-10 17:30:57.860094-05	26	2016-08-06 00:39:28+00:00 - pendiente - 2.5	3		22	2
233	2016-08-10 17:30:57.869979-05	25	2016-08-04 20:07:15+00:00 - pendiente - 2.5	3		22	2
234	2016-08-10 17:30:57.88288-05	24	2016-08-04 18:31:03+00:00 - pendiente - 21.5	3		22	2
235	2016-08-10 17:30:57.891491-05	23	2016-08-03 22:35:21+00:00 - enviado - 2.5	3		22	2
236	2016-08-10 17:30:57.899672-05	22	2016-08-03 21:45:47+00:00 - enviado - 2.5	3		22	2
237	2016-08-10 17:30:57.911899-05	21	2016-08-03 19:40:33+00:00 - enviado - 4.5	3		22	2
238	2016-08-10 17:30:57.920193-05	20	2016-08-03 19:39:29+00:00 - enviado - 2.5	3		22	2
239	2016-08-10 17:30:57.934156-05	19	2016-08-03 17:29:53+00:00 - enviado - 2.5	3		22	2
240	2016-08-10 18:34:31.506326-05	6	Frutas y Jugos	1	Añadido.	10	2
241	2016-08-10 18:35:16.079128-05	55	fruques	1	Añadido.	5	2
242	2016-08-10 18:35:39.428714-05	56	fruques2	1	Añadido.	5	2
243	2016-08-10 18:36:59.705463-05	8	Fruques	1	Añadido.	11	2
244	2016-08-10 18:37:07.462498-05	9	Fruques	1	Añadido.	15	2
245	2016-08-10 18:37:11.299691-05	8	Fruques	1	Añadido.	16	2
246	2016-08-10 18:37:51.821612-05	86	fruques	2	Modificado/a ruta_foto.	9	2
247	2016-08-10 18:37:53.055749-05	8	Fruques	2	No ha cambiado ningún campo.	11	2
248	2016-08-10 18:37:55.144419-05	8	Fruques	2	No ha cambiado ningún campo.	16	2
249	2016-08-10 18:42:44.121248-05	8	Fruques	3		16	2
250	2016-08-10 18:43:06.854409-05	6	Frutas y Jugos	3		10	2
251	2016-08-11 09:45:37.54196-05	122	2016-08-10 22:48:45+00:00 - pendiente - 30.0	3		22	2
252	2016-08-11 09:45:37.56597-05	121	2016-08-10 22:47:55+00:00 - pendiente - 6.0	3		22	2
253	2016-08-11 09:45:37.58389-05	120	2016-08-10 22:46:48+00:00 - pendiente - 2.5	3		22	2
254	2016-08-11 09:45:37.596208-05	119	2016-08-10 22:39:44+00:00 - pendiente - 11.5	3		22	2
255	2016-08-11 09:45:37.622398-05	118	2016-08-10 22:32:53+00:00 - pendiente - 19.5	3		22	2
256	2016-08-11 09:45:37.630836-05	117	2016-08-10 22:31:33+00:00 - pendiente - 6.5	3		22	2
257	2016-08-11 09:50:10.554505-05	1	4	1	Añadido.	33	2
258	2016-08-11 09:52:04.331675-05	2	3	1	Añadido.	33	2
259	2016-08-11 09:53:19.163716-05	3	5	1	Añadido.	33	2
260	2016-08-11 09:57:59.479413-05	4	4	1	Añadido.	33	2
261	2016-08-11 11:49:37.018911-05	139	2016-08-11 16:21:52+00:00 - pendiente - 10.0	3		22	2
262	2016-08-11 11:49:37.048401-05	138	2016-08-11 16:12:09+00:00 - pendiente - 10.0	3		22	2
263	2016-08-11 11:49:37.078803-05	137	2016-08-11 16:11:45+00:00 - pendiente - 4.5	3		22	2
264	2016-08-11 11:49:37.087521-05	136	2016-08-11 15:48:27+00:00 - pendiente - 3.5	3		22	2
265	2016-08-11 11:49:37.103426-05	135	2016-08-11 15:38:50+00:00 - pendiente - 3.5	3		22	2
266	2016-08-11 11:49:37.122096-05	134	2016-08-11 15:27:02+00:00 - enviado - 37.5	3		22	2
267	2016-08-11 11:49:37.135019-05	133	2016-08-11 00:11:38+00:00 - enviado - 17.0	3		22	2
268	2016-08-11 11:49:37.156985-05	132	2016-08-11 00:09:28+00:00 - pendiente - 9.0	3		22	2
269	2016-08-11 11:49:37.165883-05	131	2016-08-11 00:04:55+00:00 - pendiente - 81.5	3		22	2
270	2016-08-11 11:49:37.18034-05	130	2016-08-11 00:04:51+00:00 - pendiente - 81.5	3		22	2
271	2016-08-11 11:49:37.188706-05	129	2016-08-11 00:03:59+00:00 - pendiente - 81.5	3		22	2
272	2016-08-11 11:49:37.196671-05	128	2016-08-10 23:36:26+00:00 - pendiente - 3.0	3		22	2
273	2016-08-11 11:49:37.208086-05	127	2016-08-10 23:36:32+00:00 - pendiente - 42.5	3		22	2
274	2016-08-11 11:49:37.222485-05	126	2016-08-10 23:36:21+00:00 - pendiente - 3.0	3		22	2
275	2016-08-11 11:49:37.230231-05	125	2016-08-10 23:35:51+00:00 - pendiente - 7.0	3		22	2
276	2016-08-11 11:49:37.245423-05	124	2016-08-10 22:50:36+00:00 - pendiente - 41.0	3		22	2
277	2016-08-11 11:49:37.25511-05	123	2016-08-10 22:50:06+00:00 - pendiente - 41.0	3		22	2
278	2016-08-11 12:01:42.39682-05	5	3	1	Añadido.	33	2
279	2016-08-11 12:04:20.107037-05	6	4	1	Añadido.	33	2
280	2016-08-11 12:04:58.646755-05	7	3	1	Añadido.	33	2
281	2016-08-11 14:02:36.530052-05	7	Pollos	1	Añadido.	10	2
282	2016-08-11 14:05:00.915294-05	62	gusy	1	Añadido.	5	2
283	2016-08-11 14:06:04.483602-05	62	gusy2	2	Modificado/a username.	5	2
284	2016-08-11 14:06:40.824247-05	62	gusy2	2	Modificado/a groups.	5	2
285	2016-08-11 14:09:37.41132-05	63	gusy	1	Añadido.	5	2
286	2016-08-11 14:11:26.034102-05	94	gusy	2	Modificado/a ruta_foto.	9	2
287	2016-08-11 14:12:02.742939-05	9	Gusy	1	Añadido.	11	2
288	2016-08-11 14:12:28.228425-05	11	Gusy	1	Añadido.	15	2
289	2016-08-11 14:12:30.760267-05	9	Gusy	1	Añadido.	16	2
290	2016-08-11 16:06:37.128618-05	43	Helado de chocolate	1	Añadido.	31	2
291	2016-08-11 16:07:15.503629-05	19	chocolate	1	Añadido.	28	2
292	2016-08-11 16:07:30.591689-05	43	Helado de chocolate	2	Modificado/a id_ingrediente y requerido.	31	2
293	2016-08-15 15:19:25.45368-05	203	2016-08-15 20:17:49+00:00 - En Espera - 17.0	3		22	2
294	2016-08-15 15:19:25.490655-05	202	2016-08-15 20:16:34+00:00 - En Espera - 7.0	3		22	2
295	2016-08-15 15:19:25.494613-05	201	2016-08-15 20:16:17+00:00 - En Espera - 7.0	3		22	2
296	2016-08-15 15:19:25.497692-05	200	2016-08-15 20:12:28+00:00 - En Espera - 13.5	3		22	2
297	2016-08-15 15:19:25.500612-05	199	2016-08-15 20:11:23+00:00 - En Espera - 11.5	3		22	2
298	2016-08-15 15:19:25.503652-05	198	2016-08-15 20:11:01+00:00 - En Espera - 11.5	3		22	2
299	2016-08-15 15:19:25.507612-05	197	2016-08-15 20:10:38+00:00 - En Espera - 20.06	3		22	2
300	2016-08-15 15:19:25.510612-05	196	2016-08-15 20:10:06+00:00 - En Espera - 20.06	3		22	2
301	2016-08-15 15:19:25.513612-05	195	2016-08-15 20:09:59+00:00 - En Espera - 28.5	3		22	2
302	2016-08-15 15:19:25.516612-05	194	2016-08-15 20:06:04+00:00 - En Espera - 7.0	3		22	2
303	2016-08-15 15:19:25.519613-05	193	2016-08-15 20:05:50+00:00 - En Espera - 7.0	3		22	2
304	2016-08-15 15:19:25.522613-05	192	2016-08-15 20:05:37+00:00 - En Espera - 11.5	3		22	2
305	2016-08-15 15:19:25.525673-05	191	2016-08-15 20:05:11+00:00 - En Espera - 16.5	3		22	2
306	2016-08-15 15:19:25.528658-05	190	2016-08-15 20:04:45+00:00 - En Espera - 21.5	3		22	2
307	2016-08-15 15:19:25.531618-05	189	2016-08-15 20:04:50+00:00 - En Espera - 13.5	3		22	2
308	2016-08-15 15:19:25.534635-05	188	2016-08-15 20:02:08+00:00 - En Espera - 31.5	3		22	2
309	2016-08-15 15:19:25.537616-05	187	2016-08-15 20:01:57+00:00 - En Espera - 31.5	3		22	2
310	2016-08-15 15:19:25.540656-05	186	2016-08-15 19:59:33+00:00 - En Espera - 31.5	3		22	2
311	2016-08-15 15:19:25.543644-05	185	2016-08-15 20:00:18+00:00 - En Espera - 13.5	3		22	2
312	2016-08-15 15:19:25.546632-05	184	2016-08-15 19:59:24+00:00 - En Espera - 31.5	3		22	2
313	2016-08-15 15:19:25.573653-05	183	2016-08-15 19:59:18+00:00 - En Espera - 31.5	3		22	2
314	2016-08-15 15:19:25.590665-05	182	2016-08-15 19:59:24+00:00 - En Espera - 4.5	3		22	2
315	2016-08-15 15:19:25.607618-05	181	2016-08-15 19:58:07+00:00 - En Espera - 11.5	3		22	2
316	2016-08-15 15:19:25.623728-05	180	2016-08-15 19:53:59+00:00 - En Espera - 10.5	3		22	2
317	2016-08-15 15:19:25.640663-05	179	2016-08-15 19:52:43+00:00 - En Espera - 11.5	3		22	2
318	2016-08-15 15:19:25.656649-05	178	2016-08-15 19:51:39+00:00 - En Espera - 11.5	3		22	2
319	2016-08-15 15:19:25.673644-05	177	2016-08-15 19:50:25+00:00 - En Espera - 15.5	3		22	2
320	2016-08-15 15:19:25.690623-05	176	2016-08-15 19:50:21+00:00 - En Espera - 15.5	3		22	2
321	2016-08-15 15:19:25.707643-05	175	2016-08-15 19:50:46+00:00 - En Espera - 185.7	3		22	2
322	2016-08-15 15:19:25.710623-05	174	2016-08-15 19:49:56+00:00 - En Espera - 11.5	3		22	2
323	2016-08-15 15:19:25.714625-05	173	2016-08-15 19:49:14+00:00 - En Espera - 11.5	3		22	2
324	2016-08-15 15:19:25.717647-05	172	2016-08-15 19:48:42+00:00 - En Espera - 185.7	3		22	2
325	2016-08-15 15:19:25.721626-05	171	2016-08-15 19:43:29+00:00 - En Espera - 11.5	3		22	2
326	2016-08-15 15:19:25.724645-05	170	2016-08-15 19:42:37+00:00 - En Espera - 11.5	3		22	2
327	2016-08-15 15:19:25.729671-05	169	2016-08-15 18:35:19+00:00 - En Espera - 13.5	3		22	2
328	2016-08-15 15:19:25.732655-05	168	2016-08-15 18:10:37+00:00 - Transportista en Espera - 11.5	3		22	2
329	2016-08-15 15:19:25.736625-05	167	2016-08-15 18:01:56+00:00 - En Espera - 11.5	3		22	2
330	2016-08-15 15:19:25.739642-05	166	2016-08-15 18:00:55+00:00 - En Espera - 11.5	3		22	2
331	2016-08-15 15:19:25.742658-05	165	2016-08-15 17:40:55+00:00 - En Espera - 11.5	3		22	2
332	2016-08-15 15:19:25.745627-05	164	2016-08-15 15:14:55+00:00 - En Espera - 21.56	3		22	2
333	2016-08-15 15:19:25.749627-05	163	2016-08-15 15:06:57+00:00 - En Proceso - 10.0	3		22	2
334	2016-08-15 15:19:25.75367-05	162	2016-08-12 18:04:57+00:00 - En Espera - 13.5	3		22	2
335	2016-08-15 15:19:25.757669-05	161	2016-08-12 16:22:07+00:00 - En Espera - 14.5	3		22	2
336	2016-08-15 15:19:25.760652-05	160	2016-08-12 15:45:18+00:00 - Transportista en Espera - 11.5	3		22	2
337	2016-08-15 15:19:25.76467-05	159	2016-08-12 15:42:38+00:00 - En Espera - 11.5	3		22	2
338	2016-08-15 15:19:25.76767-05	158	2016-08-12 15:39:39+00:00 - En Proceso - 34.5	3		22	2
339	2016-08-15 15:19:25.771639-05	157	2016-08-12 15:35:37+00:00 - En Espera - 42.5	3		22	2
340	2016-08-15 15:19:25.774635-05	156	2016-08-12 15:32:50+00:00 - En Espera - 101.5	3		22	2
341	2016-08-15 15:19:25.777646-05	155	2016-08-12 15:23:11+00:00 - En Proceso - 82.5	3		22	2
342	2016-08-15 15:19:25.781664-05	154	2016-08-12 15:19:24+00:00 - En Espera - 45.5	3		22	2
343	2016-08-15 15:19:25.785668-05	153	2016-08-12 15:16:39+00:00 - En Espera - 24.5	3		22	2
344	2016-08-15 15:19:25.788646-05	152	2016-08-12 15:13:15+00:00 - En Espera - 13.0	3		22	2
345	2016-08-15 15:19:25.791646-05	151	2016-08-12 01:08:04+00:00 - En Espera - 23.5	3		22	2
346	2016-08-15 15:19:25.823632-05	150	2016-08-11 22:08:20+00:00 - En Espera - 3.0	3		22	2
347	2016-08-15 15:19:25.840633-05	149	2016-08-11 21:54:50+00:00 - En Proceso - 24.5	3		22	2
348	2016-08-15 17:56:12.384505-05	277	2016-08-15 22:54:24+00:00 - En Espera - 25.5	3		22	2
349	2016-08-15 17:56:12.40752-05	276	2016-08-15 22:53:54+00:00 - En Espera - 25.5	3		22	2
350	2016-08-15 17:56:12.411518-05	275	2016-08-15 22:53:19+00:00 - En Espera - 25.5	3		22	2
351	2016-08-15 17:56:12.414478-05	274	2016-08-15 22:52:54+00:00 - En Espera - 25.5	3		22	2
352	2016-08-15 17:56:12.418478-05	273	2016-08-15 22:52:11+00:00 - En Espera - 49.5	3		22	2
353	2016-08-15 17:56:12.422525-05	272	2016-08-15 22:51:31+00:00 - En Espera - 49.5	3		22	2
354	2016-08-15 17:56:12.425538-05	271	2016-08-15 22:47:35+00:00 - En Espera - 31.5	3		22	2
355	2016-08-15 17:56:12.429526-05	270	2016-08-15 22:46:55+00:00 - En Espera - 61.5	3		22	2
356	2016-08-15 17:56:12.432544-05	269	2016-08-15 22:45:27+00:00 - En Espera - 31.5	3		22	2
357	2016-08-15 17:56:12.435508-05	268	2016-08-15 22:43:40+00:00 - En Espera - 25.5	3		22	2
358	2016-08-15 17:56:12.438483-05	267	2016-08-15 22:43:09+00:00 - En Espera - 46.5	3		22	2
359	2016-08-15 17:56:12.441503-05	266	2016-08-15 22:42:19+00:00 - En Espera - 43.5	3		22	2
360	2016-08-15 17:56:12.445514-05	265	2016-08-15 22:20:24+00:00 - En Espera - 47.56	3		22	2
361	2016-08-15 17:56:12.44848-05	264	2016-08-15 22:18:14+00:00 - En Espera - 26.85	3		22	2
362	2016-08-15 17:56:25.339248-05	263	2016-08-15 22:16:59+00:00 - En Espera - 5.0	3		22	2
363	2016-08-15 17:56:25.356255-05	262	2016-08-15 22:16:53+00:00 - En Espera - 30.06	3		22	2
364	2016-08-15 17:56:25.374214-05	261	2016-08-15 22:15:53+00:00 - En Espera - 20.06	3		22	2
365	2016-08-15 17:56:25.383265-05	260	2016-08-15 22:09:49+00:00 - En Espera - 35.06	3		22	2
366	2016-08-15 17:56:25.391211-05	259	2016-08-15 22:09:11+00:00 - En Espera - 30.06	3		22	2
367	2016-08-15 17:56:25.398268-05	258	2016-08-15 22:08:24+00:00 - En Espera - 34.06	3		22	2
368	2016-08-15 17:56:25.406269-05	257	2016-08-15 22:07:22+00:00 - En Espera - 21.5	3		22	2
369	2016-08-15 17:56:25.423287-05	256	2016-08-15 22:06:32+00:00 - En Espera - 40.06	3		22	2
370	2016-08-15 17:56:25.431268-05	255	2016-08-15 22:05:39+00:00 - En Espera - 36.85	3		22	2
371	2016-08-15 17:56:25.439281-05	254	2016-08-15 22:04:58+00:00 - En Espera - 15.0	3		22	2
372	2016-08-15 17:56:25.447261-05	253	2016-08-15 22:03:46+00:00 - En Espera - 15.5	3		22	2
373	2016-08-15 17:56:25.456271-05	252	2016-08-15 22:03:17+00:00 - En Espera - 15.5	3		22	2
374	2016-08-15 17:56:25.465271-05	251	2016-08-15 22:01:48+00:00 - En Espera - 31.5	3		22	2
375	2016-08-15 17:56:25.472273-05	250	2016-08-15 22:01:05+00:00 - En Espera - 11.5	3		22	2
376	2016-08-15 17:56:25.481274-05	249	2016-08-15 22:00:22+00:00 - En Espera - 11.5	3		22	2
377	2016-08-15 17:56:25.490233-05	248	2016-08-15 21:59:11+00:00 - En Espera - 32.35	3		22	2
378	2016-08-15 17:56:25.495223-05	247	2016-08-15 21:58:07+00:00 - En Espera - 26.5	3		22	2
379	2016-08-15 17:56:25.50022-05	246	2016-08-15 21:13:08+00:00 - En Espera - 11.5	3		22	2
380	2016-08-15 17:56:25.505251-05	245	2016-08-15 21:10:55+00:00 - En Espera - 25.5	3		22	2
381	2016-08-15 17:56:25.510247-05	244	2016-08-15 21:10:15+00:00 - En Espera - 16.5	3		22	2
382	2016-08-15 17:56:25.515218-05	243	2016-08-15 21:09:32+00:00 - En Espera - 11.5	3		22	2
383	2016-08-15 17:56:25.520218-05	242	2016-08-15 20:50:10+00:00 - En Espera - 16.5	3		22	2
384	2016-08-15 17:56:25.52425-05	241	2016-08-15 20:49:04+00:00 - En Espera - 85.5	3		22	2
385	2016-08-15 17:56:25.528221-05	240	2016-08-15 20:48:20+00:00 - En Espera - 76.5	3		22	2
386	2016-08-15 17:56:25.53122-05	239	2016-08-15 20:47:21+00:00 - En Espera - 11.5	3		22	2
387	2016-08-15 17:56:25.535262-05	238	2016-08-15 20:46:24+00:00 - En Espera - 11.5	3		22	2
388	2016-08-16 15:58:33.289047-05	371	2016-08-16 20:51:04+00:00 - En Espera - 13.5	3		22	2
389	2016-08-16 15:58:33.336402-05	369	2016-08-16 20:47:24+00:00 - En Espera - 5.0	3		22	2
390	2016-08-16 15:58:33.351897-05	363	2016-08-16 20:30:07+00:00 - En Proceso - 17.0	3		22	2
391	2016-08-16 15:58:33.359898-05	360	2016-08-16 18:33:10+00:00 - En Espera - 5.0	3		22	2
392	2016-08-16 15:58:33.3687-05	357	2016-08-16 18:22:33+00:00 - En Espera - 5.0	3		22	2
393	2016-08-16 15:58:33.385322-05	349	2016-08-16 18:16:13+00:00 - En Espera - 5.0	3		22	2
394	2016-08-16 15:58:33.393937-05	345	2016-08-16 18:06:41+00:00 - En Espera - 5.0	3		22	2
395	2016-08-16 15:58:33.412865-05	342	2016-08-16 18:04:30+00:00 - En Espera - 13.5	3		22	2
396	2016-08-16 15:58:33.421671-05	334	2016-08-16 17:53:26+00:00 - En Espera - 5.0	3		22	2
397	2016-08-16 15:58:33.437816-05	324	2016-08-16 16:52:05+00:00 - En Espera - 13.5	3		22	2
398	2016-08-16 15:58:33.447397-05	321	2016-08-16 16:42:58+00:00 - En Espera - 21.5	3		22	2
399	2016-08-16 15:58:33.460043-05	317	2016-08-16 16:00:06+00:00 - En Espera - 5.0	3		22	2
400	2016-08-16 15:58:33.470878-05	313	2016-08-16 15:31:17+00:00 - En Espera - 11.5	3		22	2
401	2016-08-16 15:58:33.485025-05	309	2016-08-16 15:28:02+00:00 - En Espera - 5.0	3		22	2
402	2016-08-16 15:58:33.493333-05	306	2016-08-16 15:25:24+00:00 - En Espera - 15.5	3		22	2
403	2016-08-16 15:58:33.502952-05	303	2016-08-16 15:18:43+00:00 - En Espera - 5.0	3		22	2
404	2016-08-16 15:58:33.51519-05	300	2016-08-16 15:13:24+00:00 - En Espera - 5.0	3		22	2
405	2016-08-16 15:58:33.5271-05	294	2016-08-16 14:46:43+00:00 - En Espera - 5.0	3		22	2
406	2016-08-16 15:58:33.542577-05	290	2016-08-15 23:08:27+00:00 - Transportista en Espera - 69.5	3		22	2
407	2016-08-16 15:58:33.555691-05	286	2016-08-15 23:03:56+00:00 - En Espera - 57.5	3		22	2
408	2016-08-16 15:58:33.563709-05	282	2016-08-15 23:01:03+00:00 - En Proceso - 81.62	3		22	2
409	2016-08-16 15:58:33.577689-05	279	2016-08-15 22:57:36+00:00 - En Espera - 52.5	3		22	2
410	2016-08-16 15:58:33.585313-05	236	2016-08-15 20:44:40+00:00 - En Espera - 15.5	3		22	2
411	2016-08-16 15:58:33.593386-05	234	2016-08-15 20:39:15+00:00 - En Espera - 15.5	3		22	2
412	2016-08-16 15:59:27.582367-05	370	2016-08-16 20:49:52+00:00 - En Espera - 5.0	3		22	2
413	2016-08-16 15:59:27.597506-05	367	2016-08-16 20:41:25+00:00 - En Espera - 5.0	3		22	2
414	2016-08-16 15:59:27.617678-05	362	2016-08-16 18:43:33+00:00 - En Espera - 5.0	3		22	2
415	2016-08-16 15:59:27.625995-05	356	2016-08-16 18:21:44+00:00 - En Espera - 5.0	3		22	2
416	2016-08-16 15:59:27.632324-05	353	2016-08-16 18:18:28+00:00 - En Espera - 5.0	3		22	2
417	2016-08-16 15:59:27.639581-05	351	2016-08-16 18:17:16+00:00 - En Espera - 5.0	3		22	2
418	2016-08-16 15:59:27.650652-05	347	2016-08-16 18:11:17+00:00 - En Espera - 5.0	3		22	2
419	2016-08-16 15:59:27.657404-05	341	2016-08-16 18:02:26+00:00 - En Espera - 11.5	3		22	2
420	2016-08-16 15:59:27.67134-05	337	2016-08-16 17:57:47+00:00 - En Espera - 5.0	3		22	2
421	2016-08-16 15:59:27.68452-05	332	2016-08-16 17:52:21+00:00 - En Espera - 5.0	3		22	2
422	2016-08-16 15:59:27.695473-05	330	2016-08-16 17:51:30+00:00 - En Espera - 5.0	3		22	2
423	2016-08-16 15:59:27.709189-05	327	2016-08-16 17:42:47+00:00 - En Espera - 7.0	3		22	2
424	2016-08-16 15:59:27.719039-05	322	2016-08-16 16:46:06+00:00 - En Espera - 29.5	3		22	2
425	2016-08-16 15:59:27.725508-05	318	2016-08-16 16:01:10+00:00 - Enviado - 5.0	3		22	2
426	2016-08-16 15:59:27.735125-05	314	2016-08-16 15:35:12+00:00 - En Espera - 5.0	3		22	2
427	2016-08-16 15:59:27.744521-05	311	2016-08-16 15:29:22+00:00 - En Espera - 5.0	3		22	2
428	2016-08-16 15:59:27.758957-05	308	2016-08-16 15:26:01+00:00 - En Espera - 15.5	3		22	2
429	2016-08-16 15:59:27.777756-05	304	2016-08-16 15:23:58+00:00 - En Espera - 5.0	3		22	2
430	2016-08-16 15:59:27.795551-05	301	2016-08-16 15:14:16+00:00 - En Espera - 5.0	3		22	2
431	2016-08-16 15:59:27.805912-05	293	2016-08-16 14:44:46+00:00 - En Espera - 5.0	3		22	2
432	2016-08-16 15:59:27.817493-05	288	2016-08-15 23:05:29+00:00 - En Espera - 66.62	3		22	2
433	2016-08-16 15:59:27.847387-05	287	2016-08-15 23:05:57+00:00 - En Espera - 20.06	3		22	2
434	2016-08-16 15:59:27.859203-05	283	2016-08-15 23:01:57+00:00 - En Espera - 40.5	3		22	2
435	2016-08-16 15:59:27.870038-05	278	2016-08-15 22:57:16+00:00 - En Espera - 20.06	3		22	2
436	2016-08-16 15:59:27.88977-05	233	2016-08-15 20:39:02+00:00 - En Espera - 15.5	3		22	2
437	2016-08-16 15:59:27.900188-05	229	2016-08-15 20:37:18+00:00 - En Espera - 13.5	3		22	2
438	2016-08-16 15:59:27.911256-05	227	2016-08-15 20:36:50+00:00 - En Espera - 21.5	3		22	2
439	2016-08-16 15:59:27.918037-05	224	2016-08-15 20:35:13+00:00 - En Espera - 15.5	3		22	2
440	2016-08-16 15:59:27.926623-05	220	2016-08-15 20:34:03+00:00 - En Espera - 15.5	3		22	2
441	2016-08-16 15:59:27.934213-05	218	2016-08-15 20:33:56+00:00 - En Espera - 15.5	3		22	2
442	2016-08-16 15:59:27.945018-05	214	2016-08-15 20:33:38+00:00 - En Espera - 15.5	3		22	2
443	2016-08-16 16:00:03.274469-05	368	2016-08-16 20:45:26+00:00 - En Espera - 5.0	3		22	2
444	2016-08-16 16:00:03.285051-05	364	2016-08-16 20:31:35+00:00 - En Espera - 17.0	3		22	2
445	2016-08-16 16:00:03.299557-05	355	2016-08-16 18:20:04+00:00 - En Espera - 5.0	3		22	2
446	2016-08-16 16:00:03.309155-05	346	2016-08-16 18:10:45+00:00 - En Espera - 5.0	3		22	2
447	2016-08-16 16:00:03.326074-05	340	2016-08-16 18:01:48+00:00 - En Espera - 20.06	3		22	2
448	2016-08-16 16:00:03.333328-05	335	2016-08-16 17:53:59+00:00 - En Espera - 5.0	3		22	2
449	2016-08-16 16:00:03.343213-05	328	2016-08-16 17:49:49+00:00 - En Espera - 5.0	3		22	2
450	2016-08-16 16:00:03.358791-05	323	2016-08-16 16:47:32+00:00 - En Espera - 21.5	3		22	2
451	2016-08-16 16:00:03.366436-05	316	2016-08-16 15:40:55+00:00 - En Espera - 11.5	3		22	2
452	2016-08-16 16:00:03.37685-05	310	2016-08-16 15:28:40+00:00 - En Espera - 5.0	3		22	2
453	2016-08-16 16:00:03.391155-05	291	2016-08-16 13:59:33+00:00 - En Espera - 18.5	3		22	2
454	2016-08-16 16:00:03.409153-05	284	2016-08-15 23:02:49+00:00 - En Espera - 57.5	3		22	2
455	2016-08-16 16:00:03.421456-05	235	2016-08-15 20:40:15+00:00 - En Espera - 20.06	3		22	2
456	2016-08-16 16:00:03.428407-05	230	2016-08-15 20:38:25+00:00 - En Espera - 13.5	3		22	2
457	2016-08-16 16:00:03.441492-05	223	2016-08-15 20:34:54+00:00 - En Espera - 5.0	3		22	2
458	2016-08-16 16:00:03.449691-05	215	2016-08-15 20:33:42+00:00 - En Espera - 15.5	3		22	2
459	2016-08-16 16:00:03.457808-05	209	2016-08-15 20:32:30+00:00 - Enviado - 5.0	3		22	2
460	2016-08-16 16:00:03.465987-05	206	2016-08-15 20:20:34+00:00 - Enviado - 19.5	3		22	2
461	2016-08-16 16:46:32.977963-05	395	2016-08-16 21:45:05+00:00 - En Espera - 5.0	3		22	2
462	2016-08-16 16:46:32.991182-05	394	2016-08-16 21:31:58+00:00 - En Espera - 5.0	3		22	2
463	2016-08-16 16:46:33.00794-05	393	2016-08-16 21:26:33+00:00 - En Espera - 5.0	3		22	2
464	2016-08-16 16:46:33.023172-05	392	2016-08-16 21:26:10+00:00 - En Espera - 15.5	3		22	2
465	2016-08-16 16:46:33.031524-05	391	2016-08-16 21:25:58+00:00 - En Espera - 15.5	3		22	2
466	2016-08-16 16:46:33.046951-05	390	2016-08-16 21:25:16+00:00 - En Espera - 5.0	3		22	2
467	2016-08-16 16:46:33.057018-05	389	2016-08-16 21:24:49+00:00 - En Espera - 5.0	3		22	2
468	2016-08-16 16:46:33.071949-05	388	2016-08-16 21:21:54+00:00 - En Espera - 98.5	3		22	2
469	2016-08-16 16:46:33.080835-05	387	2016-08-16 21:20:52+00:00 - En Espera - 5.0	3		22	2
470	2016-08-16 16:46:33.093739-05	386	2016-08-16 21:19:51+00:00 - En Espera - 15.5	3		22	2
471	2016-08-16 16:46:33.112771-05	385	2016-08-16 21:19:08+00:00 - En Espera - 33.5	3		22	2
472	2016-08-16 16:46:33.123541-05	384	2016-08-16 21:18:41+00:00 - En Espera - 5.0	3		22	2
473	2016-08-16 16:46:33.136829-05	383	2016-08-16 21:18:10+00:00 - En Espera - 5.0	3		22	2
474	2016-08-16 16:46:33.149478-05	382	2016-08-16 21:18:01+00:00 - En Espera - 33.5	3		22	2
475	2016-08-16 16:46:33.163832-05	381	2016-08-16 21:16:44+00:00 - En Espera - 11.5	3		22	2
476	2016-08-16 16:46:33.172023-05	380	2016-08-16 21:16:32+00:00 - En Espera - 5.0	3		22	2
477	2016-08-16 16:46:33.18138-05	379	2016-08-16 21:13:33+00:00 - En Espera - 5.0	3		22	2
478	2016-08-16 16:46:33.193708-05	378	2016-08-16 21:06:04+00:00 - En Espera - 5.0	3		22	2
479	2016-08-16 16:46:33.211933-05	377	2016-08-16 21:05:40+00:00 - En Espera - 5.0	3		22	2
480	2016-08-16 16:46:33.225316-05	376	2016-08-16 21:05:08+00:00 - En Espera - 5.0	3		22	2
481	2016-08-16 16:46:33.239102-05	375	2016-08-16 21:04:26+00:00 - En Espera - 5.0	3		22	2
482	2016-08-16 16:46:33.253151-05	374	2016-08-16 21:04:10+00:00 - En Espera - 16.5	3		22	2
483	2016-08-16 16:46:33.264254-05	373	2016-08-16 21:01:26+00:00 - En Espera - 5.0	3		22	2
484	2016-08-16 16:46:33.272254-05	372	2016-08-16 20:51:21+00:00 - En Espera - 5.0	3		22	2
485	2016-08-16 16:46:33.281215-05	366	2016-08-16 20:41:02+00:00 - En Espera - 7.0	3		22	2
486	2016-08-16 16:46:33.289096-05	365	2016-08-16 20:40:57+00:00 - En Espera - 7.0	3		22	2
487	2016-08-16 16:46:33.301575-05	361	2016-08-16 18:35:14+00:00 - En Espera - 4.5	3		22	2
488	2016-08-16 16:46:33.319634-05	359	2016-08-16 18:23:42+00:00 - Transportista en Espera - 11.5	3		22	2
489	2016-08-16 16:46:33.331841-05	358	2016-08-16 18:23:01+00:00 - En Espera - 5.0	3		22	2
490	2016-08-16 16:46:33.347292-05	354	2016-08-16 18:19:42+00:00 - En Espera - 5.0	3		22	2
491	2016-08-16 16:46:33.359864-05	352	2016-08-16 18:17:54+00:00 - En Espera - 5.0	3		22	2
492	2016-08-16 16:46:33.37235-05	350	2016-08-16 18:16:29+00:00 - En Espera - 5.0	3		22	2
493	2016-08-16 16:46:33.382334-05	348	2016-08-16 18:11:52+00:00 - En Espera - 5.0	3		22	2
494	2016-08-16 16:46:33.390115-05	344	2016-08-16 18:06:31+00:00 - En Espera - 5.0	3		22	2
495	2016-08-16 16:46:33.404013-05	343	2016-08-16 18:06:01+00:00 - En Espera - 5.0	3		22	2
496	2016-08-16 16:46:33.424488-05	339	2016-08-16 17:59:35+00:00 - En Espera - 5.0	3		22	2
497	2016-08-16 16:46:33.436729-05	338	2016-08-16 17:58:32+00:00 - En Espera - 5.0	3		22	2
498	2016-08-16 16:46:33.455543-05	336	2016-08-16 17:57:02+00:00 - En Espera - 5.0	3		22	2
499	2016-08-16 16:46:33.467455-05	333	2016-08-16 17:52:50+00:00 - En Espera - 5.0	3		22	2
500	2016-08-16 16:46:33.480961-05	331	2016-08-16 17:52:14+00:00 - En Espera - 5.0	3		22	2
501	2016-08-16 16:46:33.492288-05	329	2016-08-16 17:50:42+00:00 - En Espera - 5.0	3		22	2
502	2016-08-16 16:46:33.508276-05	326	2016-08-16 16:52:05+00:00 - En Espera - 25.5	3		22	2
503	2016-08-16 16:46:33.528008-05	325	2016-08-16 16:51:28+00:00 - En Espera - 37.5	3		22	2
504	2016-08-16 16:46:33.542303-05	320	2016-08-16 16:42:21+00:00 - En Espera - 8.5	3		22	2
505	2016-08-16 16:46:33.557148-05	319	2016-08-16 16:41:10+00:00 - En Espera - 21.5	3		22	2
506	2016-08-16 16:46:33.571322-05	315	2016-08-16 15:39:33+00:00 - En Espera - 5.0	3		22	2
507	2016-08-16 16:46:33.580811-05	312	2016-08-16 15:30:19+00:00 - En Espera - 5.0	3		22	2
508	2016-08-16 16:46:33.589686-05	307	2016-08-16 15:25:50+00:00 - En Espera - 15.5	3		22	2
509	2016-08-16 16:46:33.606023-05	305	2016-08-16 15:24:27+00:00 - En Espera - 5.0	3		22	2
510	2016-08-16 16:46:33.621095-05	302	2016-08-16 15:16:46+00:00 - En Espera - 5.0	3		22	2
511	2016-08-16 16:46:33.633962-05	299	2016-08-16 15:11:21+00:00 - En Espera - 5.0	3		22	2
512	2016-08-16 16:46:33.649642-05	298	2016-08-16 14:53:56+00:00 - En Espera - 5.0	3		22	2
513	2016-08-16 16:46:33.668806-05	297	2016-08-16 14:53:03+00:00 - En Espera - 5.0	3		22	2
514	2016-08-16 16:46:33.680462-05	296	2016-08-16 14:52:30+00:00 - En Espera - 5.0	3		22	2
515	2016-08-16 16:46:33.693191-05	295	2016-08-16 14:47:45+00:00 - En Espera - 5.0	3		22	2
516	2016-08-16 16:46:33.702266-05	292	2016-08-16 14:44:25+00:00 - En Espera - 5.0	3		22	2
517	2016-08-16 16:46:33.718646-05	289	2016-08-15 23:06:55+00:00 - En Espera - 66.62	3		22	2
518	2016-08-16 16:46:33.731043-05	285	2016-08-15 23:04:42+00:00 - En Espera - 16.5	3		22	2
519	2016-08-16 16:46:33.742859-05	281	2016-08-15 23:01:30+00:00 - En Espera - 63.5	3		22	2
520	2016-08-16 16:46:33.755651-05	280	2016-08-15 22:57:36+00:00 - En Espera - 52.5	3		22	2
521	2016-08-16 16:46:33.771153-05	237	2016-08-15 20:45:35+00:00 - En Espera - 16.5	3		22	2
522	2016-08-16 16:46:33.780468-05	232	2016-08-15 20:38:46+00:00 - En Espera - 5.0	3		22	2
523	2016-08-16 16:46:33.789188-05	231	2016-08-15 20:38:26+00:00 - En Espera - 13.5	3		22	2
524	2016-08-16 16:46:33.801251-05	228	2016-08-15 20:36:58+00:00 - En Espera - 11.5	3		22	2
525	2016-08-16 16:46:33.814288-05	226	2016-08-15 20:35:51+00:00 - En Espera - 13.5	3		22	2
526	2016-08-16 16:46:33.83391-05	225	2016-08-15 20:35:49+00:00 - En Espera - 13.5	3		22	2
527	2016-08-16 16:46:33.848436-05	222	2016-08-15 20:34:17+00:00 - En Espera - 29.5	3		22	2
528	2016-08-16 16:46:33.864754-05	221	2016-08-15 20:34:16+00:00 - En Espera - 11.5	3		22	2
529	2016-08-16 16:46:33.881529-05	219	2016-08-15 20:34:00+00:00 - En Espera - 15.5	3		22	2
530	2016-08-16 16:46:33.892023-05	217	2016-08-15 20:33:50+00:00 - En Espera - 15.5	3		22	2
531	2016-08-16 16:46:33.908131-05	216	2016-08-15 20:33:45+00:00 - En Espera - 20.06	3		22	2
532	2016-08-16 16:46:33.926333-05	213	2016-08-15 20:33:32+00:00 - En Espera - 15.5	3		22	2
533	2016-08-16 16:46:33.937621-05	212	2016-08-15 20:33:30+00:00 - En Espera - 11.5	3		22	2
534	2016-08-16 16:46:33.949973-05	211	2016-08-15 20:33:22+00:00 - En Espera - 15.5	3		22	2
535	2016-08-16 16:46:33.966074-05	210	2016-08-15 20:33:19+00:00 - En Espera - 15.5	3		22	2
536	2016-08-16 16:46:33.980476-05	208	2016-08-15 20:21:43+00:00 - Transportista en Espera - 23.5	3		22	2
537	2016-08-16 16:46:33.989103-05	207	2016-08-15 20:20:06+00:00 - En Proceso - 11.5	3		22	2
538	2016-08-16 16:46:33.997716-05	205	2016-08-15 20:19:03+00:00 - En Proceso - 11.5	3		22	2
539	2016-08-16 16:46:34.014939-05	204	2016-08-15 20:18:48+00:00 - En Espera - 27.0	3		22	2
540	2016-08-17 08:34:54.903704-05	3	Casa china	2	Modificado/a me_gusta.	16	2
541	2016-08-17 08:35:03.578748-05	9	Gusy	2	Modificado/a me_gusta.	16	2
542	2016-08-17 08:35:11.735949-05	7	PapaLogo	2	Modificado/a me_gusta.	16	2
543	2016-08-17 08:35:20.806316-05	5	Chamelion	2	Modificado/a me_gusta.	16	2
544	2016-08-17 08:35:29.461491-05	3	Casa china	2	No ha cambiado ningún campo.	16	2
545	2016-08-17 08:35:38.456287-05	1	Pizzaa	2	Modificado/a me_gusta.	16	2
546	2016-08-17 10:27:40.470208-05	9	Gusy	2	Modificado/a me_gusta.	16	2
547	2016-08-17 15:41:34.231239-05	1	Pizzaa	2	Modificado/a descripcion y precio_envio.	16	2
548	2016-08-18 09:59:36.13953-05	9	papalogo	2	Modificado/a groups.	5	2
549	2016-08-18 09:59:56.112283-05	7	icecreamsocial	2	Modificado/a groups.	5	2
550	2016-08-18 10:10:18.979329-05	99	ChifaBeijing	1	Añadido.	5	2
551	2016-08-18 10:10:30.14378-05	99	ChifaBeijing	2	Modificado/a groups.	5	2
552	2016-08-18 10:25:07.591438-05	130	ChifaBeijing	2	Modificado/a cedula, fecha_nacimiento, telefono, genero y ruta_foto.	9	2
553	2016-08-18 10:29:36.26573-05	10	Chifa Beijing	1	Añadido.	11	2
554	2016-08-18 10:36:56.307153-05	95	24 de Mayo - Miguel Riofrio. Frente a escueka. Número de casa: N/A	1	Añadido.	13	2
555	2016-08-18 10:37:16.826924-05	10	Chifa Beijing - Norte	1	Añadido.	16	2
556	2016-08-18 11:12:14.572136-05	99	ChifaBeijing	2	No ha cambiado ningún campo.	5	2
557	2016-08-18 16:40:39.905377-05	95	24 de Mayo - Miguel Riofrio. Frente a escueka. Número de casa: N/A	2	Modificado/a latitud y longitud.	13	2
558	2016-08-18 16:41:33.560797-05	95	24 de Mayo - Miguel Riofrio. Frente a escueka. Número de casa: N/A	2	Modificado/a latitud.	13	2
559	2016-08-18 16:42:53.936231-05	1	Av. Universitariaaaaaaaa - Mercadillo. Frente al León. Número de casa: 25-65	2	Modificado/a latitud y longitud.	13	2
560	2016-08-19 13:57:44.854944-05	101	Posix	2	Modificado/a password.	5	2
561	2016-08-19 16:32:15.591094-05	103	pizzaHut	1	Añadido.	5	2
562	2016-08-19 16:32:26.666919-05	103	pizzaHut	2	Modificado/a groups.	5	2
563	2016-08-19 16:36:53.03975-05	134	pizzaHut	2	Modificado/a cedula, fecha_nacimiento, telefono, genero y ruta_foto.	9	2
564	2016-08-19 16:37:46.066761-05	11	PizzaHut	1	Añadido.	11	2
565	2016-08-19 16:39:44.249303-05	11	PizzaHut - Zamora	1	Añadido.	16	2
566	2016-08-19 17:11:31.628567-05	98	Av. 18 de Noviembre - MErcadillo. Esquina. Número de casa: 12-89	1	Añadido.	13	2
567	2016-08-19 17:11:39.927858-05	11	PizzaHut - Zamora	2	Modificado/a id_direccion.	16	2
568	2016-08-22 16:12:04.375255-05	5	Simple	1	Añadido.	25	2
569	2016-08-22 16:12:13.819817-05	6	Doble	1	Añadido.	25	2
570	2016-08-22 16:20:00.71473-05	33	Pastel Tres Leches	2	Modificado/a ruta_foto.	24	2
571	2016-08-22 16:30:25.793383-05	9	Pasteles	1	Añadido.	23	2
572	2016-08-22 16:38:14.808345-05	2	sucre - miguel riofrío. 1312321. Número de casa: 	2	Modificado/a id_perfil.	13	2
573	2016-08-22 16:38:16.547911-05	4	Ice Cream Social	2	No ha cambiado ningún campo.	16	2
574	2016-08-24 16:44:58.852769-05	112	nadia28	2	Modificado/a email.	5	2
575	2016-08-24 20:09:11.269157-05	114	soyard	1	Añadido.	5	2
576	2016-08-24 20:09:18.400471-05	114	soyard	2	Modificado/a groups.	5	2
577	2016-08-24 20:10:56.090899-05	8	Bebidas	1	Añadido.	10	2
578	2016-08-24 20:11:07.003295-05	12	Soyard	1	Añadido.	11	2
579	2016-08-24 20:13:43.719868-05	12	Soyard	1	Añadido.	16	2
580	2016-08-24 20:15:10.522809-05	57	be	1	Añadido.	24	2
581	2016-08-25 10:20:19.425285-05	162	Paris - Paris. En Prendho. Número de casa: 12-89	1	Añadido.	13	2
582	2016-08-25 10:20:22.714245-05	12	Soyard	2	Modificado/a id_direccion.	16	2
583	2016-08-25 10:30:00.908536-05	10	Bebidas	1	Añadido.	23	2
584	2016-08-25 10:30:04.300939-05	57	Bebida de Maracuyá	2	Modificado/a ruta_foto y id_platocategoria.	24	2
585	2016-08-25 11:17:50.635036-05	115	locochavez	1	Añadido.	5	2
586	2016-08-25 11:17:57.845121-05	115	locochavez	2	Modificado/a groups.	5	2
587	2016-08-25 11:18:58.095872-05	13	Loco Chavez	1	Añadido.	11	2
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 587, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_content_type (id, app_label, model) FROM stdin;
2	admin	logentry
3	auth	permission
4	auth	group
5	auth	user
6	contenttypes	contenttype
7	sessions	session
8	spaapp	actividadcomercial
9	spaapp	perfil
10	spaapp	categoria
11	spaapp	proveedor
12	spaapp	ciudad
13	spaapp	direccion
14	spaapp	cliente
15	spaapp	menu
16	spaapp	sucursal
17	spaapp	contrato
18	spaapp	transportista
19	spaapp	coordenadas
20	spaapp	tipopago
21	spaapp	tipopedido
22	spaapp	pedido
23	spaapp	platocategoria
24	spaapp	plato
25	spaapp	tamanio
26	spaapp	tamanioplato
27	spaapp	detallepedido
28	spaapp	ingrediente
29	spaapp	horariotransportista
30	spaapp	modificardetallepedido
31	spaapp	platoingrediente
32	spaapp	redsocial
33	spaapp	valoracion
34	administracionAPP	contacto
35	corsheaders	corsmodel
36	default	usersocialauth
37	default	nonce
38	default	association
39	default	code
40	spaapp	catalogoestado
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_content_type_id_seq', 40, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2016-07-28 19:51:48.787104-05
2	auth	0001_initial	2016-07-28 19:51:49.5139-05
3	spaapp	0001_initial	2016-07-28 19:51:53.27189-05
4	admin	0001_initial	2016-07-28 19:54:38.535332-05
5	admin	0002_logentry_remove_auto_add	2016-07-28 19:54:38.590942-05
6	administracionAPP	0001_initial	2016-07-28 19:54:38.653486-05
7	contenttypes	0002_remove_content_type_name	2016-07-28 19:54:38.738091-05
8	auth	0002_alter_permission_name_max_length	2016-07-28 19:54:38.76934-05
9	auth	0003_alter_user_email_max_length	2016-07-28 19:54:38.807789-05
10	auth	0004_alter_user_username_opts	2016-07-28 19:54:38.83907-05
11	auth	0005_alter_user_last_login_null	2016-07-28 19:54:38.870279-05
12	auth	0006_require_contenttypes_0002	2016-07-28 19:54:38.889376-05
13	auth	0007_alter_validators_add_error_messages	2016-07-28 19:54:38.908148-05
14	default	0001_initial	2016-07-28 19:54:39.527389-05
15	default	0002_add_related_name	2016-07-28 19:54:39.675726-05
16	default	0003_alter_email_max_length	2016-07-28 19:54:39.697855-05
17	default	0004_auto_20160423_0400	2016-07-28 19:54:39.745077-05
18	sessions	0001_initial	2016-07-28 19:54:39.876252-05
19	spaapp	0002_detallepedido_tamanio	2016-08-04 13:29:59.024192-05
20	spaapp	1000_permisos	2016-08-08 07:42:38.729428-05
21	spaapp	1001_tamanioplato_activo	2016-08-09 10:08:05.982222-05
22	spaapp	1001_auto_20160809_2046	2016-08-09 15:46:49.71865-05
23	spaapp	1002_auto_20160809_2048	2016-08-09 15:49:04.468895-05
24	spaapp	1003_auto_20160809_2048	2016-08-09 15:51:40.288235-05
25	spaapp	1004_auto_20160809_2052	2016-08-09 15:52:33.585714-05
26	spaapp	0002_auto_20160809_1537	2016-08-11 14:00:24.587069-05
27	spaapp	1001_merge	2016-08-11 14:00:24.591069-05
28	spaapp	1002_auto_20160811_1355	2016-08-11 14:00:25.551934-05
29	spaapp	0500_catalogo_estados	2016-08-11 14:00:25.589216-05
30	spaapp	1003_merge	2016-08-11 14:00:25.592216-05
31	spaapp	1004_auto_20160815_1245	2016-08-15 12:46:22.105552-05
32	spaapp	1002_merge	2016-08-15 13:07:33.639963-05
33	spaapp	1005_plato_activo	2016-08-15 13:11:54.542186-05
34	spaapp	1006_auto_20160817_1508	2016-08-17 15:08:36.604499-05
35	spaapp	1007_auto_20160817_1845	2016-08-17 18:45:32.827616-05
36	spaapp	0502_auto_20160818_1215	2016-08-18 17:21:25.852132-05
37	spaapp	0503_auto_20160818_1615	2016-08-18 17:23:05.067657-05
38	spaapp	0504_merge	2016-08-18 17:23:05.090659-05
39	spaapp	1005_merge	2016-08-22 13:51:53.635316-05
40	spaapp	1006_auto_20160822_1351	2016-08-22 13:58:36.597302-05
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_migrations_id_seq', 40, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
ppj6kicf3jq9qp14in8osrwxyzvyeccb	ZDA1MzA1NDA1YjY2NzZlZGRmYjQxZWQwMTA3ZWY4MTQ5MjFjN2FiZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-11 19:56:47.864751-05
byyjg5tr2lz8a75b7c2mmliyrulhelmn	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:50:25.121346-05
r8m0atiszs8wyzbm0pvq6m40y3kz8s7p	ZWMyZWIwZTJiYWFlMDczYzRhODhlMWU0Y2M2YzI2MGE5YTljMzUxNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-11 20:20:31.486666-05
629ms707i92p3h9lxr6l82is7mprz3pi	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-17 13:12:25.735389-05
p1ruep8o7eq076v57rzgybu0el5ocbot	Y2ZkZWQ4OTRlZDQwNTRhZjI5MDViMDE2NDFmYWJmNjM3MjJmYjNmZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-17 13:32:49.86211-05
ylf1lnnjy8djcgm278nff0s1z9ndn2z1	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:51:54.426512-05
qicye6ty655n7h5d7hadcqg0ay3wm1wt	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:54:26.189291-05
iqmq1opk3wwyfn1qrs1rpz8org64flh3	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-12 09:47:28.13426-05
ll4fa4qy76k81x55mm3n6gnn4qpi35dk	NzU0NDI1NTE2ZmM2NTFkZTBjM2IxOWFjNGZlMGNmZDJkZGY5MGUxNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDg1Yjk4ZWJjZmMzYzk4Y2MzZjBiNDI2MTA0MTAwMjA5MTI3Yjc3OSIsIl9hdXRoX3VzZXJfaWQiOiI1In0=	2016-08-12 10:26:24.92008-05
agwowo7ygokt7301dv34pdwlw0c2avqe	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:00:32.628856-05
jk1xb61xhok3bn9errznmbtripf8ziir	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:01:26.54987-05
92jp9m02v310e1dbl3id5kqcihznnu3j	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:02:37.675142-05
tef6cdhg87ijg2n3sesh6gt2fprypow5	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:04:05.819932-05
8dt7o50nmaz51q0475x5hkkcf4s58ab2	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:06:18.776876-05
ohyakvkhwhrg344rv1axf8vj33k9lvul	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:07:33.506678-05
xmqm3z1nwcshedj2j3rmrfoaqqxgo7z9	ZWY5MWMzMDczYzY5YjkzMTg1Y2ZkYTBmOTc2NjRlOTYwODkyODk1Njp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE2IiwiX2F1dGhfdXNlcl9oYXNoIjoiMjQ1MGJiYmU3ZTAxYzkwNzE0NDFlN2UwOGVkNmIyZDg1NzA0MWZmMyJ9	2016-08-15 19:00:06.442762-05
fbfmnokg6ds0n6v2b77md75kro2zxhgv	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:08:33.922357-05
69931dblja1ngvaqaablty4dic7mnekf	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-12 11:46:15.503914-05
hieocm5ufi9vr9mn2uakge9nklm0or51	Y2ZkZWQ4OTRlZDQwNTRhZjI5MDViMDE2NDFmYWJmNjM3MjJmYjNmZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-16 16:44:17.221614-05
c77q8j8lpupnnhfwctjq06v47dmnncib	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:09:32.99599-05
dmuyidbk4yjiar7zoekt9r8euit3yc5o	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:11:20.387611-05
60a28po4ajzfi64lcuy4yl2oaxt3exka	YjlhZTcyNmUyNjcyNWZkNjMzZWM0MmFjYWE4MzVmYzI5ZDNhNTc3Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCIsIl9hdXRoX3VzZXJfaWQiOiIxMSJ9	2016-08-16 16:13:40.970215-05
60yv4yb71z7j5gpkfcunhuhksx87ny4z	NzBkNDRkZmQwZThkMzgxNTg0NTY4ZDkxMjdiNGQ2YzBlZGQ5NWEyNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjIxMjZkNzBlY2Q4NGI4MzdlMTJkODUwMDRhNWNmYTU3YTg0YTJiMTEiLCJfYXV0aF91c2VyX2lkIjoiMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 18:47:39.240926-05
4i86no7b1ec4wewwws5bdgr2wpo3ans9	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-16 19:41:50.740148-05
evtirjme540ojzrh7ehbiwjx22p4n2v3	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-08-19 18:47:42.733928-05
3ybo7v1j7ey0ljhqv7uzjovh7ta1jjwb	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-24 18:44:05.407222-05
eeuobzj5zpineqanawnih17mq3lcn5y1	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-01 13:46:28.610109-05
4dyomumckxcmdl3l8xxponnus6xainp4	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-06 17:45:52.227769-05
1lr1tqnzdo2np9on07zc9r0oobvyamnu	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-23 17:31:07.945934-05
zye1bqyutmq5l6zejbutul5usfn7sjyl	ZDA1MzA1NDA1YjY2NzZlZGRmYjQxZWQwMTA3ZWY4MTQ5MjFjN2FiZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-30 16:46:14.330809-05
kytdr35051wmubtsqa4vqs2tp8w9vfxp	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-23 05:15:42.789643-05
ze1r4i88d4r26y42ds2c0jrbvelzb0cz	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:15:59.871789-05
z68i0whmlxvuljujrmyajl7g3zgljevj	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 19:20:06.562403-05
ql2o5uo6mu6ywxqwhltdl9p8p85dfvij	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-09-01 16:28:04.512732-05
tsngnr2tfbadbtr7wvrfudjqijtr8zy5	YzkwMTY2MDI5NGEyMWYwY2VkMDgyZjIwMjlkYTFiNzIxNTgwMmNiZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2016-09-07 20:08:47.245309-05
9ly8onkb53mwi075rk7pbannfzqcqzga	ZDA1MzA1NDA1YjY2NzZlZGRmYjQxZWQwMTA3ZWY4MTQ5MjFjN2FiZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-22 12:38:12.891261-05
ay54oths2udvc2kb7ni4yqq64sv9tp9a	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-30 11:41:36.482005-05
nu4sf6evhok3ysn1pr56eollr88wk3yn	ZmM5OWI1Y2Y5NDI0NjVjZjBkYmYxODQ2ZmZmNGUzNzZjMTVjODc2NDp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxOTcyNmVjNDQwYWU4ZGQwZWE2OGRlMjAyNTgzZWU3NzAzNjUwNGUiLCJfYXV0aF91c2VyX2lkIjoiMTA5IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==	2016-09-07 13:26:19.593557-05
ral9hrcs02558j5vlip6fhqjdddthupw	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-31 14:53:28.753123-05
wxxpqkuqdv7i1hqzdr1y60mq3yi7e56l	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-29 15:56:58.172272-05
7ltwkygblhldn3ez72zfhl67rocb4nyu	OTI2ZTQwZTcyNDVlNzE5MzFjYjg1Mjk3OWJmYTYzOTk1N2ZhZDVjYjp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 17:43:50.34102-05
x2qn0144vebevtpfetenwwthbnl4zukt	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-08 10:17:39.365019-05
6sv390nzzpns6itaa6qdhjg5yt8643un	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-29 17:16:24.426592-05
6i4hbzr5kptc6qfwk9cf13ztq06x0uww	ZDQ2NGY4NWZlZjE0NGZjOGE1Y2Y2NDFhYTNlNzFjZDhkNGVkMzExYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjMiLCJfYXV0aF91c2VyX2hhc2giOiI3ZDE4YjdlOWQxYjc3OWQ0MmYyZWNiYWFmZjBmNjgxMmU3ZTVjYWE4In0=	2016-08-30 17:23:14.029497-05
2n9yjji6phsj621ykvkubcj75h0oo6fr	YzkwMTY2MDI5NGEyMWYwY2VkMDgyZjIwMjlkYTFiNzIxNTgwMmNiZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2016-09-01 08:39:54.796726-05
s6umrqh2lnybftldn25w3w4vwygjj9k3	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-31 16:32:52.495899-05
colula2jy9vcgdfor803uv4h6wydr5t6	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-31 16:56:32.973538-05
knqzjjzntzh0fnmbqohz29v7v6681dyx	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-17 09:58:36.279725-05
97f99r15y8v31uekudgu94yhbtflcvl1	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:52:37.461965-05
smhocihg4q6uxa7k95h5bviqpg5ashi7	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-05 12:58:29.67196-05
haohankyyn1vaqqvkdu9do5ntxl04rsb	M2JkZWMxZWJiMDRkYTJiYzE0MDc0NWY0YjJkNzgxNTAwZTQxNTQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjVmNmQyNzRjZmVmNzMyYTQ3NjlhZjIwOTk3MGVjNjQwODczZTI4MDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMyJ9	2016-08-19 15:29:30.612875-05
v2a616t4h3fou3lfmpbfodsswp94o7vm	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 18:58:05.177444-05
6r13xr1pwic7gk2da7a38g95mxjlmswi	OWU5MDZjMzcwODU0OTJkYmQ3MmMwOGRmN2NiMjAzMmU4ZDk3NmE1ZDp7ImZhY2Vib29rX3N0YXRlIjoiQVh5NEgyd21Kd3hFZml0VVJNTXBwcGhQTldXcElabzQifQ==	2016-08-23 12:38:15.781441-05
0n7p7dxbdvrgyfrgomt94oqbm689fqio	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-18 18:11:00.613094-05
d812mif0fza9glx9hmuc6oq3fpfxd3m3	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 19:00:13.095156-05
i8ojuq4kc47nfzj6ka53s7hc7lpqrefw	NGM0MDQwOTM1YmVjMDFjYmFlMzMyYmIyZjZlYjI5MjM5NGQyNWQyZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkMThiN2U5ZDFiNzc5ZDQyZjJlY2JhYWZmMGY2ODEyZTdlNWNhYTgiLCJfYXV0aF91c2VyX2lkIjoiMyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 19:23:02.228264-05
30sspuhkbhyasr7dmlnyvtl9c5c6ql10	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-18 18:32:23.211228-05
0d4hy45xys5xh2p1p894r6xu08tgb5vx	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:16:31.408975-05
gjhnqtcwa2c4np85vus9nv5vn23pd32j	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-25 10:23:55.109557-05
o8ft3afaf2bbvf6fbxjz3mpu1tp7nz1f	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-23 05:16:59.421808-05
sizmi1n04eiafgirv8wxrtlpwrgyud95	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-22 11:19:34.676542-05
vlyavao82e1h3x9z1qxpvwbc68ypg1xl	YjlhZTcyNmUyNjcyNWZkNjMzZWM0MmFjYWE4MzVmYzI5ZDNhNTc3Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCIsIl9hdXRoX3VzZXJfaWQiOiIxMSJ9	2016-08-23 15:40:39.532232-05
07w4enkhj4wldnbmf8xgkj4p3goydw4u	NmMyMTJlZmFhODZlZTViY2I4OGZkMmFkZmYxNGFmOTA4NDc5Zjg3YTp7Il9hdXRoX3VzZXJfaGFzaCI6ImQ4NWI5OGViY2ZjM2M5OGNjM2YwYjQyNjEwNDEwMDIwOTEyN2I3NzkiLCJfYXV0aF91c2VyX2lkIjoiNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-23 18:21:54.059464-05
6hlx6f6dqh31r1f3awpbolopknf1i1c8	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-25 10:41:37.826063-05
t3pefx82p657mnszzcccaf09g0bf97q6	ZDI5YzBlOTI1NjU0NDU0MmZlZDQ0Y2EwMTEyYTI3MmQ5MDIzYzM2Yjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-25 13:58:23.795385-05
8vbealdgqru37rlelimdm886oigzd7jf	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 10:16:20.85285-05
mprpxxw2f4bbzgetii01nlizoczzge3e	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-24 10:21:10.422381-05
7s5sv44unuaj75akivkj5l25sjzvb4rt	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-08-25 17:14:13.755431-05
30z7ude3jl4xpim8onao87oruc30h2d1	MjVjNzBiYTk1N2RmNDJlYjlkNWM1NGYzNzFjZThjYjZjMjVkNzk1MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzFkNWI4MThhMWU3NDViNmIwMzNjMGY2Y2JmZTFiNWU2ZWE4N2JjNyIsIl9hdXRoX3VzZXJfaWQiOiI3NCJ9	2016-08-29 18:00:18.439399-05
uwtlobbr4fvl1ag2bemnmekk60a68bss	ZDI5YzBlOTI1NjU0NDU0MmZlZDQ0Y2EwMTEyYTI3MmQ5MDIzYzM2Yjp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-24 18:42:33.626202-05
mkwzzmbbj1ze9wixb6jpgtjo64zardbi	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-08-30 16:34:17.062704-05
l5av3g3l9cwj8nwk76gk9ccwoeqs95b6	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:50:44.500941-05
vcarmf4br7hb8y48wlz9if1zybovs1v7	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-25 18:39:55.97987-05
t8z6hciq9s543bz4fagd4wmpoutnp6f4	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 20:01:37.274315-05
1ufmuae690t5zuyraf4w6t87uekzy47b	MDBjYmQ0MTU3NjNmMDZkY2I3NGMxMGExZTgxNTZlNzhiZGQ2YmI2OTp7ImZhY2Vib29rX3N0YXRlIjoiRmo5NG9KTmx5M2NoYlpTdlZyblFJaWpkZlJLWnVWSDcifQ==	2016-08-23 12:43:03.416885-05
1vbvwpxwn9juq94zofqk5k0wbkevur63	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 19:07:59.703039-05
8bkb51daf06vfgntm41fzj81squ257fk	NzBkNDRkZmQwZThkMzgxNTg0NTY4ZDkxMjdiNGQ2YzBlZGQ5NWEyNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjIxMjZkNzBlY2Q4NGI4MzdlMTJkODUwMDRhNWNmYTU3YTg0YTJiMTEiLCJfYXV0aF91c2VyX2lkIjoiMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-17 15:26:45.337013-05
436gy6wymsim4kp5x5py0twlqlx20pnc	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-22 19:19:45.142173-05
ak354aecsz4ry3s6j0zifamzvvz6rh2w	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 19:49:59.18873-05
rp5vg45iu24z8d2qngjommdgxrt2y015	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-22 11:18:31.396348-05
tgqz2xgxcyrrsxgp2tqsj8av5czqcv56	M2JkZWMxZWJiMDRkYTJiYzE0MDc0NWY0YjJkNzgxNTAwZTQxNTQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjVmNmQyNzRjZmVmNzMyYTQ3NjlhZjIwOTk3MGVjNjQwODczZTI4MDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMyJ9	2016-08-19 17:17:37.144736-05
e8mcz2tl7qafw68oykti2i0fchlcko8p	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-26 13:02:32.566872-05
74vg5gvq5ud1bidt9n01c2kdkqva90bd	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-24 15:08:10.836448-05
hme61ycis1ksrln4cy0c57cpvetwqnxh	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-26 15:26:53.816808-05
wmxcsh723efebaeqypdkaf2cg0etkdpq	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-31 14:02:47.368778-05
fo1voj4aa89svlcxp4skqxvzvv2tl29p	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-29 15:21:52.507933-05
8kudyoepv3jkglgy4xyvy5yrzjhzpw9j	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-29 15:30:14.128271-05
hluktmvv1b8dartccu21sid2v2xjowxg	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-08-30 10:49:05.336257-05
nvkga0hzvpipwm3elulczq26l726oxb8	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-01 16:35:30.10789-05
rernz898vpvxx652lynug6wlxjjhm1ji	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 10:23:18.50895-05
rru2d0zfpsbrikx7ec3rg65ntgtoa01y	ZDA1MzA1NDA1YjY2NzZlZGRmYjQxZWQwMTA3ZWY4MTQ5MjFjN2FiZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-24 18:42:27.378344-05
um7x6pypvsl0lgejpa0h3ev8iwvum5lb	NDAyNTU5YzAwYjJkMTI3OGE1NDJjM2EwYThmNGExOGJkYjBhYTYzMjp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkMThiN2U5ZDFiNzc5ZDQyZjJlY2JhYWZmMGY2ODEyZTdlNWNhYTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-09-07 17:17:53.025457-05
mv6p91z2dq8cgf8r97i0tlgsp3kg02x8	NDAyNTU5YzAwYjJkMTI3OGE1NDJjM2EwYThmNGExOGJkYjBhYTYzMjp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkMThiN2U5ZDFiNzc5ZDQyZjJlY2JhYWZmMGY2ODEyZTdlNWNhYTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-08-29 18:10:08.731317-05
rvtybcbo6xexkd4mnjwh09ut9hteo72d	ZGMyNTM1OWMwOWQwMWFiMTdmOTA0NjkzNDc3M2UwNDYwYWI1MTRlMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMSJ9	2016-08-30 15:21:04.912492-05
emmf1l8cwo2dwk11qfdrxuve71bqbvr7	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-17 11:43:49.982016-05
7wv2cqf7egvxgqm3kps3cp6xy92q0unf	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:51:56.248083-05
crl9q5frwybe28s2ogu4y0q0iz56wwdu	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:52:58.365229-05
d6vth2jtrsdzui90s7uhzh6cnpwtnp77	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-09-02 16:54:47.205247-05
8q06zc9kxtloqn6acibaq0jk74rt130m	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 18:58:05.191455-05
b90hjqjxddieetw0v3b7c7frf9wqds09	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-25 19:47:40.576281-05
hb45sah2c1nihi7j9x1mb2rm4xyxia3e	YzUzOTU4MWRmYWNiNzVkYTg4MWEzN2E0OTY1YmJkMjVkYmZlNTEyZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6Ijk5IiwiX2F1dGhfdXNlcl9oYXNoIjoiMzA1NWE4OWI2MGY2OTkwNzY1NGM4YmIwNjQyMjFkYWZmMzBhYWY1OSJ9	2016-09-01 16:24:05.076932-05
8ogvb1dtzsbcpuvfuy9rxpdud9iljpgz	YzMzMzMzNzkxYjg4MTczOGI4Mjc0MWQyZWQyYTc1NzQzYTQ4YzE1YTp7Il9hdXRoX3VzZXJfaGFzaCI6IjMwNTVhODliNjBmNjk5MDc2NTRjOGJiMDY0MjIxZGFmZjMwYWFmNTkiLCJfYXV0aF91c2VyX2lkIjoiOTkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-01 11:22:42.299366-05
y7jbgns389c0ie22slraice0rliamrvo	ODgxYTNmM2Q1ZDkwNDEwZDY1ODJiMWMwYzA5OTM2M2ExNmEzZWNkNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2016-08-23 14:26:59.799133-05
ds4i3wdenys5gn56lw2kzeqtzofvigei	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:18:28.363795-05
0m6nc0qhptb6j5vhrbc9q5naic7u2gch	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-24 15:58:07.003286-05
hj9hti1sb8jkq5h14o17yd4d293is2y3	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-19 18:07:50.929245-05
ta6r1qmwegx3s906bt2bal3uq9ugfz29	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-31 19:04:11.223298-05
m4vb03ow8glvot1zcqinlo3nrw6wc00s	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-24 10:18:24.991075-05
xu0tksigrtuq74b2t0g8xpkehkcb7w7r	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 13:22:02.29852-05
zmkzlym30cub8l8njs6i7a4uktj7ntbh	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-24 18:37:29.884472-05
uqn22cz9r6sze11e7bt2nepi42b220rv	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-26 13:42:06.446134-05
tslbu2m283oro2sdi3870z45c9qwuuuy	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-26 19:28:00.37671-05
76hgm5s7ybw10e2r1w71iiebb0rzexic	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-29 15:49:38.586669-05
b6vh3msv729e7tlvump8lzakitozqghz	ZWM4OWU1ZDUzYjUyODU1OGY4M2I3ZDRjMWRiMzMxMjFlYjQxNzUzMDp7Il9hdXRoX3VzZXJfaWQiOiI1OSIsIl9hdXRoX3VzZXJfaGFzaCI6ImY5NDhkZmUxMzU0YmE2ZDIyMGJhODU3MzlmNGIwOWVkZDc4OTI4NDQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-25 17:43:50.006002-05
2vo802vmw7tkuy4hfm7gwqismwf0bdg2	ZWMyZWIwZTJiYWFlMDczYzRhODhlMWU0Y2M2YzI2MGE5YTljMzUxNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-30 15:42:29.19772-05
vyxcvf3sjz21nueum64st3zxcx5z22uh	MjVjNzBiYTk1N2RmNDJlYjlkNWM1NGYzNzFjZThjYjZjMjVkNzk1MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzFkNWI4MThhMWU3NDViNmIwMzNjMGY2Y2JmZTFiNWU2ZWE4N2JjNyIsIl9hdXRoX3VzZXJfaWQiOiI3NCJ9	2016-08-29 18:04:26.903408-05
e1i4x9gvkndzpop6jodu1g01fcbcuovc	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-29 18:11:32.252582-05
0zdrkqgj05xxcbocv7367ix06fjai3z4	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-07 12:57:20.982313-05
tfp2wqjkmuhuln4zo1wo3nmuo8d3tojd	YzkwMTY2MDI5NGEyMWYwY2VkMDgyZjIwMjlkYTFiNzIxNTgwMmNiZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2016-09-07 18:44:49.552419-05
x6kpf9cmm064thuqtf69734onzmbnoml	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 12:55:06.536238-05
i1frojthsbn6nd6k09kmlxkuqvknvxkc	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 12:57:42.72026-05
vni6dwgyfdbdaruapyrxer5m2c9e0vll	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-05 13:12:18.328213-05
87vwurssemwupot7woem4v7ivs4hutvd	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-09-02 17:49:44.697129-05
68dgmprcxa4ffty30ihp9xrndz8cmvhb	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-07 13:29:24.000863-05
bgbcbfhim4cei09ljf1an8fj4kxzx0y6	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 13:41:30.259757-05
lq6ry04p4gqr8j7mp5374y97sligf6bg	MWE3YmE3YjlhNWMwOTVhMGJiM2JjYTkwODQ1ODUzMDM1MTQ2MTcyNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjcxZDViODE4YTFlNzQ1YjZiMDMzYzBmNmNiZmUxYjVlNmVhODdiYzciLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3NCJ9	2016-09-02 18:07:45.27335-05
oy98ifgwr52l3adzgd8n9a67q9tqhhd7	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 13:54:46.658079-05
mioen27mehovjx2kzutj3jqv6i1au2ua	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-05 15:53:24.021566-05
gckv2g24pymdbwe92vz5ei3pir172spz	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-02 14:11:55.395143-05
gxf244rgj5ixujkk47w6imxdwcqjjz2f	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-02 16:33:09.605827-05
bzhbwoseqwiv0blcj471yr77cib2pzzc	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-08 11:20:39.114452-05
n5q6hlhnqxb5uq8buvabrn0jvmz7eav0	NmExMWY4OGYwNjQ2NzU4NDUzZjcwOWEzYTY1NmMwYTVhOWFkY2JjMDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-05 11:25:24.5563-05
tlzsr15pkba01ekw447upetlt1gx9qnw	YzA3Zjc5YzMzNzk1NjYxOGMyMmFkNTI5MDI0NjYxNGIwYjE0OWMxMjp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg2MDY0ZjQ4MTMzODZlYjRhMTFjY2Y1NTE2MzMwZjM4OTVkOTcwZWEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMDIifQ==	2016-09-05 12:21:26.095295-05
yxbs4u8js33rglc1kusfd6sqcqe5i9py	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-07 15:34:49.573163-05
mjksiv27n0rj9r3lhwqumf9n2b4v4vma	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-06 16:53:16.589805-05
q33f2nrkj4mxbj88tfdblqkhba397dwi	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-07 15:58:21.48045-05
289ik58bkl4vm9cycb2g2ct75s7qavob	ZmM5OWI1Y2Y5NDI0NjVjZjBkYmYxODQ2ZmZmNGUzNzZjMTVjODc2NDp7Il9hdXRoX3VzZXJfaGFzaCI6IjYxOTcyNmVjNDQwYWU4ZGQwZWE2OGRlMjAyNTgzZWU3NzAzNjUwNGUiLCJfYXV0aF91c2VyX2lkIjoiMTA5IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==	2016-09-07 11:49:08.190338-05
tkln0aik7jd5oirz8jxbn1lyjlc62a1i	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-07 13:10:42.828149-05
ni44kc3akjiqoabhyljla3ojw74glm96	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-06 15:04:03.141762-05
b5z11sa8h5td1zgspridy7l0udjopup3	NTQ4NjdlOTY1MzlkZDI1MTYyZWVkNzk2ODM5OTZjMzJlNjgxZjQ5Yjp7Il9hdXRoX3VzZXJfaWQiOiIxMTMiLCJfYXV0aF91c2VyX2hhc2giOiIwYzAxM2RmYmZkYWY2ZTUzYTUzOWMwNzRlNTYyOTIxYTYwZWNiNmU0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==	2016-09-08 09:55:14.954974-05
h1hyyh110qll6no9qo1bwpei8jnm136z	NGM0MDQwOTM1YmVjMDFjYmFlMzMyYmIyZjZlYjI5MjM5NGQyNWQyZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkMThiN2U5ZDFiNzc5ZDQyZjJlY2JhYWZmMGY2ODEyZTdlNWNhYTgiLCJfYXV0aF91c2VyX2lkIjoiMyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-05 15:56:48.417956-05
tu8j09e86vukb668pezxq5rwjg46fx0e	ZjUyNWYyYjdkNmYyODFjZDg4NmNlYTY3OTM0OTM3ZGQ1ODJkNjFkODp7Il9hdXRoX3VzZXJfaWQiOiI5OSIsIl9hdXRoX3VzZXJfaGFzaCI6IjMwNTVhODliNjBmNjk5MDc2NTRjOGJiMDY0MjIxZGFmZjMwYWFmNTkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-01 17:35:33.154423-05
v37gnu24jqgy9c4jxyeh24fidgtygtxj	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-09-02 10:54:07.654082-05
f90e3hyt0e3r1s33uuo6sbxfp30n4gf3	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-07 14:24:30.819179-05
itdheey5lyosmf0sx3b9f6v7zsxgoskm	ODgxYTNmM2Q1ZDkwNDEwZDY1ODJiMWMwYzA5OTM2M2ExNmEzZWNkNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2016-09-05 16:11:48.280019-05
repffs4arsf74hr3sln42fdnhb1mc5wz	NmExMWY4OGYwNjQ2NzU4NDUzZjcwOWEzYTY1NmMwYTVhOWFkY2JjMDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-02 17:56:18.487909-05
s5ak7255pah0b414ydw5kagmeq95s8ah	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-02 18:56:02.514593-05
fp7uxbvokmf8qi6as1zsivprlm0ubkjn	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-07 15:59:13.389845-05
ta5qzd4my4rpcrl6ao2maiw86vz6p2j1	ZTM4MDRiMThiMmI0ZDEyNWMxYzg2NGFkMDkxZTRmOTE3Nzg3N2MzODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZGZmZjVhMTEyMDdjNjEwMTZhM2ZkYjkyYTgzODU4MjU1NGQwZjI3YSIsIl9hdXRoX3VzZXJfaWQiOiI3NiJ9	2016-09-07 16:25:11.879198-05
yz82ro9283tzmdkryubls2jhnyeb47ta	NGM2YWU2ZjhjMjQwY2FlMzk0YjQzMmZhYzkxYWE0MDg4YzhmZGVmMzp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3ZDE4YjdlOWQxYjc3OWQ0MmYyZWNiYWFmZjBmNjgxMmU3ZTVjYWE4In0=	2016-09-06 10:15:34.977523-05
ej8fg8xah0lul9wf18ps605g359mltv0	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-02 13:57:10.880321-05
fcwjmp8keg0p3bahwzk18llvi8y12zfp	NGM0MDQwOTM1YmVjMDFjYmFlMzMyYmIyZjZlYjI5MjM5NGQyNWQyZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkMThiN2U5ZDFiNzc5ZDQyZjJlY2JhYWZmMGY2ODEyZTdlNWNhYTgiLCJfYXV0aF91c2VyX2lkIjoiMyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-06 16:34:31.538127-05
4627ndiamgtuv9nqtday5ch54h27b8tl	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-06 13:03:53.145226-05
8oa3clkzzygfcpscl1c6vtporreupk5g	Y2ZkZWQ4OTRlZDQwNTRhZjI5MDViMDE2NDFmYWJmNjM3MjJmYjNmZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-09-07 17:23:23.292582-05
yo3zqwllfw1114acpfctocxrxn7xz0w5	NmExMWY4OGYwNjQ2NzU4NDUzZjcwOWEzYTY1NmMwYTVhOWFkY2JjMDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-07 12:43:17.760333-05
o1r5oi1rbrvmgwa74j6lq9ux5mq1fohm	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 10:27:07.656405-05
jt784f4ktcy31ypai7miamggvn3n12xm	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-31 17:34:07.559034-05
2eaei4f4mw7n97ohfiiaqpjbect47btd	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-09-06 13:22:16.560812-05
dja9hfdaz1kwyq8gj34lif94nloue937	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-07 13:15:09.639435-05
qc6i4z83o2hgsw837e48e980ka883oeu	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-31 18:06:37.001402-05
kfcfifq3qjlzot6ji4200vm5fj33q91z	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-31 18:11:16.341093-05
6svrjocl8xnwlw1ezn130smiox0o6bk8	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-09-05 15:52:04.148959-05
9w9saqzrrwhdee9usp6c4icfhxecsdjq	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-01 17:35:51.79785-05
lia1qdp5yl112tv3njyxssep2a59sunv	NmExMWY4OGYwNjQ2NzU4NDUzZjcwOWEzYTY1NmMwYTVhOWFkY2JjMDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-02 17:53:04.635089-05
nwdo7x15kl0ilu7oarrzirxev5kjwem5	ZGUwZTk0ZGM4NDEyODIxMDVkNzI4MmU3MGQyYWIwYWM3OWYxMWZiNDp7Il9hdXRoX3VzZXJfaWQiOiIxMTQiLCJfYXV0aF91c2VyX2hhc2giOiJkZmI2MzdjMmRmNTkwNjgxNjdlZTFmOTY5ZjA3ZmFlMzM2N2YxNjNhIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==	2016-09-08 09:56:09.092732-05
ybtc5rvzzsss520iajsnoqvvsgi3l2xr	YTE3YTlkZGNhYWEyODVkY2Q2YWFiOTFiMzhhMjBjZGRhNWMyYzRmNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImRmZmY1YTExMjA3YzYxMDE2YTNmZGI5MmE4Mzg1ODI1NTRkMGYyN2EiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3NiJ9	2016-09-07 15:28:16.60513-05
53ppy6gusohi2i7sx9sqodsxojqnc827	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-09-06 16:19:01.599785-05
3yslofcdizucfmmvybmkblj5os2st68i	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 13:43:36.173033-05
cd0fz2w4xn7wrz0sh1vaswmzgqo2hdyi	ODgxYTNmM2Q1ZDkwNDEwZDY1ODJiMWMwYzA5OTM2M2ExNmEzZWNkNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2016-09-01 19:19:28.273249-05
mfivzgb2bv31q4rgadb37fr8bk2r85yn	OTM0YjY4NDA0M2MyODY3NDA1Y2ZhMjkyZjE2YWEwYmQxMDdmYmI3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-09-02 08:15:23.086715-05
1l6b13j7fvx88ig3tx0xas48jxfedn8x	NGJhYWI2NmViNzk3YmNhMzljN2ExZjVmZDkxNjkxMjc5Y2IwNmZjOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-09-02 13:48:55.78651-05
ilh7pswyjueaiv1z6685giy1vu8mpqrl	NDAyNTU5YzAwYjJkMTI3OGE1NDJjM2EwYThmNGExOGJkYjBhYTYzMjp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkMThiN2U5ZDFiNzc5ZDQyZjJlY2JhYWZmMGY2ODEyZTdlNWNhYTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-09-05 11:03:36.491363-05
s6ldfi7wd2l8mnbd635ixo2j88u0hsfd	NmExMWY4OGYwNjQ2NzU4NDUzZjcwOWEzYTY1NmMwYTVhOWFkY2JjMDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-05 11:14:07.498856-05
j40bmcfctew3f0ypoamu4mfdju4dv81i	ZDQ2NGY4NWZlZjE0NGZjOGE1Y2Y2NDFhYTNlNzFjZDhkNGVkMzExYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjMiLCJfYXV0aF91c2VyX2hhc2giOiI3ZDE4YjdlOWQxYjc3OWQ0MmYyZWNiYWFmZjBmNjgxMmU3ZTVjYWE4In0=	2016-09-06 16:43:54.010997-05
4edy7pkhbau4qc2no6u0umj0ostc7rj8	MWE3ZDkwM2VkZTI4ZDVlZTg5NTE0OWI5NWVmNTZiNDkxN2E0MzNmYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2lkIjoiMTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-02 16:30:46.998224-05
sjxjynivma3iicfb14evq2e5j2l9ai6p	NmExMWY4OGYwNjQ2NzU4NDUzZjcwOWEzYTY1NmMwYTVhOWFkY2JjMDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-05 12:24:10.409005-05
uyj5q1h7fp8tejene8wb9yndpcfka90j	NmExMWY4OGYwNjQ2NzU4NDUzZjcwOWEzYTY1NmMwYTVhOWFkY2JjMDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-09-07 12:43:35.07101-05
b43mes74yg4do9nxc5sywrbyfftm2t7q	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-09-06 17:24:35.411109-05
\.


--
-- Data for Name: social_auth_association; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_association (id, server_url, handle, secret, issued, lifetime, assoc_type) FROM stdin;
\.


--
-- Name: social_auth_association_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_association_id_seq', 1, false);


--
-- Data for Name: social_auth_code; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_code (id, email, code, verified) FROM stdin;
\.


--
-- Name: social_auth_code_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_code_id_seq', 1, false);


--
-- Data for Name: social_auth_nonce; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_nonce (id, server_url, "timestamp", salt) FROM stdin;
\.


--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_nonce_id_seq', 1, false);


--
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_usersocialauth (id, provider, uid, extra_data, user_id) FROM stdin;
1	facebook	1217705011612873	{"id": "1217705011612873", "expires": null, "access_token": "EAAYjaA6FgYMBACfChQXDIkG01FA0ZBDu4K86ZB6DJJnTr9RpCsObOOZAj8whMqLMq1KLRzbgMbWF2hW8aANvMiTEfzlt81pfRkftqvnNNvGdpOPniQdgyi6ROSnv8T9XbZAME4eGr5wGQCilhX778opKtUtcZArkZD"}	95
\.


--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_usersocialauth_id_seq', 1, true);


--
-- Data for Name: spaapp_actividadcomercial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_actividadcomercial (id_actividad, tipo_actividad, detalles) FROM stdin;
1	Restaurante	aadadasa
\.


--
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_actividadcomercial_id_actividad_seq', 1, true);


--
-- Data for Name: spaapp_catalogoestado; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_catalogoestado (id_estado, estado) FROM stdin;
1	En Espera
2	En Proceso
3	Transportista en Espera
4	Enviado
5	Entregado
6	Problema de Proveedor
7	Problema en Entrega
8	Queja de Cliente
\.


--
-- Name: spaapp_catalogoestado_id_estado_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_catalogoestado_id_estado_seq', 8, true);


--
-- Data for Name: spaapp_categoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_categoria (id_categoria, categoria, detalles, ruta_foto) FROM stdin;
7	Pollos	Pollos a la braza	categorias/pollos.jpg
1	Pizza	dadadasdq	categorias/pizza-banner.jpg
2	Comida Asiática	dadadasd	categorias/banner_china.jpg
4	Comida Casera	dasasd	categorias/cocina_casera.jpg
5	Comida rápida	dasdasd	categorias/rapida.jpg
3	Heladería	sadadsa	categorias/helado.jpg
8	Bebidas	hjkb	categorias/bebidas.jpeg
\.


--
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_categoria_id_categoria_seq', 8, true);


--
-- Data for Name: spaapp_ciudad; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_ciudad (id_ciudad, nombre, descripcion) FROM stdin;
1	Loja	dasda
2	Loja	asdasdas
\.


--
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ciudad_id_ciudad_seq', 2, true);


--
-- Data for Name: spaapp_cliente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_cliente (id_cliente, fechainicio_suscripcion, fecha_pago, detalles_adicionales, pedidos_activos, id_perfil_id) FROM stdin;
1	2016-07-29	2016-07-29	nada	t	3
\.


--
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_cliente_id_cliente_seq', 1, false);


--
-- Data for Name: spaapp_contrato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_contrato (id_contrato, codigo, detalles, vigencia, fecha_inicio, "fechaFin", valor, ruta_contrato, id_proveedor_id, id_sucursal_id) FROM stdin;
\.


--
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_contrato_id_contrato_seq', 1, false);


--
-- Data for Name: spaapp_coordenadas; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_coordenadas (id_coordenadas, latitud, longitud, fecha, estado, id_perfil_id) FROM stdin;
\.


--
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_coordenadas_id_coordenadas_seq', 1, false);


--
-- Data for Name: spaapp_detallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_detallepedido (id_detalle, valor, cantidad_solicitada, id_pedido_id, id_plato_id, tamanio_id) FROM stdin;
218	15.6	1	153	30	198
219	18.300000000000001	1	154	30	199
220	15.6	1	155	30	198
221	15.35	1	156	26	130
222	20	2	156	25	118
223	36.600000000000001	2	157	30	199
224	307	20	158	26	130
225	241.27999999999997	13	158	26	131
226	30	3	158	4	219
227	18.300000000000001	1	159	30	199
228	18.300000000000001	1	160	30	199
229	15.6	1	161	30	198
230	216	27	162	23	252
231	9850	985	162	4	219
232	3.2000000000000002	1	162	55	260
233	10	1	162	21	254
234	14	1	162	21	253
235	3.5	1	162	3	200
236	16.5	3	162	3	201
237	75	5	162	25	117
238	10	1	162	25	118
239	46.049999999999997	3	162	26	130
240	74.239999999999995	4	162	26	131
241	73.200000000000003	4	162	30	199
242	46.799999999999997	3	162	30	198
243	15.6	1	163	30	198
244	15	1	164	20	30
245	171.59999999999999	11	165	30	198
246	10	1	165	4	219
247	15.35	1	166	26	130
248	15.6	1	167	30	198
249	234	15	168	30	198
250	270	18	168	25	117
251	252	18	168	21	253
252	24	3	168	23	252
253	3.5	1	169	3	200
254	18.559999999999999	1	170	26	131
255	15.6	1	171	30	198
256	15.6	1	172	30	198
257	20	2	173	25	118
258	6.2999999999999998	1	174	56	266
259	15.6	1	175	30	198
260	15.6	1	176	30	198
261	3.5	1	177	3	200
262	8	1	178	23	252
263	15.6	1	179	30	198
264	3.2000000000000002	1	180	55	260
265	15.6	1	181	30	198
266	15.6	1	182	30	198
267	15.6	1	183	30	198
268	15.6	1	184	30	198
269	15.6	1	185	30	198
270	15.6	1	186	30	198
271	15	3	187	55	261
272	6.4000000000000004	2	187	55	260
273	36.600000000000001	2	187	30	199
275	15.6	1	189	30	198
276	15.6	1	190	30	198
114	3.5	1	79	3	200
\.


--
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_detallepedido_id_detalle_seq', 276, true);


--
-- Data for Name: spaapp_direccion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) FROM stdin;
140	11	11	11	-3.9865895000000005	-79.19686449999999	11	36
142	33	22	22	-3.9866092	-79.19685869999999	33	36
3	Vince	Ambato		-3.9870951803235783	-79.20502334833145	coca-cola	4
4	Latacunga	Bolivar		-3.9870951803235783	-79.20502334833145	coca-cola	6
144	44	44	44	-3.9865884999999994	-79.19686829999999	44	36
146	66	55	55	-3.9865925000000004	-79.1968655	66	36
148	88	88	88	-3.9865887	-79.1968778	88	36
8	we	w	sdf	-4.02147042659097	-79.21773362438964	we	36
150	cheo	cheo	N/A	-3.9865909999999998	-79.19684509999999	cheo	133
152	sqParisisisimo	paari	78-89	-3.9865783	-79.1968447	e	137
154	Venecia	Bucarest	12-98	-3.9865944	-79.1968385	Esq	138
156	Loxa	Loji	12-89	-3.9865827	-79.1968457	esq	142
158	sisi	sisi	12-36	-3.9865861999999996	-79.19685729999999	sisi	144
162	Paris	Paris	12-89	-3.986595	-79.196913	En Prendho	145
17	colombia2	filipinas2	15-36	-3.9747766385787027	-79.22028708737186	frente a casa	67
74	fdsfd	fsdf	14	-4.017973134894606	-79.21208280720565	dfd	105
2	Sucre	Miguel Riofrío	12-34	-3.9978661165384266	-79.20301992157755	Frente a la Inmaculada	5
141	22	22	22	-3.9865931999999997	-79.1968873	22	36
143	44	44	44	-3.9865687999999997	-79.1968737	44	36
145	55	55	55	-3.9865925000000004	-79.1968658	55	36
147	77	77	77	-3.9865904	-79.196877	77	36
149	Monte	CArlo	N/A	-3.9865921	-79.1968622	Bosque	133
151	Praga	Doble	12-65	-3.9865884	-79.1968437	Esq	137
153	lojita	dos	N/A	-3.9865972999999997	-79.19684389999999	lojita	137
155	5555	55	55	-3.986592	-79.1968692	555	105
91	PAris	Penci	12-98	-3.9866216	-79.1968516	Esq	128
92	sda	da	11111	-3.9866075999999993	-79.1968531	da	114
1	Av. Universitaria	Mercadillo	65-77	-3.9929586822882115	-79.20648041646669	Frente al León	2
96	LOP	LOP	12-56	-3.9866170999999997	-79.1968871	12-56	114
97	lolo		12-58	-3.9866170999999997	-79.1968871	Esq	114
157	loxi	loxi	12-98	-3.9865827	-79.19684029999999	loxi	36
95	24 de Mayo	Miguel Riofrío	N/A	-4.002087429754183	-79.19728585767066	Frente a escuela	130
159	:D	:D	12-98	-3.9865831999999997	-79.1968536	:D	144
161	calle	calle2	12-98	-3.9865797999999995	-79.1968553	dos	144
0	Para recoger	''	''	''	''	''	67
99	rreAgui	rorar	15-89	-3.9865812	-79.1968605	ora	133
101	Primcipal22	Guaman	1	-3.9865871999999998	-79.1968435	sq	133
106	mi direccion	mi dir	12-89	-3.9865941	-79.19686159999999	queti	133
120	mi	paraiso	666	-3.9866165	-79.1968574	.com	133
121	qNuevaLoja	Holi	12-78	-3.9865895000000005	-79.19684769999999	es	133
127	paris	paris	12-25	-3.9865925000000004	-79.1968488	paris	133
128	loxa	lox	1266	-3.9865925000000004	-79.1968488	loxa	133
129	yyyyyyyyyyyyyyyyyyy	qqqqqqqqqqqqqqqqqqqqqq	78	-3.9865925000000004	-79.1968488	46454653	133
98	Av. 18 de Noviembre	Mercadillo	12-89	-4.001625972451649	-79.20358799875574	Esquina	134
\.


--
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_direccion_id_direccion_seq', 163, true);


--
-- Data for Name: spaapp_horariotransportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_horariotransportista (id_horario, dias, horas, id_transportista_id) FROM stdin;
\.


--
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_horariotransportista_id_horario_seq', 1, false);


--
-- Data for Name: spaapp_ingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_ingrediente (id_ingrediente, nombre, id_proveedor_id) FROM stdin;
3	Pollo	1
5	Carne de res	2
6	Arroz	2
7	Pescado	2
8	Cebolla	2
9	pimiento	2
10	zanahoria	2
15	tomate	2
16	culantro	2
17	helado	8
18	tallarin	3
19	chocolate	8
20	harina	6
21	aceitunas	8
22	palmito	3
23	albondigas	2
24	Dos copas	7
25	cereza	7
26	oregano	5
27	veganito	5
28	carne de chancho	10
30	queso	1
29	chuleta	2
31	camaron	10
32	tallarin	10
33	verduras	10
34	arroz	10
35	queso	11
36	chorizo	11
37	CHAMPIÑONES	11
38	jamon	11
39	piña	11
40	zanahoria	11
41	pizza	11
42	Pollo	2
43	zanahoria	1
44	tomate	1
45	Arroz	1
46	Carne de res	1
47	chuleta	1
48	piña	1
49	tallarin	1
50	palmito	1
51	culantro	1
52	bizcochuelo	4
53	leche condensada	4
54	leche evaporada	4
55	leche entera	4
56	crema	4
57	helado	4
58	pan	1
59	platanito	1
60	maracuya	12
61	soya	12
62	piña	12
63	naranjilla	12
\.


--
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ingrediente_id_ingrediente_seq', 63, true);


--
-- Data for Name: spaapp_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_menu (id_menu, id_proveedor_id) FROM stdin;
1	1
2	2
3	3
4	4
5	5
6	7
7	6
8	8
9	8
10	9
11	9
12	10
13	11
14	12
15	13
\.


--
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_menu_id_menu_seq', 15, true);


--
-- Data for Name: spaapp_modificardetallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_modificardetallepedido (id_modificardetallepedido, id_ingrediente_quitar_id, id_detallepedido_id) FROM stdin;
\.


--
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_modificardetallepedido_id_modificardetallepedido_seq', 1, false);


--
-- Data for Name: spaapp_pedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_pedido (id_pedido, fecha, observaciones, valor, tiempo, id_direccion_id, id_sucursal_id, id_tipo_pago_id, id_tipo_pedido_id, id_trasportista_id, usuario_id, id_estado_id, notificado) FROM stdin;
153	2016-08-24 10:21:57-05	Sin Comentario	16.850000000000001	\N	8	1	1	1	1	15	1	f
183	2016-08-24 12:44:50-05	Sin Comentario	16.800000000000001	45	158	1	1	1	1	113	3	f
157	2016-08-24 10:36:40-05	Sin Comentario	37.850000000000001	\N	0	1	1	1	1	76	1	f
156	2016-08-24 10:33:11-05	Sin Comentario	36.600000000000001	3	8	1	1	1	1	15	5	t
158	2016-08-24 11:44:10-05	lalallalallalalala	579.48000000000002	\N	157	1	1	1	1	15	1	f
159	2016-08-24 11:45:00-05	Sin Comentario	19.5	\N	8	1	1	1	1	15	1	f
154	2016-08-24 10:22:21-05	Sin Comentario	19.550000000000001	6	8	1	1	1	1	15	3	f
164	2016-08-24 12:14:23-05	Sin Comentario	16	\N	158	2	1	1	1	113	1	f
179	2016-08-24 12:40:49-05	Sin Comentario	16.800000000000001	16	158	1	1	1	1	113	3	f
180	2016-08-24 12:41:27-05	Sin Comentario	4.4000000000000004	\N	158	1	1	1	1	113	2	f
178	2016-08-24 12:39:39-05	Sin Comentario	9.1999999999999993	8	158	1	1	1	1	113	5	t
176	2016-08-24 12:37:35-05	Sin Comentario	16.800000000000001	7	158	1	1	1	1	113	5	t
177	2016-08-24 12:39:16-05	Sin Comentario	4.7000000000000002	12	158	1	1	1	1	113	5	t
163	2016-08-24 11:55:00-05	&lt;script&gt;alert('hola pame')&lt;/script&gt;	16.800000000000001	1	158	1	1	1	1	113	5	t
162	2016-08-24 11:51:25-05	Muy grande mi pedidooooooooooooo!!!1	10439.690000000001	10	158	1	1	1	1	113	5	t
161	2016-08-24 11:47:00-05	Sin Comentario	16.800000000000001	1	158	1	1	1	1	113	5	t
79	2016-08-23 06:02:52-05	lololol	4.75	6	129	1	1	1	1	102	5	f
155	2016-08-24 10:32:47-05	Sin Comentario	16.850000000000001	43	8	1	1	1	1	15	5	t
175	2016-08-24 12:37:16-05	Sin Comentario	16.800000000000001	11	158	1	1	1	1	113	5	t
182	2016-08-24 12:44:09-05	Sin Comentario	16.800000000000001	38	158	1	1	1	1	113	3	f
181	2016-08-24 12:42:44-05	Sin Comentario	16.800000000000001	38	158	1	1	1	1	113	5	t
171	2016-08-24 12:31:11-05	Sin Comentario	16.800000000000001	\N	158	1	1	1	1	113	2	f
160	2016-08-24 11:45:49-05	op	19.5	5	8	1	1	1	1	15	2	f
184	2016-08-24 12:46:06-05	Sin Comentario	16.800000000000001	4	158	1	1	1	1	113	4	t
186	2016-08-24 12:47:14-05	Sin Comentario	16.800000000000001	\N	158	1	1	1	1	113	2	f
167	2016-08-24 12:22:05-05	as	16.800000000000001	6	158	1	1	1	1	113	5	t
166	2016-08-24 12:21:44-05	sa	16.550000000000001	8	158	1	1	1	1	113	5	t
165	2016-08-24 12:21:23-05	sa	182.80000000000001	1	158	1	1	1	1	113	5	t
185	2016-08-24 12:46:47-05	Sin Comentario	16.800000000000001	18	158	1	1	1	1	113	3	f
187	2016-08-24 14:17:15-05	Caliente por favor	59.200000000000003	\N	161	1	1	1	1	113	1	f
170	2016-08-24 12:29:23-05	Sin Comentario	19.760000000000002	6	158	1	1	1	1	113	4	t
169	2016-08-24 12:29:01-05	Sin Comentario	4.7000000000000002	7	158	1	1	1	1	113	4	t
168	2016-08-24 12:28:39-05	GRaciasssssssssss	781.20000000000005	8	159	1	1	1	1	113	4	t
188	2016-08-25 04:58:49-05	Sin Comentario	3.2000000000000002	\N	142	12	1	1	1	15	1	f
189	2016-08-25 05:59:55-05	Sin Comentario	16.800000000000001	\N	8	1	1	1	1	15	1	f
190	2016-08-25 06:18:13-05	Sin Comentario	16.800000000000001	39	8	1	1	1	1	15	3	f
172	2016-08-24 12:35:07-05	Sin Comentario	16.800000000000001	\N	158	1	1	1	1	113	2	f
174	2016-08-24 12:35:49-05	Sin Comentario	7.5	6	158	1	1	1	1	113	3	f
173	2016-08-24 12:35:30-05	Sin Comentario	21.199999999999999	7	158	1	1	1	1	113	3	f
\.


--
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_pedido_id_pedido_seq', 190, true);


--
-- Data for Name: spaapp_perfil; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) FROM stdin;
86		\N		\N	usuarioFoto/descarga.png	0	55
4		\N		\N	usuarioFoto/rest1_GTgybUv.png	14	6
6		\N		\N	usuarioFoto/rest10_Tb8n5Vo.jpg	14	8
7	123123312	2016-07-28	1312312	Femenino	usuarioFoto/papalogo_T69w2cx_chpNt8u.jpg	sdasdsa	9
8	1212121	\N		\N	usuarioFoto/rest12_pgD61rK.png	12	10
1	2222222222	2016-07-26	0984532	Masculino	noname	22222	3
52	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	23
53	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	24
126	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	95
56	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	27
45	1102984765	2016-07-29	09876543	Masculino	usuarioFoto/descarga.png	30000	3
57	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	28
94		\N		\N	usuarioFoto/Pollo-Campero.jpg	0	63
58	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	29
59	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	30
82	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	51
62	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	32
10	1102323232	2016-05-20	254494	Femenino	usuarioFoto/userDefault.jpg	12	2
46	1109483737	2016-08-26	25467877	Masculino	usuarioFoto/userDefault.jpg	7	3
48	1104322333	2000-10-10	2584179	Masculino	usuarioFoto/userDefault.jpg	2584179	3
66	1111111111	2016-08-07	12	Masculino	noname	12	3
63	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	33
44	1104857563	2016-07-28	12345	Masculino	usuarioFoto/userDefault.jpg	5554321	3
133	1102578996	1994-09-06	9999999999	Femenino	usuarioFoto/perry.png		102
64	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	34
107		\N		\N	usuarioFoto/userDefault.jpg		76
49	1212121	2016-08-08	1526464643	Masculino	usuarioFoto/userDefault.jpg	7	11
34	55555	2016-01-01	123456	\N	usuarioFoto/userDefault.jpg	aaaaa	13
35	1123456789	2016-01-01	123456	aaaa	usuarioFoto/userDefault.jpg	aaaaa	14
37	1123456789	2016-01-01	555555	genero	usuarioFoto/userDefault.jpg	aaaaa	16
39	11111	1990-05-28	13123131	femenino	usuarioFoto/userDefault.jpg		17
40	111111	2016-01-01	555555	\N	usuarioFoto/userDefault.jpg	aaaaa	18
47	1123456789	2016-01-01	555555	genero	usuarioFoto/userDefault.jpg	aaaaa	19
51	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	22
65	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	35
67	1104666332	2015-03-20	2464	Masculino	usuarioFoto/userDefault.jpg	0	36
70	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	39
71	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	40
72	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	41
73	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	42
74	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	43
75	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	44
76	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	45
77	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	46
78	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	47
79	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	48
80	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	49
81	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	50
83	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	52
84	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	53
85	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	54
87	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	56
88	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	57
89	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	58
90	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	59
91	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	60
92	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	61
93	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	62
95	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	64
96	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	65
97	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	66
98	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	67
100	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	69
101	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	70
102	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	71
103	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	72
104	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	73
105	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	74
106	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	75
118	1111111111	\N		\N	usuarioFoto/userDefault.jpg	0	87
110	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	78
111	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	79
109	110466332	2016-07-12	2588897	Masculino	usuarioFoto/hobo_owl-wallpaper-1920x1080.jpg		77
112		\N		Masculino	usuarioFoto/avatar_V2CJK2U.png		80
99	1104666332	1995-03-20	0989906930	Femenino	usuarioFoto/DSCN3826_VNWKmmL.JPG		68
113	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	81
115	1105896478	1994-06-09	2589764	Masculino	usuarioFoto/little_dragon_black-wallpaper-1366x768.jpg		83
116	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	85
119	1111111111	2016-08-11		\N	usuarioFoto/userDefault.jpg	0	88
117	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	86
120	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	89
121	11	\N		\N	usuarioFoto/userDefault.jpg	0	90
122	44	\N		\N	usuarioFoto/userDefault.jpg	0	91
123	111	\N		\N	usuarioFoto/userDefault.jpg	0	92
124	2555555555	2016-08-10	258897	Femenino	usuarioFoto/travel_the_world-wallpaper-1366x768_8amqeqC.jpg		93
108	Lolito	2016-08-09	258897	Masculino	usuarioFoto/userDefault.jpg	45	3
125	110466332	1995-03-20	258897	Masculino	usuarioFoto/hobo_owl-wallpaper-1920x1080_omWGX9L.jpg		94
127	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	96
129	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	98
128	1104666332	2013-05-12	0989906930	Masculino	usuarioFoto/new_york_city_aerial_view-wallpaper-1366x768.jpg		97
130	1666666666	1990-05-10	9999999999	Masculino	usuarioFoto/chifabei.jpg	0	99
5	1122334455	2016-07-28	213131	Femenino	usuarioFoto/icecreamsocial_yj6ICi3.jpg	sdada	7
136	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	105
135	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	104
36	1123456783	2016-01-01	5555557	Masculino	usuarioFoto/fondolinux.png		15
137	1104632454	1994-05-12	2588897	Masculino	usuarioFoto/fondolinux_gJY8f2k.png		106
138	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	107
131	1104662356	1994-06-20	0984546464	Femenino	usuarioFoto/Captura_de_pantalla_de_2016-07-13_110847.png		100
132	1970	1961-08-16	1970	Otro	usuarioFoto/nodo-electritelecom.jpg		101
114	dsdasdasd	1995-03-20	4546464646	Femenino	usuarioFoto/Captura_de_pantalla_de_2016-07-19_175259_Ct4KyY5.png		82
3	1122334455	2016-03-16	1111111	Masculino	usuarioFoto/chinito_HYopzZ2.jpg	12312312	5
134	1111111111	2016-08-19	2579797	Femenino	usuarioFoto/pizzahut.png	0	103
2	2222222	2016-02-20	555	Masculino	usuarioFoto/phu.jpg	14	4
139	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	108
140	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	109
141	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	110
142		\N		\N	usuarioFoto/userDefault.jpg		111
143	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	112
144	1105468787	1995-02-01	25879797	Masculino	usuarioFoto/vegas.jpg		113
145		\N		\N	usuarioFoto/Logo_Soyard_-_pantone.jpg	0	114
146	\N	\N	\N	\N	usuarioFoto/userDefault.jpg	0	115
\.


--
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_perfil_id_perfil_seq', 146, true);


--
-- Data for Name: spaapp_plato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id, activo) FROM stdin;
20	Tallarin Salteado de Pollo	menuFoto/e.jpg	dasdas	5	5	12:00:00	5	5	1	t
22	Burrito	menuFoto/e_y8U7CUV.jpg	fds	5	5	12:00:00	5	5	1	t
39	aa	menuFoto/vegana_Huvzqnr.jpg	agggg	5	5	12:00:00	5	5	1	t
45	Chaulafan especial2	menuFoto/Chaulafan-1.jpg	arroz, pollo, camaron, gato	5	5	12:00:00	5	5	5	t
42	dasdas	menuFoto/e_0UtoCE0.jpg	platanos	5	5	12:00:00	5	5	2	t
47	Tallarin especial	menuFoto/tallarin_aBqyVQB.jpg	tallarin, camaron, salsa de la casa	5	5	12:00:00	5	5	5	f
43	popopopo	menuFoto/cevi.jpg	ojoojojojojojojo	5	5	12:00:00	5	5	2	t
41	cualquier cosa	menuFoto/chaulafan_uN8vdk7.jpg	huhuh	5	5	12:00:00	5	5	2	t
44	Chaulafan especial2	menuFoto/1548-Chaulafan-1.jpg	arroz, carne de gato, pollo, camaron	5	5	12:00:00	5	5	2	t
48	chancho con verduras	menuFoto/verduras_y_chancho.jpg	arroz, pollo, camaron, gato	5	5	12:00:00	5	5	5	t
2	Helado de chocolate	menuFoto/he2_sL69ayP.jpg	Helado de chocolate	2	4	20:19:35	8	12	1	f
49	Pizza Extra-Queso	menuFoto/extra.jpg	Mucho queso	5	5	12:00:00	5	5	2	t
54	Copas Locas	menuFoto/helado2doble.jpg	Dos copas de helado de cualquier sabor	5	5	12:00:00	5	5	1	t
25	Pizza de tomate	menuFoto/tomate.jpg	Deliciosa Pizza	5	5	12:00:00	5	5	2	t
33	Pastel Tres Leches	menuFoto/tresleches.jpg	Con deliciosa masa de bizcochuelo	5	5	12:00:00	5	5	9	t
29	veganito	menuFoto/vegana_Agp1DbI.jpg	hihihihi	5	5	12:00:00	5	5	2	f
51	hjghjgjgj	menuFoto/pizzaamericana.jpg	dsadasdasd	5	5	12:00:00	5	5	2	f
26	Pizza Vegana	menuFoto/vegana.jpg	Deliciosa Pizza con los mejores vegetales.	5	5	12:00:00	5	5	2	t
37	chaulafan	menuFoto/chaulafan_zL7HQdn.jpg	wwwww	5	5	12:00:00	5	5	8	f
35	Chaulafan especial	menuFoto/OTR01_Chaulafandepollo_30.jpg	Delicioso plato con carnes	5	5	12:00:00	5	5	5	t
38	Chaulafan especial	menuFoto/chaulafan_vr7K4I0.jpg	Delicioso plato con arroz	5	5	12:00:00	5	5	5	t
40	Bayesias	menuFoto/relleno_oW7HMJX.jpg	sasasassasasas	5	5	12:00:00	5	5	2	t
52	iopipi	menuFoto/extra_wiNCYSv.jpg	fsdfsfds	5	5	12:00:00	5	5	2	f
32	aaaa	menuFoto/tomate_wCBXXkx.jpg	aaaaaaaaa	5	5	12:00:00	5	5	2	f
10	pollo	menuFoto/Captura_de_pantalla_de_2016-07-19_180031.png	pollo	5	5	12:00:00	5	5	4	f
50	Pizza de Jamón	menuFoto/pizzajamon.jpg	Con jamón del día	5	5	12:00:00	5	5	2	t
36	Super Lojana	menuFoto/superPizza.jpg	queso, salami, tomate	5	5	12:00:00	5	5	2	f
34	rora	menuFoto/chaulafan_tZzD5gm.jpg	dedede	5	5	12:00:00	5	5	2	f
57	Soyard Piña	menuFoto/soyardpinia.png	Bebida refrescante con delicioso sabor a piña	12	12	12:00:00	12	12	10	t
30	Super Parrilla	menuFoto/parrilla.jpg	Deliciosas carnes	5	5	12:00:00	5	5	8	t
3	Hawaiana	menuFoto/plato6_lEA1j32.jpg	Deliciosa pizza que lleva jamón , piña	2	4	11:51:03	8	12	2	t
31	Pollito	menuFoto/relleno_X2dwMru.jpg	Delicia	5	5	12:00:00	5	5	8	f
59	Soyard Naranjilla	menuFoto/soyardVerde.png	Deliciosa bebida de naranjilla y soya	5	5	12:00:00	5	5	10	t
23	Arroz Relleno	menuFoto/relleno.jpg	Delicia	5	5	12:00:00	5	5	7	t
21	Tallarin y Pollo	menuFoto/e_fXRgePw.jpg	Tallarín italiano	5	5	12:00:00	5	5	2	t
53	Una copa loca	menuFoto/helado1bola.jpg	Delicioso helado con una bola de cualquier sabor	5	5	12:00:00	5	5	1	t
4	Napolitana	menuFoto/napo.jpg	La pizza napolitana puede contener anchoas, orégano, alcaparras y ajo, entre otros ingredientes.	2	4	11:51:03	8	12	2	t
55	Pizza de tomate Las Vegas	menuFoto/vegas.jpg	La mejor pizza!!!	5	5	12:00:00	5	5	2	t
56	pizzita	menuFoto/vegas_BMeAxzk.jpg	La mejor pizzaa chiquit	5	5	12:00:00	5	5	2	t
58	Soyard Maracuyá	menuFoto/soyardMaracuya.png	Deliciosa bebida de maracuyá y soya.	5	5	12:00:00	5	5	10	t
\.


--
-- Data for Name: spaapp_plato_id_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_plato_id_menu (id, plato_id, menu_id) FROM stdin;
68	29	1
71	26	1
79	37	10
80	35	10
84	38	10
23	20	2
25	22	2
87	40	10
88	39	10
95	42	10
98	43	10
99	41	10
101	44	10
56	2	1
64	25	1
122	45	12
124	47	12
125	48	12
126	49	13
130	51	13
131	52	13
132	32	2
133	10	2
134	50	13
135	36	1
136	34	1
138	30	1
139	3	1
141	31	1
147	4	1
153	54	4
154	33	4
158	23	1
159	21	1
161	53	4
163	55	1
166	56	1
171	58	14
176	57	14
179	59	14
\.


--
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_menu_id_seq', 179, true);


--
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_plato_seq', 59, true);


--
-- Data for Name: spaapp_platocategoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_platocategoria (id_platocategoria, categoria) FROM stdin;
1	Helados
2	Pizza
3	Hamburguesa
4	Bebida
5	Marisco
6	Pescado
7	Pollo
8	Carnes
9	Pasteles
10	Bebidas
\.


--
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platocategoria_id_platocategoria_seq', 10, true);


--
-- Data for Name: spaapp_platoingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_platoingrediente (id_plato_ingrediente, requerido, horafecha, id_ingrediente_id, id_plato_id) FROM stdin;
204	t	2016-08-17 17:16:33.303509-05	8	26
205	t	2016-08-17 17:16:33.303509-05	9	26
206	t	2016-08-17 17:16:33.303509-05	15	26
207	t	2016-08-17 17:16:33.303509-05	20	26
208	t	2016-08-17 17:16:33.303509-05	21	26
209	f	2016-08-17 17:16:33.303509-05	22	26
449	t	2016-08-25 10:36:08.559135-05	60	58
155	t	2016-08-16 10:12:25.294531-05	19	2
156	f	2016-08-16 10:12:25.294531-05	23	2
157	f	2016-08-16 10:12:25.294531-05	24	2
158	f	2016-08-16 10:12:25.294531-05	23	2
159	f	2016-08-16 10:12:25.294531-05	25	2
450	t	2016-08-25 10:36:08.559135-05	61	58
312	f	2016-08-18 18:15:39.123532-05	31	45
24	t	2016-08-09 18:07:19.00023-05	3	20
25	t	2016-08-09 18:07:19.00023-05	18	20
313	f	2016-08-18 18:15:39.123532-05	32	45
28	t	2016-08-09 18:22:42.147262-05	5	22
29	t	2016-08-09 18:22:42.147262-05	18	22
226	t	2016-08-18 15:26:50.610333-05	6	37
227	f	2016-08-18 15:26:50.610333-05	29	37
228	f	2016-08-18 15:34:48.779112-05	3	35
229	f	2016-08-18 15:34:48.779112-05	5	35
230	f	2016-08-18 15:34:48.779112-05	6	35
459	t	2016-08-25 10:46:56.370801-05	61	57
460	t	2016-08-25 10:46:56.370801-05	62	57
231	t	2016-08-18 15:34:48.779112-05	29	35
318	f	2016-08-18 18:17:06.566403-05	28	47
319	f	2016-08-18 18:17:06.566403-05	31	47
320	t	2016-08-18 18:17:06.566403-05	32	47
321	f	2016-08-18 18:17:06.566403-05	33	47
322	f	2016-08-18 18:18:44.240842-05	28	48
323	f	2016-08-18 18:18:44.240842-05	31	48
324	f	2016-08-18 18:18:44.240842-05	32	48
325	f	2016-08-18 18:18:44.240842-05	33	48
326	t	2016-08-18 18:18:44.240842-05	34	48
177	f	2016-08-17 11:46:29.860823-05	8	25
178	f	2016-08-17 11:46:29.860823-05	9	25
179	t	2016-08-17 11:46:29.860823-05	15	25
180	t	2016-08-17 11:46:29.860823-05	20	25
465	t	2016-08-25 11:00:19.720175-05	61	59
327	t	2016-08-19 16:44:29.779619-05	35	49
328	f	2016-08-19 16:44:29.779619-05	36	49
329	t	2016-08-19 16:44:29.779619-05	37	49
466	t	2016-08-25 11:00:19.720175-05	63	59
248	t	2016-08-18 15:37:34.102694-05	5	38
249	t	2016-08-18 15:37:34.102694-05	6	38
250	f	2016-08-18 15:37:34.102694-05	7	38
251	t	2016-08-18 15:37:34.102694-05	10	38
252	f	2016-08-18 15:37:34.102694-05	15	38
253	t	2016-08-18 15:37:34.102694-05	31	38
256	f	2016-08-18 15:44:30.255394-05	6	40
191	f	2016-08-17 17:14:45.060005-05	27	29
257	f	2016-08-18 15:44:56.826575-05	6	39
336	f	2016-08-19 17:00:30.527266-05	36	51
337	t	2016-08-19 17:00:30.527266-05	39	51
338	t	2016-08-19 17:00:36.211187-05	40	52
339	f	2016-08-19 17:00:36.211187-05	41	52
340	t	2016-08-22 10:53:48.024912-05	5	32
341	t	2016-08-22 10:54:06.727599-05	42	10
342	t	2016-08-22 11:55:40.116511-05	35	50
343	t	2016-08-22 11:55:40.116511-05	38	50
344	f	2016-08-22 12:43:12.403272-05	3	36
345	f	2016-08-22 12:43:12.403272-05	43	36
346	t	2016-08-22 12:43:12.403272-05	44	36
347	t	2016-08-22 12:43:12.403272-05	30	36
348	f	2016-08-22 12:43:18.276554-05	45	34
272	t	2016-08-18 15:52:16.579651-05	5	42
273	f	2016-08-18 15:52:16.579651-05	31	42
351	t	2016-08-22 12:50:50.929162-05	46	30
352	t	2016-08-22 12:50:50.929162-05	47	30
353	t	2016-08-22 13:51:04.672166-05	48	3
278	t	2016-08-18 15:56:46.144587-05	3	43
279	f	2016-08-18 15:56:46.144587-05	7	43
280	f	2016-08-18 16:01:34.890881-05	5	41
281	t	2016-08-18 16:01:34.890881-05	30	41
356	t	2016-08-22 13:52:02.416631-05	3	31
285	f	2016-08-18 16:28:04.776283-05	3	44
286	f	2016-08-18 16:28:04.776283-05	5	44
287	f	2016-08-18 16:28:04.776283-05	31	44
357	f	2016-08-22 13:52:02.416631-05	46	31
358	t	2016-08-22 13:52:02.416631-05	50	31
359	f	2016-08-22 13:52:02.416631-05	47	31
365	f	2016-08-22 15:20:57.095576-05	3	4
388	t	2016-08-22 16:24:45.767256-05	56	54
389	t	2016-08-22 16:24:45.767256-05	57	54
390	t	2016-08-22 16:30:37.304881-05	52	33
391	t	2016-08-22 16:30:37.304881-05	53	33
392	t	2016-08-22 16:30:37.304881-05	54	33
393	t	2016-08-22 16:30:37.304881-05	55	33
394	t	2016-08-22 16:30:37.304881-05	56	33
410	t	2016-08-23 10:53:30.692197-05	3	23
411	f	2016-08-23 10:53:30.692197-05	46	23
412	t	2016-08-23 10:53:30.692197-05	45	23
413	t	2016-08-23 10:53:30.692197-05	43	23
414	f	2016-08-23 10:53:30.692197-05	51	23
415	f	2016-08-23 15:35:44.650732-05	3	21
416	t	2016-08-23 15:35:44.650732-05	49	21
419	t	2016-08-23 17:00:30.815114-05	56	53
420	t	2016-08-23 17:00:30.815114-05	57	53
423	t	2016-08-23 18:47:52.190414-05	46	55
424	t	2016-08-23 18:47:52.190414-05	58	55
437	f	2016-08-24 17:08:24.243801-05	3	56
438	t	2016-08-24 17:08:24.243801-05	30	56
439	t	2016-08-24 17:08:24.243801-05	44	56
440	f	2016-08-24 17:08:24.243801-05	48	56
441	t	2016-08-24 17:08:24.243801-05	58	56
442	f	2016-08-24 17:08:24.243801-05	59	56
\.


--
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platoingrediente_id_plato_ingrediente_seq', 466, true);


--
-- Data for Name: spaapp_proveedor; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) FROM stdin;
1	Pizza	65821212		4	5	5216454645	1	1	2
2	Chinito	111111		1	\N	adasdasda	1	1	3
3	Casa China	1526464643		4	\N	221212	1	2	4
4	Ice Cream Social	111111		1	\N	sadasdas	1	3	5
5	Chamelion	65821212		4	\N	5216454645	1	4	6
6	PapaLogo	213123		1	\N	asdadsa	1	5	7
7	Tony's	65821212		4	\N	12	1	3	8
8	Fruques	1526464643		4	\N	5216454645	1	3	86
9	Gusy	343434		2	\N	232	1	7	94
10	Chifa Beijing	111111	2222222222	1	0	11054587878787878	1	2	130
11	PizzaHut	2898989898	0989798797	1	1	11054464	1	1	134
12	Soyard	156413	51352142	1	1	21121221	1	8	145
13	Loco Chavez	2588646	0984654561	1	1	1104464646	1	5	146
\.


--
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_proveedor_id_proveedor_seq', 13, true);


--
-- Data for Name: spaapp_redsocial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_redsocial (id_red_social, nombre_red_social, cuenta_persona, id_proveedor_id) FROM stdin;
\.


--
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_redsocial_id_red_social_seq', 1, false);


--
-- Data for Name: spaapp_sucursal; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id, descripcion, precio_envio) FROM stdin;
6	Tony's	212121	65821212		Lunes - Sábado	8:00 - 18:00	0	4	f	11:39:43	5	1	4	6	7	\N	\N
1	Pizza	111111111112	2588864	4444444445	Lunes - Sábado	13:30 - 20:00	1	1	f	15:00:20	1	1	1	1	1	Deliciosas Pizzas, con sabor!	1.2
12	Soyard	Soyard	44564645	45646445	Lunes a Viernes	12:00-15:00	1	1	f	20:13:22	12	1	162	14	12	Bebidas refrescante, ideal para consumir a cualquier hora del dia	1
5	Chamelion	212121	1526464643		Lunes - Sábado	8:00 - 18:00	1	4	f	20:11:15	3	1	4	5	5	\N	\N
3	Casa china	5216454645	65821212		Lunes - Sábado	8:00 - 18:00	1	4	f	20:08:04	6	1	3	3	3	\N	\N
9	Gusy	12121	12121		Lunes - Sabado	09:00 - 21:00	0	2	t	14:00:09	10	1	17	11	9	\N	\N
7	PapaLogo	asdadasd	dasdasda		7	08:00	0	1	f	12:00:00	2	1	2	7	6	\N	\N
2	Chinito	Restaurante	12345678		7	08:00	1	1	f	12:00:00	1	1	2	2	2		1
10	Chifa Beijing - Norte	111111111111	0985464655	4444444444	Lunes-Domingo	11:00 - 21:00	0	1	f	13:10:00	1	1	95	12	10	!Ven y disfruta de los mejores sabores de la comida asiáticaa!	1.5
11	PizzaHut - Zamora	11045646464	2588978	098846454	Lunes-Domingo	12:30 - 18:40	1	1	f	21:39:15	1	1	98	13	11	Pizza Hut es una empresa estadounidense de restaurantes de comida rápida, especializada en la elaboración de pizzas. Pertenece al grupo Yum! Brands.	1.1000000000000001
4	Ice Cream Social	heladería	2588897		Lunes-Viernes	13:00 -20:00	1	1	f	12:00:00	1	1	2	4	4	Los mejores helados y pasteles en un solo lugar!	1
\.


--
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_sucursal_id_sucursal_seq', 12, true);


--
-- Data for Name: spaapp_tamanio; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tamanio (id_tamanio, tamanio) FROM stdin;
1	Standard
2	Mediana
3	Grande
4	Normal
5	Simple
6	Doble
\.


--
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanio_id_tamanio_seq', 6, true);


--
-- Data for Name: spaapp_tamanioplato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id, activo) FROM stdin;
235	2.5	54	6	t
236	12	33	2	t
146	12.58	38	1	t
147	15.539999999999999	38	2	t
150	12	40	1	t
151	12	39	1	t
158	12	42	1	t
249	15	23	2	f
250	10	23	4	f
251	20	23	3	t
163	15.56	43	1	t
164	78.890000000000001	43	3	t
165	12.23	43	1	t
166	12	41	2	t
30	15	20	2	t
252	8	23	1	t
32	14	22	3	t
253	14	21	3	t
168	3.5600000000000001	44	2	t
254	10	21	2	t
101	1.5	2	2	t
102	2	2	3	t
256	1.1000000000000001	53	5	f
257	1.5	53	2	t
260	3.2000000000000002	55	4	t
261	5	55	2	t
179	3.5499999999999998	45	2	t
266	6.2999999999999998	56	1	t
116	12	25	2	f
117	15	25	3	t
118	10	25	4	t
267	12.32	56	3	f
181	2.5	47	2	t
182	5.25	48	2	t
183	12.5	49	2	t
271	1.1000000000000001	58	1	t
125	12	29	2	t
184	15.48	49	3	t
188	12.6	51	1	t
189	12.52	52	2	t
190	12	32	1	t
191	12	10	2	t
192	15	50	1	t
193	18	50	2	t
130	15.35	26	2	t
194	22.350000000000001	36	3	t
195	12	34	2	t
131	18.559999999999999	26	3	t
276	1.1000000000000001	57	1	t
279	1.1000000000000001	59	1	t
140	12	37	2	t
141	3.5	35	2	t
142	2.5299999999999998	35	1	t
198	15.6	30	4	t
199	18.300000000000001	30	3	t
200	3.5	3	4	t
201	5.5	3	3	t
204	12.35	31	2	t
205	20.32	31	1	t
219	10	4	1	t
220	11	4	2	t
\.


--
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanioplato_id_tamanioplato_seq', 279, true);


--
-- Data for Name: spaapp_tipopago; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tipopago (id_tipo_pago, tipo_pago, observaciones) FROM stdin;
1	Efectivo	Efectivo
2	Dinero Electrónico	Dinero Electrónico
\.


--
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopago_id_tipo_pago_seq', 2, true);


--
-- Data for Name: spaapp_tipopedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tipopedido (id_tipo_pedido, tipo_pedido, observaciones) FROM stdin;
1	A domicilio	A domicilio
2	Para recoger	Para recoger
\.


--
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopedido_id_tipo_pedido_seq', 2, true);


--
-- Data for Name: spaapp_transportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_transportista (id_transportista, tipo_transporte, tipo_servicio, id_perfil_id) FROM stdin;
8	MotoTaxi	Entrega	48
6	Taxi	Entrega	45
7	Taxi	Entrega	46
5	Moto	Servicio	44
10	2	1	108
1	Moto	Entrega	1
9	1	1	66
\.


--
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_transportista_id_transportista_seq', 10, true);


--
-- Data for Name: spaapp_valoracion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_valoracion (id_valoracion, valor_transportista, observaciones, valor_proveedor, valor_servicio, valoracion_general, id_cliente_id, id_pedido_id, id_proveedor_id, id_sucursal_id, id_transportista_id) FROM stdin;
\.


--
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_valoracion_id_valoracion_seq', 1, false);


--
-- Name: administracionAPP_contacto_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY "administracionAPP_contacto"
    ADD CONSTRAINT "administracionAPP_contacto_pkey" PRIMARY KEY (id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: social_auth_association_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_association
    ADD CONSTRAINT social_auth_association_pkey PRIMARY KEY (id);


--
-- Name: social_auth_code_email_801b2d02_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code
    ADD CONSTRAINT social_auth_code_email_801b2d02_uniq UNIQUE (email, code);


--
-- Name: social_auth_code_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code
    ADD CONSTRAINT social_auth_code_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce_server_url_f6284463_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_server_url_f6284463_uniq UNIQUE (server_url, "timestamp", salt);


--
-- Name: social_auth_usersocialauth_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth_provider_e6b5e668_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_provider_e6b5e668_uniq UNIQUE (provider, uid);


--
-- Name: spaapp_actividadcomercial_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_actividadcomercial
    ADD CONSTRAINT spaapp_actividadcomercial_pkey PRIMARY KEY (id_actividad);


--
-- Name: spaapp_catalogoestado_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_catalogoestado
    ADD CONSTRAINT spaapp_catalogoestado_pkey PRIMARY KEY (id_estado);


--
-- Name: spaapp_categoria_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_categoria
    ADD CONSTRAINT spaapp_categoria_pkey PRIMARY KEY (id_categoria);


--
-- Name: spaapp_ciudad_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ciudad
    ADD CONSTRAINT spaapp_ciudad_pkey PRIMARY KEY (id_ciudad);


--
-- Name: spaapp_cliente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente
    ADD CONSTRAINT spaapp_cliente_pkey PRIMARY KEY (id_cliente);


--
-- Name: spaapp_contrato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaapp_contrato_pkey PRIMARY KEY (id_contrato);


--
-- Name: spaapp_coordenadas_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas
    ADD CONSTRAINT spaapp_coordenadas_pkey PRIMARY KEY (id_coordenadas);


--
-- Name: spaapp_detallepedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detallepedido_pkey PRIMARY KEY (id_detalle);


--
-- Name: spaapp_direccion_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion
    ADD CONSTRAINT spaapp_direccion_pkey PRIMARY KEY (id_direccion);


--
-- Name: spaapp_horariotransportista_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista
    ADD CONSTRAINT spaapp_horariotransportista_pkey PRIMARY KEY (id_horario);


--
-- Name: spaapp_ingrediente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente
    ADD CONSTRAINT spaapp_ingrediente_pkey PRIMARY KEY (id_ingrediente);


--
-- Name: spaapp_menu_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu
    ADD CONSTRAINT spaapp_menu_pkey PRIMARY KEY (id_menu);


--
-- Name: spaapp_modificardetallepedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT spaapp_modificardetallepedido_pkey PRIMARY KEY (id_modificardetallepedido);


--
-- Name: spaapp_pedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_pedido_pkey PRIMARY KEY (id_pedido);


--
-- Name: spaapp_perfil_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil
    ADD CONSTRAINT spaapp_perfil_pkey PRIMARY KEY (id_perfil);


--
-- Name: spaapp_plato_id_menu_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_pkey PRIMARY KEY (id);


--
-- Name: spaapp_plato_id_menu_plato_id_a7b45c73_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_plato_id_a7b45c73_uniq UNIQUE (plato_id, menu_id);


--
-- Name: spaapp_plato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato
    ADD CONSTRAINT spaapp_plato_pkey PRIMARY KEY (id_plato);


--
-- Name: spaapp_platocategoria_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platocategoria
    ADD CONSTRAINT spaapp_platocategoria_pkey PRIMARY KEY (id_platocategoria);


--
-- Name: spaapp_platoingrediente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT spaapp_platoingrediente_pkey PRIMARY KEY (id_plato_ingrediente);


--
-- Name: spaapp_proveedor_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveedor_pkey PRIMARY KEY (id_proveedor);


--
-- Name: spaapp_redsocial_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial
    ADD CONSTRAINT spaapp_redsocial_pkey PRIMARY KEY (id_red_social);


--
-- Name: spaapp_sucursal_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursal_pkey PRIMARY KEY (id_sucursal);


--
-- Name: spaapp_tamanio_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanio
    ADD CONSTRAINT spaapp_tamanio_pkey PRIMARY KEY (id_tamanio);


--
-- Name: spaapp_tamanioplato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tamanioplato_pkey PRIMARY KEY (id_tamanioplato);


--
-- Name: spaapp_tipopago_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopago
    ADD CONSTRAINT spaapp_tipopago_pkey PRIMARY KEY (id_tipo_pago);


--
-- Name: spaapp_tipopedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopedido
    ADD CONSTRAINT spaapp_tipopedido_pkey PRIMARY KEY (id_tipo_pedido);


--
-- Name: spaapp_transportista_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista
    ADD CONSTRAINT spaapp_transportista_pkey PRIMARY KEY (id_transportista);


--
-- Name: spaapp_valoracion_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valoracion_pkey PRIMARY KEY (id_valoracion);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: social_auth_code_c1336794; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_code_c1336794 ON social_auth_code USING btree (code);


--
-- Name: social_auth_code_code_a2393167_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_code_code_a2393167_like ON social_auth_code USING btree (code varchar_pattern_ops);


--
-- Name: social_auth_usersocialauth_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_usersocialauth_e8701ad4 ON social_auth_usersocialauth USING btree (user_id);


--
-- Name: spaapp_cliente_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_cliente_7d3abc31 ON spaapp_cliente USING btree (id_perfil_id);


--
-- Name: spaapp_contrato_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_contrato_ca253e0c ON spaapp_contrato USING btree (id_proveedor_id);


--
-- Name: spaapp_contrato_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_contrato_e163c0f9 ON spaapp_contrato USING btree (id_sucursal_id);


--
-- Name: spaapp_coordenadas_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_coordenadas_7d3abc31 ON spaapp_coordenadas USING btree (id_perfil_id);


--
-- Name: spaapp_detallepedido_55ef5a99; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_55ef5a99 ON spaapp_detallepedido USING btree (tamanio_id);


--
-- Name: spaapp_detallepedido_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_8690b5df ON spaapp_detallepedido USING btree (id_plato_id);


--
-- Name: spaapp_detallepedido_9b93490d; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_9b93490d ON spaapp_detallepedido USING btree (id_pedido_id);


--
-- Name: spaapp_direccion_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_direccion_7d3abc31 ON spaapp_direccion USING btree (id_perfil_id);


--
-- Name: spaapp_horariotransportista_113016bf; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_horariotransportista_113016bf ON spaapp_horariotransportista USING btree (id_transportista_id);


--
-- Name: spaapp_menu_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_menu_ca253e0c ON spaapp_menu USING btree (id_proveedor_id);


--
-- Name: spaapp_modificardetallepedido_61139c8c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_modificardetallepedido_61139c8c ON spaapp_modificardetallepedido USING btree (id_detallepedido_id);


--
-- Name: spaapp_modificardetallepedido_99518874; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_modificardetallepedido_99518874 ON spaapp_modificardetallepedido USING btree (id_ingrediente_quitar_id);


--
-- Name: spaapp_pedido_21740aad; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_21740aad ON spaapp_pedido USING btree (id_tipo_pedido_id);


--
-- Name: spaapp_pedido_9639f13b; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_9639f13b ON spaapp_pedido USING btree (id_direccion_id);


--
-- Name: spaapp_pedido_98181a38; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_98181a38 ON spaapp_pedido USING btree (id_estado_id);


--
-- Name: spaapp_pedido_abfe0f96; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_abfe0f96 ON spaapp_pedido USING btree (usuario_id);


--
-- Name: spaapp_pedido_ccfa12af; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_ccfa12af ON spaapp_pedido USING btree (id_tipo_pago_id);


--
-- Name: spaapp_pedido_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_e163c0f9 ON spaapp_pedido USING btree (id_sucursal_id);


--
-- Name: spaapp_pedido_f09d4acb; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_f09d4acb ON spaapp_pedido USING btree (id_trasportista_id);


--
-- Name: spaapp_perfil_76a74f43; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_perfil_76a74f43 ON spaapp_perfil USING btree (id_usuario_id);


--
-- Name: spaapp_plato_06d53be8; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_06d53be8 ON spaapp_plato USING btree (id_platocategoria_id);


--
-- Name: spaapp_plato_id_menu_20104dab; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_id_menu_20104dab ON spaapp_plato_id_menu USING btree (plato_id);


--
-- Name: spaapp_plato_id_menu_93e25458; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_id_menu_93e25458 ON spaapp_plato_id_menu USING btree (menu_id);


--
-- Name: spaapp_platoingrediente_4b077a6c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_platoingrediente_4b077a6c ON spaapp_platoingrediente USING btree (id_ingrediente_id);


--
-- Name: spaapp_platoingrediente_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_platoingrediente_8690b5df ON spaapp_platoingrediente USING btree (id_plato_id);


--
-- Name: spaapp_proveedor_0e29f854; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_0e29f854 ON spaapp_proveedor USING btree (id_categoria_id);


--
-- Name: spaapp_proveedor_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_7d3abc31 ON spaapp_proveedor USING btree (id_perfil_id);


--
-- Name: spaapp_proveedor_c1f5aad7; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_c1f5aad7 ON spaapp_proveedor USING btree (id_actividad_id);


--
-- Name: spaapp_redsocial_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_redsocial_ca253e0c ON spaapp_redsocial USING btree (id_proveedor_id);


--
-- Name: spaapp_sucursal_9639f13b; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_9639f13b ON spaapp_sucursal USING btree (id_direccion_id);


--
-- Name: spaapp_sucursal_b1a01987; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_b1a01987 ON spaapp_sucursal USING btree (id_ciudad_id);


--
-- Name: spaapp_sucursal_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_ca253e0c ON spaapp_sucursal USING btree (id_proveedor_id);


--
-- Name: spaapp_sucursal_e1161949; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_e1161949 ON spaapp_sucursal USING btree (id_menu_id);


--
-- Name: spaapp_tamanioplato_05d6b9c9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_tamanioplato_05d6b9c9 ON spaapp_tamanioplato USING btree (id_tamanio_id);


--
-- Name: spaapp_tamanioplato_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_tamanioplato_8690b5df ON spaapp_tamanioplato USING btree (id_plato_id);


--
-- Name: spaapp_transportista_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_transportista_7d3abc31 ON spaapp_transportista USING btree (id_perfil_id);


--
-- Name: spaapp_valoracion_113016bf; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_113016bf ON spaapp_valoracion USING btree (id_transportista_id);


--
-- Name: spaapp_valoracion_6d98a18e; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_6d98a18e ON spaapp_valoracion USING btree (id_cliente_id);


--
-- Name: spaapp_valoracion_9b93490d; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_9b93490d ON spaapp_valoracion USING btree (id_pedido_id);


--
-- Name: spaapp_valoracion_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_ca253e0c ON spaapp_valoracion USING btree (id_proveedor_id);


--
-- Name: spaapp_valoracion_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_e163c0f9 ON spaapp_valoracion USING btree (id_sucursal_id);


--
-- Name: trigger_menu; Type: TRIGGER; Schema: spaapp; Owner: spauser
--

CREATE TRIGGER trigger_menu AFTER INSERT ON spaapp_proveedor FOR EACH ROW EXECUTE PROCEDURE insertar_menu();


--
-- Name: trigger_perfil; Type: TRIGGER; Schema: spaapp; Owner: spauser
--

CREATE TRIGGER trigger_perfil AFTER INSERT ON auth_user FOR EACH ROW EXECUTE PROCEDURE insertar_perfil();


--
-- Name: D16bd3bac72562dcaa9ab5f9f6fb85ae; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista
    ADD CONSTRAINT "D16bd3bac72562dcaa9ab5f9f6fb85ae" FOREIGN KEY (id_transportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D7b946988cbfb9911c65af920b3935cb; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT "D7b946988cbfb9911c65af920b3935cb" FOREIGN KEY (id_detallepedido_id) REFERENCES spaapp_detallepedido(id_detalle) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D893b6353e00f54fed4819335a2ca882; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT "D893b6353e00f54fed4819335a2ca882" FOREIGN KEY (id_transportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D91d9aaf1a328bd31979d1581c55c5ec; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT "D91d9aaf1a328bd31979d1581c55c5ec" FOREIGN KEY (id_actividad_id) REFERENCES spaapp_actividadcomercial(id_actividad) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D944537a2d489f0d21d6406b48ddb641; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT "D944537a2d489f0d21d6406b48ddb641" FOREIGN KEY (id_trasportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ba7c74bed2241fbcd786c213851660dc; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT ba7c74bed2241fbcd786c213851660dc FOREIGN KEY (id_ingrediente_id) REFERENCES spaapp_ingrediente(id_ingrediente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: dce350061342863e912703a9dafef09e; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato
    ADD CONSTRAINT dce350061342863e912703a9dafef09e FOREIGN KEY (id_platocategoria_id) REFERENCES spaapp_platocategoria(id_platocategoria) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: e7cb8922641d3a71e0518cc63d3617b2; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT e7cb8922641d3a71e0518cc63d3617b2 FOREIGN KEY (id_ingrediente_quitar_id) REFERENCES spaapp_ingrediente(id_ingrediente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: id_tipo_pedido_id_cde83768_fk_spaapp_tipopedido_id_tipo_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT id_tipo_pedido_id_cde83768_fk_spaapp_tipopedido_id_tipo_pedido FOREIGN KEY (id_tipo_pedido_id) REFERENCES spaapp_tipopedido(id_tipo_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaa_tamanio_id_8427c076_fk_spaapp_tamanioplato_id_tamanioplato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaa_tamanio_id_8427c076_fk_spaapp_tamanioplato_id_tamanioplato FOREIGN KEY (tamanio_id) REFERENCES spaapp_tamanioplato(id_tamanioplato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_categoria_id_d672ac86_fk_spaapp_categoria_id_categoria; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaap_id_categoria_id_d672ac86_fk_spaapp_categoria_id_categoria FOREIGN KEY (id_categoria_id) REFERENCES spaapp_categoria(id_categoria) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_direccion_id_80462fd2_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaap_id_direccion_id_80462fd2_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_direccion_id_e2c4fbe4_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaap_id_direccion_id_e2c4fbe4_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_11675f72_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaap_id_proveedor_id_11675f72_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_18c32e0e_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial
    ADD CONSTRAINT spaap_id_proveedor_id_18c32e0e_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_540b8b72_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaap_id_proveedor_id_540b8b72_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_66760433_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaap_id_proveedor_id_66760433_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_e39d0124_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu
    ADD CONSTRAINT spaap_id_proveedor_id_e39d0124_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_c_id_sucursal_id_54ec1010_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaapp_c_id_sucursal_id_54ec1010_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_cliente_id_perfil_id_6a66d1dd_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente
    ADD CONSTRAINT spaapp_cliente_id_perfil_id_6a66d1dd_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_coorden_id_perfil_id_40dcc2a5_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas
    ADD CONSTRAINT spaapp_coorden_id_perfil_id_40dcc2a5_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_detalle_id_pedido_id_d433e145_fk_spaapp_pedido_id_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detalle_id_pedido_id_d433e145_fk_spaapp_pedido_id_pedido FOREIGN KEY (id_pedido_id) REFERENCES spaapp_pedido(id_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_detalleped_id_plato_id_5455a09f_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detalleped_id_plato_id_5455a09f_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_direcci_id_perfil_id_ff0b3f89_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion
    ADD CONSTRAINT spaapp_direcci_id_perfil_id_ff0b3f89_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_id_estado_id_a224fdfb_fk_spaapp_catalogoestado_id_estado; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_id_estado_id_a224fdfb_fk_spaapp_catalogoestado_id_estado FOREIGN KEY (id_estado_id) REFERENCES spaapp_catalogoestado(id_estado) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_id_tipo_pago_id_1cbd151a_fk_spaapp_tipopago_id_tipo_pago; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_id_tipo_pago_id_1cbd151a_fk_spaapp_tipopago_id_tipo_pago FOREIGN KEY (id_tipo_pago_id) REFERENCES spaapp_tipopago(id_tipo_pago) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_ingrediente_id_proveedor_id_fkey; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente
    ADD CONSTRAINT spaapp_ingrediente_id_proveedor_id_fkey FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor);


--
-- Name: spaapp_p_id_sucursal_id_b6e7aa23_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_p_id_sucursal_id_b6e7aa23_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_pedido_usuario_id_03fad4c7_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_pedido_usuario_id_03fad4c7_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_perfil_id_usuario_id_ecaf41b3_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil
    ADD CONSTRAINT spaapp_perfil_id_usuario_id_ecaf41b3_fk_auth_user_id FOREIGN KEY (id_usuario_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_plato_id_menu_menu_id_f192d1d8_fk_spaapp_menu_id_menu; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_menu_id_f192d1d8_fk_spaapp_menu_id_menu FOREIGN KEY (menu_id) REFERENCES spaapp_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_plato_id_menu_plato_id_1bbe6660_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_plato_id_1bbe6660_fk_spaapp_plato_id_plato FOREIGN KEY (plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_platoingre_id_plato_id_fe44a1d9_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT spaapp_platoingre_id_plato_id_fe44a1d9_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_proveed_id_perfil_id_fff821d0_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveed_id_perfil_id_fff821d0_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_sucursa_id_ciudad_id_f5184cf6_fk_spaapp_ciudad_id_ciudad; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursa_id_ciudad_id_f5184cf6_fk_spaapp_ciudad_id_ciudad FOREIGN KEY (id_ciudad_id) REFERENCES spaapp_ciudad(id_ciudad) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_sucursal_id_menu_id_81f57738_fk_spaapp_menu_id_menu; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursal_id_menu_id_81f57738_fk_spaapp_menu_id_menu FOREIGN KEY (id_menu_id) REFERENCES spaapp_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_tama_id_tamanio_id_a9692104_fk_spaapp_tamanio_id_tamanio; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tama_id_tamanio_id_a9692104_fk_spaapp_tamanio_id_tamanio FOREIGN KEY (id_tamanio_id) REFERENCES spaapp_tamanio(id_tamanio) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_tamaniopla_id_plato_id_5f8f9ca8_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tamaniopla_id_plato_id_5f8f9ca8_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_transpo_id_perfil_id_9a457afd_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista
    ADD CONSTRAINT spaapp_transpo_id_perfil_id_9a457afd_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_v_id_sucursal_id_a7f67c7c_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_v_id_sucursal_id_a7f67c7c_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_valo_id_cliente_id_a7d43e7d_fk_spaapp_cliente_id_cliente; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valo_id_cliente_id_a7d43e7d_fk_spaapp_cliente_id_cliente FOREIGN KEY (id_cliente_id) REFERENCES spaapp_cliente(id_cliente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_valorac_id_pedido_id_119ed90a_fk_spaapp_pedido_id_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valorac_id_pedido_id_119ed90a_fk_spaapp_pedido_id_pedido FOREIGN KEY (id_pedido_id) REFERENCES spaapp_pedido(id_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp; Type: ACL; Schema: -; Owner: spauser
--

REVOKE ALL ON SCHEMA spaapp FROM PUBLIC;
REVOKE ALL ON SCHEMA spaapp FROM spauser;
GRANT ALL ON SCHEMA spaapp TO spauser;
GRANT ALL ON SCHEMA spaapp TO postgres;


--
-- PostgreSQL database dump complete
--

