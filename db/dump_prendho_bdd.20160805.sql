--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-08-05 19:31:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 16386)
-- Name: spaapp; Type: SCHEMA; Schema: -; Owner: spauser
--

CREATE SCHEMA spaapp;


ALTER SCHEMA spaapp OWNER TO spauser;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2692 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = spaapp, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 255 (class 1259 OID 27636)
-- Name: administracionAPP_contacto; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE "administracionAPP_contacto" (
    id integer NOT NULL,
    nombre_propietario character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    ciudad character varying(50) NOT NULL,
    nombre_empresa character varying(50) NOT NULL,
    direccion character varying(50) NOT NULL,
    tipo_negocio character varying(50) NOT NULL,
    telefono double precision NOT NULL
);


ALTER TABLE "administracionAPP_contacto" OWNER TO spauser;

--
-- TOC entry 254 (class 1259 OID 27634)
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE "administracionAPP_contacto_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "administracionAPP_contacto_id_seq" OWNER TO spauser;

--
-- TOC entry 2693 (class 0 OID 0)
-- Dependencies: 254
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE "administracionAPP_contacto_id_seq" OWNED BY "administracionAPP_contacto".id;


--
-- TOC entry 189 (class 1259 OID 27056)
-- Name: auth_group; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO spauser;

--
-- TOC entry 188 (class 1259 OID 27054)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO spauser;

--
-- TOC entry 2694 (class 0 OID 0)
-- Dependencies: 188
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- TOC entry 191 (class 1259 OID 27066)
-- Name: auth_group_permissions; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO spauser;

--
-- TOC entry 190 (class 1259 OID 27064)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO spauser;

--
-- TOC entry 2695 (class 0 OID 0)
-- Dependencies: 190
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- TOC entry 187 (class 1259 OID 27048)
-- Name: auth_permission; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO spauser;

--
-- TOC entry 186 (class 1259 OID 27046)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO spauser;

--
-- TOC entry 2696 (class 0 OID 0)
-- Dependencies: 186
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- TOC entry 193 (class 1259 OID 27074)
-- Name: auth_user; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO spauser;

--
-- TOC entry 195 (class 1259 OID 27084)
-- Name: auth_user_groups; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO spauser;

--
-- TOC entry 194 (class 1259 OID 27082)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO spauser;

--
-- TOC entry 2697 (class 0 OID 0)
-- Dependencies: 194
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- TOC entry 192 (class 1259 OID 27072)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO spauser;

--
-- TOC entry 2698 (class 0 OID 0)
-- Dependencies: 192
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- TOC entry 197 (class 1259 OID 27092)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO spauser;

--
-- TOC entry 196 (class 1259 OID 27090)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO spauser;

--
-- TOC entry 2699 (class 0 OID 0)
-- Dependencies: 196
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- TOC entry 253 (class 1259 OID 27611)
-- Name: django_admin_log; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO spauser;

--
-- TOC entry 252 (class 1259 OID 27609)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO spauser;

--
-- TOC entry 2700 (class 0 OID 0)
-- Dependencies: 252
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- TOC entry 185 (class 1259 OID 27038)
-- Name: django_content_type; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO spauser;

--
-- TOC entry 184 (class 1259 OID 27036)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO spauser;

--
-- TOC entry 2701 (class 0 OID 0)
-- Dependencies: 184
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- TOC entry 183 (class 1259 OID 27027)
-- Name: django_migrations; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO spauser;

--
-- TOC entry 182 (class 1259 OID 27025)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO spauser;

--
-- TOC entry 2702 (class 0 OID 0)
-- Dependencies: 182
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- TOC entry 264 (class 1259 OID 27697)
-- Name: django_session; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO spauser;

--
-- TOC entry 257 (class 1259 OID 27644)
-- Name: social_auth_association; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_association (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    handle character varying(255) NOT NULL,
    secret character varying(255) NOT NULL,
    issued integer NOT NULL,
    lifetime integer NOT NULL,
    assoc_type character varying(64) NOT NULL
);


ALTER TABLE social_auth_association OWNER TO spauser;

--
-- TOC entry 256 (class 1259 OID 27642)
-- Name: social_auth_association_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_association_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_association_id_seq OWNER TO spauser;

--
-- TOC entry 2703 (class 0 OID 0)
-- Dependencies: 256
-- Name: social_auth_association_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_association_id_seq OWNED BY social_auth_association.id;


--
-- TOC entry 259 (class 1259 OID 27655)
-- Name: social_auth_code; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_code (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    code character varying(32) NOT NULL,
    verified boolean NOT NULL
);


ALTER TABLE social_auth_code OWNER TO spauser;

--
-- TOC entry 258 (class 1259 OID 27653)
-- Name: social_auth_code_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_code_id_seq OWNER TO spauser;

--
-- TOC entry 2704 (class 0 OID 0)
-- Dependencies: 258
-- Name: social_auth_code_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_code_id_seq OWNED BY social_auth_code.id;


--
-- TOC entry 261 (class 1259 OID 27663)
-- Name: social_auth_nonce; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_nonce (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    "timestamp" integer NOT NULL,
    salt character varying(65) NOT NULL
);


ALTER TABLE social_auth_nonce OWNER TO spauser;

--
-- TOC entry 260 (class 1259 OID 27661)
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_nonce_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_nonce_id_seq OWNER TO spauser;

--
-- TOC entry 2705 (class 0 OID 0)
-- Dependencies: 260
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_nonce_id_seq OWNED BY social_auth_nonce.id;


--
-- TOC entry 263 (class 1259 OID 27671)
-- Name: social_auth_usersocialauth; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE social_auth_usersocialauth (
    id integer NOT NULL,
    provider character varying(32) NOT NULL,
    uid character varying(255) NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE social_auth_usersocialauth OWNER TO spauser;

--
-- TOC entry 262 (class 1259 OID 27669)
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE social_auth_usersocialauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_usersocialauth_id_seq OWNER TO spauser;

--
-- TOC entry 2706 (class 0 OID 0)
-- Dependencies: 262
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE social_auth_usersocialauth_id_seq OWNED BY social_auth_usersocialauth.id;


--
-- TOC entry 199 (class 1259 OID 27152)
-- Name: spaapp_actividadcomercial; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_actividadcomercial (
    id_actividad integer NOT NULL,
    tipo_actividad character varying(100) NOT NULL,
    detalles character varying(250) NOT NULL
);


ALTER TABLE spaapp_actividadcomercial OWNER TO spauser;

--
-- TOC entry 198 (class 1259 OID 27150)
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_actividadcomercial_id_actividad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_actividadcomercial_id_actividad_seq OWNER TO spauser;

--
-- TOC entry 2707 (class 0 OID 0)
-- Dependencies: 198
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_actividadcomercial_id_actividad_seq OWNED BY spaapp_actividadcomercial.id_actividad;


--
-- TOC entry 201 (class 1259 OID 27160)
-- Name: spaapp_categoria; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_categoria (
    id_categoria integer NOT NULL,
    categoria character varying(150) NOT NULL,
    detalles character varying(250) NOT NULL
);


ALTER TABLE spaapp_categoria OWNER TO spauser;

--
-- TOC entry 200 (class 1259 OID 27158)
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_categoria_id_categoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_categoria_id_categoria_seq OWNER TO spauser;

--
-- TOC entry 2708 (class 0 OID 0)
-- Dependencies: 200
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_categoria_id_categoria_seq OWNED BY spaapp_categoria.id_categoria;


--
-- TOC entry 203 (class 1259 OID 27168)
-- Name: spaapp_ciudad; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_ciudad (
    id_ciudad integer NOT NULL,
    nombre character varying(80) NOT NULL,
    descripcion character varying(250) NOT NULL
);


ALTER TABLE spaapp_ciudad OWNER TO spauser;

--
-- TOC entry 202 (class 1259 OID 27166)
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_ciudad_id_ciudad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_ciudad_id_ciudad_seq OWNER TO spauser;

--
-- TOC entry 2709 (class 0 OID 0)
-- Dependencies: 202
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_ciudad_id_ciudad_seq OWNED BY spaapp_ciudad.id_ciudad;


--
-- TOC entry 205 (class 1259 OID 27176)
-- Name: spaapp_cliente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_cliente (
    id_cliente integer NOT NULL,
    fechainicio_suscripcion date NOT NULL,
    fecha_pago date,
    detalles_adicionales character varying(250) NOT NULL,
    pedidos_activos boolean,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_cliente OWNER TO spauser;

--
-- TOC entry 204 (class 1259 OID 27174)
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_cliente_id_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_cliente_id_cliente_seq OWNER TO spauser;

--
-- TOC entry 2710 (class 0 OID 0)
-- Dependencies: 204
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_cliente_id_cliente_seq OWNED BY spaapp_cliente.id_cliente;


--
-- TOC entry 207 (class 1259 OID 27184)
-- Name: spaapp_contrato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_contrato (
    id_contrato integer NOT NULL,
    codigo character varying(20) NOT NULL,
    detalles character varying(250) NOT NULL,
    vigencia integer NOT NULL,
    fecha_inicio date NOT NULL,
    "fechaFin" date NOT NULL,
    valor double precision NOT NULL,
    ruta_contrato character varying(100),
    id_proveedor_id integer,
    id_sucursal_id integer
);


ALTER TABLE spaapp_contrato OWNER TO spauser;

--
-- TOC entry 206 (class 1259 OID 27182)
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_contrato_id_contrato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_contrato_id_contrato_seq OWNER TO spauser;

--
-- TOC entry 2711 (class 0 OID 0)
-- Dependencies: 206
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_contrato_id_contrato_seq OWNED BY spaapp_contrato.id_contrato;


--
-- TOC entry 209 (class 1259 OID 27192)
-- Name: spaapp_coordenadas; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_coordenadas (
    id_coordenadas integer NOT NULL,
    latitud character varying(100) NOT NULL,
    longitud character varying(100) NOT NULL,
    fecha timestamp with time zone,
    estado integer,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_coordenadas OWNER TO spauser;

--
-- TOC entry 208 (class 1259 OID 27190)
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_coordenadas_id_coordenadas_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_coordenadas_id_coordenadas_seq OWNER TO spauser;

--
-- TOC entry 2712 (class 0 OID 0)
-- Dependencies: 208
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_coordenadas_id_coordenadas_seq OWNED BY spaapp_coordenadas.id_coordenadas;


--
-- TOC entry 211 (class 1259 OID 27200)
-- Name: spaapp_detallepedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_detallepedido (
    id_detalle integer NOT NULL,
    valor double precision NOT NULL,
    cantidad_solicitada integer NOT NULL,
    id_pedido_id integer,
    id_plato_id integer,
    tamanio_id integer
);


ALTER TABLE spaapp_detallepedido OWNER TO spauser;

--
-- TOC entry 210 (class 1259 OID 27198)
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_detallepedido_id_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_detallepedido_id_detalle_seq OWNER TO spauser;

--
-- TOC entry 2713 (class 0 OID 0)
-- Dependencies: 210
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_detallepedido_id_detalle_seq OWNED BY spaapp_detallepedido.id_detalle;


--
-- TOC entry 213 (class 1259 OID 27208)
-- Name: spaapp_direccion; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_direccion (
    id_direccion integer NOT NULL,
    calle_principal character varying(100) NOT NULL,
    calle_secundaria character varying(100) NOT NULL,
    numero_casa character varying(5),
    latitud character varying(100) NOT NULL,
    longitud character varying(100) NOT NULL,
    referencia character varying(250) NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_direccion OWNER TO spauser;

--
-- TOC entry 212 (class 1259 OID 27206)
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_direccion_id_direccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_direccion_id_direccion_seq OWNER TO spauser;

--
-- TOC entry 2714 (class 0 OID 0)
-- Dependencies: 212
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_direccion_id_direccion_seq OWNED BY spaapp_direccion.id_direccion;


--
-- TOC entry 215 (class 1259 OID 27219)
-- Name: spaapp_horariotransportista; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_horariotransportista (
    id_horario integer NOT NULL,
    dias character varying(80) NOT NULL,
    horas character varying(50) NOT NULL,
    id_transportista_id integer NOT NULL
);


ALTER TABLE spaapp_horariotransportista OWNER TO spauser;

--
-- TOC entry 214 (class 1259 OID 27217)
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_horariotransportista_id_horario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_horariotransportista_id_horario_seq OWNER TO spauser;

--
-- TOC entry 2715 (class 0 OID 0)
-- Dependencies: 214
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_horariotransportista_id_horario_seq OWNED BY spaapp_horariotransportista.id_horario;


--
-- TOC entry 217 (class 1259 OID 27227)
-- Name: spaapp_ingrediente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_ingrediente (
    id_ingrediente integer NOT NULL,
    nombre character varying(100) NOT NULL
);


ALTER TABLE spaapp_ingrediente OWNER TO spauser;

--
-- TOC entry 216 (class 1259 OID 27225)
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_ingrediente_id_ingrediente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_ingrediente_id_ingrediente_seq OWNER TO spauser;

--
-- TOC entry 2716 (class 0 OID 0)
-- Dependencies: 216
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_ingrediente_id_ingrediente_seq OWNED BY spaapp_ingrediente.id_ingrediente;


--
-- TOC entry 219 (class 1259 OID 27235)
-- Name: spaapp_menu; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_menu (
    id_menu integer NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_menu OWNER TO spauser;

--
-- TOC entry 218 (class 1259 OID 27233)
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_menu_id_menu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_menu_id_menu_seq OWNER TO spauser;

--
-- TOC entry 2717 (class 0 OID 0)
-- Dependencies: 218
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_menu_id_menu_seq OWNED BY spaapp_menu.id_menu;


--
-- TOC entry 221 (class 1259 OID 27243)
-- Name: spaapp_modificardetallepedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_modificardetallepedido (
    id_modificardetallepedido integer NOT NULL,
    cantidad_agregar integer,
    valor_modificacion double precision,
    id_ingrediente_quitar_id integer,
    id_plato_agregar_id integer
);


ALTER TABLE spaapp_modificardetallepedido OWNER TO spauser;

--
-- TOC entry 220 (class 1259 OID 27241)
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_modificardetallepedido_id_modificardetallepedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_modificardetallepedido_id_modificardetallepedido_seq OWNER TO spauser;

--
-- TOC entry 2718 (class 0 OID 0)
-- Dependencies: 220
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_modificardetallepedido_id_modificardetallepedido_seq OWNED BY spaapp_modificardetallepedido.id_modificardetallepedido;


--
-- TOC entry 223 (class 1259 OID 27251)
-- Name: spaapp_pedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_pedido (
    id_pedido integer NOT NULL,
    fecha timestamp with time zone NOT NULL,
    estado character varying(50) NOT NULL,
    observaciones character varying(250),
    valor double precision NOT NULL,
    tiempo integer,
    id_direccion_id integer,
    id_sucursal_id integer,
    id_tipo_pago_id integer,
    id_tipo_pedido_id integer,
    id_trasportista_id integer,
    usuario_id integer
);


ALTER TABLE spaapp_pedido OWNER TO spauser;

--
-- TOC entry 222 (class 1259 OID 27249)
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_pedido_id_pedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_pedido_id_pedido_seq OWNER TO spauser;

--
-- TOC entry 2719 (class 0 OID 0)
-- Dependencies: 222
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_pedido_id_pedido_seq OWNED BY spaapp_pedido.id_pedido;


--
-- TOC entry 225 (class 1259 OID 27259)
-- Name: spaapp_perfil; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_perfil (
    id_perfil integer NOT NULL,
    cedula character varying(10),
    fecha_nacimiento date,
    telefono character varying(10),
    genero character varying(10),
    ruta_foto character varying(100) NOT NULL,
    sim_dispositivo character varying(30) NOT NULL,
    id_usuario_id integer NOT NULL
);


ALTER TABLE spaapp_perfil OWNER TO spauser;

--
-- TOC entry 224 (class 1259 OID 27257)
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_perfil_id_perfil_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_perfil_id_perfil_seq OWNER TO spauser;

--
-- TOC entry 2720 (class 0 OID 0)
-- Dependencies: 224
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_perfil_id_perfil_seq OWNED BY spaapp_perfil.id_perfil;


--
-- TOC entry 227 (class 1259 OID 27267)
-- Name: spaapp_plato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_plato (
    id_plato integer NOT NULL,
    nombre_producto character varying(200) NOT NULL,
    ruta_foto character varying(100) NOT NULL,
    descripcion character varying(200) NOT NULL,
    cantidad_diaria integer NOT NULL,
    venta_aproximada integer NOT NULL,
    hora_mayor_venta time without time zone NOT NULL,
    cantidad_mayor_venta integer NOT NULL,
    existencia_actual integer,
    id_platocategoria_id integer NOT NULL
);


ALTER TABLE spaapp_plato OWNER TO spauser;

--
-- TOC entry 229 (class 1259 OID 27278)
-- Name: spaapp_plato_id_menu; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_plato_id_menu (
    id integer NOT NULL,
    plato_id integer NOT NULL,
    menu_id integer NOT NULL
);


ALTER TABLE spaapp_plato_id_menu OWNER TO spauser;

--
-- TOC entry 228 (class 1259 OID 27276)
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_plato_id_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_plato_id_menu_id_seq OWNER TO spauser;

--
-- TOC entry 2721 (class 0 OID 0)
-- Dependencies: 228
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_plato_id_menu_id_seq OWNED BY spaapp_plato_id_menu.id;


--
-- TOC entry 226 (class 1259 OID 27265)
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_plato_id_plato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_plato_id_plato_seq OWNER TO spauser;

--
-- TOC entry 2722 (class 0 OID 0)
-- Dependencies: 226
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_plato_id_plato_seq OWNED BY spaapp_plato.id_plato;


--
-- TOC entry 231 (class 1259 OID 27286)
-- Name: spaapp_platocategoria; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_platocategoria (
    id_platocategoria integer NOT NULL,
    categoria character varying(50) NOT NULL
);


ALTER TABLE spaapp_platocategoria OWNER TO spauser;

--
-- TOC entry 230 (class 1259 OID 27284)
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_platocategoria_id_platocategoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_platocategoria_id_platocategoria_seq OWNER TO spauser;

--
-- TOC entry 2723 (class 0 OID 0)
-- Dependencies: 230
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_platocategoria_id_platocategoria_seq OWNED BY spaapp_platocategoria.id_platocategoria;


--
-- TOC entry 233 (class 1259 OID 27294)
-- Name: spaapp_platoingrediente; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_platoingrediente (
    id_plato_ingrediente integer NOT NULL,
    requerido boolean NOT NULL,
    horafecha timestamp with time zone NOT NULL,
    id_ingrediente_id integer NOT NULL,
    id_plato_id integer NOT NULL
);


ALTER TABLE spaapp_platoingrediente OWNER TO spauser;

--
-- TOC entry 232 (class 1259 OID 27292)
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_platoingrediente_id_plato_ingrediente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_platoingrediente_id_plato_ingrediente_seq OWNER TO spauser;

--
-- TOC entry 2724 (class 0 OID 0)
-- Dependencies: 232
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_platoingrediente_id_plato_ingrediente_seq OWNED BY spaapp_platoingrediente.id_plato_ingrediente;


--
-- TOC entry 235 (class 1259 OID 27302)
-- Name: spaapp_proveedor; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_proveedor (
    id_proveedor integer NOT NULL,
    nombre_comercial character varying(100) NOT NULL,
    telefono character varying(10) NOT NULL,
    telefono_contacto character varying(10),
    prioridad integer NOT NULL,
    me_gusta integer,
    razon_social character varying(20) NOT NULL,
    id_actividad_id integer NOT NULL,
    id_categoria_id integer NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_proveedor OWNER TO spauser;

--
-- TOC entry 234 (class 1259 OID 27300)
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_proveedor_id_proveedor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_proveedor_id_proveedor_seq OWNER TO spauser;

--
-- TOC entry 2725 (class 0 OID 0)
-- Dependencies: 234
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_proveedor_id_proveedor_seq OWNED BY spaapp_proveedor.id_proveedor;


--
-- TOC entry 237 (class 1259 OID 27310)
-- Name: spaapp_redsocial; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_redsocial (
    id_red_social integer NOT NULL,
    nombre_red_social character varying(50) NOT NULL,
    cuenta_persona character varying(100) NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_redsocial OWNER TO spauser;

--
-- TOC entry 236 (class 1259 OID 27308)
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_redsocial_id_red_social_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_redsocial_id_red_social_seq OWNER TO spauser;

--
-- TOC entry 2726 (class 0 OID 0)
-- Dependencies: 236
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_redsocial_id_red_social_seq OWNED BY spaapp_redsocial.id_red_social;


--
-- TOC entry 239 (class 1259 OID 27318)
-- Name: spaapp_sucursal; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_sucursal (
    id_sucursal integer NOT NULL,
    nombre_sucursal character varying(100) NOT NULL,
    razon_social character varying(200) NOT NULL,
    telefono character varying(10) NOT NULL,
    telefono_contacto character varying(10),
    dias_atencion character varying(80) NOT NULL,
    hora_atencion character varying(50) NOT NULL,
    estado integer,
    prioridad integer NOT NULL,
    activo boolean NOT NULL,
    hora_pico time without time zone NOT NULL,
    me_gusta integer NOT NULL,
    id_ciudad_id integer NOT NULL,
    id_direccion_id integer NOT NULL,
    id_menu_id integer NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_sucursal OWNER TO spauser;

--
-- TOC entry 238 (class 1259 OID 27316)
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_sucursal_id_sucursal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_sucursal_id_sucursal_seq OWNER TO spauser;

--
-- TOC entry 2727 (class 0 OID 0)
-- Dependencies: 238
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_sucursal_id_sucursal_seq OWNED BY spaapp_sucursal.id_sucursal;


--
-- TOC entry 241 (class 1259 OID 27326)
-- Name: spaapp_tamanio; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tamanio (
    id_tamanio integer NOT NULL,
    tamanio character varying(50) NOT NULL
);


ALTER TABLE spaapp_tamanio OWNER TO spauser;

--
-- TOC entry 240 (class 1259 OID 27324)
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tamanio_id_tamanio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tamanio_id_tamanio_seq OWNER TO spauser;

--
-- TOC entry 2728 (class 0 OID 0)
-- Dependencies: 240
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tamanio_id_tamanio_seq OWNED BY spaapp_tamanio.id_tamanio;


--
-- TOC entry 243 (class 1259 OID 27334)
-- Name: spaapp_tamanioplato; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tamanioplato (
    id_tamanioplato integer NOT NULL,
    valor double precision NOT NULL,
    id_plato_id integer NOT NULL,
    id_tamanio_id integer NOT NULL
);


ALTER TABLE spaapp_tamanioplato OWNER TO spauser;

--
-- TOC entry 242 (class 1259 OID 27332)
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tamanioplato_id_tamanioplato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tamanioplato_id_tamanioplato_seq OWNER TO spauser;

--
-- TOC entry 2729 (class 0 OID 0)
-- Dependencies: 242
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tamanioplato_id_tamanioplato_seq OWNED BY spaapp_tamanioplato.id_tamanioplato;


--
-- TOC entry 245 (class 1259 OID 27342)
-- Name: spaapp_tipopago; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tipopago (
    id_tipo_pago integer NOT NULL,
    tipo_pago character varying(50) NOT NULL,
    observaciones character varying(250) NOT NULL
);


ALTER TABLE spaapp_tipopago OWNER TO spauser;

--
-- TOC entry 244 (class 1259 OID 27340)
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tipopago_id_tipo_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tipopago_id_tipo_pago_seq OWNER TO spauser;

--
-- TOC entry 2730 (class 0 OID 0)
-- Dependencies: 244
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tipopago_id_tipo_pago_seq OWNED BY spaapp_tipopago.id_tipo_pago;


--
-- TOC entry 247 (class 1259 OID 27350)
-- Name: spaapp_tipopedido; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_tipopedido (
    id_tipo_pedido integer NOT NULL,
    tipo_pedido character varying(50) NOT NULL,
    observaciones character varying(250) NOT NULL
);


ALTER TABLE spaapp_tipopedido OWNER TO spauser;

--
-- TOC entry 246 (class 1259 OID 27348)
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_tipopedido_id_tipo_pedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_tipopedido_id_tipo_pedido_seq OWNER TO spauser;

--
-- TOC entry 2731 (class 0 OID 0)
-- Dependencies: 246
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_tipopedido_id_tipo_pedido_seq OWNED BY spaapp_tipopedido.id_tipo_pedido;


--
-- TOC entry 249 (class 1259 OID 27358)
-- Name: spaapp_transportista; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_transportista (
    id_transportista integer NOT NULL,
    tipo_transporte character varying(20) NOT NULL,
    tipo_servicio character varying(50) NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_transportista OWNER TO spauser;

--
-- TOC entry 248 (class 1259 OID 27356)
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_transportista_id_transportista_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_transportista_id_transportista_seq OWNER TO spauser;

--
-- TOC entry 2732 (class 0 OID 0)
-- Dependencies: 248
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_transportista_id_transportista_seq OWNED BY spaapp_transportista.id_transportista;


--
-- TOC entry 251 (class 1259 OID 27366)
-- Name: spaapp_valoracion; Type: TABLE; Schema: spaapp; Owner: spauser
--

CREATE TABLE spaapp_valoracion (
    id_valoracion integer NOT NULL,
    valor_transportista integer NOT NULL,
    observaciones character varying(200) NOT NULL,
    valor_proveedor integer NOT NULL,
    valor_servicio integer NOT NULL,
    valoracion_general integer NOT NULL,
    id_cliente_id integer NOT NULL,
    id_pedido_id integer NOT NULL,
    id_proveedor_id integer NOT NULL,
    id_sucursal_id integer NOT NULL,
    id_transportista_id integer NOT NULL
);


ALTER TABLE spaapp_valoracion OWNER TO spauser;

--
-- TOC entry 250 (class 1259 OID 27364)
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE; Schema: spaapp; Owner: spauser
--

CREATE SEQUENCE spaapp_valoracion_id_valoracion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spaapp_valoracion_id_valoracion_seq OWNER TO spauser;

--
-- TOC entry 2733 (class 0 OID 0)
-- Dependencies: 250
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE OWNED BY; Schema: spaapp; Owner: spauser
--

ALTER SEQUENCE spaapp_valoracion_id_valoracion_seq OWNED BY spaapp_valoracion.id_valoracion;


--
-- TOC entry 2270 (class 2604 OID 27639)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY "administracionAPP_contacto" ALTER COLUMN id SET DEFAULT nextval('"administracionAPP_contacto_id_seq"'::regclass);


--
-- TOC entry 2236 (class 2604 OID 27059)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- TOC entry 2237 (class 2604 OID 27069)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 2235 (class 2604 OID 27051)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- TOC entry 2238 (class 2604 OID 27077)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- TOC entry 2239 (class 2604 OID 27087)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- TOC entry 2240 (class 2604 OID 27095)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 2268 (class 2604 OID 27614)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- TOC entry 2234 (class 2604 OID 27041)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- TOC entry 2233 (class 2604 OID 27030)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- TOC entry 2271 (class 2604 OID 27647)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_association ALTER COLUMN id SET DEFAULT nextval('social_auth_association_id_seq'::regclass);


--
-- TOC entry 2272 (class 2604 OID 27658)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code ALTER COLUMN id SET DEFAULT nextval('social_auth_code_id_seq'::regclass);


--
-- TOC entry 2273 (class 2604 OID 27666)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce ALTER COLUMN id SET DEFAULT nextval('social_auth_nonce_id_seq'::regclass);


--
-- TOC entry 2274 (class 2604 OID 27674)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth ALTER COLUMN id SET DEFAULT nextval('social_auth_usersocialauth_id_seq'::regclass);


--
-- TOC entry 2241 (class 2604 OID 27155)
-- Name: id_actividad; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_actividadcomercial ALTER COLUMN id_actividad SET DEFAULT nextval('spaapp_actividadcomercial_id_actividad_seq'::regclass);


--
-- TOC entry 2242 (class 2604 OID 27163)
-- Name: id_categoria; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_categoria ALTER COLUMN id_categoria SET DEFAULT nextval('spaapp_categoria_id_categoria_seq'::regclass);


--
-- TOC entry 2243 (class 2604 OID 27171)
-- Name: id_ciudad; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ciudad ALTER COLUMN id_ciudad SET DEFAULT nextval('spaapp_ciudad_id_ciudad_seq'::regclass);


--
-- TOC entry 2244 (class 2604 OID 27179)
-- Name: id_cliente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente ALTER COLUMN id_cliente SET DEFAULT nextval('spaapp_cliente_id_cliente_seq'::regclass);


--
-- TOC entry 2245 (class 2604 OID 27187)
-- Name: id_contrato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato ALTER COLUMN id_contrato SET DEFAULT nextval('spaapp_contrato_id_contrato_seq'::regclass);


--
-- TOC entry 2246 (class 2604 OID 27195)
-- Name: id_coordenadas; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas ALTER COLUMN id_coordenadas SET DEFAULT nextval('spaapp_coordenadas_id_coordenadas_seq'::regclass);


--
-- TOC entry 2247 (class 2604 OID 27203)
-- Name: id_detalle; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido ALTER COLUMN id_detalle SET DEFAULT nextval('spaapp_detallepedido_id_detalle_seq'::regclass);


--
-- TOC entry 2248 (class 2604 OID 27211)
-- Name: id_direccion; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion ALTER COLUMN id_direccion SET DEFAULT nextval('spaapp_direccion_id_direccion_seq'::regclass);


--
-- TOC entry 2249 (class 2604 OID 27222)
-- Name: id_horario; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista ALTER COLUMN id_horario SET DEFAULT nextval('spaapp_horariotransportista_id_horario_seq'::regclass);


--
-- TOC entry 2250 (class 2604 OID 27230)
-- Name: id_ingrediente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente ALTER COLUMN id_ingrediente SET DEFAULT nextval('spaapp_ingrediente_id_ingrediente_seq'::regclass);


--
-- TOC entry 2251 (class 2604 OID 27238)
-- Name: id_menu; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu ALTER COLUMN id_menu SET DEFAULT nextval('spaapp_menu_id_menu_seq'::regclass);


--
-- TOC entry 2252 (class 2604 OID 27246)
-- Name: id_modificardetallepedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido ALTER COLUMN id_modificardetallepedido SET DEFAULT nextval('spaapp_modificardetallepedido_id_modificardetallepedido_seq'::regclass);


--
-- TOC entry 2253 (class 2604 OID 27254)
-- Name: id_pedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido ALTER COLUMN id_pedido SET DEFAULT nextval('spaapp_pedido_id_pedido_seq'::regclass);


--
-- TOC entry 2254 (class 2604 OID 27262)
-- Name: id_perfil; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil ALTER COLUMN id_perfil SET DEFAULT nextval('spaapp_perfil_id_perfil_seq'::regclass);


--
-- TOC entry 2255 (class 2604 OID 27270)
-- Name: id_plato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato ALTER COLUMN id_plato SET DEFAULT nextval('spaapp_plato_id_plato_seq'::regclass);


--
-- TOC entry 2256 (class 2604 OID 27281)
-- Name: id; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu ALTER COLUMN id SET DEFAULT nextval('spaapp_plato_id_menu_id_seq'::regclass);


--
-- TOC entry 2257 (class 2604 OID 27289)
-- Name: id_platocategoria; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platocategoria ALTER COLUMN id_platocategoria SET DEFAULT nextval('spaapp_platocategoria_id_platocategoria_seq'::regclass);


--
-- TOC entry 2258 (class 2604 OID 27297)
-- Name: id_plato_ingrediente; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente ALTER COLUMN id_plato_ingrediente SET DEFAULT nextval('spaapp_platoingrediente_id_plato_ingrediente_seq'::regclass);


--
-- TOC entry 2259 (class 2604 OID 27305)
-- Name: id_proveedor; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor ALTER COLUMN id_proveedor SET DEFAULT nextval('spaapp_proveedor_id_proveedor_seq'::regclass);


--
-- TOC entry 2260 (class 2604 OID 27313)
-- Name: id_red_social; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial ALTER COLUMN id_red_social SET DEFAULT nextval('spaapp_redsocial_id_red_social_seq'::regclass);


--
-- TOC entry 2261 (class 2604 OID 27321)
-- Name: id_sucursal; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal ALTER COLUMN id_sucursal SET DEFAULT nextval('spaapp_sucursal_id_sucursal_seq'::regclass);


--
-- TOC entry 2262 (class 2604 OID 27329)
-- Name: id_tamanio; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanio ALTER COLUMN id_tamanio SET DEFAULT nextval('spaapp_tamanio_id_tamanio_seq'::regclass);


--
-- TOC entry 2263 (class 2604 OID 27337)
-- Name: id_tamanioplato; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato ALTER COLUMN id_tamanioplato SET DEFAULT nextval('spaapp_tamanioplato_id_tamanioplato_seq'::regclass);


--
-- TOC entry 2264 (class 2604 OID 27345)
-- Name: id_tipo_pago; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopago ALTER COLUMN id_tipo_pago SET DEFAULT nextval('spaapp_tipopago_id_tipo_pago_seq'::regclass);


--
-- TOC entry 2265 (class 2604 OID 27353)
-- Name: id_tipo_pedido; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopedido ALTER COLUMN id_tipo_pedido SET DEFAULT nextval('spaapp_tipopedido_id_tipo_pedido_seq'::regclass);


--
-- TOC entry 2266 (class 2604 OID 27361)
-- Name: id_transportista; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista ALTER COLUMN id_transportista SET DEFAULT nextval('spaapp_transportista_id_transportista_seq'::regclass);


--
-- TOC entry 2267 (class 2604 OID 27369)
-- Name: id_valoracion; Type: DEFAULT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion ALTER COLUMN id_valoracion SET DEFAULT nextval('spaapp_valoracion_id_valoracion_seq'::regclass);


--
-- TOC entry 2674 (class 0 OID 27636)
-- Dependencies: 255
-- Data for Name: administracionAPP_contacto; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY "administracionAPP_contacto" (id, nombre_propietario, apellidos, ciudad, nombre_empresa, direccion, tipo_negocio, telefono) FROM stdin;
\.


--
-- TOC entry 2734 (class 0 OID 0)
-- Dependencies: 254
-- Name: administracionAPP_contacto_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('"administracionAPP_contacto_id_seq"', 1, false);


--
-- TOC entry 2608 (class 0 OID 27056)
-- Dependencies: 189
-- Data for Name: auth_group; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- TOC entry 2735 (class 0 OID 0)
-- Dependencies: 188
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- TOC entry 2610 (class 0 OID 27066)
-- Dependencies: 191
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- TOC entry 2736 (class 0 OID 0)
-- Dependencies: 190
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- TOC entry 2606 (class 0 OID 27048)
-- Dependencies: 187
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	2	add_logentry
2	Can change log entry	2	change_logentry
3	Can delete log entry	2	delete_logentry
4	Can add permission	3	add_permission
5	Can change permission	3	change_permission
6	Can delete permission	3	delete_permission
7	Can add group	4	add_group
8	Can change group	4	change_group
9	Can delete group	4	delete_group
10	Can add user	5	add_user
11	Can change user	5	change_user
12	Can delete user	5	delete_user
13	Can add content type	6	add_contenttype
14	Can change content type	6	change_contenttype
15	Can delete content type	6	delete_contenttype
16	Can add session	7	add_session
17	Can change session	7	change_session
18	Can delete session	7	delete_session
19	Can add Actividad Comercial	8	add_actividadcomercial
20	Can change Actividad Comercial	8	change_actividadcomercial
21	Can delete Actividad Comercial	8	delete_actividadcomercial
22	Can add perfil	9	add_perfil
23	Can change perfil	9	change_perfil
24	Can delete perfil	9	delete_perfil
25	Can add categoria	10	add_categoria
26	Can change categoria	10	change_categoria
27	Can delete categoria	10	delete_categoria
28	Can add proveedor	11	add_proveedor
29	Can change proveedor	11	change_proveedor
30	Can delete proveedor	11	delete_proveedor
31	Can add ciudad	12	add_ciudad
32	Can change ciudad	12	change_ciudad
33	Can delete ciudad	12	delete_ciudad
34	Can add direccion	13	add_direccion
35	Can change direccion	13	change_direccion
36	Can delete direccion	13	delete_direccion
37	Can add cliente	14	add_cliente
38	Can change cliente	14	change_cliente
39	Can delete cliente	14	delete_cliente
40	Can add menu	15	add_menu
41	Can change menu	15	change_menu
42	Can delete menu	15	delete_menu
43	Can add sucursal	16	add_sucursal
44	Can change sucursal	16	change_sucursal
45	Can delete sucursal	16	delete_sucursal
46	Can add contrato	17	add_contrato
47	Can change contrato	17	change_contrato
48	Can delete contrato	17	delete_contrato
49	Can add transportista	18	add_transportista
50	Can change transportista	18	change_transportista
51	Can delete transportista	18	delete_transportista
52	Can add Coordenada	19	add_coordenadas
53	Can change Coordenada	19	change_coordenadas
54	Can delete Coordenada	19	delete_coordenadas
55	Can add tipo pago	20	add_tipopago
56	Can change tipo pago	20	change_tipopago
57	Can delete tipo pago	20	delete_tipopago
58	Can add tipo pedido	21	add_tipopedido
59	Can change tipo pedido	21	change_tipopedido
60	Can delete tipo pedido	21	delete_tipopedido
61	Can add pedido	22	add_pedido
62	Can change pedido	22	change_pedido
63	Can delete pedido	22	delete_pedido
64	Can add plato categoria	23	add_platocategoria
65	Can change plato categoria	23	change_platocategoria
66	Can delete plato categoria	23	delete_platocategoria
67	Can add plato	24	add_plato
68	Can change plato	24	change_plato
69	Can delete plato	24	delete_plato
70	Can add tamanio	25	add_tamanio
71	Can change tamanio	25	change_tamanio
72	Can delete tamanio	25	delete_tamanio
73	Can add tamanio plato	26	add_tamanioplato
74	Can change tamanio plato	26	change_tamanioplato
75	Can delete tamanio plato	26	delete_tamanioplato
76	Can add Detalle de pedido	27	add_detallepedido
77	Can change Detalle de pedido	27	change_detallepedido
78	Can delete Detalle de pedido	27	delete_detallepedido
79	Can add ingrediente	28	add_ingrediente
80	Can change ingrediente	28	change_ingrediente
81	Can delete ingrediente	28	delete_ingrediente
82	Can add Horario de transportista	29	add_horariotransportista
83	Can change Horario de transportista	29	change_horariotransportista
84	Can delete Horario de transportista	29	delete_horariotransportista
85	Can add modificardetallepedido	30	add_modificardetallepedido
86	Can change modificardetallepedido	30	change_modificardetallepedido
87	Can delete modificardetallepedido	30	delete_modificardetallepedido
88	Can add plato ingrediente	31	add_platoingrediente
89	Can change plato ingrediente	31	change_platoingrediente
90	Can delete plato ingrediente	31	delete_platoingrediente
91	Can add Red social	32	add_redsocial
92	Can change Red social	32	change_redsocial
93	Can delete Red social	32	delete_redsocial
94	Can add valoracion	33	add_valoracion
95	Can change valoracion	33	change_valoracion
96	Can delete valoracion	33	delete_valoracion
97	Can add contacto	34	add_contacto
98	Can change contacto	34	change_contacto
99	Can delete contacto	34	delete_contacto
100	Can add cors model	35	add_corsmodel
101	Can change cors model	35	change_corsmodel
102	Can delete cors model	35	delete_corsmodel
103	Can add user social auth	36	add_usersocialauth
104	Can change user social auth	36	change_usersocialauth
105	Can delete user social auth	36	delete_usersocialauth
106	Can add nonce	37	add_nonce
107	Can change nonce	37	change_nonce
108	Can delete nonce	37	delete_nonce
109	Can add association	38	add_association
110	Can change association	38	change_association
111	Can delete association	38	delete_association
112	Can add code	39	add_code
113	Can change code	39	change_code
114	Can delete code	39	delete_code
\.


--
-- TOC entry 2737 (class 0 OID 0)
-- Dependencies: 186
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_permission_id_seq', 114, true);


--
-- TOC entry 2612 (class 0 OID 27074)
-- Dependencies: 193
-- Data for Name: auth_user; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
12	pbkdf2_sha256$24000$HtuXmxTeRqyL$Njr1uHceH9EZxGJiFe4gXIkGCJJbwCuqkh5M1yYPwJQ=	2016-08-05 09:50:32.752293-05	f	mileiva	Milton	Leiva	mileiva@utpl.edu.ec	f	t	2016-08-01 14:33:19.44345-05
7	pbkdf2_sha256$24000$Y2i7bGayjd5J$VeG6xu3ek/0efjYyfU2RTGJugYU6rC+dOSTHzSQpReo=	\N	f	icecreamsocial				f	t	2016-07-28 20:11:05.966356-05
8	pbkdf2_sha256$24000$qNZ7WHHyykAy$b2mSftD201hNXb7dyy+usYfMa4NDyAbC4NA8UwYgH9Y=	\N	f	chamelion				f	t	2016-07-28 20:11:47.163683-05
9	pbkdf2_sha256$24000$ey7AkLLlNEzT$TtYnVywsZikZa1yFunbtK8q2xwXJ+yJLCWXR3ooS5IE=	\N	f	papalogo				f	t	2016-07-28 20:13:21.318461-05
10	pbkdf2_sha256$24000$6bgVcVaYAXwS$NU+zXJH/GgDBbl2eWk0clCxHnqyM/IsVng1kGmyqiO4=	\N	f	tonys				f	t	2016-07-28 20:14:12.452188-05
6	pbkdf2_sha256$24000$0j2DG22e2YHH$fo/V5vWPWU3iIhWUaQbJp6KoUAHlH9ethCCPyD7N7fg=	2016-07-28 20:44:49.053127-05	f	chino				f	t	2016-07-28 20:08:58.268764-05
13	pbkdf2_sha256$24000$Sz60GFfE1kLG$zH+oz3wOIk5vxngO0xey67vJUU0pPD0nYQUp1g4WnCY=	2016-08-05 18:07:30.475133-05	f	nadia	Nadia	aaaa	aaaa@hhh.com	f	t	2016-08-01 17:28:47.462339-05
2	pbkdf2_sha256$24000$VZBS5yhJk5U5$GhXFXCKGJ+XpgWULSIxDvbeUO259YcxyOtBtJaA38nc=	2016-08-05 18:07:50.913235-05	t	admin			bsbnadiapc@hotmail.com	t	t	2016-07-28 19:56:26.739528-05
14	pbkdf2_sha256$24000$bdfVfg5iZZ9h$+fr24IGm0bACx/2yE0HqDS8W2TUoi/gLNeuk9MIXuf4=	2016-08-01 18:48:42.009257-05	f	hola	Hola	Hola	aaaa@hhh.com	f	t	2016-08-01 18:48:35.863843-05
4	pbkdf2_sha256$24000$QHY0JgIxGO9D$wuYMTVHeL9UK9xtD2JbVBlWbLK54clvqJ3Obv4Iz4Is=	2016-08-05 18:23:58.583736-05	f	pizza				f	t	2016-07-28 20:05:40.76617-05
3	pbkdf2_sha256$24000$KqApxeOZFuHb$j6umz0UhXih1KqOfFr2BFm3XiPWfeRKsZ/KPE3rNarU=	2016-08-05 18:47:42.822728-05	f	transportista				f	t	2016-07-28 19:59:20.350669-05
19	pbkdf2_sha256$24000$pfB7o1M68V2N$w6wC7s6FnZAel49w/8iiaCuf5QD7z+WDkIm1sOvMsVA=	2016-08-03 15:13:16.814369-05	f	cliente3	nico	git	nico@git.com	f	t	2016-08-03 15:13:07.92167-05
16	pbkdf2_sha256$24000$7lmMQlLRKqBQ$SThCWqPoo2ib/S0qlzjhqFmkUJyo+i6WsJWWtj5+AXE=	2016-08-02 17:37:56.387539-05	f	pppp	PPPP	pPPP	aaaa@hhh.com	f	t	2016-08-01 18:59:59.33102-05
5	pbkdf2_sha256$24000$tbf3Jp4deRty$zN4o4r9lhHZk6SEN/t4iC4DnnNwg/UkREFlFqrO4ZSc=	2016-08-05 16:37:59.615144-05	f	chinito				f	t	2016-07-28 20:07:12.050627-05
15	pbkdf2_sha256$24000$cdzVFc3c83jc$YcXa4t0l8dEbmtdQ9D8jinLRihs+qcSEM80BhBlQQpo=	2016-08-05 19:08:37.250259-05	f	cliente2	Cliente	Cliente	aaaa@hhh.com	f	t	2016-08-01 18:53:25.824998-05
17	pbkdf2_sha256$24000$DAf1nJvKiWCU$zPIhLzMZCbL/52Z19+Xd268H0gxhzhrJql2Yj5yZ104=	2016-08-04 18:13:43.618189-05	f	nadia2	nnnnn	ppppp	aaaa@hhh.com	f	t	2016-08-02 16:25:32.907172-05
11	pbkdf2_sha256$24000$HrliZ4y5yUNX$fvKgnoXbuOxep2DR9fE6qicpX0UJLn3D/KONvwgjFvk=	2016-08-05 16:42:47.18455-05	f	cliente1				f	t	2016-07-28 20:46:07.635535-05
18	pbkdf2_sha256$24000$WrOICHZliOuI$CO/04wIW2F7rgpBDggBQarA7iJwSIxqMYaQpwB9qvEY=	2016-08-03 17:33:18.90328-05	f	nadia3	aaaaa	bbbbb	aaaa@hhh.com	f	t	2016-08-02 18:42:59.561961-05
\.


--
-- TOC entry 2614 (class 0 OID 27084)
-- Dependencies: 195
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- TOC entry 2738 (class 0 OID 0)
-- Dependencies: 194
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- TOC entry 2739 (class 0 OID 0)
-- Dependencies: 192
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_id_seq', 19, true);


--
-- TOC entry 2616 (class 0 OID 27092)
-- Dependencies: 197
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- TOC entry 2740 (class 0 OID 0)
-- Dependencies: 196
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- TOC entry 2672 (class 0 OID 27611)
-- Dependencies: 253
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2016-07-28 19:59:20.390682-05	3	transportista	1	Añadido.	5	2
2	2016-07-28 19:59:23.662164-05	1	1	1	Añadido.	9	2
3	2016-07-28 19:59:25.000849-05	1	1	1	Añadido.	18	2
4	2016-07-28 20:01:23.543102-05	1	Loja	1	Añadido.	12	2
5	2016-07-28 20:01:23.231307-05	2	Loja	1	Añadido.	12	2
6	2016-07-28 20:05:40.805126-05	4	pizza	1	Añadido.	5	2
7	2016-07-28 20:05:44.135955-05	2	pizza	1	Añadido.	9	2
8	2016-07-28 20:05:45.592826-05	1	Guaranda - Tulcan	1	Añadido.	13	2
9	2016-07-28 20:06:16.423005-05	1	Restaurante	1	Añadido.	8	2
10	2016-07-28 20:06:27.333526-05	1	Restaurante	1	Añadido.	10	2
11	2016-07-28 20:06:39.26334-05	1	Pizza	2	Modificado/a categoria.	10	2
12	2016-07-28 20:06:58.370523-05	1	Pizza	1	Añadido.	11	2
13	2016-07-28 20:07:03.652279-05	1	Pizza	1	Añadido.	15	2
14	2016-07-28 20:07:19.515262-05	1	Pizza	1	Añadido.	16	2
15	2016-07-28 20:07:12.08977-05	5	chinito	1	Añadido.	5	2
16	2016-07-28 20:07:16.552552-05	3	chinito	1	Añadido.	9	2
17	2016-07-28 20:07:21.834778-05	2	sucre - miguel riofrío	1	Añadido.	13	2
18	2016-07-28 20:08:00.869683-05	2	Chinito	1	Añadido.	11	2
19	2016-07-28 20:08:09.773635-05	2	Chinito	1	Añadido.	15	2
20	2016-07-28 20:08:18.043651-05	2	Chinito	1	Añadido.	16	2
21	2016-07-28 20:08:58.307718-05	6	chino	1	Añadido.	5	2
22	2016-07-28 20:09:00.811683-05	4	chino	1	Añadido.	9	2
23	2016-07-28 20:09:03.001269-05	3	Vince - Ambato	1	Añadido.	13	2
24	2016-07-28 20:09:25.161411-05	2	Comida Asiática	1	Añadido.	10	2
25	2016-07-28 20:09:59.543746-05	3	Casa China	1	Añadido.	11	2
26	2016-07-28 20:10:07.490487-05	3	Casa China	1	Añadido.	15	2
27	2016-07-28 20:10:13.315091-05	3	Casa china	1	Añadido.	16	2
28	2016-07-28 20:09:54.330835-05	3	Heladería	1	Añadido.	10	2
29	2016-07-28 20:11:05.998379-05	7	icecreamsocial	1	Añadido.	5	2
30	2016-07-28 20:11:47.204706-05	8	chamelion	1	Añadido.	5	2
31	2016-07-28 20:11:11.395078-05	5	icecreamsocial	1	Añadido.	9	2
32	2016-07-28 20:11:49.769628-05	6	chamelion	1	Añadido.	9	2
33	2016-07-28 20:11:51.730859-05	4	Latacunga - Bolivar	1	Añadido.	13	2
34	2016-07-28 20:11:15.29552-05	4	Ice Cream Social	1	Añadido.	11	2
35	2016-07-28 20:11:24.471836-05	4	Ice Cream Social	1	Añadido.	15	2
36	2016-07-28 20:11:26.747109-05	4	Ice Cream Social	1	Añadido.	16	2
37	2016-07-28 20:12:11.435858-05	4	Comida Casera	1	Añadido.	10	2
38	2016-07-28 20:12:55.152681-05	5	Chamelion	1	Añadido.	11	2
39	2016-07-28 20:13:01.261927-05	5	Chamelion	1	Añadido.	15	2
40	2016-07-28 20:13:02.643552-05	5	Chamelion	1	Añadido.	16	2
41	2016-07-28 20:12:45.508447-05	5	Comida rápida	1	Añadido.	10	2
42	2016-07-28 20:13:21.348026-05	9	papalogo	1	Añadido.	5	2
43	2016-07-28 20:13:24.568677-05	7	papalogo	1	Añadido.	9	2
44	2016-07-28 20:13:28.421589-05	6	PapaLogo	1	Añadido.	11	2
45	2016-07-28 20:14:12.495246-05	10	tonys	1	Añadido.	5	2
46	2016-07-28 20:14:16.354245-05	8	tonys	1	Añadido.	9	2
47	2016-07-28 20:14:22.382596-05	7	Tony's	1	Añadido.	11	2
48	2016-07-28 20:14:28.926592-05	6	Tony's	1	Añadido.	15	2
49	2016-07-28 20:14:30.19485-05	6	Tony's	1	Añadido.	16	2
50	2016-07-28 20:13:55.29901-05	7	PapaLogo	1	Añadido.	15	2
51	2016-07-28 20:13:57.573875-05	7	PapaLogo	1	Añadido.	16	2
52	2016-07-28 20:19:15.815955-05	1	Helados	1	Añadido.	23	2
53	2016-07-28 20:19:18.781995-05	1	Helado napolitano	1	Añadido.	24	2
54	2016-07-28 20:19:43.678705-05	2	Helado de chocolate	1	Añadido.	24	2
55	2016-07-28 20:21:43.90168-05	2	Pizza	1	Añadido.	23	2
56	2016-07-28 20:21:59.823934-05	3	Hawaiana	1	Añadido.	24	2
57	2016-07-28 20:23:00.670329-05	4	Napolitana	1	Añadido.	24	2
58	2016-07-28 20:22:23.470628-05	3	Hamburguesa	1	Añadido.	23	2
59	2016-07-28 20:22:28.21659-05	5	Hamburguesa	1	Añadido.	24	2
60	2016-07-28 20:23:19.996509-05	1	Standard	1	Añadido.	25	2
61	2016-07-28 20:23:32.471793-05	2	Mediana	1	Añadido.	25	2
62	2016-07-28 20:23:37.969029-05	3	Grande	1	Añadido.	25	2
63	2016-07-28 20:23:31.223306-05	4	Bebida	1	Añadido.	23	2
64	2016-07-28 20:24:09.793627-05	1	Helado napolitano : Standard - 1.0	1	Añadido.	26	2
65	2016-07-28 20:23:33.036534-05	6	bebida	1	Añadido.	24	2
66	2016-07-28 20:24:16.638918-05	2	Helado de chocolate : Mediana - 1.5	1	Añadido.	26	2
67	2016-07-28 20:24:24.723157-05	3	Helado de chocolate : Grande - 2.0	1	Añadido.	26	2
68	2016-07-28 20:24:07.1025-05	7	refresco	1	Añadido.	24	2
69	2016-07-28 20:24:49.665641-05	4	Normal	1	Añadido.	25	2
70	2016-07-28 20:24:56.158889-05	4	Hawaiana : Normal - 3.5	1	Añadido.	26	2
71	2016-07-28 20:25:13.03056-05	5	Hawaiana : Grande - 5.5	1	Añadido.	26	2
72	2016-07-28 20:25:21.62963-05	6	Hawaiana : Grande - 12.5	1	Añadido.	26	2
73	2016-07-28 20:25:30.330695-05	7	Napolitana : Mediana - 3.0	1	Añadido.	26	2
74	2016-07-28 20:25:42.471823-05	8	Napolitana : Grande - 12.0	1	Añadido.	26	2
75	2016-07-28 20:25:58.157964-05	9	Hamburguesa : Normal - 2.5	1	Añadido.	26	2
76	2016-07-28 20:26:53.376773-05	10	Hamburguesa : Mediana - 6.0	1	Añadido.	26	2
77	2016-07-28 20:27:05.899275-05	11	bebida : Normal - 5.0	1	Añadido.	26	2
78	2016-07-28 20:27:19.880165-05	12	bebida : Grande - 3.0	1	Añadido.	26	2
79	2016-07-28 20:27:40.220621-05	13	refresco : Grande - 4.0	1	Añadido.	26	2
80	2016-07-28 20:45:24.074742-05	3	chinito	2	No ha cambiado ningún campo.	9	2
81	2016-07-28 20:46:07.67556-05	11	cliente1	1	Añadido.	5	2
82	2016-07-28 20:46:48.647758-05	9	cliente1	1	Añadido.	9	2
83	2016-07-29 10:58:24.935239-05	1	Efectivo	1	Añadido.	20	2
84	2016-07-29 10:58:50.507783-05	2	Dinero Electrónico	1	Añadido.	20	2
85	2016-07-29 11:14:11.749621-05	1	A domicilio	1	Añadido.	21	2
86	2016-07-29 11:23:20.446587-05	2	Para recoger	1	Añadido.	21	2
87	2016-07-29 11:33:13.840393-05	4	Napolitana	2	Modificado/a ruta_foto.	24	2
88	2016-07-29 11:33:33.020604-05	5	Hamburguesa	2	Modificado/a ruta_foto.	24	2
89	2016-07-29 11:33:54.048344-05	6	Coca Cola	2	Modificado/a nombre_producto y ruta_foto.	24	2
90	2016-07-29 11:34:05.715615-05	7	Fanta naranja	2	Modificado/a nombre_producto y ruta_foto.	24	2
91	2016-07-29 11:34:42.278738-05	10	admin	1	Añadido.	9	2
92	2016-08-02 10:12:13.135851-05	1	2016-07-29 15:45:42.091906+00:00 - bueno - 5.0	3		22	2
93	2016-08-02 10:13:59.614327-05	15	2016-08-02 15:02:16+00:00 - pendiente - 3.5	3		22	2
94	2016-08-02 10:13:59.620334-05	14	2016-08-02 15:01:53+00:00 - pendiente - 15.5	3		22	2
95	2016-08-02 10:13:59.623329-05	13	2016-08-01 14:56:05+00:00 - pendiente - 4.5	3		22	2
96	2016-08-02 10:13:59.626329-05	12	2016-06-29 17:09:53+00:00 - pendiente - 16.5	3		22	2
97	2016-08-02 10:13:59.629329-05	11	2016-06-29 17:09:42+00:00 - pendiente - 11.5	3		22	2
98	2016-08-02 10:13:59.633329-05	10	2016-07-29 17:09:02+00:00 - pendiente - 34.5	3		22	2
99	2016-08-02 10:13:59.636329-05	9	2016-07-29 16:25:20+00:00 - confirmado - 8.0	3		22	2
100	2016-08-03 13:03:27.252154-05	42	cliente1	3		9	2
101	2016-08-03 13:03:27.28951-05	41	cliente1	3		9	2
102	2016-08-03 13:03:27.292022-05	9	cliente1	3		9	2
103	2016-08-03 13:03:27.294013-05	0	cliente1	3		9	2
104	2016-08-03 13:14:22.114097-05	43	cliente1	3		9	2
\.


--
-- TOC entry 2741 (class 0 OID 0)
-- Dependencies: 252
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 104, true);


--
-- TOC entry 2604 (class 0 OID 27038)
-- Dependencies: 185
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_content_type (id, app_label, model) FROM stdin;
2	admin	logentry
3	auth	permission
4	auth	group
5	auth	user
6	contenttypes	contenttype
7	sessions	session
8	spaapp	actividadcomercial
9	spaapp	perfil
10	spaapp	categoria
11	spaapp	proveedor
12	spaapp	ciudad
13	spaapp	direccion
14	spaapp	cliente
15	spaapp	menu
16	spaapp	sucursal
17	spaapp	contrato
18	spaapp	transportista
19	spaapp	coordenadas
20	spaapp	tipopago
21	spaapp	tipopedido
22	spaapp	pedido
23	spaapp	platocategoria
24	spaapp	plato
25	spaapp	tamanio
26	spaapp	tamanioplato
27	spaapp	detallepedido
28	spaapp	ingrediente
29	spaapp	horariotransportista
30	spaapp	modificardetallepedido
31	spaapp	platoingrediente
32	spaapp	redsocial
33	spaapp	valoracion
34	administracionAPP	contacto
35	corsheaders	corsmodel
36	default	usersocialauth
37	default	nonce
38	default	association
39	default	code
\.


--
-- TOC entry 2742 (class 0 OID 0)
-- Dependencies: 184
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_content_type_id_seq', 39, true);


--
-- TOC entry 2602 (class 0 OID 27027)
-- Dependencies: 183
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2016-07-28 19:51:48.787104-05
2	auth	0001_initial	2016-07-28 19:51:49.5139-05
3	spaapp	0001_initial	2016-07-28 19:51:53.27189-05
4	admin	0001_initial	2016-07-28 19:54:38.535332-05
5	admin	0002_logentry_remove_auto_add	2016-07-28 19:54:38.590942-05
6	administracionAPP	0001_initial	2016-07-28 19:54:38.653486-05
7	contenttypes	0002_remove_content_type_name	2016-07-28 19:54:38.738091-05
8	auth	0002_alter_permission_name_max_length	2016-07-28 19:54:38.76934-05
9	auth	0003_alter_user_email_max_length	2016-07-28 19:54:38.807789-05
10	auth	0004_alter_user_username_opts	2016-07-28 19:54:38.83907-05
11	auth	0005_alter_user_last_login_null	2016-07-28 19:54:38.870279-05
12	auth	0006_require_contenttypes_0002	2016-07-28 19:54:38.889376-05
13	auth	0007_alter_validators_add_error_messages	2016-07-28 19:54:38.908148-05
14	default	0001_initial	2016-07-28 19:54:39.527389-05
15	default	0002_add_related_name	2016-07-28 19:54:39.675726-05
16	default	0003_alter_email_max_length	2016-07-28 19:54:39.697855-05
17	default	0004_auto_20160423_0400	2016-07-28 19:54:39.745077-05
18	sessions	0001_initial	2016-07-28 19:54:39.876252-05
19	spaapp	0002_detallepedido_tamanio	2016-08-04 13:29:59.024192-05
\.


--
-- TOC entry 2743 (class 0 OID 0)
-- Dependencies: 182
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_migrations_id_seq', 19, true);


--
-- TOC entry 2683 (class 0 OID 27697)
-- Dependencies: 264
-- Data for Name: django_session; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
ppj6kicf3jq9qp14in8osrwxyzvyeccb	ZDA1MzA1NDA1YjY2NzZlZGRmYjQxZWQwMTA3ZWY4MTQ5MjFjN2FiZDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjIiLCJfYXV0aF91c2VyX2hhc2giOiIwYmNkMDg1OGZiM2UzMjhhZmRlZTIyZmMwMjcxYWNjMjMwZjNjZDhiIn0=	2016-08-11 19:56:47.864751-05
byyjg5tr2lz8a75b7c2mmliyrulhelmn	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:50:25.121346-05
r8m0atiszs8wyzbm0pvq6m40y3kz8s7p	ZWMyZWIwZTJiYWFlMDczYzRhODhlMWU0Y2M2YzI2MGE5YTljMzUxNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiY2QwODU4ZmIzZTMyOGFmZGVlMjJmYzAyNzFhY2MyMzBmM2NkOGIiLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-11 20:20:31.486666-05
629ms707i92p3h9lxr6l82is7mprz3pi	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-17 13:12:25.735389-05
p1ruep8o7eq076v57rzgybu0el5ocbot	Y2ZkZWQ4OTRlZDQwNTRhZjI5MDViMDE2NDFmYWJmNjM3MjJmYjNmZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-17 13:32:49.86211-05
ylf1lnnjy8djcgm278nff0s1z9ndn2z1	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:51:54.426512-05
qicye6ty655n7h5d7hadcqg0ay3wm1wt	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 15:54:26.189291-05
iqmq1opk3wwyfn1qrs1rpz8org64flh3	NDZkMTNkNjMzNWFiMjE3N2E0ZTA4MGUxYjAxMDg5OWMwZjE0YTI3Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdmMGE4Mzc3YjQxMDdlNDU4ZDQzNWNjYzdhYjgxZGIxYTg1YmNmOTgiLCJfYXV0aF91c2VyX2lkIjoiMTEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-12 09:47:28.13426-05
ll4fa4qy76k81x55mm3n6gnn4qpi35dk	NzU0NDI1NTE2ZmM2NTFkZTBjM2IxOWFjNGZlMGNmZDJkZGY5MGUxNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZDg1Yjk4ZWJjZmMzYzk4Y2MzZjBiNDI2MTA0MTAwMjA5MTI3Yjc3OSIsIl9hdXRoX3VzZXJfaWQiOiI1In0=	2016-08-12 10:26:24.92008-05
agwowo7ygokt7301dv34pdwlw0c2avqe	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:00:32.628856-05
jk1xb61xhok3bn9errznmbtripf8ziir	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:01:26.54987-05
92jp9m02v310e1dbl3id5kqcihznnu3j	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:02:37.675142-05
tef6cdhg87ijg2n3sesh6gt2fprypow5	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:04:05.819932-05
8dt7o50nmaz51q0475x5hkkcf4s58ab2	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:06:18.776876-05
ohyakvkhwhrg344rv1axf8vj33k9lvul	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:07:33.506678-05
xmqm3z1nwcshedj2j3rmrfoaqqxgo7z9	ZWY5MWMzMDczYzY5YjkzMTg1Y2ZkYTBmOTc2NjRlOTYwODkyODk1Njp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE2IiwiX2F1dGhfdXNlcl9oYXNoIjoiMjQ1MGJiYmU3ZTAxYzkwNzE0NDFlN2UwOGVkNmIyZDg1NzA0MWZmMyJ9	2016-08-15 19:00:06.442762-05
fbfmnokg6ds0n6v2b77md75kro2zxhgv	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:08:33.922357-05
69931dblja1ngvaqaablty4dic7mnekf	ZWE5MGJiZThiY2Y5Y2U0YTA2YmYyZjhkMDNiMjI2YmRlYjg1NjM5OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-12 11:46:15.503914-05
hieocm5ufi9vr9mn2uakge9nklm0or51	Y2ZkZWQ4OTRlZDQwNTRhZjI5MDViMDE2NDFmYWJmNjM3MjJmYjNmZjp7Il9hdXRoX3VzZXJfaGFzaCI6IjQyM2YxNjM0YTgxMzM3ZDg2N2VmNmQxMjg0NmNhYzg3NDFmM2U0NWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0In0=	2016-08-16 16:44:17.221614-05
c77q8j8lpupnnhfwctjq06v47dmnncib	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:09:32.99599-05
dmuyidbk4yjiar7zoekt9r8euit3yc5o	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-16 16:11:20.387611-05
60a28po4ajzfi64lcuy4yl2oaxt3exka	YjlhZTcyNmUyNjcyNWZkNjMzZWM0MmFjYWE4MzVmYzI5ZDNhNTc3Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCIsIl9hdXRoX3VzZXJfaWQiOiIxMSJ9	2016-08-16 16:13:40.970215-05
60yv4yb71z7j5gpkfcunhuhksx87ny4z	NzBkNDRkZmQwZThkMzgxNTg0NTY4ZDkxMjdiNGQ2YzBlZGQ5NWEyNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjIxMjZkNzBlY2Q4NGI4MzdlMTJkODUwMDRhNWNmYTU3YTg0YTJiMTEiLCJfYXV0aF91c2VyX2lkIjoiMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-16 18:47:39.240926-05
4i86no7b1ec4wewwws5bdgr2wpo3ans9	NThiMTUyMGQxNGEzNjNhZmI0NzdjNGYzMTk0NzQ4ZDg0OTU0N2QyZjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-16 19:41:50.740148-05
evtirjme540ojzrh7ehbiwjx22p4n2v3	ZWY3NjA3MzdkYmMwZmZlZTZjY2ZlMGE3YmU0Y2QwNGI0Yzk1YzQ1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfaWQiOiIzIn0=	2016-08-19 18:47:42.733928-05
sojderkt8zlek05izs2fftiq4g342eej	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 18:59:28.592641-05
j2g47lxwea2ad6fsycytnrokppk81s98	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-17 14:36:35.448307-05
ze1r4i88d4r26y42ds2c0jrbvelzb0cz	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:15:59.871789-05
knqzjjzntzh0fnmbqohz29v7v6681dyx	ODZkODE0MmZhM2QyOWE1ZjBlNzZmYWNjMmY0MThkNjM5YTRjZTBkNjp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9oYXNoIjoiNDIzZjE2MzRhODEzMzdkODY3ZWY2ZDEyODQ2Y2FjODc0MWYzZTQ1ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-17 09:58:36.279725-05
97f99r15y8v31uekudgu94yhbtflcvl1	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:52:37.461965-05
haohankyyn1vaqqvkdu9do5ntxl04rsb	M2JkZWMxZWJiMDRkYTJiYzE0MDc0NWY0YjJkNzgxNTAwZTQxNTQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjVmNmQyNzRjZmVmNzMyYTQ3NjlhZjIwOTk3MGVjNjQwODczZTI4MDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMyJ9	2016-08-19 15:29:30.612875-05
v2a616t4h3fou3lfmpbfodsswp94o7vm	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 18:58:05.177444-05
0n7p7dxbdvrgyfrgomt94oqbm689fqio	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-18 18:11:00.613094-05
d812mif0fza9glx9hmuc6oq3fpfxd3m3	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 19:00:13.095156-05
30sspuhkbhyasr7dmlnyvtl9c5c6ql10	ZjAxNDc5NWVkZTA1ZTE5ZjlhNDk5OGM1ZjZhYmU4ZjAyYjUzOTQ4NDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2QxOGI3ZTlkMWI3NzlkNDJmMmVjYmFhZmYwZjY4MTJlN2U1Y2FhOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-18 18:32:23.211228-05
0d4hy45xys5xh2p1p894r6xu08tgb5vx	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:16:31.408975-05
l5av3g3l9cwj8nwk76gk9ccwoeqs95b6	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:50:44.500941-05
1vbvwpxwn9juq94zofqk5k0wbkevur63	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 19:07:59.703039-05
8bkb51daf06vfgntm41fzj81squ257fk	NzBkNDRkZmQwZThkMzgxNTg0NTY4ZDkxMjdiNGQ2YzBlZGQ5NWEyNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjIxMjZkNzBlY2Q4NGI4MzdlMTJkODUwMDRhNWNmYTU3YTg0YTJiMTEiLCJfYXV0aF91c2VyX2lkIjoiMTgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-17 15:26:45.337013-05
tgqz2xgxcyrrsxgp2tqsj8av5czqcv56	M2JkZWMxZWJiMDRkYTJiYzE0MDc0NWY0YjJkNzgxNTAwZTQxNTQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjVmNmQyNzRjZmVmNzMyYTQ3NjlhZjIwOTk3MGVjNjQwODczZTI4MDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMyJ9	2016-08-19 17:17:37.144736-05
65w8gan6a3v5y504npqd4s1qw3bq0v44	MzA4NTZiZjhkNTQ4Mjc2OGM5NjUwOWY2YzFkMzgwOTE1ZmZlMGM5MTp7Il9hdXRoX3VzZXJfaWQiOiIxMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2YwYTgzNzdiNDEwN2U0NThkNDM1Y2NjN2FiODFkYjFhODViY2Y5OCJ9	2016-08-19 09:13:03.228514-05
emmf1l8cwo2dwk11qfdrxuve71bqbvr7	M2IwOTExNDNiYjI2NDViODZjZmYzZjhhYWFhMzk3MzhhODA1N2NjZjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQiLCJfYXV0aF91c2VyX2hhc2giOiI0MjNmMTYzNGE4MTMzN2Q4NjdlZjZkMTI4NDZjYWM4NzQxZjNlNDVmIn0=	2016-08-17 11:43:49.982016-05
7wv2cqf7egvxgqm3kps3cp6xy92q0unf	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:51:56.248083-05
crl9q5frwybe28s2ogu4y0q0iz56wwdu	NWJkOTFkOGM5OTE4ZTg3YzdiNjc1YmRkZDQ1Y2Q5MDA2YjdmYzI3Yzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE1IiwiX2F1dGhfdXNlcl9oYXNoIjoiOTAzNmM0NjEyODIwNWE2MDEzODA0ZDU3NDE1MmJkYzA3ZGQ0MmFmYiJ9	2016-08-19 18:52:58.365229-05
8q06zc9kxtloqn6acibaq0jk74rt130m	MDVhZjI0ZjE2MDI3Yzg2NjI2MTM0ZDRjOWQwMWMzYTRjNjExZmI2NDp7Il9hdXRoX3VzZXJfaWQiOiIxNSIsIl9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2016-08-19 18:58:05.191455-05
fmrv1a7lotqm4cggevlult885kw1i00c	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 19:08:37.261303-05
ds4i3wdenys5gn56lw2kzeqtzofvigei	NmIyZWNmNGMyZTlmZjEzMzc5OWRkYzNhMzE3NTlkNjhiYTU2MDY3Zjp7Il9hdXRoX3VzZXJfaGFzaCI6IjkwMzZjNDYxMjgyMDVhNjAxMzgwNGQ1NzQxNTJiZGMwN2RkNDJhZmIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxNSJ9	2016-08-19 17:18:28.363795-05
is5vhk4iv5sehc1168caf938c1qjwyup	MzgzY2YwYTY0NDYyMWEyOTNjYWM3MzcwNTRiMDVhNWNhZjE0NTBlYzp7Il9hdXRoX3VzZXJfaGFzaCI6ImM1MzAzOTkwYTMxNGEyN2FhODRmZGY4YjUyODk2NDEyYzI3ZTdiMjQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMiJ9	2016-08-19 09:50:32.770699-05
hj9hti1sb8jkq5h14o17yd4d293is2y3	YTU2ZmZhYjQ1MjlhYjU5YzJjYTk5YjdiYzQyM2ZjOTZjNGUyN2VmYzp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGJjZDA4NThmYjNlMzI4YWZkZWUyMmZjMDI3MWFjYzIzMGYzY2Q4YiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2016-08-19 18:07:50.929245-05
\.


--
-- TOC entry 2676 (class 0 OID 27644)
-- Dependencies: 257
-- Data for Name: social_auth_association; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_association (id, server_url, handle, secret, issued, lifetime, assoc_type) FROM stdin;
\.


--
-- TOC entry 2744 (class 0 OID 0)
-- Dependencies: 256
-- Name: social_auth_association_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_association_id_seq', 1, false);


--
-- TOC entry 2678 (class 0 OID 27655)
-- Dependencies: 259
-- Data for Name: social_auth_code; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_code (id, email, code, verified) FROM stdin;
\.


--
-- TOC entry 2745 (class 0 OID 0)
-- Dependencies: 258
-- Name: social_auth_code_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_code_id_seq', 1, false);


--
-- TOC entry 2680 (class 0 OID 27663)
-- Dependencies: 261
-- Data for Name: social_auth_nonce; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_nonce (id, server_url, "timestamp", salt) FROM stdin;
\.


--
-- TOC entry 2746 (class 0 OID 0)
-- Dependencies: 260
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_nonce_id_seq', 1, false);


--
-- TOC entry 2682 (class 0 OID 27671)
-- Dependencies: 263
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY social_auth_usersocialauth (id, provider, uid, extra_data, user_id) FROM stdin;
\.


--
-- TOC entry 2747 (class 0 OID 0)
-- Dependencies: 262
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_usersocialauth_id_seq', 1, false);


--
-- TOC entry 2618 (class 0 OID 27152)
-- Dependencies: 199
-- Data for Name: spaapp_actividadcomercial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_actividadcomercial (id_actividad, tipo_actividad, detalles) FROM stdin;
1	Restaurante	aadadasa
\.


--
-- TOC entry 2748 (class 0 OID 0)
-- Dependencies: 198
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_actividadcomercial_id_actividad_seq', 1, true);


--
-- TOC entry 2620 (class 0 OID 27160)
-- Dependencies: 201
-- Data for Name: spaapp_categoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_categoria (id_categoria, categoria, detalles) FROM stdin;
1	Pizza	dadadasd
2	Comida Asiática	dadadasd
3	Heladería	sadadsa
4	Comida Casera	dasasd
5	Comida rápida	dasdasd
\.


--
-- TOC entry 2749 (class 0 OID 0)
-- Dependencies: 200
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_categoria_id_categoria_seq', 5, true);


--
-- TOC entry 2622 (class 0 OID 27168)
-- Dependencies: 203
-- Data for Name: spaapp_ciudad; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_ciudad (id_ciudad, nombre, descripcion) FROM stdin;
1	Loja	dasda
2	Loja	asdasdas
\.


--
-- TOC entry 2750 (class 0 OID 0)
-- Dependencies: 202
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ciudad_id_ciudad_seq', 2, true);


--
-- TOC entry 2624 (class 0 OID 27176)
-- Dependencies: 205
-- Data for Name: spaapp_cliente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_cliente (id_cliente, fechainicio_suscripcion, fecha_pago, detalles_adicionales, pedidos_activos, id_perfil_id) FROM stdin;
1	2016-07-29	2016-07-29	nada	t	3
\.


--
-- TOC entry 2751 (class 0 OID 0)
-- Dependencies: 204
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_cliente_id_cliente_seq', 1, false);


--
-- TOC entry 2626 (class 0 OID 27184)
-- Dependencies: 207
-- Data for Name: spaapp_contrato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_contrato (id_contrato, codigo, detalles, vigencia, fecha_inicio, "fechaFin", valor, ruta_contrato, id_proveedor_id, id_sucursal_id) FROM stdin;
\.


--
-- TOC entry 2752 (class 0 OID 0)
-- Dependencies: 206
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_contrato_id_contrato_seq', 1, false);


--
-- TOC entry 2628 (class 0 OID 27192)
-- Dependencies: 209
-- Data for Name: spaapp_coordenadas; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_coordenadas (id_coordenadas, latitud, longitud, fecha, estado, id_perfil_id) FROM stdin;
\.


--
-- TOC entry 2753 (class 0 OID 0)
-- Dependencies: 208
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_coordenadas_id_coordenadas_seq', 1, false);


--
-- TOC entry 2630 (class 0 OID 27200)
-- Dependencies: 211
-- Data for Name: spaapp_detallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_detallepedido (id_detalle, valor, cantidad_solicitada, id_pedido_id, id_plato_id, tamanio_id) FROM stdin;
12	1	1	19	1	\N
13	1	1	20	1	\N
14	1	1	21	1	\N
15	2	1	21	2	\N
16	1	1	22	1	\N
17	1	1	23	1	\N
18	3	1	24	4	2
19	12	1	24	4	3
20	5	1	24	6	4
21	1	1	25	1	\N
\.


--
-- TOC entry 2754 (class 0 OID 0)
-- Dependencies: 210
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_detallepedido_id_detalle_seq', 21, true);


--
-- TOC entry 2632 (class 0 OID 27208)
-- Dependencies: 213
-- Data for Name: spaapp_direccion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) FROM stdin;
1	Guaranda	Tulcan		-3.9870951803235783	-79.20502334833145	coca-cola	2
2	sucre	miguel riofrío		1312312	12312312	1312321	3
3	Vince	Ambato		-3.9870951803235783	-79.20502334833145	coca-cola	4
4	Latacunga	Bolivar		-3.9870951803235783	-79.20502334833145	coca-cola	6
7	Princiapl	Secundaria	nn	-4.031867404538332	-79.20914054178733	referencia	1
\.


--
-- TOC entry 2755 (class 0 OID 0)
-- Dependencies: 212
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_direccion_id_direccion_seq', 7, true);


--
-- TOC entry 2634 (class 0 OID 27219)
-- Dependencies: 215
-- Data for Name: spaapp_horariotransportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_horariotransportista (id_horario, dias, horas, id_transportista_id) FROM stdin;
\.


--
-- TOC entry 2756 (class 0 OID 0)
-- Dependencies: 214
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_horariotransportista_id_horario_seq', 1, false);


--
-- TOC entry 2636 (class 0 OID 27227)
-- Dependencies: 217
-- Data for Name: spaapp_ingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_ingrediente (id_ingrediente, nombre) FROM stdin;
1	Arroz
2	Pescado
3	Pollo
4	Cebolla
5	Carne de res
\.


--
-- TOC entry 2757 (class 0 OID 0)
-- Dependencies: 216
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ingrediente_id_ingrediente_seq', 5, true);


--
-- TOC entry 2638 (class 0 OID 27235)
-- Dependencies: 219
-- Data for Name: spaapp_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_menu (id_menu, id_proveedor_id) FROM stdin;
1	1
2	2
3	3
4	4
5	5
6	7
7	6
\.


--
-- TOC entry 2758 (class 0 OID 0)
-- Dependencies: 218
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_menu_id_menu_seq', 7, true);


--
-- TOC entry 2640 (class 0 OID 27243)
-- Dependencies: 221
-- Data for Name: spaapp_modificardetallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_modificardetallepedido (id_modificardetallepedido, cantidad_agregar, valor_modificacion, id_ingrediente_quitar_id, id_plato_agregar_id) FROM stdin;
\.


--
-- TOC entry 2759 (class 0 OID 0)
-- Dependencies: 220
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_modificardetallepedido_id_modificardetallepedido_seq', 1, false);


--
-- TOC entry 2642 (class 0 OID 27251)
-- Dependencies: 223
-- Data for Name: spaapp_pedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_pedido (id_pedido, fecha, estado, observaciones, valor, tiempo, id_direccion_id, id_sucursal_id, id_tipo_pago_id, id_tipo_pedido_id, id_trasportista_id, usuario_id) FROM stdin;
19	2016-08-03 12:29:53-05	confirmado	Sin Comentario	2.5	1	1	1	1	1	1	4
24	2016-08-04 13:31:03-05	pendiente	Sin Comentario	21.5	\N	1	1	1	1	1	4
25	2016-08-04 15:07:15-05	pendiente	Sin Comentario	2.5	\N	1	1	1	1	1	4
20	2016-08-03 14:39:29-05	enviado	Sin Comentario	2.5	1	7	1	1	1	1	11
21	2016-08-03 14:40:33-05	enviado	Sin Comentario	4.5	3	7	1	1	1	1	11
22	2016-08-03 16:45:47-05	enviado	Sin Comentario	2.5	0	7	1	2	1	1	11
23	2016-08-03 17:35:21-05	enviado	Sin Comentario	2.5	1	7	1	2	1	1	11
\.


--
-- TOC entry 2760 (class 0 OID 0)
-- Dependencies: 222
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_pedido_id_pedido_seq', 25, true);


--
-- TOC entry 2644 (class 0 OID 27259)
-- Dependencies: 225
-- Data for Name: spaapp_perfil; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) FROM stdin;
2		\N		\N	usuarioFoto/rest2_U7l883L.jpg	14	4
4		\N		\N	usuarioFoto/rest1_GTgybUv.png	14	6
5	1122334455	2016-07-28	213131	Femenino	usuarioFoto/icecreamsocial_yj6ICi3.jpg	sdada	7
6		\N		\N	usuarioFoto/rest10_Tb8n5Vo.jpg	14	8
7	123123312	2016-07-28	1312312	Femenino	usuarioFoto/papalogo_T69w2cx_chpNt8u.jpg	sdasdsa	9
8	1212121	\N		\N	usuarioFoto/rest12_pgD61rK.png	12	10
3	1122334455	2016-03-16	1111111	Masculino	usuarioFoto/chinito_HYopzZ2.jpg	12312312	5
45	1102984765	2016-07-29	09876543	Masculino	noname	30000	3
10		\N		\N		12	2
46	1109483737	2016-08-26	25467877	Masculino	noname	7	3
44	1104857563	2016-07-28	12345	Masculino	noname	555555	3
35	1123456789	2016-01-01	123456	aaaa	aaa/aaa	aaaaa	14
36	1123456789	2016-01-01	555555	genero	foto/foto.jpg	aaaaa	15
37	1123456789	2016-01-01	555555	genero	foto/foto.jpg	aaaaa	16
39	11111	1990-05-28	13123131	femenino	asdadasdad		17
34	55555	2016-01-01	123456	\N	aaa/aaa	aaaaa	13
47	1123456789	2016-01-01	555555	genero	foto/foto.jpg	aaaaa	19
40	111111	2016-01-01	555555	\N	foto/foto.jpg	aaaaa	18
1	1103819338	2016-07-27	0984532	Masculino	noname	22222	3
48	1104322333	2000-10-10	2584179	Masculino	noname	2584179	3
\.


--
-- TOC entry 2761 (class 0 OID 0)
-- Dependencies: 224
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_perfil_id_perfil_seq', 48, true);


--
-- TOC entry 2646 (class 0 OID 27267)
-- Dependencies: 227
-- Data for Name: spaapp_plato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) FROM stdin;
1	Helado napolitano	menuFoto/helados_5hs4kl8.jpg	Helado napolitano	2	4	20:18:58	8	12	1
2	Helado de chocolate	menuFoto/he2_sL69ayP.jpg	Helado de chocolate	2	4	20:19:35	8	12	1
3	Hawaiana	menuFoto/plato6_lEA1j32.jpg	lleva jamón , piña	2	4	11:51:03	8	12	2
4	Napolitana	menuFoto/napo.jpg	La pizza napolitana puede contener anchoas, orégano, alcaparras y ajo, entre otros ingredientes.	2	4	11:51:03	8	12	2
5	Hamburguesa	menuFoto/burger.jpg	asdasd	1	1	20:22:07	2	2	3
6	Coca Cola	menuFoto/coca.jpg	qwe	3	2	20:23:19	3	3	4
7	Fanta naranja	menuFoto/fanta.jpg	adad	3	3	20:24:00	3	3	4
\.


--
-- TOC entry 2648 (class 0 OID 27278)
-- Dependencies: 229
-- Data for Name: spaapp_plato_id_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_plato_id_menu (id, plato_id, menu_id) FROM stdin;
1	1	1
2	2	1
3	3	1
4	4	1
5	5	1
6	6	1
7	7	1
\.


--
-- TOC entry 2762 (class 0 OID 0)
-- Dependencies: 228
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_menu_id_seq', 7, true);


--
-- TOC entry 2763 (class 0 OID 0)
-- Dependencies: 226
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_plato_seq', 7, true);


--
-- TOC entry 2650 (class 0 OID 27286)
-- Dependencies: 231
-- Data for Name: spaapp_platocategoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_platocategoria (id_platocategoria, categoria) FROM stdin;
1	Helados
2	Pizza
3	Hamburguesa
4	Bebida
\.


--
-- TOC entry 2764 (class 0 OID 0)
-- Dependencies: 230
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platocategoria_id_platocategoria_seq', 4, true);


--
-- TOC entry 2652 (class 0 OID 27294)
-- Dependencies: 233
-- Data for Name: spaapp_platoingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_platoingrediente (id_plato_ingrediente, requerido, horafecha, id_ingrediente_id, id_plato_id) FROM stdin;
\.


--
-- TOC entry 2765 (class 0 OID 0)
-- Dependencies: 232
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platoingrediente_id_plato_ingrediente_seq', 1, false);


--
-- TOC entry 2654 (class 0 OID 27302)
-- Dependencies: 235
-- Data for Name: spaapp_proveedor; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) FROM stdin;
1	Pizza	65821212		4	5	5216454645	1	1	2
2	Chinito	111111		1	\N	adasdasda	1	1	3
3	Casa China	1526464643		4	\N	221212	1	2	4
4	Ice Cream Social	111111		1	\N	sadasdas	1	3	5
5	Chamelion	65821212		4	\N	5216454645	1	4	6
6	PapaLogo	213123		1	\N	asdadsa	1	5	7
7	Tony's	65821212		4	\N	12	1	3	8
\.


--
-- TOC entry 2766 (class 0 OID 0)
-- Dependencies: 234
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_proveedor_id_proveedor_seq', 7, true);


--
-- TOC entry 2656 (class 0 OID 27310)
-- Dependencies: 237
-- Data for Name: spaapp_redsocial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_redsocial (id_red_social, nombre_red_social, cuenta_persona, id_proveedor_id) FROM stdin;
\.


--
-- TOC entry 2767 (class 0 OID 0)
-- Dependencies: 236
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_redsocial_id_red_social_seq', 1, false);


--
-- TOC entry 2658 (class 0 OID 27318)
-- Dependencies: 239
-- Data for Name: spaapp_sucursal; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) FROM stdin;
3	Casa china	5216454645	65821212		Lunes - Sábado	8:00 - 18:00	\N	4	f	20:08:04	5	1	3	3	3
4	Ice Cream Social	heladería	111111		7	08:00	\N	1	f	12:00:00	1	1	2	4	4
5	Chamelion	212121	1526464643		Lunes - Sábado	8:00 - 18:00	\N	4	f	20:11:15	5	1	4	5	5
6	Tony's	212121	65821212		Lunes - Sábado	8:00 - 18:00	\N	4	f	11:39:43	5	1	4	6	7
7	PapaLogo	asdadasd	dasdasda		7	08:00	\N	1	f	12:00:00	1	1	2	7	6
2	Chinito	Restaurante	12345678		7	08:00	1	1	f	12:00:00	1	1	2	2	2
1	Pizza	5216454645	65821212		Lunes - Sábado	8:00 - 18:00	0	1	f	20:07:16	5	1	1	1	1
\.


--
-- TOC entry 2768 (class 0 OID 0)
-- Dependencies: 238
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_sucursal_id_sucursal_seq', 7, true);


--
-- TOC entry 2660 (class 0 OID 27326)
-- Dependencies: 241
-- Data for Name: spaapp_tamanio; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tamanio (id_tamanio, tamanio) FROM stdin;
1	Standard
2	Mediana
3	Grande
4	Normal
\.


--
-- TOC entry 2769 (class 0 OID 0)
-- Dependencies: 240
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanio_id_tamanio_seq', 4, true);


--
-- TOC entry 2662 (class 0 OID 27334)
-- Dependencies: 243
-- Data for Name: spaapp_tamanioplato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) FROM stdin;
1	1	1	1
2	1.5	2	2
3	2	2	3
4	3.5	3	4
5	5.5	3	3
7	3	4	2
8	12	4	3
9	2.5	5	4
10	6	5	2
11	5	6	4
12	3	6	3
13	4	7	3
\.


--
-- TOC entry 2770 (class 0 OID 0)
-- Dependencies: 242
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanioplato_id_tamanioplato_seq', 13, true);


--
-- TOC entry 2664 (class 0 OID 27342)
-- Dependencies: 245
-- Data for Name: spaapp_tipopago; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tipopago (id_tipo_pago, tipo_pago, observaciones) FROM stdin;
1	Efectivo	Efectivo
2	Dinero Electrónico	Dinero Electrónico
\.


--
-- TOC entry 2771 (class 0 OID 0)
-- Dependencies: 244
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopago_id_tipo_pago_seq', 2, true);


--
-- TOC entry 2666 (class 0 OID 27350)
-- Dependencies: 247
-- Data for Name: spaapp_tipopedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_tipopedido (id_tipo_pedido, tipo_pedido, observaciones) FROM stdin;
1	A domicilio	A domicilio
2	Para recoger	Para recoger
\.


--
-- TOC entry 2772 (class 0 OID 0)
-- Dependencies: 246
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopedido_id_tipo_pedido_seq', 2, true);


--
-- TOC entry 2668 (class 0 OID 27358)
-- Dependencies: 249
-- Data for Name: spaapp_transportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_transportista (id_transportista, tipo_transporte, tipo_servicio, id_perfil_id) FROM stdin;
1	Moto	Entrega	1
8	MotoTaxi	Entrega	48
6	Taxi	Entrega	45
7	Taxi	Entrega	46
5	Moto	Servicio	44
\.


--
-- TOC entry 2773 (class 0 OID 0)
-- Dependencies: 248
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_transportista_id_transportista_seq', 8, true);


--
-- TOC entry 2670 (class 0 OID 27366)
-- Dependencies: 251
-- Data for Name: spaapp_valoracion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

COPY spaapp_valoracion (id_valoracion, valor_transportista, observaciones, valor_proveedor, valor_servicio, valoracion_general, id_cliente_id, id_pedido_id, id_proveedor_id, id_sucursal_id, id_transportista_id) FROM stdin;
1	2	ninguna	2	5	3	1	19	1	1	1
6	2	ninguna	2	5	5	1	19	1	5	1
8	2	ninguna	2	5	4	1	19	1	6	1
10	2	ninguna	2	5	4	1	19	1	7	1
11	2	ninguna	2	5	4	1	19	1	1	1
15	2	ninguna	2	5	5	1	19	1	4	1
16	2	ninguna	2	5	5	1	19	1	7	1
2	2	ninguna	2	5	5	1	19	2	2	1
3	2	ninguna	2	5	1	1	19	3	3	1
7	2	ninguna	2	5	4	1	19	4	5	1
12	2	ninguna	2	5	2	1	19	6	2	1
4	2	ninguna	2	5	3	1	19	2	4	1
13	2	ninguna	2	5	5	1	19	2	2	1
9	2	ninguna	2	5	3	1	19	5	7	1
5	2	ninguna	2	5	4	1	19	3	5	1
14	2	ninguna	2	5	3	1	19	7	1	1
\.


--
-- TOC entry 2774 (class 0 OID 0)
-- Dependencies: 250
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_valoracion_id_valoracion_seq', 1, false);


--
-- TOC entry 2415 (class 2606 OID 27641)
-- Name: administracionAPP_contacto_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY "administracionAPP_contacto"
    ADD CONSTRAINT "administracionAPP_contacto_pkey" PRIMARY KEY (id);


--
-- TOC entry 2288 (class 2606 OID 27063)
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 2294 (class 2606 OID 27118)
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 2296 (class 2606 OID 27071)
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2290 (class 2606 OID 27061)
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 2283 (class 2606 OID 27104)
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 2285 (class 2606 OID 27053)
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 2305 (class 2606 OID 27089)
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 2307 (class 2606 OID 27133)
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- TOC entry 2298 (class 2606 OID 27079)
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2311 (class 2606 OID 27097)
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2313 (class 2606 OID 27147)
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- TOC entry 2301 (class 2606 OID 27081)
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 2413 (class 2606 OID 27620)
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2278 (class 2606 OID 27045)
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 2280 (class 2606 OID 27043)
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 2276 (class 2606 OID 27035)
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2435 (class 2606 OID 27704)
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 2417 (class 2606 OID 27652)
-- Name: social_auth_association_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_association
    ADD CONSTRAINT social_auth_association_pkey PRIMARY KEY (id);


--
-- TOC entry 2421 (class 2606 OID 27695)
-- Name: social_auth_code_email_801b2d02_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code
    ADD CONSTRAINT social_auth_code_email_801b2d02_uniq UNIQUE (email, code);


--
-- TOC entry 2423 (class 2606 OID 27660)
-- Name: social_auth_code_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_code
    ADD CONSTRAINT social_auth_code_pkey PRIMARY KEY (id);


--
-- TOC entry 2425 (class 2606 OID 27668)
-- Name: social_auth_nonce_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_pkey PRIMARY KEY (id);


--
-- TOC entry 2427 (class 2606 OID 27685)
-- Name: social_auth_nonce_server_url_f6284463_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_server_url_f6284463_uniq UNIQUE (server_url, "timestamp", salt);


--
-- TOC entry 2430 (class 2606 OID 27679)
-- Name: social_auth_usersocialauth_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_pkey PRIMARY KEY (id);


--
-- TOC entry 2432 (class 2606 OID 27681)
-- Name: social_auth_usersocialauth_provider_e6b5e668_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_provider_e6b5e668_uniq UNIQUE (provider, uid);


--
-- TOC entry 2315 (class 2606 OID 27157)
-- Name: spaapp_actividadcomercial_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_actividadcomercial
    ADD CONSTRAINT spaapp_actividadcomercial_pkey PRIMARY KEY (id_actividad);


--
-- TOC entry 2317 (class 2606 OID 27165)
-- Name: spaapp_categoria_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_categoria
    ADD CONSTRAINT spaapp_categoria_pkey PRIMARY KEY (id_categoria);


--
-- TOC entry 2319 (class 2606 OID 27173)
-- Name: spaapp_ciudad_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ciudad
    ADD CONSTRAINT spaapp_ciudad_pkey PRIMARY KEY (id_ciudad);


--
-- TOC entry 2322 (class 2606 OID 27181)
-- Name: spaapp_cliente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente
    ADD CONSTRAINT spaapp_cliente_pkey PRIMARY KEY (id_cliente);


--
-- TOC entry 2326 (class 2606 OID 27189)
-- Name: spaapp_contrato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaapp_contrato_pkey PRIMARY KEY (id_contrato);


--
-- TOC entry 2329 (class 2606 OID 27197)
-- Name: spaapp_coordenadas_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas
    ADD CONSTRAINT spaapp_coordenadas_pkey PRIMARY KEY (id_coordenadas);


--
-- TOC entry 2334 (class 2606 OID 27205)
-- Name: spaapp_detallepedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detallepedido_pkey PRIMARY KEY (id_detalle);


--
-- TOC entry 2337 (class 2606 OID 27216)
-- Name: spaapp_direccion_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion
    ADD CONSTRAINT spaapp_direccion_pkey PRIMARY KEY (id_direccion);


--
-- TOC entry 2340 (class 2606 OID 27224)
-- Name: spaapp_horariotransportista_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista
    ADD CONSTRAINT spaapp_horariotransportista_pkey PRIMARY KEY (id_horario);


--
-- TOC entry 2342 (class 2606 OID 27232)
-- Name: spaapp_ingrediente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente
    ADD CONSTRAINT spaapp_ingrediente_pkey PRIMARY KEY (id_ingrediente);


--
-- TOC entry 2345 (class 2606 OID 27240)
-- Name: spaapp_menu_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu
    ADD CONSTRAINT spaapp_menu_pkey PRIMARY KEY (id_menu);


--
-- TOC entry 2349 (class 2606 OID 27248)
-- Name: spaapp_modificardetallepedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT spaapp_modificardetallepedido_pkey PRIMARY KEY (id_modificardetallepedido);


--
-- TOC entry 2357 (class 2606 OID 27256)
-- Name: spaapp_pedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_pedido_pkey PRIMARY KEY (id_pedido);


--
-- TOC entry 2360 (class 2606 OID 27264)
-- Name: spaapp_perfil_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil
    ADD CONSTRAINT spaapp_perfil_pkey PRIMARY KEY (id_perfil);


--
-- TOC entry 2367 (class 2606 OID 27283)
-- Name: spaapp_plato_id_menu_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_pkey PRIMARY KEY (id);


--
-- TOC entry 2369 (class 2606 OID 27401)
-- Name: spaapp_plato_id_menu_plato_id_a7b45c73_uniq; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_plato_id_a7b45c73_uniq UNIQUE (plato_id, menu_id);


--
-- TOC entry 2363 (class 2606 OID 27275)
-- Name: spaapp_plato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato
    ADD CONSTRAINT spaapp_plato_pkey PRIMARY KEY (id_plato);


--
-- TOC entry 2371 (class 2606 OID 27291)
-- Name: spaapp_platocategoria_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platocategoria
    ADD CONSTRAINT spaapp_platocategoria_pkey PRIMARY KEY (id_platocategoria);


--
-- TOC entry 2375 (class 2606 OID 27299)
-- Name: spaapp_platoingrediente_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT spaapp_platoingrediente_pkey PRIMARY KEY (id_plato_ingrediente);


--
-- TOC entry 2380 (class 2606 OID 27307)
-- Name: spaapp_proveedor_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveedor_pkey PRIMARY KEY (id_proveedor);


--
-- TOC entry 2383 (class 2606 OID 27315)
-- Name: spaapp_redsocial_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial
    ADD CONSTRAINT spaapp_redsocial_pkey PRIMARY KEY (id_red_social);


--
-- TOC entry 2389 (class 2606 OID 27323)
-- Name: spaapp_sucursal_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursal_pkey PRIMARY KEY (id_sucursal);


--
-- TOC entry 2391 (class 2606 OID 27331)
-- Name: spaapp_tamanio_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanio
    ADD CONSTRAINT spaapp_tamanio_pkey PRIMARY KEY (id_tamanio);


--
-- TOC entry 2395 (class 2606 OID 27339)
-- Name: spaapp_tamanioplato_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tamanioplato_pkey PRIMARY KEY (id_tamanioplato);


--
-- TOC entry 2397 (class 2606 OID 27347)
-- Name: spaapp_tipopago_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopago
    ADD CONSTRAINT spaapp_tipopago_pkey PRIMARY KEY (id_tipo_pago);


--
-- TOC entry 2399 (class 2606 OID 27355)
-- Name: spaapp_tipopedido_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopedido
    ADD CONSTRAINT spaapp_tipopedido_pkey PRIMARY KEY (id_tipo_pedido);


--
-- TOC entry 2402 (class 2606 OID 27363)
-- Name: spaapp_transportista_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista
    ADD CONSTRAINT spaapp_transportista_pkey PRIMARY KEY (id_transportista);


--
-- TOC entry 2409 (class 2606 OID 27371)
-- Name: spaapp_valoracion_pkey; Type: CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valoracion_pkey PRIMARY KEY (id_valoracion);


--
-- TOC entry 2286 (class 1259 OID 27106)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 2291 (class 1259 OID 27119)
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- TOC entry 2292 (class 1259 OID 27120)
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- TOC entry 2281 (class 1259 OID 27105)
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- TOC entry 2302 (class 1259 OID 27135)
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- TOC entry 2303 (class 1259 OID 27134)
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- TOC entry 2308 (class 1259 OID 27149)
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 2309 (class 1259 OID 27148)
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 2299 (class 1259 OID 27121)
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 2410 (class 1259 OID 27631)
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- TOC entry 2411 (class 1259 OID 27632)
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- TOC entry 2433 (class 1259 OID 27705)
-- Name: django_session_de54fa62; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- TOC entry 2436 (class 1259 OID 27706)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 2418 (class 1259 OID 27686)
-- Name: social_auth_code_c1336794; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_code_c1336794 ON social_auth_code USING btree (code);


--
-- TOC entry 2419 (class 1259 OID 27687)
-- Name: social_auth_code_code_a2393167_like; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_code_code_a2393167_like ON social_auth_code USING btree (code varchar_pattern_ops);


--
-- TOC entry 2428 (class 1259 OID 27693)
-- Name: social_auth_usersocialauth_e8701ad4; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX social_auth_usersocialauth_e8701ad4 ON social_auth_usersocialauth USING btree (user_id);


--
-- TOC entry 2320 (class 1259 OID 27602)
-- Name: spaapp_cliente_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_cliente_7d3abc31 ON spaapp_cliente USING btree (id_perfil_id);


--
-- TOC entry 2323 (class 1259 OID 27590)
-- Name: spaapp_contrato_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_contrato_ca253e0c ON spaapp_contrato USING btree (id_proveedor_id);


--
-- TOC entry 2324 (class 1259 OID 27596)
-- Name: spaapp_contrato_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_contrato_e163c0f9 ON spaapp_contrato USING btree (id_sucursal_id);


--
-- TOC entry 2327 (class 1259 OID 27584)
-- Name: spaapp_coordenadas_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_coordenadas_7d3abc31 ON spaapp_coordenadas USING btree (id_perfil_id);


--
-- TOC entry 2330 (class 1259 OID 33460)
-- Name: spaapp_detallepedido_55ef5a99; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_55ef5a99 ON spaapp_detallepedido USING btree (tamanio_id);


--
-- TOC entry 2331 (class 1259 OID 27578)
-- Name: spaapp_detallepedido_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_8690b5df ON spaapp_detallepedido USING btree (id_plato_id);


--
-- TOC entry 2332 (class 1259 OID 27572)
-- Name: spaapp_detallepedido_9b93490d; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_9b93490d ON spaapp_detallepedido USING btree (id_pedido_id);


--
-- TOC entry 2335 (class 1259 OID 27566)
-- Name: spaapp_direccion_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_direccion_7d3abc31 ON spaapp_direccion USING btree (id_perfil_id);


--
-- TOC entry 2338 (class 1259 OID 27560)
-- Name: spaapp_horariotransportista_113016bf; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_horariotransportista_113016bf ON spaapp_horariotransportista USING btree (id_transportista_id);


--
-- TOC entry 2343 (class 1259 OID 27554)
-- Name: spaapp_menu_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_menu_ca253e0c ON spaapp_menu USING btree (id_proveedor_id);


--
-- TOC entry 2346 (class 1259 OID 27377)
-- Name: spaapp_modificardetallepedido_99518874; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_modificardetallepedido_99518874 ON spaapp_modificardetallepedido USING btree (id_ingrediente_quitar_id);


--
-- TOC entry 2347 (class 1259 OID 27548)
-- Name: spaapp_modificardetallepedido_ac281bad; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_modificardetallepedido_ac281bad ON spaapp_modificardetallepedido USING btree (id_plato_agregar_id);


--
-- TOC entry 2350 (class 1259 OID 27530)
-- Name: spaapp_pedido_21740aad; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_21740aad ON spaapp_pedido USING btree (id_tipo_pedido_id);


--
-- TOC entry 2351 (class 1259 OID 27383)
-- Name: spaapp_pedido_9639f13b; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_9639f13b ON spaapp_pedido USING btree (id_direccion_id);


--
-- TOC entry 2352 (class 1259 OID 27542)
-- Name: spaapp_pedido_abfe0f96; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_abfe0f96 ON spaapp_pedido USING btree (usuario_id);


--
-- TOC entry 2353 (class 1259 OID 27524)
-- Name: spaapp_pedido_ccfa12af; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_ccfa12af ON spaapp_pedido USING btree (id_tipo_pago_id);


--
-- TOC entry 2354 (class 1259 OID 27518)
-- Name: spaapp_pedido_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_e163c0f9 ON spaapp_pedido USING btree (id_sucursal_id);


--
-- TOC entry 2355 (class 1259 OID 27536)
-- Name: spaapp_pedido_f09d4acb; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_pedido_f09d4acb ON spaapp_pedido USING btree (id_trasportista_id);


--
-- TOC entry 2358 (class 1259 OID 27389)
-- Name: spaapp_perfil_76a74f43; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_perfil_76a74f43 ON spaapp_perfil USING btree (id_usuario_id);


--
-- TOC entry 2361 (class 1259 OID 27512)
-- Name: spaapp_plato_06d53be8; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_06d53be8 ON spaapp_plato USING btree (id_platocategoria_id);


--
-- TOC entry 2364 (class 1259 OID 27402)
-- Name: spaapp_plato_id_menu_20104dab; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_id_menu_20104dab ON spaapp_plato_id_menu USING btree (plato_id);


--
-- TOC entry 2365 (class 1259 OID 27403)
-- Name: spaapp_plato_id_menu_93e25458; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_plato_id_menu_93e25458 ON spaapp_plato_id_menu USING btree (menu_id);


--
-- TOC entry 2372 (class 1259 OID 27414)
-- Name: spaapp_platoingrediente_4b077a6c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_platoingrediente_4b077a6c ON spaapp_platoingrediente USING btree (id_ingrediente_id);


--
-- TOC entry 2373 (class 1259 OID 27415)
-- Name: spaapp_platoingrediente_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_platoingrediente_8690b5df ON spaapp_platoingrediente USING btree (id_plato_id);


--
-- TOC entry 2376 (class 1259 OID 27432)
-- Name: spaapp_proveedor_0e29f854; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_0e29f854 ON spaapp_proveedor USING btree (id_categoria_id);


--
-- TOC entry 2377 (class 1259 OID 27433)
-- Name: spaapp_proveedor_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_7d3abc31 ON spaapp_proveedor USING btree (id_perfil_id);


--
-- TOC entry 2378 (class 1259 OID 27431)
-- Name: spaapp_proveedor_c1f5aad7; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_proveedor_c1f5aad7 ON spaapp_proveedor USING btree (id_actividad_id);


--
-- TOC entry 2381 (class 1259 OID 27439)
-- Name: spaapp_redsocial_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_redsocial_ca253e0c ON spaapp_redsocial USING btree (id_proveedor_id);


--
-- TOC entry 2384 (class 1259 OID 27461)
-- Name: spaapp_sucursal_9639f13b; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_9639f13b ON spaapp_sucursal USING btree (id_direccion_id);


--
-- TOC entry 2385 (class 1259 OID 27460)
-- Name: spaapp_sucursal_b1a01987; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_b1a01987 ON spaapp_sucursal USING btree (id_ciudad_id);


--
-- TOC entry 2386 (class 1259 OID 27463)
-- Name: spaapp_sucursal_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_ca253e0c ON spaapp_sucursal USING btree (id_proveedor_id);


--
-- TOC entry 2387 (class 1259 OID 27462)
-- Name: spaapp_sucursal_e1161949; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_sucursal_e1161949 ON spaapp_sucursal USING btree (id_menu_id);


--
-- TOC entry 2392 (class 1259 OID 27475)
-- Name: spaapp_tamanioplato_05d6b9c9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_tamanioplato_05d6b9c9 ON spaapp_tamanioplato USING btree (id_tamanio_id);


--
-- TOC entry 2393 (class 1259 OID 27474)
-- Name: spaapp_tamanioplato_8690b5df; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_tamanioplato_8690b5df ON spaapp_tamanioplato USING btree (id_plato_id);


--
-- TOC entry 2400 (class 1259 OID 27481)
-- Name: spaapp_transportista_7d3abc31; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_transportista_7d3abc31 ON spaapp_transportista USING btree (id_perfil_id);


--
-- TOC entry 2403 (class 1259 OID 27511)
-- Name: spaapp_valoracion_113016bf; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_113016bf ON spaapp_valoracion USING btree (id_transportista_id);


--
-- TOC entry 2404 (class 1259 OID 27507)
-- Name: spaapp_valoracion_6d98a18e; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_6d98a18e ON spaapp_valoracion USING btree (id_cliente_id);


--
-- TOC entry 2405 (class 1259 OID 27508)
-- Name: spaapp_valoracion_9b93490d; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_9b93490d ON spaapp_valoracion USING btree (id_pedido_id);


--
-- TOC entry 2406 (class 1259 OID 27509)
-- Name: spaapp_valoracion_ca253e0c; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_ca253e0c ON spaapp_valoracion USING btree (id_proveedor_id);


--
-- TOC entry 2407 (class 1259 OID 27510)
-- Name: spaapp_valoracion_e163c0f9; Type: INDEX; Schema: spaapp; Owner: spauser
--

CREATE INDEX spaapp_valoracion_e163c0f9 ON spaapp_valoracion USING btree (id_sucursal_id);


--
-- TOC entry 2452 (class 2606 OID 27561)
-- Name: D16bd3bac72562dcaa9ab5f9f6fb85ae; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista
    ADD CONSTRAINT "D16bd3bac72562dcaa9ab5f9f6fb85ae" FOREIGN KEY (id_transportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2483 (class 2606 OID 27502)
-- Name: D893b6353e00f54fed4819335a2ca882; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT "D893b6353e00f54fed4819335a2ca882" FOREIGN KEY (id_transportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2468 (class 2606 OID 27416)
-- Name: D91d9aaf1a328bd31979d1581c55c5ec; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT "D91d9aaf1a328bd31979d1581c55c5ec" FOREIGN KEY (id_actividad_id) REFERENCES spaapp_actividadcomercial(id_actividad) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2460 (class 2606 OID 27537)
-- Name: D944537a2d489f0d21d6406b48ddb641; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT "D944537a2d489f0d21d6406b48ddb641" FOREIGN KEY (id_trasportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2439 (class 2606 OID 27112)
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2438 (class 2606 OID 27107)
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2437 (class 2606 OID 27098)
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2441 (class 2606 OID 27127)
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2440 (class 2606 OID 27122)
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2443 (class 2606 OID 27141)
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2442 (class 2606 OID 27136)
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2466 (class 2606 OID 27404)
-- Name: ba7c74bed2241fbcd786c213851660dc; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT ba7c74bed2241fbcd786c213851660dc FOREIGN KEY (id_ingrediente_id) REFERENCES spaapp_ingrediente(id_ingrediente) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2463 (class 2606 OID 27513)
-- Name: dce350061342863e912703a9dafef09e; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato
    ADD CONSTRAINT dce350061342863e912703a9dafef09e FOREIGN KEY (id_platocategoria_id) REFERENCES spaapp_platocategoria(id_platocategoria) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2484 (class 2606 OID 27621)
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2485 (class 2606 OID 27626)
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2454 (class 2606 OID 27372)
-- Name: e7cb8922641d3a71e0518cc63d3617b2; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT e7cb8922641d3a71e0518cc63d3617b2 FOREIGN KEY (id_ingrediente_quitar_id) REFERENCES spaapp_ingrediente(id_ingrediente) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2459 (class 2606 OID 27531)
-- Name: id_tipo_pedido_id_cde83768_fk_spaapp_tipopedido_id_tipo_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT id_tipo_pedido_id_cde83768_fk_spaapp_tipopedido_id_tipo_pedido FOREIGN KEY (id_tipo_pedido_id) REFERENCES spaapp_tipopedido(id_tipo_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2486 (class 2606 OID 27688)
-- Name: social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2450 (class 2606 OID 33461)
-- Name: spaa_tamanio_id_8427c076_fk_spaapp_tamanioplato_id_tamanioplato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaa_tamanio_id_8427c076_fk_spaapp_tamanioplato_id_tamanioplato FOREIGN KEY (tamanio_id) REFERENCES spaapp_tamanioplato(id_tamanioplato) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2469 (class 2606 OID 27421)
-- Name: spaap_id_categoria_id_d672ac86_fk_spaapp_categoria_id_categoria; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaap_id_categoria_id_d672ac86_fk_spaapp_categoria_id_categoria FOREIGN KEY (id_categoria_id) REFERENCES spaapp_categoria(id_categoria) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2473 (class 2606 OID 27445)
-- Name: spaap_id_direccion_id_80462fd2_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaap_id_direccion_id_80462fd2_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2456 (class 2606 OID 27378)
-- Name: spaap_id_direccion_id_e2c4fbe4_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaap_id_direccion_id_e2c4fbe4_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2445 (class 2606 OID 27591)
-- Name: spaap_id_proveedor_id_11675f72_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaap_id_proveedor_id_11675f72_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2471 (class 2606 OID 27434)
-- Name: spaap_id_proveedor_id_18c32e0e_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial
    ADD CONSTRAINT spaap_id_proveedor_id_18c32e0e_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2475 (class 2606 OID 27455)
-- Name: spaap_id_proveedor_id_540b8b72_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaap_id_proveedor_id_540b8b72_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2481 (class 2606 OID 27492)
-- Name: spaap_id_proveedor_id_66760433_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaap_id_proveedor_id_66760433_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2453 (class 2606 OID 27555)
-- Name: spaap_id_proveedor_id_e39d0124_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu
    ADD CONSTRAINT spaap_id_proveedor_id_e39d0124_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2446 (class 2606 OID 27597)
-- Name: spaapp_c_id_sucursal_id_54ec1010_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaapp_c_id_sucursal_id_54ec1010_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2444 (class 2606 OID 27603)
-- Name: spaapp_cliente_id_perfil_id_6a66d1dd_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente
    ADD CONSTRAINT spaapp_cliente_id_perfil_id_6a66d1dd_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2447 (class 2606 OID 27585)
-- Name: spaapp_coorden_id_perfil_id_40dcc2a5_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas
    ADD CONSTRAINT spaapp_coorden_id_perfil_id_40dcc2a5_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2448 (class 2606 OID 27573)
-- Name: spaapp_detalle_id_pedido_id_d433e145_fk_spaapp_pedido_id_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detalle_id_pedido_id_d433e145_fk_spaapp_pedido_id_pedido FOREIGN KEY (id_pedido_id) REFERENCES spaapp_pedido(id_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2449 (class 2606 OID 27579)
-- Name: spaapp_detalleped_id_plato_id_5455a09f_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detalleped_id_plato_id_5455a09f_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2451 (class 2606 OID 27567)
-- Name: spaapp_direcci_id_perfil_id_ff0b3f89_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion
    ADD CONSTRAINT spaapp_direcci_id_perfil_id_ff0b3f89_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2458 (class 2606 OID 27525)
-- Name: spaapp_id_tipo_pago_id_1cbd151a_fk_spaapp_tipopago_id_tipo_pago; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_id_tipo_pago_id_1cbd151a_fk_spaapp_tipopago_id_tipo_pago FOREIGN KEY (id_tipo_pago_id) REFERENCES spaapp_tipopago(id_tipo_pago) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2455 (class 2606 OID 27549)
-- Name: spaapp_mo_id_plato_agregar_id_90134a8a_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_modificardetallepedido
    ADD CONSTRAINT spaapp_mo_id_plato_agregar_id_90134a8a_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_agregar_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2457 (class 2606 OID 27519)
-- Name: spaapp_p_id_sucursal_id_b6e7aa23_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_p_id_sucursal_id_b6e7aa23_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2461 (class 2606 OID 27543)
-- Name: spaapp_pedido_usuario_id_03fad4c7_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_pedido_usuario_id_03fad4c7_fk_auth_user_id FOREIGN KEY (usuario_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2462 (class 2606 OID 27384)
-- Name: spaapp_perfil_id_usuario_id_ecaf41b3_fk_auth_user_id; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil
    ADD CONSTRAINT spaapp_perfil_id_usuario_id_ecaf41b3_fk_auth_user_id FOREIGN KEY (id_usuario_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2465 (class 2606 OID 27395)
-- Name: spaapp_plato_id_menu_menu_id_f192d1d8_fk_spaapp_menu_id_menu; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_menu_id_f192d1d8_fk_spaapp_menu_id_menu FOREIGN KEY (menu_id) REFERENCES spaapp_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2464 (class 2606 OID 27390)
-- Name: spaapp_plato_id_menu_plato_id_1bbe6660_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_plato_id_menu
    ADD CONSTRAINT spaapp_plato_id_menu_plato_id_1bbe6660_fk_spaapp_plato_id_plato FOREIGN KEY (plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2467 (class 2606 OID 27409)
-- Name: spaapp_platoingre_id_plato_id_fe44a1d9_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_platoingrediente
    ADD CONSTRAINT spaapp_platoingre_id_plato_id_fe44a1d9_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2470 (class 2606 OID 27426)
-- Name: spaapp_proveed_id_perfil_id_fff821d0_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveed_id_perfil_id_fff821d0_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2472 (class 2606 OID 27440)
-- Name: spaapp_sucursa_id_ciudad_id_f5184cf6_fk_spaapp_ciudad_id_ciudad; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursa_id_ciudad_id_f5184cf6_fk_spaapp_ciudad_id_ciudad FOREIGN KEY (id_ciudad_id) REFERENCES spaapp_ciudad(id_ciudad) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2474 (class 2606 OID 27450)
-- Name: spaapp_sucursal_id_menu_id_81f57738_fk_spaapp_menu_id_menu; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursal_id_menu_id_81f57738_fk_spaapp_menu_id_menu FOREIGN KEY (id_menu_id) REFERENCES spaapp_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2477 (class 2606 OID 27469)
-- Name: spaapp_tama_id_tamanio_id_a9692104_fk_spaapp_tamanio_id_tamanio; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tama_id_tamanio_id_a9692104_fk_spaapp_tamanio_id_tamanio FOREIGN KEY (id_tamanio_id) REFERENCES spaapp_tamanio(id_tamanio) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2476 (class 2606 OID 27464)
-- Name: spaapp_tamaniopla_id_plato_id_5f8f9ca8_fk_spaapp_plato_id_plato; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_tamanioplato
    ADD CONSTRAINT spaapp_tamaniopla_id_plato_id_5f8f9ca8_fk_spaapp_plato_id_plato FOREIGN KEY (id_plato_id) REFERENCES spaapp_plato(id_plato) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2478 (class 2606 OID 27476)
-- Name: spaapp_transpo_id_perfil_id_9a457afd_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista
    ADD CONSTRAINT spaapp_transpo_id_perfil_id_9a457afd_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2482 (class 2606 OID 27497)
-- Name: spaapp_v_id_sucursal_id_a7f67c7c_fk_spaapp_sucursal_id_sucursal; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_v_id_sucursal_id_a7f67c7c_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2479 (class 2606 OID 27482)
-- Name: spaapp_valo_id_cliente_id_a7d43e7d_fk_spaapp_cliente_id_cliente; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valo_id_cliente_id_a7d43e7d_fk_spaapp_cliente_id_cliente FOREIGN KEY (id_cliente_id) REFERENCES spaapp_cliente(id_cliente) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2480 (class 2606 OID 27487)
-- Name: spaapp_valorac_id_pedido_id_119ed90a_fk_spaapp_pedido_id_pedido; Type: FK CONSTRAINT; Schema: spaapp; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valorac_id_pedido_id_119ed90a_fk_spaapp_pedido_id_pedido FOREIGN KEY (id_pedido_id) REFERENCES spaapp_pedido(id_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 2690 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2691 (class 0 OID 0)
-- Dependencies: 8
-- Name: spaapp; Type: ACL; Schema: -; Owner: spauser
--

REVOKE ALL ON SCHEMA spaapp FROM PUBLIC;
REVOKE ALL ON SCHEMA spaapp FROM spauser;
GRANT ALL ON SCHEMA spaapp TO spauser;
GRANT ALL ON SCHEMA spaapp TO postgres;


-- Completed on 2016-08-05 19:31:25

--
-- PostgreSQL database dump complete
--

