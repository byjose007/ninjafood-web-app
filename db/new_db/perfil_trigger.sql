CREATE OR REPLACE FUNCTION spaapp.insertar_perfil() RETURNS TRIGGER AS $$
	BEGIN
		INSERT INTO spaapp_perfil (id_usuario_id,ruta_foto, sim_dispositivo) VALUES (NEW.id,'usuarioFoto/userDefault.jpg',0);
		RETURN NULL;
	END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trigger_perfil ON spaapp.auth_user;

CREATE TRIGGER trigger_perfil
AFTER INSERT ON spaapp.auth_user
	FOR EACH ROW EXECUTE PROCEDURE spaapp.insertar_perfil();
