--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = new_public, public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO spauser;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO spauser;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO spauser;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO spauser;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO spauser;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO spauser;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO spauser;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO spauser;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO spauser;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO spauser;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO spauser;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO spauser;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO spauser;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO spauser;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO spauser;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO spauser;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO spauser;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO spauser;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO spauser;

--
-- Name: social_auth_association; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE social_auth_association (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    handle character varying(255) NOT NULL,
    secret character varying(255) NOT NULL,
    issued integer NOT NULL,
    lifetime integer NOT NULL,
    assoc_type character varying(64) NOT NULL
);


ALTER TABLE social_auth_association OWNER TO spauser;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE social_auth_association_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_association_id_seq OWNER TO spauser;

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE social_auth_association_id_seq OWNED BY social_auth_association.id;


--
-- Name: social_auth_code; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE social_auth_code (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    code character varying(32) NOT NULL,
    verified boolean NOT NULL
);


ALTER TABLE social_auth_code OWNER TO spauser;

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE social_auth_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_code_id_seq OWNER TO spauser;

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE social_auth_code_id_seq OWNED BY social_auth_code.id;


--
-- Name: social_auth_nonce; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE social_auth_nonce (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    "timestamp" integer NOT NULL,
    salt character varying(65) NOT NULL
);


ALTER TABLE social_auth_nonce OWNER TO spauser;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE social_auth_nonce_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_nonce_id_seq OWNER TO spauser;

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE social_auth_nonce_id_seq OWNED BY social_auth_nonce.id;


--
-- Name: social_auth_usersocialauth; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE social_auth_usersocialauth (
    id integer NOT NULL,
    provider character varying(32) NOT NULL,
    uid character varying(255) NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE social_auth_usersocialauth OWNER TO spauser;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE social_auth_usersocialauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE social_auth_usersocialauth_id_seq OWNER TO spauser;

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE social_auth_usersocialauth_id_seq OWNED BY social_auth_usersocialauth.id;


--
-- Name: spaapp_actividadcomercial; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_actividadcomercial (
    id_actividad integer NOT NULL,
    tipo_actividad character varying(100) NOT NULL,
    detalles character varying(250) NOT NULL
);


ALTER TABLE spaapp_actividadcomercial OWNER TO spauser;

--
-- Name: spaapp_actividadcomercial_idActividad_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_actividadcomercial_idActividad_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_actividadcomercial_idActividad_seq" OWNER TO spauser;

--
-- Name: spaapp_actividadcomercial_idActividad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_actividadcomercial_idActividad_seq" OWNED BY spaapp_actividadcomercial.id_actividad;


--
-- Name: spaapp_categoria; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_categoria (
    id_categoria integer NOT NULL,
    categoria character varying(150) NOT NULL,
    detalles character varying(250) NOT NULL
);


ALTER TABLE spaapp_categoria OWNER TO spauser;

--
-- Name: spaapp_categoria_idCategoria_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_categoria_idCategoria_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_categoria_idCategoria_seq" OWNER TO spauser;

--
-- Name: spaapp_categoria_idCategoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_categoria_idCategoria_seq" OWNED BY spaapp_categoria.id_categoria;


--
-- Name: spaapp_ciudad; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_ciudad (
    id_ciudad integer NOT NULL,
    nombre character varying(80) NOT NULL,
    descripcion character varying(250) NOT NULL
);


ALTER TABLE spaapp_ciudad OWNER TO spauser;

--
-- Name: spaapp_ciudad_idCiudad_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_ciudad_idCiudad_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_ciudad_idCiudad_seq" OWNER TO spauser;

--
-- Name: spaapp_ciudad_idCiudad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_ciudad_idCiudad_seq" OWNED BY spaapp_ciudad.id_ciudad;


--
-- Name: spaapp_cliente; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_cliente (
    id_cliente integer NOT NULL,
    fechainicio_suscripcion date NOT NULL,
    suscripcion_activa boolean NOT NULL,
    fecha_pago date,
    detalles_adicionales character varying(250) NOT NULL,
    pedidos_activos boolean,
    valor_suscripcion double precision,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_cliente OWNER TO spauser;

--
-- Name: spaapp_cliente_idCliente_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_cliente_idCliente_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_cliente_idCliente_seq" OWNER TO spauser;

--
-- Name: spaapp_cliente_idCliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_cliente_idCliente_seq" OWNED BY spaapp_cliente.id_cliente;


--
-- Name: spaapp_contrato; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_contrato (
    id_contrato integer NOT NULL,
    codigo character varying(20) NOT NULL,
    detalles character varying(250) NOT NULL,
    vigencia integer NOT NULL,
    fecha_inicio date NOT NULL,
    "fechaFin" date NOT NULL,
    valor double precision NOT NULL,
    ruta_contrato character varying(100),
    id_proveedor_id integer,
    id_sucursal_id integer
);


ALTER TABLE spaapp_contrato OWNER TO spauser;

--
-- Name: spaapp_contrato_idContrato_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_contrato_idContrato_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_contrato_idContrato_seq" OWNER TO spauser;

--
-- Name: spaapp_contrato_idContrato_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_contrato_idContrato_seq" OWNED BY spaapp_contrato.id_contrato;


--
-- Name: spaapp_coordenadas; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_coordenadas (
    id_coordenadas integer NOT NULL,
    latitud character varying(100) NOT NULL,
    longitud character varying(100) NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_coordenadas OWNER TO spauser;

--
-- Name: spaapp_coordenadas_idCoordenadas_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_coordenadas_idCoordenadas_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_coordenadas_idCoordenadas_seq" OWNER TO spauser;

--
-- Name: spaapp_coordenadas_idCoordenadas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_coordenadas_idCoordenadas_seq" OWNED BY spaapp_coordenadas.id_coordenadas;


--
-- Name: spaapp_detallepedido; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_detallepedido (
    id_detalle integer NOT NULL,
    valor double precision NOT NULL,
    observaciones character varying(250) NOT NULL,
    cantidad_solicitada integer NOT NULL,
    id_ingrediente_id integer,
    id_menu_id integer NOT NULL,
    id_pedido_id integer NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_detallepedido OWNER TO spauser;

--
-- Name: spaapp_detallepedido_idDetalle_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_detallepedido_idDetalle_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_detallepedido_idDetalle_seq" OWNER TO spauser;

--
-- Name: spaapp_detallepedido_idDetalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_detallepedido_idDetalle_seq" OWNED BY spaapp_detallepedido.id_detalle;


--
-- Name: spaapp_direccion; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_direccion (
    id_direccion integer NOT NULL,
    calle_principal character varying(100) NOT NULL,
    calle_secundario character varying(100) NOT NULL,
    numero_casa character varying(5),
    latitud character varying(100) NOT NULL,
    longitud character varying(100) NOT NULL,
    referencia character varying(250) NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_direccion OWNER TO spauser;

--
-- Name: spaapp_direccion_idDireccion_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_direccion_idDireccion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_direccion_idDireccion_seq" OWNER TO spauser;

--
-- Name: spaapp_direccion_idDireccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_direccion_idDireccion_seq" OWNED BY spaapp_direccion.id_direccion;


--
-- Name: spaapp_horariotransportista; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_horariotransportista (
    id_horario integer NOT NULL,
    dias character varying(80) NOT NULL,
    horas character varying(50) NOT NULL,
    id_transportista_id integer NOT NULL
);


ALTER TABLE spaapp_horariotransportista OWNER TO spauser;

--
-- Name: spaapp_horariotransportista_idHorario_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_horariotransportista_idHorario_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_horariotransportista_idHorario_seq" OWNER TO spauser;

--
-- Name: spaapp_horariotransportista_idHorario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_horariotransportista_idHorario_seq" OWNED BY spaapp_horariotransportista.id_horario;


--
-- Name: spaapp_ingrediente; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_ingrediente (
    id_ingrediente integer NOT NULL,
    nombre character varying(100) NOT NULL,
    tipo character varying(20),
    disponible boolean NOT NULL,
    valor double precision,
    id_menu_id integer NOT NULL
);


ALTER TABLE spaapp_ingrediente OWNER TO spauser;

--
-- Name: spaapp_ingrediente_idIngrediente_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_ingrediente_idIngrediente_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_ingrediente_idIngrediente_seq" OWNER TO spauser;

--
-- Name: spaapp_ingrediente_idIngrediente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_ingrediente_idIngrediente_seq" OWNED BY spaapp_ingrediente.id_ingrediente;


--
-- Name: spaapp_menu; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_menu (
    id_menu integer NOT NULL,
    nombre_producto character varying(200) NOT NULL,
    foto character varying(100) NOT NULL,
    descripcion character varying(200) NOT NULL,
    cantidad_diaria integer NOT NULL,
    venta_aproximada integer NOT NULL,
    hora_mayor_venta time without time zone NOT NULL,
    cantidad_mayor_venta integer NOT NULL,
    existencia_actual integer NOT NULL,
    valor double precision NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_menu OWNER TO spauser;

--
-- Name: spaapp_menu_idMenu_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_menu_idMenu_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_menu_idMenu_seq" OWNER TO spauser;

--
-- Name: spaapp_menu_idMenu_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_menu_idMenu_seq" OWNED BY spaapp_menu.id_menu;


--
-- Name: spaapp_pedido; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_pedido (
    id_pedido integer NOT NULL,
    fecha date NOT NULL,
    estado character varying(50) NOT NULL,
    valor double precision NOT NULL,
    id_cliente_id integer NOT NULL,
    id_direccion_id integer NOT NULL,
    id_tipo_pago_id integer NOT NULL,
    id_tipo_pedido_id integer NOT NULL,
    id_trasportista_id integer NOT NULL
);


ALTER TABLE spaapp_pedido OWNER TO spauser;

--
-- Name: spaapp_pedido_idPedido_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_pedido_idPedido_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_pedido_idPedido_seq" OWNER TO spauser;

--
-- Name: spaapp_pedido_idPedido_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_pedido_idPedido_seq" OWNED BY spaapp_pedido.id_pedido;


--
-- Name: spaapp_perfil; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_perfil (
    id_perfil integer NOT NULL,
    cedula character varying(10),
    fecha_nacimiento date,
    telefono character varying(10),
    genero character varying(10),
    ruta_foto character varying(100),
    id_usuario_id integer NOT NULL,
    sim_dispositivo character varying(30) NOT NULL
);


ALTER TABLE spaapp_perfil OWNER TO spauser;

--
-- Name: spaapp_perfil_idPerfil_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_perfil_idPerfil_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_perfil_idPerfil_seq" OWNER TO spauser;

--
-- Name: spaapp_perfil_idPerfil_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_perfil_idPerfil_seq" OWNED BY spaapp_perfil.id_perfil;


--
-- Name: spaapp_proveedor; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_proveedor (
    id_proveedor integer NOT NULL,
    nombre_comercial character varying(100) NOT NULL,
    telefono character varying(10) NOT NULL,
    telefono_contacto character varying(10),
    dias_atencion character varying(80) NOT NULL,
    horario_atencion character varying(50) NOT NULL,
    prioridad integer NOT NULL,
    activo boolean NOT NULL,
    hora_pico time without time zone,
    me_gusta integer,
    id_actividad_id integer NOT NULL,
    id_categoria_id integer NOT NULL,
    id_ciudad_id integer NOT NULL,
    id_direccion_id integer NOT NULL,
    id_perfil_id integer NOT NULL,
    razon_social character varying(20) NOT NULL
);


ALTER TABLE spaapp_proveedor OWNER TO spauser;

--
-- Name: spaapp_proveedor_idProveedor_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_proveedor_idProveedor_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_proveedor_idProveedor_seq" OWNER TO spauser;

--
-- Name: spaapp_proveedor_idProveedor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_proveedor_idProveedor_seq" OWNED BY spaapp_proveedor.id_proveedor;


--
-- Name: spaapp_redsocial; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_redsocial (
    id_red_social integer NOT NULL,
    nombre_red_social character varying(50) NOT NULL,
    cuenta_persona character varying(100) NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_redsocial OWNER TO spauser;

--
-- Name: spaapp_redsocial_idRedSocial_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_redsocial_idRedSocial_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_redsocial_idRedSocial_seq" OWNER TO spauser;

--
-- Name: spaapp_redsocial_idRedSocial_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_redsocial_idRedSocial_seq" OWNED BY spaapp_redsocial.id_red_social;


--
-- Name: spaapp_sucursal; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_sucursal (
    id_sucursal integer NOT NULL,
    nombre_sucursal character varying(100) NOT NULL,
    razon_social character varying(200) NOT NULL,
    telefono character varying(10) NOT NULL,
    telefono_contacto character varying(10),
    dias_atencion character varying(80) NOT NULL,
    hora_atencion character varying(50) NOT NULL,
    prioridad integer NOT NULL,
    activo boolean NOT NULL,
    hora_pico time without time zone NOT NULL,
    me_gusta integer NOT NULL,
    id_ciudad_id integer NOT NULL,
    id_direccion_id integer NOT NULL,
    id_proveedor_id integer NOT NULL
);


ALTER TABLE spaapp_sucursal OWNER TO spauser;

--
-- Name: spaapp_sucursal_idSucursal_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_sucursal_idSucursal_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_sucursal_idSucursal_seq" OWNER TO spauser;

--
-- Name: spaapp_sucursal_idSucursal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_sucursal_idSucursal_seq" OWNED BY spaapp_sucursal.id_sucursal;


--
-- Name: spaapp_tipopago; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_tipopago (
    id_tipo_pago integer NOT NULL,
    tipo_pago character varying(50) NOT NULL,
    observaciones character varying(250) NOT NULL
);


ALTER TABLE spaapp_tipopago OWNER TO spauser;

--
-- Name: spaapp_tipopago_idTipoPago_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_tipopago_idTipoPago_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_tipopago_idTipoPago_seq" OWNER TO spauser;

--
-- Name: spaapp_tipopago_idTipoPago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_tipopago_idTipoPago_seq" OWNED BY spaapp_tipopago.id_tipo_pago;


--
-- Name: spaapp_tipopedido; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_tipopedido (
    id_tipo_pedido integer NOT NULL,
    tipo_pedido character varying(50) NOT NULL,
    observaciones character varying(250) NOT NULL
);


ALTER TABLE spaapp_tipopedido OWNER TO spauser;

--
-- Name: spaapp_tipopedido_idTipoPedido_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_tipopedido_idTipoPedido_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_tipopedido_idTipoPedido_seq" OWNER TO spauser;

--
-- Name: spaapp_tipopedido_idTipoPedido_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_tipopedido_idTipoPedido_seq" OWNED BY spaapp_tipopedido.id_tipo_pedido;


--
-- Name: spaapp_transportista; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_transportista (
    id_transportista integer NOT NULL,
    tipo_transporte character varying(20) NOT NULL,
    tipo_servicio character varying(50) NOT NULL,
    id_perfil_id integer NOT NULL
);


ALTER TABLE spaapp_transportista OWNER TO spauser;

--
-- Name: spaapp_transportista_idTransportista_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_transportista_idTransportista_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_transportista_idTransportista_seq" OWNER TO spauser;

--
-- Name: spaapp_transportista_idTransportista_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_transportista_idTransportista_seq" OWNED BY spaapp_transportista.id_transportista;


--
-- Name: spaapp_valoracion; Type: TABLE; Schema: public; Owner: spauser
--

CREATE TABLE spaapp_valoracion (
    id_valoracion integer NOT NULL,
    valor_transportista integer NOT NULL,
    observaciones character varying(200) NOT NULL,
    id_cliente_id integer NOT NULL,
    id_pedido_id integer NOT NULL,
    id_proveedor_id integer NOT NULL,
    id_transportista_id integer NOT NULL,
    valor_proveedor integer NOT NULL,
    valor_servicio integer NOT NULL,
    valoracion_general integer NOT NULL
);


ALTER TABLE spaapp_valoracion OWNER TO spauser;

--
-- Name: spaapp_valoracion_idValoracion_seq; Type: SEQUENCE; Schema: public; Owner: spauser
--

CREATE SEQUENCE "spaapp_valoracion_idValoracion_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "spaapp_valoracion_idValoracion_seq" OWNER TO spauser;

--
-- Name: spaapp_valoracion_idValoracion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: spauser
--

ALTER SEQUENCE "spaapp_valoracion_idValoracion_seq" OWNED BY spaapp_valoracion.id_valoracion;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_association ALTER COLUMN id SET DEFAULT nextval('social_auth_association_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_code ALTER COLUMN id SET DEFAULT nextval('social_auth_code_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce ALTER COLUMN id SET DEFAULT nextval('social_auth_nonce_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth ALTER COLUMN id SET DEFAULT nextval('social_auth_usersocialauth_id_seq'::regclass);


--
-- Name: id_actividad; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_actividadcomercial ALTER COLUMN id_actividad SET DEFAULT nextval('"spaapp_actividadcomercial_idActividad_seq"'::regclass);


--
-- Name: id_categoria; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_categoria ALTER COLUMN id_categoria SET DEFAULT nextval('"spaapp_categoria_idCategoria_seq"'::regclass);


--
-- Name: id_ciudad; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_ciudad ALTER COLUMN id_ciudad SET DEFAULT nextval('"spaapp_ciudad_idCiudad_seq"'::regclass);


--
-- Name: id_cliente; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente ALTER COLUMN id_cliente SET DEFAULT nextval('"spaapp_cliente_idCliente_seq"'::regclass);


--
-- Name: id_contrato; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato ALTER COLUMN id_contrato SET DEFAULT nextval('"spaapp_contrato_idContrato_seq"'::regclass);


--
-- Name: id_coordenadas; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas ALTER COLUMN id_coordenadas SET DEFAULT nextval('"spaapp_coordenadas_idCoordenadas_seq"'::regclass);


--
-- Name: id_detalle; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido ALTER COLUMN id_detalle SET DEFAULT nextval('"spaapp_detallepedido_idDetalle_seq"'::regclass);


--
-- Name: id_direccion; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion ALTER COLUMN id_direccion SET DEFAULT nextval('"spaapp_direccion_idDireccion_seq"'::regclass);


--
-- Name: id_horario; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista ALTER COLUMN id_horario SET DEFAULT nextval('"spaapp_horariotransportista_idHorario_seq"'::regclass);


--
-- Name: id_ingrediente; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente ALTER COLUMN id_ingrediente SET DEFAULT nextval('"spaapp_ingrediente_idIngrediente_seq"'::regclass);


--
-- Name: id_menu; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu ALTER COLUMN id_menu SET DEFAULT nextval('"spaapp_menu_idMenu_seq"'::regclass);


--
-- Name: id_pedido; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido ALTER COLUMN id_pedido SET DEFAULT nextval('"spaapp_pedido_idPedido_seq"'::regclass);


--
-- Name: id_perfil; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil ALTER COLUMN id_perfil SET DEFAULT nextval('"spaapp_perfil_idPerfil_seq"'::regclass);


--
-- Name: id_proveedor; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor ALTER COLUMN id_proveedor SET DEFAULT nextval('"spaapp_proveedor_idProveedor_seq"'::regclass);


--
-- Name: id_red_social; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial ALTER COLUMN id_red_social SET DEFAULT nextval('"spaapp_redsocial_idRedSocial_seq"'::regclass);


--
-- Name: id_sucursal; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal ALTER COLUMN id_sucursal SET DEFAULT nextval('"spaapp_sucursal_idSucursal_seq"'::regclass);


--
-- Name: id_tipo_pago; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopago ALTER COLUMN id_tipo_pago SET DEFAULT nextval('"spaapp_tipopago_idTipoPago_seq"'::regclass);


--
-- Name: id_tipo_pedido; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopedido ALTER COLUMN id_tipo_pedido SET DEFAULT nextval('"spaapp_tipopedido_idTipoPedido_seq"'::regclass);


--
-- Name: id_transportista; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista ALTER COLUMN id_transportista SET DEFAULT nextval('"spaapp_transportista_idTransportista_seq"'::regclass);


--
-- Name: id_valoracion; Type: DEFAULT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion ALTER COLUMN id_valoracion SET DEFAULT nextval('"spaapp_valoracion_idValoracion_seq"'::regclass);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: social_auth_association_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_association
    ADD CONSTRAINT social_auth_association_pkey PRIMARY KEY (id);


--
-- Name: social_auth_code_email_801b2d02_uniq; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_code
    ADD CONSTRAINT social_auth_code_email_801b2d02_uniq UNIQUE (email, code);


--
-- Name: social_auth_code_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_code
    ADD CONSTRAINT social_auth_code_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce_server_url_f6284463_uniq; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_server_url_f6284463_uniq UNIQUE (server_url, "timestamp", salt);


--
-- Name: social_auth_usersocialauth_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth_provider_e6b5e668_uniq; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_provider_e6b5e668_uniq UNIQUE (provider, uid);


--
-- Name: spaapp_actividadcomercial_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_actividadcomercial
    ADD CONSTRAINT spaapp_actividadcomercial_pkey PRIMARY KEY (id_actividad);


--
-- Name: spaapp_categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_categoria
    ADD CONSTRAINT spaapp_categoria_pkey PRIMARY KEY (id_categoria);


--
-- Name: spaapp_ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_ciudad
    ADD CONSTRAINT spaapp_ciudad_pkey PRIMARY KEY (id_ciudad);


--
-- Name: spaapp_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente
    ADD CONSTRAINT spaapp_cliente_pkey PRIMARY KEY (id_cliente);


--
-- Name: spaapp_contrato_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT spaapp_contrato_pkey PRIMARY KEY (id_contrato);


--
-- Name: spaapp_coordenadas_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas
    ADD CONSTRAINT spaapp_coordenadas_pkey PRIMARY KEY (id_coordenadas);


--
-- Name: spaapp_detallepedido_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT spaapp_detallepedido_pkey PRIMARY KEY (id_detalle);


--
-- Name: spaapp_direccion_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion
    ADD CONSTRAINT spaapp_direccion_pkey PRIMARY KEY (id_direccion);


--
-- Name: spaapp_horariotransportista_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista
    ADD CONSTRAINT spaapp_horariotransportista_pkey PRIMARY KEY (id_horario);


--
-- Name: spaapp_ingrediente_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente
    ADD CONSTRAINT spaapp_ingrediente_pkey PRIMARY KEY (id_ingrediente);


--
-- Name: spaapp_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu
    ADD CONSTRAINT spaapp_menu_pkey PRIMARY KEY (id_menu);


--
-- Name: spaapp_pedido_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_pedido_pkey PRIMARY KEY (id_pedido);


--
-- Name: spaapp_perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil
    ADD CONSTRAINT spaapp_perfil_pkey PRIMARY KEY (id_perfil);


--
-- Name: spaapp_proveedor_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveedor_pkey PRIMARY KEY (id_proveedor);


--
-- Name: spaapp_redsocial_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial
    ADD CONSTRAINT spaapp_redsocial_pkey PRIMARY KEY (id_red_social);


--
-- Name: spaapp_sucursal_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursal_pkey PRIMARY KEY (id_sucursal);


--
-- Name: spaapp_tipopago_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopago
    ADD CONSTRAINT spaapp_tipopago_pkey PRIMARY KEY (id_tipo_pago);


--
-- Name: spaapp_tipopedido_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_tipopedido
    ADD CONSTRAINT spaapp_tipopedido_pkey PRIMARY KEY (id_tipo_pedido);


--
-- Name: spaapp_transportista_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista
    ADD CONSTRAINT spaapp_transportista_pkey PRIMARY KEY (id_transportista);


--
-- Name: spaapp_valoracion_pkey; Type: CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valoracion_pkey PRIMARY KEY (id_valoracion);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: social_auth_code_c1336794; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX social_auth_code_c1336794 ON social_auth_code USING btree (code);


--
-- Name: social_auth_code_code_a2393167_like; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX social_auth_code_code_a2393167_like ON social_auth_code USING btree (code varchar_pattern_ops);


--
-- Name: social_auth_usersocialauth_e8701ad4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX social_auth_usersocialauth_e8701ad4 ON social_auth_usersocialauth USING btree (user_id);


--
-- Name: spaapp_cliente_7d3abc31; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_cliente_7d3abc31 ON spaapp_cliente USING btree (id_perfil_id);


--
-- Name: spaapp_contrato_9422f678; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_contrato_9422f678 ON spaapp_contrato USING btree (id_sucursal_id);


--
-- Name: spaapp_contrato_9c285ef4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_contrato_9c285ef4 ON spaapp_contrato USING btree (id_proveedor_id);


--
-- Name: spaapp_coordenadas_7d3abc31; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_coordenadas_7d3abc31 ON spaapp_coordenadas USING btree (id_perfil_id);


--
-- Name: spaapp_detallepedido_57c35fbe; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_57c35fbe ON spaapp_detallepedido USING btree (id_pedido_id);


--
-- Name: spaapp_detallepedido_59e33291; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_59e33291 ON spaapp_detallepedido USING btree (id_menu_id);


--
-- Name: spaapp_detallepedido_9c285ef4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_9c285ef4 ON spaapp_detallepedido USING btree (id_proveedor_id);


--
-- Name: spaapp_detallepedido_ac2cf7b5; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_detallepedido_ac2cf7b5 ON spaapp_detallepedido USING btree (id_ingrediente_id);


--
-- Name: spaapp_direccion_7d3abc31; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_direccion_7d3abc31 ON spaapp_direccion USING btree (id_perfil_id);


--
-- Name: spaapp_horariotransportista_04efa2b5; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_horariotransportista_04efa2b5 ON spaapp_horariotransportista USING btree (id_transportista_id);


--
-- Name: spaapp_ingrediente_59e33291; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_ingrediente_59e33291 ON spaapp_ingrediente USING btree (id_menu_id);


--
-- Name: spaapp_menu_9c285ef4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_menu_9c285ef4 ON spaapp_menu USING btree (id_proveedor_id);


--
-- Name: spaapp_pedido_774f209b; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_pedido_774f209b ON spaapp_pedido USING btree (id_trasportista_id);


--
-- Name: spaapp_pedido_8e074c1f; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_pedido_8e074c1f ON spaapp_pedido USING btree (id_direccion_id);


--
-- Name: spaapp_pedido_9a7b2b76; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_pedido_9a7b2b76 ON spaapp_pedido USING btree (id_cliente_id);


--
-- Name: spaapp_pedido_c121e897; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_pedido_c121e897 ON spaapp_pedido USING btree (id_tipo_pago_id);


--
-- Name: spaapp_pedido_d131b8e2; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_pedido_d131b8e2 ON spaapp_pedido USING btree (id_tipo_pedido_id);


--
-- Name: spaapp_perfil_58e9ac76; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_perfil_58e9ac76 ON spaapp_perfil USING btree (id_usuario_id);


--
-- Name: spaapp_proveedor_0968ab1e; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_proveedor_0968ab1e ON spaapp_proveedor USING btree (id_ciudad_id);


--
-- Name: spaapp_proveedor_6c37563d; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_proveedor_6c37563d ON spaapp_proveedor USING btree (id_categoria_id);


--
-- Name: spaapp_proveedor_7d3abc31; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_proveedor_7d3abc31 ON spaapp_proveedor USING btree (id_perfil_id);


--
-- Name: spaapp_proveedor_8e074c1f; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_proveedor_8e074c1f ON spaapp_proveedor USING btree (id_direccion_id);


--
-- Name: spaapp_proveedor_bd2aa545; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_proveedor_bd2aa545 ON spaapp_proveedor USING btree (id_actividad_id);


--
-- Name: spaapp_redsocial_9c285ef4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_redsocial_9c285ef4 ON spaapp_redsocial USING btree (id_proveedor_id);


--
-- Name: spaapp_sucursal_0968ab1e; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_sucursal_0968ab1e ON spaapp_sucursal USING btree (id_ciudad_id);


--
-- Name: spaapp_sucursal_8e074c1f; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_sucursal_8e074c1f ON spaapp_sucursal USING btree (id_direccion_id);


--
-- Name: spaapp_sucursal_9c285ef4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_sucursal_9c285ef4 ON spaapp_sucursal USING btree (id_proveedor_id);


--
-- Name: spaapp_transportista_7d3abc31; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_transportista_7d3abc31 ON spaapp_transportista USING btree (id_perfil_id);


--
-- Name: spaapp_valoracion_04efa2b5; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_valoracion_04efa2b5 ON spaapp_valoracion USING btree (id_transportista_id);


--
-- Name: spaapp_valoracion_57c35fbe; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_valoracion_57c35fbe ON spaapp_valoracion USING btree (id_pedido_id);


--
-- Name: spaapp_valoracion_9a7b2b76; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_valoracion_9a7b2b76 ON spaapp_valoracion USING btree (id_cliente_id);


--
-- Name: spaapp_valoracion_9c285ef4; Type: INDEX; Schema: public; Owner: spauser
--

CREATE INDEX spaapp_valoracion_9c285ef4 ON spaapp_valoracion USING btree (id_proveedor_id);


--
-- Name: D0d68d447f4a6e6ff4e652eee7eb9355; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT "D0d68d447f4a6e6ff4e652eee7eb9355" FOREIGN KEY (id_trasportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D893b6353e00f54fed4819335a2ca882; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT "D893b6353e00f54fed4819335a2ca882" FOREIGN KEY (id_transportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: D91d9aaf1a328bd31979d1581c55c5ec; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT "D91d9aaf1a328bd31979d1581c55c5ec" FOREIGN KEY (id_actividad_id) REFERENCES spaapp_actividadcomercial(id_actividad) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permiss_permission_id_84c5c92e_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permiss_content_type_id_2f476e4b_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_per_permission_id_1fbb5f2c_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: db36c5fcf29c7d8fe610ac3a310d5e58; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_horariotransportista
    ADD CONSTRAINT db36c5fcf29c7d8fe610ac3a310d5e58 FOREIGN KEY (id_transportista_id) REFERENCES spaapp_transportista(id_transportista) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_content_type_id_c4bce8eb_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_content_type_id_c4bce8eb_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: id_ingrediente_id_db36c488_fk_spaapp_ingrediente_idIngrediente; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT "id_ingrediente_id_db36c488_fk_spaapp_ingrediente_idIngrediente" FOREIGN KEY (id_ingrediente_id) REFERENCES spaapp_ingrediente(id_ingrediente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sp_id_tipo_pedido_id_cde83768_fk_spaapp_tipopedido_idTipoPedido; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT "sp_id_tipo_pedido_id_cde83768_fk_spaapp_tipopedido_idTipoPedido" FOREIGN KEY (id_tipo_pedido_id) REFERENCES spaapp_tipopedido(id_tipo_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_categoria_id_d672ac86_fk_spaapp_categoria_id_categoria; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaap_id_categoria_id_d672ac86_fk_spaapp_categoria_id_categoria FOREIGN KEY (id_categoria_id) REFERENCES spaapp_categoria(id_categoria) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_direccion_id_19a2780d_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaap_id_direccion_id_19a2780d_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_direccion_id_80462fd2_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaap_id_direccion_id_80462fd2_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_direccion_id_e2c4fbe4_fk_spaapp_direccion_id_direccion; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaap_id_direccion_id_e2c4fbe4_fk_spaapp_direccion_id_direccion FOREIGN KEY (id_direccion_id) REFERENCES spaapp_direccion(id_direccion) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_18c32e0e_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_redsocial
    ADD CONSTRAINT spaap_id_proveedor_id_18c32e0e_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_540b8b72_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaap_id_proveedor_id_540b8b72_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaap_id_proveedor_id_66760433_fk_spaapp_proveedor_id_proveedor; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaap_id_proveedor_id_66760433_fk_spaapp_proveedor_id_proveedor FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_cliente_id_perfil_id_6a66d1dd_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_cliente
    ADD CONSTRAINT spaapp_cliente_id_perfil_id_6a66d1dd_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_co_id_sucursal_id_54ec1010_fk_spaapp_sucursal_idSucursal; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT "spaapp_co_id_sucursal_id_54ec1010_fk_spaapp_sucursal_idSucursal" FOREIGN KEY (id_sucursal_id) REFERENCES spaapp_sucursal(id_sucursal) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_coorden_id_perfil_id_40dcc2a5_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_coordenadas
    ADD CONSTRAINT spaapp_coorden_id_perfil_id_40dcc2a5_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_detallep_id_pedido_id_d433e145_fk_spaapp_pedido_idPedido; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT "spaapp_detallep_id_pedido_id_d433e145_fk_spaapp_pedido_idPedido" FOREIGN KEY (id_pedido_id) REFERENCES spaapp_pedido(id_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_detallepedido_id_menu_id_75fb5011_fk_spaapp_menu_idMenu; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT "spaapp_detallepedido_id_menu_id_75fb5011_fk_spaapp_menu_idMenu" FOREIGN KEY (id_menu_id) REFERENCES spaapp_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_direcci_id_perfil_id_ff0b3f89_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_direccion
    ADD CONSTRAINT spaapp_direcci_id_perfil_id_ff0b3f89_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_id_proveedor_id_11675f72_fk_spaapp_proveedor_idProveedor; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_contrato
    ADD CONSTRAINT "spaapp_id_proveedor_id_11675f72_fk_spaapp_proveedor_idProveedor" FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_id_proveedor_id_e39d0124_fk_spaapp_proveedor_idProveedor; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_menu
    ADD CONSTRAINT "spaapp_id_proveedor_id_e39d0124_fk_spaapp_proveedor_idProveedor" FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_id_proveedor_id_f53812db_fk_spaapp_proveedor_idProveedor; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_detallepedido
    ADD CONSTRAINT "spaapp_id_proveedor_id_f53812db_fk_spaapp_proveedor_idProveedor" FOREIGN KEY (id_proveedor_id) REFERENCES spaapp_proveedor(id_proveedor) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_ingrediente_id_menu_id_d3eb6e91_fk_spaapp_menu_idMenu; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_ingrediente
    ADD CONSTRAINT "spaapp_ingrediente_id_menu_id_d3eb6e91_fk_spaapp_menu_idMenu" FOREIGN KEY (id_menu_id) REFERENCES spaapp_menu(id_menu) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_p_id_tipo_pago_id_1cbd151a_fk_spaapp_tipopago_idTipoPago; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT "spaapp_p_id_tipo_pago_id_1cbd151a_fk_spaapp_tipopago_idTipoPago" FOREIGN KEY (id_tipo_pago_id) REFERENCES spaapp_tipopago(id_tipo_pago) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_pedi_id_cliente_id_2addba9c_fk_spaapp_cliente_id_cliente; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_pedido
    ADD CONSTRAINT spaapp_pedi_id_cliente_id_2addba9c_fk_spaapp_cliente_id_cliente FOREIGN KEY (id_cliente_id) REFERENCES spaapp_cliente(id_cliente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_perfil_id_usuario_id_ecaf41b3_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_perfil
    ADD CONSTRAINT spaapp_perfil_id_usuario_id_ecaf41b3_fk_auth_user_id FOREIGN KEY (id_usuario_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_proveed_id_ciudad_id_8f779713_fk_spaapp_ciudad_id_ciudad; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveed_id_ciudad_id_8f779713_fk_spaapp_ciudad_id_ciudad FOREIGN KEY (id_ciudad_id) REFERENCES spaapp_ciudad(id_ciudad) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_proveed_id_perfil_id_fff821d0_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_proveedor
    ADD CONSTRAINT spaapp_proveed_id_perfil_id_fff821d0_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_sucursa_id_ciudad_id_f5184cf6_fk_spaapp_ciudad_id_ciudad; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_sucursal
    ADD CONSTRAINT spaapp_sucursa_id_ciudad_id_f5184cf6_fk_spaapp_ciudad_id_ciudad FOREIGN KEY (id_ciudad_id) REFERENCES spaapp_ciudad(id_ciudad) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_transpo_id_perfil_id_9a457afd_fk_spaapp_perfil_id_perfil; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_transportista
    ADD CONSTRAINT spaapp_transpo_id_perfil_id_9a457afd_fk_spaapp_perfil_id_perfil FOREIGN KEY (id_perfil_id) REFERENCES spaapp_perfil(id_perfil) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_valo_id_cliente_id_a7d43e7d_fk_spaapp_cliente_id_cliente; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valo_id_cliente_id_a7d43e7d_fk_spaapp_cliente_id_cliente FOREIGN KEY (id_cliente_id) REFERENCES spaapp_cliente(id_cliente) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: spaapp_valorac_id_pedido_id_119ed90a_fk_spaapp_pedido_id_pedido; Type: FK CONSTRAINT; Schema: public; Owner: spauser
--

ALTER TABLE ONLY spaapp_valoracion
    ADD CONSTRAINT spaapp_valorac_id_pedido_id_119ed90a_fk_spaapp_pedido_id_pedido FOREIGN KEY (id_pedido_id) REFERENCES spaapp_pedido(id_pedido) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA new_public FROM PUBLIC;
REVOKE ALL ON SCHEMA new_public FROM postgres;
REVOKE ALL ON SCHEMA new_public FROM spauser;
GRANT ALL ON SCHEMA new_public TO spauser;
GRANT ALL ON SCHEMA new_public TO postgres;
--GRANT ALL ON SCHEMA new_public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

