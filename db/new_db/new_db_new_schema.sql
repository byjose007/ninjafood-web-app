CREATE SCHEMA spaapp;
ALTER SCHEMA spaapp OWNER TO spauser;
SET search_path = spaapp, PUBLIC, pg_catalog;
ALTER ROLE spauser SET search_path TO spaapp, PUBLIC, pg_catalog;
REVOKE ALL ON SCHEMA spaapp FROM PUBLIC;
REVOKE ALL ON SCHEMA spaapp FROM spauser;
GRANT ALL ON SCHEMA spaapp TO spauser;
GRANT ALL ON SCHEMA spaapp TO postgres;
