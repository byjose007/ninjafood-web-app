CREATE OR REPLACE FUNCTION spaapp.insertar_menu() RETURNS TRIGGER AS $$
	BEGIN
		INSERT INTO spaapp_menu (id_proveedor_id) VALUES (NEW.id_proveedor);
		RETURN NULL;
	END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trigger_menu ON spaapp.spaapp_proveedor;

CREATE TRIGGER trigger_menu
AFTER INSERT ON spaapp.spaapp_proveedor
	FOR EACH ROW EXECUTE PROCEDURE spaapp.insertar_menu();

