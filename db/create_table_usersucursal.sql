CREATE TABLE spaapp_usersurcursal
(
  id serial NOT NULL,
  id_sucursal_id integer NOT NULL,
  id_user_id integer NOT NULL,
  CONSTRAINT spaapp_usersurcursal_pkey PRIMARY KEY (id),
  CONSTRAINT spaapp_u_id_sucursal_id_efbf6b36_fk_spaapp_sucursal_id_sucursal FOREIGN KEY (id_sucursal_id)
      REFERENCES spaapp_sucursal (id_sucursal) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT spaapp_usersurcursal_id_user_id_8735a020_fk_auth_user_id FOREIGN KEY (id_user_id)
      REFERENCES auth_user (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);
ALTER TABLE spaapp_usersurcursal
  OWNER TO spauser;

-- Index: spaapp_usersurcursal_3002e0f2

-- DROP INDEX spaapp_usersurcursal_3002e0f2;

CREATE INDEX spaapp_usersurcursal_3002e0f2
  ON spaapp_usersurcursal
  USING btree
  (id_user_id);

-- Index: spaapp_usersurcursal_e163c0f9

-- DROP INDEX spaapp_usersurcursal_e163c0f9;

CREATE INDEX spaapp_usersurcursal_e163c0f9
  ON spaapp_usersurcursal
  USING btree
  (id_sucursal_id);
