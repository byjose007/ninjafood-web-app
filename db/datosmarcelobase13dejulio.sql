--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = spaapp, pg_catalog;

--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO django_content_type (id, app_label, model) VALUES (1, 'admin', 'logentry');
INSERT INTO django_content_type (id, app_label, model) VALUES (2, 'auth', 'permission');
INSERT INTO django_content_type (id, app_label, model) VALUES (3, 'auth', 'group');
INSERT INTO django_content_type (id, app_label, model) VALUES (4, 'auth', 'user');
INSERT INTO django_content_type (id, app_label, model) VALUES (5, 'contenttypes', 'contenttype');
INSERT INTO django_content_type (id, app_label, model) VALUES (6, 'sessions', 'session');
INSERT INTO django_content_type (id, app_label, model) VALUES (7, 'spaapp', 'actividadcomercial');
INSERT INTO django_content_type (id, app_label, model) VALUES (8, 'spaapp', 'perfil');
INSERT INTO django_content_type (id, app_label, model) VALUES (9, 'spaapp', 'categoria');
INSERT INTO django_content_type (id, app_label, model) VALUES (10, 'spaapp', 'proveedor');
INSERT INTO django_content_type (id, app_label, model) VALUES (11, 'spaapp', 'ciudad');
INSERT INTO django_content_type (id, app_label, model) VALUES (12, 'spaapp', 'direccion');
INSERT INTO django_content_type (id, app_label, model) VALUES (13, 'spaapp', 'cliente');
INSERT INTO django_content_type (id, app_label, model) VALUES (14, 'spaapp', 'menu');
INSERT INTO django_content_type (id, app_label, model) VALUES (15, 'spaapp', 'sucursal');
INSERT INTO django_content_type (id, app_label, model) VALUES (16, 'spaapp', 'contrato');
INSERT INTO django_content_type (id, app_label, model) VALUES (17, 'spaapp', 'transportista');
INSERT INTO django_content_type (id, app_label, model) VALUES (18, 'spaapp', 'coordenadas');
INSERT INTO django_content_type (id, app_label, model) VALUES (19, 'spaapp', 'tipopago');
INSERT INTO django_content_type (id, app_label, model) VALUES (20, 'spaapp', 'tipopedido');
INSERT INTO django_content_type (id, app_label, model) VALUES (21, 'spaapp', 'pedido');
INSERT INTO django_content_type (id, app_label, model) VALUES (22, 'spaapp', 'platocategoria');
INSERT INTO django_content_type (id, app_label, model) VALUES (23, 'spaapp', 'plato');
INSERT INTO django_content_type (id, app_label, model) VALUES (24, 'spaapp', 'tamanio');
INSERT INTO django_content_type (id, app_label, model) VALUES (25, 'spaapp', 'tamanioplato');
INSERT INTO django_content_type (id, app_label, model) VALUES (26, 'spaapp', 'detallepedido');
INSERT INTO django_content_type (id, app_label, model) VALUES (27, 'spaapp', 'ingrediente');
INSERT INTO django_content_type (id, app_label, model) VALUES (28, 'spaapp', 'horariotransportista');
INSERT INTO django_content_type (id, app_label, model) VALUES (29, 'spaapp', 'modificardetallepedido');
INSERT INTO django_content_type (id, app_label, model) VALUES (30, 'spaapp', 'platoingrediente');
INSERT INTO django_content_type (id, app_label, model) VALUES (31, 'spaapp', 'redsocial');
INSERT INTO django_content_type (id, app_label, model) VALUES (32, 'spaapp', 'valoracion');
INSERT INTO django_content_type (id, app_label, model) VALUES (33, 'corsheaders', 'corsmodel');
INSERT INTO django_content_type (id, app_label, model) VALUES (34, 'default', 'usersocialauth');
INSERT INTO django_content_type (id, app_label, model) VALUES (35, 'default', 'nonce');
INSERT INTO django_content_type (id, app_label, model) VALUES (36, 'default', 'association');
INSERT INTO django_content_type (id, app_label, model) VALUES (37, 'default', 'code');


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (4, 'Can add permission', 2, 'add_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (5, 'Can change permission', 2, 'change_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (6, 'Can delete permission', 2, 'delete_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (7, 'Can add group', 3, 'add_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (8, 'Can change group', 3, 'change_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (9, 'Can delete group', 3, 'delete_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (10, 'Can add user', 4, 'add_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (11, 'Can change user', 4, 'change_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (12, 'Can delete user', 4, 'delete_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (13, 'Can add content type', 5, 'add_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (14, 'Can change content type', 5, 'change_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (15, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (16, 'Can add session', 6, 'add_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (17, 'Can change session', 6, 'change_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (18, 'Can delete session', 6, 'delete_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (19, 'Can add Actividad Comercial', 7, 'add_actividadcomercial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (20, 'Can change Actividad Comercial', 7, 'change_actividadcomercial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (21, 'Can delete Actividad Comercial', 7, 'delete_actividadcomercial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (22, 'Can add perfil', 8, 'add_perfil');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (23, 'Can change perfil', 8, 'change_perfil');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (24, 'Can delete perfil', 8, 'delete_perfil');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (25, 'Can add categoria', 9, 'add_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (26, 'Can change categoria', 9, 'change_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (27, 'Can delete categoria', 9, 'delete_categoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (28, 'Can add proveedor', 10, 'add_proveedor');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (29, 'Can change proveedor', 10, 'change_proveedor');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (30, 'Can delete proveedor', 10, 'delete_proveedor');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (31, 'Can add ciudad', 11, 'add_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (32, 'Can change ciudad', 11, 'change_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (33, 'Can delete ciudad', 11, 'delete_ciudad');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (34, 'Can add direccion', 12, 'add_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (35, 'Can change direccion', 12, 'change_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (36, 'Can delete direccion', 12, 'delete_direccion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (37, 'Can add cliente', 13, 'add_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (38, 'Can change cliente', 13, 'change_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (39, 'Can delete cliente', 13, 'delete_cliente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (40, 'Can add menu', 14, 'add_menu');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (41, 'Can change menu', 14, 'change_menu');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (42, 'Can delete menu', 14, 'delete_menu');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (43, 'Can add sucursal', 15, 'add_sucursal');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (44, 'Can change sucursal', 15, 'change_sucursal');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (45, 'Can delete sucursal', 15, 'delete_sucursal');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (46, 'Can add contrato', 16, 'add_contrato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (47, 'Can change contrato', 16, 'change_contrato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (48, 'Can delete contrato', 16, 'delete_contrato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (49, 'Can add transportista', 17, 'add_transportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (50, 'Can change transportista', 17, 'change_transportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (51, 'Can delete transportista', 17, 'delete_transportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (52, 'Can add Coordenada', 18, 'add_coordenadas');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (53, 'Can change Coordenada', 18, 'change_coordenadas');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (54, 'Can delete Coordenada', 18, 'delete_coordenadas');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (55, 'Can add tipo pago', 19, 'add_tipopago');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (56, 'Can change tipo pago', 19, 'change_tipopago');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (57, 'Can delete tipo pago', 19, 'delete_tipopago');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (58, 'Can add tipo pedido', 20, 'add_tipopedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (59, 'Can change tipo pedido', 20, 'change_tipopedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (60, 'Can delete tipo pedido', 20, 'delete_tipopedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (61, 'Can add pedido', 21, 'add_pedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (62, 'Can change pedido', 21, 'change_pedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (63, 'Can delete pedido', 21, 'delete_pedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (64, 'Can add plato categoria', 22, 'add_platocategoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (65, 'Can change plato categoria', 22, 'change_platocategoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (66, 'Can delete plato categoria', 22, 'delete_platocategoria');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (67, 'Can add plato', 23, 'add_plato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (68, 'Can change plato', 23, 'change_plato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (69, 'Can delete plato', 23, 'delete_plato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (70, 'Can add tamanio', 24, 'add_tamanio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (71, 'Can change tamanio', 24, 'change_tamanio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (72, 'Can delete tamanio', 24, 'delete_tamanio');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (73, 'Can add tamanio plato', 25, 'add_tamanioplato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (74, 'Can change tamanio plato', 25, 'change_tamanioplato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (75, 'Can delete tamanio plato', 25, 'delete_tamanioplato');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (76, 'Can add Detalle de pedido', 26, 'add_detallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (77, 'Can change Detalle de pedido', 26, 'change_detallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (78, 'Can delete Detalle de pedido', 26, 'delete_detallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (79, 'Can add ingrediente', 27, 'add_ingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (80, 'Can change ingrediente', 27, 'change_ingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (81, 'Can delete ingrediente', 27, 'delete_ingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (82, 'Can add Horario de transportista', 28, 'add_horariotransportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (83, 'Can change Horario de transportista', 28, 'change_horariotransportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (84, 'Can delete Horario de transportista', 28, 'delete_horariotransportista');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (85, 'Can add modificardetallepedido', 29, 'add_modificardetallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (86, 'Can change modificardetallepedido', 29, 'change_modificardetallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (87, 'Can delete modificardetallepedido', 29, 'delete_modificardetallepedido');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (88, 'Can add plato ingrediente', 30, 'add_platoingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (89, 'Can change plato ingrediente', 30, 'change_platoingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (90, 'Can delete plato ingrediente', 30, 'delete_platoingrediente');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (91, 'Can add Red social', 31, 'add_redsocial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (92, 'Can change Red social', 31, 'change_redsocial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (93, 'Can delete Red social', 31, 'delete_redsocial');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (94, 'Can add valoracion', 32, 'add_valoracion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (95, 'Can change valoracion', 32, 'change_valoracion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (96, 'Can delete valoracion', 32, 'delete_valoracion');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (97, 'Can add cors model', 33, 'add_corsmodel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (98, 'Can change cors model', 33, 'change_corsmodel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (99, 'Can delete cors model', 33, 'delete_corsmodel');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (100, 'Can add user social auth', 34, 'add_usersocialauth');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (101, 'Can change user social auth', 34, 'change_usersocialauth');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (102, 'Can delete user social auth', 34, 'delete_usersocialauth');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (103, 'Can add nonce', 35, 'add_nonce');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (104, 'Can change nonce', 35, 'change_nonce');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (105, 'Can delete nonce', 35, 'delete_nonce');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (106, 'Can add association', 36, 'add_association');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (107, 'Can change association', 36, 'change_association');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (108, 'Can delete association', 36, 'delete_association');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (109, 'Can add code', 37, 'add_code');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (110, 'Can change code', 37, 'change_code');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (111, 'Can delete code', 37, 'delete_code');


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_permission_id_seq', 111, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (2, 'pbkdf2_sha256$24000$EyPTqDaVpWe0$H9N1qJn8ji2sdTd81wlqmBztMPMNB5n76tUNdyqrFkc=', NULL, false, 'AARON001', '', '', '', false, true, '2016-07-13 15:31:40-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (3, 'pbkdf2_sha256$24000$bB6NV6SpQMNH$8fwLwi98+CNY4SnTxF8N/BcheShsm9V+opj8h71kHbk=', NULL, false, 'ABEL002', '', '', '', false, true, '2016-07-13 15:32:04-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (4, 'pbkdf2_sha256$24000$dO5CBQkwrS0Z$hVmR3AEsx/wysQ1hrLLElSK6ugDLEAkXrZ1W8jPDl7s=', NULL, false, 'ABELARDO003', '', '', '', false, true, '2016-07-13 15:32:30-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (25, 'pbkdf2_sha256$24000$6JdtJYmjmRoh$CsPIW8SYjMJ2SuxVe73TRDB3X2aCnyHvgPhJCLeWiW4=', NULL, false, 'MARCELO024', '', '', '', false, true, '2016-07-13 15:42:18-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (6, 'pbkdf2_sha256$24000$bK3dyqN8G5xN$TqZJME0oNNCM71YUbq+nW3ElhVIwUI0jb8oimybuLzo=', NULL, false, 'ABRAHAM004', '', '', '', false, true, '2016-07-13 15:33:12-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (7, 'pbkdf2_sha256$24000$XkCIaNj2HfTM$+/vdT0zuTXnYVR+xiKAxDJ5sLSksJrDbymA2q/QLReY=', NULL, false, 'ABRIL005', '', '', '', false, true, '2016-07-13 15:33:33-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (5, 'pbkdf2_sha256$24000$rxcm09JpOum1$fJFtheI03n+0dL9qxjELIjnZCl/hvd5B4rb+hJMqNHE=', NULL, false, 'ABIGAÍL006', '', '', '', false, true, '2016-07-13 15:32:44-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (8, 'pbkdf2_sha256$24000$CyhDsZ03zCwo$Ll3qX28DG0miO9gPl78SfSZNVTyAWCz4quyX2Hs/Gd4=', NULL, false, 'BALDOMERO007', '', '', '', false, true, '2016-07-13 15:34:41-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (9, 'pbkdf2_sha256$24000$HMybQJLA7El4$u/PXYoy7e11a4kpdg3pro1enSH3ATXk7i2+H4YGV7ow=', NULL, false, 'BALTASAR008', '', '', '', false, true, '2016-07-13 15:34:54-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (10, 'pbkdf2_sha256$24000$h3BzR6kmcyx9$Iv0T+ESXPsY9TjE5Up47SDshVNKSjMdQph+MVID0l+I=', NULL, false, 'BÁRBARA009', '', '', '', false, true, '2016-07-13 15:35:32-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (11, 'pbkdf2_sha256$24000$90UzylLD0QXq$dDywgkFBdPA0Uk8kHHIR2UyYHqPw/KtMx3l7N8hjs/g=', NULL, false, 'BENITO010', '', '', '', false, true, '2016-07-13 15:36:05-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (12, 'pbkdf2_sha256$24000$m3ARY7aG7g3e$FhctL3BWEmBEu+aiun/wYe10hWgi6ftR7+U1IdYWLnY=', NULL, false, 'CALIXTO011', '', '', '', false, true, '2016-07-13 15:36:36-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (13, 'pbkdf2_sha256$24000$LVZmoncRH1U8$j4bvw7ISTonju8SfAWSXuBX34hxdPccUC/XALXT82WQ=', NULL, false, 'CAMELIA012', '', '', '', false, true, '2016-07-13 15:36:57-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (14, 'pbkdf2_sha256$24000$Nj0WD1AD27NO$UXfSGvyNksPzsoTOS+9+o6kg5S+4IL0j6Z0AwNUFMlU=', NULL, false, 'CAMILA013', '', '', '', false, true, '2016-07-13 15:37:17-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (15, 'pbkdf2_sha256$24000$eYkOTBsSOhSF$zjoHjgo03r1pKEZ/i95N88JCutIU00XOQvy4ApB9WC8=', NULL, false, 'CARIDAD014', '', '', '', false, true, '2016-07-13 15:37:34-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (16, 'pbkdf2_sha256$24000$a44tZF9BDhnj$38SqsN8NzJZhuJR8pRPblh0jOk2iOW6/UPpOGmPw0tI=', NULL, false, 'CASILDA015', '', '', '', false, true, '2016-07-13 15:37:51-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (17, 'pbkdf2_sha256$24000$3597LVAbffGD$2S5gy2VYebzcNUkiy6JaNT4VLLeuQoHHCVMO2f8MUXU=', NULL, false, 'CASIMIRO016', '', '', '', false, true, '2016-07-13 15:38:23-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (18, 'pbkdf2_sha256$24000$WmjmeYSzBShR$3ZWSl+mmxAE/4YYn1KEjeLuEE8aUGKbPEyOB5OJAUSE=', NULL, false, 'CATALINA017', '', '', '', false, true, '2016-07-13 15:38:42-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (20, 'pbkdf2_sha256$24000$632Oj03mrG0o$uu5iPRg1abD6lVb5zgIUwWpqBAjwtb8lrAjOEqZSdns=', NULL, false, 'CECILIA019', '', '', '', false, true, '2016-07-13 15:39:40-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (21, 'pbkdf2_sha256$24000$VvmwPwdZAuoA$V+nxUGzsJuEGDePbl8XTarUqBmShcbExScKUe04so6M=', NULL, false, 'EDGARDO020', '', '', '', false, true, '2016-07-13 15:40:10-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (22, 'pbkdf2_sha256$24000$YejxXYwK2JQJ$bGJSmf+bQq0M8bU5XqmTrIN8pC6P6tQSrX/blELAUJQ=', NULL, false, 'EUFEMIO021', '', '', '', false, true, '2016-07-13 15:40:47-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (23, 'pbkdf2_sha256$24000$JHjjgdIaBlG5$+h9mFHL+q6NMv230vMMdNfzcV9b93r1QCiUaFZLaHos=', NULL, false, 'EVA022', '', '', '', false, true, '2016-07-13 15:41:27-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (24, 'pbkdf2_sha256$24000$LfOBUzP3b1jp$zStyxsbUqFGXIH2AQHnN0KCfOqG3nhjBQUhCyYEv4HQ=', NULL, false, 'MACARENA023', '', '', '', false, true, '2016-07-13 15:41:57-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (26, 'pbkdf2_sha256$24000$bPyG7saGMRvG$twZMISKOZOWFQKSCSBA/vE9jae7Da5fiPkOnmv+BGyY=', NULL, false, 'pamela025', '', '', '', false, true, '2016-07-13 15:42:41-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (19, 'pbkdf2_sha256$24000$GA6VNSMye1hn$U83jrVJ4sj1aZa7lmjS+zFtb1z9TZ9d0MuMK75DW54M=', NULL, false, 'Nicholas018', '', '', '', false, true, '2016-07-13 15:39:16-05');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (1, 'pbkdf2_sha256$24000$XBXe673vv3gG$WzxEb9ZdLeJF7TNMADQzhZKsZ15WU+HrDBvHjojCVV8=', '2016-07-13 17:53:00.130317-05', true, 'admin', '', '', '', true, true, '2016-07-13 15:29:31.559616-05');


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_id_seq', 26, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (1, '2016-07-13 15:31:41.014017-05', '2', 'AARON001', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (2, '2016-07-13 15:31:45.005677-05', '2', 'AARON001', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (3, '2016-07-13 15:32:04.712752-05', '3', 'ABEL002', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (4, '2016-07-13 15:32:11.036695-05', '3', 'ABEL002', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (5, '2016-07-13 15:32:31.027726-05', '4', 'ABELARDO003', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (6, '2016-07-13 15:32:34.323041-05', '4', 'ABELARDO003', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (7, '2016-07-13 15:32:45.04427-05', '5', 'ABIGAÍL', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (8, '2016-07-13 15:32:46.994806-05', '5', 'ABIGAÍL', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (9, '2016-07-13 15:33:12.54989-05', '6', 'ABRAHAM004', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (10, '2016-07-13 15:33:16.511406-05', '6', 'ABRAHAM004', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (11, '2016-07-13 15:33:33.447364-05', '7', 'ABRIL005', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (12, '2016-07-13 15:33:36.458518-05', '7', 'ABRIL005', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (13, '2016-07-13 15:34:07.188894-05', '5', 'ABIGAÍL006', 2, 'Modificado/a username.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (14, '2016-07-13 15:34:41.339897-05', '8', 'BALDOMERO007', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (15, '2016-07-13 15:34:43.845806-05', '8', 'BALDOMERO007', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (16, '2016-07-13 15:34:54.940837-05', '9', 'BALTASAR', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (17, '2016-07-13 15:34:58.446585-05', '9', 'BALTASAR', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (18, '2016-07-13 15:35:17.123752-05', '9', 'BALTASAR008', 2, 'Modificado/a username.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (19, '2016-07-13 15:35:32.295986-05', '10', 'BÁRBARA009', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (20, '2016-07-13 15:35:34.62322-05', '10', 'BÁRBARA009', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (21, '2016-07-13 15:36:05.562777-05', '11', 'BENITO010', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (22, '2016-07-13 15:36:09.779972-05', '11', 'BENITO010', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (23, '2016-07-13 15:36:36.080496-05', '12', 'CALIXTO011', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (24, '2016-07-13 15:36:39.662904-05', '12', 'CALIXTO011', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (25, '2016-07-13 15:36:57.135165-05', '13', 'CAMELIA012', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (26, '2016-07-13 15:36:59.955247-05', '13', 'CAMELIA012', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (27, '2016-07-13 15:37:17.09578-05', '14', 'CAMILA013', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (28, '2016-07-13 15:37:19.567061-05', '14', 'CAMILA013', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (29, '2016-07-13 15:37:34.263761-05', '15', 'CARIDAD014', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (30, '2016-07-13 15:37:36.942708-05', '15', 'CARIDAD014', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (31, '2016-07-13 15:37:51.154547-05', '16', 'CASILDA015', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (32, '2016-07-13 15:37:54.584528-05', '16', 'CASILDA015', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (33, '2016-07-13 15:38:23.465683-05', '17', 'CASIMIRO016', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (34, '2016-07-13 15:38:26.115748-05', '17', 'CASIMIRO016', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (35, '2016-07-13 15:38:42.486023-05', '18', 'CATALINA017', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (36, '2016-07-13 15:39:01.778803-05', '18', 'CATALINA017', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (37, '2016-07-13 15:39:16.827965-05', '19', '018', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (38, '2016-07-13 15:39:19.818815-05', '19', '018', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (39, '2016-07-13 15:39:40.80246-05', '20', 'CECILIA019', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (40, '2016-07-13 15:39:44.057168-05', '20', 'CECILIA019', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (41, '2016-07-13 15:40:10.972153-05', '21', 'EDGARDO020', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (42, '2016-07-13 15:40:14.451926-05', '21', 'EDGARDO020', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (43, '2016-07-13 15:40:47.936364-05', '22', 'EUFEMIO021', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (44, '2016-07-13 15:40:50.737595-05', '22', 'EUFEMIO021', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (45, '2016-07-13 15:41:27.082648-05', '23', 'EVA022', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (46, '2016-07-13 15:41:30.150305-05', '23', 'EVA022', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (47, '2016-07-13 15:41:57.533619-05', '24', 'MACARENA023', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (48, '2016-07-13 15:42:01.440838-05', '24', 'MACARENA023', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (49, '2016-07-13 15:42:18.615928-05', '25', 'MARCELO024', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (50, '2016-07-13 15:42:21.66194-05', '25', 'MARCELO024', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (51, '2016-07-13 15:42:41.079077-05', '26', 'pamela025', 1, 'Añadido.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (52, '2016-07-13 15:42:44.405281-05', '26', 'pamela025', 2, 'No ha cambiado ningún campo.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (53, '2016-07-13 15:43:51.127087-05', '19', 'Nicholas018', 2, 'Modificado/a username.', 4, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (54, '2016-07-13 16:09:20.011986-05', '1', 'Comida china', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (55, '2016-07-13 16:11:02.180459-05', '1', 'restaurante', 1, 'Añadido.', 7, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (56, '2016-07-13 16:11:08.675638-05', '1', 'Comida china', 2, 'No ha cambiado ningún campo.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (57, '2016-07-13 16:11:32.060677-05', '1', 'AARON001', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (58, '2016-07-13 16:12:55.621002-05', '1', 'Casa china', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (59, '2016-07-13 16:13:25.500106-05', '2', 'ABEL002', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (60, '2016-07-13 16:13:48.966047-05', '3', 'ABELARDO003', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (61, '2016-07-13 16:14:15.786223-05', '4', 'ABRAHAM004', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (62, '2016-07-13 16:14:34.654288-05', '5', 'ABRIL005', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (63, '2016-07-13 16:14:57.255718-05', '6', 'ABIGAÍL006', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (64, '2016-07-13 16:15:24.369773-05', '7', 'BALDOMERO007', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (65, '2016-07-13 16:15:45.455939-05', '8', 'BALTASAR008', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (66, '2016-07-13 16:16:26.546941-05', '9', 'BÁRBARA009', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (67, '2016-07-13 16:16:50.623982-05', '10', 'BENITO010', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (68, '2016-07-13 16:17:13.137325-05', '11', 'CALIXTO011', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (69, '2016-07-13 16:21:33.321755-05', '12', 'CAMELIA012', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (70, '2016-07-13 16:21:59.420376-05', '13', 'CAMILA013', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (71, '2016-07-13 16:22:50.94534-05', '14', 'CARIDAD014', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (72, '2016-07-13 16:23:16.140772-05', '15', 'CASILDA015', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (73, '2016-07-13 16:23:36.390338-05', '16', 'CATALINA017', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (74, '2016-07-13 16:23:36.54652-05', '17', 'CATALINA017', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (75, '2016-07-13 16:23:59.877425-05', '18', 'Nicholas018', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (76, '2016-07-13 16:24:23.579056-05', '19', 'CECILIA019', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (77, '2016-07-13 16:24:42.157002-05', '20', 'EDGARDO020', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (78, '2016-07-13 16:25:13.655583-05', '21', 'EUFEMIO021', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (79, '2016-07-13 16:25:39.765507-05', '22', 'MACARENA023', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (80, '2016-07-13 16:26:07.489301-05', '23', 'MARCELO024', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (81, '2016-07-13 16:26:32.773969-05', '24', 'pamela025', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (82, '2016-07-13 16:27:30.3865-05', '2', 'pizzeria', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (83, '2016-07-13 16:28:23.323972-05', '2', 'Pizza', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (84, '2016-07-13 16:28:52.10775-05', '3', 'Heladeria', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (85, '2016-07-13 16:29:30.009341-05', '3', 'Ice cream Social', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (86, '2016-07-13 16:30:25.221756-05', '4', 'Comida tipica', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (87, '2016-07-13 16:30:37.960936-05', '4', 'Mogi Mirim', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (88, '2016-07-13 16:31:16.317941-05', '5', 'Papas Fritas', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (89, '2016-07-13 16:31:25.135055-05', '5', 'papaloca', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (90, '2016-07-13 16:32:42.671801-05', '6', 'Comida Rapida', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (91, '2016-07-13 16:33:13.667237-05', '6', 'Porky''s', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (92, '2016-07-13 16:34:02.996855-05', '6', 'ABIGAÍL006', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (93, '2016-07-13 16:34:45.569677-05', '7', 'kitchen next door', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (94, '2016-07-13 16:35:52.291265-05', '7', 'Comida Lojana', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (95, '2016-07-13 16:37:14.789471-05', '7', 'Comida gourmet', 2, 'Modificado/a categoria y detalles.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (96, '2016-07-13 16:38:21.770198-05', '8', 'especialidades', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (97, '2016-07-13 16:38:45.785681-05', '9', 'familiar', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (98, '2016-07-13 16:39:17.30271-05', '9', 'familiar', 2, 'Modificado/a detalles.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (99, '2016-07-13 16:40:21.172633-05', '8', 'Splendore', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (100, '2016-07-13 16:41:11.827015-05', '9', 'Champur', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (101, '2016-07-13 16:42:25.744524-05', '10', 'Chamelion', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (102, '2016-07-13 16:43:09.795843-05', '11', 'Open kitchen', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (103, '2016-07-13 16:43:58.064411-05', '12', 'Tony''s', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (104, '2016-07-13 16:44:59.913187-05', '13', 'Los llanos', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (105, '2016-07-13 16:46:04.595204-05', '14', 'Great Fruit', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (106, '2016-07-13 16:47:20.018964-05', '15', 'La casona', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (107, '2016-07-13 16:49:49.963308-05', '16', 'CASIMIRO016', 2, 'Modificado/a ruta_foto, id_usuario y sim_dispositivo.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (108, '2016-07-13 16:50:27.74324-05', '16', 'Restaurant', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (109, '2016-07-13 16:50:32.289617-05', '16', 'Restaurant', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (110, '2016-07-13 16:52:23.69847-05', '10', 'Mariscos', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (111, '2016-07-13 16:52:37.43064-05', '17', 'Flying fish', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (112, '2016-07-13 16:53:42.124122-05', '11', 'Comida mexicana', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (113, '2016-07-13 16:54:00.206825-05', '18', 'chicks ''n salsa', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (114, '2016-07-13 16:54:49.760392-05', '19', 'dine out', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (115, '2016-07-13 16:55:39.853836-05', '12', 'Comida vegetaria', 1, 'Añadido.', 9, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (116, '2016-07-13 16:55:48.035835-05', '20', 'The garden', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (117, '2016-07-13 16:57:31.065572-05', '21', 'pecmopahhblu beumunt', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (118, '2016-07-13 16:59:11.927264-05', '21', 'EUFEMIO021', 2, 'Modificado/a sim_dispositivo.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (119, '2016-07-13 17:00:12.60852-05', '25', 'EVA022', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (120, '2016-07-13 17:01:31.428421-05', '22', 'hot Spicy', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (121, '2016-07-13 17:02:28.353741-05', '23', 'Bonafide', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (122, '2016-07-13 17:03:17.794274-05', '24', 'Bringes', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (123, '2016-07-13 17:04:25.902722-05', '25', 'Fonda del Castillo', 1, 'Añadido.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (124, '2016-07-13 17:12:36.095417-05', '1', 'Chaulafan', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (125, '2016-07-13 17:12:43.216161-05', '2', 'Carnes', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (126, '2016-07-13 17:13:02.966249-05', '3', 'Sopas', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (127, '2016-07-13 17:13:13.468677-05', '4', 'Bebidas', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (128, '2016-07-13 17:17:16.791924-05', '1', 'Chaulafan especial', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (129, '2016-07-13 17:18:11.421768-05', '1', 'Pequeño', 1, 'Añadido.', 24, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (130, '2016-07-13 17:18:17.731369-05', '2', 'Mediano', 1, 'Añadido.', 24, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (131, '2016-07-13 17:18:22.149804-05', '3', 'Grande', 1, 'Añadido.', 24, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (132, '2016-07-13 17:18:33.832878-05', '4', 'Estandar', 1, 'Añadido.', 24, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (133, '2016-07-13 17:19:44.071093-05', '1', 'Chaulafan especial : Estandar - 4.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (134, '2016-07-13 17:19:57.183037-05', '2', 'Chaulafan especial : Mediano - 3.25', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (135, '2016-07-13 17:20:34.801423-05', '3', 'Chaulafan especial : Grande - 6.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (136, '2016-07-13 17:21:21.451862-05', '5', 'Mariscos', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (137, '2016-07-13 17:22:12.50615-05', '2', 'Brocheta de Carne', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (138, '2016-07-13 17:22:29.073226-05', '4', 'Brocheta de Carne : Estandar - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (139, '2016-07-13 17:22:29.234381-05', '5', 'Brocheta de Carne : Estandar - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (140, '2016-07-13 17:24:46.096774-05', '3', 'Chaulafan de camaron', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (141, '2016-07-13 17:25:00.018472-05', '6', 'Chaulafan de camaron : Estandar - 5.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (142, '2016-07-13 17:25:14.602685-05', '7', 'Chaulafan de camaron : Grande - 7.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (143, '2016-07-13 17:26:32.967048-05', '6', 'embutidos', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (144, '2016-07-13 17:26:38.838885-05', '4', 'pizza de chorizo', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (145, '2016-07-13 17:26:57.494322-05', '8', 'pizza de chorizo : Pequeño - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (146, '2016-07-13 17:26:57.651502-05', '9', 'pizza de chorizo : Pequeño - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (147, '2016-07-13 17:27:10.612312-05', '10', 'pizza de chorizo : Mediano - 10.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (148, '2016-07-13 17:27:22.359626-05', '11', 'pizza de chorizo : Grande - 20.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (149, '2016-07-13 17:30:40.60323-05', '7', 'pizza', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (150, '2016-07-13 17:31:24.676415-05', '5', 'pizza de queso', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (151, '2016-07-13 17:31:39.444539-05', '12', 'pizza de queso : Estandar - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (152, '2016-07-13 17:31:51.028124-05', '13', 'pizza de queso : Mediano - 10.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (153, '2016-07-13 17:32:01.4279-05', '14', 'pizza de queso : Grande - 20.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (154, '2016-07-13 17:33:43.78383-05', '6', 'pizza Hawaiana', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (155, '2016-07-13 17:34:00.56573-05', '15', 'pizza Hawaiana : Estandar - 6.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (156, '2016-07-13 17:34:18.347292-05', '16', 'pizza Hawaiana : Mediano - 12.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (157, '2016-07-13 17:34:29.326969-05', '17', 'pizza Hawaiana : Grande - 22.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (158, '2016-07-13 17:35:19.924181-05', '8', 'helados', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (159, '2016-07-13 17:35:24.813496-05', '7', 'Helado napolitano', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (160, '2016-07-13 17:35:40.394906-05', '18', 'Helado napolitano : Estandar - 1.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (161, '2016-07-13 17:36:15.520318-05', '8', 'Helado de chocolate', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (162, '2016-07-13 17:36:30.480338-05', '19', 'Helado de chocolate : Estandar - 1.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (163, '2016-07-13 17:37:11.724564-05', '9', 'Helado vainilla', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (164, '2016-07-13 17:37:31.358972-05', '20', 'Helado vainilla : Estandar - 1.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (165, '2016-07-13 17:38:35.63115-05', '9', 'Papas', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (166, '2016-07-13 17:38:45.374828-05', '10', 'Papas a la francesa', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (167, '2016-07-13 17:39:02.873525-05', '21', 'Papas a la francesa : Grande - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (168, '2016-07-13 17:40:11.542166-05', '11', 'Filet mignon', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (169, '2016-07-13 17:40:37.567847-05', '22', 'Filet mignon : Estandar - 7.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (170, '2016-07-13 17:40:48.3564-05', '23', 'Filet mignon : Grande - 10.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (171, '2016-07-13 17:41:46.20602-05', '12', 'Pollo a la plancha', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (172, '2016-07-13 17:42:11.188421-05', '24', 'Pollo a la plancha : Estandar - 4.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (173, '2016-07-13 17:42:55.289405-05', '13', 'Papa simple', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (174, '2016-07-13 17:43:11.872797-05', '25', 'Papa simple : Estandar - 0.75', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (175, '2016-07-13 17:44:32.700927-05', '14', 'salchi papa', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (176, '2016-07-13 17:44:45.615546-05', '26', 'salchi papa : Estandar - 1.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (177, '2016-07-13 17:45:20.927606-05', '15', 'chori papa', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (178, '2016-07-13 17:45:41.667614-05', '27', 'chori papa : Estandar - 1.25', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (179, '2016-07-13 17:47:36.265307-05', '10', 'hamburguesas', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (180, '2016-07-13 17:47:42.333122-05', '16', 'hamburguesa solita', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (181, '2016-07-13 17:47:58.43105-05', '28', 'hamburguesa solita : Estandar - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (182, '2016-07-13 17:49:30.882983-05', '17', 'hamburguesa doble', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (183, '2016-07-13 17:49:52.331044-05', '29', 'hamburguesa doble : Estandar - 6.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (184, '2016-07-13 17:50:42.14082-05', '18', 'Carne asada', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (185, '2016-07-13 17:50:56.134258-05', '30', 'Carne asada : Estandar - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (186, '2016-07-13 17:58:05.592539-05', '1', 'Loja', 1, 'Añadido.', 11, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (187, '2016-07-13 17:58:40.803826-05', '1', 'colon - Leon', 1, 'Añadido.', 12, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (188, '2016-07-13 17:59:48.282457-05', '1', 'casa china N', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (189, '2016-07-13 18:09:37.705982-05', '26', 'admin', 1, 'Añadido.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (190, '2016-07-13 18:11:17.741881-05', '2', 'Pizza', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (191, '2016-07-13 18:12:31.957631-05', '3', 'Ice Cream Social', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (192, '2016-07-13 18:13:12.16681-05', '4', 'Mogi Mirim', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (193, '2016-07-13 18:13:52.196814-05', '5', 'papaloca', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (194, '2016-07-13 18:14:38.70058-05', '6', 'Porky''s', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (195, '2016-07-13 18:15:20.727299-05', '7', 'kitchen next door', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (196, '2016-07-13 18:15:58.932312-05', '8', 'Splendore', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (197, '2016-07-13 18:16:58.909984-05', '9', 'Champur', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (198, '2016-07-13 18:17:33.838163-05', '10', 'Chamelion', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (199, '2016-07-13 18:18:11.628193-05', '11', 'Open Kitchen', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (200, '2016-07-13 18:18:49.49367-05', '12', 'Tony''s', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (201, '2016-07-13 18:19:29.796429-05', '13', 'Los Llanos', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (202, '2016-07-13 18:20:12.905198-05', '14', 'Great Fruit', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (203, '2016-07-13 18:20:57.499753-05', '15', 'La Casona', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (204, '2016-07-13 18:21:38.625832-05', '16', 'Restaurant', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (205, '2016-07-13 18:22:29.947813-05', '17', 'Flying Fish', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (206, '2016-07-13 18:23:12.48494-05', '18', 'Chicks ''N Salsa', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (207, '2016-07-13 18:23:55.603666-05', '19', 'Dine Out', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (208, '2016-07-13 18:24:43.651756-05', '20', 'The Garden', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (209, '2016-07-13 18:25:34.804446-05', '21', 'Pecmopahhblu Beumunt', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (210, '2016-07-13 18:26:30.733775-05', '22', 'Hot Spicy', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (211, '2016-07-13 18:27:07.312531-05', '23', 'Bonafide', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (212, '2016-07-13 18:27:41.295012-05', '24', 'Bringes', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (213, '2016-07-13 18:28:10.211903-05', '25', 'Fonda del Castillo', 1, 'Añadido.', 15, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (214, '2016-07-13 18:30:47.27002-05', '19', 'Rollo Primavera', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (215, '2016-07-13 18:31:02.070494-05', '31', 'Rollo Primavera : Estandar - 3.75', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (216, '2016-07-13 18:32:16.971357-05', '11', 'Pastas', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (217, '2016-07-13 18:32:20.812792-05', '20', 'espagueti con carne', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (218, '2016-07-13 18:32:34.893203-05', '32', 'espagueti con carne : Estandar - 4.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (219, '2016-07-13 18:33:20.095568-05', '21', 'espagueti con camaron', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (220, '2016-07-13 18:33:32.722332-05', '33', 'espagueti con camaron : Estandar - 6.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (221, '2016-07-13 18:34:30.955931-05', '22', 'espagueti con albóndigas', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (222, '2016-07-13 18:34:42.889182-05', '34', 'espagueti con albóndigas : Estandar - 7.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (223, '2016-07-13 18:35:43.321403-05', '23', 'Taco de carnita', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (224, '2016-07-13 18:35:54.395686-05', '35', 'Taco de carnita : Estandar - 4.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (225, '2016-07-13 18:36:40.026697-05', '24', 'Papas fritas', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (226, '2016-07-13 18:36:53.407479-05', '36', 'Papas fritas : Mediano - 3.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (227, '2016-07-13 18:38:26.319115-05', '25', 'Pollo picante', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (228, '2016-07-13 18:38:42.306869-05', '37', 'Pollo picante : Estandar - 3.75', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (229, '2016-07-13 18:40:08.54624-05', '26', 'Brochetas de  Pollo & Jalpeño', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (230, '2016-07-13 18:40:24.651468-05', '38', 'Brochetas de  Pollo & Jalpeño : Estandar - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (231, '2016-07-13 18:42:44.779897-05', '27', 'Sopa de pollo', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (232, '2016-07-13 18:42:57.299475-05', '39', 'Sopa de pollo : Estandar - 1.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (233, '2016-07-13 18:43:37.184404-05', '28', 'Sopa de Vegetales', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (234, '2016-07-13 18:44:05.94324-05', '40', 'Sopa de Vegetales : Estandar - 2.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (235, '2016-07-13 18:45:32.528402-05', '29', 'pizza Hawaiana', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (236, '2016-07-13 18:45:50.863844-05', '41', 'pizza Hawaiana : Pequeño - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (237, '2016-07-13 18:46:02.147294-05', '42', 'pizza Hawaiana : Mediano - 10.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (238, '2016-07-13 18:46:54.893673-05', '30', 'Carne al jugo', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (239, '2016-07-13 18:47:05.36129-05', '43', 'Carne al jugo : Estandar - 2.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (240, '2016-07-13 18:49:05.025102-05', '31', 'Taco al pastor', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (241, '2016-07-13 18:49:22.89936-05', '44', 'Taco al pastor : Estandar - 2.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (242, '2016-07-13 18:49:56.242988-05', '32', 'Sopa de la casa', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (243, '2016-07-13 18:50:11.556252-05', '45', 'Sopa de la casa : Estandar - 1.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (244, '2016-07-13 18:50:36.453585-05', '32', 'Sopa de la casa', 2, 'Modificado/a ruta_foto.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (245, '2016-07-13 22:25:39.888304-05', '13', 'CAMILA013', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (246, '2016-07-13 22:25:51.286496-05', '12', 'Tony''s', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (247, '2016-07-13 22:27:31.967585-05', '13', 'Los llanos', 2, 'Modificado/a id_perfil.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (248, '2016-07-13 22:45:54.062132-05', '14', 'CARIDAD014', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (249, '2016-07-13 22:45:56.813859-05', '13', 'Los llanos', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (250, '2016-07-13 22:50:33.044569-05', '12', 'CAMELIA012', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (251, '2016-07-13 22:50:35.898272-05', '12', 'Tony''s', 2, 'Modificado/a id_perfil.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (252, '2016-07-13 22:51:23.938864-05', '13', 'CAMILA013', 2, 'No ha cambiado ningún campo.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (253, '2016-07-13 22:51:26.530596-05', '13', 'Los llanos', 2, 'Modificado/a id_perfil.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (254, '2016-07-13 22:52:00.605571-05', '14', 'CARIDAD014', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (255, '2016-07-13 22:52:03.959988-05', '14', 'Great Fruit', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (256, '2016-07-13 22:52:45.274585-05', '20', 'EDGARDO020', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (257, '2016-07-13 22:53:33.964749-05', '12', 'CAMELIA012', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (258, '2016-07-13 22:53:36.796706-05', '12', 'Tony''s', 2, 'No ha cambiado ningún campo.', 10, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (259, '2016-07-13 22:54:05.589866-05', '13', 'CAMILA013', 2, 'Modificado/a ruta_foto.', 8, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (260, '2016-07-13 22:56:23.641586-05', '12', 'Ensaladas', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (261, '2016-07-13 22:56:28.246576-05', '33', 'Ensalada de frutas Junior', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (262, '2016-07-13 22:56:50.803347-05', '46', 'Ensalada de frutas Junior : Estandar - 4.9', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (263, '2016-07-13 22:58:08.640152-05', '34', 'Ensalada de frutas especial', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (264, '2016-07-13 22:58:28.627775-05', '47', 'Ensalada de frutas especial : Estandar - 5.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (265, '2016-07-13 23:01:02.369197-05', '13', 'Nachos', 1, 'Añadido.', 22, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (266, '2016-07-13 23:01:05.844886-05', '35', 'Nacho carnita', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (267, '2016-07-13 23:01:18.904902-05', '48', 'Nacho carnita : Estandar - 4.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (268, '2016-07-13 23:06:34.555889-05', '36', 'Encebollado', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (269, '2016-07-13 23:06:48.738725-05', '49', 'Encebollado : Estandar - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (270, '2016-07-13 23:06:48.909083-05', '50', 'Encebollado : Estandar - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (271, '2016-07-13 23:07:09.276454-05', '51', 'Encebollado : Grande - 7.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (272, '2016-07-13 23:07:50.731597-05', '37', 'Guatita', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (273, '2016-07-13 23:08:13.742756-05', '52', 'Guatita : Estandar - 2.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (274, '2016-07-13 23:08:30.410501-05', '53', 'Guatita : Grande - 3.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (275, '2016-07-13 23:09:47.580359-05', '38', 'Cecina', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (276, '2016-07-13 23:10:06.062623-05', '54', 'Cecina : Pequeño - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (277, '2016-07-13 23:10:21.021035-05', '55', 'Cecina : Mediano - 6.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (278, '2016-07-13 23:10:39.553607-05', '56', 'Cecina : Grande - 8.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (279, '2016-07-13 23:14:36.775883-05', '39', 'Seco de pollo', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (280, '2016-07-13 23:14:51.253658-05', '57', 'Seco de pollo : Estandar - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (281, '2016-07-13 23:15:49.552051-05', '40', 'Encocado de camaron', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (282, '2016-07-13 23:16:08.23211-05', '58', 'Encocado de camaron : Estandar - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (283, '2016-07-13 23:17:53.699275-05', '41', 'Encocado de pescado', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (284, '2016-07-13 23:18:14.267971-05', '59', 'Encocado de pescado : Estandar - 4.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (285, '2016-07-13 23:19:11.013708-05', '42', 'Encocado de Cangreo', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (286, '2016-07-13 23:19:29.360436-05', '60', 'Encocado de Cangreo : Estandar - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (287, '2016-07-13 23:21:12.104354-05', '43', 'Ceviche de Camaron', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (288, '2016-07-13 23:21:27.328355-05', '61', 'Ceviche de Camaron : Estandar - 4.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (289, '2016-07-13 23:21:27.494692-05', '62', 'Ceviche de Camaron : Estandar - 4.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (290, '2016-07-13 23:25:17.104168-05', '44', 'Taco de carne', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (291, '2016-07-13 23:25:47.835208-05', '63', 'Taco de carne : Estandar - 3.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (292, '2016-07-13 23:26:35.611273-05', '45', 'Coca Cola', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (293, '2016-07-13 23:26:55.296365-05', '64', 'Coca Cola : Estandar - 0.6', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (294, '2016-07-13 23:27:11.971491-05', '65', 'Coca Cola : Mediano - 1.1', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (295, '2016-07-13 23:27:24.703773-05', '66', 'Coca Cola : Grande - 2.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (296, '2016-07-13 23:28:44.418015-05', '46', 'Avena', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (297, '2016-07-13 23:29:00.359324-05', '67', 'Avena : Estandar - 1.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (298, '2016-07-13 23:29:38.12102-05', '47', 'Batido de mora', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (299, '2016-07-13 23:29:55.277688-05', '68', 'Batido de mora : Estandar - 1.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (300, '2016-07-13 23:30:47.746786-05', '48', 'Rollitos de carne', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (301, '2016-07-13 23:31:10.107935-05', '69', 'Rollitos de carne : Estandar - 3.25', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (302, '2016-07-13 23:32:04.911396-05', '49', 'Jugo de mango', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (303, '2016-07-13 23:33:06.648491-05', '50', 'Fuente de frutas', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (304, '2016-07-13 23:34:24.208085-05', '51', 'Palmera de frutas', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (305, '2016-07-13 23:34:43.175235-05', '70', 'Jugo de mango : Estandar - 1.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (306, '2016-07-13 23:34:57.121053-05', '71', 'Fuente de frutas : Estandar - 2.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (307, '2016-07-13 23:35:07.991892-05', '72', 'Palmera de frutas : Estandar - 1.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (308, '2016-07-13 23:35:37.774704-05', '51', 'Palmera de frutas', 2, 'Modificado/a ruta_foto.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (309, '2016-07-13 23:36:42.042779-05', '52', 'Filet mignon', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (310, '2016-07-13 23:39:55.237712-05', '53', 'Pasta a la boloñesa', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (311, '2016-07-13 23:40:15.449508-05', '73', 'Pasta a la boloñesa : Estandar - 7.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (312, '2016-07-13 23:40:32.504761-05', '74', 'Pasta a la boloñesa : Estandar - 10.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (313, '2016-07-13 23:40:49.396967-05', '74', 'Filet mignon : Estandar - 10.0', 2, 'Modificado/a id_plato.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (314, '2016-07-13 23:42:10.510172-05', '54', 'Cecina', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (315, '2016-07-13 23:43:06.764562-05', '55', 'hamburguesa de queso', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (316, '2016-07-13 23:43:27.459964-05', '75', 'Cecina : Estandar - 8.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (317, '2016-07-13 23:43:27.624097-05', '76', 'Cecina : Estandar - 8.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (318, '2016-07-13 23:43:44.696384-05', '77', 'hamburguesa de queso : Estandar - 5.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (319, '2016-07-13 23:44:09.639539-05', '55', 'hamburguesa de queso', 2, 'Modificado/a id_menu.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (320, '2016-07-13 23:45:52.102255-05', '56', 'Arroz relleno', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (321, '2016-07-13 23:47:20.094664-05', '57', 'espagueti al ajillo', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (322, '2016-07-13 23:47:33.713399-05', '78', 'Arroz relleno : Estandar - 2.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (323, '2016-07-13 23:47:44.853094-05', '79', 'espagueti al ajillo : Estandar - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (324, '2016-07-13 23:48:51.471786-05', '58', 'Avena', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (325, '2016-07-13 23:49:29.819341-05', '59', 'Jugo de mango', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (326, '2016-07-13 23:50:26.416409-05', '60', 'Pizza de salami', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (327, '2016-07-13 23:51:05.622885-05', '61', 'Sopa marinera', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (328, '2016-07-13 23:51:29.723804-05', '80', 'Avena : Estandar - 1.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (329, '2016-07-13 23:51:44.057032-05', '81', 'Jugo de mango : Estandar - 1.25', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (330, '2016-07-13 23:52:10.929186-05', '82', 'Pizza de salami : Mediano - 10.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (331, '2016-07-13 23:52:24.83468-05', '83', 'Pizza de salami : Grande - 20.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (332, '2016-07-13 23:52:40.099568-05', '84', 'Sopa marinera : Estandar - 3.0', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (333, '2016-07-13 23:53:42.802252-05', '62', 'Filete de pollo', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (334, '2016-07-13 23:54:40.889394-05', '63', 'espagueti con carne', 1, 'Añadido.', 23, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (335, '2016-07-13 23:55:09.745022-05', '85', 'Filete de pollo : Estandar - 3.5', 1, 'Añadido.', 25, 1);
INSERT INTO django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) VALUES (336, '2016-07-13 23:55:28.233679-05', '86', 'espagueti con carne : Estandar - 4.0', 1, 'Añadido.', 25, 1);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 336, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_content_type_id_seq', 37, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO django_migrations (id, app, name, applied) VALUES (1, 'contenttypes', '0001_initial', '2016-07-13 15:29:11.461067-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (2, 'auth', '0001_initial', '2016-07-13 15:29:12.255774-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (3, 'admin', '0001_initial', '2016-07-13 15:29:12.436326-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2016-07-13 15:29:12.485678-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (5, 'contenttypes', '0002_remove_content_type_name', '2016-07-13 15:29:12.559101-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (6, 'auth', '0002_alter_permission_name_max_length', '2016-07-13 15:29:12.592092-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (7, 'auth', '0003_alter_user_email_max_length', '2016-07-13 15:29:12.624951-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (8, 'auth', '0004_alter_user_username_opts', '2016-07-13 15:29:12.646788-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (9, 'auth', '0005_alter_user_last_login_null', '2016-07-13 15:29:12.674271-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (10, 'auth', '0006_require_contenttypes_0002', '2016-07-13 15:29:12.682697-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (11, 'auth', '0007_alter_validators_add_error_messages', '2016-07-13 15:29:12.713158-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (12, 'default', '0001_initial', '2016-07-13 15:29:13.2454-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (13, 'default', '0002_add_related_name', '2016-07-13 15:29:13.300258-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (14, 'default', '0003_alter_email_max_length', '2016-07-13 15:29:13.326276-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (15, 'default', '0004_auto_20160423_0400', '2016-07-13 15:29:13.359228-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (16, 'sessions', '0001_initial', '2016-07-13 15:29:13.50083-05');
INSERT INTO django_migrations (id, app, name, applied) VALUES (17, 'spaapp', '0001_initial', '2016-07-13 15:29:17.206588-05');


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('django_migrations_id_seq', 17, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('mtpyaj1bg7kilu74wjuetwjqkw5h8ul7', 'N2MyMjc4Y2RkM2NiNWVjOWY0OTBkNDkwYzYyNDEwMzVlNTA5NmZjNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImFhNjgxNTQ0NzIyNjg5NjA0NWNhNWM4MThhNzM4ODliNzdkZGE5OTgiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=', '2016-07-27 17:53:00.162033-05');


--
-- Data for Name: social_auth_association; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: social_auth_association_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_association_id_seq', 1, false);


--
-- Data for Name: social_auth_code; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: social_auth_code_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_code_id_seq', 1, false);


--
-- Data for Name: social_auth_nonce; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_nonce_id_seq', 1, false);


--
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('social_auth_usersocialauth_id_seq', 1, false);


--
-- Data for Name: spaapp_actividadcomercial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_actividadcomercial (id_actividad, tipo_actividad, detalles) VALUES (1, 'restaurante', 'restaurantes');


--
-- Name: spaapp_actividadcomercial_id_actividad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_actividadcomercial_id_actividad_seq', 1, true);


--
-- Data for Name: spaapp_categoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (1, 'Comida china', 'venta de comida china en general');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (2, 'pizzeria', 'venta de pizza');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (3, 'Heladeria', 'venta de helados');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (4, 'Comida tipica', 'comida nacional');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (5, 'Papas Fritas', 'venta de papas fritas');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (6, 'Comida Rapida', 'cangreburguers y mas');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (7, 'Comida gourmet', 'ofrecer platillos que atraen a personas aficionadas a comer marjales delicados. El servicio y los precios están de acuerdo con la calidad de la comida, por lo que estos restaurantes son los más caros.');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (8, 'especialidades', 'ofrece una variedad limitada o estilo de cocina. Estos establecimientos muestran en su carta una extensa variedad de su especialidad, ya sean mariscos, aves, carnes o pastas, entre otros posibles');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (9, 'familiar', 'sirve alimentos sencillos a precios moderados, accesibles a la familia.');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (10, 'Mariscos', 'venta de comida del mar');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (11, 'Comida mexicana', 'comida mexicana en general');
INSERT INTO spaapp_categoria (id_categoria, categoria, detalles) VALUES (12, 'Comida vegetaria', 'venta de comida vegetariana');


--
-- Name: spaapp_categoria_id_categoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_categoria_id_categoria_seq', 12, true);


--
-- Data for Name: spaapp_ciudad; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_ciudad (id_ciudad, nombre, descripcion) VALUES (1, 'Loja', 'castellana');


--
-- Name: spaapp_ciudad_id_ciudad_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ciudad_id_ciudad_seq', 1, true);


--
-- Data for Name: spaapp_perfil; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (1, '1102394200', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest1.png', '01', 2);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (2, '1102394200', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest2.jpg', '02', 3);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (3, '1104039288', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest3.jpg', '03', 4);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (4, '1102394200', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest4.png', '04', 6);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (5, '1102394200', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest5.jpg', '05', 7);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (7, '1104928322', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest7.jpg', '07', 8);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (8, '1104039288', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest8.jpg', '08', 9);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (9, '1104039288', '2016-07-13', '2345384', 'Masculino', 'usuarioFoto/rest9.jpg', '09', 10);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (10, '1104039288', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest10.jpg', '10', 11);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (11, '323232', '2016-07-13', '2045345', 'Masculino', 'usuarioFoto/rest11.jpg', '11', 12);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (15, '1104039288', '2016-07-13', '2045345', 'Masculino', 'usuarioFoto/rest15.jpg', '16', 16);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (17, '1104928322', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest17_N3hF7pF.png', '17', 18);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (18, '1104039288', '2016-07-13', '2045345', 'Masculino', 'usuarioFoto/rest18.jpg', '18', 19);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (19, '1104928322', '2016-07-13', '2045345', 'Masculino', 'usuarioFoto/rest19.jpeg', '19', 20);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (22, '1104928322', '2016-07-13', '2696969', 'Masculino', 'usuarioFoto/rest23.gif', '23', 24);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (23, '1104928322', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest24.gif', '24', 25);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (24, '1102394200', '2016-07-13', '2345384', 'Masculino', 'usuarioFoto/rest25.png', '25', 26);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (6, '1104039288', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest6.jpg', '06', 5);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (16, '1104928322', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest16.gif', '16', 17);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (21, '1102394200', '2016-07-13', '2045345', 'Masculino', 'usuarioFoto/rest21.gif', '21', 22);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (25, '1104039288', '2016-07-13', '2045345', 'Masculino', 'usuarioFoto/rest22.jpg', '22', 23);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (26, '1102394200', '2016-07-13', '2045345', 'Masculino', 'usuarioFoto/usuario_uv81YZd.jpg', '000', 1);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (14, '1104928322', '2016-07-13', '2345384', 'Masculino', 'usuarioFoto/rest14_KJOVvSx.jpg', '14', 15);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (20, '1104039288', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest20.jpg', '20', 21);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (12, '1104928322', '2016-07-13', '2345384', 'Masculino', 'usuarioFoto/rest12_ZKAqP0y.png', '12', 13);
INSERT INTO spaapp_perfil (id_perfil, cedula, fecha_nacimiento, telefono, genero, ruta_foto, sim_dispositivo, id_usuario_id) VALUES (13, '1104039288', '2016-07-13', '2590324', 'Masculino', 'usuarioFoto/rest13_8XZm3WL.jpg', '13', 14);


--
-- Data for Name: spaapp_cliente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_cliente_id_cliente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_cliente_id_cliente_seq', 1, false);


--
-- Data for Name: spaapp_direccion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_direccion (id_direccion, calle_principal, calle_secundaria, numero_casa, latitud, longitud, referencia, id_perfil_id) VALUES (1, 'colon', 'Leon', '1233', '-79,00000', '-3,00000', 'alado del chifa', 1);


--
-- Data for Name: spaapp_proveedor; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (1, 'Casa china', '2345384', '2504000', 1, 1234, 'Ventas', 1, 1, 1);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (2, 'Pizza', '2345384', '2504000', 2, 312, 'ventas', 1, 2, 2);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (3, 'Ice cream Social', '2590324', '2504000', 3, 3124, 'Ventas', 1, 3, 3);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (4, 'Mogi Mirim', '2345384', '2504000', 4, 634, 'Ventas', 1, 4, 4);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (5, 'papaloca', '2590324', '2504000', 5, 753, 'ventas', 1, 5, 5);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (6, 'Porky''s', '2345384', '2504000', 3, 987, 'Ventas', 1, 6, 6);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (7, 'kitchen next door', '2345384', '2504000', 3, 645, 'ventas', 1, 1, 7);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (8, 'Splendore', '2345384', '2504000', 4, 312, 'Ventas', 1, 9, 8);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (9, 'Champur', '2045345', '2504000', 3, 234, 'Ventas', 1, 9, 19);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (10, 'Chamelion', '2045345', '2504000', 456, 343, 'Ventas', 1, 9, 10);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (11, 'Open kitchen', '2045345', '2504000', 5, 676, 'Ventas', 1, 1, 11);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (15, 'La casona', '2045345', '2504000', 4, 566, 'Ventas', 1, 9, 15);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (16, 'Restaurant', '2590324', '2504000', 5, 345, 'Ventas', 1, 8, 16);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (17, 'Flying fish', '2696969', '2504000', 2, 234, 'Ventas', 1, 10, 17);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (18, 'chicks ''n salsa', '2345384', '2504000', 4, 111, 'Ventas', 1, 11, 18);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (19, 'dine out', '2590324', '2504000', 1, 22, 'Ventas', 1, 8, 19);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (20, 'The garden', '2590324', '2504000', 3, 333, 'Ventas', 1, 12, 20);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (21, 'pecmopahhblu beumunt', '2045345', '2504000', 5, 444, 'Ventas', 1, 7, 21);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (22, 'hot Spicy', '2345384', '2504000', 5, 666, '22', 1, 7, 25);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (23, 'Bonafide', '2345384', '2504000', 5, 777, '23', 1, 7, 22);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (24, 'Bringes', '2045345', '2504000', 1, 1234, '24', 1, 8, 23);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (25, 'Fonda del Castillo', '2696969', '2504000', 4, 999, '25', 1, 8, 24);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (13, 'Los llanos', '2564734', '2504000', 4, 312, 'Ventas', 1, 9, 13);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (14, 'Great Fruit', '2696969', '2504000', 4, 809, 'Ventas', 1, 3, 14);
INSERT INTO spaapp_proveedor (id_proveedor, nombre_comercial, telefono, telefono_contacto, prioridad, me_gusta, razon_social, id_actividad_id, id_categoria_id, id_perfil_id) VALUES (12, 'Tony''s', '2345384', '2504000', 2, 312, 'Ventas', 1, 7, 12);


--
-- Data for Name: spaapp_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (1, 1);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (2, 2);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (3, 3);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (4, 4);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (5, 5);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (6, 6);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (7, 7);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (8, 8);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (9, 9);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (10, 10);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (11, 11);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (12, 12);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (13, 13);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (14, 14);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (15, 15);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (16, 16);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (17, 17);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (18, 18);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (19, 19);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (20, 20);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (21, 21);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (22, 22);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (23, 23);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (24, 24);
INSERT INTO spaapp_menu (id_menu, id_proveedor_id) VALUES (25, 25);


--
-- Data for Name: spaapp_sucursal; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (1, 'casa china N', 'Ventas', '2590324', '2504000', 'lunes a viernes', '24h', 1, 1, true, '22:58:45', 1234, 1, 1, 1, 1);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (2, 'Pizza', 'Ventas', '2590324', '2504000', 'lunes a viernes', '24h', 1, 4, true, '23:11:02', 312, 1, 1, 2, 2);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (3, 'Ice Cream Social', 'Ventas', '2590324', '2504000', 'lunes a viernes', '24h', 1, 4, true, '23:12:02', 343, 1, 1, 3, 3);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (4, 'Mogi Mirim', 'Ventas', '2345384', '2504000', 'lunes a viernes', '24h', 1, 3, true, '23:13:05', 1234, 1, 1, 4, 4);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (5, 'papaloca', 'Ventas', '2590324', '2504000', 'lunes a viernes', '24h', 1, 1, true, '23:13:40', 343, 1, 1, 5, 5);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (6, 'Porky''s', 'Ventas', '2345384', '2504000', 'todos', '24h', 1, 5, true, '23:14:14', 343, 1, 1, 6, 6);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (7, 'kitchen next door', 'Ventas', '2345384', '2504000', 'lunes a viernes', '24h', 1, 4, true, '23:15:14', 343, 1, 1, 7, 7);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (8, 'Splendore', 'Ventas', '2345384', '2504000', 'lunes a viernes', '24h', 1, 4, true, '23:15:47', 421, 1, 1, 8, 8);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (9, 'Champur', 'Ventas', '2345384', '2504000', 'lunes a sabado', '08:00-21:00', 1, 0, true, '23:16:15', 234, 1, 1, 9, 9);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (10, 'Chamelion', 'Ventas', '2345384', '2504000', 'lunes a sabado', '08:00-21:00', 1, 123, true, '23:17:24', 234, 1, 1, 10, 10);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (11, 'Open Kitchen', 'Ventas', '2590324', '2504000', 'lunes a viernes', '08:00-21:00', 1, 5, true, '23:18:00', 421, 1, 1, 11, 11);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (12, 'Tony''s', 'Ventas', '2590324', '2504000', 'lunes a sabado', '08:00-21:00', 1, 5, true, '23:18:26', 312, 1, 1, 12, 12);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (13, 'Los Llanos', 'Ventas', '2590324', '2504000', 'lunes a sabado', '08:00-21:00', 1, 4, true, '23:19:16', 234, 1, 1, 13, 13);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (14, 'Great Fruit', 'Ventas', '2345384', '2504000', 'lunes a sabado', '08:00-21:00', 1, 4, true, '23:19:50', 343, 1, 1, 14, 14);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (15, 'La Casona', 'Ventas', '2590324', '2504000', 'lunes a sabado', '24h', 1, 4, true, '23:20:50', 312, 1, 1, 15, 15);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (16, 'Restaurant', 'Ventas', '2345384', '2504000', 'lunes a viernes', '08:00-21:00', 1, 5, true, '23:21:13', 343, 1, 1, 16, 16);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (17, 'Flying Fish', 'Ventas', '2345384', '2504000', 'lunes a sabado', '08:00-21:00', 1, 3, true, '23:22:02', 421, 1, 1, 17, 17);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (18, 'Chicks ''N Salsa', 'Ventas', '2345384', '2504000', 'lunes a viernes', '24h', 1, 1, true, '23:23:07', 3124, 1, 1, 18, 18);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (19, 'Dine Out', 'Ventas', '2590324', '2504000', 'lunes a sabado', '08:00-21:00', 0, 4, true, '23:23:48', 343, 1, 1, 19, 19);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (20, 'The Garden', 'Ventas', '2590324', '2504000', 'lunes a viernes', '08:00-21:00', 1, 4, true, '23:24:26', 312, 1, 1, 20, 20);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (21, 'Pecmopahhblu Beumunt', 'Ventas', '2590324', '2504000', 'lunes a viernes', '08:00-21:00', 1, 5, true, '23:25:28', 312, 1, 1, 21, 21);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (22, 'Hot Spicy', 'Ventas', '2590324', '2504000', 'lunes a sabado', '08:00-21:00', 1, 1, true, '23:26:12', 1234, 1, 1, 22, 22);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (23, 'Bonafide', 'Ventas', '2590324', '2504000', 'lunes a viernes', '24h', NULL, 5, true, '23:26:54', 343, 1, 1, 23, 23);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (24, 'Bringes', 'Ventas', '2345384', '2504000', 'lunes a sabado', '08:00-21:00', 0, 4, true, '23:27:35', 343, 1, 1, 24, 24);
INSERT INTO spaapp_sucursal (id_sucursal, nombre_sucursal, razon_social, telefono, telefono_contacto, dias_atencion, hora_atencion, estado, prioridad, activo, hora_pico, me_gusta, id_ciudad_id, id_direccion_id, id_menu_id, id_proveedor_id) VALUES (25, 'Fonda del Castillo', 'Ventas', '2590324', '2504000', 'lunes a viernes', '24h', 1, 5, true, '23:27:51', 343, 1, 1, 25, 25);


--
-- Data for Name: spaapp_contrato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_contrato_id_contrato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_contrato_id_contrato_seq', 1, false);


--
-- Data for Name: spaapp_coordenadas; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_coordenadas_id_coordenadas_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_coordenadas_id_coordenadas_seq', 1, false);


--
-- Data for Name: spaapp_tipopago; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Data for Name: spaapp_tipopedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Data for Name: spaapp_transportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Data for Name: spaapp_pedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Data for Name: spaapp_platocategoria; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (1, 'Chaulafan');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (2, 'Carnes');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (3, 'Sopas');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (4, 'Bebidas');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (5, 'Mariscos');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (6, 'embutidos');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (7, 'pizza');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (8, 'helados');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (9, 'Papas');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (10, 'hamburguesas');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (11, 'Pastas');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (12, 'Ensaladas');
INSERT INTO spaapp_platocategoria (id_platocategoria, categoria) VALUES (13, 'Nachos');


--
-- Data for Name: spaapp_plato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (1, 'Chaulafan especial', 'menuFoto/plato13.jpg', 'chaulafan con carne', 200, 150, '22:08:34', 140, 100, 1);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (2, 'Brocheta de Carne', 'menuFoto/plato1.jpg', 'Brocheta de carne de res al jugo', 200, 150, '22:20:59', 140, 150, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (3, 'Chaulafan de camaron', 'menuFoto/plato13_rzPoBn2.jpg', 'chulafan con camaron', 200, 200, '22:23:39', 140, 150, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (4, 'pizza de chorizo', 'menuFoto/pizza_IgKG4qJ.jpeg', 'pizza de queso con chorizo', 200, 100, '22:25:57', 140, 150, 6);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (5, 'pizza de queso', 'menuFoto/plato6.jpg', 'pizza de solo queso', 200, 150, '22:31:12', 140, 150, 7);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (6, 'pizza Hawaiana', 'menuFoto/plato5.jpg', 'pizza de piña', 100, 90, '22:33:27', 100, 30, 7);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (7, 'Helado napolitano', 'menuFoto/helados_BJdChJq.jpg', 'Helado napolitano', 50, 40, '22:35:01', 40, 50, 8);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (8, 'Helado de chocolate', 'menuFoto/helados_tG2W85y.jpg', 'chocolate', 60, 50, '22:36:02', 50, 100, 8);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (9, 'Helado vainilla', 'menuFoto/helados_RzOAHxy.jpg', 'vainilla en el helado', 200, 100, '22:37:01', 90, 100, 8);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (10, 'Papas a la francesa', 'menuFoto/papas_hVApEiz.jpg', 'papas fritas', 100, 100, '22:38:11', 90, 100, 9);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (11, 'Filet mignon', 'menuFoto/carnes.jpg', 'filete bien cocinado saber a carne', 200, 190, '22:40:02', 140, 100, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (12, 'Pollo a la plancha', 'menuFoto/plato3.jpg', 'pollo a la plancha con ensalada', 200, 190, '22:41:26', 150, 100, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (13, 'Papa simple', 'menuFoto/papas_EUntVFj.jpg', 'solo papas', 400, 300, '22:42:40', 500, 400, 9);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (14, 'salchi papa', 'menuFoto/papas_gVRnbEk.jpg', 'papa con salchicha', 500, 500, '22:44:20', 500, 500, 9);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (15, 'chori papa', 'menuFoto/papas_J8yi8Bg.jpg', 'papa con chorizo', 600, 500, '22:45:09', 500, 450, 9);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (16, 'hamburguesa solita', 'menuFoto/plato8.jpg', 'hamburguesa con carne y tomate', 300, 230, '22:47:11', 300, 240, 10);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (17, 'hamburguesa doble', 'menuFoto/plato9.jpg', 'hamburguesa con doble carne', 200, 200, '22:48:26', 100, 140, 10);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (18, 'Carne asada', 'menuFoto/plato2.jpg', 'carne bien asada', 600, 500, '22:50:27', 700, 300, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (19, 'Rollo Primavera', 'menuFoto/comidachina_KzlVGo0.jpg', 'rollo de pollo', 200, 200, '23:30:20', 140, 100, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (20, 'espagueti con carne', 'menuFoto/plato15.jpg', 'fideos y carne', 100, 100, '23:31:49', 100, 150, 11);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (21, 'espagueti con camaron', 'menuFoto/plato11.JPG', 'fideos y camaron', 100, 200, '23:33:08', 100, 150, 11);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (22, 'espagueti con albóndigas', 'menuFoto/plato12.jpg', 'albóndigas y fideos', 300, 200, '23:34:19', 140, 100, 11);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (23, 'Taco de carnita', 'menuFoto/plato4.png', 'carne molida y tortilla de maiz', 400, 200, '23:35:22', 100, 400, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (24, 'Papas fritas', 'menuFoto/papas_4TimV9n.jpg', 'Papas fritas con salsas', 200, 200, '23:36:32', 100, 150, 9);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (25, 'Pollo picante', 'menuFoto/plato2_C7c0l29.jpg', 'pollo con aji', 200, 100, '23:38:19', 140, 100, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (26, 'Brochetas de  Pollo & Jalpeño', 'menuFoto/plato1_h7qdYxM.jpg', 'brochetas de pollo con jalapeños', 200, 100, '23:39:32', 100, 100, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (27, 'Sopa de pollo', 'menuFoto/plato20.jpg', 'Sopa con pollo y verduras', 200, 100, '23:42:34', 100, 150, 3);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (28, 'Sopa de Vegetales', 'menuFoto/plato21.jpg', 'Sopa con vegetales', 100, 100, '23:43:25', 90, 150, 3);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (29, 'pizza Hawaiana', 'menuFoto/plato5_tshQtoY.jpg', 'pizza de piña', 200, 100, '23:45:25', 140, 150, 7);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (30, 'Carne al jugo', 'menuFoto/carnes_tentVhQ.jpg', 'carne hecha al jugo', 200, 200, '23:46:44', 100, 150, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (31, 'Taco al pastor', 'menuFoto/plato22.jpg', 'tao con carne', 100, 200, '23:48:59', 100, 150, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (32, 'Sopa de la casa', 'menuFoto/plato21_gdA8o5u.jpg', 'sopa de pollo', 200, 200, '23:49:44', 140, 150, 3);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (33, 'Ensalada de frutas Junior', 'menuFoto/plato18.jpg', 'Quiwi, banana, fresas', 200, 100, '03:55:50', 100, 150, 12);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (34, 'Ensalada de frutas especial', 'menuFoto/plato17.jpg', 'Quiwi, banana, fresas,uva, Yogurt', 100, 100, '03:57:54', 100, 150, 12);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (35, 'Nacho carnita', 'menuFoto/plato23.jpg', 'nachos con carne y vegetales', 500, 200, '04:00:33', 90, 100, 13);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (36, 'Encebollado', 'menuFoto/plato26.jpg', 'encebollado con cebolla', 200, 200, '04:06:21', 140, 150, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (37, 'Guatita', 'menuFoto/plato27.jpg', 'Guatita con limon', 100, 100, '04:07:36', 100, 150, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (38, 'Cecina', 'menuFoto/plato25.jpg', 'Carne bien aliñada y cocinada al humo de la leña', 100, 100, '04:09:36', 100, 150, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (39, 'Seco de pollo', 'menuFoto/plato29.jpg', 'pollo al jugo', 200, 100, '04:14:22', 100, 150, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (40, 'Encocado de camaron', 'menuFoto/plato32.jpeg', 'encocado con camarones al ajillo', 200, 100, '04:15:34', 140, 100, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (41, 'Encocado de pescado', 'menuFoto/plato31.jpg', 'coco y pezcado', 200, 200, '04:17:43', 100, 150, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (42, 'Encocado de Cangreo', 'menuFoto/plato33.jpg', 'Cangrejo encocado', 100, 100, '04:18:43', 100, 90, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (43, 'Ceviche de Camaron', 'menuFoto/plato34.JPG', 'ceviche con cebolla y cmaron', 200, 200, '04:20:57', 140, 150, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (44, 'Taco de carne', 'menuFoto/plato22_vuYy34b.jpg', 'tortilla de maiz con carne y vegetales', 300, 200, '04:24:56', 100, 100, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (45, 'Coca Cola', 'menuFoto/bebida1.jpg', 'coca cola de la vida', 200, 200, '04:26:22', 100, 123, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (46, 'Avena', 'menuFoto/bebida2.jpg', 'vaso de avena', 200, 200, '04:28:29', 140, 150, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (47, 'Batido de mora', 'menuFoto/bebida4.jpg', 'mora en jugo', 200, 200, '04:29:25', 100, 150, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (48, 'Rollitos de carne', 'menuFoto/comidachina_FadBNgm.jpg', 'rollos de cane en tubos de fideo', 200, 100, '04:30:28', 100, 150, 11);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (49, 'Jugo de mango', 'menuFoto/bebida3.jpg', 'mango en jugo', 200, 200, '04:31:48', 140, 150, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (50, 'Fuente de frutas', 'menuFoto/plato19.jpg', 'Frutas variadas', 200, 200, '04:32:30', 500, 140, 12);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (51, 'Palmera de frutas', 'menuFoto/plato16.jpg', 'Quiwi, banana, mandarina', 100, 200, '04:34:07', 140, 400, 12);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (52, 'Filet mignon', 'menuFoto/carnes_zmXMrtT.jpg', 'Carne cocina al humo de leña hasta su punto', 100, 200, '04:36:25', 140, 300, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (53, 'Pasta a la boloñesa', 'menuFoto/plato11_QDz9lJy.JPG', 'fideos con carne', 200, 200, '04:39:39', 140, 150, 11);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (54, 'Cecina', 'menuFoto/plato25_f8D1dcL.jpg', 'Carne cocina al humo de leña hasta su punto', 100, 100, '04:41:58', 100, 150, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (55, 'hamburguesa de queso', 'menuFoto/plato10.jpg', 'hamburguesa con carne y queso', 100, 100, '04:43:00', 90, 100, 10);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (56, 'Arroz relleno', 'menuFoto/plato30.jpg', 'Arroz con embutidos  y verduras', 200, 100, '04:45:13', 140, 150, 6);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (57, 'espagueti al ajillo', 'menuFoto/plato15_JKp440p.jpg', 'fideos con tocino, jamon y verduras', 200, 100, '04:46:54', 140, 100, 11);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (58, 'Avena', 'menuFoto/bebida2_cDaQFaG.jpg', 'vaso de avena', 200, 200, '04:48:31', 100, 150, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (59, 'Jugo de mango', 'menuFoto/bebida3_WBbtkfD.jpg', 'jugo hecho con mango', 100, 100, '04:49:17', 140, 150, 4);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (60, 'Pizza de salami', 'menuFoto/pizza_a84rj3K.jpeg', 'pizza con vegetales y salami', 200, 100, '04:50:06', 100, 100, 7);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (61, 'Sopa marinera', 'menuFoto/plato21_D8kfxn1.jpg', 'sopa con mariscos', 200, 200, '04:50:53', 100, 150, 5);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (62, 'Filete de pollo', 'menuFoto/plato3_oIH1Rl9.jpg', 'pollo fileteado hecho a la plancha', 200, 200, '04:53:31', 100, 150, 2);
INSERT INTO spaapp_plato (id_plato, nombre_producto, ruta_foto, descripcion, cantidad_diaria, venta_aproximada, hora_mayor_venta, cantidad_mayor_venta, existencia_actual, id_platocategoria_id) VALUES (63, 'espagueti con carne', 'menuFoto/plato12_Ujh05rb.jpg', 'fideos con carne', 200, 200, '04:54:25', 90, 150, 11);


--
-- Data for Name: spaapp_detallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_detallepedido_id_detalle_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_detallepedido_id_detalle_seq', 1, false);


--
-- Name: spaapp_direccion_id_direccion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_direccion_id_direccion_seq', 1, true);


--
-- Data for Name: spaapp_horariotransportista; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_horariotransportista_id_horario_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_horariotransportista_id_horario_seq', 1, false);


--
-- Data for Name: spaapp_ingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_ingrediente_id_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_ingrediente_id_ingrediente_seq', 1, false);


--
-- Name: spaapp_menu_id_menu_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_menu_id_menu_seq', 25, true);


--
-- Data for Name: spaapp_modificardetallepedido; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_modificardetallepedido_id_modificardetallepedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_modificardetallepedido_id_modificardetallepedido_seq', 1, false);


--
-- Name: spaapp_pedido_id_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_pedido_id_pedido_seq', 1, false);


--
-- Name: spaapp_perfil_id_perfil_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_perfil_id_perfil_seq', 26, true);


--
-- Data for Name: spaapp_plato_id_menu; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (1, 1, 1);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (2, 2, 1);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (3, 3, 1);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (4, 4, 2);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (5, 5, 2);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (6, 6, 2);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (7, 7, 3);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (8, 8, 3);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (9, 9, 3);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (10, 10, 4);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (11, 11, 4);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (12, 12, 4);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (13, 13, 5);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (14, 14, 5);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (15, 15, 5);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (16, 16, 6);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (17, 17, 6);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (18, 18, 7);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (19, 19, 7);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (20, 20, 8);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (21, 21, 8);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (22, 22, 8);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (23, 23, 9);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (24, 24, 9);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (25, 25, 10);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (26, 26, 10);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (27, 27, 11);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (28, 28, 11);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (29, 29, 12);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (30, 30, 12);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (31, 31, 13);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (32, 32, 13);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (33, 33, 14);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (34, 34, 14);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (35, 35, 15);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (36, 36, 15);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (37, 37, 15);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (38, 38, 16);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (39, 39, 16);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (40, 40, 16);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (41, 41, 17);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (42, 42, 17);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (43, 43, 17);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (44, 44, 18);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (45, 45, 18);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (46, 46, 19);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (47, 47, 19);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (48, 48, 19);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (49, 49, 20);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (50, 50, 20);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (51, 51, 20);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (52, 52, 21);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (53, 53, 21);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (54, 54, 22);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (56, 55, 22);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (57, 56, 23);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (58, 57, 23);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (59, 58, 24);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (60, 59, 24);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (61, 60, 24);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (62, 61, 24);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (63, 62, 25);
INSERT INTO spaapp_plato_id_menu (id, plato_id, menu_id) VALUES (64, 63, 25);


--
-- Name: spaapp_plato_id_menu_id_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_menu_id_seq', 64, true);


--
-- Name: spaapp_plato_id_plato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_plato_id_plato_seq', 63, true);


--
-- Name: spaapp_platocategoria_id_platocategoria_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platocategoria_id_platocategoria_seq', 13, true);


--
-- Data for Name: spaapp_platoingrediente; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_platoingrediente_id_plato_ingrediente_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_platoingrediente_id_plato_ingrediente_seq', 1, false);


--
-- Name: spaapp_proveedor_id_proveedor_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_proveedor_id_proveedor_seq', 25, true);


--
-- Data for Name: spaapp_redsocial; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_redsocial_id_red_social_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_redsocial_id_red_social_seq', 1, false);


--
-- Name: spaapp_sucursal_id_sucursal_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_sucursal_id_sucursal_seq', 25, true);


--
-- Data for Name: spaapp_tamanio; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_tamanio (id_tamanio, tamanio) VALUES (1, 'Pequeño');
INSERT INTO spaapp_tamanio (id_tamanio, tamanio) VALUES (2, 'Mediano');
INSERT INTO spaapp_tamanio (id_tamanio, tamanio) VALUES (3, 'Grande');
INSERT INTO spaapp_tamanio (id_tamanio, tamanio) VALUES (4, 'Estandar');


--
-- Name: spaapp_tamanio_id_tamanio_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanio_id_tamanio_seq', 4, true);


--
-- Data for Name: spaapp_tamanioplato; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--

INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (1, 4.5, 1, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (2, 3.25, 1, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (3, 6.5, 1, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (4, 3.5, 2, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (5, 3.5, 2, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (6, 5.5, 3, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (7, 7, 3, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (8, 5, 4, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (9, 5, 4, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (10, 10, 4, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (11, 20, 4, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (12, 5, 5, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (13, 10.5, 5, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (14, 20, 5, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (15, 6, 6, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (16, 12, 6, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (17, 22, 6, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (18, 1, 7, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (19, 1, 8, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (20, 1.5, 9, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (21, 5, 10, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (22, 7.5, 11, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (23, 10, 11, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (24, 4.5, 12, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (25, 0.75, 13, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (26, 1, 14, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (27, 1.25, 15, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (28, 5, 16, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (29, 6, 17, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (30, 3.5, 18, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (31, 3.75, 19, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (32, 4.5, 20, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (33, 6, 21, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (34, 7, 22, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (35, 4, 23, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (36, 3, 24, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (37, 3.75, 25, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (38, 3.5, 26, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (39, 1.5, 27, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (40, 2, 28, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (41, 5, 29, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (42, 10, 29, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (43, 2.5, 30, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (44, 2.5, 31, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (45, 1.5, 32, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (46, 4.90000000000000036, 33, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (47, 5.5, 34, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (48, 4, 35, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (49, 5, 36, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (50, 5, 36, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (51, 7, 36, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (52, 2.5, 37, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (53, 3, 37, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (54, 5, 38, 1);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (55, 6.5, 38, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (56, 8, 38, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (57, 3.5, 39, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (58, 5, 40, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (59, 4, 41, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (60, 5, 42, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (61, 4, 43, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (62, 4, 43, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (63, 3, 44, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (64, 0.599999999999999978, 45, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (65, 1.10000000000000009, 45, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (66, 2.5, 45, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (67, 1, 46, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (68, 1.5, 47, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (69, 3.25, 48, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (70, 1, 49, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (71, 2, 50, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (72, 1.5, 51, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (73, 7, 53, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (74, 10, 52, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (75, 8, 54, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (76, 8, 54, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (77, 5, 55, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (78, 2, 56, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (79, 3.5, 57, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (80, 1, 58, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (81, 1.25, 59, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (82, 10, 60, 2);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (83, 20, 60, 3);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (84, 3, 61, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (85, 3.5, 62, 4);
INSERT INTO spaapp_tamanioplato (id_tamanioplato, valor, id_plato_id, id_tamanio_id) VALUES (86, 4, 63, 4);


--
-- Name: spaapp_tamanioplato_id_tamanioplato_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tamanioplato_id_tamanioplato_seq', 86, true);


--
-- Name: spaapp_tipopago_id_tipo_pago_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopago_id_tipo_pago_seq', 1, false);


--
-- Name: spaapp_tipopedido_id_tipo_pedido_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_tipopedido_id_tipo_pedido_seq', 1, false);


--
-- Name: spaapp_transportista_id_transportista_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_transportista_id_transportista_seq', 1, false);


--
-- Data for Name: spaapp_valoracion; Type: TABLE DATA; Schema: spaapp; Owner: spauser
--



--
-- Name: spaapp_valoracion_id_valoracion_seq; Type: SEQUENCE SET; Schema: spaapp; Owner: spauser
--

SELECT pg_catalog.setval('spaapp_valoracion_id_valoracion_seq', 1, false);


--
-- PostgreSQL database dump complete
--

